package com.kf.palms.reportcontent.service;

import javax.ejb.Local;

import com.kf.palms.reportcontent.Manifest;
import com.kf.palms.reportcontent.ReportContent;

@Local
public interface ReportContentService {

	Manifest findManifestById(String id);
	ReportContent findReportContentById(String id);
	
}
