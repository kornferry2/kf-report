package com.kf.palms.reportcontent.service.marklogic;

import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Stateless;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.kf.palms.reportcontent.Manifest;
import com.kf.palms.reportcontent.ReportContent;
import com.kf.palms.reportcontent.service.ReportContentService;
import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.document.XMLDocumentManager;
import com.marklogic.client.io.DOMHandle;

@Stateless
public class ReportContentServiceImpl implements ReportContentService {
	
	private DatabaseClient databaseClient;
	private XMLDocumentManager xmlDocumentManager;
	
	//leave this here to not break dev finder
	@Resource(mappedName="java:module/marklogic/reportContentRepository/config_properties")
	private Properties localConfigProperties;
	
	@Resource(mappedName="markLogic/rest/reportContentRepository/config_properties")
	private Properties globalConfigProperties;

	@PostConstruct
	public void init() {
		
		Properties properties = null;
		
		if (globalConfigProperties != null) {
			properties = globalConfigProperties;									
		} else if(localConfigProperties != null) {
			properties = localConfigProperties;
		} 
		
		databaseClient = DatabaseClientFactory.newClient(properties.getProperty("host"),
				Integer.parseInt(properties.getProperty("port")),
				properties.getProperty("username"),
				properties.getProperty("password"),
				DatabaseClientFactory.Authentication.DIGEST);

		xmlDocumentManager = databaseClient.newXMLDocumentManager();
	}
	
	private static final String MANIFEST_BASE = "/manifest";
	private static final String CONTENT_BASE = "/content";
	
	@Override
	public Manifest findManifestById(String id) {
		
		DOMHandle manifestHandle = new DOMHandle();
		
		xmlDocumentManager.read(MANIFEST_BASE + "/" + id + "/manifest.xml", manifestHandle);
		Document manifestDoc = manifestHandle.get();
		
		Element root = (Element) manifestDoc.getFirstChild();
		
		// check for base document and load that
		String base = root.getAttribute("base");
		
		Manifest manifest = null;
		
		if (base != null && base.length() > 0) {
			DOMHandle manifestBaseHandle = new DOMHandle();
			
			xmlDocumentManager.read(MANIFEST_BASE + "/" + id + "/manifest.xml", manifestHandle);			
			Document manifestBaseDoc = manifestBaseHandle.get();
			
			manifest = new Manifest(manifestDoc, manifestBaseDoc);
		} else if (manifestDoc != null){
			manifest = new Manifest(manifestDoc);
		}
		
		return manifest;
	}

	@Override
	public ReportContent findReportContentById(String id) {
		
		ReportContent reportContent = null;
		
		DOMHandle reportContentHandle = new DOMHandle();
		
		xmlDocumentManager.read(CONTENT_BASE + "/" + id + ".xml", reportContentHandle);
		
		Document reportContentDoc = reportContentHandle.get();
		if(reportContentDoc != null) {
			reportContent = new ReportContent(reportContentDoc);
		}
		return reportContent;
	}

	@PreDestroy
	public void cleanup() {
		if(databaseClient != null) {
			databaseClient.release();
		}
	}

	public DatabaseClient getDatabaseClient() {
		return databaseClient;
	}

	public void setDatabaseClient(DatabaseClient databaseClient) {
		this.databaseClient = databaseClient;
	}

	public XMLDocumentManager getXmlDocumentManager() {
		return xmlDocumentManager;
	}

	public void setXmlDocumentManager(XMLDocumentManager xmlDocumentManager) {
		this.xmlDocumentManager = xmlDocumentManager;
	}

	public Properties getLocalConfigProperties() {
		return localConfigProperties;
	}

	public void setLocalConfigProperties(Properties localConfigProperties) {
		this.localConfigProperties = localConfigProperties;
	}

	public Properties getGlobalConfigProperties() {
		return globalConfigProperties;
	}

	public void setGlobalConfigProperties(Properties globalConfigProperties) {
		this.globalConfigProperties = globalConfigProperties;
	}


	
}
