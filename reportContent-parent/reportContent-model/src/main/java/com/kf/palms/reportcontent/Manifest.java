package com.kf.palms.reportcontent;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Manifest {

	private String id = "";
	private String modelContentRef = "";
	private String behaviorContentRef = "";
	private String traitContentRef = "";
	private String aboutContentRef = "";
	private String appendixContentRef = "";
	private String graphTypeLookupRef = "";
	private Document xmlDoc;
	private Document xmlBaseDoc;

	private Map<String, String> reportContents = new HashMap<String, String>();

	public Manifest(Document xmlDoc) {
		this.xmlDoc = xmlDoc;
		loadManifestValues();
	}
	
	public Manifest(Document xmlDoc, Document xmlBaseDoc) {
		this.xmlDoc = xmlDoc;
		this.xmlBaseDoc = xmlBaseDoc;
		loadManifestValues();
	}

	private void loadManifestValues() {
		modelContentRef = getTopLevelRef("modelContent");
		behaviorContentRef = getTopLevelRef("behaviorContent");
		traitContentRef = getTopLevelRef("traitContent");
		aboutContentRef = getTopLevelRef("aboutContent");
		appendixContentRef = getTopLevelRef("appendixContent");
		reportContents = getMapRefs("reportContents", "reportContent");
		graphTypeLookupRef = getTopLevelRef("graphTypeLookup");
	}

	private String getTopLevelRef(String name) {
		String ref = "";
		Object node = findNodeByXPath("manifest/" + name, xmlDoc);
		if (node != null) {
			ref = ((Element) node).getAttribute("ref");
		} else if (xmlBaseDoc != null) {
			ref = ((Element) findNodeByXPath("manifest/" + name, xmlBaseDoc))
					.getAttribute("ref");
		}
		return ref;
	}

	private Map<String, String> getMapRefs(String listName, String elementName) {
		Map<String, String> map = new HashMap<String, String>();

		// has base, loop through base first and replace client specific
		if (xmlBaseDoc != null) {

			NodeList nodeList = findNodesByXPath("manifest/" + listName + "/"
					+ elementName, xmlBaseDoc);

			for (int i = 0; i < nodeList.getLength(); i++) {
				Element element = (Element) nodeList.item(i);

				String xpath = "manifest/" + listName + "/" + elementName
						+ "[@id='" + element.getAttribute("id") + "']";
				Object node = findNodeByXPath(xpath, xmlDoc);
				if (node != null) {
					Element overrideElement = (Element) node;
					map.put(overrideElement.getAttribute("id"),
							overrideElement.getAttribute("ref"));
				} else {
					map.put(element.getAttribute("id"),
							element.getAttribute("ref"));
				}
			}
		} else { // no base, just loop through
			NodeList nodeList = findNodesByXPath("manifest/" + listName + "/"
					+ elementName, xmlDoc);
			for (int i = 0; i < nodeList.getLength(); i++) {
				Element element = (Element) nodeList.item(i);
				map.put(element.getAttribute("id"), element.getAttribute("ref"));
			}
		}
		return map;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String, String> getReportContents() {
		return reportContents;
	}

	public void setReportContents(Map<String, String> reportContents) {
		this.reportContents = reportContents;
	}

	public String getModelContentRef() {
		return modelContentRef;
	}

	public void setModelContentRef(String modelContentRef) {
		this.modelContentRef = modelContentRef;
	}

	public String getBehaviorContentRef() {
		return behaviorContentRef;
	}

	public void setBehaviorContentRef(String behaviorContentRef) {
		this.behaviorContentRef = behaviorContentRef;
	}

	public String getTraitContentRef() {
		return traitContentRef;
	}

	public void setTraitContentRef(String traitContentRef) {
		this.traitContentRef = traitContentRef;
	}

	public Document getXmlDoc() {
		return xmlDoc;
	}

	public void setXmlDoc(Document xmlDoc) {
		this.xmlDoc = xmlDoc;
	}

	public Document getXmlBaseDoc() {
		return xmlBaseDoc;
	}

	public void setXmlBaseDoc(Document xmlBaseDoc) {
		this.xmlBaseDoc = xmlBaseDoc;
	}

	public static Object findNodeByXPath(String xPathStr, Node node) {
		XPath xPath = XPathFactory.newInstance().newXPath();
		Object result = null;
		try {
			XPathExpression expr = xPath.compile(xPathStr);
			result = expr.evaluate(node, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static NodeList findNodesByXPath(String xPathStr, Node node) {
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodeList = null;
		try {
			XPathExpression expr = xPath.compile(xPathStr);
			nodeList = (NodeList) expr.evaluate(node, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return nodeList;
	}

	@Override
	public String toString() {
		String str = "id: " + id + "\n";
		str += "modelContent ref: " + modelContentRef + "\n";
		str += "behaviorContent ref: " + behaviorContentRef + "\n";
		str += "traitContent ref: " + traitContentRef + "\n";
		str += "aboutContent ref: " + aboutContentRef + "\n";
		str += "appendixContent ref: " + appendixContentRef + "\n";
		str += "graphTypeLookup ref: " + graphTypeLookupRef + "\n";

		str += "\n";

		Iterator<Entry<String, String>> iter = reportContents.entrySet()
				.iterator();
		str += "reportContents\n";
		while (iter.hasNext()) {
			Entry<String, String> entry = iter.next();
			str += "id: " + entry.getKey() + " ref: " + entry.getValue() + "\n";
		}
		return str;
	}

	public String getAboutContentRef() {
		return aboutContentRef;
	}

	public void setAboutContentRef(String aboutContentRef) {
		this.aboutContentRef = aboutContentRef;
	}

	public String getAppendixContentRef() {
		return appendixContentRef;
	}

	public void setAppendixContentRef(String appendixContentRef) {
		this.appendixContentRef = appendixContentRef;
	}

	public String getGraphTypeLookupRef() {
		return graphTypeLookupRef;
	}

	public void setGraphTypeLookupRef(String graphTypeLookupRef) {
		this.graphTypeLookupRef = graphTypeLookupRef;
	}
}
