package com.kf.palms.reportcontent;

import java.text.MessageFormat;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ReportContent {
	private Document xmlDoc;
	private static final Logger log = LoggerFactory
			.getLogger(ReportContent.class);

	public ReportContent(Document xmlDoc) {
		this.xmlDoc = xmlDoc;
	}

	public String getKeyMapValue(String keyMapId, String keyId) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("//keyMap[@id='" + keyMapId
					+ "']/key[@id='" + keyId + "']");
			Element elm = (Element) expr.evaluate(xmlDoc, XPathConstants.NODE);
			if (elm == null) {
				log.error("ReportContent.getKeyMapValue(): Value not found for "
						+ keyMapId + "/" + keyId);
				return "";
			} else {
				return elm.getTextContent();
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return "";
		}
	}

	public String getKeyMapValue(String keyMapId, String keyId,
			Object... params) {
		String value = getKeyMapValue(keyMapId, keyId);
		return MessageFormat.format(value, params);
	}

	public String getGridMapValue(String gridMapId, String rowId,
			String columnId) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("//gridMap[@id='" + gridMapId
					+ "']/row[@id='" + rowId + "']/column[@id='" + columnId
					+ "']");
			Element elm = (Element) expr.evaluate(xmlDoc, XPathConstants.NODE);

			if (elm == null) {
				log.error("ReportContent.getGridMapValue(): Value not found for "
						+ gridMapId + "/" + rowId + "/" + columnId);
				return "";
			} else {
				return elm.getTextContent();
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return "";
		}
	}

	public String getGridMapValue(String gridMapId, String rowId,
			String columnId, Object... params) {
		String value = getGridMapValue(gridMapId, rowId, columnId);
		return MessageFormat.format(value, params);
	}

	// Key Value lookup With Language
	public String getKeyMapValue4Lang(String keyMapId, String keyId, String lang) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("//keyMap[@id='" + keyMapId
					+ "']/key[@id='" + keyId + "']/contents/content[@lang='"
					+ lang + "']");
			Element elm = (Element) expr.evaluate(xmlDoc, XPathConstants.NODE);
			if (elm == null) {
				log.error("ReportContent.getKeyMapValue(): Value not found for "
						+ keyMapId + "/" + keyId + "/" + lang);
				return "";
			} else {
				return elm.getTextContent();
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public String getKeyMapValue4LangWithDefault(String keyMapId, String keyId, String lang, String defaultLang) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("//keyMap[@id='" + keyMapId
					+ "']/key[@id='" + keyId + "']/contents/content[@lang='"
					+ lang + "']");
			Element elm = (Element) expr.evaluate(xmlDoc, XPathConstants.NODE);
			if (elm == null) {
				return getKeyMapValue4Lang(keyMapId, keyId, defaultLang);
			} else {
				return elm.getTextContent();
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public String getKeyMapValue4LangWithDefault(String keyMapId, String keyId, String lang, String defaultLang, Object ... params) {
		String value = getKeyMapValue4LangWithDefault(keyMapId, keyId, lang, defaultLang);
		return MessageFormat.format(value, params);
	}
	
	public String getKeyMapValue4Lang(String keyMapId, String keyId, String lang, Object ... params) {
		String value = getKeyMapValue4Lang(keyMapId, keyId, lang);
		return MessageFormat.format(value, params);
	}

	// Grid Map lookup with Language
	public String getGridMapValue4Lang(String gridMapId, String rowId,
			String columnId, String lang) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr;
			if (columnId != "*") {
				expr = xpath.compile("//gridMap[@id='" + gridMapId
						+ "']/row[@id='" + rowId + "']/column[@id='" + columnId
						+ "']/contents/content[@lang='" + lang + "']");
				Element elm = (Element) expr.evaluate(xmlDoc,
						XPathConstants.NODE);
				if (elm == null) {
					log.error("ReportContent.getGridMapValue(): Value not found for "
							+ gridMapId
							+ "/"
							+ rowId
							+ "/"
							+ columnId
							+ "/"
							+ lang);
					return "";
				} else {
					return elm.getTextContent();
				}
			} else {
				// FIXME refactor into it's own function for making a list from
				// many columns contents per lang and show attributes?

				// if '*' get all columns, else get only column specified
				expr = xpath.compile("//gridMap[@id='" + gridMapId
						+ "']/row[@id='" + rowId
						+ "']/column/contents/content[@lang='" + lang
						+ "' and @show='true']");

				NodeList result = (NodeList) expr.evaluate(xmlDoc,
						XPathConstants.NODESET);

				String returnString = "<ul>";
				for (int i = 0; i < result.getLength(); i++) {
					Node n = result.item(i);
					if (n != null && n.getNodeType() == Node.ELEMENT_NODE) {
						Element column = (Element) n;
						returnString += "<li>" + column.getTextContent()
								+ "</li>";
					}
				}
				returnString += "</ul>";
				return returnString;
			}

		} catch (XPathExpressionException e) {
			log.error(e.getMessage(), e);
			return "";
		}
	}
	
	public String getGridMapValue4Lang(String gridMapId, String rowId,
			String columnId, String lang, Object ... params) {
		String value = getGridMapValue4Lang(gridMapId, rowId, columnId, lang);
		return MessageFormat.format(value, params);
	}

	public Document getXmlDoc() {
		return xmlDoc;
	}

	public void setXmlDoc(Document xmlDoc) {
		this.xmlDoc = xmlDoc;
	}
}
