USE [reportservice]
GO

SET IDENTITY_INSERT [dbo].[report_content_model] ON
GO

INSERT INTO [dbo].[report_content_model] (report_content_model_id, participant_id, report_type, report_xml)
VALUES (1, 1, 'INBOX_SIMULATOR','<?xml version="1.0" encoding="utf-8"?><ibReport><reportTitle>Leadership Assessment Report</reportTitle></ibReport>');
GO

SET IDENTITY_INSERT [dbo].[report_content_model] OFF
GO

