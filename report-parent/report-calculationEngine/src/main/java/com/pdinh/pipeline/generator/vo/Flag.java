/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.generator.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(propOrder={"id", "value"})	// order has no effect on attributes
@XmlType(name = "")
public class Flag
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute(name = "id")
	private String id;
	@XmlAttribute(name = "value")
	private String value;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Flag()
	{
		// Default constructor
	}
	
	// 2-parameter constructor
	public Flag(String id, String value)
	{
		this.id = id;
		this.value = value;
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "Flag:  ";
		str += "id=" + this.id + ",  val=" + this.value + "  ";
		
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getId()
	{
		return this.id;
	}

	public void setId(String value)
	{
		this.id = value;
	}

	//----------------------------------
	public String getValue()
	{
		return this.value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}
}
