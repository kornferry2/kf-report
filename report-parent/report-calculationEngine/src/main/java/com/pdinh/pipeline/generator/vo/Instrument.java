/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.generator.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"var"})
public class Instrument
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute(name = "id")
	private String id;
	private List<Var> var;

	//
	// Constructors.
	//

	// 0 parameter constructor
	public Instrument()
	{
		// default 0 parameter constructor
	}

	// 1 parameter constructor
	public Instrument(String id)
	{
		this.id = id;
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "Instrument:  id=" + this.id + "  ";
		if (var == null)
		{
			str += "Var list = null  ";
		}
		else
		{
			for (Var vv : var)
			{
				str += vv.toString() + "  ";
			}
		}
		
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getId()
	{
		return this.id;
	}

	public void setId(String value)
	{
		this.id = value;
	}

	//----------------------------------
	public List<Var> getVar()
	{
		if (this.var == null)
		{
			this.var = new ArrayList<Var>();
		}
		return this.var;
	}
}
