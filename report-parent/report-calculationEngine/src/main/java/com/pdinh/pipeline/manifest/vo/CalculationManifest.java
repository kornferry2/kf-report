/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.manifest.vo;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class CalculationManifest
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlElement
	private String description;

	@XmlElement
	private Steps steps;


	//
	// Constructors.
	//

	// 0-parameter constructor
	public CalculationManifest()
	{
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		return "CalculationManifest:  calc=" + this.description + "   " + steps.toString();
	}
	
	// Getters & setters
	

	//--------------------------------------
	public String getDescription()
	{
		return this.description;
	}

	public void setDesciption(String val)
	{
		this.description = val;
	}

	//--------------------------------------
	public Steps getSteps()
	{
		return this.steps;
	}

	public void setSteps(Steps val)
	{
		this.steps = val;
	}
}
