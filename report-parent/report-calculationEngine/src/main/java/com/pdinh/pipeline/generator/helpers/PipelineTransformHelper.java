/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.generator.helpers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.pdinh.pipeline.manifest.vo.ReportManifest;
import com.pdinh.pipeline.manifest.vo.Param;
import com.pdinh.pipeline.manifest.vo.CalculationManifest;
import com.pdinh.pipeline.manifest.vo.Step;

public class PipelineTransformHelper
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private boolean debugging = false;
	private String cerPath;
	
	// NOTE: This is here to help with debug.  is this really needed?
	private String pptId;

	//
	// Constructors.
	//
	public PipelineTransformHelper(Boolean debugging, String cerPath)
	{
		this.debugging = debugging;
		this.cerPath = cerPath;
	}

	//
	// Instance methods.
	//
	
	/*
	 * This is the heart of the pipeline.  It takes a manifest object and a
	 * report generation request and return the transformed data as a string.
	 * 
	 * This method loops through the steps in the manifest and invokes the transform as needed.
	 * 
	 * NOTE:	This logic and that in the transforms assumes that the responses are all present.  If
	 * 			they are not, the transform will gack when it cannot find the response.  Per Chris,
	 * 			the JavaScript does not handle null responses well.
	 */
	public String process(ReportManifest man, String inpXml)
	{
		String workingXml = new String(inpXml);
		
		//if (debugging)
		//{
		//	// Look for the participant ID - kinda hoky but it saves us a transform.
		//	int idx = workingXml.indexOf("<participant ");
		//	if (idx == -1)
		//	{
		//		this.pptId = "noPptId";
		//	}
		//	else
		//	{
		//		char ch = '"';
		//		idx = workingXml.indexOf(ch,idx);
		//		int idx2 = workingXml.indexOf(ch,idx+1);
		//		this.pptId = workingXml.substring(idx+1,idx2);
		//	}
		//}

		//String type = man.getCalculationSteps().getType();	// Not used currently

		// Make a Xalan transformer
		TransformerFactory tFactory = TransformerFactory.newInstance("org.apache.xalan.processor.TransformerFactoryImpl", null); 

		// Got the stage map... loop through the manifest steps
		for (Step ss : man.getCalculationSteps().getStep())
		{
			String stepName = ss.getStepName();
			String stageLabel = ss.getStageLabel();
			String xslFile = ss.getXsl();
			Param param = ss.getParam();

			//// Debug
			//if (param != null)
			//{
			//	System.out.println("Step/stage " + stepName + "/" + stageId + " has a parameter... Skipping");
			//	continue;
			//}
			
			// Create the path name for the step and get the Source object
			String xslPath = cerPath + "transforms/" + stepName + xslFile;
			System.out.println("XSL v1 path = " + xslPath);

			workingXml = transform(tFactory,param,xslPath, workingXml, stepName,stageLabel);
		}	// End of steps loop		
		return workingXml;
	}

	public String process(CalculationManifest man, String inpXml)
	{
		String workingXml = new String(inpXml);
		
		// Make a Xalan transformer
		TransformerFactory tFactory = TransformerFactory.newInstance("org.apache.xalan.processor.TransformerFactoryImpl", null); 

		// Got the stage map... loop through the manifest steps
		for (Step ss : man.getSteps().getStep())
		{
			Param param = ss.getParam();

			String stepName = "not_required";
			String stageId = ss.getId();
			String xslFile = ss.getCalculationRef();
						
			// Create the path name for the step and get the Source object
			
			String xslPath = cerPath + "transforms/" + xslFile +".xsl";
			System.out.println("XSL v2 path = " + xslPath);
			
			// Now recycle the xml to make the output of this step the input fr the next
			workingXml = transform(tFactory,param,xslPath, workingXml, stepName, stageId);
		}	// End of steps loop
		
		return workingXml;
	}
	
	private String transform(TransformerFactory tFactory, Param param, String xslPath, String workingXml, String stepName, String stageId){
		// Set up the transform (will be Xalan, see above)
		Transformer tformr = null;
		StreamSource styleSheet = new StreamSource(new File(xslPath));
		
		try
		{
			tformr = tFactory.newTransformer(styleSheet);
			tformr.setErrorListener(new PipelineTransformErrorListener());
		}
		catch (TransformerConfigurationException e)
		{
			System.out.println("Bad Transformer Config:  Stylesheet=" + xslPath + ", msg=" + e.getMessage());
			e.printStackTrace();
			return null;
		}

		// Do the Xalan transform
		StringReader inp = new StringReader(workingXml);
		StringWriter out = new StringWriter();
		try
		{
			if (param != null)
			{
				tformr.setParameter(param.getName(), param.getUri());
			}
			
			tformr.transform(new StreamSource(inp), new StreamResult(out));
		}
		catch (TransformerException e)
		{
			System.out.println("Bad transform:  step/stage=" + stepName + "/" + stageId + ", msg=" + e.getMessage());
			e.printStackTrace();
			return null;
		}

		if (debugging)
		{
			// Look for the participant ID - kinda hoky but it saves us a transform.
			int idx = workingXml.indexOf("<participant ");
			if (idx == -1)
			{
				this.pptId = "noPptId";
			}
			else
			{
				char ch = '"';
				idx = workingXml.indexOf(ch,idx);
				int idx2 = workingXml.indexOf(ch,idx+1);
				this.pptId = workingXml.substring(idx+1,idx2);
			}

			String fName = this.pptId + "-" + stepName + "-" + stageId + "-output.xml";
			String outName = cerPath + "testing/debugFiles/" + fName;
			try
			{
				FileWriter fw = new FileWriter(new File(outName));
				fw.write(out.toString());
				fw.close();
			}
			catch (IOException e)
			{
				System.out.println("Can't dump transform output file.  msg=" + e.getMessage());
				//e.printStackTrace();
			}
			System.out.println("DEBUG:  Transform for " + stepName + "/" + stageId + " complete.  Output saved in " + outName);
		}
		
		// Now recycle the xml to make the output of this step the input fr the next
		return workingXml = out.toString();
	}	

}
