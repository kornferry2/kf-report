/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.dto;


/*
 * StatusDTO is a convenience class that wraps a status holder around other
 * content.  It is meant to be used as a parent class for other DTOs
 */
public class StatusDTO
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private boolean success;
	private String errorMsg;
	// As needed, add anything else that might be useful
	
	//
	// Constructors.
	//
	public StatusDTO()
	{
		this.success = true;	// default to successful
	}

	//
	// Instance methods.
	//

	// Getters & setters

	// booleans are "special
	//-------------------------------------------
	public boolean isSuccess()
	{
		return success;
	}
	
	public void setSuccess()
	{
		this.success = true;
	}
	
	public void setFailure()
	{
		this.success = false;
	}

	//-------------------------------------------
	public String getErrorMsg()
	{
		return this.errorMsg;
	}
	
	public void setErrorMsg(String value)
	{
		this.errorMsg = value;
	}
}
