/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.generator.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "flag", "participant", "groupStage" })
@XmlRootElement(name = "reportGenerationRequest")
public class ReportGenerationRequest {
	//
	// Static data.
	//

	private static String RESPONSE_STAGE = "responses";
	private static String TLT_INSTRUMENT = "tlt";
	private static String GPIRV_INSTRUMENT = "gpi-rv";

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private List<Flag> flag;
	@XmlElementWrapper(name = "participants")
	private List<Participant> participant;

	@XmlElementWrapper(name = "groupStages")
	private List<GroupStage> groupStage;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public ReportGenerationRequest() {
	}

	//
	//
	// Instance methods.
	//

	@Override
	public String toString() {
		String str = "ReportGenerationRequest:  ";
		if (this.flag == null) {
			str += "No flags  ";
		} else {
			for (Flag fl : this.flag) {
				str += fl.toString() + "  ";
			}
		}

		for (Participant pt : this.participant) {
			str += pt.toString();
		}

		for (GroupStage g : this.groupStage) {
			str += g.toString();
		}

		return str;
	}

	// Getters & setters

	// ----------------------------------
	public List<Flag> getFlag() {
		if (this.flag == null) {
			this.flag = new ArrayList<Flag>();
		}
		return this.flag;
	}

	// ----------------------------------
	public List<Participant> getParticipant() {
		if (this.participant == null)
			this.participant = new ArrayList<Participant>();

		return this.participant;
	}

	public Participant findParticipantByExtId(String extId) {
		Participant part = null;
		for (Participant p : getParticipant()) {
			if (p.getExtId().equals(extId)) {
				part = p;
				break;
			}
		}
		return part;
	}

	// ----------------------------------

	public List<GroupStage> getGroupStage() {
		if (this.groupStage == null)
			this.groupStage = new ArrayList<GroupStage>();
		return this.groupStage;
	}

	public GroupStage findGroupStageByExtId(String extId) {
		GroupStage groupStage = null;
		for (GroupStage g : getGroupStage()) {
			if (g.getExtId().equals(extId)) {
				groupStage = g;
				break;
			}
		}
		return groupStage;
	}

	/**
	 * checkTltAndGpiComplete - Looks for both a "tlt" and a "gpi-rv" instrument
	 * in the responses section. NOTE: This assumes a single participant in the
	 * RGR.
	 * 
	 * @param rgr
	 * @return
	 */
	public boolean checkTltAndGpiComplete() {
		boolean tltFlg = false;
		boolean gpirvFlg = false;

		if (getParticipant() == null || getParticipant().get(0) == null || getParticipant().get(0).getStage() == null)
			return false;

		// Loop through stages looking for "responses"
		for (Stage stg : getParticipant().get(0).getStage()) {
			if (stg.getId().equals(RESPONSE_STAGE)) {
				List<Instrument> insts = stg.getInstrument();
				if (insts == null) {
					continue;
				}

				// Loop through instruments looking for tlt & gpi-rv
				for (Instrument inst : insts) {
					if (inst.getId().equals(TLT_INSTRUMENT)) {
						tltFlg = true;
						continue;
					}
					if (inst.getId().equals(GPIRV_INSTRUMENT)) {
						// We have the "instrument" but do we have at least some
						// GPI?? Ravens may be optional
						boolean gpi = false;
						// boolean rv = false;
						for (Var var : inst.getVar()) {
							String str = var.getId().substring(0, 4);
							// if (str.equals("RAVE")) {
							// rv = true;
							// continue;
							// }
							if (str.equals("GPI_")) {
								gpi = true;
								continue;
							}
							// if (gpi && rv) {
							// break;
							// }
							if (gpi) {
								break;
							}
						} // End of the var loop

						// if (gpi && rv) {
						if (gpi) {
							gpirvFlg = true;
							break;
						} else {
							continue;
						}
					}
				} // End of the instrument loop
			} // End of the "responses" stage processing
		} // End of the stage loop

		return (tltFlg && gpirvFlg);
	}

}
