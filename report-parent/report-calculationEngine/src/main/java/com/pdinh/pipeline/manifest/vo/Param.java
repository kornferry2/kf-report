/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.manifest.vo;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder={"name", "uri"})
public class Param
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String name;
	private String uri;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Param()
	{
		
	}
	
	// 2-parameter constructor
	public Param(String nm, String uri)
	{
		this.name = nm;
		this.uri = uri;
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		return "Param: name=" + name + ", id=" + uri + "  ";
	}
	
	// Getters & setters
	
	//--------------------------------------
	@XmlAttribute
	public String getName()
	{
		return this.name;
	}

	public void setName(String val)
	{
		this.name = val;
	}
	
	//--------------------------------------
	@XmlAttribute
	public String getUri()
	{
		return this.uri;
	}
	
	public void setUri(String val)
	{
		this.uri = val;
	}
}
