/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.manifest.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"step"})
public class CalculationSteps
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute
	private String type;
    private List<Step> step;


	//
	// Constructors.
	//

	// 0-parameter constructor
	public CalculationSteps()
	{
	}

	// 1-parameter constructor
	public CalculationSteps(String type)
	{
		this.type = type;
	}
	
	//
	//
	// Instance methods.
	//
	
    public String toString()
    {
    	String ret = " CalculationSteps:  type=" + type;
    	ret += ", steps=";
    	for(Step s : step)
    	{
    		ret += s.toString() + "    ";
    	}
    		
    	return ret;
    }
	
	// Getters & setters
	
	//--------------------------
	public String getType()
	{
		return this.type;
	}

	public void setType(String value)
	{
		this.type = value;
	}

	//----------------------------------
    public List<Step> getStep()
    {
        if (this.step == null)
        {
        	this.step = new ArrayList<Step>();
        }
        return this.step;
    }
}
