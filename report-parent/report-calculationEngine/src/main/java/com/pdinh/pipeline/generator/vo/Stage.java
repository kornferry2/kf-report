/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.generator.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"norm", "var", "instrument"})
public class Stage
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute(name = "id")
	private String id;
	private List<Var> var;
    private List<Instrument> instrument;
    protected List<Stage.Norm> norm;

	//
	// Constructors.
	//

	// 0 parameter constructor
	public Stage()
	{
		// default 0 parameter constructor
	}

	// 1 parameter constructor
	public Stage(String id)
	{
		this.id = id;
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "Stage:  id=" + this.id + "  ";
		if (var == null)
		{
			str += "Var list = null  ";
		}
		else
		{
			for (Var vv : var)
			{
				str += vv.toString() + "  ";
			}
		}
		if (instrument == null)
		{
			str += "Instrument list = null  ";
		}
		else
		{
			for (Instrument inst : instrument)
			{
				str += inst.toString() + "  ";
			}
		}
		if (norm == null)
		{
			str += "Norm list = null  ";
		}
		else
		{
			for (Stage.Norm nn : norm)
			{
				str += nn.toString() + "  ";
			}
		}
		
		return str;
	}
	
	// Getters & setters
	
	//----------------------------------
	public String getId()
	{
		return this.id;
	}

	public void setId(String value)
	{
		this.id = value;
	}
	
	//----------------------------------
	public List<Instrument> getInstrument()
	{
		if (this.instrument == null)
		{
			this.instrument = new ArrayList<Instrument>();
		}
		return this.instrument;
	}
	
	//----------------------------------
	public List<Var> getVar()
	{
		if (this.var == null)
		{
			this.var = new ArrayList<Var>();
		}
		return this.var;
	}
	
	//----------------------------------
	public List<Stage.Norm> getNorm()
	{
		if (this.norm == null)
		{
			this.norm = new ArrayList<Stage.Norm>();
		}
		return this.norm;
	}
	
	
	
	
	
	
	
	
	
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {"mean", "sd"})
	public static class Norm
    {
		//
		// Instance data.
		//
		@XmlAttribute(name = "scaleGroup")
		protected String scaleGroup;
		protected String mean;
		protected String sd;

		//
		// Constructors
		//

		public Norm()
		{
			// 0 parameter constructor (default)
		}

		// 3 parameter constructor
		public Norm(String name, String mean, String sd)
		{
			this.scaleGroup = name;
			this.mean = mean;
			this.sd = sd;
		}
		
		//
		//
		// Instance methods.
		//

		public String toString()
		{
			String str = "Stage.norm:  scaleGroup=" + this.scaleGroup;
			str += ", mean=" + this.mean;
			str += ", stdev=" + this.sd + "  ";

			return str;
		}
		
		// Getters & setters

		public void setScaleGroup(String value)
		{
			this.scaleGroup = value;
		}

		public String getMean()
		{
			return mean;
		}

		public void setMean(String value)
		{
			this.mean = value;
		}

		public String getSd()
		{
			return sd;
		}

		public void setSd(String value)
		{
			this.sd = value;
		}

		public String getScaleGroup()
		{
			return scaleGroup;
		}
    }

}
