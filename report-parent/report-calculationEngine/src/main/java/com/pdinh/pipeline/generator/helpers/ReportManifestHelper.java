/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.generator.helpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.pdinh.pipeline.manifest.vo.ReportManifest;

/*
 * Fetch a canned report manifest.
 * 
 * Assumes that the data lies in SREngine/manifest/<instCode>
 */
public class ReportManifestHelper {
	//
	// Static data.
	//
	private static String MANIFEST_FOLDER = "manifest/";
	private static String DFLT_MAN_FILE = "reportManifest.xml";

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private boolean debugging = false;
	private String cerPath = null; // The path to the
									// CalculationEngingeResources folder

	//
	// Constructors.
	//

	/*
	 * There is only a 2-parameter constructor
	 */
	public ReportManifestHelper(Boolean debugging, String cerPath) {
		this.debugging = debugging;
		this.cerPath = cerPath;
	}

	//
	// Instance methods.
	//

	/*
	 * Method to fetch a report manifest.
	 * 
	 * Assumes:
	 * 		-- The user knows what he/she is doing and the file name is correct
	 */
	public ReportManifest getReportManifest(String instCode, String manFile) {
		return fetchManifest(instCode, manFile);
	}

	/*
	 * Method to fetch a report manifest.
	 * 
	 * Assumes:
	 * 		-- The manifest name is the  static default name.
	 * 		-- It resides in SREngine/response in the folder named for the instrument being gathered.
	 * 		-- The name of the manifest file is "reportManifest.xml".
	 */
	public ReportManifest getReportManifest(String instCode) {
		// Add the default file name and call the private method
		return fetchManifest(instCode, DFLT_MAN_FILE);
	}

	/*
	 * Method to fetch a report manifest.
	 * 
	 * Assumes:
	 * 		-- The manifest resides in CalculationEngineResources/manifest in the folder
	 * 		   named for the instrument being gathered.
	 */
	private ReportManifest fetchManifest(String instCode, String manFile) {
		// Find the manifest file
		// build the path to the report manifest
		String genPath = this.cerPath + MANIFEST_FOLDER + instCode + "/" + manFile;
		if (this.debugging)
			System.out.println("Manifest genPath=" + genPath);
		File ff = new File(genPath);

		// Check if accessible
		String tstPath = null;
		try {
			tstPath = ff.getCanonicalPath();
		} catch (IOException e) {
			System.out.println("Error accessing path name (" + genPath + ").  msg=" + e.getMessage());
			return null;
		}
		if (this.debugging)
			System.out.println("Manifest File Path=" + tstPath);

		// Un-marshal the manifest
		FileInputStream fis = null;
		JAXBContext jc = null;
		Unmarshaller um = null;
		ReportManifest rm = null;
		try {
			fis = new FileInputStream(ff);
			// Create the context & the unmarshaller
			jc = JAXBContext.newInstance(ReportManifest.class);
			um = jc.createUnmarshaller();
			rm = (ReportManifest) um.unmarshal(fis);

			if (this.debugging) {
				// Convert the input to a string for output
				StringBuilder inp = new StringBuilder((int) ff.length());
				Scanner scanner = new Scanner(ff);
				String lineSeparator = System.getProperty("line.separator");
				try {
					while (scanner.hasNextLine()) {
						inp.append(scanner.nextLine() + lineSeparator);
					}
				} finally {
					scanner.close();
				}
				System.out.println("DEBUG - Report Manifest input:\n" + inp.toString());

				System.out.println("DEBUG - Report Manifest output:\n" + rm.toString());
			}

			return rm;
		} catch (FileNotFoundException e) {
			System.out.println("Manifest does not exist.  Path=" + tstPath + ",  msg=" + e.getMessage());
			return null;
		} catch (JAXBException e) {
			System.out.println("Error unmarshalling manifest.  msg=" + e.getMessage());
			return null;
		} finally {
			um = null;
			jc = null;
			try {
				fis.close();
			} catch (Exception e) {
				System.out
						.println("ReportManifestHelper.fetchManifest() - Error closing FileInputStream is ignored.  Msg="
								+ e.getMessage());
			}
		}
	}
}
