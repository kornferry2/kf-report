/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.manifest.vo;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder={"stepName", "stageLabel", "stageDesc", "genFile", "xsl", "id", "calculationRef", "param"})
public class Step
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	// Attribute/Element declaration done at setter/getter level
	private String stepName;
	private String stageLabel;
	private String stageDesc;
	private String genFile;
	private String xsl;
	private String id;	
	private String calculationRef;	
	private Param param;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Step()
	{
	}
	
	// 2-parameter constructor 
	public Step(String id, String calRef)
	{
		this.id = id;
		this.calculationRef = calRef;
	}
	// 3-parameter constructor 
	public Step(String step, String stage, String x)
	{
		this.stepName = step;
		this.stageLabel = stage;
		this.xsl = x;
	}
	// 5-parameter for calculation pipeline 
	public Step(String step, String stage, String desc, String gen, String x)
	{
		this.stepName = step;
		this.stageLabel = stage;
		this.stageDesc = desc;
		this.genFile = gen;
		this.xsl = x;
	}
	
	// 4-parameter constructor
	public Step(String step, String stage, String x, Param p)
	{
		this.stepName = step;
		this.stageLabel = stage;
		this.xsl = x;
		this.param = p;
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String ret = "  Step: stepName=" + stepName + ", stageLabel=" + stageLabel +", stageDesc=" + 
									stageDesc +", genFile=" + genFile +", xsl=" + xsl +
									", id=" + id + ", calculationRef=" + calculationRef + ", parm=";
		ret += (param==null ? "NONE" : param.toString());
		
		return ret;
	}
	
	// Getters & setters
	
	//--------------------------------------
	@XmlAttribute
	public String getStepName()
	{
		return this.stepName;
	}
	
	public void setStepName(String val)
	{
		this.stepName = val;
	}
	//--------------------------------------
	@XmlAttribute
	public String getStageLabel()
	{
		return this.stageLabel;
	}
	
	public void setStageLabel(String val)
	{
		this.stageLabel = val;
	}

	//--------------------------------------
	@XmlAttribute
	public String getStageDesc()
	{
		return this.stageDesc;
	}
	
	public void setStageDesc(String val)
	{
		this.stageDesc = val;
	}
	
	//--------------------------------------
	@XmlAttribute
	public String getGenFile()
	{
		return this.genFile;
	}
	
	public void setGenFile(String val)
	{
		this.genFile = val;
	}

	//--------------------------------
	@XmlAttribute
	public String getXsl()
	{
		return this.xsl;
	}
	
	public void setXsl(String val)
	{
		this.xsl = val;
	}
	
	//--------------------------------
	@XmlAttribute
	public String getId()
	{
		return this.id;
	}
	
	public void setId(String val)
	{
		this.id = val;
	}

	//--------------------------------
	@XmlAttribute
	public String getCalculationRef()
	{
		return this.calculationRef;
	}
	
	public void setCalculationRef(String val)
	{
		this.calculationRef = val;
	}

	//--------------------------------
	@XmlElement
	public Param getParam()
	{
		return this.param;
	}
	
	public void setParam(Param val)
	{
		this.param = val;
	}
}
