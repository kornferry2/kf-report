/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.generator.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"name"})
public class Project
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute
	private int id;
	@XmlAttribute
	private String extId;
	@XmlValue
	private String name;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Project()
	{
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "Project:  ";
		str += "id=" + this.id + ", extId=" + this.extId + ", name=" + this.name + "  ";
		
		return str;
	}
	
	// Getters & setters
	
	//----------------------------------
	public int getId()
	{
		return this.id;
	}

	public void setId(int value)
	{
		this.id = value;
	}
	
	//----------------------------------
	public String getExtId()
	{
		return this.extId;
	}

	public void setExtId(String value)
	{
		this.extId = value;
	}

	//----------------------------------
	public String getName()
	{
		return this.name;
	}

	public void setName(String value)
	{
		this.name = value;
	}

}
