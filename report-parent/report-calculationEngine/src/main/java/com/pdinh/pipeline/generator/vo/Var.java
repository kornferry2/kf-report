/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.generator.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class Var
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute(name = "id")
	private String id;
	@XmlAttribute(name = "value")
	private String value;
	@XmlAttribute(name = "blockId")
	private String blockId;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Var()
	{
		// default 0 parameter constructor
	}

	// 2 parameter constructor
	public Var(String id, String value)
	{
		this.id = id;
		this.value = value;
	}
	// 3 parameter constructor
	public Var(String id, String value, String blockId)
	{
		this.id = id;
		this.value = value;
		this.blockId = blockId;
	}

	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		return "Var:  id=" + this.id + " value=" + this.value + " blockId=" + this.blockId + "  ";
	}
	
	// Getters & setters

	//----------------------------------
	public String getId()
	{
		return this.id;
	}

	public void setId(String value)
	{
		this.id = value;
	}

	//----------------------------------
	public String getValue()
	{
		return this.value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}
	//----------------------------------
	public String getBlockId()
	{
		return this.blockId;
	}

	public void setBlockId(String value)
	{
		this.blockId = value;
	}

}
