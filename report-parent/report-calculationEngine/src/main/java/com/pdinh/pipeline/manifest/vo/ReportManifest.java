/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.manifest.vo;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class ReportManifest
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlElement
	private String report;

	@XmlElement
	private CalculationSteps calculationSteps;


	//
	// Constructors.
	//

	// 0-parameter constructor
	public ReportManifest()
	{
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		return "ReportManifest:  rpt=" + this.report + "   " + calculationSteps.toString();
	}
	
	// Getters & setters
	

	//--------------------------------------
	public String getReport()
	{
		return this.report;
	}

	public void setReport(String val)
	{
		this.report = val;
	}

	//--------------------------------------
	public CalculationSteps getCalculationSteps()
	{
		return this.calculationSteps;
	}

	public void setCalculationSteps(CalculationSteps val)
	{
		this.calculationSteps = val;
	}
}
