/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.pipeline.dto;

/*
 * ObjectDTO is a convenience class that wraps an arbitrary object with a status holder
 */
public class ObjectDTO extends StatusDTO
{
	//
	// Static data.
	//
 
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Object obj;
	// As needed, add anything else that might be useful
	
	//
	// Constructors.
	//
	public ObjectDTO()
	{
		// Default 0-parm constructor
	}

	// 1-param constructor allows us to create a successful DTO with its conten packet
	public ObjectDTO(Object obj)
	{
		this.obj = obj;
	}

	// 1-param constructor allows us to pass through a StatusDTO
	public ObjectDTO(StatusDTO stat)
	{
		if (stat.isSuccess())
			this.setSuccess();
		else
			this.setFailure();
		this.setErrorMsg(stat.getErrorMsg());
	}

	//
	// Instance methods.
	//
	
	/*
	 * Convenience method to set up a failed process with the string 
	 */
	public ObjectDTO failed(String errMsg)
	{
		this.setFailure();
		this.setErrorMsg(errMsg);
		
		return this;
	}

	// Getters & setters
	//-------------------------------------------
	public Object getObject()
	{
		return this.obj;
	}
	
	public void setObject(Object value)
	{
		this.obj = value;
	}

}
