/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.generator.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "assessmentDate", "company", "firstName", "lastName", "title", "project",
		"reportContentModel", "stage" })
public class Participant {
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute
	private int id;
	@XmlAttribute
	private String extId;
	private String assessmentDate;
	private String company;
	private String firstName;
	private String lastName;
	private String title;
	private Project project;
	private ReportContentModelData reportContentModel;
	private List<Stage> stage;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Participant() {
	}

	// // 3-parameter constructor
	// public Participant(String id, String fn, String ln)
	// {
	// this.id = id;
	// this.firstName = fn;
	// this.lastName = ln;
	// }

	//
	//
	// Instance methods.
	//

	@Override
	public String toString() {
		String str = "Participant:  ";
		str += "id=" + this.id + ", name=" + this.firstName + " " + this.lastName + "  ";
		str += project.toString() + "  ";

		for (Stage st : stage) {
			str += st.toString() + "  ";
		}

		return str;
	}

	// Getters & setters

	// ----------------------------------
	public int getId() {
		return this.id;
	}

	public void setId(int value) {
		this.id = value;
	}

	// ----------------------------------
	public String getExtId() {
		return this.extId;
	}

	public void setExtId(String value) {
		this.extId = value;
	}

	// ----------------------------------
	public String getAssessmentDate() {
		return this.assessmentDate;
	}

	public void setAssessmentDate(String value) {
		this.assessmentDate = value;
	}

	// ----------------------------------
	public String getCompany() {
		return this.company;
	}

	public void setCompany(String value) {
		this.company = value;
	}

	// ----------------------------------
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String value) {
		this.firstName = value;
	}

	// ----------------------------------
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String value) {
		this.lastName = value;
	}

	// ----------------------------------
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String value) {
		this.title = value;
	}

	// ----------------------------------
	public Project getProject() {
		return this.project;
	}

	public void setProject(Project value) {
		this.project = value;
	}

	// ----------------------------------
	public ReportContentModelData getReportContentModel() {
		if (this.reportContentModel == null)
			this.reportContentModel = new ReportContentModelData();
		return this.reportContentModel;
	}

	public void setReportContentModel(ReportContentModelData value) {
		this.reportContentModel = value;
	}

	// ----------------------------------
	public List<Stage> getStage() {
		if (this.stage == null) {
			this.stage = new ArrayList<Stage>();
		}
		return this.stage;
	}

	// ----------------------------------
	public Var findVarById(String id) {
		Var var = null;
		for (Stage stage : getStage()) {
			for (Instrument instrument : stage.getInstrument()) {
				for (Var instrumentVar : instrument.getVar()) {
					if (instrumentVar.getId().equals(id)) {
						return instrumentVar;
					}
				}
			}
			for (Var stageVar : stage.getVar()) {
				if (stageVar.getId().equals(id)) {
					return stageVar;
				}
			}
		}
		return var;
	}
}
