/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.generator.helpers;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.TransformerException;

/*
 * Class to detect the internal Transfomer errors that don't bubble out and make them do so
 * This may need additional logic if we find that some don't need to bubble out
 */
public class PipelineTransformErrorListener implements ErrorListener
{
	/*
	 * Method to catch internal warnings
	 * (non-Javadoc)
	 * @see javax.xml.transform.ErrorListener#warning(javax.xml.transform.TransformerException)
	 */
	public void warning(TransformerException e)
		throws TransformerException
	{
		show("Warning",e);
		throw(e);
	}
	
	/*
	 * Method to catch internal errors
	 * (non-Javadoc)
	 * @see javax.xml.transform.ErrorListener#error(javax.xml.transform.TransformerException)
	 */
	public void error(TransformerException e)
		throws TransformerException
	{
		show("Error",e);
		throw(e);
	}
	
	/*
	 * Method to catch internal fatal errors messages

	 * (non-Javadoc)
	 * @see javax.xml.transform.ErrorListener#fatalError(javax.xml.transform.TransformerException)
	 */
	public void fatalError(TransformerException e)
	throws TransformerException
	{
		show("Fatal Error",e);
		throw(e);
	}

	/*
	 * Convenience method to display the intercepted errors
	 */
	private void show(String type,TransformerException e)
	{
		System.out.println(type + ": " + e.getMessage());
		if(e.getLocationAsString() != null)
			System.out.println(e.getLocationAsString());
	}
}
