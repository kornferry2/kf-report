/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.generator.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/*
 * This method is called ReportContentModelData to differentiate it from the head class
 * called ReportContentModel to be used when a participant wishes to generate a report.
 * 
 * This class is used in the Participant class (under ReportGenerationRequest) only
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"reportTitle", "level"})
public class ReportContentModelData
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String reportTitle;
	private String level;

	//
	// Constructors.
	//

	// 0 parameter constructor
	public ReportContentModelData()
	{
		// default 0 parameter constructor
	}
	
	//
	//
	// Instance methods.
	//
	
	// Getters & setters

	//----------------------------------
	public String getReportTitle()
	{
		return this.reportTitle;
	}

	public void setReportTitle(String value)
	{
		this.reportTitle = value;
	}

	//----------------------------------
	public String getLevel()
	{
		return this.level;
	}

	public void setLevel(String value)
	{
		this.level = value;
	}
}
