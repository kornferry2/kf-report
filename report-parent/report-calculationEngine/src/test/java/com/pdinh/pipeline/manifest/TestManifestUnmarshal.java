/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.manifest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.pdinh.pipeline.manifest.vo.ReportManifest;

public class TestManifestUnmarshal
{
	/*
	 * Test the JAXB annotations (creating XML from POJOs) for the manifest.
	 * 
	 * Generally used to test that we have the manifest.vo class annotations right.
	 * No validation is performed
	 */
	public static void main(String args[])
	{
		System.out.println("Starting unmarshal test...");
		
		File ff = null;
		FileInputStream fis = null;
		JAXBContext jc = null;
		Unmarshaller um = null;
		ReportManifest rm = null;
		
		// Get the parameter (test input file file path)
		if (args.length < 1)
		{
			System.out.println("No test file path passed.  Terminating...");
			return;
		}
		
		String testFilePath = args[0];
		
		// Set up for unmarshalling
		ff = new File(testFilePath);
		System.out.println("Unmarshalling from " + testFilePath);
		try
		{
			System.out.println("   cannonical=" + ff.getCanonicalPath());
			System.out.println("     absolute=" + ff.getAbsolutePath());
		}
		catch (IOException e)
		{
			System.out.println("Error getting file paths...");
			e.printStackTrace();
			return;
		}
		
		//Unmarshal
		try
		{
			fis = new FileInputStream(ff);

			// Create the context & the unmarshaller
			jc = JAXBContext.newInstance(ReportManifest.class);
			um = jc.createUnmarshaller(); 
			System.out.println("Unmarshalling...");
			rm = (ReportManifest)um.unmarshal(fis); 

			System.out.println("Unmarshal complete... output:\n" + rm.toString());
			return;
		}
		catch (FileNotFoundException e)
		{
			System.out.println("Input file not found; terminating...  File name=" + ff);
			return;
		}
		catch (JAXBException e)
		{
			// Setting up context or the unmarshaller
			System.out.println("JAXBException; terminating...  msg=" + e.getMessage());
			return;
		}
		finally
		{
			// Clean up
			if (rm != null)
				rm = null;
			if (um != null)
				um = null;
			if (jc != null)
				jc = null;
			if (fis != null)
			{
				try { fis.close(); }
				catch (Exception e) { /* Swallow this guy */ }
			}
			if (ff != null)
				ff = null;
		}
		
		
	}
}
