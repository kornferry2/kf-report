/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.manifest;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import com.pdinh.pipeline.manifest.vo.CalculationSteps;
import com.pdinh.pipeline.manifest.vo.Param;
import com.pdinh.pipeline.manifest.vo.ReportManifest;
import com.pdinh.pipeline.manifest.vo.Step;

/*
 * Test the JAXB annotations (creating XML from POJOs) for the manifest.
 * 
 * Generally used to test that we have the manifest.vo class annotations right.
 */
public class TestManifestMarshal
{
	public static void main(String args[])
	{
		System.out.println("Start ReportManifest marshalling (POJO to XML) tests...");
		
		// create an object structure
		ReportManifest rm = new ReportManifest();
		CalculationSteps cs = new CalculationSteps();
		rm.setCalculationSteps(cs);
		cs.setType("xml");
		cs.getStep().add(new Step("scale", "scale", "/lei/raw2scale.xsl"));
		cs.getStep().add(new Step("score", "stanine", "/lei/scale2stanine.xsl", new Param("norms", "/norms/norms.xml")));
		cs.getStep().add(new Step("score", "pctl", "/lei/scale2stanine.xsl", new Param("norms", "/norms/norms.xml")));

		// Marshal it
		try
		{
			JAXBContext jc = JAXBContext.newInstance(ReportManifest.class);
			Marshaller marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			//marshaller.marshal(rm, System.out);	// Dumps the output to screen
			StringWriter sw = new StringWriter();
			marshaller.marshal(rm, sw);
			// Dump the output to screen
			System.out.println("Marshalling complete... Output XML:\n" + sw.toString());
		}
		catch (Exception e)
		{
			System.out.println("getReportManifest - testManifestMarshall:  " + e.getMessage());
			e.printStackTrace();
			return;
		}
	}
}
