/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.generator;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.pdinh.pipeline.generator.vo.Participant;
import com.pdinh.pipeline.generator.vo.Project;
import com.pdinh.pipeline.generator.vo.ReportContentModelData;
import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.pipeline.generator.vo.Stage;
import com.pdinh.pipeline.generator.vo.Var;

/*
 * Test the JAXB annotations (creating XML from POJOs) for the report generation request.
 * 
 * Generally used to test that we have the vo class annotations right.
 */
public class TestRGRMarshal {
	public static void main(String args[]) {
		System.out.println("Start ReportGenerationRequest marshalling (POJO to XML) tests...");

		// create the object structure
		ReportGenerationRequest rgr = new ReportGenerationRequest();
		// rgr.getFlag().add(new Flag("COGNITIVES_INCLUDED", "1"));
		// rgr.getFlag().add(new Flag("NORM_GROUP", "bul"));

		Participant pp = new Participant();
		pp.setId(54321);
		pp.setExtId("54321");
		pp.setFirstName("Chris");
		pp.setLastName("Harrington");
		pp.setAssessmentDate("2011-03-06");
		pp.setCompany("Shell");
		pp.setTitle("Assistant Plant Manager");
		Project proj = new Project();
		pp.setProject(proj);
		proj.setId(1234);
		proj.setExtId("567890");
		// proj.setName("Project Test Name");
		ReportContentModelData rcmd = new ReportContentModelData();
		rcmd.setReportTitle("Leadership Assessment Report");
		rcmd.setLevel("Leader of Teams");
		pp.setReportContentModel(rcmd);

		// rgr.getParticipants().getParticipant().add(pp);
		rgr.getParticipant().add(pp);

		Stage ss = new Stage("raw");
		pp.getStage().add(ss);
		ss.getVar().add(new Var("Q1", "1"));
		ss.getVar().add(new Var("Q2", "5"));
		ss.getVar().add(new Var("Q3", "1"));
		//
		// ss = new Stage("scaled");
		// pp.getStage().add(ss);
		// ss.getVar().add(new Var("LEI3_SGY", "92.60000000000001"));
		// ss.getVar().add(new Var("LEI3_PGT", "115.19999999999999"));
		// ss.getVar().add(new Var("LEI3_BDV", "80.5"));
		//
		// ss = new Stage("stanine");
		// pp.getStage().add(ss);
		// ss.getVar().add(new Var("OPERATIONS", "5"));
		// ss.getVar().add(new Var("LEI_SIT_TOUGH_CHAL", "4"));
		// ss.getVar().add(new Var("LEI_HIGH_VIS", "6"));
		// ss.getVar().add(new Var("LEI_BD_GROW_BUS", "8"));
		// ss.getVar().add(new Var("LEI_PERSONAL", "5"));
		//
		// ss.getNorm().add(new Stage.Norm("OPERATIONS", "191.57", "35.38"));
		// ss.getNorm().add(new Stage.Norm("LEI_SIT_TOUGH_CHAL", "138.86",
		// "24.06"));

		// Marshall it
		System.out.println("Set up the JAXBContext...");
		JAXBContext jc = null;
		Marshaller marshaller = null;
		StringWriter sw = null;
		try {
			jc = JAXBContext.newInstance(ReportGenerationRequest.class);
			marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			sw = new StringWriter();
			marshaller.marshal(rgr, sw);
			// Dump the output to screen
			System.out.println("Marshalling complete... Output XML:\n" + sw.toString());
		} catch (JAXBException e) {
			System.out.println("JAXBException detected... TerminatinggetReportManifest - testManifestMarshall:  "
					+ e.getMessage());
			e.printStackTrace();
			return;
		} finally {
			if (sw != null) {
				try {
					sw.close();
				} catch (Exception e) {
					System.out.println("TestRGRMarshal.main() - Error closing FileInputStream is ignored.  Msg="
							+ e.getMessage());
				}
				sw = null;
			}
			marshaller = null;
			jc = null;
			rgr = null;
		}
	}
}
