/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.generator;

import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.pdinh.pipeline.generator.helpers.PipelineTransformHelper;
import com.pdinh.pipeline.generator.helpers.ReportManifestHelper;
import com.pdinh.pipeline.manifest.vo.ReportManifest;
import com.pdinh.pipeline.generator.vo.Flag;
import com.pdinh.pipeline.generator.vo.Instrument;
import com.pdinh.pipeline.generator.vo.Participant;
import com.pdinh.pipeline.generator.vo.ReportContentModelData;
import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.pipeline.generator.vo.Stage;
import com.pdinh.pipeline.generator.vo.Var;

/*
 * Test the pipeline generator using data from the database
 * 
 * NOTE:  At this time (3/9/12) this won't work so hot with LEI as the scoring
 *        algorithm does not account for the multipart answers in Q34 - Q80.
 */
public class TestGeneratorFromDB {
	//
	// Static data.
	//
	private static int partId = 0;
	private static String instCode = null;

	private static ResourceBundle rb = null;
	private static String cerPath;

	// Debug flag
	private static boolean debugging = false;

	//
	// Static methods.
	//

	/*
	 * Runs as a headless app
	 * 
	 * NOTE:  This assumes that the data is all there.  If there are responses missing
	 *        from the data there will be an error thrown.
	 *        
	 * NOTE:  This has not been much tested as the format of the data is not firm yet.
	 *        In addition, the transforms that are there are not complete ((e.g., the LEI raw2scale)
	 */
	public static void main(String args[]) {
		// Get the parameters
		HashMap<String, String> parms = TestUtils.getParms("DB", args);
		if (parms == null)
			return;
		partId = Integer.parseInt(parms.get(TestUtils.PART_ID));
		instCode = parms.get(TestUtils.INST_CODE);

		// rb used only for testing parameters (driver userid, password, etc.
		rb = ResourceBundle.getBundle("com.pdinh.pipeline.generator.application", Locale.getDefault());
		if (rb == null) {
			System.out.println("ResourceBundle not available... Terminating");
			return;
		}

		// // Debugging
		// System.out.println("ResourceBundle content:");
		// for (Enumeration<String> e = rb.getKeys(); e.hasMoreElements();)
		// {
		// String key = e.nextElement();
		// System.out.println("  " + key + "=" + rb.getString(key));
		// }

		debugging = true; // Always true for debug
		cerPath = "C:/Users/kbeukelm/Workspace/Indigo/CalculationEngineResources"; // Hard
																					// coded
																					// for
																					// testing

		// Possibly make this a step in the manifest? it would not be a
		// transformative step, however
		String respXml = getResponseData();
		if (respXml == null)
			return;

		// This is the pipeline process

		// Get the manifest
		ReportManifestHelper rmh = new ReportManifestHelper(debugging, cerPath);
		ReportManifest manifest = rmh.getReportManifest(instCode);
		if (manifest == null) {
			System.out.println("Unable to fetch report manifest... Terminating.");
			return;
		}

		// Process the XML per the manifest
		PipelineTransformHelper pth = new PipelineTransformHelper(debugging, cerPath);
		String output = pth.process(manifest, respXml);

		// Output goes into the next stage of the pipeline.

		if (debugging) {
			System.out.println("Calc output XML:\n" + output);
		}
	}

	/*
	 * Method to fetch the data to be transformed from the database
	 */
	private static String getResponseData() {
		Connection nhnCon = null;

		try {
			// Open the DB
			nhnCon = openNhnDb(rb);
			if (nhnCon == null) {
				return null;
			}
			// System.out.println("Opened " + rb.getString("nhnUrl"));

			// Read participant data here... figure out way to pass it in
			// (probably the POJO)
			// // fake it for now
			// Participant ppt = new Participant(partId, "Bogus Participant");

			// read response data - Probably could have populated the XML
			// directly, but split out the functionality to keep it segmented
			Map<String, String> respMap = getResponses(nhnCon, partId, instCode);
			if (respMap == null)
				return null;
			// System.out.println("Got resps");

			// create the XML using JAXB
			String responseXML = makeRespXml(partId, instCode, respMap);

			if (debugging) {
				// Dump the XML
				System.out.println("DEBUG - Response XML:\n" + responseXML);
			}

			return responseXML;
		} finally {
			if (nhnCon != null) {
				try {
					nhnCon.close();
				} catch (Exception e) {
					System.out
							.println("TestGeneratorFromDB.getResponseData() - Error closing FileInputStream is ignored.  Msg="
									+ e.getMessage());
				}
			}
		}
	}

	/*
	 * Open the db connection
	 */
	private static Connection openNhnDb(ResourceBundle rb) {
		Connection con = null;

		String driver = rb.getString("nhnDriver");
		String url = rb.getString("nhnUrl");
		String uid = rb.getString("nhnUsername");
		String pw = rb.getString("nhnPassword");
		// System.out.println("NHN DB parms: url=" + url + ", driver=" + driver
		// + ", uid=" + uid);

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url, uid, pw);
		} catch (ClassNotFoundException e) {
			System.out.println("Unable to find driver class.");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("Unable to open database connection.");
			e.printStackTrace();
		} finally {

		}

		return con;
	}

	/*
	 * getResponses - get a map (key = response ID, value is response as a string)
	 * 
	 * @param con - A Connection object
	 * @param partId - The participant ID
	 * @param instCode - The instrument to pick up
	 * @returns Response XML in a String
	 */
	private static Map<String, String> getResponses(Connection con, int partId, String instCode) {
		Statement stmt = null;
		ResultSet rs = null;
		HashMap<String, String> ret = new HashMap<String, String>();

		// debugging... if we use this code, we need to deal with this
		String ic = instCode.equals("gpi") ? "gpil" : instCode;

		StringBuffer q = new StringBuffer();
		q.append("SELECT i.interactionId AS respId, ");
		q.append("       i.learnerResponse AS response ");
		q.append("FROM scorm.dbo.interaction_view i WITH(NOLOCK) ");
		q.append("  JOIN platform.dbo.users u WITH(NOLOCK) ON cast(u.users_id AS varchar(64)) = i.learnerId ");
		q.append("  JOIN platform.dbo.scorm_course_directories scd WITH(NOLOCK) ON scd.scorm_root_id = i.activityId ");
		q.append("  JOIN platform.dbo.course c WITH(NOLOCK) ON c.course = scd.course_id ");
		q.append("WHERE c.abbv = '" + ic + "' ");
		q.append("  AND u.users_id = " + partId);

		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(q.toString());

			if (!rs.isBeforeFirst()) {
				System.out.println("No response data available in database.");
				return null;
			}

			// save the responses
			// int i = 1;
			while (rs.next()) {
				ret.put(rs.getString("respId"), rs.getString("response"));
				// // Throttling code -----
				// if (i >= 10)
				// break;
				// i++;
				// // ---------------------
			}
			// System.out.println("THROTTLED!!! Comment out throttling code when no longer needed...");

			// System.out.println("Fetched " + ret.size() + " responses...");

			// Do we want to do a sort here?
			// Yes, eventually we do

			return ret;
		} catch (SQLException e) {
			System.out.println("SQLException fetching response data.");
			e.printStackTrace();
			return null;
		} finally {
			if (!(rs == null)) {
				try {
					rs.close();
				} catch (SQLException e) { /* Swallow it */
				}
			}
			if (!(stmt == null)) {
				try {
					stmt.close();
				} catch (SQLException e) { /* Swallow it */
				}
			}
		}
	}

	/*
	 * makeRespXml - Fill the Java content tree with responses and marshal the response XML
	 * 
	 * @param con - A Participant object object  - for now though it's just tte ID
	 * @param inst - The instrument code
	 * @param respMap - A HashMap of responses
	 * @returns XML in a string
	 */
	private static String makeRespXml(int partId, String instCode, Map<String, String> respMap) {
		ReportGenerationRequest rgr = new ReportGenerationRequest();
		rgr.getFlag().add(new Flag("COGNITIVES_INCLUDED", "1"));
		rgr.getFlag().add(new Flag("NORM_GROUP", "bul"));

		// set up the participants (here only one so no loop, but top if there
		// were more)
		Participant ppt = new Participant();
		ppt.setId(partId);
		ppt.setExtId("" + partId);
		ppt.setFirstName("Bogus");
		ppt.setLastName("Test-Boy");
		ppt.setAssessmentDate("1971-01-01");
		ppt.setCompany("Testing Co.");
		ppt.setTitle("Testing Title");
		ReportContentModelData rcmd = new ReportContentModelData();
		ppt.setReportContentModel(rcmd);
		rcmd.setReportTitle("Test Report");
		rcmd.setLevel("Test Level");

		// .. and put it in the collection
		rgr.getParticipant().add(ppt);

		// Add a stage to the participant
		Stage st = new Stage();
		st.setId("raw");
		ppt.getStage().add(st);

		// Add an instrument to the stage (top of a loop if multiples)
		Instrument inst = new Instrument();
		inst.setId(instCode.toUpperCase());
		st.getInstrument().add(inst);

		// Add the responses
		for (Iterator<Map.Entry<String, String>> itr = respMap.entrySet().iterator(); itr.hasNext();) {
			Map.Entry<String, String> ent = itr.next();
			Var r = new Var();
			r.setId(ent.getKey());
			r.setValue(ent.getValue());

			inst.getVar().add(r);
		}

		// bottom of the instrument list (if there was more than one)

		// Bottom of the ppt loop (if there was one)

		// Marshal the data
		// System.out.println("Marshal the request XML...");
		try {
			// Create the context
			JAXBContext jc = JAXBContext.newInstance(ReportGenerationRequest.class);

			// Marshal to String
			StringWriter sw = new StringWriter();
			Marshaller mm = jc.createMarshaller();
			mm.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); // set
																			// only
																			// if
																			// output?
			mm.marshal(rgr, sw);

			return sw.toString();
		} catch (JAXBException e) {
			System.out.println("Error Marshalling the report Request.");
			e.printStackTrace();
			return null;
		}
	}

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//

}
