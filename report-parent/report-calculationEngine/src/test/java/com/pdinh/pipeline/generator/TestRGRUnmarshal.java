/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.generator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;


public class TestRGRUnmarshal
{
	/*
	 * Test the JAXB annotations (creating XML from POJOs) for the manifest.
	 * 
	 * Generally used to test that we have the manifest.vo class annotations right.
	 * No validation is performed
	 */
	public static void main(String args[])
	{
		System.out.println("Starting RGR unmarshal test...");

		File ff = null;
		FileInputStream fis = null;
		JAXBContext jc = null;
		Unmarshaller um = null;
		ReportGenerationRequest rgr = null;

		// Get the parameter (test input file file path)
		if (args.length < 1)
		{
			System.out.println("No test file path passed.  Terminating...");
			return;
		}
		
		String testFilePath = args[0];
		
		// Set up for unmarshalling
		ff = new File(testFilePath);
		System.out.println("Unmarshalling from " + testFilePath);
		try
		{
			System.out.println("  Canonical - " + ff.getCanonicalPath());
			System.out.println("   Absolute - " + ff.getAbsolutePath());
		}
		catch (IOException e)
		{
			System.out.println("Unable to fetch file path...");
			e.printStackTrace();
			return;
		}
		
		//Unmarshal
		try
		{
			fis = new FileInputStream(ff);

			// Create the context & the unmarshaller
			jc = JAXBContext.newInstance(ReportGenerationRequest.class);
			um = jc.createUnmarshaller(); 
			System.out.println("Unmarshalling...");
			rgr = (ReportGenerationRequest)um.unmarshal(fis); 

			System.out.println("Unmarshal complete... output:\n" + rgr.toString());
			return;
		}
		catch (FileNotFoundException e)
		{
			System.out.println("Input file not found; terminating...  File name=" + ff);
			return;
		}
		catch (JAXBException e)
		{
			// Setting up context or the unmarshaller
			System.out.println("JAXBException; terminating...  msg=" + e.getMessage());
			e.printStackTrace();
			return;
		}
		finally
		{
			// Clean up
			rgr = null;
			um = null;
			jc = null;
			if (fis != null)
			{
				try { fis.close(); }
				catch (Exception e) { /* Swallow this guy */ }
			}
			ff = null;
		}
	}
}
