/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.generator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.pdinh.pipeline.generator.helpers.PipelineTransformHelper;
import com.pdinh.pipeline.generator.helpers.ReportManifestHelper;
import com.pdinh.pipeline.generator.vo.Participant;
import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.pipeline.generator.vo.Stage;
import com.pdinh.pipeline.manifest.vo.ReportManifest;

public class TestGeneratorFromXML {
	//
	// Static data.
	//
	// private static String pptId = null;
	private static String instCode = null;

	// Debug flag
	static private String inpXmlPath;
	static private boolean debugging = false;

	static private String cerPath;

	//
	// Static methods.
	//

	/*
	 * Runs as a headless app
	 */
	public static void main(String args[]) {
		// Get the parameters
		HashMap<String, String> parms = TestUtils.getParms("XML", args);
		if (parms == null)
			return;
		inpXmlPath = parms.get(TestUtils.INP_XML);

		debugging = true; // Always true for testing
		cerPath = "C:/PALMS/CalculationEngineResources/"; // Hard coded for
															// testing

		if (debugging)
			System.out.println("Input XML in " + inpXmlPath);

		// Possibly make this a step in the manifest? it would not be a
		// transformative step, however
		String respXml = fetchXmlFile(inpXmlPath);
		if (respXml == null)
			return;
		if (instCode == null)
			return;

		// // for testing lva, override the instCode
		// instCode = "lva";

		// This is the pipeline process

		// Get the manifest
		ReportManifestHelper rmh = new ReportManifestHelper(debugging, cerPath);
		// ReportManifest manifest = rmh.getReportManifest(instCode); // Use
		// dflt name
		// ReportManifest manifest = rmh.getReportManifest(instCode, "kdb.xml");
		// // Erroneous name
		// ReportManifest manifest = rmh.getReportManifest(instCode,
		// "testMan.xml"); // a correct name
		// ReportManifest manifest = rmh.getReportManifest(instCode,
		// "iblvaManifest.xml"); // iblva data only
		// ReportManifest manifest = rmh.getReportManifest(instCode,
		// "gpiManifest.xml");
		// ReportManifest manifest = rmh.getReportManifest(instCode,
		// "leiManifest.xml");
		// ReportManifest manifest = rmh.getReportManifest(instCode,
		// "rvManifest.xml");
		// ReportManifest manifest = rmh.getReportManifest(instCode,
		// "wgManifest.xml");
		// ReportManifest manifest = rmh.getReportManifest(instCode,
		// "finexRawManifest.xml");
		// ReportManifest manifest = rmh.getReportManifest(instCode,
		// "finexRatingManifest.xml");
		ReportManifest manifest = rmh.getReportManifest(instCode, "csManifest.xml");

		// ReportManifest manifest = rmh.getReportManifest(instCode,
		// "lvaManifest.xml"); // all lva data
		if (manifest == null) {
			System.out.println("Unable to fetch report manifest... Terminating.");
			return;
		}

		// Process the XML per the manifest
		PipelineTransformHelper pth = new PipelineTransformHelper(debugging, cerPath);
		String output = pth.process(manifest, respXml);

		// if (debugging)
		// {
		System.out.println("Calc output XML:\n" + output);
		// }
	}

	/*
	 * fetchXmlFile - Get a responses XML file from the file system
	 * DEBUG ROUTINE ONLY.
	 */
	private static String fetchXmlFile(String path) {
		String xml = "";
		File file = new File(path);
		FileReader fr = null;
		BufferedReader br = null;

		try {
			// Get the output string
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			String s;
			while ((s = br.readLine()) != null) {
				xml += s;
			}

			// Oh, get the instrument as well
			instCode = getInstCode(file);
			if (instCode == null) {
				System.out.println("Unable to fetch instrument code from " + path);
				return null;
			}

			return xml;
		} catch (FileNotFoundException e) {
			System.out.println("File not found:  " + path);
			return null;
		} catch (IOException e) {
			System.out.println("Error reading " + path);
			return null;
		} finally {
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException e) { /* Swallow the exception */
			}
			try {
				if (fr != null) {
					fr.close();
				}
			} catch (IOException e) { /* Swallow the exception */
			}
		}
	}

	/*
	 * Get the first inst code
	 * In real life, this will probably get an array of strings, one for each instrument in the XML
	 * 
	 * XML as a string in, 1st instrument found out
	 */
	private static String getInstCode(File xml) {
		String ret = null;

		// Unmarshall it so we can get the instrument list... need it to get the
		// manifest
		// seems kinda dumb, but...
		ReportGenerationRequest rgr = null;
		FileInputStream fis = null;
		JAXBContext jc = null;
		Unmarshaller um = null;

		// Unmarshal
		try {
			fis = new FileInputStream(xml);

			// Create the context & the unmarshaller
			jc = JAXBContext.newInstance(ReportGenerationRequest.class);
			um = jc.createUnmarshaller();
			rgr = (ReportGenerationRequest) um.unmarshal(fis);

			// Now find the first instrument in the first participant (for
			// testing only)
			Participant ppt = rgr.getParticipant().get(0);
			if (ppt == null) {
				System.out.println("No participant found in XML:  file=" + xml);
				return null;
			}
			// pptId = ppt.getId();
			List<Stage> stages = ppt.getStage();
			// Look for the "raw" stage
			for (Stage st : stages) {
				if (st.getId().equals("raw") || st.getId().equals("responses")) {
					ret = st.getInstrument().get(0).getId();
					break;
				}
			}
			if (ret == null)
				System.out.println("No instrument id found via testing code");

			// return ret.toLowerCase();
			return ret;
		} catch (FileNotFoundException e) {
			System.out.println("Input file not found; terminating...  File name=" + xml);
			return null;
		} catch (JAXBException e) {
			// Setting up context or the unmarshaller
			System.out.println("JAXBException; terminating...  msg=" + e.getMessage());
			return null;
		} finally {
			// Clean up
			rgr = null;
			um = null;
			jc = null;
			if (fis != null) {
				try {
					fis.close();
				} catch (Exception e) { /* Swallow this guy */
				}
			}
		}
	}

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//

}
