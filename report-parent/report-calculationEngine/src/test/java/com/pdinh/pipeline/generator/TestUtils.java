/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.pipeline.generator;

import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;

public class TestUtils
{
	//
	// Static data.
	//
    static public String PART_ID = "ParticipantId";
    static public String INST_CODE = "InstrumentCode";
    static public String INP_XML = "InputXML";
	//
	// Static methods.
	//

		/*
		 * Method to fetch parameters.
		 * Args:
		 * 	 0 - participant ID
		 *   1 - instrument type
		 *   2 - (Optional) Input data file pathname (reads input response data from here instead of from database)
		 */
		static public HashMap<String, String> getParms(String type, String args[])
		{
			// Notes for the the db testing
			// + means in Oxcart, - means in Reflex only
			// +367889 has chq, demo, finex, gpil
			// +367581 has chq, cs, demo, finex, gpil, lei, rv, wg
			// -367543 has chq, cs, demo, finex, gpil, lei, rv, wg
			// -367258 has chq, cs, demo, finex, gpil
			// -367257 has chq, demo, gpil, wg

			HashMap<String, String> ret = new HashMap<String, String>();
			
			boolean validType = false;
			if (type.equals("DB") || type.equals("XML"))
			{
				validType = true;
			}
			if (! validType)
			{
				System.out.println("Type parameter must be either 'DB' or 'XML'");
				return null;
			}
			
			if (type.equals("DB"))
			{
				if (args.length == 0)
				{
					System.out.println(" No parameters were detected.");
					System.out.println(dbParmDescString());
					return null;
				}
				if (args.length > 2)
				{
					System.out.println("Incorrect number of parameters (" + args.length + ")");
					System.out.println(dbParmDescString());
					return null;
				}

				ret.put(PART_ID, args[0]);
				ret.put(INST_CODE, args[1]);
			}
			else
			{
				// Type must be XML...
				if (args.length == 0)
				{
					System.out.println(" No parameters were detected.");
					System.out.println(xmlParmDescString());
					return null;
				}
				if (args.length > 1)
				{
					System.out.println("Incorrect number of parameters (" + args.length + ")");
					System.out.println(xmlParmDescString());
					return null;
				}
				
				ret.put(INP_XML, args[0]);
			}
			
			//// Debugging
			//System.out.println("Input Parameters:  Type=" + type);
			//for(Iterator<Map.Entry<String, String>> itr=ret.entrySet().iterator(); itr.hasNext();  )
			//{
			//	Map.Entry<String, String> ent = itr.next();
			//	System.out.println("  " + ent.getKey() + " = " + ent.getValue());
			//}
			
			return ret;
		}


		/*
		 * Method to display appropriate parameters for DB type request.
		 * 
		 * Note that there is no closing new-line character; assumes that this method is called
		 * in a println() or that the user will handle the closing new-line himself.
		 */
		private static String dbParmDescString()
		{
			String str = "  This program requires arguments as follows:\n";
			str += "    Participant ID  - The numeric participant (users) ID for whom data is to be fetched.\n";
			str += "    Instrument Code - The code for the instrument to be fetched.\n";
			
			return str;
		}


		/*
		 * Method to display appropriate parameters fo XML type request.
		 * 
		 * Note that there is no closing new-line character; assumes that this method is called
		 * in a println() or that the user will handle the closing new-line himself.
		 */
		private static String xmlParmDescString()
		{
			String str = "  This program requires a single argument as follows:\n";
			str += "    XML Path - The path of the input XML file.\n";
			
			return str;
		}


		//
		// Instance data.
		//

		//
		// Constructors.
		//

		//
		// Instance methods.
		//
		//
		//

}
