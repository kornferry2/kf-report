<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>Test ReportServer servlets</title>
  </head>
  <body>
    <h1>Test GenerateReport</h1>

    <h2>Shell</h2>
    <!-- 
    <p><a href="/reportweb/GenerateReport?pid=1&type=SHELL_LAR&phase=10">pid 1 as PDF</a></p>
    <p><a href="/reportweb/GenerateReportHtml?pid=1&type=SHELL_LAR&phase=10">pid 1 as HTML</a></p>
     -->
     <p>No supported data currently available in database (Supported types are SHELL_LAR_DEVELOPMENT and SHELL_LAR_READINESS)<p>
    
    <h2>LVA (from cmd)</h2>
    <p><a href="/reportweb/test/LvaMllDar.xhtml">LVAMLL DAR generated into test via lvadar.cmd</a></p>

    <h2>LVA (from db)</h2>
    <p><a href="/reportweb/GenerateReport?pid=367025&type=LVA_MLL_DEVELOPMENT&subtype=development&phase=10">pid 367025 LVAMLL DAR Customizing PDF</a></p>
    <p><a href="/reportweb/GenerateReport?pid=367025&type=LVA_MLL_DEVELOPMENT&subtype=development&phase=20">pid 367025 LVAMLL DAR Editing PDF</a></p>
    <p><a href="/reportweb/GenerateReport?pid=367025&type=LVA_MLL_DEVELOPMENT&subtype=development&phase=30">pid 367025 LVAMLL DAR Reporting PDF</a></p>

    <p><a href="/reportweb/GenerateReportHtml?pid=367025&type=LVA_MLL_DEVELOPMENT&subtype=development&phase=30">pid 367025 LVAMLL DAR Reporting HTML</a></p>

    <p><a href="/reportweb/GenerateReport?pid=368256&type=LVA_MLL_DEVELOPMENT&subtype=development&phase=10">pid 368256 LVAMLL DAR Customizing PDF</a></p>
    <p><a href="/reportweb/GenerateReport?pid=368256&type=LVA_MLL_DEVELOPMENT&subtype=development&phase=20">pid 368256 LVAMLL DAR Editing PDF</a></p>
    <p><a href="/reportweb/GenerateReport?pid=368256&type=LVA_MLL_DEVELOPMENT&subtype=development&phase=30">pid 368256 LVAMLL DAR Reporting PDF</a></p>

    <p><a href="/reportweb/GenerateReportHtml?pid=368256&type=LVA_MLL_DEVELOPMENT&subtype=development&phase=10">pid 368256 LVAMLL DAR Reporting HTML P10</a></p>
    <p><a href="/reportweb/GenerateReportHtml?pid=368256&type=LVA_MLL_DEVELOPMENT&subtype=development&phase=20">pid 368256 LVAMLL DAR Reporting HTML P20</a></p>

    <p><a href="/reportweb/GenerateReportHtml?pid=367018&type=LVA_MLL_DEVELOPMENT&subtype=development&phase=10">pid 367018 LVAMLL DAR Reporting HTML P10</a></p>
    <p><a href="/reportweb/GenerateReport?pid=367018&type=LVA_MLL_DEVELOPMENT&subtype=development&phase=10">pid 367018 LVAMLL DAR Reporting PDF P10</a></p>

    <h2>LVA coversheet</h2>
    <p><a href="/reportweb/core/xhtml/cover.xhtml?productname=GreatLeaderGPS&amp;reportname=Development Action Report&reportsubhead=Mid-level Leader&participant=Jonathan Douglas&client=Global Corporation&project=Mid-level Leader Virtual Assessment&date=04 Nov 2012">test 1 as HTML</a></p>

    <h2>LVA (from file)</h2>
<!-- 
    <p><a href="/reportweb/GenerateReport?pid=1&type=LVA_MLL_DEVELOPMENT&subtype=development&mock=LvaMll_new.xml">LVAMLL DAR as PDF</a></p>
    <p><a href="/reportweb/GenerateReportHtml?pid=1&type=LVA_MLL_DEVELOPMENT&subtype=development&mock=LvaMll_new.xml">LVAMLL DAR as HTML</a></p>
 -->
     <p>No supported data currently available<p>

  </body>

</html> 
