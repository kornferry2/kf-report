// JavaScript Document

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// VARS
//	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
var chart;
var chartWidth;
var chartX;
var pins;

// pins
var labels;
// #pin1 label x, width
var label1Width;
var label1X;
// #pin2 label x, width
var label2Width;
var label2X;
// #pin3 label x, width
var label3Width;
var label3X;

// guids
var guides;
// #pin1 guide x
var guide1X;
// #pin2 guide x
var guide2X;
// #pin3 guide x
var guide3X;

// icons
var icons;
// pin1 icon
var icon1X;
// pin1 icon
var icon2X;
// pin1 icon
var icon3X;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// FUNCTIONS
//	
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function initChart() {
	try {
	//document.getElementById("test902").innerHTML = document.getElementById("test902").innerHTML + "INIT() ";
	configVars();
	//document.getElementById("test902").innerHTML = document.getElementById("test902").innerHTML + "configVars1() ";
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// CENTER labels on guides
	//	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// assumes .guide is offset right:18px and .guide:width is 2 px thick
	labels[0].style.left = label1Width / 2 - 19 + "px";
	labels[1].style.left = label2Width / 2 - 19 + "px";
	labels[2].style.left = label3Width / 2 - 19 + "px";
	
	configVars();
	//document.getElementById("test902").innerHTML = document.getElementById("test902").innerHTML + "configVars2() ";
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// REPOSITION IF 2 or MORE PINS ARE ON SAME STEP
	//	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	if (icon1X == icon2X || icon2X == icon3X || icon1X == icon3X) {
		//document.getElementById("test902").innerHTML = document.getElementById("test902").innerHTML + "-2 or more the same |  ";
		// 2 are on same step!
		if (icon1X != icon2X || icon2X != icon3X || icon1X != icon3X) {
			//document.getElementById("test902").innerHTML = document.getElementById("test902").innerHTML + "-2 the same |  ";
			if (icon1X == icon2X || icon1X == icon3X) {
				//document.getElementById("test902").innerHTML = document.getElementById("test902").innerHTML + "IF icon1X == icon2X || icon1X == icon3X |  ";
				var oldTop = window.getComputedStyle(icons[0])
						.getPropertyValue('top');
				icons[0].style.top = parseFloat(oldTop) + 15 + "px";
				pins[0].style.left = -35 + "px";
				var guideOldTop = window.getComputedStyle(guides[0])
						.getPropertyValue('height');
				guides[0].style.height = parseFloat(guideOldTop) + 10 + "px";
				if (icon1X == icon2X) {
					pins[1].style.top = -84 + "px";
				} else {
					pins[3].style.top = -84 + "px";
				}
			} else {
				//document.getElementById("test902").innerHTML = document.getElementById("test902").innerHTML + "ELSE |  ";
				var oldTop = window.getComputedStyle(icons[1])
						.getPropertyValue('top');
				icons[1].style.top = parseFloat(oldTop) + 15 + "px";
				pins[1].style.left = -35 + "px";
				var guideOldTop = window.getComputedStyle(guides[1])
						.getPropertyValue('height');
				guides[1].style.height = parseFloat(guideOldTop) + 10 + "px";
				pins[2].style.top = -84 + "px";
			}
		} else {
			//document.getElementById("test902").innerHTML = document.getElementById("test902").innerHTML + "ALL 3 PINS SAME POSITION |  ";
			pins[0].style.left = -46 + "px";
			// ELSE 3 are on SAME STEP! AHHH!$!$!$
			pins[1].style.left = -18 + "px";
			pins[2].style.left = 10 + "px";
			// assuming .pin height:84px, they are stacked vertically 0,1,2; var
			oldTop = window.getComputedStyle(icons[0]).getPropertyValue('top');
			icons[0].style.top = parseFloat(oldTop) + 15 + "px";
			pins[1].style.top = -84 + "px";
			pins[2].style.top = -168 + "px";
			var guideOldTop = window.getComputedStyle(guides[0]).getPropertyValue('height');
			guides[0].style.height = parseFloat(guideOldTop) + 10 + "px";
		}
	}
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// LEFT SIDE BOUNDARY CHECKS
	//	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// if label one does not fit on chart, add the amount it is
	if (label1X < chartX) {
		//document.getElementById("test902").innerHTML = document.getElementById("test902").innerHTML + "Inside Fix Left |  ";
		var elementStyle = window.getComputedStyle(labels[0]);
		var oldLeft = parseFloat(elementStyle.getPropertyValue('left'));
		// add an offset if label and label 2 are on the left
		if (guide1X == guide2X) {
			// offset is different if target is CEO
			if (guide3X > 600) {
				labels[0].style.left = 139 + "px";
							}
			else {
				labels[0].style.left = 93 + "px";
			}
		}
		else {
			labels[0].style.left = oldLeft + Math.abs(label1X) + "px";
		}
	
		
		// if label 2 is over left of guide 1
		if (label2X < guide1X) {
			//document.getElementById("test902").innerHTML = document.getElementById("test902").innerHTML + "Inside Fix Left IF 1 |  ";
			var elementStyle = window.getComputedStyle(labels[1]);
			var oldLeft = parseFloat(elementStyle.getPropertyValue('left'));
			// if label 2 is left of chart boundary, add how much outside
			// boundary to equasion
			var newLeft = (label2X < 0) ? oldLeft + Math.abs(label2X) : oldLeft;
			// additional offset relative to guide1 label1
			labels[1].style.left = newLeft + guide2X - guide1X + "px";
			// add an offset if label and label 2 are on the left
			if (guide1X == guide2X) {
				labels[1].style.left = newLeft + guide2X - guide1X + 20 + "px";
			}
			else {
				labels[1].style.left = newLeft + guide2X - guide1X + "px";
			}
			
		}
		// if label 3 over left of guide 2
		if (label3X < guide2X) {
			//document.getElementById("test902").innerHTML = document.getElementById("test902").innerHTML + "Inside Fix Left IF 2 |  ";
			var elementStyle = window.getComputedStyle(labels[2]);
			var oldLeft = parseFloat(elementStyle.getPropertyValue('left'));
			// //if label 2 is left of chart boundary, add how much outside
			// boundary to equasion
			var newLeft = (label3X < 0) ? oldLeft + Math.abs(label3X) : oldLeft;
			// additional offset relative to guide1 label1 (makes look more
			// consistant)
			labels[1].style.left = newLeft + guide3X - guide2X + "px";
		}
	}

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// RIGHT SIDE BOUNDARY CHECKS
	//	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// if label 3 is to the right of right boundary
	if ((label3X + label3Width) > chartWidth) {
		//document.getElementById("test902").innerHTML = document.getElementById("test902").innerHTML + "Inside Fix Right |  ";
		var elementStyle = window.getComputedStyle(labels[2]);
		var oldLeft = parseFloat(elementStyle.getPropertyValue('left'));
		labels[2].style.left = oldLeft - (label3X + label3Width - chartWidth)
				+ "px";

		// if label 2 is to right of label 3 icon (left side)
		if (label2X + label2Width > icon3X) {
			//document.getElementById("test902").innerHTML = document.getElementById("test902").innerHTML + "Inside Fix Right IF 1 |  ";
			var elementStyle = window.getComputedStyle(labels[1]);
			var oldLeft = parseFloat(elementStyle.getPropertyValue('left'));
			// if label 2 is right of chart boundary, add how much outside
			// boundary to equasion
			var newLeft;
			if (label2X + label2Width > icon3X) {
				newLeft = oldLeft - ((label2X + label2Width) - icon3X - 23);
			} else {
				newLeft = oldLeft;
			}
			// additional offset relative to guide1 label1
			labels[1].style.left = newLeft + "px";
		}
		// if label 1 is to right of label 3 icon (left side)
		if (label1X + label1Width > icon2X) {
			//document.getElementById("test902").innerHTML = document.getElementById("test902").innerHTML + "Inside Fix Right IF 2 |  ";
			var elementStyle = window.getComputedStyle(labels[0]);
			var oldLeft = parseFloat(elementStyle.getPropertyValue('left'));
			// if label 2 is right of chart boundary, add how much outside
			// boundary to equasion
			var newLeft;
			if (label1X + label1Width > icon2X) {
				newLeft = oldLeft - ((label1X + label1Width) - icon2X - 23);
			} else {
				newLeft = oldLeft;
			}
			// additional offset relative to guide1 label1
			labels[0].style.left = newLeft + "px";
		}
	}
	}
	catch(err)
	{
		document.getElementById("test902").innerHTML = "There was a javascript error while generating this report, please contact Korn Ferry Support for assistance. "+ err + "<br/>" + document.getElementById("test902").innerHTML;
	}
}

function configVars() {
	chart = document.getElementById("step-chart");
	chartWidth = chart.offsetWidth;
	chartX = chart.getBoundingClientRect().left;

	pins = document.getElementsByClassName("pin");
	labels = document.getElementsByClassName("label");
	// #pin1 label x, width
	label1Width = labels[0].offsetWidth;
	label1X = labels[0].getBoundingClientRect().left;
	// #pin2 label x, width
	label2Width = labels[1].offsetWidth;
	label2X = labels[1].getBoundingClientRect().left;
	// #pin3 label x, width
	label3Width = labels[2].offsetWidth;
	label3X = labels[2].getBoundingClientRect().left;

	// guids
	guides = document.getElementsByClassName("guide");
	// #pin1 guide x
	guide1X = guides[0].getBoundingClientRect().left;
	// #pin2 guide x
	guide2X = guides[1].getBoundingClientRect().left;
	// #pin3 guide x
	guide3X = guides[2].getBoundingClientRect().left;

	// icons
	icons = document.getElementsByClassName("step-icon");
	// pin1 icon
	icon1X = icons[0].getBoundingClientRect().left;
	// pin1 icon
	icon2X = icons[1].getBoundingClientRect().left;
	// pin1 icon
	icon3X = icons[2].getBoundingClientRect().left;
	//document.getElementById("test902").innerHTML = document.getElementById("test902").innerHTML + "" +
			//"<br/>chartWidth" +chartWidth+
		//"<br/>chartX" +chartX+
		//"<br/>label1Width" +label1Width+
		//"<br/>label1X" +label1X+
		//"<br/>label2Width" +label2Width+
			//"<br/>label2X" +label2X+
		//"<br/>label3Width" +label3Width+
		//"<br/>label3X" +label3X+
		//"<br/>guide1X" +guide1X+
		//"<br/>guide2X" +guide2X+
		//"<br/>guide3X" +guide3X+
			//"<br/>icon1X" +icon1X+
			//"<br/>icon2X" +icon2X+
			//"<br/>icon3X" +icon3X;
}