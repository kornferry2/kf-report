<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Pipeline Publisher</title>
</head>
  	<body>
  	<font face="verdana" size="4" >Pipeline Publisher</font>
  	<br> 
  	<p> 	
  	<font face="verdana" size="2" >Please select a publish mode before click a pipeline you like to publish</font>
  	<p>
	<form action ="frm" action ="post">
  		<input type="radio" name="mode" value="false" checked><font face="verdana" size="2">Regular Mode</font> 
		<input type="radio" name="mode" value="true"><font face="verdana" size="2">Debug Mode </font>	
    </form>
    <table>  
  	<tr>
  	<th><font face="verdana" size="2" >Vesion 1.0</font></th>
  	<th><p><br></th><th><p><br></th>
  	<th><font face="verdana" size="2" >Vesion 2.0</font></th>
	</tr>
	<p>
	<tr>
	<td><a href="/reportweb/PipelinePublisher?pipeline=lva&manifest=lvaManifest&version=1" id="lva">GL-GPS Calculation Pipeline  </a><p></td>
	<td></td>
	<td style="border-left: 2px solid black; padding: 5px;"></td>
	    <td><a href="/reportweb/PipelinePublisher?pipeline=lva&manifest=lvaManifestV2&version=2" id="lva2">GL-GPS Calculation Pipeline  </a><p></td>
    </tr>
    <tr>
    <td><a href="/reportweb/PipelinePublisher?pipeline=vedge&manifest=vedgeIndividualManifest&version=1" id="vedge">  viaEdge Individual Calculation Pipeline  </a><p></td>
    <td></td>
    <td style="border-left: 2px solid black; padding: 5px;"></td>
    <td><a href="/reportweb/PipelinePublisher?pipeline=vedge&manifest=vedgeIndividualManifestV2&version=2" id="vedge2">viaEdge Individual Calculation Pipeline  </a><p></td>
    </tr>
    <tr>
    <td><a href="/reportweb/PipelinePublisher?pipeline=kfpi&manifest=kfpiManifest&version=1" id="kfpi">KFPI Calculation Pipeline  </a><p></td>
    <td></td>
    <td style="border-left: 2px solid black; padding: 5px;"></td>
    <td><a href="/reportweb/PipelinePublisher?pipeline=vedge&manifest=vedgeGroupManifestV2&version=2&type=group" id="vedge2">viaEdge Group Calculation Pipeline  </a><p></td>
    </tr>
    <tr>
    <td><a href="/reportweb/PipelinePublisher?pipeline=gpi&manifest=gpiManifest&version=1" id="gpi">GPI Calculation Pipeline  </a><p></td>
    <td></td>
    <td style="border-left: 2px solid black; padding: 5px;"></td>
    <td><a href="/reportweb/PipelinePublisher?pipeline=gpi&manifest=gpiManifestV2&version=2" id="gpi2">GPI Calculation Pipeline </a><p></td>
    </tr>
    <tr>
    <td><a href="/reportweb/PipelinePublisher?pipeline=cs&manifest=csManifest&version=1" id="cs">CS Calculation Pipeline  </a><p></td>
    <td></td>
    <td style="border-left: 2px solid black; padding: 5px;"></td>
    <td><p></td>
    </tr>
    <tr>
    <td><a href="/reportweb/PipelinePublisher?pipeline=finex&manifest=finexManifest&version=1" id="finex">FINEX Calculation Pipeline  </a><p></td>
    <td></td>
    <td style="border-left: 2px solid black; padding: 5px;"></td>
    <td><p></td>
    </tr>
    <tr>
    <td><a href="/reportweb/PipelinePublisher?pipeline=lei&manifest=leiManifest&version=1" id="lei">LEI Calculation Pipeline  </a><p></td>
    <td></td>
    <td style="border-left: 2px solid black; padding: 5px;"></td>
    <td><p></td>
    </tr>
    <tr>
    <td><a href="/reportweb/PipelinePublisher?pipeline=rv&manifest=rvManifest&version=1" id="rv">RV Calculation Pipeline  </a><p></td>
    <td></td>
    <td style="border-left: 2px solid black; padding: 5px;"></td>
    <td><p></td>
    </tr>
    <tr>
    <td><a href="/reportweb/PipelinePublisher?pipeline=ssc&manifest=sscManifest&version=1" id="ssc">SSC Calculation Pipeline  </a><p></td>
    <td></td>
    <td style="border-left: 2px solid black; padding: 5px;"></td>
    <td><p></td>
    </tr>
    <tr>
	<td><a href="/reportweb/PipelinePublisher?pipeline=wge&manifest=wgeManifest&version=1" id="wge">WGE Calculation Pipeline  </a><p></td>
	<td></td>
	<td style="border-left: 2px solid black; padding: 5px;"></td>
	<td><p></td>
    </tr>
     </table>
     <br>
     <hr>
     
	<font face="verdana" size="4">
	<title>Test RGR Generation</title>
 	</font>
<!--   	
 	<table>
    <tr>
 	<td>
 	<font face="verdana" size="2">
 	<h3>viaEdge Individual Report RGR</h3><br>
 	 Enter one PID for an individual RGR
 	</font>
 	</td>
 	<td></td>  	
 	<td>
 	<font face="verdana" size="2">
 	<h3>viaEdge Group Report RGR</h3><br>
 	Enter a group PIDs for a Group report RGR
 	</font>  
 	</td>
 	<tr>
 	<td>
 	<input type="text" name="pidtext" id="pidinput" size="6">
 	</td>
 	<td style="border-left: 2px solid black; padding: 5px;"></td>
 	<td>
	<textarea name="pidarea" id="pidsinput" cols="6" rows="3"></textarea>
	</td>
	<tr>
	<td>
	<a href="/reportweb/ViaEdgeRgrServlet?type=indiv&manifest=vedgeIndividualManifest" id="vedgeIndivRgr">  viaEdge Individual Report RGR Scores  </a>
	</td>
	<td style="border-left: 2px solid black; padding: 5px;"></td>
	<td>
	<a href="/reportweb/ViaEdgeRgrServlet?type=grp&manifest=vedgeGroupManifest" id="vedgeGrpRgr">  viaEdge Group Report RGR Scores (enter one PID per line)  </a>
	</td>	
	</tr>
	</table>
	<br>
	
 	<table>
    <tr>
 	<td>
 	<font face="verdana" size="2">
 	<h3>KF Potential RCM Testing V1</h3><br>
 	 Enter one target level (PID:374125) for an individual RGR
 	</font>
 	</td>
 	<td></td>  	
 	<tr>
 	<td>
 	<input type="text" name="pidtext" id="targId" size="6" value ="1">
 	</td>
 	<td style="border-left: 2px solid black; padding: 5px;"></td>
 	<tr>
	<td>
	<a href="/reportweb/KfapRcmServlet?pid=374325&type=KFP_INDIVIDUAL&proj=921&langCode=0&ver=v1" onclick="getRcm(this);" id="kfprcm">  KF Potential RCM  </a>
	</td>				
	</tr>
	</table>
	<br>

 	<table>
    <tr>
 	<td>
 	<font face="verdana" size="2">
 	<h3>KF Potential RCM Testing V2</h3><br>
 	 Enter one target level (PID:374182) for an individual RGR
 	</font>
 	</td>
 	<td></td>  	
 	<tr>
 	<td>
 	<input type="text" name="pidtext" id="targId" size="6" value ="1">
 	</td>
 	<td style="border-left: 2px solid black; padding: 5px;"></td>
 	<tr>
	<td>
	<a href="/reportweb/KfapRcmServlet?pid=374182&type=ALP_IND_V2&proj=921&langCode=0" onclick="getRcm(this);" id="kfprcm2">  KF Potential RCM  </a>
	</td>				
	</tr>
	</table>
	<br>

  
 	<table>
    <tr>
 	<td>    
	<font face="verdana" size="2">
 	<h3>KF Potential Report Testing</h3><br>
 	 Enter RCM
 	</font>
 	</td>
 	<td></td>  	
 	<tr>
 	<td>
 	 <textarea id="rcmTextArea" cols="160" rows="20"></textarea>
 	</td>
 	<td></td>
 	<tr>
	<td>
	<a href="/reportweb/GenerateReportFromRcm?type=KFP_INDIVIDUAL_REPORT&rcm=&pid=374325&hybrid=true&mock=null&" id="kfprpt">  KF Potential Report  </a>
	</td>				
	</tr>
	</table>
	<br>
-->

	<script language="javascript" type="text/javascript">

	function getInputValue(x){
		var val = document.getElementById(x).value;
	    if(val){
	        return val;
	    }else{
	        alert("Please enter a PID!");
	        return;
	    }
	}

	function getTextValues(x){
		var val = document.getElementById(x).value;
		if(val){           
	       var lines = val.split("\n");
	       var catlines = "";
	       for(var i=0; i<lines.length; i++) {
	       	 if(i < lines.length-1){
	  	     	 catlines += lines[i]+";";
		     }else{
		         catlines += lines[i];
		     }
		   }
		   return catlines;
		}else{
		   alert("Please enter a group PIDs!");
		   return;
		}
	}

	function getRadioElement(x){
		var elements = document.getElementsByName(x);
  		var debug = "";
 		for (var i = 0, l = elements.length; i < l; i++){
     		if(elements[i].checked){
         		return elements[i].value;
     		}
 		} 
	}
	
	function getRcm(e){			
		var targ;
	    if (!e) var e = window.event;
	    if (e.target) targ = e.target;
	    else if (e.srcElement) targ = e.srcElement;
	    if (targ.nodeType == 3) // defeat Safari bug
	        targ = targ.parentNode;
	    
		alert("rcm="+targ);
	}
	
	document.getElementById("lva").onclick = function(){this.href += "&debug="+getRadioElement("mode");}
	document.getElementById("vedge").onclick = function(){this.href += "&debug="+getRadioElement("mode");}
	document.getElementById("kfpi").onclick = function(){this.href += "&debug="+getRadioElement("mode");}
	document.getElementById("gpi").onclick = function(){this.href += "&debug="+getRadioElement("mode");}
	document.getElementById("cs").onclick = function(){this.href += "&debug="+getRadioElement("mode");}
	document.getElementById("finex").onclick = function(){this.href += "&debug="+getRadioElement("mode");}
	document.getElementById("lei").onclick = function(){this.href += "&debug="+getRadioElement("mode");}
	document.getElementById("rv").onclick = function(){this.href += "&debug="+getRadioElement("mode");}
	document.getElementById("ssc").onclick = function(){this.href += "&debug="+getRadioElement("mode");}
	document.getElementById("wge").onclick = function(){this.href += "&debug="+getRadioElement("mode");}

	document.getElementById("gpi2").onclick = function(){this.href += "&debug="+getRadioElement("mode");}
	document.getElementById("vedge2").onclick = function(){this.href += "&debug="+getRadioElement("mode");}
	document.getElementById("lva2").onclick = function(){this.href += "&debug="+getRadioElement("mode");}

	document.getElementById("kfprcm").onclick = function(){this.href += "&targ="+getInputValue("targId");}
	document.getElementById("kfprcm2").onclick = function(){this.href += "&targ="+getInputValue("targId");}
	document.getElementById("kfprpt").onclick = function(){this.href += "&rcm="+getInputValue("rcmTextArea");}
	
//	document.getElementById("kfprpt").onclick = function(){this.href += "&rcm="+getRcm();}								 
//	document.getElementById("vedgeIndivRgr").onclick = function(){this.href += "&pid="+getInputValue("pidinput");}
//	document.getElementById("vedgeGrpRgr").onclick = function(){this.href += "&pids="+getTextValues("pidsinput");}

	</script>
</body>
</html>