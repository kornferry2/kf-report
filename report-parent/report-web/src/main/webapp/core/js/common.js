function urlDecode(str) {
    // Javascript, amazingly, lacks a native function that grabs GET params out of the
    // URL, and the decoder doesn't know how to handle "+" used in place of a space.
    return decodeURIComponent(str.replace(/\+/g, '%20'));
}

function extractGetParams() {
  var params={};
  var rawParamPairs=document.location.search.substring(1).split('&');
  for(var pairIndex in rawParamPairs) {
    var keyValue=rawParamPairs[pairIndex].split('=',2);
    params[keyValue[0]] = urlDecode(keyValue[1]);
  }
  var lang = params["lang"];
  if (lang == "ko" || lang == "zh-CN" || lang == "ja" || lang == "zh" || lang == "zh_CN" ) {
	  addClass(lang);
	  }
  return params;
}

function fillIn(classNames) {
	var params=extractGetParams();
	for(var nameIndex in classNames) {
	  var className = classNames[nameIndex];
	  var elements = document.getElementsByClassName(className);
	  var value = params[className];
	  for(var elementIndex = 0; elementIndex < elements.length; ++elementIndex) {
		 var element = elements[elementIndex];
	     if (value){
	    	 element.innerHTML = value;
	     }else{
	    	 element.parentNode.removeChild(element);
	     }
	  }
	}
}

function addClass(lang) {
	document.getElementById("body").className += lang;
}