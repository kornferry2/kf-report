package com.pdinh.rs.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pdinh.data.jaxb.reporting.ErrorRepresentation;
import com.pdinh.exception.PalmsException;
import com.pdinh.report.helper.GenerateReportHelperBean;
import com.pdinh.report.util.JaxbUtil;
import com.pdinh.reports.ReportProcessor;
import com.pdinh.reportserver.data.dao.jpa.ReportContentModelDao;
import com.pdinh.reportserver.persistence.jpa.ReportContentModel;

/**
 * Routine to generate the report to HTML instead of all the way to a PDF
 * 
 * NOTE: This routine is appears to have been created to demonstrate that we can
 * generate the output in multiple formats. It is currently (11/13) being used
 * only in some test routines. We should perform additional analysis at some
 * point to determine if we should continue to support it, deprecate it or
 * delete it.
 */
@WebServlet("/GenerateReportHtml")
public class GenerateReportHtml extends HttpServlet {

	@EJB
	private ReportProcessor reportProcessor;

	@EJB
	private ReportContentModelDao reportDao;

	@EJB
	private GenerateReportHelperBean rptHelper;

	private static final long serialVersionUID = 1L;

	// // Report template paths
	// // Old school... resources not moved for shell... Shell branch not set
	// for
	// // Maven as yet
	// private static final String SHELL_RPT_XFORM =
	// "com/pdinh/resource/projects/shell/ib_report.xsl";
	// // New school... Maven style
	// private static final String LVA_MLL_RPT_XFORM =
	// "projects/lva/mll/lva_report.xsl";
	// private static final String COACHING_PLAN_RPT_XFORM =
	// "projects/coaching/plan_report.xsl";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GenerateReportHtml() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Create the report HTML. Assumes that this is analogous to GenerateRport
	 * (RCM is present in the report_content_model table)
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {

		// Set up hard-coded parm... Hybrid report doesn't hit this code
		boolean hybFlag = false;

		// get request parameters
		// participant id
		int pptId = Integer.parseInt(request.getParameter("pid"));
		// report type
		String reportType = request.getParameter("type");
		// report phase
		int phaseId = Integer.parseInt(request.getParameter("phase"));

		// PhaseId = 0 means the phase is unspecified; we have more work to do
		if (phaseId == 0) {
			phaseId = rptHelper.findPhase(pptId, reportType);
		}

		// mock - if specified, get XML from mock file
		String mockFile = request.getParameter("mock");

		Map<String, String> customParams = rptHelper.getCustomParams(reportType, request);

		// xslt output will be to response stream
		OutputStream responseOutputStream = response.getOutputStream();

		String rcmXml = null;
		try {
			if (mockFile == null || mockFile.isEmpty()) {
				// No mock file... Load the RCM from db
				ReportContentModel rcm = reportDao.findSingleByParticipantIdAndTypeAndPhaseTypeId(pptId, reportType,
						phaseId);
				if (rcm == null) {
					System.out.println("rcm not found (HTML)");
					response.setContentType("text/html");
					String msg = "<html><body><h1 style=\"color:red;\">No reports generated.</h1>No report info available in database for participant "
							+ pptId
							+ ", report type "
							+ reportType
							+ ".  A \"Save\" must be performed prior to a report request.</body></html>";
					response.addHeader("Error", msg);
					responseOutputStream.write(msg.getBytes());
					return;
				}

				// Got a good one
				rcmXml = rcm.getReportXml();
			}
		} catch (Exception excp) {
			excp.printStackTrace();
			response.addHeader("Error", "Report generation failed due to: " + excp.getMessage());
			throw new ServletException(excp);
		}

		// call the helper

		try {
			rptHelper.genReport(GenerateReportHelperBean.GEN_HTML_OUTPUT, reportType, rcmXml, "" + pptId, hybFlag,
					mockFile, customParams, response);
		} catch (PalmsException pe){
			response.addHeader("Error", pe.getMessage() + " " + pe.getErrorDetail());
			ErrorRepresentation error = new ErrorRepresentation(pe);
			
			OutputStream out = response.getOutputStream();
			out.write(JaxbUtil.marshalToXmlString(error, ErrorRepresentation.class).getBytes());
			out.close();
			//return;
		}

		return;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		processRequest(request, response);
	}

}
