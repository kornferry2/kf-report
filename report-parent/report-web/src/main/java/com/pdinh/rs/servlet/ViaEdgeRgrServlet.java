package com.pdinh.rs.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.pdinh.data.jaxb.reporting.ErrorRepresentation;
import com.pdinh.exception.PalmsException;
import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.project.generic.GenericRgrBean;
import com.pdinh.project.viaedge.ViaEdgeRgrBean;
import com.pdinh.report.util.JaxbUtil;

@WebServlet("/ViaEdgeRgrServlet")
public class ViaEdgeRgrServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	ViaEdgeRgrBean vedgeRgrBean;

	@EJB
	GenericRgrBean rgrbean;

	public ViaEdgeRgrServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		process(request, response);
	}

	private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String type = request.getParameter("type");
		String manifest = request.getParameter("manifest") + ".xml";

		ReportGenerationRequest rgr1 = null;

		if (type.equals("indiv")) {
			String pid = request.getParameter("pid");
			try {
				rgr1 = vedgeRgrBean.createIndividualReportRgr(Integer.parseInt(pid));
			
			} catch (PalmsException e) {
				response.addHeader("Error", e.getMessage() + " " + e.getErrorDetail());
				ErrorRepresentation error = new ErrorRepresentation(e);
				
				OutputStream out = response.getOutputStream();
				out.write(JaxbUtil.marshalToXmlString(error, ErrorRepresentation.class).getBytes());
				out.close();
				return;
			}
		} else {
			String pidStr = request.getParameter("pids");
			if (pidStr.substring(pidStr.length() - 1).equals(";")) {
				pidStr = pidStr.substring(0, pidStr.length() - 1);
			}
			List<Integer> pids = new ArrayList<Integer>();
			int count = StringUtils.countMatches(pidStr, ";");

			String[] parts = pidStr.split("\\;");
			for (int i = 0; i < count + 1; i++) {
				int part = Integer.parseInt(parts[i]);
				pids.add(part);
			}
			try {
				rgr1 = vedgeRgrBean.createGroupReportRgr(pids);
			} catch (PalmsException e) {
				response.addHeader("Error", e.getMessage() + " " + e.getErrorDetail());
				ErrorRepresentation error = new ErrorRepresentation(e);
				
				OutputStream out = response.getOutputStream();
				out.write(JaxbUtil.marshalToXmlString(error, ErrorRepresentation.class).getBytes());
				out.close();
				return;
			}
		}

		ReportGenerationRequest rgr2 = null;
		rgr2 = rgrbean.createScoredRgr(manifest, rgr1);
		if (rgr2 == null){
			response.addHeader("Error", "Error creating report generation request");
			
			return;
		}
		String output = null;
		output = JaxbUtil.marshalToXmlString(rgr2, ReportGenerationRequest.class);

		// System.out.println("RGR input for viaEdge Report:"+" "
		// + output);

		try {
			response.setContentType("text/html;charset=UTF-8");
			OutputStream out = response.getOutputStream();
			out.write(output.getBytes("UTF-8"));
			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			response.addHeader("Error", "Error writing Report Generation request to output stream.");
			return;
		}

		// PrintWriter pw = response.getWriter();
		// pw.print("<html>");
		// pw.print("<head>");
		// pw.print("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
		// pw.print("</head>");
		// pw.print("<font face=\"verdana\" size=\"2\">");
		// pw.print(ok ? " Calculate vaiEdge "+type+" report scores... " :
		// "Fail to test viaEdge "+type+" report calculation.");
		// pw.print("<p>");
		// pw.print("Output Scores (view scores from the eclipse console)");
		// pw.print("</p>");
		// pw.print("<form name=\"theForm\" method=\"POST\" action=\"/reportweb/faces/pipelinePublisher.jsp\">");
		// pw.print("<p>");
		// pw.print("<input type = \"submit\" value = \"Go Back\"/>");
		// pw.print("</p>");
		// pw.print("</form>");
		// pw.print("<p>");
		// pw.print("</font>");
		// pw.print("</p>");
		// pw.print("</body>");
		// pw.print("</html>");
		// pw.close();

		return;
	}
}