package com.pdinh.rs.servlet;

import java.io.IOException;
import java.io.OutputStream;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.report.util.JaxbUtil;
import com.pdinh.reportserver.data.dao.jpa.ReportRequestResponseDao;
import com.pdinh.reportserver.persistence.jpa.ReportRequestResponse;

/**
 * Servlet implementation class FetchRgrServlet
 */
@WebServlet("/FetchRgrServlet")
public class FetchRgrServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	final static Logger logger = LoggerFactory.getLogger(FetchRgrServlet.class);

	//
	// Beans and properties
	//
	@EJB
	private ReportRequestResponseDao rgrAccessBean;

	//
	// Constructors
	//
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FetchRgrServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	//
	// Instance methods
	//
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		process(request, response);
	}

	/**
	 * process - Does the work for the servlet. It turns out that it is merely a
	 * wrapper around an instance of the ReportRequestResponseDao bean that does
	 * the heavy lifting
	 * 
	 * NOTE: This could probably have been wrapped up with the SaveRgrServlet,
	 * but was maintained separately partly to segregate functionality and
	 * partly because there is really no good way to define constants (for the
	 * transaction type, in this case) that are available to peers unless we
	 * start a constants class in the admin app somewhere to provide that
	 * functionality.
	 * 
	 * If that were to happen, then this servlet could be easily combined with
	 * the SaveRgrServlet; all we would need is an additional parameter with the
	 * trasaction type ("Save" or "Fetch") an a little logic to do the
	 * validation and action for each transaction type.
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean ok = true;
		int pptId = 0;
		String outXml = "";

		// Get the parameters
		String pptIdStr = request.getParameter("pptId");
		String type = request.getParameter("type");

		// Check them for null
		if (pptIdStr == null || type == null) {
			logger.info("Invalid RGR parameter(s); fetch not attempted.  Ppt={}, type={}", pptIdStr, type);
			ok = false;
		} else {
			try {
				pptId = Integer.parseInt(pptIdStr);
			} catch (NumberFormatException e) {
				logger.info("Invalid participant Id for fetch; PptID={}", pptIdStr);
				ok = false;
			}
		}

		ReportRequestResponse rrr = null;
		if (ok) {
			// Fetch it
			rrr = rgrAccessBean.findSingleByParticipantIdAndType(pptId, type);
			if (rrr == null) {
				logger.error("RGR fetch failed.  Ppt={}, type={}", pptId, type);
				ok = false;
			}
		}

		if (ok) {
			outXml = rrr.getRgrXml();
		}

		// Send it back
		response.setContentType("text/plain");
		OutputStream out = response.getOutputStream();
		out.write(outXml.getBytes("UTF-8"));
		out.close();

		return;
	}
}
