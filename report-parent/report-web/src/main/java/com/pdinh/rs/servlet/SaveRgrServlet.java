package com.pdinh.rs.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.reportserver.data.dao.jpa.ReportRequestResponseDao;
import com.pdinh.reportserver.persistence.jpa.ReportRequestResponse;

/**
 * Servlet implementation class SaveRgrServlet - Saves the passed XML in the
 * report_request_response table
 */
@WebServlet("/SaveRgrServlet")
public class SaveRgrServlet extends HttpServlet {
	//
	// Static variables
	//
	private static final long serialVersionUID = 1L;

	final static Logger logger = LoggerFactory.getLogger(SaveRgrServlet.class);

	//
	// Beans and resources
	//

	@EJB
	private ReportRequestResponseDao rgrAccessBean;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SaveRgrServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		process(request, response);
	}

	/**
	 * process - Does the work for the servlet. It turns out that it is merely a
	 * wrapper around an instance of the ReportRequestResponseDao bean that does
	 * the heavy lifting
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean ok = true;
		// Get the parameters
		String pptId = request.getParameter("pptId");
		String type = request.getParameter("type");
		String rgrXml = request.getParameter("rgrXml");

		// Check them for null
		if (pptId == null || type == null || rgrXml == null) {
			logger.info("Invalid RGR parameter(s); persistence not attempted.  Ppt={}, type={}, xml={}", pptId, type,
					rgrXml);
			ok = false;
		} else {
			// Persist it
			ReportRequestResponse rrr = rgrAccessBean.createOrUpdate(Integer.valueOf(pptId), type, rgrXml, null);
			if (rrr == null) {
				logger.error("RGR persistence failed.  Ppt={}, type={}, xml={}", pptId, type, rgrXml);
				ok = false;
			}
		}

		response.setContentType("text/plain");
		PrintWriter pw = response.getWriter();
		pw.print(ok ? "Succeed" : "Fail");
	}

}
