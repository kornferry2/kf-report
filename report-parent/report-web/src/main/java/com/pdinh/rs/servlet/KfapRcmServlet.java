package com.pdinh.rs.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import java.util.Scanner;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.jaxb.reporting.ErrorRepresentation;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.project.kfap.KfapGroupRgrToRcm;
import com.pdinh.project.kfap.KfapReportRgrToRcm;
import com.pdinh.project.kfap.KfapReportRgrToRcmV2;
import com.pdinh.project.kfap.TalentGridRgrToRcmV2;
import com.pdinh.report.util.JaxbUtil;

@WebServlet("/KfapRcmServlet")
public class KfapRcmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = LoggerFactory.getLogger(KfapRcmServlet.class);

	@EJB
	private KfapReportRgrToRcm kfapReportRgrToRcmBean;

	@EJB
	private KfapReportRgrToRcmV2 kfapReportRgrToRcmBeanV2;

	@EJB
	private KfapGroupRgrToRcm kfapGroupRgrToRcmBean;

	@EJB
	private TalentGridRgrToRcmV2 TalentGridRgrToRcmBeanV2;

	@Resource(mappedName = "application/report/config_properties")
	private Properties rcrProperty;

	public KfapRcmServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			process(request, response);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		try {
			process(request, response);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * process - The guts of the servlet. Fetches or creates the appropriate
	 * RGR, creates the proper RCM and returns it
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException, JAXBException {
		String pid = request.getParameter("pid");
		String type = request.getParameter("type");
		String proj = request.getParameter("proj");
		// String targetRoleId = request.getParameter("targetRoleId");
		String langCode = request.getParameter("langCode");
		String targ = request.getParameter("targ");
		String targId = request.getParameter("targId");
		String targCode = request.getParameter("targCode");
		String pptList = request.getParameter("pptList");
		String rptTitle = request.getParameter("rptTitle");

		System.out.println("In KfapRcmSevlet. Target level = " + targ + ", type = " + type + ", pid = " + pid + ", targId = " + targId + ", targCode = " + targCode);

		// String rgrXml = request.getParameter("rgrxml");
		// ReportGenerationRequest rgrScored = fetchRgr2("kfap", rgrXml, false);
		String rcm = null;
		try {
			if (type.equals(ReportType.KFP_INDIVIDUAL)) {
				rcm = kfapReportRgrToRcmBean.generateReportRcm(type, proj, pid, langCode, targ);
			} else if (type.equals(ReportType.ALP_IND_V2)) {
				rcm = kfapReportRgrToRcmBeanV2.generateReportRcm(type, proj, pid, langCode, targ);
			} else if (type.equals(ReportType.ALP_TALENT_GRID_V2)) {
				rcm = TalentGridRgrToRcmBeanV2.generateReportRcm(type, pptList, langCode, targ, rptTitle);
			} else if (type.equals(ReportType.KFP_SLATE)) {
				rcm = kfapGroupRgrToRcmBean.generateReportRcm(type, pptList, langCode, targ, rptTitle);
			}
			if (rcm == null) {
				logger.warn("Passed type ({}) causes no RCM to be generated", type);
				throw new PalmsException("Passed type ({}) causes no RCM to be generated", type);
			}
			OutputStream out = response.getOutputStream();
			out.write(rcm.getBytes("UTF-8"));
			out.close();
		} catch (PalmsException pe) {
			response.addHeader("Error", pe.getMessage());
			ErrorRepresentation error = new ErrorRepresentation(pe);
			logger.debug("Caught Palms Exception, added header with value: {}", pe.getMessage());
			OutputStream out = response.getOutputStream();
			out.write(JaxbUtil.marshalToXmlString(error, ErrorRepresentation.class).getBytes());
			out.close();
			// return;
		}
		/*
				try {
					OutputStream out = response.getOutputStream();
					out.write(rcm.getBytes("UTF-8"));
					out.close();
				} catch (Exception ex) {
					ex.printStackTrace();
					return;
				}
				*/
	}

	private ReportGenerationRequest fetchRgr2(String instCode, String rgr2Xml, boolean debugging) {

		String cerPath = rcrProperty.getProperty("reportContentRepository") + "/" + "reportGenerationResponse" + "/";
		String rgr2xmlPath = cerPath + "projects/internal/" + instCode + "/" + rgr2Xml + ".xml";

		if (debugging)
			System.out.println("rgr2Xml path=" + rgr2xmlPath);
		File ff = new File(rgr2xmlPath);
		// System.out.println("Manifest=" + genPath);
		// Check if accessible
		String tstPath = null;
		try {
			tstPath = ff.getCanonicalPath();
		} catch (IOException e) {
			System.out.println("Error accessing path name (" + rgr2xmlPath + ").  msg=" + e.getMessage());
			return null;
		}
		if (debugging)
			System.out.println("rgr2 File Path=" + tstPath);

		// Un-marshal the manifest
		FileInputStream fis = null;
		JAXBContext jc = null;
		Unmarshaller um = null;
		ReportGenerationRequest rgr2 = null;

		try {
			fis = new FileInputStream(ff);
			// Create the context & the unmarshaller
			jc = JAXBContext.newInstance(ReportGenerationRequest.class);
			um = jc.createUnmarshaller();
			rgr2 = (ReportGenerationRequest) um.unmarshal(fis);

			if (debugging) {
				// Convert the input to a string for output
				StringBuilder inp = new StringBuilder((int) ff.length());
				Scanner scanner = new Scanner(ff);
				String lineSeparator = System.getProperty("line.separator");
				try {
					while (scanner.hasNextLine()) {
						inp.append(scanner.nextLine() + lineSeparator);
					}
				} finally {
					scanner.close();
				}
				System.out.println("DEBUG - rgr2 input:\n" + inp.toString());

				System.out.println("DEBUG - rgr2 output:\n" + rgr2.toString());
			}

			return rgr2;
		} catch (FileNotFoundException e) {
			System.out.println("Rgr2 does not exist.  Path=" + tstPath + ",  msg=" + e.getMessage());
			return null;
		} catch (JAXBException e) {
			System.out.println("Error unmarshalling rgr2.  msg=" + e.getMessage());
			return null;
		} finally {
			um = null;
			jc = null;
			try {
				fis.close();
			} catch (Exception e) { /* Swallow it */
			}
		}
	}

}
