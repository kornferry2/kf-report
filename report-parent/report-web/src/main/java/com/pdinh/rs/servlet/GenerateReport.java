package com.pdinh.rs.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pdinh.data.jaxb.reporting.ErrorRepresentation;
import com.pdinh.exception.PalmsException;
import com.pdinh.report.helper.GenerateReportHelperBean;
import com.pdinh.report.util.JaxbUtil;
import com.pdinh.reportserver.data.dao.jpa.ReportContentModelDao;
import com.pdinh.reportserver.persistence.jpa.ReportContentModel;

@WebServlet("/GenerateReport")
public class GenerateReport extends HttpServlet {

	@EJB
	ReportContentModelDao reportDao;

	@EJB
	private GenerateReportHelperBean rptHelper;

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GenerateReport() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		// Set up hard-coded parm... Hybrid report doesn't hit this code
		boolean hybFlag = false;

		// get request parameters
		// participant id
		int pptId = Integer.parseInt(request.getParameter("pid"));
		// report type
		String reportType = request.getParameter("type");
		// report phase
		int phaseId = Integer.parseInt(request.getParameter("phase"));

		// PhaseId = 0 means the phase is unspecified; we have more work to do
		if (phaseId == 0) {
			phaseId = rptHelper.findPhase(pptId, reportType);
		}

		// mock - if specified, get XML from mock file
		String mockFile = request.getParameter("mock");

		Map<String, String> customParams = rptHelper.getCustomParams(reportType, request);

		OutputStream responseOutputStream = response.getOutputStream();
		String rcmXml = null;
		if (mockFile == null || mockFile.isEmpty()) {
			// No mock file... Load the RCM
			// load RCM from db
			ReportContentModel rcm = reportDao.findSingleByParticipantIdAndTypeAndPhaseTypeId(pptId, reportType,
					phaseId);
			if (rcm == null) {
				System.out.println("rcm not found");
				
				response.setContentType("text/html");
				String msg = "<html><body><h1 style=\"color:red;\">No reports generated.</h1>No report info available in database for participant "
						+ pptId
						+ ", report type "
						+ reportType
						+ ".  A \"Save\" must be performed prior to a report request.</body></html>";
				response.addHeader("Error", msg);
				responseOutputStream.write(msg.getBytes());
				return;
			}

			// Got a live one here
			rcmXml = rcm.getReportXml();
		}

		try {
			rptHelper.genReport(GenerateReportHelperBean.GEN_PDF_OUTPUT, reportType, rcmXml, "" + pptId, hybFlag, mockFile,
					customParams, response);
		}catch (PalmsException pe){
			response.addHeader("Error", pe.getMessage() + " " + pe.getErrorDetail());
			ErrorRepresentation error = new ErrorRepresentation(pe);
			
			OutputStream out = response.getOutputStream();
			out.write(JaxbUtil.marshalToXmlString(error, ErrorRepresentation.class).getBytes());
			out.close();
			//return;
		}

		return;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		processRequest(request, response);
	}

}
