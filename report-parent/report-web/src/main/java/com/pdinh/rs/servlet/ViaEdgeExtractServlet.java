/**
 * Copyright (c) 2013 Personnel Decisions International, Inc. a Korn/Ferry company
 * All rights reserved.
 */
package com.pdinh.rs.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.pdinh.rs.helper.ViaEdgeExtractHelper;

/**
 * Servlet implementation. ViaEdgeExtractServlet generates the extract of the
 * individual scores in the Group Report.
 * 
 * NOTE: This generates a .xlsx Workbook (Office 2007 and later)
 * 
 * Should this be in PDINHServer? Or somewhere else?
 */
@WebServlet(name = "/ViaEdgeExtractServlet", urlPatterns = { "/ViaEdgeExtractServlet/*" })
public class ViaEdgeExtractServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String EXTRACT_DATE_FORMAT = "yyyyMMdd";

	final static Logger logger = LoggerFactory.getLogger(ViaEdgeRcmServlet.class);

	@Resource(mappedName = "application/report/config_properties")
	private Properties configProperties;

	//
	// Constructors
	//
	public ViaEdgeExtractServlet() {
		super();
	}

	/**
	 * doGet
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	/**
	 * doPost
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		process(request, response);
	}

	/**
	 * process - The guts of the servlet. Transforms the RCM to the spreadsheet
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String rcm = request.getParameter("rcm");
		String reportType = request.getParameter("type");
		String proj = request.getParameter("proj");

		if (rcm == null) {
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			String msg = "<html><body><h1 style=\"color:red;\">No reports generated.</h1>rcm paramemter is null."
					+ "  proj = " + proj + ", report type " + reportType + "</body></html>";
			pw.print(msg);
			response.addHeader("Error", msg);
			return;
		}

		// Make a document
		Document doc = null;
		List<Map<String, Object>> scores = new ArrayList<Map<String, Object>>();
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			docFactory.setIgnoringElementContentWhitespace(true);
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// convert to xml doc
			doc = docBuilder.parse(new InputSource(new StringReader(rcm)));

			// Extract the data from the doc

			XPath xpath = XPathFactory.newInstance().newXPath();
			try {
				NodeList rows = (NodeList) xpath.evaluate(
						"/viaEdgeGroupReport/individualScoresSection/tables/table/rows/row", doc,
						XPathConstants.NODESET);
				// System.out.println("Found " + rows.getLength() +
				// " matching nodes...");
				for (int i = 0, len = rows.getLength(); i < len; i++) {
					Node row = rows.item(i);
					// System.out.println(i + ":  name=" + row.getNodeName()
					// + ", value=" + row.getNodeValue()
					// + ", x=" + row.getFirstChild());
					Map<String, Object> rowVals = new HashMap<String, Object>();
					NodeList nodeList = row.getChildNodes();
					// System.out.println("Found " + nodeList.getLength() +
					// " child nodes...");
					for (int j = 0, len2 = nodeList.getLength(); j < len2; j++) {
						Node col = nodeList.item(j);
						if (col.getNodeType() == Node.ELEMENT_NODE) {
							// System.out.println(j + ":  nodeType=" +
							// col.getNodeType() + ", localName="
							// + col.getLocalName() + ", nodeName=" +
							// col.getNodeName() + ", nodeValue="
							// + col.getNodeValue() + ", txtContent=" +
							// col.getTextContent());
							rowVals.put(col.getNodeName(), col.getTextContent());
						}
					}
					if (!rowVals.isEmpty()) {
						scores.add(rowVals);
					}
				}

			} catch (XPathExpressionException e) {
				logger.error("viaEdge Group Score Extract gen - XPath error.", e);
				response.setContentType("text/html");
				PrintWriter pw = response.getWriter();
				String msg = "<html><body><h1 style=\"color:red;\">No reports generated.</h1>Logic error (XPath error)."
						+ ", report type " + reportType + "</body></html>";
				pw.print(msg);
				response.addHeader("Error", msg);
				return;
			}
		} catch (ParserConfigurationException e) {
			logger.error("viaEdge Group Score Extract gen - Parser config error.", e);
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			String msg = "<html><body><h1 style=\"color:red;\">No reports generated.</h1>Logic error (Parser config error)."
					+ ", report type " + reportType + "</body></html>";
			pw.print(msg);
			response.addHeader("Error", msg);
			return;
		} catch (SAXException e) {
			logger.error("viaEdge Group Score Extract gen - Parser error.", e);
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			String msg = "<html><body><h1 style=\"color:red;\">No reports generated.</h1>Data error (Parser exception)."
					+ ", report type " + reportType + "</body></html>";
			pw.print(msg);
			response.addHeader("Error", msg);
			return;
		}

		// Mark Arnold May 12, 2015
		// Get the watermark and put it in the title field
		String watermark = "";
		watermark = configProperties.getProperty("watermark");

		// create the spreadsheet
		Workbook wb = ViaEdgeExtractHelper.createGroupScoreExtractExcel(scores, watermark);
		// Workbook wb = null;
		if (wb == null) {
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			String msg = "<html><body><h1 style=\"color:red;\">No reports generated.</h1>Generated Workbook is null."
					+ "  proj = " + proj + ", report type " + reportType + "</body></html>";
			pw.print(msg);
			response.addHeader("Error", msg);
			return;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(EXTRACT_DATE_FORMAT);
		String date = sdf.format(new Date());

		response.setHeader("Content-Disposition", "attachment; filename=viaEdgeGroupScoreExtract_" + proj + "_" + date
				+ ".xlsx");
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

		OutputStream out = response.getOutputStream();
		wb.write(out);
		out.close();

		return;

	}
}
