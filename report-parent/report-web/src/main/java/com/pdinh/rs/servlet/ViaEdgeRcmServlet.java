/**
 * Copyright (c) 2013 Personnel Decisions International, Inc. a Korn/Ferry company
 * All rights reserved.
 */
package com.pdinh.rs.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.jaxb.reporting.ErrorRepresentation;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.presentation.domain.ReportOptionsObj;
import com.pdinh.project.viaedge.CoachingReportRgrToRcm;
import com.pdinh.project.viaedge.GroupReportRgrToRcm;
import com.pdinh.project.viaedge.IndividualReportRgrToRcm;
import com.pdinh.project.viaedge.ViaEdgeRgrBean;
import com.pdinh.report.util.JaxbUtil;
import com.pdinh.reporting.generation.Participants;
import com.pdinh.reporting.generation.ReportParticipant;
import com.pdinh.reportserver.data.dao.jpa.ReportRequestResponseDao;
import com.pdinh.reportserver.persistence.jpa.ReportRequestResponse;

/**
 * Servlet implementation class GetVEdgeIndvRcm Get the RCM for the various
 * viaEDGE individual reports
 * 
 * Should this be in PDINHServer?
 */
@WebServlet(name = "/ViaEdgeRcmServlet", urlPatterns = { "/ViaEdgeRcmServlet/*" })
public class ViaEdgeRcmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	final static Logger logger = LoggerFactory.getLogger(ViaEdgeRcmServlet.class);

	@EJB
	private ViaEdgeRgrBean vEdgeRgrBean;

	@EJB
	private com.pdinh.project.generic.GenericRgrBean genericRgrBean;

	@EJB
	private ReportRequestResponseDao rgrAccessBean;

	@EJB
	private IndividualReportRgrToRcm viaEdgeIReportBean;

	@EJB
	private CoachingReportRgrToRcm viaEdgeCReportBean;

	@EJB
	private GroupReportRgrToRcm viaEdgeGReportBean;

	@EJB
	private ViaEdgeRgrBean rgrBean;

	// @ManagedProperty(name = "rgrHelperBean", value = "#{rgrHelperBean}")
	// private RgrHelperBean rgrHelperBean;
	//
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ViaEdgeRcmServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		process(request, response);
	}

	/**
	 * process - The guts of the servlet. Fetches or creates the appropriate
	 * RGR, creates the proper RCM and returns it
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Get the parameters
		// Not used for Group Summary... null in that case
		String pptId = request.getParameter("pptId");
		// Used only in Group Report... null otherwise
		String pptList = request.getParameter("pptList");
		String langCode = request.getParameter("langCode");
		String type = request.getParameter("type");
		String pptInfoType = request.getParameter("pptInfoType");
		boolean showPptIds = false;
		if (pptInfoType != null) {
			showPptIds = pptInfoType.equals(ReportOptionsObj.RO_IDS) ? true : false;
		}
		String title = request.getParameter("rptTitle");

		// Create the RGR
		ReportGenerationRequest rgrScored = null;
		if (type.equals(ReportType.VIAEDGE_GROUP_FEEDBACK) || type.equals(ReportType.VIAEDGE_GROUP_EXTRACT)) {
			// This is a group report (or extract)
			// Unmarshal the pptList and make a List of Integers
			Participants participants = (Participants) JaxbUtil.unmarshal(pptList, Participants.class);
			List<Integer> groupPpts = new ArrayList<Integer>();
			for (ReportParticipant ppt : participants.getParticipants()) {
				Integer id;
				try {
					id = Integer.parseInt(ppt.getParticipantId());
				} catch (NumberFormatException e) {
					logger.warn("viaEdge Group Feedback RCM gen - Invalid pptId {}. Skipped.", ppt.getParticipantId());
					continue;
				}
				groupPpts.add(id);
			}

			// Create the RGR
			ReportGenerationRequest rgrRaw;
			try {
				rgrRaw = rgrBean.createGroupReportRgr(groupPpts);
			} catch (PalmsException e) {
				response.addHeader("Error", e.getMessage() + " " + e.getErrorDetail());
				ErrorRepresentation error = new ErrorRepresentation(e);
				
				OutputStream out = response.getOutputStream();
				out.write(JaxbUtil.marshalToXmlString(error, ErrorRepresentation.class).getBytes());
				out.close();
				return;
			}
			rgrScored = genericRgrBean.createScoredRgr("vedgeGroupManifest.xml", rgrRaw);
		} else {
			// Individual report generation
			ReportGenerationRequest rgrRaw = null;
			try{
			// Get the appropriate responses
				if (type.equals(ReportType.HYBRID_RPT)) {
					// Responses reside in scorm
					rgrRaw = vEdgeRgrBean.createIndividualReportRgrScorm(Integer.parseInt(pptId));
					// Set type to individual report (from hybrid)
					type = ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK;
				} else {
					// Responses reside in TinCan
					rgrRaw = vEdgeRgrBean.createIndividualReportRgr(Integer.parseInt(pptId));
				}
			}catch (PalmsException e){
				response.addHeader("Error", e.getMessage() + " " + e.getErrorDetail());
				ErrorRepresentation error = new ErrorRepresentation(e);
				
				OutputStream out = response.getOutputStream();
				out.write(JaxbUtil.marshalToXmlString(error, ErrorRepresentation.class).getBytes());
				out.close();
				return;
			}
			// Score it
			rgrScored = genericRgrBean.createScoredRgr("vedgeIndividualManifest.xml", rgrRaw);
			// Persist it
			rgrAccessBean.createOrUpdate(Integer.valueOf(pptId), ReportRequestResponse.VIAEDGE_INDIVIDUALS,
					JaxbUtil.marshalToXmlString(rgrScored, ReportGenerationRequest.class), null);
		}

		// Now create the RCM
		String rcm = "";
		if (type.equals(ReportType.HYBRID_RPT)) {
			type = ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK;
		}
		try{
			if (type.equals(ReportType.VIAEDGE_INDIVIDUAL_SUMMARY)) {
				// individual Summary Report
				
				rcm = viaEdgeIReportBean.generateReportRcm(rgrScored, langCode);
				
			} else if (type.equals(ReportType.VIAEDGE_INDIVIDUAL_COACHING)
					|| type.equals(ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK)) {
				
				rcm = viaEdgeCReportBean.generateReportRcm(rgrScored, langCode, type);
				
			} else if (type.equals(ReportType.VIAEDGE_GROUP_FEEDBACK) || type.equals(ReportType.VIAEDGE_GROUP_EXTRACT)) {
				rcm = viaEdgeGReportBean.generateReportRcm(rgrScored, langCode, showPptIds, title);
			} else {
				logger.warn("RCM generation - Invalid report type - {}. PptId={}", type, pptId);
			}
		}catch (PalmsException e){
			response.addHeader("Error", e.getMessage() + " " + e.getErrorDetail());
			ErrorRepresentation error = new ErrorRepresentation(e);
			
			OutputStream out = response.getOutputStream();
			out.write(JaxbUtil.marshalToXmlString(error, ErrorRepresentation.class).getBytes());
			out.close();
			return;
		}
		logger.trace("Generated rcm: {}", rcm);

		// Send it back

		OutputStream out = response.getOutputStream();
		out.write(rcm.getBytes("UTF-8"));
		out.close();

		return;
	}
}
