package com.pdinh.rs.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.exception.PalmsException;
import com.pdinh.report.helper.KfalpGroupExtractHelperBean;
import com.pdinh.report.util.JaxbUtil;
import com.pdinh.reporting.generation.Participants;
import com.pdinh.reportserver.data.dao.GroupExtractData;

/**
 * Servlet implementation. KfalpGroupExtractServlet generates the ALP Group
 * Extract of the participant(s) designated upon input.
 * 
 * NOTE: This generates a .xlsx Workbook (Office 2007 and later)
 */
@WebServlet(name = "/KfalpGroupExtractServlet", urlPatterns = { "/KfalpGroupExtractServlet/*" })
public class KfalpGroupExtractServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	final static Logger logger = LoggerFactory.getLogger(KfalpGroupExtractServlet.class);

	private static final String DEFAULT_LANG = "en-US";
	// Only supports US English at this time
	private static String[] langCodes = { "en" };

	private static final String RPT_NAME_DATE_FORMAT = "yyMMdd_HHmmss";

	@EJB
	KfalpGroupExtractHelperBean helperBean;

	//
	// Constructors
	//
	public KfalpGroupExtractServlet() {
		super();
	}

	//
	/**
	 * doGet
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	/**
	 * doPost
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		process(request, response);
	}

	/**
	 * process - The guts of the servlet. Transforms the data into the
	 * spreadsheet
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// validate
		GroupExtractData parms;
		try {
			parms = validateParameters(request);
		} catch (IllegalArgumentException e) {
			genErrResponse("ALP Group Extract parameter error", e, response);
			return;
		}
		//

		// Create the Workbook
		Workbook wb = null;
		try {
			wb = helperBean.constructExtractWorkbook(parms);
		} catch (IllegalArgumentException e) {
			genErrResponse("Invalid Parameters", e, response);
			return;
		} catch (IllegalStateException e) {
			genErrResponse("State Error", e, response);
			return;
		} catch (PalmsException e) {
			genErrResponse("Generation Error", e, response);
			return;
		}

		if (wb == null) {
			genErrResponse("Invalid Workbook Error", new Exception("no Workbook was returned from generation."),
					response);
			return;
		}

		// Set up for the file name
		SimpleDateFormat sdf = new SimpleDateFormat(RPT_NAME_DATE_FORMAT);
		String date = sdf.format(new Date());

		// /////username_ReportName_en-US_datetime_TargetLevel
		String fileName = "ALPGroupExtract_" + parms.getLanguageCode() + "_" + date + "_" + parms.getTargCode();
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".xlsx");

		OutputStream out = response.getOutputStream();
		wb.write(out);
		out.close();

		return;
	}

	private void genErrResponse(String errHdr, Exception ex, HttpServletResponse response) throws IOException {
		SimpleDateFormat sdf = new SimpleDateFormat(RPT_NAME_DATE_FORMAT);
		String errDate = sdf.format(new Date());

		String msg = "<html><body><h1 style=\"color:red;\">" + errHdr + "</h1>" + ex.getMessage() + "</body></html>";
		response.setContentType("text/html");
		response.addHeader("Error", msg);
		response.setHeader("Content-Disposition", "attachment; filename=ALPGroupExtract_Error_" + errDate + ".html");
		OutputStream out = response.getOutputStream();
		out.write(msg.getBytes(Charset.forName("UTF-8")));
		out.close();
	}

	/**
	 * 
	 * @param request
	 */
	private GroupExtractData validateParameters(HttpServletRequest request) throws IllegalArgumentException {
		GroupExtractData ret = new GroupExtractData();

		// Check target level
		String targStr = request.getParameter("targ");
		if (StringUtils.isEmpty(targStr)) {
			String eStr = "Target level is missing";
			logger.error(eStr);
			throw new IllegalArgumentException(eStr);
		}
		try {
			ret.setTargetLevel(Integer.parseInt(targStr));
		} catch (NumberFormatException e) {
			String eStr = "Target level (" + targStr + ") is not a valid number";
			logger.error(eStr);
			throw new IllegalArgumentException(eStr);
		}

		// Pull in the target code
		if (StringUtils.isEmpty(request.getParameter("targCode"))) {
			String eStr = "Target level code is not a available";
			logger.error(eStr);
			throw new IllegalArgumentException(eStr);
		} else {
			ret.setTargCode(request.getParameter("targCode"));
		}

		// pull in the language code
		if (StringUtils.isEmpty(request.getParameter("langCode"))) {
			logger.info("Language not passed.  Using defined default (" + DEFAULT_LANG + ")");
		}
		ret.setLanguageCode(getLanguage(request.getParameter("langCode")));

		// Check the group name parameter (comes in as rptTitle)
		String gStr = request.getParameter("rptTitle");
		if (gStr != null) {
			gStr = StringEscapeUtils.unescapeHtml4(gStr.trim());
		}
		ret.setGroupName(gStr);

		// Validate the type is present... no additional validation at this
		// time. It will be added if/when it becomes necessary.
		String tStr = request.getParameter("type");
		if (StringUtils.isEmpty(tStr)) {
			String eStr = "Report type string is missing";
			logger.error(eStr);
			throw new IllegalArgumentException(eStr);
		}
		ret.setRptType(tStr);

		// Validate the participant input
		String pStr = request.getParameter("pptList");
		if (StringUtils.isEmpty(pStr)) {
			String eStr = "Participant list string is missing";
			logger.info(eStr);
			throw new IllegalArgumentException(eStr);
		}
		String dStr = null;
		try {
			dStr = (URLDecoder.decode(pStr, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			String eStr = "Unable to decode Participant list as UTF-8.  String=" + dStr;
			logger.info(eStr);
			throw new IllegalArgumentException(eStr);
		}
		// Get Participants object - a list of ReportParticipant objects
		Participants ppts = (Participants) JaxbUtil.unmarshal(dStr, Participants.class);
		if (ppts == null || ppts.getParticipants() == null || ppts.getParticipants().isEmpty()) {
			String eStr = "Participant list is empty";
			logger.info(eStr);
			throw new IllegalArgumentException(eStr);
		}
		ret.setParticipantsObject(ppts);

		// Nothing is done with the parameters not used -- type (rptType),
		// targCode (TargCode), or targId (targId)

		return ret;

	}

	/**
	 * getLanguage - transforms the input for language code to a valid language
	 * code
	 * 
	 * @param language
	 * @return
	 */
	private String getLanguage(String language) {
		/* 
		 * in a case other languages are not available it use default "en", 
		 * which the langCode = langCodes.get(0) in the language code array.   
		 */
		String lang = "";

		if (language != null) {
			if (Arrays.asList(langCodes).contains(language)) {
				if (language.indexOf("_") != -1) {
					// Language has a '_' char... transform it to a '-'
					lang = language.replace("_", "-");
				} else if (language.equals("en")) {
					// "en" should be "en-US" (the default language)
					lang = DEFAULT_LANG;
				} else {
					// The input was good... use it
					lang = language;
				}
			} else {
				// The input was not a supported language... use default
				lang = DEFAULT_LANG;
			}
		} else {
			// input was null... use default
			lang = DEFAULT_LANG;
		}

		return lang;
	}

}