package com.pdinh.rs.servlet;

import java.io.IOException;
import java.io.OutputStream;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pdinh.data.jaxb.reporting.ErrorRepresentation;
import com.pdinh.exception.PalmsException;
import com.pdinh.report.helper.GenerateReportHelperBean;
import com.pdinh.report.util.JaxbUtil;

/**
 * Servlet implementation class GenerateReportFromRcm
 */
@WebServlet("/GenerateReportFromRcm")
public class GenerateReportFromRcm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private GenerateReportHelperBean rptHelper;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GenerateReportFromRcm() {
		super();
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {

		response.setContentType("application/pdf");
		response.setCharacterEncoding("UTF-8");

		// get querystring parameters
		// participant id
		String reportType = request.getParameter("type");

		// rcm xml
		String rcmXml = request.getParameter("rcm");

		// ppt id... If you want it ibn the report name, send it into here with
		// a key of "pid"
		String pptId = request.getParameter("pid");

		boolean hybFlag = (request.getParameter("hybrid") == null ? false : true);

		// Optional parameter
		String mockFile = request.getParameter("mock");

		// No custom parameters in reports that call this servlet (yet)
		// Gen the report... no custom params exist in this generation type
		try {
			rptHelper.genReport(GenerateReportHelperBean.GEN_PDF_OUTPUT, reportType, rcmXml, pptId, hybFlag, mockFile,
					null, response);
		}catch (PalmsException pe){
			response.addHeader("Error", pe.getMessage() + " " + pe.getErrorDetail());
			ErrorRepresentation error = new ErrorRepresentation(pe);
			
			OutputStream out = response.getOutputStream();
			out.write(JaxbUtil.marshalToXmlString(error, ErrorRepresentation.class).getBytes());
			out.close();
			//return;
		}

		return;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		processRequest(request, response);
	}

}
