/**
 * Copyright (c) 2013 Personnel Decisions International, Inc. a Korn/Ferry company
 * All rights reserved.
 */
package com.pdinh.rs.helper;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.POIXMLProperties;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Class to create a spreadsheet containing selected viaEDGE data
 */
public class ViaEdgeExtractHelper {

	//
	// Static variables
	//
	static private final Object[][] columnMap = new Object[][] {
			(new Object[] { "Individual", "column1Value", Cell.CELL_TYPE_STRING }),
			(new Object[] { "Overall (Low)", "column2LowValue", Cell.CELL_TYPE_NUMERIC }),
			(new Object[] { "Overall Score", "column2Value", Cell.CELL_TYPE_NUMERIC }),
			(new Object[] { "Overall (High)", "column2HighValue", Cell.CELL_TYPE_NUMERIC }),
			(new Object[] { "Mental Agility", "column3Value", Cell.CELL_TYPE_NUMERIC }),
			(new Object[] { "People Agility", "column4Value", Cell.CELL_TYPE_NUMERIC }),
			(new Object[] { "Change Agility", "column5Value", Cell.CELL_TYPE_NUMERIC }),
			(new Object[] { "Results Agility", "column6Value", Cell.CELL_TYPE_NUMERIC }),
			(new Object[] { "Self-Awareness", "column7Value", Cell.CELL_TYPE_NUMERIC }),
			(new Object[] { "Confidence Bar", "column8ColorValue", Cell.CELL_TYPE_STRING }) };

	static private CellStyle titleCellStyle = null;

	//
	// Static methods
	//
	public static Workbook createGroupScoreExtractExcel(List<Map<String, Object>> scores, String watermark) {
		Workbook wb = new XSSFWorkbook();

		POIXMLProperties xmlProps = ((POIXMLDocument) wb).getProperties();
		POIXMLProperties.CoreProperties coreProps = xmlProps.getCoreProperties();
		coreProps.setTitle(watermark);

		Sheet s = wb.createSheet();

		/* Titles */
		titleCellStyle = wb.createCellStyle();
		Font boldFont = wb.createFont();
		boldFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		titleCellStyle.setFont(boldFont);

		int rr = 0;
		Row titleRow = s.createRow(rr++);

		// Put out the heading row
		for (int i = 0; i < columnMap.length; i++) {
			// createCellInRow(titleRow, i, columnMap[i][0], titleCellStyle);
			createCellInRow(titleRow, i, columnMap[i][0], Cell.CELL_TYPE_STRING, titleCellStyle);
		}

		// Put out the data...
		// For each row (ppt)...
		for (Map<String, Object> rowData : scores) {
			// Create the row
			Row wbRow = s.createRow(rr++);
			// Loop through the column map data...
			for (int i = 0; i < columnMap.length; i++) {
				String key = (String) columnMap[i][1];
				// get the data
				Object dat = rowData.get(key);
				createCellInRow(wbRow, i, dat, (Integer) columnMap[i][2]);
			}

		}

		return wb;
	}

	/**
	 * Convenience method to create a cell in a row with a style as a parameter
	 * 
	 * @param row
	 * @param idx
	 * @param value
	 * @param cellStyle
	 * @return
	 */
	private static Cell createCellInRow(Row row, int idx, Object value, int cellType, CellStyle cellStyle) {
		Cell cell = createCellInRow(row, idx, value, cellType);
		cell.setCellStyle(cellStyle);
		return cell;
	}

	/**
	 * Method to create a cell in a row. Uses default styling.
	 * 
	 * @param row
	 * @param idx
	 * @param value
	 * @return
	 */
	private static Cell createCellInRow(Row row, int idx, Object value, int cellType) {
		Cell cell = row.createCell(idx);
		if (value == null || value.equals("&#160;") || value.equals("NaN")) {
			cellType = Cell.CELL_TYPE_BLANK;
		}
		cell.setCellType(cellType);

		switch (cellType) {
		case Cell.CELL_TYPE_STRING:
			// Special handling for the AVERAGE confidence bar value
			cell.setCellValue(String.valueOf(value));
			break;
		case Cell.CELL_TYPE_NUMERIC:
			// This is sort of ugly... can we refactor it?
			if (value instanceof Number) {
				cell.setCellValue((Double) value);
			} else if (value instanceof String) {
				// try to convert it... if it works, good. if not, put out a
				// string
				String val = ((String) value).trim();
				// Is it an integer?
				if (StringUtils.isNumeric(val)) {
					// yes... convert it
					long ll = Long.parseLong(val);
					cell.setCellValue(ll);
				} else if (StringUtils.contains(val, '.')) {
					// Assume it is a floating point number
					double dd = Double.parseDouble(val);
					cell.setCellValue(dd);
				} else {
					// Assume not a number... put it out as a string
					cell.setCellType(Cell.CELL_TYPE_STRING);
					cell.setCellValue((String) value);
				}
			}
			break;
		case Cell.CELL_TYPE_BLANK:
			// Do nothing (no content)
			break;
		default:
			// Unrecognized data type... try to put in a string
			cell.setCellType(Cell.CELL_TYPE_STRING);
			cell.setCellValue(String.valueOf(value));
			break;
		}

		return cell;
	}
}
