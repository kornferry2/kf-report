package com.pdinh.rs.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.project.generic.GenericPipelinePublisher;
import com.pdinh.project.generic.GenericCalculationPublisher;

@WebServlet("/PipelinePublisher")
public class PipelinePublisher extends HttpServlet{	
	
	private static final long serialVersionUID = 1L;

	final static Logger logger = LoggerFactory.getLogger(PipelinePublisher.class);

	//
	// Beans and resources
	//

	@EJB
	private GenericPipelinePublisher genericPipelinePublisher;

	@EJB
	private GenericCalculationPublisher genericCalculationPublisher;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PipelinePublisher() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * process - Does the work for the servlet. It turns out that it is merely a
	 * wrapper around an instance of the ReportRequestResponseDao bean that does
	 * the heavy lifting
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @throws JAXBException 
	 */
	private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JAXBException {
		boolean ok = true;
		// Get the parameters
		String pipeline = request.getParameter("pipeline");
		String manifest = request.getParameter("manifest");
		String version = request.getParameter("version");
		String type= request.getParameter("type");
		Boolean debug = Boolean.valueOf(request.getParameter("debug")); 

		// Check them for null
		if(pipeline != null && manifest != null && debug != null && version.equals("1")){
			System.out.println("Calling v1 calculation pipeline!");
			genericPipelinePublisher.publishPipeline(pipeline, manifest, debug);
		}else if(pipeline != null && manifest == null && debug != null && version.equals("1")){
			System.out.println("Calling v1 calculation pipeline");
			genericPipelinePublisher.publishPipeline(pipeline, debug);
		}else if(pipeline != null && manifest != null && version.equals("2")){
			System.out.println("Calling v2 calculation pipeline");
			genericCalculationPublisher.publishPipeline(pipeline, manifest, type, false);
		}else{
			logger.info("Invalid parameter(s);  pipeline={}, manifest={}, version={}, debug={}", pipeline, manifest, version, debug);
			ok = false;
		}

		response.setContentType("text/html;charset=UTF-8");

		PrintWriter pw = response.getWriter();
		pw.print("<html>");
		pw.print("<head>");
		pw.print("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
		pw.print("</head>");	
		pw.print("<font face=\"verdana\" size=\"2\">");
		pw.print(ok ? ""+pipeline+" has successfully published! " : "Fail to publish "+pipeline);
		pw.print("<form name=\"theForm\" method=\"POST\" action=\"/reportweb/faces/pipelinePublisher.jsp\">");
		pw.print("<p>");
		pw.print("<input type = \"submit\" value = \"Go Back\"/>");
		pw.print("</p>");
		pw.print("</form>");
		pw.print("</body>");
		pw.print("</html>"); 	
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			process(request, response);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		try {
			process(request, response);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
