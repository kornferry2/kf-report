<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" indent="yes" encoding="UTF-8" />

	<xsl:template match="/Table">
     <keymaps>
       <xsl:apply-templates select="Row" />
     </keymaps>
	</xsl:template>

	<xsl:template match="Row">

     <keyMap id="devSuggB">
       <key id="devSugg"><xsl:value-of select="Cell[1]/Data" /></key>
       <key id="Ia1DisplayText"><xsl:value-of select="Cell[3]/Data" /></key>
       <key id="Ia1Id"><xsl:value-of select="Cell[2]/Data" /></key>
       <key id="Ia2DisplayText"><xsl:value-of select="Cell[6]/Data" /></key>
       <key id="Ia2Id"><xsl:value-of select="Cell[5]/Data" /></key>
       <key id="Ia3DisplayText"><xsl:value-of select="Cell[8]/Data" /></key>
       <key id="Ia3Id"><xsl:value-of select="Cell[7]/Data" /></key>
       <key id="Ia4DisplayText"><xsl:value-of select="Cell[10]/Data" /></key>
       <key id="Ia4Id"><xsl:value-of select="Cell[9]/Data" /></key>
       <key id="Ia5DisplayText"><xsl:value-of select="Cell[12]/Data" /></key>
       <key id="Ia5Id"><xsl:value-of select="Cell[11]/Data" /></key>
       <key id="Ia6DisplayText"><xsl:value-of select="Cell[14]/Data" /></key>
       <key id="Ia6Id"><xsl:value-of select="Cell[13]/Data" /></key>
     </keyMap>

   </xsl:template>
</xsl:stylesheet>
