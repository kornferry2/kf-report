Unit testing LVA html and PDF generation
--------------------------------------------

First, you'll have to edit the CMD files to set the correct workspace path.

Report generation is a two-step process.
1. Generate HTML
2. Generate PDF

The files lvarar.cmd and lvadar.cmd generate the HTML for the Readiness and Development reports respectively.

The files lvararpdf.cmd and lvadarpdf.cmd generate the PDF for the Readiness and Development reports respectively.

Both reports generate from the file LvaMll.xml located in this folder. 
This is an LVA Report Content Model (RCM) which was hand-generated and reflects the current RCM schema.

To generate the Readiness PDF:

1. run lvarar.cmd (first edit file)

2. refresh WebContent in Eclipse so that it knows about the updated xhtml file

3. run lvararpdf.cmd (first edit file)

The second CMD will open the generated PDF

For the Development report, use the same steps but with lvadar.cmd and lvadarpdf.cmd


There are also unit test CMD files for the cover pages in lva_rar_cover.cmd and lva_dar_cover.cmd