::
::  coaching.cmd: generate coaching plan report
::
SET WORKSPACE=C:\9h_Source\workspace\
SET JARS=%WORKSPACE%\ReportServer-ear\EarContent\lib
SET XSLFOLDER=%WORKSPACE%\report-ejb\src\main\resources\projects\viaedge
SET OUTFOLDER=%WORKSPACE%\ReportServer-web\WebContent\test
java -jar %JARS%\saxon9he.jar -xi -s:viaEdgeCoachingRcm.xml -xsl:%XSLFOLDER%\individual_coaching_report.xsl -o:%OUTFOLDER%\veCoachingTEST.xhtml subtype=coaching genmode=pdf baseurl=http://kyamry:18080/reportweb

