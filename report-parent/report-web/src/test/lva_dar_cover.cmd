SET WORKSPACE=C:\Users\chris\workspace-indigo
SET TESTPATH=%WORKSPACE%\ReportServer-web\test

wkhtmltopdf --quiet --dpi 600 --print-media-type --disable-smart-shrinking --no-pdf-compression -s Letter --margin-top 10 --margin-bottom 10 --margin-left 10 --margin-right 10 --header-spacing 0 --footer-spacing 0 "http://localhost:8080/ReportServer-web/core/xhtml/cover.xhtml?productname=Virtual Assessment&reportname=Development Action Report&reportsubhead=Mid-level Leader&participant=Jonathan Douglas&client=Global Corporation&project=Mid-level Leader Virtual Assessment&assdate=23 September 2012" - > lva_dar_cover.pdf

start acrord32.exe /A "navpanes=0=OpenActions" "%TESTPATH%\lva_dar_cover.pdf"
