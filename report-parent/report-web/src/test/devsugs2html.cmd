::
::  content2html.cmd: generate html to QA parameterized content
SET WORKSPACE=C:\Users\chris\workspace-indigo
SET JARS=%WORKSPACE%\ReportServer-ear\EarContent\lib
SET CONTENTFOLDER=%WORKSPACE%\ReportContentRepository\content\projects\internal\gl-gps\mll
java -jar %JARS%\saxon9he.jar -xi -s:%CONTENTFOLDER%\behaviorContent.xml -xsl:devsugs2html.xsl -o:DevsugContent.xhtml

