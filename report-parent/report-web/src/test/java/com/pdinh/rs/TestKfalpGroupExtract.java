package com.pdinh.rs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringEscapeUtils;

/**
 * Test class for the KfalrFroupReportExtract servlet. All test parameters are
 * internal to the program.
 */
public class TestKfalpGroupExtract {
	private static final String REPORT_SERVER_BASE_URL = "http://kbeukelm2:18080/reportweb";

	public static void main(String[] args) {
		HttpURLConnection conn = null;

		String url = REPORT_SERVER_BASE_URL + "/KfalpGroupExtractServlet";
		System.out.println("Got urlTarget: " + url);

		try {
			URL currUrl = new URL(url);
			conn = (HttpURLConnection) currUrl.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
			conn.setRequestProperty("Accept-Charset", "UTF-8");
			conn.setDoOutput(true);
			// Add the parameters to the url
			StringBuffer urlParameters = new StringBuffer();
			String key = "pptList";

			// There is commented out code below. This has been left in on
			// purpose to allow ease of testing.

			// These are dev
			String val = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<participants>\n"
					+ "<participant participantId=\"374182\" projectId=\"921\" projectName=\"A1 KF Asmt of Potential\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"demo:1|chq:1|piq:1|kfp:1|rv2:1\"/>\n"
					+ "<participant participantId=\"386250\" projectId=\"921\" projectName=\"A1 KF Asmt of Potential\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:2|rv2:1\"/>\n"
					+ "<participant participantId=\"384849\" projectId=\"921\" projectName=\"A1 KF Asmt of Potential\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
					+ "<participant participantId=\"385821\" projectId=\"921\" projectName=\"A1 KF Asmt of Potential\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:2|rv2:1\"/>\n"
					+ "</participants>";

			// These are QA
			// Sort test
			// String val =
			// "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<participants>\n"
			// +
			// "<participant participantId=\"401905\" projectId=\"2372\" projectName=\"-- QA AoLP SCORING / REPORTING v.1\" langCode=\"\" defaultProjectTargetLevelIndex=\"2\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"401915\" projectId=\"2372\" projectName=\"-- QA AoLP SCORING / REPORTING v.1\" langCode=\"\" defaultProjectTargetLevelIndex=\"2\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"401916\" projectId=\"2372\" projectName=\"-- QA AoLP SCORING / REPORTING v.1\" langCode=\"\" defaultProjectTargetLevelIndex=\"2\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"401917\" projectId=\"2372\" projectName=\"-- QA AoLP SCORING / REPORTING v.1\" langCode=\"\" defaultProjectTargetLevelIndex=\"2\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"401918\" projectId=\"2372\" projectName=\"-- QA AoLP SCORING / REPORTING v.1\" langCode=\"\" defaultProjectTargetLevelIndex=\"2\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"401919\" projectId=\"2372\" projectName=\"-- QA AoLP SCORING / REPORTING v.1\" langCode=\"\" defaultProjectTargetLevelIndex=\"2\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"401920\" projectId=\"2372\" projectName=\"-- QA AoLP SCORING / REPORTING v.1\" langCode=\"\" defaultProjectTargetLevelIndex=\"2\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"401921\" projectId=\"2372\" projectName=\"-- QA AoLP SCORING / REPORTING v.1\" langCode=\"\" defaultProjectTargetLevelIndex=\"2\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"401924\" projectId=\"2372\" projectName=\"-- QA AoLP SCORING / REPORTING v.1\" langCode=\"\" defaultProjectTargetLevelIndex=\"2\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"401925\" projectId=\"2372\" projectName=\"-- QA AoLP SCORING / REPORTING v.1\" langCode=\"\" defaultProjectTargetLevelIndex=\"2\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// + "</participants>";

			// // Sort II (QA)
			// String val =
			// "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<participants>\n"
			// +
			// "<participant participantId=\"411206\" projectId=\"2706\" projectName=\"KFAP VERSION 2...\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"411209\" projectId=\"2372\" projectName=\"KFAP VERSION 2...\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:2|rv2:2\"/>\n"
			// +
			// "<participant participantId=\"410973\" projectId=\"2706\" projectName=\"No Ravens\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:2|rv2:2\"/>\n"
			// +
			// "<participant participantId=\"402145\" projectId=\"2538\" projectName=\"KFAP &amp; SCORING\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:2|rv2:2\"/>\n"
			// +
			// "<participant participantId=\"402051\" projectId=\"2538\" projectName=\"KFAP &amp; SCORING\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:2|rv2:2\"/>\n"
			// +
			// "<participant participantId=\"402007\" projectId=\"2538\" projectName=\"KFAP &amp; SCORING\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:2|rv2:2\"/>\n"
			// +
			// "<participant participantId=\"402116\" projectId=\"2538\" projectName=\"KFAP &amp; SCORING\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:2|rv2:2\"/>\n"
			// +
			// "<participant participantId=\"402080\" projectId=\"2538\" projectName=\"KFAP &amp; SCORING\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:2|rv2:2\"/>\n"
			// +
			// "<participant participantId=\"402075\" projectId=\"2538\" projectName=\"KFAP &amp; SCORING\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:2|rv2:2\"/>\n"
			// +
			// "<participant participantId=\"402036\" projectId=\"2538\" projectName=\"KFAP &amp; SCORING\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:2|rv2:2\"/>\n"
			// +
			// "<participant participantId=\"402120\" projectId=\"2538\" projectName=\"KFAP &amp; SCORING\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:2|rv2:2\"/>\n"
			// +
			// "<participant participantId=\"402104\" projectId=\"2538\" projectName=\"KFAP &amp; SCORING\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:2|rv2:2\"/>\n"
			// + "</participants>";

			// // Sort III (QA)
			// String val =
			// "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<participants>\n"
			// +
			// "<participant participantId=\"411896\" projectId=\"2376\" projectName=\"Bogus\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"409847\" projectId=\"2706\" projectName=\"Bogus\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"410807\" projectId=\"2706\" projectName=\"Bogus\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"410833\" projectId=\"2706\" projectName=\"Bogus\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"411031\" projectId=\"2706\" projectName=\"Bogus\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"411095\" projectId=\"2706\" projectName=\"Bogus\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"411217\" projectId=\"2706\" projectName=\"Bogus\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"411228\" projectId=\"2706\" projectName=\"Bogus\" langCode=\"\" defaultProjectTargetLevelIndex=\"1\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// + "</participants>";

			// // V1/V2 test (QA)
			// String val =
			// "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<participants>\n"
			// +
			// "<participant participantId=\"401905\" projectId=\"2372\" projectName=\"-- QA AoLP SCORING / REPORTING v.1\" langCode=\"\" defaultProjectTargetLevelIndex=\"6\" courseVersionMap=\"kfp:1|rv2:1\"/>\n"
			// +
			// "<participant participantId=\"412254\" projectId=\"2372\" projectName=\"-- QA AoLP SCORING / REPORTING v.1\" langCode=\"\" defaultProjectTargetLevelIndex=\"6\" courseVersionMap=\"kfp:2|rv2:2\"/>\n"
			// + "</participants>";

			urlParameters.append(key + "=" + URLEncoder.encode(val, "UTF-8"));
			urlParameters.append("&");
			key = "langCode";
			val = "en";
			urlParameters.append(key + "=" + URLEncoder.encode(val, "UTF-8"));
			urlParameters.append("&");
			key = "type";
			val = "ALP_GROUP_EXTRACT";
			urlParameters.append(key + "=" + URLEncoder.encode(val, "UTF-8"));
			urlParameters.append("&");
			// ----------
			key = "rptTitle";
			val = StringEscapeUtils.escapeHtml4("M & M"); // Testing &
			// val = StringEscapeUtils.escapeHtml4("Türk"); // Testing umlauts
			// val = "Group Bogus Test Name";
			// val = "   ";
			urlParameters.append(key + "=" + URLEncoder.encode(val, "UTF-8"));
			urlParameters.append("&");
			// ----------

			// Code and idx must remain in sync, below. Here is the secret
			// decoder ring.
			// code target_level_index
			// KFP_CEO 1
			// KFP_SEA 2
			// KFP_SLL 3
			// KFP_BUL 4
			// KFP_FL 5
			// KFP_MLL 6
			// KFP_FLL 7
			// KFP_TL 8
			// KFP_IC 9
			key = "targ";
			val = "1";
			urlParameters.append(key + "=" + URLEncoder.encode(val, "UTF-8"));
			urlParameters.append("&");
			key = "targCode";
			val = "KFP_CEO";
			urlParameters.append(key + "=" + URLEncoder.encode(val, "UTF-8"));
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
			writer.write(urlParameters.toString());
			writer.flush();
		} catch (MalformedURLException e) {
			System.out.println("Maformed URL: " + url + ".  msg=" + e.getMessage());
			return;
		} catch (IOException e) {
			System.out.println("I/O Exception:  msg=" + e.getMessage());
			return;
		}

		// Get the generated report as a stream
		try {
			String fileName = null;
			InputStream inp = conn.getInputStream();
			String disposition = conn.getHeaderField("Content-Disposition");
			String contentType = conn.getContentType();
			int contentLength = conn.getContentLength();
			int index = disposition.indexOf("filename=");
			if (index > 0) {
				fileName = disposition.substring(index + 9);
			}
			System.out.println("Disp=-->" + disposition + "<--, filename=" + fileName);
			System.out.println("Content:  type=" + contentType + ", length=" + contentLength);
			String saveFilePath = "C:\\test\\" + fileName;
			File file = new File("C:\\test\\" + fileName);
			System.out.println("Save filename=" + saveFilePath);
			if (!file.exists()) {
				file.createNewFile();
			}

			FileOutputStream outputStream = new FileOutputStream(saveFilePath);
			int bytesRead = -1;
			byte[] buffer = new byte[4096];
			while ((bytesRead = inp.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}

			outputStream.close();
			inp.close();
		} catch (IOException ioe) {
			System.out.println("I/O Exception:  msg=" + ioe.getMessage());
		}
	}
}
