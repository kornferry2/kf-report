package com.pdinh.rs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;
import java.util.Locale;
//import java.util.Properties;
import java.util.ResourceBundle;
//import java.util.Map;

//import javax.naming.Context;
//import javax.naming.InitialContext;
//import javax.naming.NamingException;

import com.pdinh.pipeline.generator.vo.Participant;
import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.pipeline.generator.vo.Stage;

/**
 * TestRgrRestResource is a class that can be used to test the
 * ReportGenerationRequestResource RESTful service. It processes as a POST. It
 * is written as a "headless" Java app and takes as input parameters the full
 * pathname of the raw data RGR xml file and the file name (no path, no ',xml'
 * suffix) of the manifest to be used to score the data in the file. In Eclipse
 * this can easily be set up as a run configuration with two parameters.
 * 
 * This could be generalized to test any even further by passing in the URL of
 * the RESTful service you wish to test.
 */
public class TestRgrRestResource {

	// For now, always format. In future, we could make this a passed parameter.
	private static boolean OUTPUT_FORMATTED_DEFAULT = true;

	/**
	 * The main method for the app. It checks the parameters, reads in the data
	 * file, and performs the access to the designated page. It could be
	 * simplified by abstracting out more of the functionality.
	 * 
	 * Uses marshal and unmarshal routines from TestUtils
	 * 
	 * If we want to generalize we could create the whole RESTful URL in the
	 * properties file with parameter names (".../{parm}/...). We could then
	 * pass in a bunch of key/value pairs to be used as replacement values. The
	 * formatting option then either has to be there or we can do some extra
	 * work in reading the parameters (but it wouldn't be as reliable in the
	 * checking)
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		String filePath = null;
		String manName = null;
		String fmtFlag = null;
		boolean doFormat = OUTPUT_FORMATTED_DEFAULT;

		// Check params
		if (args.length == 0) {
			System.out.println(" No parameters were detected.");
			System.out.println(parmDescString());
			return;
		}
		if ((args.length < 2 || args.length > 3)) {
			System.out.println("Incorrect number of parameters (" + args.length + ")");
			System.out.println(parmDescString());
			return;
		} else {
			filePath = args[0];
			manName = args[1];
			if (args.length == 3) {
				fmtFlag = args[2].toLowerCase();
				if (fmtFlag.equals("t") || fmtFlag.equals("true") || fmtFlag.equals("1")) {
					doFormat = true;
				} else if (fmtFlag.equals("f") || fmtFlag.equals("false") || fmtFlag.equals("0")) {
					doFormat = false;
				} else {
					// Probably redundant (but thorough)
					doFormat = OUTPUT_FORMATTED_DEFAULT;
				}
			}
			System.out.println("filePath=" + filePath + ", manName=" + manName + ", fmtFlag=" + fmtFlag);
		}

		File fileObj = new File(filePath);

		// Read the input file
		String inp = fetchXmlFile(fileObj);
		System.out.println(inp);

		// Get the instrument
		String instCode = getInstCode(inp);
		if (instCode == null) {
			System.out.println("Unable to fetch instrument code from " + filePath);
			return;
		}
		System.out.println("instCode=" + instCode);

		// Set up for the restful service
		// Two ways to get the service name. A) From JNDI property, or B) from
		// .properties file. See comments, below.
		// Stayed with simple (B) for now as all of the elements could be
		// checked in with no additional manipulation of the environment

		String urlStr = null;

		// ----------------------------------------------------------------
		// // Method A: Get from JNDI
		// // In order to implement this you need to:
		// // -- Add Glassfish3/glassfish/glassfish/lib/appserv-rt.jar to the
		// // classpath
		// // -- Add the parameter "test.rgrUrl" to
		// // application/report/config_properties with a value of the URL
		// (without
		// // parameters). example URL --
		// // "http://kbeukelm2:18080/reportrest/jaxrs/reportGenerationRequest/"
		// // Advantages - Utilizes the same infrastructure
		// // Disadvantages - Slow, throws an error nattering about the Derby
		// JDBC
		// // driver, requires change to the sun-resources.xml file
		// ----------------------------------------------------------------
		// try {
		// InitialContext ic = new InitialContext();
		// Object o = ic.lookup("application/report/config_properties");
		// // Object o = ic.lookup("application/report");
		// // // Properties props = (Properties) o;
		// urlStr = (String) ((Map) o).get("test.rgrUrl");
		// } catch (NamingException e2) {
		// System.out.println("Unable to fetch test Url parameter.  msg=" +
		// e2.getMessage());
		// return;
		// }

		// ----------------------------------------------------------------
		// Method B: Get from properties file
		// In order to implement this you need to create a properties file
		// (done).
		// It will contain the following line:
		// test.rgrUrl=<URL name> where <URL name> is the value of the URL
		// (without parameters and without quotes). example URL --
		// "http://kbeukelm2:18080/reportrest/jaxrs/reportGenerationRequest/"
		// Advantages - Simple
		// Disadvantages - May need to fudge with the .properties file as we
		// move from
		// environment to environment.
		// ----------------------------------------------------------------
		// Might be an issue here... sometimes an error comes back that says it
		// can't find the bundle. If I change the name of the test.properties
		// (or test_en.properties or test_en_us.properties) it seems to work
		// ----------------------------------------------------------------
		ResourceBundle rb = ResourceBundle.getBundle("com.pdinh.rs.test", Locale.getDefault());
		urlStr = rb.getString("test.rgrUrl");

		// Add the parameters - "{instCode}/{file}" in the service definition
		urlStr += instCode;
		urlStr += "/";
		urlStr += manName;

		URL url = null;
		try {
			url = new URL(urlStr);
		} catch (MalformedURLException e) {
			System.out.println("Malformed URL:  " + urlStr);
			return;
		}
		HttpURLConnection urlConnection = null;
		try {
			urlConnection = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			System.out.println("Unable to open URL:  " + urlStr);
			return;
		}
		try {
			urlConnection.setRequestMethod("POST");
		} catch (ProtocolException e) {
			System.out.println("Protocol exception on URL:  " + urlStr);
			return;
		}
		urlConnection.setRequestProperty("Content-Type", "application/xml");
		urlConnection.setDoOutput(true);

		// Set up to write the POST data to the page (connection actually)
		OutputStreamWriter writer = null;
		try {
			writer = new OutputStreamWriter(urlConnection.getOutputStream());
		} catch (IOException e) {
			System.out.println("Unable to fetch writer:  " + urlStr);
			return;
		}
		try {
			writer.write(inp);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			System.out.println("Error utilizing writer:  " + urlStr);
			return;
		}

		// Prepare to invoke the service and retrieve the data
		String line;
		InputStreamReader isr = null;
		try {
			isr = new InputStreamReader(urlConnection.getInputStream());
		} catch (IOException e) {
			System.out.println("Error getting stream reader:  msg=" + e.getMessage());
			try {
				System.out.println("Status=" + urlConnection.getResponseCode());
			} catch (IOException e1) {
				System.out.println("Unable to fetch stats:  msg=" + e1.getMessage());
			}
			return;
		}

		String outStr = "";
		try {
			BufferedReader reader = new BufferedReader(isr);
			while ((line = reader.readLine()) != null) {
				// System.out.println("testPost: " + line);
				outStr += line;
			}
			reader.close();
		} catch (IOException e) {
			System.out.println("Error utilizing buffered reader:  msg=" + e.getMessage());
			return;
		}

		if (doFormat && outStr.length() > 1) {
			// Unmarshal then marshal the output to get pretty formatting
			ReportGenerationRequest tmp = (ReportGenerationRequest) TestUtils.unmarshal(outStr,
					ReportGenerationRequest.class);
			outStr = TestUtils.marshalToXmlString(tmp, ReportGenerationRequest.class);
		}

		System.out.println("Output=" + outStr);
	}

	/**
	 * Method to display appropriate parameters for DB type request.
	 * 
	 * Note that there is no closing new-line character; assumes that this
	 * method is called in a println() or that the user will handle the closing
	 * new-line himself.
	 */
	private static String parmDescString() {
		String str = "  This program requires arguments as follows:\n";
		str += "    file path  - The The path to the XML file to be scored.\n";
		str += "    manifest name - The name of the manifest to be used in testing.\n";
		str += "    format output - Put out a formatted XML output string.\n"
				+ "                    Valid values are:  't', 'true', '1' to do the formatting,\n"
				+ "                                       'f', 'false', or '0' for no formatting.\n"
				+ "                    Case is ignored ('t' and 'T' are equally valid)."
				+ "        The absence of a 3rd parameter or an invalid value there reverts to the default (currently \""
				+ OUTPUT_FORMATTED_DEFAULT + "\").";

		return str;
	}

	/**
	 * fetchXmlFile - Get a responses XML file from the file system DEBUG
	 * ROUTINE ONLY.
	 */
	private static String fetchXmlFile(File file) {
		String xml = "";
		FileReader fr = null;
		BufferedReader br = null;
		String path = file.getName();

		try {
			// Get the output string
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			String s;
			while ((s = br.readLine()) != null) {
				xml += s;
			}

			return xml;
		} catch (FileNotFoundException e) {
			System.out.println("File not found:  " + path);
			return null;
		} catch (IOException e) {
			System.out.println("Error reading " + path);
			return null;
		} finally {
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException e) { /* Swallow the exception */
			}
			try {
				if (fr != null) {
					fr.close();
				}
			} catch (IOException e) { /* Swallow the exception */
			}
		}
	}

	/**
	 * Get the first inst code In real life, this would probably get an array of
	 * strings, one for each instrument in the XML, but we are using this
	 * primarily for aone-at-a-time testing
	 * 
	 * XML as a string in, 1st instrument found out
	 */
	private static String getInstCode(String xml) {
		String ret = null;

		// Unmarshall it so we can get the instrument list... need it to get the
		// manifest. Seems kinda dumb, but...

		// Unmarshal
		ReportGenerationRequest rgr = (ReportGenerationRequest) TestUtils.unmarshal(xml, ReportGenerationRequest.class);

		// Now find the first instrument in the first participant (for testing
		// only)
		Participant ppt = rgr.getParticipant().get(0);
		if (ppt == null) {
			System.out.println("No participant found in XML:  file=" + xml);
			return null;
		}

		List<Stage> stages = ppt.getStage();
		// Look for the "raw" stage
		for (Stage st : stages) {
			if (st.getId().equals("raw") || st.getId().equals("responses")) {
				ret = st.getInstrument().get(0).getId();
				break;
			}
		}
		if (ret == null)
			System.out.println("No instrument id found via testing code");

		return ret;
	}

}
