/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdinh.rs;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.w3c.dom.Node;

public class TestUtils {
	//
	// Static data.
	//
	static public String PART_ID = "ParticipantId";
	static public String INST_CODE = "InstrumentCode";
	static public String INP_XML = "InputXML";

	//
	// Static methods.
	//

	public static Object unmarshal(Node node, Class<?> clazz) {
		JAXBContext ctx = null;
		Object obj = null;
		try {
			ctx = JAXBContext.newInstance(clazz);
			Unmarshaller u = ctx.createUnmarshaller();
			obj = u.unmarshal(node);
		} catch (JAXBException e) {
			e.printStackTrace();
		}

		return obj;
	}

	public static Object unmarshal(String xml, Class<?> clazz) {
		JAXBContext ctx = null;
		Object obj = null;
		try {
			ctx = JAXBContext.newInstance(clazz);
			Unmarshaller u = ctx.createUnmarshaller();
			obj = u.unmarshal(new StringReader(xml));
		} catch (JAXBException e) {
			e.printStackTrace();
		}

		return obj;
	}

	public static String marshalToXmlString(Object object, Class<?> clazz) {
		JAXBContext ctx = null;
		String xml = "";
		try {
			ctx = JAXBContext.newInstance(clazz);
			Marshaller m = ctx.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter sw = new StringWriter();
			m.marshal(object, sw);
			xml = sw.toString();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return xml;
	}

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//
	//
	//

}
