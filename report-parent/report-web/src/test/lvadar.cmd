::
::  lvadar.cmd: generate lva development report
SET WORKSPACE=C:\Users\chris\workspace-indigo
SET JARS=%WORKSPACE%\ReportServer-ear\EarContent\lib
SET XSLFOLDER=%WORKSPACE%\ReportServer-ejb\ejbModule\com\pdinh\resource\projects\lva\mll
SET OUTFOLDER=%WORKSPACE%\ReportServer-web\WebContent\test
::java -jar %JARS%\saxon9he.jar -xi -s:LvaMll.xml -xsl:%XSLFOLDER%\lva_report.xsl -o:%OUTFOLDER%\LvaMllDar.xhtml subtype=development genmode=pdf baseurl=http://localhost:8080/ReportServer-web
java -jar %JARS%\saxon9he.jar -xi -s:LvaMll_new.xml -xsl:%XSLFOLDER%\lva_report.xsl -o:%OUTFOLDER%\LvaMllDar.xhtml subtype=development genmode=pdf baseurl=http://localhost:8080/ReportServer-web

