<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns="http://www.w3.org/1999/xhtml" 
  xmlns:f="http://java.sun.com/jsf/core"
  xmlns:h="http://java.sun.com/jsf/html"
  xmlns:ui="http://java.sun.com/jsf/facelets"
  xmlns:p="http://primefaces.org/ui"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xalan="http://xml.apache.org/xslt"
  xmlns:svg="http://www.w3.org/2000/svg"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="xsl xalan xs"
>

  <!--
      http://stackoverflow.com/questions/9538619/xslt-refuses-to-write-doctype-declaration
  -->
  <xsl:output method="xml" indent="yes" encoding="UTF-8"
              doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />
  
  <xsl:param name="gender" select="''" />

  <xsl:variable name="heshe">
    <xsl:choose>
      <xsl:when test="$gender = 'MALE'">he</xsl:when>
      <xsl:when test="$gender = 'FEMALE'">she</xsl:when>
      <xsl:when test="$gender = 'YOU'">you</xsl:when>
      <xsl:otherwise>he/she</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="Heshe">
    <xsl:choose>
      <xsl:when test="$gender = 'MALE'">He</xsl:when>
      <xsl:when test="$gender = 'FEMALE'">She</xsl:when>
      <xsl:when test="$gender = 'YOU'">You</xsl:when>
      <xsl:otherwise>He/she</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="hisher">
    <xsl:choose>
      <xsl:when test="$gender = 'MALE'">his</xsl:when>
      <xsl:when test="$gender = 'FEMALE'">her</xsl:when>
      <xsl:when test="$gender = 'YOU'">your</xsl:when>
      <xsl:otherwise>his/her</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="Hisher">
    <xsl:choose>
      <xsl:when test="$gender = 'MALE'">His</xsl:when>
      <xsl:when test="$gender = 'FEMALE'">Her</xsl:when>
      <xsl:when test="$gender = 'YOU'">Your</xsl:when>
      <xsl:otherwise>His/her</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="himher">
    <xsl:choose>
      <xsl:when test="$gender = 'MALE'">him</xsl:when>
      <xsl:when test="$gender = 'FEMALE'">her</xsl:when>
      <xsl:when test="$gender = 'YOU'">your</xsl:when>
      <xsl:otherwise>him/her</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>


  <xsl:variable name="isare">
    <xsl:choose>
      <xsl:when test="$gender = 'YOU'">are</xsl:when>
      <xsl:otherwise>is</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="hashave">
    <xsl:choose>
      <xsl:when test="$gender = 'YOU'">have</xsl:when>
      <xsl:otherwise>has</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="verbend">
    <xsl:choose>
      <xsl:when test="$gender = 'YOU'"></xsl:when>
      <xsl:otherwise>s</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- these next are new and aren't yet used in apply-gender -->
  <xsl:variable name="does">
    <xsl:choose>
      <xsl:when test="$gender = 'YOU'">do</xsl:when>
      <xsl:otherwise>does</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- no default match -->
  <xsl:template match="*"/>

  <xsl:template match="/reportContent">
    <html>
      <body>
        <h1>Report Content</h1>
        <xsl:apply-templates select="keyMap" />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="keyMap">
    <h2>KeyMap <xsl:value-of select="@id" /></h2>
    <xsl:apply-templates select="key" />
  </xsl:template>

  <xsl:template match="key">
    <h3>Key <xsl:value-of select="@id" /></h3>
    <p>
      <xsl:if test="contains(.,'{')">
        <xsl:attribute name="style">color:darkred</xsl:attribute>
      </xsl:if>
      <xsl:call-template name="apply-gender">
        <xsl:with-param name="text" select="."/>
      </xsl:call-template>
    </p>
  </xsl:template>


  <!--
      Big nested string replace to turn placeholder variables 
      like {himher} into "him" or "her" or "you"
  -->

  <xsl:template name="apply-gender">
    <xsl:param name="text"/>
    <xsl:call-template name="replace-substring">
      <xsl:with-param name="original">
        <xsl:call-template name="replace-substring">
          <xsl:with-param name="original">
            <xsl:call-template name="replace-substring">
              <xsl:with-param name="original">
                <xsl:call-template name="replace-substring">
                  <xsl:with-param name="original">
                    <xsl:call-template name="replace-substring">
                      <xsl:with-param name="original">
                        <xsl:call-template name="replace-substring">
                          <xsl:with-param name="original">
                            <xsl:call-template name="replace-substring">
                              <xsl:with-param name="original">
                                <xsl:call-template name="replace-substring">
                                  <xsl:with-param name="original">
                                    <xsl:call-template name="replace-substring">
                                      <xsl:with-param name="original" select="$text"/>
                                      <xsl:with-param name="substring" select="'{does}'"/>
                                      <xsl:with-param name="replacement" select="$does"/>
                                    </xsl:call-template>
                                  </xsl:with-param>
                                  <xsl:with-param name="substring" select="'{s}'"/>
                                  <xsl:with-param name="replacement" select="$verbend"/>
                                </xsl:call-template>
                              </xsl:with-param>
                              <xsl:with-param name="substring" select="'{hashave}'"/>
                              <xsl:with-param name="replacement" select="$hashave"/>
                            </xsl:call-template>
                          </xsl:with-param>
                          <xsl:with-param name="substring" select="'{isare}'"/>
                          <xsl:with-param name="replacement" select="$isare"/>
                        </xsl:call-template>
                      </xsl:with-param>
                      <xsl:with-param name="substring" select="'{himher}'"/>
                      <xsl:with-param name="replacement" select="$himher"/>
                    </xsl:call-template>
                  </xsl:with-param>
                  <xsl:with-param name="substring" select="'{Hisher}'"/>
                  <xsl:with-param name="replacement" select="$Hisher"/>
                </xsl:call-template>
              </xsl:with-param>
              <xsl:with-param name="substring" select="'{hisher}'"/>
              <xsl:with-param name="replacement" select="$hisher"/>
            </xsl:call-template>
          </xsl:with-param>
          <xsl:with-param name="substring" select="'{heshe}'"/>
          <xsl:with-param name="replacement" select="$heshe"/>
        </xsl:call-template>
      </xsl:with-param>
      <xsl:with-param name="substring" select="'{Heshe}'"/>
      <xsl:with-param name="replacement" select="$Heshe"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="replace-substring">
    <xsl:param name="original"/>
    <xsl:param name="substring"/>
    <xsl:param name="replacement" select="''"/>
    <xsl:choose>
      <xsl:when test="contains($original, $substring)">
        <xsl:value-of select="substring-before($original, $substring)"/>
        <xsl:copy-of select="$replacement"/>
        <xsl:call-template name="replace-substring">
          <xsl:with-param name="original" select="substring-after($original, $substring)"/>
          <xsl:with-param name="substring" select="$substring"/>
          <xsl:with-param name="replacement" select="$replacement"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$original"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


</xsl:stylesheet>
