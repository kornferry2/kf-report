<viaEdgeIndividualSummaryReport lang="en-US">
    <reportTitle>Individual Summary Report</reportTitle>
    <participant>
        <firstName>James</firstName>
        <lastName>Aird</lastName>
    </participant>
    <reportDate>06 Feb 2014</reportDate>
    <serialNumber>785627</serialNumber>
    <copyright>Copyright &amp;#169; 2011-2013 Korn/Ferry International. All rights reserved.
                                                            </copyright>
    <introductionSection>
        <title>Introduction</title>
        <p1>An organization's success depends largely on its people, on talented
                                                                           individuals who apply a wide range of skills and competencies to help achieve organizational
                                                                           goals. The organizations that most effectively leverage their people are focused on assessing
                                                                           and differentiating talent so all employees can be effectively developed, engaged, and
                                                                           deployed. All talent is important, but not all talent is the same. High professionals, for
                                                                           instance, are those individuals who generally have deep technical expertise and are well suited
                                                                           to functional positions. They provide tremendous value to the organization, and their
                                                                           contribution is enhanced when they are developed, engaged, and deployed in particular ways. In
                                                                           contrast, high potentials are better suited for general management roles and benefit from
                                                                           different approaches to development, engagement, and deployment. The professional and potential
                                                                           dimensions are extremely important criteria on which to manage talent.
                                                            </p1>
        <p2>The key criterion that differentiates talent along the
                                                                           high-professional/high-potential continuum is Learning Agility. Learning agility is the ability
                                                                           and willingness to learn from experience and subsequently apply those lessons to perform
                                                                           successfully in new or first-time situations. Learning agility is primarily an indicator of
                                                                           adaptability rather than of intelligence. And although intelligence influences the ability to
                                                                           learn from a traditional perspective, learning agility is a different and distinct trait that
                                                                           is not significantly correlated with intelligence.
                                                            </p2>
        <p3>The viaEDGE&lt;sup&gt;®&lt;/sup&gt; Assessment is designed to measure an individual's work preferences and values, personality characteristics, life experiences, and work-related behaviors in order to assess and understand an individual's learning agility. In addition to assessing Overall Learning Agility, the following five facets of learning agility are measured:

                                                            </p3>
        <mentalDescriptionHeading>Working with Concepts and Ideas</mentalDescriptionHeading>
        <mentalDescription>The extent to which an individual embraces complexity,
                                             examines problems in unique and unusual ways, is inquisitive, and can
                                             make fresh connections between different concepts.</mentalDescription>
        <peopleDescriptionHeading>Working with and Relating to People</peopleDescriptionHeading>
        <peopleDescription>The degree to which one is open-minded toward others, enjoys
                                             interacting with a diversity of people, understands their unique
                                             strengths, interests, and limitations, and uses them effectively to
                                             accomplish organizational goals.</peopleDescription>
        <changeDescriptionHeading>Tolerance for Uncertainty and Ability to Lead Change</changeDescriptionHeading>
        <changeDescription>The extent to which an individual likes change, continuously
                                             explores new options and solutions, and is interested in leading
                                             organizational change efforts.</changeDescription>
        <resultsDescriptionHeading>Preference for How to Get Things Done</resultsDescriptionHeading>
        <resultsDescription>The degree to which an individual is motivated by challenge
                                             and can deliver results in first-time and/or tough situations through
                                             resourcefulness and by inspiring others.</resultsDescription>
        <selfAwareDescriptionHeading>Interest in and Extent of Self-Reflection</selfAwareDescriptionHeading>
        <selfAwareDescription>The degree to which an individual has personal insight,
                                             clearly understands his or her strengths and weaknesses, is free of
                                             blind spots, and uses this knowledge to perform effectively.</selfAwareDescription>
    </introductionSection>
    <resultsSummarySection>
        <title>Results Summary
                                                            </title>
        <subSection id="mental">
            <heading>Working with Concepts and Ideas</heading>
            <traitLow>Deep Expertise / Detail Oriented</traitLow>
            <traitHigh>Inquisitive / Broad Perspective</traitHigh>
            <score>0.0</score>
            <scoreNarrative>The results suggest that you have intellectual interests in
                                             select areas, often related to your functional expertise. You tend to
                                             value conventional wisdom but are willing to try new solutions if you
                                             are confident that they have been well researched. You are able to
                                             handle problems with a moderate level of complexity.</scoreNarrative>
        </subSection>
        <subSection id="people">
            <heading>Working with and Relating to People</heading>
            <traitLow>Autonomous / Consistent</traitLow>
            <traitHigh>Networking / Inspirational</traitHigh>
            <score>1.0</score>
            <scoreNarrative>The assessment indicates that you are interpersonally savvy.
                                             You can shift easily between different job roles and build
                                             relationships with a diversity of people at work. You tend to be
                                             skilled at influencing others and leveraging others' skills to
                                             get the job done. You are sensitive to the politics and can manage
                                             conflict constructively.</scoreNarrative>
        </subSection>
        <subSection id="change">
            <heading>Tolerance for Uncertainty and Ability to Lead Change</heading>
            <traitLow>Structured / Conventional</traitLow>
            <traitHigh>Experimental / Risk Taking</traitHigh>
            <score>0.0</score>
            <scoreNarrative>Your scores imply that you generally prefer continuity and
                                             stability but are willing to change when it is necessary. You prefer
                                             to follow rather than lead change efforts. Although you tend to be
                                             conventional and avoid risky activities, you will explore
                                             alternatives and try new behaviors when your environment changes.</scoreNarrative>
        </subSection>
        <subSection id="results">
            <heading>Preference for How to Get Things Done</heading>
            <traitLow>Focused / Dependable</traitLow>
            <traitHigh>Multitasking / Resourceful</traitHigh>
            <score>0.0</score>
            <scoreNarrative>The results reveal that you likely are a consistent
                                             performer. Although you prefer a stable environment, you are willing
                                             to go outside of your comfort zone if required. Typically, you do not
                                             rush to action. Rather you prefer to think things through and
                                             carefully plan your work activities. Generally, you focus on
                                             near-term goals and prefer to accomplish one set of tasks before
                                             moving to another.</scoreNarrative>
        </subSection>
        <subSection id="selfAware">
            <heading>Interest in and Extent of Self-Reflection</heading>
            <traitLow>Self-Assured / Content</traitLow>
            <traitHigh>Reflective / Insightful</traitHigh>
            <score>1.0</score>
            <scoreNarrative>The assessment shows that you actively seek feedback from
                                             others. You often engage in self-reflection and are very aware of
                                             your feelings and moods. You are mindful and monitor your behavior,
                                             gaining much from your life and work experiences. You have a clear
                                             understanding of your strengths and weaknesses and know what to do to
                                             achieve your career goals.</scoreNarrative>
        </subSection>
    </resultsSummarySection>
</viaEdgeIndividualSummaryReport>
