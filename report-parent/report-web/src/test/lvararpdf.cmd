SET WORKSPACE=C:\Users\chris\workspace-indigo
SET TESTPATH=%WORKSPACE%\ReportServer-web\test

wkhtmltopdf --quiet --enable-forms --dpi 600 --print-media-type --disable-smart-shrinking --no-pdf-compression -s Letter --header-html "http://localhost:8080/ReportServer-web/core/xhtml/header.xhtml?reportname=Readiness Action Report&participant=Jonathan Douglas&headerdate=23 September 2013" --footer-html "http://localhost:8080/ReportServer-web/core/xhtml/footer.xhtml"  --margin-top 37 --header-spacing 0 --margin-bottom 22 --margin-left 10 --margin-right 10 --footer-spacing 0 http://localhost:8080/ReportServer-web/test/LvaMllRar.xhtml - > LvaMllRar.pdf

start acrord32.exe /A "navpanes=0=OpenActions" "%TESTPATH%\LvaMllRar.pdf"
