<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" indent="yes" encoding="UTF-8" />

	<xsl:template match="/Table">
     <keymaps>
       <xsl:apply-templates select="Row" />
     </keymaps>
	</xsl:template>

	<xsl:template match="Row">
     <keyMap id="devSuggT">
       <key id="devSugg"><xsl:value-of select="Cell[1]/Data" /></key>
       <key id="Ia1DisplayText"><xsl:value-of select="Cell[2]/Data" /></key>
       <key id="Ia1Id"><xsl:value-of select="Cell[3]/Data" /></key>
       <key id="Ia2DisplayText"><xsl:value-of select="Cell[4]/Data" /></key>
       <key id="Ia2Id"><xsl:value-of select="Cell[5]/Data" /></key>
     </keyMap>

   </xsl:template>
</xsl:stylesheet>
