<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns="http://www.w3.org/1999/xhtml" 
  xmlns:f="http://java.sun.com/jsf/core"
  xmlns:h="http://java.sun.com/jsf/html"
  xmlns:ui="http://java.sun.com/jsf/facelets"
  xmlns:p="http://primefaces.org/ui"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xalan="http://xml.apache.org/xslt"
  xmlns:svg="http://www.w3.org/2000/svg"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="xsl xalan xs"
>

  <!--
      http://stackoverflow.com/questions/9538619/xslt-refuses-to-write-doctype-declaration
  -->
  <xsl:output method="xml" indent="yes" encoding="UTF-8"
              doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />
  

  <!-- no default match -->
  <xsl:template match="*"/>

  <xsl:template match="/reportContent">
    <html>
      <head><title>Devsug content test</title></head>
      <body>
        <h1>Report Content</h1>
        <xsl:apply-templates select="keyMap" />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="keyMap">
    <h2>KeyMap <xsl:value-of select="@id" /></h2>
    <div>
      <xsl:apply-templates select="key" />
    </div>
  </xsl:template>

  <xsl:template match="key">
    <h3>Key <xsl:value-of select="@id" /></h3>
    <xsl:value-of disable-output-escaping="yes" select="." />
  </xsl:template>

</xsl:stylesheet>
