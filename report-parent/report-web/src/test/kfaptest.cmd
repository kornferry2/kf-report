::
::  KFAP Individual Feedback Report TEST
::
SET WORKSPACE=C:\Users\arnoldm\workspace-kepler\
SET JARS=C:\glassfish3\glassfish\lib
SET XSLFOLDER=%WORKSPACE%\report-parent\report-ejb\src\main\resources\projects\kfap
SET OUTFOLDER=%WORKSPACE%\report-parent\report-web\src\main\webapp\test
java -jar %JARS%\saxon9he.jar -xi -s:kfaptest.xml -xsl:%XSLFOLDER%\individual_feedback_report.xsl -o:%OUTFOLDER%\kfapTest.xhtml subtype=coaching genmode=pdf baseurl=http://dur-2a-arnoldm:8080/reportweb

