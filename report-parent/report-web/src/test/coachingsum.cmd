::
::  coaching.cmd: generate coaching plan report
::
SET WORKSPACE=C:\9h_Source\workspace\
SET JARS=%WORKSPACE%ReportServer-ear\EarContent\lib
SET XSLFOLDER=%WORKSPACE%ReportServer-ejb\ejbModule\com\pdinh\resource\projects\coaching
SET OUTFOLDER=%WORKSPACE%ReportServer-web\WebContent\test
::java -jar %JARS%\saxon9he.jar -xi -s:coachingPlanReport.xml -xsl:%XSLFOLDER%\plan_report.xsl -o:%OUTFOLDER%\plan.xhtml subtype=development genmode=pdf baseurl=http://localhost:18080/ReportServer-web
java -jar %JARS%\saxon9he.jar -xi -s:coachingSummaryRcm.xml -xsl:%XSLFOLDER%\summary_report.xsl -o:%OUTFOLDER%\summary.xhtml genmode=pdf baseurl=http://localhost:18080/ReportServer-web

