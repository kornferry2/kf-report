IF "%GLASSFISH_HOME%" == "" GOTO NOHOME
:YESHOME
call %GLASSFISH_HOME%\bin\asadmin delete-jdbc-resource jdbc/pdinh-report
call %GLASSFISH_HOME%\bin\asadmin delete-jdbc-connection-pool --cascade=true MsSql_pdinh_report_pool
call %GLASSFISH_HOME%\bin\asadmin delete-custom-resource custom/generator_properties
call %GLASSFISH_HOME%\bin\asadmin delete-custom-resource custom/shell_properties
call %GLASSFISH_HOME%\bin\asadmin delete-custom-resource custom/lva_properties
call %GLASSFISH_HOME%\bin\asadmin delete-custom-resource application/report/config_properties

@cd /D %~dp0
call %GLASSFISH_HOME%\bin\asadmin add-resources sun-resources.xml
GOTO END
:NOHOME
@ECHO The GLASSFISH_HOME environment variable was NOT detected
GOTO END
:END
pause

