package com.pdinh.reportserver.data.dao.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.reportserver.persistence.jpa.ReportRequestResponse;

@Stateless
public class ReportRequestResponseDaoImpl extends GenericDaoImpl<ReportRequestResponse, Integer> implements
		ReportRequestResponseDao {

	@Override
	public ReportRequestResponse createOrUpdate(int id, String type, String rgrXml, Integer targ) {
		ReportRequestResponse reportRequestResponse;

		if (findSingleByParticipantIdAndTypeAndTarget(id, type, targ) != null) {
			reportRequestResponse = update(id, type, rgrXml, targ);
		} else {
			reportRequestResponse = create(id, type, rgrXml, targ);
		}
		return reportRequestResponse;
	}

	@Override
	public ReportRequestResponse create(int id, String type, String rgrXml, Integer targ) {
		// Only add if it doesn't exist
		if (findSingleByParticipantIdAndTypeAndTarget(id, type, targ) == null) {
			ReportRequestResponse reportRequestResponse = new ReportRequestResponse();
			reportRequestResponse.setParticipantId(id);
			reportRequestResponse.setRgrType(type);
			reportRequestResponse.setRgrXml(rgrXml);
			reportRequestResponse.setModifiedDate(new java.util.Date());
			reportRequestResponse.setTargLevelIndex(targ);
			getEntityManager().persist(reportRequestResponse);
			return reportRequestResponse;
		} else {
			return null;
		}
	}

	@Override
	public ReportRequestResponse update(int id, String type, String rgrXml, Integer targ) {
		ReportRequestResponse reportRequestResponse = findSingleByParticipantIdAndTypeAndTarget(id, type, targ);
		reportRequestResponse.setRgrType(type);
		reportRequestResponse.setRgrXml(rgrXml);
		reportRequestResponse.setModifiedDate(new java.util.Date());
		reportRequestResponse.setTargLevelIndex(targ);
		getEntityManager().merge(reportRequestResponse);
		return reportRequestResponse;
	}

	@Override
	public void removeByParticipantId(int id) {
		ReportRequestResponse reportRequestResponse = findSingleByParticipantId(id);
		if (reportRequestResponse != null) {
			getEntityManager().remove(reportRequestResponse);
		}
	}

	@Override
	public void removeByParticipantIdAndType(int id, String type) {
		// there can be multiple rows per ppt and type for different target
		// levels. need to delete them all.
		List<Integer> idList = new ArrayList<Integer>();
		idList.add(id);
		List<ReportRequestResponse> responses = findBatchByParticipantIdAndRgrType(idList, type);

		for (ReportRequestResponse reportRequestResponse : responses) {
			if (reportRequestResponse != null) {
				getEntityManager().remove(reportRequestResponse);
			}
		}
	}

	/**
	 * named queries
	 */
	@Override
	public List<ReportRequestResponse> findByParticipantId(Integer participantId) {

		TypedQuery<ReportRequestResponse> query = getEntityManager().createNamedQuery("findRgrsByParticipantId",
				ReportRequestResponse.class);
		query.setParameter("pid", participantId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	@Override
	public List<ReportRequestResponse> findByParticipantIdAndType(Integer participantId, String rgrType) {

		TypedQuery<ReportRequestResponse> query = getEntityManager().createNamedQuery(
				"findRgrsByParticipantIdAndRgrType", ReportRequestResponse.class);
		query.setParameter("pid", participantId);
		query.setParameter("rgrType", rgrType);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	@Override
	public ReportRequestResponse findSingleByParticipantId(Integer participantId) {

		TypedQuery<ReportRequestResponse> query = getEntityManager().createNamedQuery("findSingleByParticipantId",
				ReportRequestResponse.class);
		query.setParameter("pid", participantId);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	@Override
	public ReportRequestResponse findSingleByParticipantIdAndType(Integer participantId, String type) {

		TypedQuery<ReportRequestResponse> query = getEntityManager().createNamedQuery(
				"findSingleByParticipantIdAndRgrType", ReportRequestResponse.class);
		query.setParameter("pid", participantId);
		query.setParameter("type", type);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	@Override
	public ReportRequestResponse findSingleByParticipantIdAndTypeAndTarget(Integer participantId, String type,
			Integer targ) {

		List<String> reportTypes = new ArrayList<String>();
		if (type.equals(ReportType.ALP_IND_V2) || type.equals(ReportType.ALP_TALENT_GRID_V2)) {
			reportTypes.add(ReportType.ALP_IND_V2);
			reportTypes.add(ReportType.ALP_TALENT_GRID_V2);
		} else {
			reportTypes.add(type);
		}
		TypedQuery<ReportRequestResponse> query = getEntityManager().createNamedQuery(
				"findSingleByParticipantIdAndRgrTypeAndTarg", ReportRequestResponse.class);
		query.setParameter("pid", participantId);
		query.setParameter("types", reportTypes);
		query.setParameter("targ", targ);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	@Override
	public List<ReportRequestResponse> findBatchByParticipantIdAndRgrType(List<Integer> participantIds, String type) {
		TypedQuery<ReportRequestResponse> query = getEntityManager().createNamedQuery(
				"findBatchByParticipantIdAndRgrType", ReportRequestResponse.class);
		query.setParameter("pids", participantIds);
		query.setParameter("type", type);

		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	@Override
	public List<ReportRequestResponse> findBatchByParticipantIdAndRgrTypeAndTarget(List<Integer> participantIds,
			String type, Integer targ) {
		if (participantIds.isEmpty()) {
			return null;
		}
		List<String> reportTypes = new ArrayList<String>();
		if (type.equals(ReportType.ALP_IND_V2) || type.equals(ReportType.ALP_TALENT_GRID_V2)) {
			reportTypes.add(ReportType.ALP_IND_V2);
			reportTypes.add(ReportType.ALP_TALENT_GRID_V2);
		} else {
			reportTypes.add(type);
		}

		TypedQuery<ReportRequestResponse> query = getEntityManager().createNamedQuery(
				"findBatchByParticipantIdAndRgrTypeAndTarg", ReportRequestResponse.class);
		query.setParameter("pids", participantIds);
		query.setParameter("types", reportTypes);
		query.setParameter("targ", targ);

		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}
}
