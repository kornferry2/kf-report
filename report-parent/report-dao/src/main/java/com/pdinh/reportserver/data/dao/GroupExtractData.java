package com.pdinh.reportserver.data.dao;

import com.pdinh.reporting.generation.Participants;
import com.pdinh.reporting.generation.ReportParticipant;

// A thin class to carry validated input parameters to where they are used group extract servlet to the helper bean).
public class GroupExtractData {

	// Validated parameters
	private String languageCode = null;
	private int targetLevel = 0; // Index ID of the target type
	private String groupName = null;
	private Participants participantsObject = null;

	// Parameters not yet used
	// Report type code
	private String rptType = null;
	// The code column from target_type table (not currently used)
	private String targCode = null;
	// The ID of the type from the target_type table (not currently used)
	private String targId = null;

	public String toString() {
		StringBuilder ret = new StringBuilder("GroupExtractData:  ");
		ret.append("type=" + rptType + ", ");
		ret.append("lang=" + languageCode + ", ");
		ret.append("target=[level=" + targetLevel + " code=" + targCode + " id=" + targId + "], ");
		ret.append("group=" + groupName + ", ");
		ret.append("participants:  ");
		boolean first = true;
		for (ReportParticipant ppt : participantsObject.getParticipants()) {
			if (first) {
				first = false;
			} else {
				ret.append(", ");
			}
			ret.append(ppt.getParticipantId());
		}

		return ret.toString();
	}

	// Getters and setters

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public int getTargetLevel() {
		return targetLevel;
	}

	public void setTargetLevel(int targetLevel) {
		this.targetLevel = targetLevel;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Participants getParticipantsObject() {
		return participantsObject;
	}

	public void setParticipantsObject(Participants participants) {
		this.participantsObject = participants;
	}

	public String getRptType() {
		return rptType;
	}

	public void setRptType(String rptType) {
		this.rptType = rptType;
	}

	public String getTargCode() {
		return targCode;
	}

	public void setTargCode(String targCode) {
		this.targCode = targCode;
	}

	public String getTargId() {
		return targId;
	}

	public void setTargId(String targId) {
		this.targId = targId;
	}
}
