package com.pdinh.reportserver.data.dao.jpa;

import java.util.List;

import com.pdinh.reportserver.persistence.jpa.ReportContentModel;

public interface ReportContentModelDao extends GenericDaoLocal<ReportContentModel, Integer> {

	public ReportContentModel createOrUpdate(int id, String type, String reportXml);

	public ReportContentModel createOrUpdate(int id, String type, String reportXml, int phaseTypeId);

	public ReportContentModel create(int id, String type, String reportXml);

	public ReportContentModel create(int id, String type, String reportXml, int phaseTypeId);

	public ReportContentModel update(int id, String type, String reportXml);

	public ReportContentModel update(int id, String type, String reportXml, int phaseTypeId);

	public void removeByParticipantIdAndType(int id, String type);

	public void removeByParticipantIdAndTypeAndPhaseTypeId(int id, String type, int phaseTypeId);

	public List<ReportContentModel> findByParticipantId(Integer participantId);

	public List<ReportContentModel> findByParticipantIdAndPhaseTypeId(Integer participantId, Integer phaseTypeId);

	public List<ReportContentModel> findByParticipantIdAndReportType(Integer participantId, String reportType);

	public ReportContentModel findSingleByParticipantIdAndType(Integer participantId, String type);

	public ReportContentModel findSingleByParticipantIdAndTypeAndPhaseTypeId(Integer participantId, String type,
			Integer phaseTypeId);

	public void evict();

	public void evictAll();
}
