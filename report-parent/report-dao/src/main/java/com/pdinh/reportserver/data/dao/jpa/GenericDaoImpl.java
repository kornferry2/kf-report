package com.pdinh.reportserver.data.dao.jpa;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;



public abstract class GenericDaoImpl<T, ID extends Serializable> implements
        GenericDaoLocal<T, ID> {
	
    protected Class<T> persistentClass;
    
    @PersistenceContext(unitName = "ReportServer-dao")
    protected EntityManager entityManager;
    
    @SuppressWarnings("unchecked")
    protected GenericDaoImpl() {
        if (getClass().getGenericSuperclass() instanceof ParameterizedType) {
            ParameterizedType type = (ParameterizedType) (getClass()
                    .getGenericSuperclass());
            System.out.println(type.getActualTypeArguments()[0]);
            this.persistentClass = (Class<T>) type.getActualTypeArguments()[0];
        }
    }
    
    protected GenericDaoImpl(Class<T> persistentClass) {
        this.persistentClass = persistentClass;
    }
    
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    
    public T findById(ID id) {
        return entityManager.find(persistentClass, id);
    }
    
    public List<T> findAll() {
        List<T> list = entityManager.createQuery(
                "SELECT obj from " + persistentClass.getSimpleName() + " obj",
                persistentClass).getResultList();
        return list;
    }
    
    public List<T> findAll(boolean doRefresh) {
        String sql = String.format("SELECT obj from %1$s obj",
                persistentClass.getSimpleName());
        TypedQuery<T> query = entityManager.createQuery(sql,
                persistentClass);
        if (doRefresh)
            query.setHint("javax.persistence.cache.storeMode", "REFRESH");
        List<T> list = query.getResultList();
        return list;
    }
    
    public void update(T object) {
        entityManager.merge(object);
        // Not yet sure if need this
        // entityManager.flush();
    }
    
    public T create(T object) {
        entityManager.persist(object);
        
        //need this to get generated IDs after persist
        entityManager.flush();
        return object;
    }
    
    public void delete(T object) {
        if (!entityManager.contains(object)) {
            object = entityManager.merge(object);
        }
        entityManager.remove(object);
    }

    public void evict() {
    	entityManager.getEntityManagerFactory().getCache().evict(persistentClass);
    }
    
    public void evictAll() {
        entityManager.getEntityManagerFactory().getCache().evictAll();
    }
}
