package com.pdinh.reportserver.data.dao.jpa;

import com.pdinh.reportserver.persistence.jpa.PhaseType;

public interface PhaseTypeDao extends GenericDaoLocal<PhaseType, Integer> {

	public PhaseType findPhaseByPhaseTypeId(int phaseTypeId);
}