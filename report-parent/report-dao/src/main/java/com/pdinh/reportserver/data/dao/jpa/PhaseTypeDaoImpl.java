package com.pdinh.reportserver.data.dao.jpa;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.reportserver.persistence.jpa.PhaseType;

@Stateless
public class PhaseTypeDaoImpl extends GenericDaoImpl<PhaseType, Integer> implements PhaseTypeDao {

	@Override
	public PhaseType findPhaseByPhaseTypeId(int phaseTypeId) {

		TypedQuery<PhaseType> typedQuery = entityManager.createNamedQuery("findPhaseByPhaseTypeId", PhaseType.class);
		typedQuery.setParameter("phaseTypeId", phaseTypeId);

		try {

			PhaseType phaseType = typedQuery.getSingleResult();
			return phaseType;
		} catch (NoResultException e) {
			return null;
		}
	}

}