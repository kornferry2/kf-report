package com.pdinh.reportserver.data.dao.jpa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.pdinh.reportserver.persistence.jpa.ReportContentModel;

@Stateless
public class ReportContentModelDaoImpl extends GenericDaoImpl<ReportContentModel, Integer> implements
		ReportContentModelDao {

	@EJB
	private PhaseTypeDao phaseTypeDao;

	/*
	 * Write a row to the database; inserts if it is not there, updates it it is
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pdinh.reportserver.data.dao.jpa.ReportContentModelDao#createOrUpdate
	 * (int, java.lang.String, java.lang.String)
	 */
	@Override
	public ReportContentModel createOrUpdate(int id, String type, String reportXml) {
		ReportContentModel rcm;
		if (findSingleByParticipantIdAndType(id, type) != null) {
			rcm = update(id, type, reportXml);
		} else {
			rcm = create(id, type, reportXml);
		}
		return rcm;
	}

	@Override
	public ReportContentModel createOrUpdate(int id, String type, String reportXml, int phaseTypeId) {
		ReportContentModel rcm;
		if (findSingleByParticipantIdAndTypeAndPhaseTypeId(id, type, phaseTypeId) != null) {
			rcm = update(id, type, reportXml, phaseTypeId);
		} else {
			rcm = create(id, type, reportXml, phaseTypeId);
		}
		return rcm;
	}

	@Override
	public ReportContentModel create(int id, String type, String reportXml) {
		// Only add if it doesn't exist
		if (findSingleByParticipantIdAndType(id, type) == null) {
			ReportContentModel rcm = new ReportContentModel();
			rcm.setParticipantId(id);
			rcm.setReportType(type);
			rcm.setReportXml(reportXml);
			getEntityManager().persist(rcm);
			return rcm;
		} else {
			return null;
		}
	}

	@Override
	public ReportContentModel create(int id, String type, String reportXml, int phaseTypeId) {
		// Only add if it doesn't exist
		if (findSingleByParticipantIdAndTypeAndPhaseTypeId(id, type, phaseTypeId) == null) {
			ReportContentModel rcm = new ReportContentModel();
			rcm.setParticipantId(id);
			rcm.setReportType(type);
			rcm.setReportXml(reportXml);
			rcm.setPhaseType(phaseTypeDao.findById(phaseTypeId));
			getEntityManager().persist(rcm);
			return rcm;
		} else {
			return null;
		}
	}

	@Override
	public ReportContentModel update(int id, String type, String reportXml) {
		ReportContentModel rcm = findSingleByParticipantIdAndType(id, type);
		rcm.setReportType(type);
		rcm.setReportXml(reportXml);
		getEntityManager().merge(rcm);
		return rcm;
	}

	@Override
	public ReportContentModel update(int id, String type, String reportXml, int phaseTypeId) {
		ReportContentModel rcm = findSingleByParticipantIdAndTypeAndPhaseTypeId(id, type, phaseTypeId);
		rcm.setReportType(type);
		rcm.setReportXml(reportXml);
		getEntityManager().merge(rcm);
		return rcm;
	}

	@Override
	public void removeByParticipantIdAndType(int id, String type) {
		ReportContentModel rcm = findSingleByParticipantIdAndType(id, type);
		if (rcm != null) {
			getEntityManager().remove(rcm);
		}
	}

	@Override
	public void removeByParticipantIdAndTypeAndPhaseTypeId(int id, String type, int phaseTypeId) {
		ReportContentModel rcm = findSingleByParticipantIdAndTypeAndPhaseTypeId(id, type, phaseTypeId);
		if (rcm != null) {
			getEntityManager().remove(rcm);
		}
	}

	@Override
	public List<ReportContentModel> findByParticipantId(Integer participantId) {
		System.out.println("IN ReportContentModelDaoImpl.findByParticipantId participantId: " + participantId);
		TypedQuery<ReportContentModel> query = getEntityManager().createNamedQuery("findReportsByParticipantId",
				ReportContentModel.class);
		query.setParameter("pid", participantId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Get all of the reports for a person that are of a particular phase type
	 */
	@Override
	public List<ReportContentModel> findByParticipantIdAndPhaseTypeId(Integer participantId, Integer phaseTypeId) {
		System.out.println("IN ReportContentModelDaoImpl.findByParticipantIdAndPhaseTypeId.  participantId: "
				+ participantId + ", phaseTypeId=" + phaseTypeId);
		TypedQuery<ReportContentModel> query = getEntityManager().createNamedQuery(
				"findReportsByParticipantIdAndPhaseTypeId", ReportContentModel.class);
		query.setParameter("pid", participantId);
		query.setParameter("phaseTypeId", phaseTypeId);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Get all of the reports for a person that are of a particular report type
	 */
	@Override
	public List<ReportContentModel> findByParticipantIdAndReportType(Integer participantId, String reportType) {
		System.out.println("IN ReportContentModelDaoImpl.findByParticipantIdAndReportType.  participantId: "
				+ participantId + ", reportType=" + reportType);
		TypedQuery<ReportContentModel> query = getEntityManager().createNamedQuery(
				"findReportsByParticipantIdAndReportType", ReportContentModel.class);
		query.setParameter("pid", participantId);
		query.setParameter("reportType", reportType);
		try {
			return query.getResultList();
		} catch (NoResultException nre) {
			return null;
		}
	}

	@Override
	public ReportContentModel findSingleByParticipantIdAndType(Integer participantId, String type) {
		System.out.println("IN ReportContentModelDaoImpl.findSingleByParticipantIdAndType participantId: "
				+ participantId + " type = " + type);
		TypedQuery<ReportContentModel> query = getEntityManager().createNamedQuery(
				"findSingleReportByParticipantIdAndType", ReportContentModel.class);
		query.setParameter("pid", participantId);
		query.setParameter("type", type);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	@Override
	public ReportContentModel findSingleByParticipantIdAndTypeAndPhaseTypeId(Integer participantId, String type,
			Integer phaseTypeId) {
		System.out
				.println("IN ReportContentModelDaoImpl.findSingleByParticipantIdAndTypeAndPhaseTypeId participantId: "
						+ participantId + " type = " + type + " phaseTypeId = " + phaseTypeId);
		TypedQuery<ReportContentModel> query = getEntityManager().createNamedQuery(
				"findSingleReportByParticipantIdAndTypeAndPhaseTypeId", ReportContentModel.class);
		query.setParameter("pid", participantId);
		query.setParameter("type", type);
		query.setParameter("phaseTypeId", phaseTypeId);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public PhaseTypeDao getPhaseTypeDao() {
		return phaseTypeDao;
	}

	public void setPhaseTypeDao(PhaseTypeDao phaseTypeDao) {
		this.phaseTypeDao = phaseTypeDao;
	}

}
