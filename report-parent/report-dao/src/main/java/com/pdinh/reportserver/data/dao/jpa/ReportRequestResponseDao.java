package com.pdinh.reportserver.data.dao.jpa;

import java.util.List;

import com.pdinh.reportserver.persistence.jpa.ReportRequestResponse;

public interface ReportRequestResponseDao extends GenericDaoLocal<ReportRequestResponse, Integer> {

	public ReportRequestResponse createOrUpdate(int id, String type, String rgrXml, Integer targ);

	public ReportRequestResponse create(int id, String type, String rgrXml, Integer targ);

	public ReportRequestResponse update(int id, String type, String rgrXml, Integer targ);

	public ReportRequestResponse findSingleByParticipantId(Integer participantId);

	public ReportRequestResponse findSingleByParticipantIdAndType(Integer participantId, String type);
	
	public ReportRequestResponse findSingleByParticipantIdAndTypeAndTarget(Integer participantId, String type, Integer targ);
		
	public List<ReportRequestResponse> findBatchByParticipantIdAndRgrType(List<Integer> participantIds, String type);
	
	public List<ReportRequestResponse> findBatchByParticipantIdAndRgrTypeAndTarget(List<Integer> participantIds, String type, Integer targ);

	public List<ReportRequestResponse> findByParticipantId(Integer participantId);

	public List<ReportRequestResponse> findByParticipantIdAndType(Integer participantId, String type);
	
	public void removeByParticipantId(int id);
	
	public void removeByParticipantIdAndType(int id, String type);

}
