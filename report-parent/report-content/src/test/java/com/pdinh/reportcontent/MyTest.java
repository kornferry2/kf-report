package com.pdinh.reportcontent;

import com.pdinh.reportcontent.ReportContent;

public class MyTest {
	private static String sourceReportContent = "C:/DevTools/Eclipse/Workspaces/indigo/workspace/ReportContentRepository/content/projects/internal/gl-gps/mll/developmentReport.xml"; 

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ReportContent reportContent = new ReportContent(sourceReportContent);
		
		System.out.println("keyMap test: " + reportContent.getKeyMapValue("configuration","asyncAndDrm"));
		System.out.println("gridMap test: " + reportContent.getGridMapValue("developmentFocusPotentialLowOrMixed","experienceStrength","competenciesStrength"));
	}

}
