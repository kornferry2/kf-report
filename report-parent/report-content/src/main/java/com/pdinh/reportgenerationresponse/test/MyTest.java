package com.pdinh.reportgenerationresponse.test;

import java.io.File;

import com.pdinh.reportgenerationresponse.ReportGenerationResponse;

public class MyTest {
	private static String reportGenerationResponsePath = "C:/DevTools/Eclipse/Workspaces/indigo/workspace/ReportContentRepository/reportGenerationResponse/projects/internal/gl-gps/mll/12-score-rgr-output.xml"; 

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		File rgrFile = new File(reportGenerationResponsePath);
		ReportGenerationResponse reportGenerationResponse = new ReportGenerationResponse(rgrFile);
		
		System.out.println("Get var by stage and var id: " + reportGenerationResponse.getVarValue("responses", "iblva", "I8B29"));
		System.out.println("Get var by var id: " + reportGenerationResponse.getVarValue("I8B29"));
	}

}
