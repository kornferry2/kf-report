package com.pdinh.reportgenerationresponse;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;

//TODO rework this class to use a logger
public class ReportGenerationResponse {

	//
	// Instance variables
	//
	private Document xmlDoc;

	//
	// Constructors - All create the object and load an XML Document into memory
	//
	// Note that we may want to add a constructor that uses an XMLDocument as
	// input, but
	// that code is not implemented at this time
	//

	// Start with an XML String
	public ReportGenerationResponse(String xml) {
		loadXml(xml);
	}

	// Start with a File - This is primarily for debug purposes
	public ReportGenerationResponse(File file) {
		loadXml(file);
	}

	// Start with an rgr Object
	public ReportGenerationResponse(ReportGenerationRequest rgr) {
		loadXml(rgr);
	}

	public String getVarValue(String stageId, String instrumentId, String varId) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("//stage[@id='" + stageId + "']/instrument[@id='" + instrumentId
					+ "']/var[@id='" + varId + "']/@value");
			Node node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);

			return node.getNodeValue();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		return "";
	}

	/**
	 * getVarValue - gets a value from a <var> tag. The first parameter is the
	 * id attribute associated with the <stage> and the second is the id
	 * attribute associated with the <var> within that <stage>. The value passed
	 * back is the value for the <var> in the stage.
	 * 
	 * @param stageId
	 * @param varId
	 * @return
	 */
	public String getVarValue(String stageId, String varId) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("//stage[@id='" + stageId + "']/var[@id='" + varId + "']/@value");
			Node node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);

			return node.getNodeValue();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		return "";
	}

	/**
	 * getVarValue - gets a value from a <var> tag. The passed parameter is the
	 * id attribute associated with the <var> and the value passed back is the
	 * value attribute associated with the designated tag.
	 * 
	 * @param varId
	 * @return
	 */
	public String getVarValue(String varId) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("//var[@id='" + varId + "']/@value");
			Node node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);
			if (node != null)
				return node.getNodeValue();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		// falure return (associated with the exception)
		return "";
	}

	/**
	 * getNodeAttribute - Get the value of a named attribute in a unique node
	 * 
	 * @param nodeName
	 *            - The name of the unique node
	 * @param attributeName
	 *            - The name of the attribute within the named node
	 * @return The value of the attribute
	 */
	public String getNodeAttribute(String nodeName, String attributeName) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("//" + nodeName + "/@" + attributeName);
			Node node = (Node) expr.evaluate(xmlDoc, XPathConstants.NODE);

			return node.getNodeValue();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		return "";
	}

	/**
	 * loadXml - Create the ReportGenerationResponse object and load the
	 * internal XML Document object from a String
	 * 
	 * @param xml
	 *            - A String object. Content is an RGR XML string
	 * @return none
	 */
	private void loadXml(String xml) {
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true);
		DocumentBuilder builder;
		try {
			builder = domFactory.newDocumentBuilder();
			setXmlDoc(builder.parse(new InputSource(new StringReader(xml))));
		} catch (ParserConfigurationException e) {
			System.out.println("ReportGenerationResponse.loadXml(String) - ParserConfigurationException: Msg="
					+ e.getMessage());
			e.printStackTrace();
		} catch (SAXException e) {
			System.out.println("ReportGenerationResponse.loadXml(String) - SAXException: Msg=" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("ReportGenerationResponse.loadXml(String) - IOException: Msg=" + e.getMessage());
			e.printStackTrace();
		} finally {

		}
	}

	/**
	 * loadXml - Create the ReportGenerationResponse object and load the
	 * internal XML Document object from a file
	 * 
	 * NOTE: This is primarily for Debug at this time
	 * 
	 * @param file
	 *            - A File object. File content is an RGR XML string
	 * @return none
	 */
	private void loadXml(File file) {
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true);
		DocumentBuilder builder;
		try {
			builder = domFactory.newDocumentBuilder();
			setXmlDoc(builder.parse(file));
		} catch (ParserConfigurationException e) {
			System.out.println("ReportGenerationResponse.loadXml(File) - ParserConfigurationException: Msg="
					+ e.getMessage());
			e.printStackTrace();
		} catch (SAXException e) {
			System.out.println("ReportGenerationResponse.loadXml(File) - SAXException:  Msg=" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("ReportGenerationResponse.loadXml(File) - IOException:  Msg=" + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * loadXml - Create the ReportGenerationResponse object and load the
	 * internal XML Document object from an rgr Object
	 * 
	 * @param rgr
	 *            - A ReportGenerationRequest object.
	 * @return none
	 */
	private void loadXml(ReportGenerationRequest rgr) {
		// Marshal the input...
		JAXBContext jc = null;
		Marshaller marshaller = null;
		StringWriter sw = null;
		try {
			jc = JAXBContext.newInstance(ReportGenerationRequest.class);
			marshaller = jc.createMarshaller();
			sw = new StringWriter();
			marshaller.marshal(rgr, sw);
		} catch (JAXBException e) {
			System.out.println("ReportGenerationResponse.loadXml(ReportGenerationRequest) - JAXBException: Msg="
					+ e.getMessage());
			e.printStackTrace();
		}

		// ... and call the one that uses a string
		if (sw == null) {
			System.out.println("loadXml(rgr) did not produce a StringWriter; call to loadXml(String) not called.");
		} else {
			loadXml(sw.toString());
		}
	}

	// Setters and getters
	public Document getXmlDoc() {
		return this.xmlDoc;
	}

	public void setXmlDoc(Document xmlDoc) {
		this.xmlDoc = xmlDoc;
	}
}
