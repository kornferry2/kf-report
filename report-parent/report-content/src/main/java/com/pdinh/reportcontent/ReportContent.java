package com.pdinh.reportcontent;

import java.io.IOException;
import java.text.MessageFormat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ReportContent {
	private Document xmlDoc;
	private static final Logger log = LoggerFactory.getLogger(ReportContent.class);

	public ReportContent(String path) {
		// Load xml in memory
		loadXml(path);
	}

	public String getKeyMapValue(String keyMapId, String keyId) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("//keyMap[@id='" + keyMapId + "']/key[@id='" + keyId + "']");
			Element elm = (Element) expr.evaluate(xmlDoc, XPathConstants.NODE);
			if (elm == null) {
				System.out.println("ReportContent.getKeyMapValue(): Value not found for " + keyMapId + "/" + keyId);
				return "";
			} else {
				return elm.getTextContent();
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return "";
		}
	}

	public String getKeyMapValue(String keyMapId, String keyId, Object... params) {
		String value = getKeyMapValue(keyMapId, keyId);
		return MessageFormat.format(value, params);
	}

	public String getGridMapValue(String gridMapId, String rowId, String columnId) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("//gridMap[@id='" + gridMapId + "']/row[@id='" + rowId
					+ "']/column[@id='" + columnId + "']");
			Element elm = (Element) expr.evaluate(xmlDoc, XPathConstants.NODE);

			if (elm == null) {
				System.out.println("ReportContent.getGridMapValue(): Value not found for " + gridMapId + "/" + rowId
						+ "/" + columnId);
				return "";
			} else {
				return elm.getTextContent();
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return "";
		}
	}

	public String getGridMapValue(String gridMapId, String rowId, String columnId, Object... params) {
		String value = getGridMapValue(gridMapId, rowId, columnId);
		return MessageFormat.format(value, params);
	}

	private void loadXml(String path) {
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true);
		DocumentBuilder builder;
		try {
			builder = domFactory.newDocumentBuilder();
			setXmlDoc(builder.parse(path));
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Document getXmlDoc() {
		return xmlDoc;
	}

	public void setXmlDoc(Document xmlDoc) {
		this.xmlDoc = xmlDoc;
	}

	// Key Value lookup With Language
	public String getKeyMapValue4Lang(String keyMapId, String keyId, String lang) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("//keyMap[@id='" + keyMapId + "']/key[@id='" + keyId
					+ "']/contents/content[@lang='" + lang + "']");
			Element elm = (Element) expr.evaluate(xmlDoc, XPathConstants.NODE);
			if (elm == null) {
				System.out.println("ReportContent.getKeyMapValue(): Value not found for " + keyMapId + "/" + keyId
						+ "/" + lang);
				return "";
			} else {
				return elm.getTextContent();
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return "";
		}
	}

	// Grid Map lookup with Language
	public String getGridMapValue4Lang(String gridMapId, String rowId, String columnId, String lang) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr;
			if (!columnId.equals("*")) {
				expr = xpath.compile("//gridMap[@id='" + gridMapId + "']/row[@id='" + rowId + "']/column[@id='"
						+ columnId + "']/contents/content[@lang='" + lang + "']");
				Element elm = (Element) expr.evaluate(xmlDoc, XPathConstants.NODE);
				if (elm == null) {
					log.error("ReportContent.getGridMapValue(): Value not found for " + gridMapId + "/" + rowId + "/"
							+ columnId + "/" + lang);
					return "";
				} else {
					return elm.getTextContent();
				}
			} else {
				// FIXME refactor into it's own function for making a list from
				// many columns contents per lang and show attributes?

				// if '*' get all columns, else get only column specified
				expr = xpath.compile("//gridMap[@id='" + gridMapId + "']/row[@id='" + rowId
						+ "']/column/contents/content[(@show='true' or @show='TRUE') and @lang='" + lang + "']");

				NodeList result = (NodeList) expr.evaluate(xmlDoc, XPathConstants.NODESET);

				String returnString = "<ul>";
				for (int i = 1; i <= result.getLength(); i++) { // put them in
																// order and
																// select by
																// 'id' number
					expr = xpath.compile("//gridMap[@id='" + gridMapId + "']/row[@id='" + rowId + "']/column[@id='" + i
							+ "']/contents/content[(@show='true' or @show='TRUE') and @lang='" + lang + "']");
					Element column = (Element) expr.evaluate(xmlDoc, XPathConstants.NODE);
					if (column != null) {
						returnString += "<li>" + column.getTextContent() + "</li>";
					}
				}
				returnString += "</ul>";
				// log.debug(returnString);
				return returnString;
			}

		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return "";
		}
	}

	public String getKeyMapCaseSentenceLangValue(String keyMapId, String keyId, String sentenceId, String caseId,
			String lang) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		XPathExpression expr = null;

		try {
			if (sentenceId != null && caseId == null) {
				expr = xpath.compile("//keyMap[@id='" + keyMapId + "']/key[@id='" + keyId + "' and @sentence ='"
						+ sentenceId + "']/contents/content[@lang='" + lang + "']");
			} else if (sentenceId == null && caseId != null) {
				expr = xpath.compile("//keyMap[@id='" + keyMapId + "']/key[@id='" + keyId
						+ "']/contents/content[@lang='" + lang + "' and @case ='" + caseId + "']");
			} else {
				expr = xpath.compile("//keyMap[@id='" + keyMapId + "']/key[@id='" + keyId + "' and @sentence='"
						+ sentenceId + "']/contents/content[@lang='" + lang + "' and @case = '" + caseId + "']");
			}

			Element elm = (Element) expr.evaluate(xmlDoc, XPathConstants.NODE);
			if (elm == null) {
				System.out.println("ReportContent.getKeyMapCaseSentenceLangValue(): Value not found for " + keyMapId
						+ "/" + keyId + "/" + lang);
				return "";
			} else {
				return elm.getTextContent();
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return "";
		}
	}

}
