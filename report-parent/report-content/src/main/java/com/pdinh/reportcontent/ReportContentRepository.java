package com.pdinh.reportcontent;

import java.io.File;

public class ReportContentRepository {

	public String repositoryDir;

	public ReportContentRepository(String repositoryDir) {
		this.repositoryDir = repositoryDir;
	}

	public Manifest findManifestById(String id) {
		String path = repositoryDir + File.separator + id + ".xml";
		Manifest manifest = new Manifest(path);
		return manifest;
	}

	public ReportContent findReportContentById(String id) {
		String path = repositoryDir + File.separator + id + ".xml";
		ReportContent reportContent = new ReportContent(path);
		return reportContent;
	}

	public String getRepositoryDir() {
		return repositoryDir;
	}
}
