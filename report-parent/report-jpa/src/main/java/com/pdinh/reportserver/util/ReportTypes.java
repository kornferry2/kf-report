package com.pdinh.reportserver.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReportTypes {

	/*
	 * Report types
	 */
	public static final String SHELL_LAR_DEVELOPMENT = "SHELL_LAR_DEVELOPMENT";
	public static final String SHELL_LAR_READINESS = "SHELL_LAR_READINESS";
	public static final String LVA_MLL_DEVELOPMENT = "LVA_MLL_DEVELOPMENT";
	public static final String LVA_MLL_READINESS = "LVA_MLL_READINESS";
	public static final String COACHING_PLAN = "COACHING_PLAN";
	public static final String COACHING_SUMMARY = "COACHING_SUMMARY";
	public static final String VIAEDGE_INDIVIDUAL_SUMMARY = "VIAEDGE_INDIVIDUAL_SUMMARY";
//	public static final String VIAEDGE_INDIVIDUAL_COACHING = "VIAEDGE_INDIVIDUAL_COACHING";
//	public static final String VIAEDGE_INDIVIDUAL_FEEDBACK = "VIAEDGE_INDIVIDUAL_FEEDBACK";
//	public static final String VIAEDGE_GROUP_FEEDBACK = "VIAEDGE_GROUP_FEEDBACK";

	/*
	 * Report type lists
	 */
	public static final List<String> LVA_MLL_REPORTS_TYPES = new ArrayList<String>();
	public static final List<String> SHELL_LOT_REPORTS_TYPES = new ArrayList<String>();
	public static final Map<String, String> REPORT_TYPE_DISPLAY_NAMES = new HashMap<String, String>();

	/*
	 * Report sub types, these can be derived from the report type but for now
	 * the current report generator is expecting these values
	 */
	public static final String SUB_TYPE_DEVELOPMENT = "development";
	public static final String SUB_TYPE_READINESS = "readiness";

	static {
		LVA_MLL_REPORTS_TYPES.add(LVA_MLL_DEVELOPMENT);
		LVA_MLL_REPORTS_TYPES.add(LVA_MLL_READINESS);

		SHELL_LOT_REPORTS_TYPES.add(SHELL_LAR_DEVELOPMENT);
		SHELL_LOT_REPORTS_TYPES.add(SHELL_LAR_READINESS);

		REPORT_TYPE_DISPLAY_NAMES.put(LVA_MLL_DEVELOPMENT, "GL GPS Development");
		REPORT_TYPE_DISPLAY_NAMES.put(LVA_MLL_READINESS, "GL GPS Readiness");
		REPORT_TYPE_DISPLAY_NAMES.put(SHELL_LAR_DEVELOPMENT, "SHELL LOT Development");
		REPORT_TYPE_DISPLAY_NAMES.put(SHELL_LAR_READINESS, "SHELL LOT Readiness");
	}

	public static String getSubTypeFromType(String type) {
		String subType = "";
		if (SHELL_LAR_DEVELOPMENT.equals(type) || LVA_MLL_DEVELOPMENT.equals(type)) {
			subType = SUB_TYPE_DEVELOPMENT;
		} else if (SHELL_LAR_READINESS.equals(type) || LVA_MLL_READINESS.equals(type)) {
			subType = SUB_TYPE_READINESS;
		}
		return subType;
	}
}
