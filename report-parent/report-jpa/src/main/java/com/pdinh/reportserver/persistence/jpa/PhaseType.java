package com.pdinh.reportserver.persistence.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the phase_type database table.
 * 
 */
@Entity
@Table(name = "phase_type")
@NamedQueries({ @NamedQuery(name = "findPhaseByPhaseTypeId", query = "SELECT p FROM PhaseType p WHERE p.phaseTypeId = :phaseTypeId") })
public class PhaseType implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final int CUSTOMIZING = 10;
	public static final int EDITING = 20;
	public static final int REPORTING = 30;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "phase_type_id")
	private int phaseTypeId;

	private String name;

	private String description;

	public PhaseType() {

	}

	public int getPhaseTypeId() {
		return this.phaseTypeId;
	}

	public void setPhaseTypeId(int phaseTypeId) {
		this.phaseTypeId = phaseTypeId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return this.description;
	}
}
