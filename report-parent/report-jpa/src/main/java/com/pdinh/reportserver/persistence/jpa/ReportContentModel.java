package com.pdinh.reportserver.persistence.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * The persistent class for the report_content_model database table.
 * 
 */
@Entity
@Table(name = "report_content_model")
@NamedQueries({
		@NamedQuery(name = "findReportsByParticipantId", query = "Select rcm from ReportContentModel rcm where rcm.participantId = :pid order by rcm.phaseType.phaseTypeId", hints = { @QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE) }),
		@NamedQuery(name = "findReportsByParticipantIdAndPhaseTypeId", query = "Select rcm from ReportContentModel rcm where rcm.participantId = :pid and rcm.phaseType.phaseTypeId = :phaseTypeId", hints = { @QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE) }),
		@NamedQuery(name = "findReportsByParticipantIdAndReportType", query = "Select rcm from ReportContentModel rcm where rcm.participantId = :pid and rcm.reportType = :reportType", hints = { @QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE) }),
		@NamedQuery(name = "findSingleReportByParticipantIdAndType", query = "Select rcm from ReportContentModel rcm where rcm.participantId = :pid and rcm.reportType = :type", hints = { @QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE) }),
		@NamedQuery(name = "findSingleReportByParticipantIdAndTypeAndPhaseTypeId", query = "Select rcm from ReportContentModel rcm where rcm.participantId = :pid and rcm.reportType = :type and rcm.phaseType.phaseTypeId = :phaseTypeId", hints = { @QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE) }) })
public class ReportContentModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "report_content_model_id")
	private int reportContentModelId;

	@Column(name = "participant_id")
	private int participantId;

	@Column(name = "report_type")
	private String reportType;

	@Column(name = "report_xml")
	private String reportXml;

	// bi-directional many-to-one association to phase_type
	@ManyToOne
	@JoinColumn(name = "phase_type_id")
	private PhaseType phaseType;

	public ReportContentModel() {
	}

	public int getReportContentModelId() {
		return this.reportContentModelId;
	}

	public void setReportContentModelId(int reportContentModelId) {
		this.reportContentModelId = reportContentModelId;
	}

	public int getParticipantId() {
		return this.participantId;
	}

	public void setParticipantId(int participantId) {
		this.participantId = participantId;
	}

	public String getReportType() {
		return this.reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getReportXml() {
		return this.reportXml;
	}

	public void setReportXml(String reportXml) {
		this.reportXml = reportXml;
	}

	public PhaseType getPhaseType() {
		return this.phaseType;
	}

	public void setPhaseType(PhaseType phaseType) {
		this.phaseType = phaseType;
	}

}