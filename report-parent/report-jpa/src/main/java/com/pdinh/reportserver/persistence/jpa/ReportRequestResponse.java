package com.pdinh.reportserver.persistence.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * The persistent class for the report_generation_response database table.
 * 
 */
@Entity
@Table(name = "report_request_response")
@NamedQueries({
		@NamedQuery(name = "findRgrsByParticipantId", query = "Select rrr from ReportRequestResponse rrr where rrr.participantId = :pid", hints = { @QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE) }),
		@NamedQuery(name = "findRgrsByParticipantIdAndRgrType", query = "Select rrr from ReportRequestResponse rrr where rrr.participantId = :pid and rrr.rgrType =:type", hints = { @QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE) }),
		@NamedQuery(name = "findSingleByParticipantId", query = "Select rrr from ReportRequestResponse rrr where rrr.participantId = :pid", hints = { @QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE) }),
		@NamedQuery(name = "findSingleByParticipantIdAndRgrType", query = "Select rrr from ReportRequestResponse rrr where rrr.participantId = :pid and rrr.rgrType = :type", hints = { @QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE) }),
		@NamedQuery(name = "findSingleByParticipantIdAndRgrTypeAndTarg", query = "Select rrr from ReportRequestResponse rrr where rrr.participantId = :pid and rrr.rgrType IN :types and (rrr.targLevelIndex = :targ or rrr.targLevelIndex IS null)", hints = { @QueryHint(name = QueryHints.REFRESH, value = HintValues.TRUE) }),
		
		@NamedQuery(name = "findBatchByParticipantIdAndRgrType", query = "SELECT rrr FROM ReportRequestResponse rrr WHERE "
				+ "rrr.participantId IN :pids AND rrr.rgrType = :type", hints = { 
				@QueryHint(name = "eclipselink.batch.type", value="EXISTS")
			}),
		@NamedQuery(name = "findBatchByParticipantIdAndRgrTypeAndTarg", query = "SELECT rrr FROM ReportRequestResponse rrr WHERE "
				+ "rrr.participantId IN :pids AND rrr.rgrType IN :types AND (rrr.targLevelIndex = :targ or rrr.targLevelIndex IS null)", hints = { 
				@QueryHint(name = "eclipselink.batch.type", value="EXISTS")
			})
		})
public class ReportRequestResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	// Value of the RGR type code
	public static final String VIAEDGE_INDIVIDUALS = "VIAEDGE_INDIV";
	public static final String GLGPS_MLL_PRE = "GL_GPS_MLL_PRE_INT";
	public static final String GLGPS_MLL_POST = "GL_GPS_MLL_POST_INT";
	// Note that there is a duplcate copy of these in
	// scorecard-ejb/.../GpsExtractBean. Changes here should also be made there.

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "report_request_response_id")
	private int reportRequestResponseId;

	@Column(name = "participant_id")
	private int participantId;

	@Column(name = "rgr_type")
	private String rgrType;

	// Note that this column is overidden when the object is persisted. The
	// value in the database will always be the date the row was created/last
	// updated.
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "mod_date")
	private Date modifiedDate;
	
	@Column(name = "rgr_xml")
	private String rgrXml;
	
	@Column(name = "target_level_index")
	private Integer targLevelIndex;

	public ReportRequestResponse() {
	}

	public int getReportRequestResponseId() {
		return this.reportRequestResponseId;
	}

	public void setReportRequestResponseId(int reportRequestResponseId) {
		this.reportRequestResponseId = reportRequestResponseId;
	}

	public int getParticipantId() {
		return this.participantId;
	}

	public void setParticipantId(int participantId) {
		this.participantId = participantId;
	}

	public String getRgrType() {
		return this.rgrType;
	}

	public void setRgrType(String rgrType) {
		this.rgrType = rgrType;
	}

	public String getRgrXml() {
		return this.rgrXml;
	}

	public void setRgrXml(String rgrXml) {
		this.rgrXml = rgrXml;
	}

	// --------------------------------
	// Note that while there is a setter for the date attribute, it should be
	// treated as read-only. The persistence methods in the DAO overide
	// whatever value is present in order to make the date the actual
	// "last modified" date (at least from a JPA perspective).
	// --------------------------------
	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date value) {
		this.modifiedDate = value;
	}

	public Integer getTargLevelIndex() {
		return targLevelIndex;
	}

	public void setTargLevelIndex(Integer targLevelIndex) {
		this.targLevelIndex = targLevelIndex;
	}
	
	

}