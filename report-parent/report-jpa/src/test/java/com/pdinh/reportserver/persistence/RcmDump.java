package com.pdinh.reportserver.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.pdinh.reportserver.persistence.jpa.ReportContentModel;

public class RcmDump {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("ReportServer-jpa");
		EntityManager em = emf.createEntityManager();

		List<ReportContentModel> rcms = em.createQuery("select r from ReportContentModel r").getResultList();

		for (ReportContentModel rcm : rcms) {

			System.out.println("RCM ID: " + rcm.getReportContentModelId());
			System.out.println("RCM TYPE: " + rcm.getReportType());
			System.out.println("RCM PHASE TYPE ID: " + rcm.getPhaseType().getPhaseTypeId());
			System.out.println("RCM XML: \n" + rcm.getReportXml());

			System.out.println("=====================================================================\n\n");
		}
	}
}
