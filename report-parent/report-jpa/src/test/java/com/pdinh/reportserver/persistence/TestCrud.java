package com.pdinh.reportserver.persistence;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.pdinh.reportserver.persistence.jpa.ReportContentModel;

public class TestCrud {

   /**
    * @param args
    */
   public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("ReportServer-jpa");
		EntityManager em = emf.createEntityManager();
		
		String output;
      ReportContentModel rcm;

		Iterator<ReportContentModel> rcmi = null;
		
		// ==============
		// ADD
/*
		System.out.println( "Test add" );
		em.getTransaction().begin();
		
		rcm = new ReportContentModel();
		rcm.setParticipantId(1);
		rcm.setReportType("DUMMY_REPORT");
		rcm.setReportXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><ibReport><reportTitle>Report A</reportTitle></ibReport>");
      em.persist(rcm);
      
      em.getTransaction().commit();
  		System.out.println( "Done" );
*/

		// ==============
		// FIND SINGLE
/*
		System.out.println( "Test find single" );
		TypedQuery<ReportContentModel> rcmquery = 
		   em.createNamedQuery("findSingleReportByParticipantIdAndType",ReportContentModel.class);
		rcmquery.setParameter("pid", 1);
		rcmquery.setParameter("type", "DUMMY_REPORT");
		rcm = rcmquery.getSingleResult();
		output = "id: " + rcm.getReportContentModelId() + " type: " + rcm.getReportType() + " xml:" + rcm.getReportXml();
		System.out.println( output );
*/
		// ==============
		// FIND AND DELETE SINGLE
   /*
		System.out.println( "Test find and delete" );
		TypedQuery<ReportContentModel> rcmquery = 
		   em.createNamedQuery("findSingleReportByParticipantIdAndType",ReportContentModel.class);
		rcmquery.setParameter("pid", 1);
		rcmquery.setParameter("type", "DUMMY_REPORT");
		rcm = rcmquery.getSingleResult();
		output = "id: " + rcm.getReportContentModelId() + " type: " + rcm.getReportType() + " xml:" + rcm.getReportXml();
		System.out.println( output );

		em.getTransaction().begin();
		em.remove(rcm);
		em.getTransaction().commit();
		System.out.println( "removed rcm" );
   */
		
		// ================
		// List RCMs by participant

		System.out.println( "list by participant" );
		Query listrcm = em.createNamedQuery("findReportsByParticipantId");
		listrcm.setParameter("pid", 1);
		List<ReportContentModel> list = (List<ReportContentModel>) listrcm.getResultList();
		rcmi = list.iterator();
		while (rcmi.hasNext()) {
         rcm = rcmi.next();
         output = "id: " + rcm.getReportContentModelId() + " type: " + rcm.getReportType() + " xml:" + rcm.getReportXml();
			System.out.println( output );
		}


   }

}
