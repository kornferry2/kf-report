package com.pdinh.messages;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.project.lva.mll.jaxb.LvaDbReport;
import com.pdinh.reportserver.data.dao.jpa.ReportContentModelDao;
import com.pdinh.reportserver.persistence.jpa.ReportContentModel;

/**
 * Message-Driven Bean implementation class for: PptUpdateListener
 * 
 */
@MessageDriven(activationConfig = { @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue") }, mappedName = "jms/PptUpdateQueue")
public class PptUpdateListener implements MessageListener {

	//
	// Beans and resources
	//
	@EJB
	private UserDao userDao;
	@EJB
	private ReportContentModelDao reportContentModelDao;

	/**
	 * Default constructor.
	 */
	public PptUpdateListener() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * onMessage - the business end of a Message-Driven Bean
	 * 
	 * @see MessageListener#onMessage(Message)
	 */
	public void onMessage(Message message) {
		if (message instanceof TextMessage) {
			TextMessage text = (TextMessage) message;
			try {
				System.out.println(this.getClass().getName() + " : " + text.getText());

				updateRcm(text.getText());
			} catch (JMSException e) {
				System.out.println("JMS Error in PptUpdateListener...");
				e.printStackTrace();
			}
		}
	}

	/**
	 * updateRcm - This routine takes the message passed ppt ID and updates the
	 * RCMs associated with that ppt, if necessary.
	 * 
	 * Note that errors should not cause this routine to fail. Errors should be
	 * reported, but they should not fail and bring down the process.
	 * 
	 * @param userId
	 *            - the message-passed participant ID string
	 */
	private void updateRcm(String userId) {
		if (userId == null || userId.trim().length() < 1) {
			System.out.println("Participant ID is null or empty.  RCM update skipped.");
			return;
		}
		// convert the id to a number
		int pptId;
		try {
			pptId = Integer.parseInt(userId);
		} catch (NumberFormatException e) {
			System.out.println("Participant ID (" + userId + ") is an invalid number.  RCM update skipped.");
			return;
		}

		// Get the ppt/user
		User user = userDao.findById(pptId);
		// get the RCM(s) and loop through them
		List<ReportContentModel> rcmList = reportContentModelDao.findByParticipantId(pptId);
		for (ReportContentModel rcm : rcmList) {
			// get the object stack from the xml
			LvaDbReport os = this.unmarshal(rcm.getReportXml());
			// compare the name
			if (user.getFirstname().trim().equals(os.getParticipant().getFirstName().trim())
					&& user.getLastname().trim().equals(os.getParticipant().getLastName())) {
				// The name is the same... Nothing to do
				continue;
			} else {
				// update it and marshal the RCM
				os.getParticipant().setFirstName(user.getFirstname());
				os.getParticipant().setLastName(user.getLastname());
				String xml = this.marshal(os);
				// write out the RCM
				if (xml != null) {
					rcm.setReportXml(xml);
					int kdb = 1;
					reportContentModelDao.update(rcm);
				}
			}
		}
	}

	// Methods to marshal and unmarshal the lvaRbReport object stack
	// TODO Should these be in a utility folder in the same area as the stack
	// definitions?
	/**
	 * Unmarshal an RCM for further use
	 * 
	 * @param xml
	 *            The RCM XML String
	 * @return The LvaDbReport object stack
	 */
	private LvaDbReport unmarshal(String xml) {
		LvaDbReport ret = null;
		try {
			StreamSource ss = new StreamSource(new StringReader(xml));
			JAXBContext ctx = JAXBContext.newInstance(LvaDbReport.class);
			Unmarshaller u = ctx.createUnmarshaller();
			ret = (LvaDbReport) u.unmarshal(ss);
		} catch (JAXBException e) {
			System.out.println("PptUpdateListener: Error unmarshaling RCM to object stack");
			e.printStackTrace();
		}
		return ret;
	}

	private String marshal(LvaDbReport inpOs) {
		try {
			JAXBContext ctx = JAXBContext.newInstance(LvaDbReport.class);
			Marshaller m = ctx.createMarshaller();
			StringWriter sw = new StringWriter();
			m.marshal(inpOs, sw);
			return sw.toString();
		} catch (JAXBException e) {
			System.out.println("PptUpdateListener: Error marshaling to RCM XML");
			e.printStackTrace();
			return null;
		}
	}

}
