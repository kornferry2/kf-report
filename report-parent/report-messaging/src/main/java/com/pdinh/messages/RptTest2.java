package com.pdinh.messages;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Message-Driven Bean implementation class for: RptTest2
 * 
 */
// This works, but is not durable...
// @MessageDriven(activationConfig = { @ActivationConfigProperty(propertyName =
// "destinationType", propertyValue = "javax.jms.Topic") }, mappedName =
// "jms/PalmsProjUpdateTopic")

// Durable
// ---> COMMENT TzHIS OUT
// ---> IF NOT RUNNING THE PU//B/SUB SETUP SCRIPT (test_refresh.bat)
// @MessageDriven(activationConfig = {
// @ActivationConfigProperty(propertyName = "destinationType", propertyValue =
// "javax.jms.Topic"),
// @ActivationConfigProperty(propertyName = "subscriptionDurability",
// propertyValue = "Durable"),
// @ActivationConfigProperty(propertyName = "clientId", propertyValue =
// "TestClientId2"),
// @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue =
// "TestingTopic") }, mappedName = "jms/TestingTopic")
public class RptTest2 implements MessageListener {

	/**
	 * Default constructor.
	 */
	public RptTest2() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see MessageListener#onMessage(Message)
	 */
	public void onMessage(Message message) {
		if (message instanceof TextMessage) {
			TextMessage text = (TextMessage) message;
			try {
				System.out.println("---------------  Recieved - listener 2:  " + this.getClass().getName() + " : "
						+ text.getText() + "  ---------------");
			} catch (JMSException e) {
				e.printStackTrace();
			}
		}
	}

}
