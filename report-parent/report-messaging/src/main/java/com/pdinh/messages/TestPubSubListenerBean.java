package com.pdinh.messages;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Message-Driven Bean implementation class for: TestPubSubListenerBean
 * 
 */
// The "@messageDriven..." annotation is commented out so that it won't break in
// others' environments... client ID and testing Topic not added to
// sun-resources.xml as of yet
//
// This works, but is not durable...
// @MessageDriven(activationConfig = { @ActivationConfigProperty(propertyName =
// "destinationType", propertyValue = "javax.jms.Topic") }, mappedName =
// "jms/PalmsProjUpdateTopic")
//
// This is durable.
// So is there a way to "resource" the clientID property value so we don't need
// to hard code it in each listener? It will still need to match the value in
// the connection factory...
//
// ---> COMMENT THIS OUT
// ---> IF NOT RUNNING THE PUB/SUB SETUP SCRIPT (test_refresh.bat)
// @MessageDriven(activationConfig = {
// @ActivationConfigProperty(propertyName = "destinationType", propertyValue =
// "javax.jms.Topic"),
// @ActivationConfigProperty(propertyName = "subscriptionDurability",
// propertyValue = "Durable"),
// @ActivationConfigProperty(propertyName = "clientId", propertyValue =
// "TestClientId1"),
// @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue =
// "TestingTopic") }, mappedName = "jms/TestingTopic")
public class TestPubSubListenerBean implements MessageListener {

	/**
	 * Default constructor.
	 */
	public TestPubSubListenerBean() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see MessageListener#onMessage(Message)
	 */
	public void onMessage(Message message) {
		if (message instanceof TextMessage) {
			TextMessage text = (TextMessage) message;
			try {
				System.out.println("---------------  Recieved - listener 1:  " + this.getClass().getName() + " : "
						+ text.getText() + "  ---------------");
			} catch (JMSException e) {
				e.printStackTrace();
			}
		}
	}
}
