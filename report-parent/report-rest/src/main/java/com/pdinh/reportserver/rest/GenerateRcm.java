package com.pdinh.reportserver.rest;



import java.io.IOException;
import java.net.URISyntaxException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.script.ScriptException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import org.w3c.dom.Document;

import ch.qos.logback.core.status.Status;

import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.project.kfap.KfapReportRgrToRcm;
import com.pdinh.project.kfap.KfapReportRgrToRcmV2;

@Stateless
@Path("/generateRcm")
public class GenerateRcm {

    @EJB
    private KfapReportRgrToRcm kfapReportRgrToRcmBean;
    
    @EJB
    private KfapReportRgrToRcmV2 kfapReportRgrToRcmBeanV2;

    /**
     * Default constructor.
     */
    public GenerateRcm() {
    }




    @POST
    @Consumes("application/xml")
    @Produces("application/xml")
    @Path("projects/{proj}/participants/{pid}/targetLevels/{targ}/reportTypes/{type}/lang={langCode}")
    public Response processParticipant(Document scoreDoc, @PathParam("pid") String pid,
                                       @PathParam("proj") String proj, @PathParam("langCode") String langCode, @PathParam("type") String type, @PathParam("targ") String targ) throws ScriptException, URISyntaxException, XMLStreamException, JAXBException, InterruptedException, IOException, PalmsException {
    	
    	String rcm = "";
    	boolean found = false;
    	
    	if(ReportType.KFP_INDIVIDUAL.equals(type)) {    	
    		rcm = kfapReportRgrToRcmBean.generateReportRcm(scoreDoc, type, proj, pid, langCode, targ);
    		found = true;
    	} else if (ReportType.ALP_IND_V2.equals(type)) {
    		rcm = kfapReportRgrToRcmBeanV2.generateReportRcm(scoreDoc, type, proj, pid, langCode, targ);
    		found = true;
    	} 

    	Response response = null;
    	
    	if(found == false) {
    		response = Response.status(Response.Status.NOT_FOUND).entity("Report type not found: " + type).type(MediaType.TEXT_PLAIN).build();
    	} else {	    	
    		response = Response.ok(rcm,MediaType.APPLICATION_XML).build();
    	}
    	
    	return response;
    }
    
    
    
    

	public KfapReportRgrToRcm getKfapReportRgrToRcmBean() {
		return kfapReportRgrToRcmBean;
	}

	public void setKfapReportRgrToRcmBean(KfapReportRgrToRcm kfapReportRgrToRcmBean) {
		this.kfapReportRgrToRcmBean = kfapReportRgrToRcmBean;
	}

	public KfapReportRgrToRcmV2 getKfapReportRgrToRcmBeanV2() {
		return kfapReportRgrToRcmBeanV2;
	}

	public void setKfapReportRgrToRcmBeanV2(
			KfapReportRgrToRcmV2 kfapReportRgrToRcmBeanV2) {
		this.kfapReportRgrToRcmBeanV2 = kfapReportRgrToRcmBeanV2;
	}

}
