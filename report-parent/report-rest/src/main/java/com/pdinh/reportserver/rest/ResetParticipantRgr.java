package com.pdinh.reportserver.rest;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.PositionDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.reportserver.data.dao.jpa.ReportRequestResponseDao;

@Stateless
@Path("/resetrgr")
public class ResetParticipantRgr {
	private static final Logger logger = LoggerFactory.getLogger(ResetParticipantRgr.class);

	@EJB
	private ReportRequestResponseDao reportRequestResponseDao;

	@EJB
	private UserDao userDao;

	@EJB
	private PositionDao positionDao;

	@POST
	@Path("{participantId}/{rgrTypeCode}")
	@Produces({ javax.ws.rs.core.MediaType.TEXT_PLAIN })
	public Response resetParticipantRgr(@PathParam("rgrTypeCode") String rgrTypeCode,
			@PathParam("participantId") int participantId) {
		try {
			logger.debug("Deleting rgr: {}, for participant: {}", rgrTypeCode, participantId);
			reportRequestResponseDao.removeByParticipantIdAndType(participantId, rgrTypeCode);

		} catch (Exception e) {
			// logger.error("Caught some exception while deleting RGR: {}",
			// e.getMessage());
			logger.error("Caught some exception while deleting RGR", e);

		}

		return Response.ok("OK").build();
	}
}
