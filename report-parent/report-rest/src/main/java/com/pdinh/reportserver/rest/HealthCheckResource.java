package com.pdinh.reportserver.rest;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Stateless
@Path("/healthcheck")
public class HealthCheckResource {
	//Called from ReportRequestResource in palms admin to determine
		//if server is up
		//TODO:  do more health checking.. 
		@GET
		@Path("/ping")
		@Produces({javax.ws.rs.core.MediaType.TEXT_PLAIN})
		public Response ping(){
			return Response.ok("OK").build();
		}
}
