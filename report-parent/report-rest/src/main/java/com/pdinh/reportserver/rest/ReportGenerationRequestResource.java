package com.pdinh.reportserver.rest;

import java.io.IOException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.xml.bind.JAXBException;

import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
//import com.pdinh.project.generic.GenericRgrBeanR;

@Stateless
@Path("/reportGenerationRequest")
public class ReportGenerationRequestResource {
	@EJB
	private com.pdinh.project.generic.GenericRgrBean genericRgrBean;
	
//	@EJB
//	private GenericRgrBeanR genericRgrBeanR;

	/**
	 * Default constructor.
	 */
	public ReportGenerationRequestResource() {
	}

	
	/**
	 * Retrieves representation of an instance of raw scores
	 * 
	 * @return an instance of String
	 */

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	@Path("{instCode}/{file}")
	public ReportGenerationRequest processJsonData(ReportGenerationRequest rgr, @PathParam("instCode") String instCode,
			@PathParam("file") String file) {
        //try {
            return this.getGenericRgrBean().createScoredRgr(instCode, file + ".xml", rgr);
        //} catch (JAXBException e) {
        //    e.printStackTrace();
        //}
        //return null;
    }
	
	@POST
	@Consumes("application/xml")
	@Produces("application/xml")
	@Path("{instCode}/{file}")
	public ReportGenerationRequest processXMLData(ReportGenerationRequest rgr, @PathParam("instCode") String instCode,
			@PathParam("file") String file) {
        //try {
            return this.getGenericRgrBean().createScoredRgr(instCode, file + ".xml", rgr);
        //} catch (JAXBException e) {
        //    e.printStackTrace();
        //}
        //return null;
    }
	
//	@POST
//	@Consumes("application/xml")
//	@Produces("application/xml")
//	@Path("{instCode}/{file}/r")
//	public ReportGenerationRequest processXMLDataR(ReportGenerationRequest rgr, @PathParam("instCode") String instCode,
//			@PathParam("file") String file) throws JAXBException, IOException {
//
//		return this.getGenericRgrBeanR().createScoredRgr(instCode, file + ".xml", rgr);
//	}

	private com.pdinh.project.generic.GenericRgrBean getGenericRgrBean() {
		return genericRgrBean;
	}


//	public GenericRgrBeanR getGenericRgrBeanR() {
//		return genericRgrBeanR;
//	}
//
//
//	public void setGenericRgrBeanR(GenericRgrBeanR genericRgrBeanR) {
//		this.genericRgrBeanR = genericRgrBeanR;
//	}
//

}
