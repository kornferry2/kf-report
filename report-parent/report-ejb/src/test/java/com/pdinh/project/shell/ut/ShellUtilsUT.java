/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.shell.ut;

import java.io.File;

import com.pdinh.pipeline.dto.StatusDTO;
import com.pdinh.project.shell.utils.ShellUtils;

import junit.framework.TestCase;

public class ShellUtilsUT extends TestCase
{
	public ShellUtilsUT(String name)
	{
		super(name);
	}
	
	/*
	 * testXmlValidateString
	 * 
	 * Tests RGR
	 */
//	public void testXmlValidateString()
//		throws Exception
//	{
//		System.out.println("Testing xmlValidate for RGR (String)...");
//		
//		String xmlPath = "../CalculationPipeline/CalculationEngineResources/testing/shellScorecard/shell-tc1.xml";
//
//		File ff = new File(xmlPath);
////		System.out.println("XML file:");
////		try
////		{
////			System.out.println("  Abs   - " + ff.getAbsolutePath());
////			System.out.println("  Canon - " + ff.getCanonicalPath());
////		}
////		catch (IOException e)
////		{
////			System.out.println("UT: Unable to fetch file named... Terminating.");
////			return;
////		}
//		if(! ff.exists())
//		{
//			System.out.println("UT: File does not exist.  Path=" + xmlPath);
//		}
//		
//		// Read the XML data from the file
//		byte[] ba = new byte[(int)ff.length()];
//		BufferedInputStream bis = null;
//		try
//		{
//			bis = new BufferedInputStream(new FileInputStream(ff));
//			bis.read(ba);
//		}
//		finally
//		{
//	        if (bis != null) try { bis.close(); } catch (IOException ignored) { }
//	    }
//		String xml = new String( ba);
//		
//		StatusDTO ret = ShellUtils.validateXml(ShellUtils.RQR_TYPE, xml);
//
//		System.out.println("String-based Validate succeded? " + ret.isSuccess() + (ret.isSuccess() ? "" : "  Err-" + ret.getErrorMsg()));
//	}


	/*
	 * testXmlValidateFile
	 * 
	 * Tests RGR
	 */
//	public void testXmlValidateFile()
//		throws Exception
//	{
//		System.out.println("Testing xmlValidate for RGR (File)...");
//		
//		//String xmlPath = "../CalculationPipeline/CalculationEngineResources/testing/shellScorecard/shell-tc1.xml";
//		String xmlPath = "../CalculationPipeline/CalculationEngineResources/testing/shellScorecard/shell-tc1.xml";
//
//		File ff = new File(xmlPath);
////		System.out.println("XML file:");
////		try
////		{
////			System.out.println("  Abs   - " + ff.getAbsolutePath());
////			System.out.println("  Canon - " + ff.getCanonicalPath());
////		}
////		catch (IOException e)
////		{
////			System.out.println("UT: Unable to fetch file specified... Terminating.");
////			return;
////		}
			//// Let the util catch the "not exist"
////		if(! ff.exists())
////		{
////			System.out.println("UT: File does not exist.  Path=" + xmlPath);
////		}
//		
//		StatusDTO ret = ShellUtils.validateXml(ShellUtils.RQR_TYPE, ff);
//
//		System.out.println("File-based Validate succeded? " + ret.isSuccess() + (ret.isSuccess() ? "" : "  Err-" + ret.getErrorMsg()));
//	}


	/*
	 * testXmlValidateFile
	 * 
	 * Tests RGR
	 */
	public void testXmlValidateFile()
		throws Exception
	{
		System.out.println("Testing xmlValidate for RCM (File)...");

		// Use valid xml on your box
		String xmlPath = "C:/Users/kbeukelm/Workspace/Indigo/ReportServer-web/src/com/pdinh/rs/resource/ib_report_refactor.xml";
		File ff = new File(xmlPath);
		//System.out.println("XML file:");
		//try
		//{
		//	System.out.println("  Abs   - " + ff.getAbsolutePath());
		//	System.out.println("  Canon - " + ff.getCanonicalPath());
		//}
		//catch (IOException e)
		//{
		//	System.out.println("UT: Unable to fetch file specified... Terminating.");
		//	return;
		//}
		//// Let the util catch the "not exist"
		//if(! ff.exists())
		//{
		//	System.out.println("UT: File does not exist.  Path=" + xmlPath);
		//}
		
		// Pull this from the properties... Can't inject a resource into a non-bean
		String calculationEngineResources = "C:/Users/kbeukelm/Workspace/Indigo/CalculationEngineResources/";
		StatusDTO ret = ShellUtils.validateXml(calculationEngineResources, ShellUtils.SHELL_LAR_RCM_TYPE, ff);

		System.out.println("File-based Validate succeded? " + ret.isSuccess() + (ret.isSuccess() ? "" : "  Err-" + ret.getErrorMsg()));
	}
}
