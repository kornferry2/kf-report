/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.shell.ut;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.pdinh.core.vo.Score;
import com.pdinh.core.vo.Participant;
import com.pdinh.project.shell.vo.Behavior;
import com.pdinh.project.shell.vo.IbReport;
import com.pdinh.project.shell.vo.Level;
import com.pdinh.project.shell.vo.Module;
import com.pdinh.project.shell.vo.Suggestion;
import com.pdinh.project.shell.vo.Section;
import com.sun.xml.txw2.IllegalAnnotationException;

/*
 * Test the JAXB annotations (creating XML from POJOs) for the Shell Report
 * Content Model. 
 * 
 * Generally used to test that we have the class annotations right.
 * 
 * NOT a JUnit test
 */
public class TestShellRcmMarshal {
	public static void main(String args[]) {
		System.out.println("Start Shell RCM marshalling (POJO to XML) tests...");

		// create the object structure
		IbReport ibr = new IbReport();
		ibr.setReportTitle("Leadership Assessment Report");
		ibr.setLevel("Leader of Teams");
		ibr.setAssessmentDate("2011-01-11");
		ibr.setScoringDate("2011-01-11");
		ibr.setReportDate("2011-01-11");
		ibr.setCompany("Shell");

		Participant pp = new Participant();
		pp.setId(12345);
		pp.setExtId("12345");
		pp.setFirstName("Samantha");
		pp.setLastName("Sample");
		pp.setTitle("Assistant Plant Manager");
		ibr.setParticipant(pp);
		//
		// Overview ov = new Overview("intermediate",
		// "Ms Sample performed at the Intermediate Level on the Leader of Teams Assessment.� She collaborates effectively with internal and external stakeholders, and leads with a strong set of personal values.� She would benefit from a greater focus on driving innovation and getting results through others.");
		// ibr.setOverview(ov);

		Section sec = new Section("authenticity", "Authenticity");
		ibr.getSection().add(sec);
		sec.setScore(new Score(3, "solid"));

		Level lvl = new Level("effective");
		sec.getLevel().add(lvl);
		lvl.getPoint().add("Consistently demonstrates values and beliefs through action");
		lvl.getPoint().add("Has a high level of clarity on the source of personal motivation");
		lvl.getPoint().add("Adroitly balances honest self-expression with being open to other perspectives");
		lvl = new Level("competent");
		sec.getLevel().add(lvl);
		lvl.getPoint().add("Models personal well-being");
		lvl = new Level("development");
		sec.getLevel().add(lvl);
		lvl.getPoint().add("Displays hesitancy to speak difficult truths");

		sec.setComments("Ms Sample took many opportunities to convey her values as a leader to her team.� She leads with integrity.� She is not someone who needs to have the right answers herself, but rather is very open to other points of view and perspectives.� At the same time, she will readily express her opinion.� She comes across as a leader who is comfortable in her own skin.");

		// sec.getPoint().add("Hold firm to be influential in your organization, you do not want to be seen as pushover or as stubborn and inflexible. It will always be important to pick your battles carefully. In some situations, you will need to maintain your position against the attempts to sway you.");
		// sec.getPoint().add("Find out about the other person�s concerns. To discover any potential misunderstandings you may want to ask the other person to explain your viewpoint, while you offer to explain his or hers. Restate your main points to check you are being understood.");
		// sec.getPoint().add("Suggested Instant Advice module <a href=\"http://www.pdinh.com/module?id=1\">Building Self Confidence</a>");
		// sec.getPoint().add("Challenge yourself to be outspoken and clear about things that are hard for you to say.  Make sure you first practice this in a safe environment.");
		Behavior beh = new Behavior("AUTH_1");
		sec.getBehavior().add(beh);
		beh.getSuggestion()
				.add(new Suggestion(
						"Don't be swayed by what others would like you to say. It may be easier to shade the truth or only tell people part of the story, but the consequences will reflect on you."));
		beh.getSuggestion()
				.add(new Suggestion(
						"Give truthful answers to questions and challenges. Give the requested information if it is available, or provide the information when it becomes available. If you are not able to reveal the information, explain why."));
		beh.getSuggestion()
				.add(new Suggestion(
						"Be consistent. If people know that you will be truthful even when it is difficult, they will respect and believe you. If they think you are telling people what they want to hear, they will think you are simply trying to protect yourself."));

		beh.getModule().add(new Module("Maintaining professional integrity", "http://www.bogus.com?module_id=220"));
		beh.getModule().add(new Module("Fostering trust in teams", "http://www.bogus.com?module_id=149"));
		beh.getModule().add(new Module("Talking heart-to-heart", "http://www.bogus.com?module_id=69"));

		beh = new Behavior("AUTH_2");
		sec.getBehavior().add(beh);
		beh.getSuggestion()
				.add(new Suggestion(
						"Ask for feedback from your colleagues about whether your actions are consistent with your words. Ask for examples, but do not put people on the spot."));
		beh.getSuggestion()
				.add(new Suggestion(
						"Be aware of your motives. Unless you are honest with yourself about what you want or need, you may inadvertently send mixed messages."));
		beh.getSuggestion()
				.add(new Suggestion(
						"Become aware of how you talk about policies and practices. Do you refer to them in a calm, straightforward way, or do you show disrespect, mock them, and tease people who follow them? Remember that people base their perceptions on your words and actions."));
		beh.getModule().add(new Module("Maintaining professional integrity", "http://www.bogus.com?module_id=220"));
		beh.getModule().add(new Module("Ways to gain respect", "http://www.bogus.com?module_id=104"));
		beh.getModule().add(new Module("Building trust", "http://www.bogus.com?module_id=471"));

		beh = new Behavior("AUTH_4");
		sec.getBehavior().add(beh);
		beh.getSuggestion()
				.add(new Suggestion(
						" If team members seem uncomfortable encouraging different viewpoints to be expressed, stage a practice session in which each team member is assigned a particular point of view and asked to defend it during a group discussion. Afterwards, talk about what worked well and what needed improvement."));
		beh.getSuggestion()
				.add(new Suggestion(
						"Create team norms for discussions to ensure that all participants contribute equally. Include listening actively, not criticizing, asking others questions, and contributing ideas."));
		beh.getSuggestion()
				.add(new Suggestion(
						"Monitor how you respond to people who have viewpoints different from your own. Note when you feel resistant to others' ideas and assess why you might have had this reaction."));
		beh.getModule().add(new Module("Disagreement on process", "http://www.bogus.com?module_id=123"));
		beh.getModule().add(new Module("Dealing with individual conflict", "http://www.bogus.com?module_id=99"));
		beh.getModule().add(new Module("Building agreement", "http://www.bogus.com?module_id=482"));
		beh.getModule().add(new Module("Put yourself in their shoes", "http://www.bogus.com?module_id=284"));
		beh.getModule().add(new Module("Turning opponents into allies", "http://www.bogus.com?module_id=339"));

		sec = new Section("growth", "Growth");
		ibr.getSection().add(sec);
		sec.setScore(new Score(1, "novice"));

		lvl = new Level("effective");
		sec.getLevel().add(lvl);
		lvl = new Level("competent");
		sec.getLevel().add(lvl);
		lvl.getPoint().add("Aligns team targets to business goals.");
		lvl.getPoint().add("Visibly intervenes to manage safety and compliance risks.");
		lvl.getPoint().add("Seeks opportunities to enhance the reputation of the business.");
		lvl = new Level("development");
		sec.getLevel().add(lvl);
		lvl.getPoint().add("Identifies conventional, status quo approaches");
		lvl.getPoint().add("Misses opportunities to encourage the team to apply ideas from outside the business");
		lvl.getPoint().add("Misses opportunities to harness creative ideas and approaches from the team");
		lvl.getPoint().add("Allows resistance to slow down or stall change efforts");

		sec.setComments("Ms Sample took a cautious and conventional approach in handling the issues in the simulation.� She consistently avoided opportunities to apply and leverage innovative thinking, even when suggested by others.� In the one situation where she seemed inclined to take a bolder approach, she backed down when faced with resistance.� She is likely quite risk-averse.");

		// sec.getPoint().add("To increase your creative thinking, you need to challenge your thoughts and assumptions. This may mean being more open to ideas and feedback from others, or it may involve more sophisticated skills in challenging your underlying beliefs.");
		// sec.getPoint().add("Get into the habit of identifying and challenging judgements.");
		// sec.getPoint().add("Listen to new employees. They often challenge the assumptions and work process. Use their observations to revitalize and improve business processes.");
		// sec.getPoint().add("Suggested Instant Advice module <a href=\"http://www.pdinh.com/module?id=2\">Understand the Creative Process</a>");
		beh = new Behavior("GROW_1");
		sec.getBehavior().add(beh);
		beh.getSuggestion()
				.add(new Suggestion(
						" Obtain copies of the corporate and/or business line's vision, goals, and strategies. If you were not involved in their development, talk with people who were involved to better understand the direction."));
		beh.getSuggestion()
				.add(new Suggestion(
						"Share your understanding of the vision, goals, and strategies with your team. Provide the background information so the direction makes sense. It is often useful for the team to know the strategic options discussed so they will have a richer understanding of the direction chosen."));
		beh.getSuggestion()
				.add(new Suggestion(
						"Once you have decided the vision, goals, and strategies for the business unit or team, review them against the criteria your team should be using to ensure accomplishment of the corporate and business unit goals. Look for alignment. Where alignment is missing, determine if there is good reason for it. "));
		// beh.getModule().add(new Module("0", "122",
		// "When your manager disagrees on goals"));
		beh.getModule().add(new Module("When your manager disagrees on goals", "http://www.bogus.com?module_id=122"));
		beh.getModule().add(new Module("Developing a mission statement", "http://www.bogus.com?module_id=394"));
		beh.getModule().add(new Module("Creating a project plan", "http://www.bogus.com?module_id=483"));
		beh.getModule().add(new Module("Start with a project plan", "http://www.bogus.com?module_id=392"));

		beh = new Behavior("GROW_2");
		sec.getBehavior().add(beh);
		beh.getSuggestion()
				.add(new Suggestion(
						" Regularly challenge yourself and your staff to look at your business from perspectives outside of your organization. You may find new opportunities to create value."));
		beh.getSuggestion()
				.add(new Suggestion(
						"Study past innovations in your organization, industry, and marketplace. What prompted the innovation? What could spark a new innovation today?"));
		beh.getSuggestion()
				.add(new Suggestion(
						"Learn from successful practices at other organizations. Do some research on your own. Then discuss best practices with people at your organization and determine how you can adapt them for your situation."));
		beh.getModule().add(new Module("Find opportunitites for innovation", "http://www.bogus.com?module_id=326"));
		beh.getModule().add(
				new Module("Non-linear approaches to problem-solving", "http://www.bogus.com?module_id=442"));
		beh.getModule().add(new Module("Being the best in your field", "http://www.bogus.com?module_id=201"));

		beh = new Behavior("GROW_3");
		sec.getBehavior().add(beh);
		beh.getSuggestion()
				.add(new Suggestion(
						" Regularly challenge yourself and your staff to look at your business from perspectives outside of your organization. You may find new opportunities to create value."));
		beh.getSuggestion()
				.add(new Suggestion(
						"Study past innovations in your organization, industry, and marketplace. What prompted the innovation? What could spark a new innovation today?"));
		beh.getSuggestion()
				.add(new Suggestion(
						"Learn from successful practices at other organizations. Have your team track down information. Then discuss best practices with people at your organization and determine how you can adapt them for your situation."));
		beh.getModule().add(new Module("Find opportunitites for innovation", "http://www.bogus.com?module_id=326"));
		beh.getModule().add(
				new Module("Non-linear approaches to problem-solving", "http://www.bogus.com?module_id=442"));
		beh.getModule().add(new Module("Being the best in your field", "http://www.bogus.com?module_id=201"));

		beh = new Behavior("GROW_4");
		sec.getBehavior().add(beh);
		beh.getSuggestion()
				.add(new Suggestion(
						" Take advantage of the perspectives that new employees bring. Ask them to point out assumptions made in the organization and things that people take for granted."));
		beh.getSuggestion()
				.add(new Suggestion(
						"Talk to experts from several areas when you are developing new ideas. This will help you see relationships between ideas that you, and they, might otherwise overlook.  "));
		beh.getSuggestion()
				.add(new Suggestion(
						"Make a point of talking with people whose perspectives differ from your own. Look at it as an opportunity to learn more and to explore new ways of thinking. "));
		beh.getModule().add(
				new Module("Non-linear approaches to problem-solving", "http://www.bogus.com?module_id=442"));
		beh.getModule().add(new Module("Increasing group creativity", "http://www.bogus.com?module_id=444"));

		beh = new Behavior("GROW_5");
		sec.getBehavior().add(beh);
		beh.getSuggestion()
				.add(new Suggestion(
						" Recognize that some people will resist examining problems and processes. Learn why they are resistant. For example, people may believe their work will get more difficult, that it will become more complicated, or that they will lose control over a process."));
		beh.getSuggestion()
				.add(new Suggestion(
						"Establish a norm that people first discuss what they like about an idea. (They will have plenty of time later to focus on what they dislike.)"));
		beh.getSuggestion()
				.add(new Suggestion(
						"Support individuals who challenge assumptions and question the way things are done. Publicly recognize them and point out tangible benefits from their ideas."));
		beh.getModule().add(new Module("Brainstorming ground rules", "http://www.bogus.com?module_id=443"));
		beh.getModule().add(new Module("Break the status quo", "http://www.bogus.com?module_id=328"));
		beh.getModule().add(new Module("Piloting new ideas", "http://www.bogus.com?module_id=327"));
		beh.getModule().add(
				new Module("Leadership strategies (Encouraging Innovation)", "http://www.bogus.com?module_id=332"));
		beh.getModule().add(
				new Module("Getting over hurdles of innovation (Basic-level content)",
						"http://www.bogus.com?module_id=171"));

		beh = new Behavior("GROW_6");
		sec.getBehavior().add(beh);
		beh.getSuggestion()
				.add(new Suggestion(
						"Avoid \"analysis paralysis.\" Understand the urgency of the situation and set a deadline for analyzing your information. Prioritize your greatest concerns, and spend your time on those issues. "));
		beh.getSuggestion()
				.add(new Suggestion(
						"Rather than insisting on certainty in the decision-which is likely to be impossible in any case-anticipate likely problems and plan for contingencies."));
		beh.getSuggestion()
				.add(new Suggestion(
						"Most people do not procrastinate every time they make a decision; rather, they tend to delay decisions under certain circumstances. It's useful to identify those circumstances and determine how you can handle them in the future. "));
		beh.getModule().add(new Module("Making timely decisions", "http://www.bogus.com?module_id=316"));
		beh.getModule().add(
				new Module("Making decisions in risky or uncertain times", "http://www.bogus.com?module_id=317"));
		beh.getModule().add(new Module("Decision making (Basic-level content)", "http://www.bogus.com?module_id=13"));

		beh = new Behavior("GROW_7");
		sec.getBehavior().add(beh);
		beh.getSuggestion()
				.add(new Suggestion(
						"When someone's behavior blatantly contradicts ethical standards or corporate policies, swift action is necessary to signal to the individual and the organization that certain behaviors won't be tolerated.  Talk to the employee about what you observed or heard. Be very specific about the principle or policy that is involved and the behavior that violated it. Learn the reasons why the employee acted in this manner."));
		beh.getSuggestion()
				.add(new Suggestion(
						" Consistently follow practices and policies. At times, you may work with individuals or groups who want to take short cuts, apply them selectively, or ignore them all together. Stand firm and encourage people to be consistent."));
		beh.getSuggestion()
				.add(new Suggestion(
						"Become aware of how you talk about policies and practices. Do you refer to them in a calm, straightforward way, or do you show disrespect, mock them, and tease people who follow them? Remember that people base their perceptions on your words and actions."));
		beh.getModule().add(new Module("Maintaining professional integrity", "http://www.bogus.com?module_id=220"));
		beh.getModule().add(new Module("Ways to gain respect", "http://www.bogus.com?module_id=104"));

		// Marshall it
		JAXBContext jc = null;
		Marshaller marshaller = null;
		StringWriter sw = null;
		try {
			System.out.println("Set up the JAXBContext...");
			jc = JAXBContext.newInstance(IbReport.class);
			marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); // Make
																			// pretty
																			// output
			sw = new StringWriter();
			marshaller.marshal(ibr, sw);
			// Dump the output to screen
			System.out.print("Marshalling complete...  ");
			String str = (sw == null) ? "Failed... no output" : "Output XML:\n" + sw.toString();
			System.out.println(str);
		} catch (IllegalAnnotationException e) {
			System.out.println("Illegal annotation:");
			System.out.println(e.toString());
			return;
		} catch (JAXBException e) {
			System.out.println("JAXBException detected... Terminating.  Msg=" + e.getMessage());
			e.printStackTrace();
			return;
		} finally {
			if (sw != null) {
				try {
					sw.close();
				} catch (Exception e) {
					System.out.println("Exception on StringWriter close in TestShellRcmMarshal.main().  Ignored.  Msg="
							+ e.getMessage());
				}
				sw = null;
			}
			marshaller = null;
			jc = null;
			ibr = null;
		}
	}

}
