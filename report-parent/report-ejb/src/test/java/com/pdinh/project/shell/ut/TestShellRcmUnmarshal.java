/**
* Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
* All rights reserved.
*/
package com.pdinh.project.shell.ut;

import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.pdinh.project.shell.vo.IbReport;

/*
 * Test the unmarshalling of the ReportGenerationRequest
 * 
 * Note:  This implementation uses the @XmlAnyType annotation in the Suggestions object to handle both
 * 		  ModuleRef and Point objects (objects defined with a single attribute and no other content).
 * 		  This may change as the design evolves.  In that mode the results of the unmarshal for the
 * 		  "Suggestions" object are DOM "Element" objects rather that the object type (Point or ModuleRef)
 * 		  that was originally input.  A little work is required to get the data out, but it is do-able
 * 		  (see the toString() method in the Suggestions object).
 */

public class TestShellRcmUnmarshal
{
	/*
	 * Test the JAXB annotations (creating XML from POJOs) for the Shell RCM.
	 * 
	 * Generally used to test that we have the class annotations right.
	 * No validation is performed
	 */
	public static void main(String args[])
	{
		System.out.println("Starting Shell RCM (IbReport) unmarshal test...");

		File ff = null;
		//FileInputStream fis = null;
		FileReader fr = null;
		JAXBContext jc = null;
		Unmarshaller um = null;
		IbReport ibr = null;

		// Get the parameter (test input file file path)
		if (args.length < 1)
		{
			System.out.println("No test file path passed.  Terminating...");
			return;
		}
		
		String testFilePath = args[0];
		System.out.println("Unmarshalling from " + testFilePath);

		// Set up for unmarshalling
		ff = new File(testFilePath);
		try
		{
			//System.out.println("  passed=" + args[0]);
			//System.out.println("  file.name=" + ff.getName());
			//System.out.println("  file.path=" + ff.getPath());
			System.out.println("  file.parent=" + ff.getParent());
			System.out.println("  Abs=" + ff.getAbsolutePath());
			System.out.println("  Canonical=" + ff.getCanonicalPath());
		}
		catch (IOException e)
		{
			System.out.println("Unable to get file name.");
			e.printStackTrace();
			return;
		}
		
		//Unmarshal
		try
		{
			//fis = new FileInputStream(ff);
			fr = new FileReader(ff);

			// Create the context & the unmarshaller
			jc = JAXBContext.newInstance(IbReport.class);
			um = jc.createUnmarshaller(); 
			System.out.println("Unmarshalling...");
			//ibr = (IbReport)um.unmarshal(fis); 
			ibr = (IbReport)um.unmarshal(fr); 

			System.out.println("Unmarshal complete... output:\n" + ibr.toString());
			return;
		}
		catch (FileNotFoundException e)
		{
			System.out.println("Input file not found; terminating...  File name=" + ff);
			return;
		}
		catch (JAXBException e)
		{
			// Setting up context or the unmarshaller
			System.out.println("JAXBException; terminating...  msg=" + e.getMessage());
			e.printStackTrace();
			return;
		}
		finally
		{
			// Clean up
			ibr = null;
			um = null;
			jc = null;
//			if (fis != null)
//			{
//				try { fis.close(); }
//				catch (Exception e) { /* Swallow this guy */ }
//			}
			if (fr != null)
			{
				try { fr.close(); }
				catch (Exception e) { /* Swallow this guy */ }
			}
			ff = null;
		}
	}

}
