/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/*
 * This is the third level JAXB class for the boilerplate data that is present in the
 * Appendix to the dashboard reports.  It is used in the AppSection JAXB class.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"title", "text", "appTextGroup"})
public class AppSubSection
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String title;
	private String text;	// For when there is not a textGroup just a text
	@XmlElementWrapper(name = "appendixTextGroups")
	private List<AppTextGroup> appTextGroup;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public AppSubSection()
	{
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "    AppSubSection:  ";
		str += "title=" + this.title +", text=" + this.text;
		// May not appear
		for (AppTextGroup atg : this.getAppTextGroup())
		{
			str += "\n      " + atg.toString();
		}
		
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getTitle()
	{
		return this.title;
	}

	public void setTitle(String value)
	{
		this.title = value;
	}

	//----------------------------------
	public String getText()
	{
		return this.text;
	}

	public void setText(String value)
	{
		this.text = value;
	}

	//----------------------------------
	// Note that the list has a "get" only
	public List<AppTextGroup> getAppTextGroup()
	{
		if (this.appTextGroup == null)
			this.appTextGroup = new ArrayList<AppTextGroup>();
		
		return this.appTextGroup;
	}
}
