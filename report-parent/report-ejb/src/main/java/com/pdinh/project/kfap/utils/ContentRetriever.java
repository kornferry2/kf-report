package com.pdinh.project.kfap.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pdinh.exception.PalmsException;
import com.pdinh.exception.RCMGenerationException;
import com.pdinh.report.util.ContentServiceUtil;

@Stateless
public class ContentRetriever {
	@Resource(name = "custom/marklogic", type = Properties.class)
	private Properties markLogicProperties;

	@Resource(mappedName = "application/report/config_properties")
	private Properties configProperties;

	private static final Logger log = LoggerFactory.getLogger(ContentRetriever.class);

	// private final String CONTENT_STRING_PARAMS =
	// "{contentVer}/contentDictionary/manifests/projects.internal.kfap{rptVer}.{reportType}?"
	// + "lang={lang}";

	private final String CONTENT_STRING_PARAMS = "{contentVer}/contentDictionary/manifests/projects.internal.kfap{rptVer}.{reportType}?"
			+ "lang={lang}";

	private final String QUESTION_STRING_PARAMS = "modules/question-bank-2.xqy?assessmentCode={assessmentCode}&lang={lang}&format={format}";

	private String host = "";
	private String port = "";
	private String username = "";
	private String password = "";

	private ContentServiceUtil contentServiceUtil = new ContentServiceUtil();

	// public JsonNode getContent(String contentVer, String rptVer, String
	// rptType, String lang) throws PalmsException {

	// setupAuthentication("rest");

	// String urlBase = "http://" + getHost() + ":" + getPort() + "/";
	// String contentUrl = null;

	// contentUrl = urlBase
	// + CONTENT_STRING_PARAMS.replace("{contentVer}", contentVer +
	// "").replace("{rptVer}", rptVer + "")
	// .replace("{reportType}", rptType + "").replace("{lang}", lang);

	// System.out.println("Connecting the marklogic rest server... " +
	// contentUrl);

	/**
	 * getReportContent - Fetches the published CMS content for the KFALP
	 * reports. Thin method that properly sets up the parameters and then calls
	 * the "real" method to do the work.
	 * 
	 * @param contentFolder
	 * @param lang
	 * @return
	 * @throws PalmsException
	 */
	public JsonNode getReportContent(String contentFolder, String lang) throws PalmsException {
		String urlBase = configProperties.getProperty("reportserverBaseUrl");
		String contentUrl = null;
		contentUrl = urlBase + "/projects/kfap/" + contentFolder + "/reportContent/" + lang + ".json";
		return getContent(contentUrl);
	}

	/**
	 * getGrpExtractContent - Fetches the published CMS content for the KFALP
	 * Group Extract.. Thin method that properly sets up the parameters and then
	 * calls the "real" method to do the work.
	 * 
	 * @param contentFolder
	 * @param lang
	 * @return
	 * @throws PalmsException
	 */
	public JsonNode getGrpExtractContent(String contentFolder, String lang) throws PalmsException {
		String urlBase = configProperties.getProperty("reportserverBaseUrl");
		String contentUrl = null;
		contentUrl = urlBase + "/projects/kfap/" + contentFolder + "/extractContent/" + lang + ".json";
		return getContent(contentUrl);
	}

	/**
	 * getContent - This is the "real" method that makes work of getting
	 * published CMS content.
	 * 
	 * @param contentUrl
	 * @return
	 * @throws PalmsException
	 */
	private JsonNode getContent(String contentUrl) throws PalmsException {
		log.debug("Getting Report content at: " + contentUrl);

		contentServiceUtil.initializeVariables();
		// contentServiceUtil.authenticate(getUsername(), getPassword());

		try {
			contentServiceUtil.doHttpGet(contentUrl);
		} catch (Exception e) {

			log.error("Service returned error: " + e.getMessage());
			log.error("HTTP status code is: " + contentServiceUtil.getHttpStatusCode());
			throw new PalmsException("Service returned error. Message: {}" + e.getMessage() + " HTTP Status code: "
					+ contentServiceUtil.getHttpStatusCode());
		}

		String jsonContentDictionary = contentServiceUtil.getjSON();
		log.info("Content dictionary is: " + jsonContentDictionary);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = null;
		try {
			jsonNode = mapper.readValue(jsonContentDictionary, JsonNode.class);
			JsonNode rptId = jsonNode.path("id");

			// Check the returned content is the report content
			if (rptId.textValue() == null) {
				log.debug("Expected report content is not found by connecting: " + contentUrl);
				log.error("Expected report content is not found by connecting: " + contentUrl);
				throw new RCMGenerationException("Expected report content is not found by connecting: " + contentUrl);
			}

		} catch (JsonParseException e) {
			log.debug("Joson Parser error creating document from ext. server response. " + e.getMessage());
			log.debug("Possible reasons using url:" + contentUrl);
			log.error("Joson Parser error creating document from ext. server response. Message:", e.getMessage());
			log.error("Possible reasons using url:" + contentUrl);
			throw new RCMGenerationException(
					"Parser error creating document from marklogic. server response. Message: " + e.getMessage()
							+ " url:" + contentUrl);
		} catch (JsonMappingException e) {
			log.debug("Json mapping error: Cannot mapping Json object. Message: " + e.getMessage());
			log.error("Joson Mapping error creating document from ext. server response. Message:", e.getMessage());
			throw new RCMGenerationException("json object mapping error. server response. Message: " + e.getMessage());
		} catch (IOException e) {
			log.debug("IO Exception: Server may not available " + e.getMessage());
			log.error("IOException url: {}, Message: {}", e.getMessage());
			throw new RCMGenerationException("IO error. Message: " + e.getMessage());
		}
		return jsonNode;
	}

	// public String getAssessmentContent(String assessmentCode, String lang,
	// String format) throws PalmsException {
	// setupAuthentication("xqy");

	// String urlBase = "http://" + getHost() + ":" + getPort() + "/";
	// String contentUrl = urlBase
	// + QUESTION_STRING_PARAMS.replace("{assessmentCode}", assessmentCode +
	// "").replace("{lang}", lang + "")
	// .replace("{format}", format + "");

	// System.out.println("Connecting the marklogic xqy server... " +
	// contentUrl);

	public String getAssessmentContent(String lang) throws PalmsException {

		String urlBase = configProperties.getProperty("reportserverBaseUrl");
		String contentUrl = null;
		contentUrl = urlBase + "/projects/kfap/feedback/assessmentContent/" + lang + ".json";

		// contentUrl = urlBase +
		// "/projects/kfap/feedback/assessmentContent/en-US.json";

		log.debug("Getting Assessment content at: " + contentUrl);

		contentServiceUtil.initializeVariables();
		// contentServiceUtil.authenticate(getUsername(), getPassword());
		try {
			contentServiceUtil.doHttpGet(contentUrl);
		} catch (IOException e) {
			log.debug("IO error creating document from ext. server response. Message:" + e.getMessage());
			log.error("IO error creating document from ext. server response. Message: {}", e.getMessage());
			throw new RCMGenerationException("IO error to returning data. server response. Message: {}"
					+ e.getMessage());
		} catch (Exception e) {
			log.debug("Service returned error: " + e.getMessage() + " HTTP status code is: "
					+ contentServiceUtil.getHttpStatusCode());
			log.error("Service returned error: Message: {}" + e.getMessage());
			log.error("HTTP status code is: " + contentServiceUtil.getHttpStatusCode());
			throw new PalmsException("Service returned error. Message: {}" + e.getMessage() + " HTTP status code is: "
					+ contentServiceUtil.getHttpStatusCode());
		}

		return contentServiceUtil.getjSON();
	}

	public List<String> getAsmtListTxt(String questionContent, int groupId, String questionId) throws PalmsException {
		List<String> questionListStr = new ArrayList<String>();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = null;

		try {
			root = mapper.readTree(questionContent);
			JsonNode activityNode = root.get("assessment").get(0).get("activities").get(0).get("activities").get(0)
					.get("activities");
			JsonNode questionSetNode = null;
			JsonNode optionNode = null;

			if (activityNode == null) {
				log.debug("KfapReportRgrToRcmV2 - Expected Data is not found from marklogic xqy server; "
						+ "One or many paramters to extract data may not valide. " + "HTTP Status code: "
						+ contentServiceUtil.getHttpStatusCode());
				log.error("KfapReportRgrToRcmV2 - Expected Data is not found from marklogic xqy server; "
						+ "One or many paramters to extract data may not valide. " + "HTTP Status code: "
						+ contentServiceUtil.getHttpStatusCode());
				throw new RCMGenerationException(
						"KfapReportRgrToRcmV2 - Expected Data is not found from marklogic xqy server; "
								+ "One or many paramters to extract data may not valide. " + "HTTP Status code: "
								+ contentServiceUtil.getHttpStatusCode());
			}

			Iterator<JsonNode> ite = activityNode.elements();
			int i = 0;
			while (ite.hasNext()) {
				JsonNode temp = ite.next();
				if (temp.has("id") && temp.get("id").intValue() == groupId) {
					questionSetNode = activityNode.get(i).get("questionSet");
				}
				i++;
			}

			Iterator<JsonNode> ite2 = questionSetNode.elements();
			i = 0;
			while (ite2.hasNext()) {
				JsonNode temp2 = ite2.next();
				if (temp2.has("id") && questionId.equals(temp2.get("id").textValue())) {
					optionNode = questionSetNode.get(i).get("options").get("option");
				}
				i++;
			}

			for (JsonNode node : optionNode) {
				if (node != null) {
					questionListStr.add(node.get("content").textValue());
				}
			}

		} catch (JsonParseException e) {
			log.debug("Joson Parser error creating document from ext. server response. Message:" + e.getMessage());
			log.error("Joson Parser error creating document from ext. server response. Message: {}", e.getMessage());
			throw new RCMGenerationException(
					"Parser error creating document from marklogic. server response. Message: {}" + e.getMessage());
		} catch (JsonMappingException e) {
			log.debug("Json mapping error: Cannot mapping Json object.");
			log.error("Joson Mapping error creating document from ext. server response. Message: {}", e.getMessage());
			throw new RCMGenerationException("json object mapping error. server response. Message: {}" + e.getMessage());
		} catch (IOException e) {
			log.debug("IO Exception: Server may not available " + e.getMessage());
			log.error("IOException url: {}, Message: {}", e.getMessage());
			throw new RCMGenerationException("IO error. Message: " + e.getMessage());
		} catch (NullPointerException npe) {
			log.debug("KfapReportRgrToRcmV2 - Expected Data not found in marklogic assessment extract: node: "
					+ groupId + " id: " + questionId + "=" + npe.getMessage());
			log.error("NullPointerException: {}, Message: {}", npe.getMessage());
			throw new RCMGenerationException(
					"KfapReportRgrToRcmV2 - Expected Data not found in marklogic assessment extract: node: " + groupId
							+ " id: " + questionId + "Message:" + npe.getMessage());
		}

		return questionListStr;
	}

	public String getRptContentTxt(JsonNode rptContentNode, String keyMap, String key) throws PalmsException {
		String value = "";
		try {
			value = rptContentNode.get(keyMap).get(key).textValue();
		} catch (NullPointerException e) {
			log.debug("KfapReportRgrToRcmV2 - Expected Data not found in marklogic report content extract: keyMap: "
					+ keyMap + " key: " + key + "=" + e.getMessage());
			log.error("NullPointerException: {}, Message: {}", e.getMessage());
			throw new RCMGenerationException(
					"KfapReportRgrToRcmV2 - Expected Data not found in marklogic report content extract: keyMap: "
							+ keyMap + " key: " + key + "= Message:" + e.getMessage());
		}
		return value;
	}

	/**
	 * getSharedContentTxt - Method to retrieve data from a 'sharedContent' node
	 * in the passed JSON content. Other than first finding the shared content
	 * node, it acts just like the getRptContentTxt() method.
	 * 
	 * @param content
	 *            JSON content to search
	 * @param keyMap
	 *            The 'top level' key
	 * @param key
	 *            The 'low-' or 'second-level' key for which to return data
	 * @return Text value associated with the specified keys
	 * @throws PalmsException
	 */
	public String getSharedContentTxt(JsonNode content, String keyMap, String key) throws PalmsException {
		String ret = "";
		String err = "";

		JsonNode scNode = content.get("sharedContent");
		if (scNode == null) {
			err = "ContentRetriever.getSharedContentTxt() - 'sharedContent' node not found in JSON node.";
			log.error(err);
			throw new RCMGenerationException(err);
		}
		ret = scNode.get(keyMap).get(key).textValue();

		return ret;
	}

	private void setupAuthentication(String port) {
		setHost(this.markLogicProperties.getProperty("mark.logic." + port + ".host"));
		setPort(this.markLogicProperties.getProperty("mark.logic." + port + ".port"));
		setUsername(this.markLogicProperties.getProperty("mark.logic." + port + ".username"));
		setPassword(this.markLogicProperties.getProperty("mark.logic." + port + ".password"));
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
