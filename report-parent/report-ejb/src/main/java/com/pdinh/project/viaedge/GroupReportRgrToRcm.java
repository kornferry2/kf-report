package com.pdinh.project.viaedge;

import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.pipeline.generator.vo.Stage;
import com.pdinh.pipeline.generator.vo.Var;
import com.pdinh.project.viaedge.reports.jaxb.AgilityChart;
import com.pdinh.project.viaedge.reports.jaxb.AgilityCharts;
import com.pdinh.project.viaedge.reports.jaxb.GroupSummarySection;
import com.pdinh.project.viaedge.reports.jaxb.IndividualScoresSection;
import com.pdinh.project.viaedge.reports.jaxb.PScale;
import com.pdinh.project.viaedge.reports.jaxb.PScales;
import com.pdinh.project.viaedge.reports.jaxb.Participant;
import com.pdinh.project.viaedge.reports.jaxb.Row;
import com.pdinh.project.viaedge.reports.jaxb.Rows;
import com.pdinh.project.viaedge.reports.jaxb.Table;
import com.pdinh.project.viaedge.reports.jaxb.Tables;
import com.pdinh.project.viaedge.reports.jaxb.ViaEdgeGroupReport;
import com.pdinh.reportcontent.Manifest;
import com.pdinh.reportcontent.ReportContent;

@Stateless
@LocalBean
/* ############################################################# 

	Handles Individual Coaching and Individual Feedback Reports
	Feedback Report is only a slight variation/subset 
	
	############################################################ */
public class GroupReportRgrToRcm {
	private static final Logger log = LoggerFactory.getLogger(GroupReportRgrToRcm.class);
	private ReportContent staticContent = null;
	private ReportContent sharedContent = null;
	private ReportContent agilityContent = null;
	// private static String SHORT_DATE_FORMAT = "dd-MMM-yyyy";
	private final String LONG_DATE_FORMAT = "dd MMM yyyy";
	private ReportGenerationRequest rgr;
	private final GroupSummarySection gss = new GroupSummarySection();
	private final AgilityCharts agilityChartList = new AgilityCharts();
	private ViaEdgeGroupReport report = new ViaEdgeGroupReport();
	private final Row lastRow = new Row();
	@EJB
	private UserDao userDao;
	private final String gridMapId = "agility";
	private String lang = "en-US";
	private final Map<String, String> agilityCharts = new HashMap<String, String>();
	private final Map<String, String> groupScales = new HashMap<String, String>();
	private final Map<String, String> groupStdDvs = new HashMap<String, String>();
	private final Map<String, String> chartRanges = new HashMap<String, String>();

	public ReportGenerationRequest getRgr() {
		return rgr;
	}

	public void setRgr(ReportGenerationRequest rgrp) {
		rgr.equals(rgrp);
	}

	@Resource(name = "application/report/config_properties", type = Properties.class)
	private Properties configProperties;

	public GroupReportRgrToRcm() {

	}

	private final List<String> langCodes = Arrays.asList("en-US", "ja", "zh", "it", "fr", "de", "pt", "es", "ru", "nl",
			"pl", "tr", "ko");

	public String generateReportRcm(ReportGenerationRequest reportGenReq, String language, Boolean showIds, String title)
			throws PalmsException {
		log.debug("viaEDGE GROUP FEEDBACK RCM Generate! Language: {}, showIDs: {} ", language, showIds);
		lang = langCodes.get(0);
		if (language != null) {
			if (langCodes.contains(language)) {
				lang = language;
			}
			if (language.equals("fr_CA")) {
				lang = langCodes.get(4);
			} else if (language.equals("pt_BR")) {
				lang = langCodes.get(6);
			} else if (language.equals("zh_CN")) {
				lang = langCodes.get(2);
			} else if (language.equals("en")) {
				lang = langCodes.get(0);
			}
		}
		this.rgr = reportGenReq;

		// SimpleDateFormat shortSdf = new SimpleDateFormat(SHORT_DATE_FORMAT);
		SimpleDateFormat longSdf = new SimpleDateFormat(LONG_DATE_FORMAT);

		String reportContentRepoBaseDir = configProperties.getProperty("reportContentRepository");
		// set manifest for group report content
		Manifest manifest = new Manifest(reportContentRepoBaseDir
				+ "/manifest/projects/internal/viaEdge/group/manifest.xml");
		// set agility model content from manifest (shared content)
		agilityContent = new ReportContent(reportContentRepoBaseDir + "/content/" + manifest.getModelContentRef()
				+ ".xml");
		// set static SHARED content from manifest
		sharedContent = new ReportContent(reportContentRepoBaseDir + "/content/"
				+ manifest.getReportContents().get("sharedContent") + ".xml");
		staticContent = new ReportContent(reportContentRepoBaseDir + "/content/"
				+ manifest.getReportContents().get("staticContent") + ".xml");
		// new IndividualSummaryReport
		report = new ViaEdgeGroupReport();
		report.setLang(lang);

		report.setCompanyName(rgr.getParticipant().get(0).getCompany());
		report.setCopyright(sharedContent.getKeyMapValue4Lang("shared", "copyright", lang));
		// report.setReportTitle(staticContent.getKeyMapValue4Lang("group",
		// "reportName", lang));
		String stripped = title.replaceAll("\\s+", "");// make sure stirng is
														// not only spaces

		title = escapeString(title, true);
		Participant part = new Participant();
		if (title != null && stripped.length() > 0) {
			// report.setReportTitle(title);
			part.setFirstName(title);
			part.setLastName("");
		} else {
			// what is the default?
			part.setFirstName("");
			part.setLastName("");
		}
		report.setParticipant(part);
		report.setReportTitle(staticContent.getKeyMapValue4Lang("group", "reportName", lang));
		// report.setParticipant(part);
		// GET STAGES FROM RGR PARTICIPANT
		// com.pdinh.pipeline.generator.vo.Participant extP =
		// rgr.getParticipant().get(0);
		List<com.pdinh.pipeline.generator.vo.Participant> participants = rgr.getParticipant();
		report.setCompanyName(rgr.getParticipant().get(0).getCompany());
		report.setReportDate(longSdf.format(new Date()));
		report.setSerialNumber(rgr.getParticipant().get(0).getExtId());
		String gridMapId = "agility";
		report.setIntroHeading(sharedContent.getKeyMapValue4Lang("shared", "introductionTitle", lang));
		report.setReportIntro(staticContent.getKeyMapValue4Lang("group", "reportIntro", lang));
		report.setP1(staticContent.getKeyMapValue4Lang("group", "p1", lang));
		report.setP1Heading(staticContent.getKeyMapValue4Lang("group", "p1Heading", lang));
		report.setP2(staticContent.getKeyMapValue4Lang("group", "p2", lang));
		report.setP2Heading(staticContent.getKeyMapValue4Lang("group", "p2Heading", lang));
		report.setP3("<p>" + staticContent.getKeyMapValue4Lang("group", "p3a", lang) + "</p><p>"
				+ staticContent.getKeyMapValue4Lang("group", "p3b", lang) + "</p>");
		report.setP3Heading(staticContent.getKeyMapValue4Lang("group", "p3Heading", lang));
		report.setP4(staticContent.getKeyMapValue4Lang("group", "p4", lang));
		report.setP4Heading(staticContent.getKeyMapValue4Lang("group", "p4Heading", lang));
		report.setP4B1(staticContent.getKeyMapValue4Lang("group", "p4B1", lang));
		report.setP4B2(staticContent.getKeyMapValue4Lang("group", "p4B2", lang));
		report.setP4B3(staticContent.getKeyMapValue4Lang("group", "p4B3", lang));
		report.setP5(staticContent.getKeyMapValue4Lang("group", "p5", lang));
		report.setP6(staticContent.getKeyMapValue4Lang("group", "p6", lang));
		report.setP6Heading(staticContent.getKeyMapValue4Lang("group", "p6Heading", lang));

		IndividualScoresSection iScoresSection = new IndividualScoresSection();
		iScoresSection.setColumn1Heading(staticContent.getKeyMapValue4Lang("group", "tableLabelInd", lang));
		iScoresSection.setColumn2Heading(agilityContent.getGridMapValue4Lang(gridMapId, "overall",
				"headingDwWrdBreaks", lang));
		iScoresSection.setColumn3Heading(agilityContent.getGridMapValue4Lang(gridMapId, "mental", "headingDwWrdBreaks",
				lang));
		iScoresSection.setColumn4Heading(agilityContent.getGridMapValue4Lang(gridMapId, "people", "headingDwWrdBreaks",
				lang));
		iScoresSection.setColumn5Heading(agilityContent.getGridMapValue4Lang(gridMapId, "change", "headingDwWrdBreaks",
				lang));
		iScoresSection.setColumn6Heading(agilityContent.getGridMapValue4Lang(gridMapId, "results",
				"headingDwWrdBreaks", lang));
		iScoresSection.setColumn7Heading(agilityContent.getGridMapValue4Lang(gridMapId, "selfAware",
				"headingDwWrdBreaks", lang));
		iScoresSection.setColumn8Heading(staticContent.getKeyMapValue4Lang("group", "tableLabelOC", lang));
		/*##################################################################
		 *
		 *LOOP THROUGH PARTICIPANTS AND SETUP THE INDIVIDUAL SCORES DATA GRID
		 *
		 ###################################################################*/
		Tables tables = new Tables();
		iScoresSection.setTables(tables);
		int i = 1;
		boolean lastTable = false; // used to skip i % 25 == 0 when earlyBreak
									// has occurred
		boolean earlyBreak = false; // used so there is never a new page with a
									// table with < 5 participants.
		boolean isFirstFootnote = true;
		Table table = new Table();
		Rows rows = new Rows();
		table.setRows(rows);

		for (com.pdinh.pipeline.generator.vo.Participant participant : participants) {
			List<Stage> ps = participant.getStage();
			// This logic would preferably be in the xsl report doc, but I could
			// not accomplish these conditions there.

			User user = userDao.findById(Integer.parseInt(participant.getExtId()));
			for (Stage st : ps) { // for each stage in participant
				if (st.getId().equals("indiv-list")) { // Individual scores
														// table data
					Row r = new Row();
					for (Var v : st.getVar()) {
						String value = v.getValue();
						String id = v.getId();
						if (id.equals("indivID_list")) {
							if (report.getCompanyName() == null)
								report.setCompanyName(user.getCompany().getCoName());
							if (showIds == null || !showIds) {
								r.setColumn1Value(user.getLastname() + ", " + user.getFirstname());
							} else {
								r.setColumn1Value(participants.size() + " " + i + " " + participant.getExtId());
							}
						} else if (id.equals("pAdjOLG_list")) {
							if (!value.equals("NaN")) {
								r.setColumn2Value(value);
							} else {
								r.setColumn2Value("&#160;");
							}
						} else if (id.equals("pAdjOLG_leftDeviation")) {
							if (!value.equals("NaN")) {
								r.setColumn2LowValue(value);
							} else {
								r.setColumn2Value("&#160;");
							}
						} else if (id.equals("pAdjOLG_rightDeviation")) {
							if (!value.equals("NaN")) {
								r.setColumn2HighValue(value);
							} else {
								r.setColumn2Value("&#160;");
							}
						} else if (id.equals("pAdjMLG_list")) {
							if (!value.equals("NaN")) {
								r.setColumn3Value(value);
							} else {
								r.setColumn3Value("&#160;");
							}
						} else if (id.equals("pAdjPLG_list")) {
							if (!value.equals("NaN")) {
								r.setColumn4Value(value);
							} else {
								r.setColumn4Value("&#160;");
							}
						} else if (id.equals("pAdjCLG_list")) {
							if (!value.equals("NaN")) {
								r.setColumn5Value(value);
							} else {
								r.setColumn5Value("&#160;");
							}
						} else if (id.equals("pAdjRLG_list")) {
							if (!value.equals("NaN")) {
								r.setColumn6Value(value);
							} else {
								r.setColumn6Value("&#160;");
							}
						} else if (id.equals("pAdjSLG_list")) {
							if (!value.equals("NaN")) {
								r.setColumn7Value(value);
							} else {
								r.setColumn7Value("&#160;");
							}
						} else if (id.equals("confdcindex_list")) {
							if (!value.equals("NaN")) {
								int score = Integer.parseInt(value);
								if (score < -5) {
									r.setColumn8ColorValue(staticContent.getKeyMapValue4Lang("group", "confValue3",
											lang));
									r.setColumn8ScoreValue("3");
								} else if (score < -3 && score > -6) {
									r.setColumn8ColorValue(staticContent.getKeyMapValue4Lang("group", "confValue2",
											lang));
									r.setColumn8ScoreValue("2");
								} else if (score > -4) {
									r.setColumn8ColorValue(staticContent.getKeyMapValue4Lang("group", "confValue1",
											lang));
									r.setColumn8ScoreValue("1");
								}
								Boolean pIsRetake = false;
								for (Position p : user.getPositions()) {
									if (p.isReset()) {
										pIsRetake = true;
										if (isFirstFootnote && pIsRetake) {
											table.setRetakeFootnote(staticContent.getKeyMapValue4Lang("group",
													"retakeFootnote", lang));
											isFirstFootnote = false;
										}
									}
								}
								r.setColumn8IsRetake(pIsRetake.toString());
							} else {
								r.setColumn8ColorValue("&#160;"); // nothing
							}
						}
					}
					table.getRows().getRow().add(r);
					// check if new table required for last table (new page)
					int remainder = i % 16;
					if ((participants.size() - i) == 5 && remainder > 11) {
						earlyBreak = true;
						lastTable = true;
					}
					if (i == participants.size())
						lastTable = true; // this is the end.

					// only fit 16 participants per table maximum
					if (i % 16 == 0 && !lastTable || earlyBreak) {
						table.setRows(rows);
						iScoresSection.getTables().getTable().add(table);

						table = new Table();
						rows = new Rows();
						table.setRows(rows);
						earlyBreak = false; // only do for early break once.
						isFirstFootnote = true;

					}

					i++; // NEXT participant index

				}
			}

		}

		int participantCount = 0;

		List<com.pdinh.pipeline.generator.vo.GroupStage> groupStages = rgr.getGroupStage();
		for (com.pdinh.pipeline.generator.vo.Stage stage : groupStages.get(0).getStage()) {
			/*##################################################################
			 *
			 * Get group data from Stages
			 * 
			 ###################################################################*/
			if (stage.getId().equals("group-avg")) {
				for (Var v : stage.getVar()) {
					groupScales.put(v.getId(), v.getValue());
				}

			} else if (stage.getId().equals("participant-total")) {
				for (Var v : stage.getVar()) { // there will only be one in here
					participantCount = Integer.parseInt(v.getValue());
					log.debug("There are {} participants in group.", participantCount);
				}
			} else if (stage.getId().equals("group-stdv")) {
				for (Var v : stage.getVar()) {
					groupStdDvs.put(v.getId(), v.getValue());
				}
			} else { // all the rest are chart ranges, put these all in the same
						// hashmap.
				for (Var v : stage.getVar()) {
					chartRanges.put(v.getId(), v.getValue());
				}
			}
		}
		lastRow.setColumn1Value(staticContent.getKeyMapValue4Lang("group", "tableLabel2", lang));
		lastRow.setColumn2Value(this.groupScales.get("pAdjOLG_avg"));
		lastRow.setColumn3Value(this.groupScales.get("pAdjMLG_avg"));
		lastRow.setColumn4Value(this.groupScales.get("pAdjPLG_avg"));
		lastRow.setColumn5Value(this.groupScales.get("pAdjCLG_avg"));
		lastRow.setColumn6Value(this.groupScales.get("pAdjRLG_avg"));
		lastRow.setColumn7Value(this.groupScales.get("pAdjSLG_avg"));
		lastRow.setColumn8ColorValue("&#160;");
		table.getRows().getRow().add(lastRow);
		iScoresSection.getTables().getTable().add(table);
		report.setIndividualScoresSection(iScoresSection);
		/*##################################################################
		 *
		 * Setup summary section and Agility Charts
		 *
		 ###################################################################*/
		PScales pScales = new PScales();
		gss.setPScales(pScales);
		gss.getPScales().getPScale().clear();
		gss.setTitle(staticContent.getKeyMapValue4Lang("group", "p6Heading", lang));
		gss.setKeysLabel(staticContent.getKeyMapValue4Lang("group", "summaryKeyLabel", lang));
		gss.setScoreDescLabelHigh(staticContent.getKeyMapValue4Lang("group", "summaryHighLabel", lang));
		gss.setScoreDescLabelLow(staticContent.getKeyMapValue4Lang("group", "summaryLowLabel", lang));
		gss.setSummaryFootnote(staticContent.getKeyMapValue4Lang("group", "summaryFootnote", lang));

		agilityChartList.getAgilityChart().clear();
		setChartData("overall", "OLG");
		setChartData("mental", "MLG");
		setChartData("people", "PLG");
		setChartData("change", "CLG");
		setChartData("results", "RLG");
		setChartData("selfAware", "SLG");

		// }

		report.setGroupSummarySection(gss);
		report.setAgilityCharts(agilityChartList);
		String rcm = "";
		rcm = this.marshalOutput(report);
		log.trace("RCM = {}", rcm);
		if (rcm == null) {
			log.error("RCM is null");
			throw new PalmsException("RCM is null.", PalmsErrorCode.RCM_GEN_FAILED.getCode());
		}

		return rcm; // RCM xml string

	}

	private void setChartData(String row, String agilityPrefix) {
		AgilityChart s = new AgilityChart();
		// agility gridMap content (use row)
		s.setTitle(agilityContent.getGridMapValue4Lang(gridMapId, row, "headingD", lang));
		s.setPercentile10(chartRanges.get("pAdj" + agilityPrefix + "_range_1"));
		s.setPercentile20(chartRanges.get("pAdj" + agilityPrefix + "_range_2"));
		s.setPercentile30(chartRanges.get("pAdj" + agilityPrefix + "_range_3"));
		s.setPercentile40(chartRanges.get("pAdj" + agilityPrefix + "_range_4"));
		s.setPercentile50(chartRanges.get("pAdj" + agilityPrefix + "_range_5"));
		s.setPercentile60(chartRanges.get("pAdj" + agilityPrefix + "_range_6"));
		s.setPercentile70(chartRanges.get("pAdj" + agilityPrefix + "_range_7"));
		s.setPercentile80(chartRanges.get("pAdj" + agilityPrefix + "_range_8"));
		s.setPercentile90(chartRanges.get("pAdj" + agilityPrefix + "_range_9"));
		s.setPercentile100(chartRanges.get("pAdj" + agilityPrefix + "_range_10"));
		// scaleKeys content
		// only put out if value is NOT 'NaN' (single participant group report)
		if (!this.groupStdDvs.get("pAdj" + agilityPrefix + "_stdv").equals("NaN"))
			s.setStdDev(staticContent.getKeyMapValue4Lang("group", "chartLabel2", lang) + " = "
					+ this.groupStdDvs.get("pAdj" + agilityPrefix + "_stdv"));
		// only put out if value is NOT 'NaN' (single participant group report)
		if (!this.groupScales.get("pAdj" + agilityPrefix + "_avg").equals("NaN"))
			s.setGrpMean(staticContent.getKeyMapValue4Lang("group", "chartLabel1", lang) + " = "
					+ this.groupScales.get("pAdj" + agilityPrefix + "_avg"));

		s.getPercentile10();

		s.setPIOLowList(agilityContent.getGridMapValue4Lang("issuesAndTendenciesGrid", row + "IssuesLow", "*", lang));
		s.setBTLowList(agilityContent.getGridMapValue4Lang("issuesAndTendenciesGrid", row + "TendenciesLow", "*", lang));
		s.setPIOHighList(agilityContent.getGridMapValue4Lang("issuesAndTendenciesGrid", row + "IssuesHigh", "*", lang));
		s.setBTHighList(agilityContent.getGridMapValue4Lang("issuesAndTendenciesGrid", row + "TendenciesHigh", "*",
				lang));

		s.setXLabel(staticContent.getKeyMapValue4Lang("group", "chartLabel4", lang));
		s.setYLabel(staticContent.getKeyMapValue4Lang("group", "chartLabel3", lang));

		s.setBTHighLabel(staticContent.getKeyMapValue4Lang("group", "chartLabel6", lang));
		s.setBTLowLabel(staticContent.getKeyMapValue4Lang("group", "chartLabel5", lang));
		s.setPIOHighLabel(staticContent.getKeyMapValue4Lang("group", "chartLabel8", lang));
		s.setPIOLowLabel(staticContent.getKeyMapValue4Lang("group", "chartLabel7", lang));
		log.debug("AGILITY CHART ADDED: " + s.getTitle());
		agilityChartList.getAgilityChart().add(s);

		PScale ps = new PScale();

		if (!agilityPrefix.equals("OLG")) {
			ps.setLowScoreDesc(agilityContent.getGridMapValue4Lang(gridMapId, row, "traitLow", lang));
			ps.setHighScoreDesc(agilityContent.getGridMapValue4Lang(gridMapId, row, "traitHigh", lang));
		} else {
			ps.setHighScoreDesc(agilityContent.getGridMapValue4Lang(gridMapId, row, "traitHigh", lang) + "<br/>"
					+ agilityContent.getGridMapValue4Lang(gridMapId, row, "traitHighDesc", lang));
			ps.setLowScoreDesc(agilityContent.getGridMapValue4Lang(gridMapId, row, "traitLow", lang) + "<br/>"
					+ agilityContent.getGridMapValue4Lang(gridMapId, row, "traitLowDesc", lang));
		}

		ps.setScore(this.groupScales.get("pAdj" + agilityPrefix + "_avg"));
		ps.setTitle(agilityContent.getGridMapValue4Lang(gridMapId, row, "headingD", lang));
		gss.getPScales().getPScale().add(ps);
	}

	private String marshalOutput(ViaEdgeGroupReport gReport) {
		JAXBContext jc = null;
		Marshaller m = null;
		StringWriter sw = new StringWriter();
		try {
			jc = JAXBContext.newInstance(ViaEdgeGroupReport.class);
			m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(gReport, sw);

			return sw.toString();
		} catch (JAXBException jbe) {
			log.error("VIA EDGE CoachingReportRgrToRcm.marshalOutput() - JAXBException: Msg={}", jbe.getMessage());
			jbe.printStackTrace();
			return null;
		}
	}

	public Map<String, String> getGroupScales() {
		return groupScales;
	}

	public Map<String, String> getAgilityCharts() {
		return agilityCharts;
	}

	/*
	 * PORTED FROM OXCART XMLUTILS
	 * Method to replace sensitive characters in xml (html markup characters)
	 * This only escapes non html values, useful for strings that might contain
	 * valid html but also contain invalid things such as "Company & Company"
	 * In that instance it should change it to "Company &amp; Company"
	 * 10/18/10 - Added <, > and '
	 * 06/23/12 - Added the ability to suppress the apostrophe... not needed on spreadsheets
	 * 
	 * @param input - The string to do the business on
	 * @param doEmAll - Flag to tell the logic to do all of the escapement (true)
	 * 					or to drop the stuff that Excel doesn't need (false)
	 * @return Escaped string
	 */
	// public static String xmlEscapeString(String input) {
	private static String escapeString(String input, boolean doEmAll) {
		if (input != null) {
			// We want to "escape" stuff that needs it but not the stuff that
			// doesn't. The
			// logic below assumes that if the input parses it needs no
			// escapement but if it
			// does not parse it does need it
			try {
				// Build a doc and parse it
				DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
				// We won't be looking at the generated doc, so don't assign it
				// to a variable
				docBuilder.parse(new InputSource(new StringReader("<root>" + input + "</root>")));
			} catch (SAXException se) {
				// parse "problem"... We need to escape the characters
				// Put out a message saying this was an expected result and that
				// it triggers the special character substitution.
				System.out
						.println("Not really a [Fatal Error]; Explanation: Special character detected and replaced -- XMLUtils.xmlEscapeString()");

				// Step 1, to prevent double escaping
				input = input.replaceAll("&amp;", "&");
				input = input.replaceAll("&lt;", "<");
				input = input.replaceAll("&gt;", ">");
				if (doEmAll) {
					// Do the stuff that the spreadsheets don't need
					input = input.replaceAll("&apos;", "'");
				}

				// Step 2, escape
				input = input.replaceAll("&", "&amp;");
				input = input.replaceAll("<", "&lt;");
				input = input.replaceAll(">", "&gt;");
				if (doEmAll) {
					// Again, do the stuff that the spreadsheets don't need
					input = input.replaceAll("'", "&apos;"); // or else &#39;
				}
			} catch (Exception se) {
				// ParderConfiguration or IOException...
				se.printStackTrace();
			}
		}

		return input;
	}
}
