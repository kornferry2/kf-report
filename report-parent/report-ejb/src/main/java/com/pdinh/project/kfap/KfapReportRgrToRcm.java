package com.pdinh.project.kfap;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import sun.net.www.protocol.http.HttpURLConnection;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kf.project.kfap.reports.jaxb.AssessmentDetailsSection;
import com.kf.project.kfap.reports.jaxb.BackgroundListItem;
import com.kf.project.kfap.reports.jaxb.CapacitySection;
import com.kf.project.kfap.reports.jaxb.CategoryItem;
import com.kf.project.kfap.reports.jaxb.ChallengeListItem;
import com.kf.project.kfap.reports.jaxb.DerailmentSection;
import com.kf.project.kfap.reports.jaxb.DevelopmentPrioritiesSection;
import com.kf.project.kfap.reports.jaxb.ExpTable;
import com.kf.project.kfap.reports.jaxb.ExpTableCategory;
import com.kf.project.kfap.reports.jaxb.ExperienceSection;
import com.kf.project.kfap.reports.jaxb.GoalsObjectivesList;
import com.kf.project.kfap.reports.jaxb.IdealRoleList;
import com.kf.project.kfap.reports.jaxb.InterestSection;
import com.kf.project.kfap.reports.jaxb.KeyLeadershipChallenges;
import com.kf.project.kfap.reports.jaxb.KfapIndividualFeedbackReport;
import com.kf.project.kfap.reports.jaxb.LeadershipTraitsSection;
import com.kf.project.kfap.reports.jaxb.LearningAgilitySection;
import com.kf.project.kfap.reports.jaxb.Level;
import com.kf.project.kfap.reports.jaxb.OverviewSection;
import com.kf.project.kfap.reports.jaxb.ParticipantName;
import com.kf.project.kfap.reports.jaxb.Priority;
import com.kf.project.kfap.reports.jaxb.SectionText;
import com.kf.project.kfap.reports.jaxb.SelfAwareSection;
import com.kf.project.kfap.reports.jaxb.SubSection;
import com.kf.project.kfap.reports.jaxb.SubSectionText;
import com.kf.project.kfap.reports.jaxb.SummarySection;
import com.pdinh.data.ms.dao.PositionDao;
import com.pdinh.data.ms.dao.ProjectCourseDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.pipeline.generator.vo.Participant;
import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.project.kfap.utils.KfapUtils;
import com.pdinh.project.kfap.utils.RgrRetriever;
import com.pdinh.project.kfap.utils.SortDevPriorities;
import com.pdinh.reportcontent.Manifest;
import com.pdinh.reportcontent.ReportContent;
import com.pdinh.reportgenerationresponse.ReportGenerationResponse;

@Stateless
@LocalBean
public class KfapReportRgrToRcm {
	private static final Logger log = LoggerFactory.getLogger(KfapReportRgrToRcm.class);

	@Resource(name = "application/report/config_properties", type = Properties.class)
	private Properties configProperties;

	private static String reportManifestDir = "";
	private static String reportContentDir = "";
	private static String reportGenRespDir = "";

	private ReportContent varContent = null;
	private ReportContent sharedStaticContent = null;
	private ReportContent staticContent = null;

	private String LONG_DATE_FORMAT = "MM/dd/yyyy";
	private String strDate;
	private String strCreateDate;
	private String createDate;
	private String strDateCompleted;
	private String strDateCompletedForP1;

	// private ReportGenerationRequest rgr;
	private KfapIndividualFeedbackReport rpt;

	private String lang = "en-US";
	private List<String> langCodes = Arrays.asList("en-US");

	// store rgr2 id and its mapped text
	private Map<String, String> varTextWrapper = new HashMap<String, String>();

	// getListItem return type
	private static int STR_ARR = 1;
	private static int INT_SUM = 2;

	// no ravens taken
	boolean NO_RAVENS = false;

	private static String KFP_INST = "kfp";
	private static String manifest = "kfpManifest";

	// private static String RESTFUL_STRING_PARAMS =
	// "/{instrumentId}/{manifest}/{participantId}";
	private static String RESTFUL_STRING_PARAMS = "/{instrumentId}/{manifest}/{participantId}/{tgtRole}";
	private static String TINCAN_STRING_PARAMS = "?participantId={participantId}&instrumentId={instrumentId}";

	// store individual item text and item value matched
	private String vTgtRole = "";
	private String vLTAspiration = "";
	// private static String vNTAspiration = "";
	private String vDateComplete = "";
	private String vClient = "";
	private String vFirstName = "";
	private String vLastName = "";

	private List<String> ROLE_LEVEL_LIST = null;
	private List<String> ROLE_TYPE_LIST = null;
	private List<String> ORG_TYPE_LIST = null;
	private List<String> ORG_TYPE_LIST_CURRENT = null;
	private List<String> ORG_SIZE_LIST = null;
	private List<String> IND_TYPE_LIST = null;
	private List<String> IND_TYPE_LIST_CURRENT = null;
	private List<String> FUNCT_AREA_LIST = null;
	private List<String> FUNCT_AREA_LIST_CURRENT = null;
	private List<String> KEY_CHALLENGE_LIST = null;
	private List<String> IDEAL_ROLE_LIST = null;

	private int tgtRole; // from project setup
	private int currOrgType; // ITM1
	private int yrsInCurrOrg; // ITM2
	private int currRoleLevel; // ITM3
	private int currRoleType; // ITM4
	private int yrsInCurrRole; // ITM5
	private int currOrgSize; // ITM6
	private int currIndustry; // ITM7
	private int currFunctArea; // ITM9
	private int yrsInWorkforce; // ITM12
	private int yrsInManagement; // ITM13
	private int yrsOnBoard; // ITM14
	private int carMix; // ITM39 possible value from 1-3, 1= line ;2=line staff
						// split; 3=staff
	private int nLevels;

	// aVariable only use to match an element of an arrayList
	// they always equal to int variable - 1
	private int aTgtRole;
	// private final int[] tempTargetMapping = { 8, 7, 6, 5, 4, 3, 2, 1, 0 };
	private int aCurrRoleLevel;
	private int aLtAspiration;
	// private int aNtAspiration;
	private int aCurrRoleType;
	private int aCurrIndustry;
	private int aCurrFunctArea;
	private int aCurrOrgType;
	private int aCurrOrgSize;

	// CAREER GOALS & OBJECTIVES
	private int carPlan; // ITM127

	private int threeToFiveYrsGoal; // ITM128 ntAspiration
	private int drThreeToFiveYrsGoal;
	private int drYrsInCurrRole;

	private int ltAspiration; // ITM130 aspire_role_level
	private int nOrgs; // ITM24
	private int nCountries; // ITM79

	// scores suppose get from input
	private int interestScore; // 1 or 0
	private int experienceScore; // 1 or 0
	private int awarenessScore; // 1 or 0
	private int agileScore; // 1 or 0
	private int traitsScore; // 1 or 0
	private int capacityScore; // 1 or 0
	private int derailerScore; // 1 or 0

	private int highestRole; // HighestRole
	private int nFunctAreas; // NFunctAreas
	private int nOrgTypes; // NOrgTypes
	private int nIndustries; // Nindustries
	private int nOrgSizes; // NOrgSizes
	private int rolePref; // RolePref pref_role_types
	private int functPref; // FunctPref
	private int orgPref; // OrgPref
	private int sizePref; // SizePref
	private int indPref; // IndPref
	private int nStrongPrefs; // NStrongPrefs
	private int nIdealAttributes; // NIdealAttributes
	private int expatCount; // number from Item100
	private int expatValue; // 1 or 0

	private int interest;
	private int pace;
	private int jump;
	private int drive;
	private int focus;
	private int engagement;
	private int engageScorePct; // Bruce says not to use this (Sept 26,
								// 2014) Previous comment: it percentile
								// to determine
								// engagement dynamic text.
	private int core;
	private int coreExpected;
	private int coreActual;
	private int experience;

	private int awareness;
	private int slfAware;
	private int sitAware;
	private int lrnAgile;
	private int cAgile;
	private int mAgile;
	private int pAgile;
	private int rAgile;

	private int expNOrgs;
	private int expNOrgTypes;
	private int expNIndustries;
	private int expNCountries;
	private int expNOrgSizes;
	private int expNRoleTypes;
	private int expNFunctAreas;
	private int perspective;
	private int nKeyChallenges;
	private double kcExp;
	private int keyChallenges;
	private int traits;
	private int focused;
	private int sociable;
	private int tolerance;
	private int composed;
	private int optimistic;
	private int ldrTraits; // n_traits
	private int capacity;
	private int volatil;
	private int micro;
	private int closed;

	private int slfAwarePct;
	private int sitAwarePct;
	private int changePct;
	private int resultsPct;
	private int peoplePct;
	private int mentalPct;
	private int focusedPct;
	private int sociablePct;
	private int composedPct;
	private int optimismPct;
	private int tolerancePct;
	private int volatilePct;
	private int closedPct;
	private int microPct;
	private int capacityPct; // is it a problem solving percentile score?
	private int perspectivePct;
	private int engagementPct;
	private int keyChallengePct;
	private int drivePct;
	private int focusPct;
	private int corePct;

	// list
	private List<Integer> role_level_list = new ArrayList<Integer>();
	private List<Integer> role_type_list = new ArrayList<Integer>();
	private List<Integer> org_type_list = new ArrayList<Integer>();
	private List<Integer> org_size_list = new ArrayList<Integer>();
	private List<Integer> ind_type_list = new ArrayList<Integer>();
	private List<Integer> funct_area_list = new ArrayList<Integer>();
	private List<Integer> key_challenge_list = new ArrayList<Integer>();
	private List<Integer> ideal_role_list = new ArrayList<Integer>();
	private List<Integer> pref_org_type_list = new ArrayList<Integer>();
	private List<Integer> pref_org_size_list = new ArrayList<Integer>();
	private List<Integer> pref_ind_type_list = new ArrayList<Integer>();
	private List<Integer> pref_role_type_list = new ArrayList<Integer>();
	private List<Integer> pref_funct_area_list = new ArrayList<Integer>();

	@EJB
	private ProjectUserDao projectUserDao;

	@EJB
	private ProjectDao projectDao;

	@EJB
	private UserDao userDao;

	@EJB
	private PositionDao positionDao;

	@EJB
	private ProjectCourseDao projectCourseDao;

	@EJB
	private RgrRetriever rgrRetriever;

	public KfapReportRgrToRcm() {

	}

	public String generateReportRcm(String type, String projId, String pid, String language, String targ)
			throws PalmsException {
		Document document = this.rgrRetriever.getRgr(type, pid, targ);
		try {
			return this.generateReportRcm(document, type, projId, pid, language, targ);
		} catch (PalmsException pe) {
			throw pe;
		}
	}

	/**************************************************************************************
	 * This is a major method to create RCM. Most of rcm object are set in this
	 * method. to debug easier, we create a corresponded method for each java
	 * list object.
	 * 
	 * 
	 * @return RCM String
	 **************************************************************************************/

	public String generateReportRcm(Document scoreDoc, String type, String projId, String pid, String language,
			String targ) throws PalmsException {

		// log.debug("starting generateReportRcm potential RGR to RCM - language="
		// + language);
		lang = langCodes.get(0);
		if (language != null) {
			if (langCodes.contains(language)) {
				lang = language;
			}
			if (language.equals("fr_CA")) {
				lang = langCodes.get(4);
			} else if (language.equals("pt_BR")) {
				lang = langCodes.get(6);
			} else if (language.equals("zh_CN")) {
				lang = langCodes.get(2);
			} else if (language.equals("en")) {
				lang = langCodes.get(0);
			}
		}
		log.debug("using " + lang);

		// date format
		if (lang.equals("en-US"))
			// LONG_DATE_FORMAT = "MM/dd/yyyy";
			LONG_DATE_FORMAT = "dd MMMMM yyyy";
		if (lang.equals("ja"))
			LONG_DATE_FORMAT = "yyyy-MM-dd";
		if (lang.equals("zh"))
			LONG_DATE_FORMAT = "yyyy-MM-dd";
		if (lang.equals("it"))
			LONG_DATE_FORMAT = "dd-MM-yyyy";
		if (lang.equals("fr"))
			LONG_DATE_FORMAT = "dd.MM.yyyy";
		if (lang.equals("de"))
			LONG_DATE_FORMAT = "dd.MM.yyyy";
		if (lang.equals("pt"))
			LONG_DATE_FORMAT = "dd-MM-yyyy";
		if (lang.equals("es"))
			LONG_DATE_FORMAT = "dd.MM.yyyy";
		if (lang.equals("ru"))
			LONG_DATE_FORMAT = "dd.MM.yyyy";

		// SimpleDateFormat shortSdf = new SimpleDateFormat(SHORT_DATE_FORMAT);
		SimpleDateFormat longSdf = new SimpleDateFormat(LONG_DATE_FORMAT);
		SimpleDateFormat longSdfForP1 = new SimpleDateFormat("MMMMM yyyy");
		strDate = longSdf.format(new Date());
		// Assessment Date
		// Business defined Assessment Date in the extract enhancements:
		// "date for each person (last possible finalization date)"
		// If no data, default to today

		List<ProjectCourse> pcs = projectCourseDao.findProjectCourseByProjectId(Integer.parseInt(projId));
		for (ProjectCourse pc : pcs) {
			if (pc.getCourse().getAbbv().equals("rv2") && pc.getSelected()) {
				NO_RAVENS = false;
			} else if (pc.getCourse().getAbbv().equals("rv2") && !pc.getSelected()) {
				NO_RAVENS = true;
			}
		}
		List<Position> positions = positionDao.findPositionsByUsersIdAndProjectId(Integer.parseInt(pid),
				Integer.parseInt(projId));
		if (positions == null) {
			strDateCompleted = strDate;
			log.debug("No valid Position rows found for ppt={}, proj={}, ... defaulting to todays date.", pid, projId);
		} else {
			// Look for the latest date
			int comp = 0;
			Timestamp ts = new Timestamp(0);
			for (Position pos : positions) {
				if (pos.getCompletionStatus() == Position.COMPLETE) {
					comp++;
					if (pos.getCompletionStatusDate() != null && pos.getCompletionStatusDate().after(ts)) {
						ts = pos.getCompletionStatusDate();
					}
				}
			}
			if (comp < 1) {
				strDateCompleted = strDate;
				log.debug("No completed Position rows found for ppt={}, proj={} ... defaulting to todays date.", pid,
						projId);
			} else {
				long l = ts.getTime();
				strDateCompleted = longSdf.format(new Date(l));
				strDateCompletedForP1 = longSdfForP1.format(new Date(l));
			}
		}
		// first thing is calling to get input data which come from R pipeline
		// and tincan rawResponse
		ReportGenerationRequest rgr = new ReportGenerationRequest();
		Participant ppt = new Participant();
		rgr.getParticipant().add(ppt);
		ppt.setExtId(pid + "");

		// Project proj = new Project();
		com.pdinh.persistence.ms.entity.Project proj = projectDao.findById(Integer.parseInt(projId));

		// System.out.println("target level passed in: "+targ);
		// tgtRole = 1;

		tgtRole = Integer.parseInt(targ);

		User user = userDao.findById(Integer.parseInt(pid));
		vFirstName = user.getFirstname();
		vLastName = user.getLastname();

		ParticipantName part = new ParticipantName();
		part.setFirstName(vFirstName);
		part.setLastName(vLastName);

		// tgtRole = 5;

		try {
			mapInputData(scoreDoc, type, pid, language);
		} catch (Exception e) {
			System.out.println("problem to fetch and map input data!");
			e.printStackTrace();
			throw new PalmsException("Problem Generating RCM due to : " + e.getMessage());
			// return null;
		}

		vTgtRole = ROLE_LEVEL_LIST.get(aTgtRole);
		vLTAspiration = ROLE_LEVEL_LIST.get(aLtAspiration);
		// vNTAspiration = ROLE_LEVEL_LIST.get(aNtAspiration);
		vDateComplete = strDateCompletedForP1;
		vClient = proj.getCompany().getCoName();

		// System.out.println("current level=" + currRoleLevel +
		// " aspiration Level=" + ltAspiration + " Target Level="
		// + tgtRole);

		// last thing to call is wrap a map to replace [xxx] variables
		wrapDisplayVars();

		// set manifest
		setReportManifestDir(configProperties.getProperty("reportContentRepository")
				+ "/manifest/projects/internal/kfap/");
		setReportContentDir(configProperties.getProperty("reportContentRepository") + "/content/");

		Manifest manifest = new Manifest(getReportManifestDir() + "feedback/" + "manifest.xml");

		sharedStaticContent = new ReportContent(getReportContentDir()
				+ manifest.getReportContents().get("sharedStaticContent") + ".xml");

		staticContent = new ReportContent(getReportContentDir() + manifest.getReportContents().get("staticContent")
				+ ".xml");

		strCreateDate = sharedStaticContent.getKeyMapValue4Lang("shared", "createDate", lang);
		createDate = strCreateDate.replace("{todaysDate}", strDate);

		// new IndividualSummaryReport
		rpt = new KfapIndividualFeedbackReport();
		rpt.setLang(lang);

		// set parent report object value
		rpt.setReportName(sharedStaticContent.getKeyMapValue4Lang("shared", "reportTitle", lang));
		rpt.setAssessmentTitle(sharedStaticContent.getKeyMapValue4Lang("shared", "reportName", lang));
		rpt.setConfidential(sharedStaticContent.getKeyMapValue4Lang("shared", "confidential", lang));

		rpt.setNameLabel(sharedStaticContent.getKeyMapValue4Lang("shared", "nameLabel", lang));
		rpt.setConfidentialWPeriod(sharedStaticContent.getKeyMapValue4Lang("shared", "confidentialWPeriod", lang));
		rpt.setClientLabel(sharedStaticContent.getKeyMapValue4Lang("shared", "clientLabel", lang));
		rpt.setTargetLabel(sharedStaticContent.getKeyMapValue4Lang("shared", "targetLabel", lang));
		rpt.setReportDateLabel(sharedStaticContent.getKeyMapValue4Lang("shared", "reportDateLabel", lang));

		rpt.setClient(vClient);
		rpt.setTarget(vTgtRole);
		rpt.setReportDate(strDate);
		// rpt.setReportDate(longSdf.format(new Date()));
		// rpt.setReportDate(DateUtils.truncate(new Date(),
		// java.util.Calendar.DAY_OF_MONTH).toString());
		rpt.setSerialNumber(pid);
		rpt.setParticipantName(part);
		rpt.setCopyright(sharedStaticContent.getKeyMapValue4Lang("shared", "copyright", lang));
		rpt.setProvidedBy(sharedStaticContent.getKeyMapValue4Lang("shared", "providedBy", lang));
		rpt.setCreateDate(createDate);

		if (NO_RAVENS) {
			rpt.setNoRavens(true);
		}

		rpt.setEndpageHeader(sharedStaticContent.getKeyMapValue4Lang("shared", "endpageHeader", lang));
		rpt.setEndpageP1(sharedStaticContent.getKeyMapValue4Lang("shared", "endpageP1", lang));
		rpt.setEndpageP2(sharedStaticContent.getKeyMapValue4Lang("shared", "endpageP2", lang));
		rpt.setEndpageP3(sharedStaticContent.getKeyMapValue4Lang("shared", "endpageP3", lang));

		rpt.setInterestLabel(staticContent.getKeyMapValue4Lang("preSection", "interestLabel", lang));
		rpt.setIsInterestStregth(getIsStrength(interestScore));
		rpt.setExperienceLabel(staticContent.getKeyMapValue4Lang("preSection", "experienceLabel", lang));
		rpt.setIsExperienceStregth(getIsStrength(experienceScore));
		rpt.setSelfAwarenessLabel(staticContent.getKeyMapValue4Lang("preSection", "selfAwarenessLabel", lang));
		rpt.setIsSelfAwarenessStregth(getIsStrength(awarenessScore));
		rpt.setLearningAgilityLabel(staticContent.getKeyMapValue4Lang("preSection", "learningAgilityLabel", lang));
		rpt.setIsLearningAgilityStregth(getIsStrength(agileScore));
		rpt.setLeadershipTraitsLabel(staticContent.getKeyMapValue4Lang("preSection", "leadershipTraitsLabel", lang));
		rpt.setIsLeadershipTraitsStregth(getIsStrength(traitsScore));

		if (!NO_RAVENS) {
			rpt.setCapacityLabel(staticContent.getKeyMapValue4Lang("preSection", "capacityLabel", lang));
			rpt.setIsCapacityStregth(getIsStrength(capacityScore));
		}

		rpt.setDerailmentRisksLabel(staticContent.getKeyMapValue4Lang("preSection", "derailmentRisksLabel", lang));
		rpt.setIsDerailmentRisksStregth(getIsStrength(derailerScore));

		// set report content for Overview Section
		OverviewSection overview = new OverviewSection();
		overview.setTitle(staticContent.getKeyMapValue4Lang("overview", "title", lang));
		overview.setP1(replaceContentVar(staticContent.getKeyMapValue4Lang("overview", "p1", lang)));
		overview.setP2(replaceContentVar(staticContent.getKeyMapValue4Lang("overview", "p2", lang)));
		overview.setNeedsLabel(staticContent.getKeyMapValue4Lang("overview", "needsLabel", lang));
		overview.setStrengthLabel(staticContent.getKeyMapValue4Lang("overview", "strengthLabel", lang));
		overview.setStrengthIconFootnote(replaceContentVar(staticContent.getKeyMapValue4Lang("overview",
				"strengthIconFootnote", lang)));
		overview.setDevNeedIconFootnote(replaceContentVar(staticContent.getKeyMapValue4Lang("overview",
				"devNeedIconFootnot", lang)));
		rpt.setOverviewSection(overview);

		// set report content for CareerToDate Section
		// CareerToDateSection careerToDate = new CareerToDateSection();
		// careerToDate.setTitle(staticContent.getKeyMapValue4Lang("careerToDate",
		// "title", lang));
		// careerToDate.setRoleHeading(staticContent.getKeyMapValue4Lang(
		// "careerToDate", "roleHeading", lang));
		// careerToDate.setPRole(replaceContentVar(getPRoleContent("careerToDate",
		// "pRole")));
		// careerToDate.setSynopsisHeading(staticContent.getKeyMapValue4Lang(
		// "careerToDate", "synopsisHeading", lang));
		// careerToDate.setPSynopsis(replaceContentVar(getPSynopsisContent(
		// "careerToDate", "pSynopsis")));
		// careerToDate.setGoalHeading(staticContent.getKeyMapValue4Lang(
		// "careerToDate", "goalHeading", lang));
		// careerToDate.setPGoal(replaceContentVar(getPGoalContent("careerToDate",
		// "pGoal")));
		// rpt.setCareerToDateSection(careerToDate);

		// set report content for Overview Section
		AssessmentDetailsSection assessmentDetails = new AssessmentDetailsSection();
		assessmentDetails.setTitle(staticContent.getKeyMapValue4Lang("assessmentDetailsSection", "title", lang));

		// create Interest section
		InterestSection interestSection = new InterestSection();
		interestSection.setLabel(staticContent.getKeyMapValue4Lang("interestSection", "label", lang));
		interestSection.getSectionText().add(getInterestPara("interestSection", "text"));

		SubSection drive = new SubSection();
		drive.setTitle(staticContent.getKeyMapValue4Lang("driveSubsection", "title", lang));
		drive.setDevelopmentText(staticContent.getKeyMapValue4Lang("driveSubsection", "developmentText", lang));
		drive.setScore(new BigInteger(String.valueOf(roundPct(drivePct))));
		drive.setStrongText(staticContent.getKeyMapValue4Lang("driveSubsection", "strongText", lang));
		interestSection.getSubSection().add(getDriveText(drive, "driveSubsection", "text"));

		SubSection focus = new SubSection();
		focus.setTitle(staticContent.getKeyMapValue4Lang("focusSubsection", "title", lang));
		focus.setDevelopmentText(staticContent.getKeyMapValue4Lang("focusSubsection", "developmentText", lang));
		focus.setScore(new BigInteger(String.valueOf(roundPct(focusPct))));
		focus.setStrongText(staticContent.getKeyMapValue4Lang("focusSubsection", "strongText", lang));
		interestSection.getSubSection().add(getFocusText(focus, "focusSubsection", "text"));

		SubSection engagement = new SubSection();
		engagement.setTitle(staticContent.getKeyMapValue4Lang("engagementSubsection", "title", lang));
		engagement.setDevelopmentText(staticContent
				.getKeyMapValue4Lang("engagementSubsection", "developmentText", lang));
		engagement.setScore(new BigInteger(String.valueOf(roundPct(engagementPct))));
		engagement.setStrongText(staticContent.getKeyMapValue4Lang("engagementSubsection", "strongText", lang));
		interestSection.getSubSection().add(getEngagementText(engagement, "engagementSubsection", "text"));

		assessmentDetails.setInterestSection(interestSection);

		// create experience section
		ExperienceSection expSection = new ExperienceSection();
		expSection.setLabel(staticContent.getKeyMapValue4Lang("experienceSection", "label", lang));
		expSection.getSectionText().add(getExperiencePara("experienceSection", "text"));

		SubSection core = new SubSection();
		core.setTitle(staticContent.getKeyMapValue4Lang("coreSubsection", "title", lang));
		core.setDevelopmentText(staticContent.getKeyMapValue4Lang("coreSubsection", "developmentText", lang));
		core.setScore(new BigInteger(String.valueOf(roundPct(corePct))));
		core.setStrongText(staticContent.getKeyMapValue4Lang("coreSubsection", "strongText", lang));
		expSection.getSubSection().add(getCoreText(core, "coreSubsection", "text"));

		SubSection perspective = new SubSection();
		perspective.setTitle(staticContent.getKeyMapValue4Lang("perspectiveSubsection", "title", lang));
		perspective.setDevelopmentText(staticContent.getKeyMapValue4Lang("perspectiveSubsection", "developmentText",
				lang));
		perspective.setScore(new BigInteger(String.valueOf(roundPct(perspectivePct))));
		perspective.setStrongText(staticContent.getKeyMapValue4Lang("perspectiveSubsection", "strongText", lang));
		expSection.getSubSection().add(getPerspectiveText(perspective, "perspectiveSubsection", null));

		SubSection keyChallenges = new SubSection();
		keyChallenges.setTitle(staticContent.getKeyMapValue4Lang("keyChallengeSubsection", "title", lang));
		keyChallenges.setDevelopmentText(staticContent.getKeyMapValue4Lang("keyChallengeSubsection", "developmentText",
				lang));
		keyChallenges.setScore(new BigInteger(String.valueOf(roundPct(keyChallengePct))));
		keyChallenges.setStrongText(staticContent.getKeyMapValue4Lang("keyChallengeSubsection", "strongText", lang));
		expSection.getSubSection().add(getKeyChallengeText(keyChallenges, "keyChallengeSubsection", "text"));
		assessmentDetails.setExperienceSection(expSection);

		// set key leadership challenge list
		// KeyLeadershipChallenges keyLeadershipChallenges = new
		// KeyLeadershipChallenges();
		// keyLeadershipChallenges.setTitle(staticContent.getKeyMapValue4Lang("keyLeadershipChallenges",
		// "title", lang));

		KeyLeadershipChallenges challenges = new KeyLeadershipChallenges();
		challenges.setTitle(staticContent.getKeyMapValue4Lang("keyLeadershipChallenges", "title", lang));
		challenges.getChallengeListItem().addAll(getChallengeListItem());
		assessmentDetails.setKeyLeadershipChallenges(challenges);

		// create awareness section
		SelfAwareSection selfAwareSection = new SelfAwareSection();
		selfAwareSection.setLabel(staticContent.getKeyMapValue4Lang("selfAwareSetion", "label", lang));
		selfAwareSection.getSectionText().add(getSelfAwarePara("selfAwareSetion", "text"));

		SubSection self = new SubSection();
		self.setTitle(staticContent.getKeyMapValue4Lang("selfSubsetion", "title", lang));
		self.setDevelopmentText(staticContent.getKeyMapValue4Lang("selfSubsetion", "developmentText", lang));
		self.setScore(new BigInteger(String.valueOf(roundPct(slfAwarePct))));
		self.setStrongText(staticContent.getKeyMapValue4Lang("selfSubsetion", "strongText", lang));
		selfAwareSection.getSubSection().add(getSelfText(self, "selfSubsetion", "text"));

		SubSection sit = new SubSection();
		sit.setTitle(staticContent.getKeyMapValue4Lang("sitSubsection", "title", lang));
		sit.setDevelopmentText(staticContent.getKeyMapValue4Lang("sitSubsection", "developmentText", lang));
		sit.setScore(new BigInteger(String.valueOf(roundPct(sitAwarePct))));
		sit.setStrongText(staticContent.getKeyMapValue4Lang("sitSubsection", "strongText", lang));
		selfAwareSection.getSubSection().add(getSitText(sit, "sitSubsection", "text"));

		assessmentDetails.setSelfAwareSection(selfAwareSection);

		// create Agility section
		LearningAgilitySection agility = new LearningAgilitySection();

		agility.setLabel(staticContent.getKeyMapValue4Lang("learningAgilitySection", "label", lang));
		agility.getSectionText().add(getLearningAgilityPara("learningAgilitySection", "text"));

		SubSection people = new SubSection();
		people.setTitle(staticContent.getKeyMapValue4Lang("peopleSubsection", "title", lang));
		people.setDevelopmentText(staticContent.getKeyMapValue4Lang("peopleSubsection", "developmentText", lang));
		people.setScore(new BigInteger(String.valueOf(roundPct(peoplePct))));
		people.setStrongText(staticContent.getKeyMapValue4Lang("peopleSubsection", "strongText", lang));
		agility.getSubSection().add(getPeopleText(people, "peopleSubsection", "text"));

		SubSection results = new SubSection();
		results.setTitle(staticContent.getKeyMapValue4Lang("resultsSubsection", "title", lang));
		results.setDevelopmentText(staticContent.getKeyMapValue4Lang("resultsSubsection", "developmentText", lang));
		results.setScore(new BigInteger(String.valueOf(roundPct(resultsPct))));
		results.setStrongText(staticContent.getKeyMapValue4Lang("resultsSubsection", "strongText", lang));
		agility.getSubSection().add(getResultsText(results, "resultsSubsection", "text"));

		SubSection mental = new SubSection();
		mental.setTitle(staticContent.getKeyMapValue4Lang("mentalSubsection", "title", lang));
		mental.setDevelopmentText(staticContent.getKeyMapValue4Lang("mentalSubsection", "developmentText", lang));
		mental.setScore(new BigInteger(String.valueOf(roundPct(mentalPct))));
		mental.setStrongText(staticContent.getKeyMapValue4Lang("mentalSubsection", "strongText", lang));
		agility.getSubSection().add(getMentalText(mental, "mentalSubsection", "text"));

		SubSection change = new SubSection();
		change.setTitle(staticContent.getKeyMapValue4Lang("changeSubsection", "title", lang));
		change.setDevelopmentText(staticContent.getKeyMapValue4Lang("changeSubsection", "developmentText", lang));
		change.setScore(new BigInteger(String.valueOf(roundPct(changePct))));
		change.setStrongText(staticContent.getKeyMapValue4Lang("changeSubsection", "strongText", lang));
		agility.getSubSection().add(getChangeText(change, "changeSubsection", "text"));

		assessmentDetails.setLearningAgilitySection(agility);

		// create Leadership Traits section
		LeadershipTraitsSection leadershipTraits = new LeadershipTraitsSection();
		leadershipTraits.setLabel(staticContent.getKeyMapValue4Lang("leadershipTraitsSection", "label", lang));
		leadershipTraits.getSectionText().add(getLeadershipTraitsPara("leadershipTraitsSection", "text"));

		SubSection focused = new SubSection();
		focused.setTitle(staticContent.getKeyMapValue4Lang("focusedSubsection", "title", lang));
		focused.setDevelopmentText(staticContent.getKeyMapValue4Lang("focusedSubsection", "developmentText", lang));
		focused.setScore(new BigInteger(String.valueOf(roundPct(focusedPct))));
		focused.setStrongText(staticContent.getKeyMapValue4Lang("focusedSubsection", "strongText", lang));
		leadershipTraits.getSubSection().add(getFocusedText(focused, "focusedSubsection", "text"));

		SubSection sociable = new SubSection();
		sociable.setTitle(staticContent.getKeyMapValue4Lang("sociableSubsection", "title", lang));
		sociable.setDevelopmentText(staticContent.getKeyMapValue4Lang("sociableSubsection", "developmentText", lang));
		sociable.setScore(new BigInteger(String.valueOf(roundPct(sociablePct))));
		sociable.setStrongText(staticContent.getKeyMapValue4Lang("sociableSubsection", "strongText", lang));
		leadershipTraits.getSubSection().add(getSociableText(sociable, "sociableSubsection", "text"));

		SubSection toleranceOfAmbiguity = new SubSection();
		toleranceOfAmbiguity.setTitle(staticContent
				.getKeyMapValue4Lang("toleranceOfAmbiguitySubsection", "title", lang));
		toleranceOfAmbiguity.setDevelopmentText(staticContent.getKeyMapValue4Lang("toleranceOfAmbiguitySubsection",
				"developmentText", lang));
		toleranceOfAmbiguity.setScore(new BigInteger(String.valueOf(roundPct(tolerancePct))));
		toleranceOfAmbiguity.setStrongText(staticContent.getKeyMapValue4Lang("toleranceOfAmbiguitySubsection",
				"strongText", lang));
		leadershipTraits.getSubSection().add(
				getToleranceOfAmbiguityText(toleranceOfAmbiguity, "toleranceOfAmbiguitySubsection", "text"));

		SubSection composed = new SubSection();
		composed.setTitle(staticContent.getKeyMapValue4Lang("composedSubsection", "title", lang));
		composed.setDevelopmentText(staticContent.getKeyMapValue4Lang("composedSubsection", "developmentText", lang));
		composed.setScore(new BigInteger(String.valueOf(roundPct(composedPct))));
		composed.setStrongText(staticContent.getKeyMapValue4Lang("composedSubsection", "strongText", lang));
		leadershipTraits.getSubSection().add(getComposedText(composed, "composedSubsection", "text"));

		SubSection optimistic = new SubSection();
		optimistic.setTitle(staticContent.getKeyMapValue4Lang("optimisticSubsection", "title", lang));
		optimistic.setDevelopmentText(staticContent
				.getKeyMapValue4Lang("optimisticSubsection", "developmentText", lang));
		optimistic.setScore(new BigInteger(String.valueOf(roundPct(optimismPct))));
		optimistic.setStrongText(staticContent.getKeyMapValue4Lang("optimisticSubsection", "strongText", lang));
		leadershipTraits.getSubSection().add(getOptimisticText(optimistic, "optimisticSubsection", "text"));

		assessmentDetails.setLeadershipTraitsSection(leadershipTraits);

		// create Capacity section only when Reaven capacity is available
		if (!NO_RAVENS) {
			CapacitySection capacity = new CapacitySection();
			capacity.setLabel(staticContent.getKeyMapValue4Lang("capacitySection", "label", lang));
			capacity.getSectionText().add(getCapacityPara("capacitySection", "text"));

			SubSection problemSolving = new SubSection();
			problemSolving.setTitle(staticContent.getKeyMapValue4Lang("problemSolvingSubsection", "title", lang));
			problemSolving.setDevelopmentText(staticContent.getKeyMapValue4Lang("problemSolvingSubsection",
					"developmentText", lang));
			problemSolving.setScore(new BigInteger(String.valueOf(roundPct(capacityPct))));
			problemSolving.setStrongText(staticContent.getKeyMapValue4Lang("problemSolvingSubsection", "strongText",
					lang));
			capacity.getSubSection().add(getProblemSolvingText(problemSolving, "problemSolvingSubsection", "text"));
			assessmentDetails.setCapacitySection(capacity);
		}

		// create derailment risk section
		DerailmentSection derailment = new DerailmentSection();
		derailment.setLabel(staticContent.getKeyMapValue4Lang("DerailmentSection", "label", lang));

		derailment.getSectionText().add(getDerailmentPara("DerailmentSection", "text"));

		SubSection volat = new SubSection();
		volat.setTitle(staticContent.getKeyMapValue4Lang("volatileSubsection", "title", lang));
		volat.setDevelopmentText(staticContent.getKeyMapValue4Lang("volatileSubsection", "developmentText", lang));
		volat.setScore(new BigInteger(String.valueOf(roundPct(volatilePct))));
		volat.setStrongText(staticContent.getKeyMapValue4Lang("volatileSubsection", "strongText", lang));
		derailment.getSubSection().add(getVolatileText(volat, "volatileSubsection", "text"));

		SubSection micro = new SubSection();
		micro.setTitle(staticContent.getKeyMapValue4Lang("microSubsection", "title", lang));
		micro.setDevelopmentText(staticContent.getKeyMapValue4Lang("microSubsection", "developmentText", lang));
		micro.setScore(new BigInteger(String.valueOf(roundPct(microPct))));
		micro.setStrongText(staticContent.getKeyMapValue4Lang("microSubsection", "strongText", lang));
		derailment.getSubSection().add(getMicroText(micro, "microSubsection", "text"));

		SubSection closed = new SubSection();
		closed.setTitle(staticContent.getKeyMapValue4Lang("closedSubsection", "title", lang));
		closed.setDevelopmentText(staticContent.getKeyMapValue4Lang("closedSubsection", "developmentText", lang));
		closed.setScore(new BigInteger(String.valueOf(roundPct(closedPct))));
		closed.setStrongText(staticContent.getKeyMapValue4Lang("closedSubsection", "strongText", lang));
		derailment.getSubSection().add(getClosedText(closed, "closedSubsection", "text"));

		assessmentDetails.setDerailmentSection(derailment);

		rpt.setAssessmentDetailsSection(assessmentDetails);

		// set recommentedCareer Path
		// RecommendedCareerPath recommendedCareerPath = new
		// RecommendedCareerPath();
		// recommendedCareerPath.setTitle(staticContent.getKeyMapValue4Lang(
		// "recommendedCareerPath", "title", lang));
		// recommendedCareerPath.setP1(staticContent.getKeyMapValue4Lang(
		// "recommendedCareerPath", "p1", lang));
		//
		// recommendedCareerPath.getRecommendation().addAll(
		// getRecommendationItem());
		//
		// rpt.setRecommendedCareerPath(recommendedCareerPath);

		// set developmentPriorities section
		DevelopmentPrioritiesSection developmentPriorities = new DevelopmentPrioritiesSection();

		developmentPriorities.setTitle(staticContent.getKeyMapValue4Lang("developmentPriorities", "title", lang));
		developmentPriorities.setP1(staticContent.getKeyMapValue4Lang("developmentPriorities", "p1", lang));

		developmentPriorities.getPriority().addAll(getPriorityItem());

		rpt.setDevelopmentPrioritiesSection(developmentPriorities);

		// set Summary section
		SummarySection summary = new SummarySection();

		summary.setTitle(staticContent.getKeyMapValue4Lang("summary", "title", lang));
		summary.setCurrentLabel(staticContent.getKeyMapValue4Lang("summary", "currentLabel", lang));
		summary.setAspirationLabel(staticContent.getKeyMapValue4Lang("summary", "aspirationLabel", lang));
		summary.setTargetLabel(staticContent.getKeyMapValue4Lang("summary", "targetLabel", lang));

		summary.getLevel().addAll(getLevelItem());

		summary.setBackgroundHeading(staticContent.getKeyMapValue4Lang("summary", "backgroundHeading", lang));

		summary.getBackgroundListItem().addAll(getBackgroundListItem());

		summary.setGoalsObjectivesHeading(staticContent.getKeyMapValue4Lang("summary", "goalsObjectivesHeading", lang));

		summary.setGoalsObjectivesList(getGoalsObjectivesList());

		summary.setIdealRoleHeading(staticContent.getKeyMapValue4Lang("summary", "idealRoleHeading", lang));

		summary.setIdealRoleList(getIdealRoleList());

		ExpTable expTable = new ExpTable();
		expTable.setExpColHeader(staticContent.getKeyMapValue4Lang("expTable", "expColHeader", lang));
		expTable.setFutureColHeader(staticContent.getKeyMapValue4Lang("expTable", "futureColHeader", lang));
		expTable.setCurrentRoleLabel(staticContent.getKeyMapValue4Lang("expTable", "currentRoleLabel", lang));

		ExpTableCategory expTableCategory = null;
		expTableCategory = new ExpTableCategory();
		expTableCategory.setCategoryLabel(staticContent.getKeyMapCaseSentenceLangValue("expTableCategory",
				"categoryLabel", null, "1", lang));
		expTableCategory.getCategoryItem().addAll(getOrgTypeCategoryItem());
		expTable.getExpTableCategory().add(expTableCategory);

		expTableCategory = new ExpTableCategory();
		expTableCategory.setCategoryLabel(staticContent.getKeyMapCaseSentenceLangValue("expTableCategory",
				"categoryLabel", null, "2", lang));
		expTableCategory.getCategoryItem().addAll(getOrgSizeCategoryItem());
		expTable.getExpTableCategory().add(expTableCategory);

		expTableCategory = new ExpTableCategory();
		expTableCategory.setCategoryLabel(staticContent.getKeyMapCaseSentenceLangValue("expTableCategory",
				"categoryLabel", null, "3", lang));
		expTableCategory.getCategoryItem().addAll(getIndTypeCategoryItem());
		expTable.getExpTableCategory().add(expTableCategory);

		expTableCategory = new ExpTableCategory();
		expTableCategory.setCategoryLabel(staticContent.getKeyMapCaseSentenceLangValue("expTableCategory",
				"categoryLabel", null, "4", lang));
		expTableCategory.getCategoryItem().addAll(getRoleTypeCategoryItem());
		expTable.getExpTableCategory().add(expTableCategory);

		expTableCategory = new ExpTableCategory();
		expTableCategory.setCategoryLabel(staticContent.getKeyMapCaseSentenceLangValue("expTableCategory",
				"categoryLabel", null, "5", lang));
		expTableCategory.getCategoryItem().addAll(getFunctAreaCategoryItem());
		expTable.getExpTableCategory().add(expTableCategory);

		summary.setExpTable(expTable);
		rpt.setSummarySection(summary);

		String rcm = "";
		rcm = marshalOutput(rpt);

		// System.out.println("RCM = " + rcm);

		return rcm; // RCM xml string
	}

	// private String getPRoleContent(String keyMap, String key) {
	// String pRoleContent = "";
	//
	// if (currOrgType >= 1 && currOrgType <= 6) {
	// pRoleContent = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// null, "1", lang);
	// } else if (currOrgType >= 4 && currOrgType <= 5) {
	// pRoleContent = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// null, "2", lang);
	// } else if (currOrgType == 7) {
	// pRoleContent = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// null, "3", lang);
	// } else if (currOrgType == 8) {
	// pRoleContent = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// null, "4", lang);
	// } else {
	// System.out.println("The pRole content is empty!" + pRoleContent);
	// }
	// return pRoleContent;
	// }
	//
	// private String getPSynopsisContent(String keyMap, String key) {
	// String sentence1 = null;
	// String sentence2 = null;
	// String sentence3 = null;
	// String sentence4 = null;
	// String sentence5 = null;
	// String sentence6 = null;
	//
	// if (yrsInManagement == 0) {
	// return staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, "1",
	// "1", lang);
	// } else {
	// sentence1 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "1", "2", lang);
	//
	// sentence2 = highestRole == currRoleLevel ?
	// staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, "2",
	// "1", lang) : staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "2", "2", lang);
	//
	// // carMix = ITM39 possible value from 1-3, 1= line; 2=line staff
	// if (carMix == 1 && nFunctAreas == 1) {
	// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "3", "1", lang);
	// } else if (carMix == 2 && nFunctAreas == 1) {
	// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "3", "2", lang);
	// } else if (carMix == 3 && nFunctAreas == 1) {
	// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "3", "3", lang);
	// } else if (carMix == 1 && nFunctAreas > 1) {
	// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "3", "4", lang);
	// } else if (carMix == 2 && nFunctAreas > 1) {
	// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "3", "5", lang);
	// } else if (carMix == 3 && nFunctAreas > 1) {
	// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "3", "6", lang);
	// } else {
	// System.out.println("pSynopsis sentence 3 content is empty!");
	// }
	// //
	// System.out.println("ks-n_orgs="+n_orgs+" n_org_types="+n_org_types+" n_industries="+n_industries);
	// if (nOrgs == 1) {
	// sentence4 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "4", "1", lang);
	// } else if (nOrgs > 1 && nOrgTypes == 1 && nIndustries == 1 && nOrgSizes
	// == 1) {
	// sentence4 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "4", "2", lang);
	// } else if (nOrgs > 1 && nOrgTypes > 1 && nIndustries == 1 && nOrgSizes ==
	// 1) {
	// sentence4 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "4", "3", lang);
	// } else if (nOrgs > 1 && nOrgTypes > 1 && nIndustries > 1 && nOrgSizes ==
	// 1) {
	// sentence4 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "4", "4", lang);
	// } else if (nOrgs > 1 && nOrgTypes > 1 && nIndustries == 1 && nOrgSizes >
	// 1) {
	// sentence4 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "4", "5", lang);
	// } else if (nOrgs > 1 && nOrgTypes > 1 && nIndustries > 1 && nOrgSizes ==
	// 1) {
	// sentence4 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "4", "6", lang);
	// } else if (nOrgs > 1 && nOrgTypes > 1 && nIndustries == 1 && nOrgSizes >
	// 1) {
	// sentence4 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "4", "7", lang);
	// } else if (nOrgs > 1 && nOrgTypes > 1 && nIndustries > 1 && nOrgSizes >
	// 1) {
	// sentence4 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "4", "8", lang);
	// } else {
	// System.out.println("pSynopsis sentence 4 content is empty!");
	// }
	//
	// sentence5 = nCountries == 1 ?
	// staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, "5", "1", lang)
	// : staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, "5", "2",
	// lang);
	//
	// // Pace = CarTenure / HighestRole. Round to nearest whole number. ks
	// if (pace < 3) {
	// sentence6 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "6", "1", lang);
	// } else if (pace >= 3 && pace <= 5) {
	// sentence6 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "6", "2", lang);
	// } else if (pace > 5) {
	// sentence6 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "6", "3", lang);
	// } else {
	// System.out.println("pSynopsis sentence 6 content is empty!");
	// }
	// // not sure if one of a sentence misses, should display rest of
	// // sentence?
	// if (sentence1 != null && sentence2 != null && sentence3 != null &&
	// sentence4 != null && sentence5 != null
	// && sentence6 != null) {
	// return sentence1 + sentence2 + sentence3 + sentence4 + sentence5 +
	// sentence6;
	// } else {
	// System.out.println("some part or parts of pSynopsis content is empty!");
	// return "";
	// }
	// }
	// }
	//
	// private String getPGoalContent(String keyMap, String key) {
	// String sentence1 = "";
	// String sentence2 = "";
	// String sentence3 = "";
	// String txt = "";
	//
	// sentence1 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "1", null, lang);
	//
	// if (rolePref == 1 && functPref == 1) {
	// sentence2 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "2", "1", lang);
	// } else if (rolePref == 1 && functPref > 1 && functPref <= 5) {
	// sentence2 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "2", "2", lang);
	// } else if (rolePref == 1 && functPref > 1) {
	// sentence2 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "2", "3", lang);
	// } else if (rolePref > 1 && functPref == 1) {
	// sentence2 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "2", "4", lang);
	// } else if (rolePref > 1 && functPref > 1 && functPref <= 5) {
	// sentence2 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "2", "5", lang);
	// } else if (rolePref > 1 && functPref > 1) {
	// sentence2 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "2", "6", lang);
	// } else {
	// System.out.println("The getGoal sentence 2 content is empty!");
	// }
	//
	// if (sizePref == 1 && orgPref == 1 && indPref == 1) {
	// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "3", "1", lang);
	// } else if (sizePref == 1 && orgPref == 1 && indPref > 1 && indPref <= 3)
	// {
	// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "3", "2", lang);
	// } else if (sizePref == 1 && orgPref > 1 && indPref == 1) {
	// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "3", "3", lang);
	// } else if (sizePref > 1 && orgPref == 1 && indPref == 1) {
	// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "3", "4", lang);
	// } else if (sizePref == 1 && orgPref > 1 && indPref > 1) {
	// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "3", "5", lang);
	// } else if (sizePref > 1 && orgPref == 1 && indPref > 1) {
	// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "3", "6", lang);
	// } else if (sizePref > 1 && orgPref > 1 && indPref == 1) {
	// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "3", "7", lang);
	// } else if (sizePref > 1 && orgPref > 1 && indPref > 1) {
	// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
	// "3", "8", lang);
	// } else {
	// System.out.println("The getGoal sentence 3 content is empty!");
	// }
	//
	// txt = sentence1 + sentence2 + sentence3;
	// if (txt != "") {
	// return txt;
	// } else {
	// System.out.println("The getGoal content is empty!");
	// return "";
	// }
	// }

	private SectionText getInterestPara(String keyMap, String key) {
		String txt = "";
		// System.out.println("interest drive=" + drive + " focus=" + focus +
		// " engagement=" + engagement);

		if (drive == 1 && focus == 1 && engagement == 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else if (drive == 1 && focus == 1 && engagement == 0) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (drive == 1 && focus == 0 && engagement == 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (drive == 0 && focus == 1 && engagement == 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (drive == 1 && focus == 0 && engagement == 0) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (drive == 0 && focus == 1 && engagement == 0) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (drive == 0 && focus == 0 && engagement == 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (drive == 0 && focus == 0 && engagement == 0) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "8", lang);
		} else {
			System.out.println("The interestMainPara content is empty.");
			return null;
		}
		SectionText sectionText = new SectionText();
		sectionText.setText(replaceContentVar(txt));
		return sectionText;
	}

	private SubSection getDriveText(SubSection subSection, String keyMap, String key) {
		String txt = null;
		SubSectionText subText = new SubSectionText();

		/**********************************************************************************
		 * RoleTenure - a = scored input: interest.drive.yeasCurrentRole
		 * threeToFiveYrsGoal - b = scored input:
		 * interest.drive.threeToFiveYearGoal jump - c = scored input:
		 * interest.drive.jump; d = if ltAspiration > b true, Else = false
		 * 
		 * jump is actually a level and not the calculation, hence the new value
		 * for c below
		 * *******************************************************************************/

		txt = "";
		int a = yrsInCurrRole;
		int b = threeToFiveYrsGoal;
		int c = 0;
		int theJump = 0;
		theJump = jump; // if this value is 0 then the person didn't answer
						// the question

		int d = 1;

		// lower target level numbers are actually higher levels so the logic is
		// reversed

		// d = 1 = A < T
		// d = 0 = A >= T

		if (ltAspiration > tgtRole) {
			d = 1;
		} else {
			d = 0;
		}

		if (theJump != 0) {

			c = (currRoleLevel - theJump);

			if (a >= 3 && b == 1 && c >= 2 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
			} else if (a >= 3 && b == 1 && c < 2 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
			} else if (a >= 3 && b == 1 && c >= 2 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "10", lang);
			} else if (a >= 3 && b == 1 && c < 2 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "11", lang);
			} else if (a < 3 && b == 1 && c >= 2 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "19", lang);
			} else if (a < 3 && b == 1 && c < 2 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "20", lang);
			} else if (a < 3 && b == 1 && c >= 2 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "28", lang);
			} else if (a < 3 && b == 1 && c < 2 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "29", lang);
			}

		} else {

			if (a >= 3 && b == 2 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
			} else if (a >= 3 && b == 3 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
			} else if (a >= 3 && b == 4 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
			} else if (a >= 3 && b == 5 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
			} else if (a >= 3 && b == 6 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
			} else if (a >= 3 && b == 7 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "8", lang);
			} else if (a >= 3 && b == 8 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "9", lang);
			} else if (a >= 3 && b == 2 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "12", lang);
			} else if (a >= 3 && b == 3 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "13", lang);
			} else if (a >= 3 && b == 4 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "14", lang);
			} else if (a >= 3 && b == 5 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "15", lang);
			} else if (a >= 3 && b == 6 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "16", lang);
			} else if (a >= 3 && b == 7 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "17", lang);
			} else if (a >= 3 && b == 8 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "18", lang);
			} else if (a < 3 && b == 2 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "21", lang);
			} else if (a < 3 && b == 3 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "22", lang);
			} else if (a < 3 && b == 4 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "23", lang);
			} else if (a < 3 && b == 5 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "24", lang);
			} else if (a < 3 && b == 6 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "25", lang);
			} else if (a < 3 && b == 7 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "26", lang);
			} else if (a < 3 && b == 8 && d == 1) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "27", lang);
			} else if (a < 3 && b == 2 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "30", lang);
			} else if (a < 3 && b == 3 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "31", lang);
			} else if (a < 3 && b == 4 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "32", lang);
			} else if (a < 3 && b == 5 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "33", lang);
			} else if (a < 3 && b == 6 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "34", lang);
			} else if (a < 3 && b == 7 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "35", lang);
			} else if (a < 3 && b == 8 && d == 0) {
				txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "36", lang);
			}

		}

		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);
		return subSection;

	}

	/* *************************************** 
	 * 11 Options
	 * A = 3 to 5 year career plan
	 * B = N of strong career preferences
	 *****************************************/
	private SubSection getFocusText(SubSection subSection, String keyMap, String key) {
		SubSectionText subText = new SubSectionText();

		String txt = "";
		int a = carPlan; // ITEM 127
		int b = nStrongPrefs;

		// System.out.println("carPlan=" + carPlan + " nStrongPref=" +
		// nStrongPrefs);

		if (a >= 1 && a <= 2 && b >= 4 && b <= 5) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else if (a >= 1 && a <= 2 && b >= 2 && b <= 3) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (a >= 3 && a <= 4 && b >= 4 && b <= 5) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (a >= 1 && a <= 2 && b >= 0 && b <= 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (a == 3 && b >= 2 && b <= 3) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (a == 4 && b == 3) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (a >= 5 && a <= 6 && b >= 4 && b <= 5) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (a >= 3 && a <= 4 && b >= 0 && b <= 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "8", lang);
		} else if (a == 4 && b == 2) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "9", lang);
		} else if (a >= 5 && a <= 6 && b >= 2 && b <= 3) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "10", lang);
		} else if (a >= 5 && a <= 6 && b >= 0 && b <= 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "11", lang);
		} else {
			System.out.println("interest focus subsection 1 is empty!");
		}

		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);

		String elseKey = "elseText";
		String preListKey = "preListText";

		SubSectionText listText = null;

		if (nStrongPrefs == 5) {
			listText = new SubSectionText();
			listText.setText(staticContent.getKeyMapCaseSentenceLangValue(keyMap, preListKey, null, "1", lang));
			subSection.getSubSectionText().add(listText);
			addFocusList(subSection, true, 1);

		} else if (nStrongPrefs >= 1 && nStrongPrefs <= 4) {
			listText = new SubSectionText();
			listText.setText(staticContent.getKeyMapCaseSentenceLangValue(keyMap, preListKey, null, "1", lang));
			subSection.getSubSectionText().add(listText);

			addFocusList(subSection, false, 1);

			listText = new SubSectionText();
			listText.setText(replaceContentVar(staticContent.getKeyMapValue4Lang(keyMap, elseKey, lang)));
			subSection.getSubSectionText().add(listText);

			addFocusList(subSection, false, 0);

		} else if (nStrongPrefs == 0) {
			listText = new SubSectionText();
			listText.setText(replaceContentVar(staticContent.getKeyMapCaseSentenceLangValue(keyMap, preListKey, null,
					"2", lang)));
			subSection.getSubSectionText().add(listText);

			addFocusList(subSection, true, 1);
		} else {
			System.out.println("interest focus list content is empty!");
		}
		return subSection;
	}

	private void addFocusList(SubSection subSection, boolean full, int filterNum) {
		List<Integer> prefList = Arrays.asList(rolePref, orgPref, indPref, sizePref, functPref);

		SubSectionText listText = new SubSectionText();
		String listContent = "<ul>";
		for (int i = 0; i < prefList.size(); i++) {
			if (full) {
				listContent += "<li>";
				listContent += staticContent.getKeyMapCaseSentenceLangValue("focusSubsection", "listContent", null,
						String.valueOf(i + 1), lang);
				listContent += "</li>";
			} else {
				if (prefList.get(i) == filterNum) {
					listContent += "<li>";
					listContent += staticContent.getKeyMapCaseSentenceLangValue("focusSubsection", "listContent", null,
							String.valueOf(i + 1), lang);
					listContent += "</li>";
				}
			}
		}
		listContent += "</ul>";
		listText.setText(replaceContentVar(listContent));
		subSection.getSubSectionText().add(listText);
	}

	/******************************************************************************************************************
	 * Engagement 6 options A = Number of power attributes picked = count of the
	 * number of ideal role attributes picked that are flagged as power related
	 * ( can range from 1 to 7 ) B = Engagement score = 100 *( n power
	 * attributes/n attributes ) rounded to an integer (range = 0 to 100) N
	 * attributes = total number of ideal role attributes picked (can range from
	 * 1 to 7)
	 * 
	 *******************************************************************************************************************/
	private SubSection getEngagementText(SubSection subSection, String keyMap, String key) {
		String txt = "";
		SubSectionText subText = new SubSectionText();
		// System.out.println("nIdealAttributes=" + nIdealAttributes +
		// " engageScorePct=" + engageScorePct);

		// int b = engageScorePct;
		// Sept 26 2014: Bruce says to use engagementPct instead of
		// engageScorePct

		int a = nIdealAttributes;
		int b = engagementPct;

		if (a >= 1 && a <= 3 && b >= 75) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else if (a >= 1 && a <= 3 && b >= 25 && b <= 74) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (a >= 1 && a <= 3 && b >= 1 && b <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (a >= 4 && a <= 7 && b >= 75) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (a >= 4 && a <= 7 && b >= 25 && b < 74) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (a >= 4 && a <= 7 && b >= 1 && b <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else {
			System.out.println("The interestEngagementPara content is empty.");
		}
		subText.setText(replaceContentVar(replaceContentVar(txt)));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	private SectionText getExperiencePara(String keyMap, String key) {
		// System.out.println("?? core="+core+" perspective="+perspective+" keyChallenges="+keyChallenges);
		String txt = null;
		if (core == 1 && perspective == 1 && keyChallenges == 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else if (core == 0 && perspective == 1 && keyChallenges == 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (core == 1 && perspective == 1 && keyChallenges == 0) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (core == 1 && perspective == 0 && keyChallenges == 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (core == 1 && perspective == 0 && keyChallenges == 0) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (core == 0 && perspective == 1 && keyChallenges == 0) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (core == 0 && perspective == 0 && keyChallenges == 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (core == 0 && perspective == 0 && keyChallenges == 0) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "8", lang);
		} else {
			System.out.println("The experienceMainPara content is empty.");
		}
		SectionText sectionText = new SectionText();
		sectionText.setText(replaceContentVar(txt));
		return sectionText;
	}

	private SubSection getCoreText(SubSection subSection, String keyMap, String key) {

		/********************************************************************************
		 * Scoring ks: assume core_experience and core_expected are from input
		 * scoring
		 * 
		 * Core 9 options Use the option based on the indicated pattern of
		 * scores A = Core Experience: if actual core experience exceeds
		 * expected = ?more?, if equivalent = ?same?, if actual is less than
		 * expected = ?less? B = Pace = total tenure / number) of roles held
		 * (rounded to integer) = yrs per role for leader?s progress
		 *********************************************************************************/
		String txt = null;
		SubSectionText subText = new SubSectionText();

		String relCoreExp = null;
		/*  Core calculation now uses the Percentile score and 
		 *  not the comparison below 
		 
		if (coreActual > coreExpected) {
			relCoreExp = "more";
		} else if (coreActual == coreExpected) {
			relCoreExp = "same";
		} else if (coreActual < coreExpected) {
			relCoreExp = "less";
		}
		
		*/

		if (corePct >= 75) {
			relCoreExp = "more";
		} else if (corePct < 25) {
			relCoreExp = "less";
		} else {
			relCoreExp = "same";
		}
		// System.out.println("coreActual=" + coreActual + " coreExpected=" +
		// coreExpected + " relCoreExp=" + relCoreExp
		// + " pace=" + pace);
		if (relCoreExp.equals("more") && pace >= 5) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else if (relCoreExp.equals("more") && pace >= 3 && pace <= 4) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (relCoreExp.equals("more") && pace >= 1 && pace <= 2) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (relCoreExp.equals("same") && pace >= 5) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (relCoreExp.equals("same") && pace >= 3 && pace <= 4) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (relCoreExp.equals("same") && pace >= 1 && pace <= 2) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (relCoreExp.equals("less") && pace >= 5) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (relCoreExp.equals("less") && pace >= 3 && pace <= 4) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "8", lang);
		} else if (relCoreExp.equals("less") && pace >= 1 && pace <= 2) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "9", lang);
		} else {
			System.out.println("The experienceCorePara content is empty.");
		}
		// only one subSectionText need to return
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	private SubSection getPerspectiveText(SubSection subSection, String keyMap, String key) {
		String txt1 = "";

		SubSectionText subText1 = new SubSectionText();
		key = "text1";
		if (perspectivePct >= 75 && perspectivePct <= 99) {
			txt1 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else if (perspectivePct >= 50 && perspectivePct <= 74) {
			txt1 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (perspectivePct >= 25 && perspectivePct <= 49) {
			txt1 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (perspectivePct >= 1 && perspectivePct <= 24) {
			txt1 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else {
			System.out.println("Experience perspective first para content is empty.");
		}

		subText1.setText(replaceContentVar(txt1));
		subSection.getSubSectionText().add(subText1);

		// re-design dropped this section. leave it for future use just in case.
		// SubSectionText subText2 = new SubSectionText();
		//
		// key = "text2";
		//
		// sentence1 = staticContent.getKeyMapCaseSentenceLangValue(keyMap,
		// key, "1", null, lang);
		//
		// System.out.println("ks-expNOrgs="+expNOrgs+" expNOrgTypes="+expNOrgSizes+" expNIndustries="+expNIndustries
		// +" expNRoleTypes="+expNRoleTypes+" expNFunctAreas="+expNFunctAreas+" expNCountries="+expNCountries);
		//
		// if (expNOrgs > 1 && expNOrgTypes == 1 && expNOrgSizes == 1 &&
		// expNIndustries == 1) {
		// sentence2 = staticContent.getKeyMapCaseSentenceLangValue(keyMap,
		// key, "2", "1", lang);
		// } else if (expNOrgs > 1 && expNOrgTypes > 1 && expNOrgSizes == 1 &&
		// expNIndustries == 1) {
		// sentence2 = staticContent.getKeyMapCaseSentenceLangValue(keyMap,
		// key, "2", "2", lang);
		// } else if (expNOrgs > 1 && expNOrgTypes == 1 && expNOrgSizes > 1 &&
		// expNIndustries == 1) {
		// sentence2 = staticContent.getKeyMapCaseSentenceLangValue(keyMap,
		// key, "2", "3", lang);
		// } else if (expNOrgs > 1 && expNOrgTypes == 1 && expNOrgSizes == 1 &&
		// expNIndustries > 1) {
		// sentence2 = staticContent.getKeyMapCaseSentenceLangValue(keyMap,
		// key, "2", "4", lang);
		// } else if (expNOrgs > 1 && expNOrgTypes > 1 && expNOrgSizes > 1 &&
		// expNIndustries == 1) {
		// sentence2 = staticContent.getKeyMapCaseSentenceLangValue(keyMap,
		// key, "2", "5", lang);
		// } else if (expNOrgs > 1 && expNOrgTypes > 1 && expNOrgSizes == 1 &&
		// expNIndustries > 1) {
		// sentence2 = staticContent.getKeyMapCaseSentenceLangValue(keyMap,
		// key, "2", "6", lang);
		// } else if (expNOrgs > 1 && expNOrgTypes == 1 && expNOrgSizes > 1 &&
		// expNIndustries > 1) {
		// sentence2 = staticContent.getKeyMapCaseSentenceLangValue(keyMap,
		// key, "2", "7", lang);
		// } else if (expNOrgs > 1 && expNOrgTypes > 1 && expNOrgSizes > 1 &&
		// expNIndustries > 1) {
		// sentence2 = staticContent.getKeyMapCaseSentenceLangValue(keyMap,
		// key, "2", "8", lang);
		// } else {
		// System.out
		// .println("Experience perspective second para sentence2 content is empty.");
		// }
		//
		// if (expNRoleTypes == 1 && expNFunctAreas == 1) {
		// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap,
		// key, "3", "1", lang);
		// } else if (expNRoleTypes > 1 && expNFunctAreas > 1) {
		// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap,
		// key, "3", "2", lang);
		// } else if (expNRoleTypes > 1 && expNFunctAreas == 1) {
		// sentence3 = staticContent.getKeyMapCaseSentenceLangValue(keyMap,
		// key, "3", "3", lang);
		// } else {
		// System.out
		// .println("Experience perspective second para sentence3 content is empty.");
		// }
		//
		// if(expNCountries > 1){
		// sentence4 = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key,
		// "4", null, lang);
		// }
		// if (!sentence1.equals("") && !sentence2.equals("") &&
		// !sentence3.equals("") && !sentence4.equals("")) {
		// txt2 = sentence1 + sentence2 + sentence3 + sentence4;
		// } else {
		// System.out.println("some part of experience perspective content is empty.");
		// return null;
		// }
		// //only one subText need to return
		// subText2.setText(replaceContentVar(txt2));
		// subSection.getSubSectionText().add(subText2);

		return subSection;
	}

	private SubSection getKeyChallengeText(SubSection subSection, String keyMap, String key) {
		String txt = "";
		SubSectionText subText = new SubSectionText();

		if (nKeyChallenges >= 6 && keyChallengePct >= 75 && keyChallengePct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else if (nKeyChallenges >= 6 && keyChallengePct >= 25 && keyChallengePct <= 74) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (nKeyChallenges >= 6 && keyChallengePct >= 1 && keyChallengePct <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (nKeyChallenges >= 3 && nKeyChallenges <= 5 && keyChallengePct >= 75 && keyChallengePct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (nKeyChallenges >= 3 && nKeyChallenges <= 5 && keyChallengePct >= 25 && keyChallengePct <= 74) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (nKeyChallenges >= 3 && nKeyChallenges <= 5 && keyChallengePct >= 1 && keyChallengePct <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (nKeyChallenges >= 0 && nKeyChallenges <= 2 && keyChallengePct >= 75 && keyChallengePct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (nKeyChallenges >= 0 && nKeyChallenges <= 2 && keyChallengePct >= 25 && keyChallengePct <= 74) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "8", lang);
		} else if (nKeyChallenges >= 0 && nKeyChallenges <= 2 && keyChallengePct >= 1 && keyChallengePct <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "9", lang);
		} else {
			System.out.println("The experienceKeyChallengePara content is empty.");
		}

		String checkMarktxt = "";
		String checkmarkKey = "checkmarkText";
		checkMarktxt = staticContent.getKeyMapValue4Lang(keyMap, checkmarkKey, lang);

		txt = txt + " " + checkMarktxt;
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	private SectionText getSelfAwarePara(String keyMap, String key) {
		String txt = "";

		if (slfAware == 1 && sitAware == 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else if (slfAware == 1 && sitAware == 0) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (slfAware == 0 && sitAware == 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (slfAware == 0 && sitAware == 0) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else {
			System.out.println("The selfAwareMainPara content is empty.");
		}
		SectionText sectionText = new SectionText();
		sectionText.setText(replaceContentVar(txt));
		return sectionText;
	}

	private SubSection getSelfText(SubSection subSection, String keyMap, String key) {
		String txt = "";
		SubSectionText subText = new SubSectionText();

		if (slfAwarePct >= 91 && slfAwarePct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (slfAwarePct >= 66 && slfAwarePct <= 90) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (slfAwarePct >= 51 && slfAwarePct <= 65) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (slfAwarePct >= 35 && slfAwarePct <= 50) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (slfAwarePct >= 25 && slfAwarePct <= 34) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (slfAwarePct >= 11 && slfAwarePct <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (slfAwarePct >= 1 && slfAwarePct <= 10) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else {
			log.debug("The selfAware Self content is empty.");
		}
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	private SubSection getSitText(SubSection subSection, String keyMap, String key) {
		String txt = "";
		SubSectionText subText = new SubSectionText();

		if (sitAwarePct >= 91 && sitAwarePct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (sitAwarePct >= 66 && sitAwarePct <= 90) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (sitAwarePct >= 51 && sitAwarePct <= 65) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (sitAwarePct >= 35 && sitAwarePct <= 50) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (sitAwarePct >= 25 && sitAwarePct <= 34) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (sitAwarePct >= 11 && sitAwarePct <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (sitAwarePct >= 1 && sitAwarePct <= 10) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else {
			System.out.println("The selfAware sit content is empty.");
		}
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	private SectionText getLearningAgilityPara(String keyMap, String key) {
		String txt = "";
		if (lrnAgile == 4) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else if (lrnAgile == 3) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (lrnAgile == 2) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (lrnAgile == 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (lrnAgile == 0) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else {
			System.out.println("The learningAgilityMainPara content is empty.");
		}
		SectionText sectionText = new SectionText();
		sectionText.setText(replaceContentVar(txt));
		return sectionText;

	}

	private SubSection getPeopleText(SubSection subSection, String keyMap, String key) {
		String txt = "";
		// String extraText = "";
		if (peoplePct >= 91 && peoplePct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (peoplePct >= 66 && peoplePct <= 90) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (peoplePct >= 51 && peoplePct <= 65) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (peoplePct >= 35 && peoplePct <= 50) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (peoplePct >= 25 && peoplePct <= 34) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (peoplePct >= 11 && peoplePct <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (peoplePct >= 1 && peoplePct <= 10) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else {
			System.out.println("The agility people content is allowed to be empty.");
		}

		SubSectionText subText = new SubSectionText();
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);

		// Text below not needed now as it is incorporated into main block
		/*if (peoplePct >= 91 && peoplePct <= 99) {
			extraText = staticContent.getKeyMapValue4Lang(keyMap, "extraText", lang);

			SubSectionText subNote = new SubSectionText();
			subNote.setText(replaceContentVar(extraText));
			subSection.getSubSectionText().add(subNote);
		}*/
		return subSection;
	}

	private SubSection getResultsText(SubSection subSection, String keyMap, String key) {
		String txt = "";
		// String extraText = "";
		if (resultsPct >= 91 && resultsPct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (resultsPct >= 66 && resultsPct <= 90) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (resultsPct >= 51 && resultsPct <= 65) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (resultsPct >= 35 && resultsPct <= 50) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (resultsPct >= 25 && resultsPct <= 34) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (resultsPct >= 11 && resultsPct <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (resultsPct >= 1 && resultsPct <= 10) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else {
			System.out.println("The agility results content is allowed to be empty.");
		}

		SubSectionText subText = new SubSectionText();
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);

		// Text below not needed now as it is incorporated into main block
		/*if (resultsPct >= 91 && resultsPct <= 99) {
			extraText = staticContent.getKeyMapValue4Lang(keyMap, "extraText", lang);
			SubSectionText subNote = new SubSectionText();
			subNote.setText(replaceContentVar(extraText));
			subSection.getSubSectionText().add(subNote);
		}*/
		return subSection;
	}

	private SubSection getMentalText(SubSection subSection, String keyMap, String key) {
		String txt = "";
		// String extraText = "";
		if (mentalPct >= 91 && mentalPct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (mentalPct >= 66 && mentalPct <= 90) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (mentalPct >= 51 && mentalPct <= 65) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (mentalPct >= 35 && mentalPct <= 50) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (mentalPct >= 25 && mentalPct <= 34) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (mentalPct >= 11 && mentalPct <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (mentalPct >= 1 && mentalPct <= 10) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else {
			System.out.println("The agility mental content is allowed to be empty.");
		}

		SubSectionText subText = new SubSectionText();
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);

		// Text below not needed now as it is incorporated into main block
		/*if (mentalPct >= 91 && mentalPct <= 99) {
			extraText = staticContent.getKeyMapValue4Lang(keyMap, "extraText", lang);
			SubSectionText subNote = new SubSectionText();
			subNote.setText(replaceContentVar(extraText));
			subSection.getSubSectionText().add(subNote);

		}*/
		return subSection;
	}

	private SubSection getChangeText(SubSection subSection, String keyMap, String key) {
		String txt = "";
		// String extraText = "";
		if (changePct >= 91 && changePct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (changePct >= 66 && changePct <= 90) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (changePct >= 51 && changePct <= 65) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (changePct >= 35 && changePct <= 50) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (changePct >= 25 && changePct <= 34) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (changePct >= 11 && changePct <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (changePct >= 1 && changePct <= 10) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else {
			System.out.println("The agility change content is allowed to be empty.");
		}
		SubSectionText subText = new SubSectionText();
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);

		// Text below not needed now as it is incorporated into main block
		/*if (changePct >= 91 && changePct <= 99) {
			extraText = staticContent.getKeyMapValue4Lang(keyMap, "extraText", lang);
			SubSectionText subNote = new SubSectionText();
			subNote.setText(replaceContentVar(extraText));
			subSection.getSubSectionText().add(subNote);
		}*/
		return subSection;
	}

	/****************************************************************************
	 * Summarize the results of five leadership traits The traits and their KFPI
	 * components are listed below ? Composed (KFPI ? Composure) ? Sociable
	 * (KFPI ? Sociability) ? Optimistic (KFPI ? Optimism) ? Focused (KFPI ?
	 * Focus) ? Risk Oriented (KFPI ? risk taking)
	 * 
	 * Scoring 28. Norm each trait score relative to the participant?s career
	 * stage 29. Count the number of traits at or above the 15th percentile =
	 * NTraits
	 ****************************************************************************/

	private SectionText getLeadershipTraitsPara(String keyMap, String key) {
		String txt = "";
		if (ldrTraits == 5) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else if (ldrTraits == 4) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (ldrTraits == 3) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (ldrTraits == 2) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (ldrTraits == 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (ldrTraits == 0) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else {
			System.out.println("The leadershipTraits main Para content is empty.");
		}
		SectionText sectionText = new SectionText();
		sectionText.setText(replaceContentVar(txt));
		return sectionText;
	}

	/********************************************************
	 * ks: is this use focused? int pctl so, we assume not use following input.
	 * but get other input ids instead.
	 * 
	 * kfpi_composure = ITM224 kfpi_sociability = ITM218 kfpi_optimism = ITM211
	 * kfpi_risk_taking = ITM209
	 * 
	 *********************************************************/
	private SubSection getFocusedText(SubSection subSection, String keyMap, String key) {
		String txt = "";
		// String extraText = "";
		if (focusedPct >= 91 && focusedPct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (focusedPct >= 66 && focusedPct <= 90) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (focusedPct >= 51 && focusedPct <= 65) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (focusedPct >= 35 && focusedPct <= 50) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (focusedPct >= 25 && focusedPct <= 34) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (focusedPct >= 11 && focusedPct <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (focusedPct >= 1 && focusedPct <= 10) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else {
			System.out.println("The leadershipTrait focused content is allowed to be empty.");
		}

		SubSectionText subText = new SubSectionText();
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);

		// Text below not needed now as it is incorporated into main block
		/*if (focusedPct >= 91 && focusedPct <= 99) {
			extraText = staticContent.getKeyMapValue4Lang(keyMap, "extraText", lang);
			SubSectionText subNote = new SubSectionText();
			subNote.setText(replaceContentVar(extraText));
			subSection.getSubSectionText().add(subNote);
		}*/
		return subSection;
	}

	private SubSection getSociableText(SubSection subSection, String keyMap, String key) {
		String txt = "";
		// String extraText = "";
		if (sociablePct >= 91 && sociablePct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (sociablePct >= 66 && sociablePct <= 90) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (sociablePct >= 51 && sociablePct <= 65) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (sociablePct >= 35 && sociablePct <= 50) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (sociablePct >= 25 && sociablePct <= 34) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (sociablePct >= 11 && sociablePct <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (sociablePct >= 1 && sociablePct <= 10) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else {
			System.out.println("The leadershipTraits sociable content is allowed to be empty.");
		}
		SubSectionText subText = new SubSectionText();
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);

		// Text below not needed now as it is incorporated into main block
		/*if (sociablePct >= 91 && sociablePct <= 99) {
			extraText = staticContent.getKeyMapValue4Lang(keyMap, "extraText", lang);
			SubSectionText subNote = new SubSectionText();
			subNote.setText(replaceContentVar(extraText));
			subSection.getSubSectionText().add(subNote);
		}*/
		return subSection;
	}

	private SubSection getToleranceOfAmbiguityText(SubSection subSection, String keyMap, String key) {
		String txt = "";
		// String extraText = "";
		if (tolerancePct >= 91 && tolerancePct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (tolerancePct >= 66 && tolerancePct <= 90) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (tolerancePct >= 51 && tolerancePct <= 65) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (tolerancePct >= 35 && tolerancePct <= 50) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (tolerancePct >= 25 && tolerancePct <= 34) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (tolerancePct >= 11 && tolerancePct <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (tolerancePct >= 1 && tolerancePct <= 10) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else {
			System.out.println("The traits tolerance subsection content is allowed to be empty.");
		}
		SubSectionText subText = new SubSectionText();
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);

		// Text below not needed now as it is incorporated into main block
		/*if (tolerancePct >= 91 && tolerancePct <= 99) {
			extraText = staticContent.getKeyMapValue4Lang(keyMap, "extraText", lang);
			SubSectionText subNote = new SubSectionText();
			subNote.setText(replaceContentVar(extraText));
			subSection.getSubSectionText().add(subNote);
		}*/
		return subSection;
	}

	private SubSection getComposedText(SubSection subSection, String keyMap, String key) {
		String txt = "";
		// String extraText = "";
		if (composedPct >= 91 && composedPct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (composedPct >= 66 && composedPct <= 90) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (composedPct >= 51 && composedPct <= 65) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (composedPct >= 35 && composedPct <= 50) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (composedPct >= 25 && composedPct <= 34) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (composedPct >= 11 && composedPct <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (composedPct >= 1 && composedPct <= 10) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else {
			System.out.println("The traits composed subsection content is allowed to be empty.");
		}
		SubSectionText subText = new SubSectionText();
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);

		// Text below not needed now as it is incorporated into main block
		/*if (composedPct >= 91 && composedPct <= 99) {
			extraText = staticContent.getKeyMapValue4Lang(keyMap, "extraText", lang);
			SubSectionText subNote = new SubSectionText();
			subNote.setText(replaceContentVar(extraText));
			subSection.getSubSectionText().add(subNote);
		}*/
		return subSection;
	}

	private SubSection getOptimisticText(SubSection subSection, String keyMap, String key) {
		String txt = "";
		// String extraText = "";
		if (optimismPct >= 91 && optimismPct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (optimismPct >= 66 && optimismPct <= 90) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (optimismPct >= 51 && optimismPct <= 65) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (optimismPct >= 35 && optimismPct <= 50) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (optimismPct >= 25 && optimismPct <= 34) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (optimismPct >= 11 && optimismPct <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (optimismPct >= 1 && optimismPct <= 10) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else {
			System.out.println("The traits composed subsection content is allowed to be empty.");
		}
		SubSectionText subText = new SubSectionText();
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);

		// Text below not needed now as it is incorporated into main block
		/*if (optimismPct >= 91 && optimismPct <= 99) {
			extraText = staticContent.getKeyMapValue4Lang(keyMap, "extraText", lang);
			SubSectionText subNote = new SubSectionText();
			subNote.setText(replaceContentVar(extraText));
			subSection.getSubSectionText().add(subNote);
		}*/
		return subSection;
	}

	// Capacity main para content is static
	private SectionText getCapacityPara(String keyMap, String key) {
		SectionText sectionText = new SectionText();
		sectionText.setText(replaceContentVar(staticContent.getKeyMapValue4Lang(keyMap, key, lang)));
		return sectionText;
	}

	private SubSection getProblemSolvingText(SubSection subSection, String keyMap, String key) {
		String txt = "";
		if (capacityPct >= 91 && capacityPct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (capacityPct >= 66 && capacityPct <= 90) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (capacityPct >= 51 && capacityPct <= 65) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (capacityPct >= 35 && capacityPct <= 50) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (capacityPct >= 25 && capacityPct <= 34) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (capacityPct >= 11 && capacityPct <= 24) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (capacityPct >= 1 && capacityPct <= 10) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else {
			System.out.println("The capacity main para is empty.");
		}

		SubSectionText subText = new SubSectionText();
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);

		// Text below not needed now as it is incorporated into main block
		/*if (capacityPct >= 91 && capacityPct <= 99) {
			String extraText = "";
			extraText = staticContent.getKeyMapValue4Lang(keyMap, "extraText", lang);
			if (extraText != null) {
				SubSectionText subNote = new SubSectionText();
				subNote.setText(replaceContentVar(extraText));
				subSection.getSubSectionText().add(subNote);
			} else {
				System.out.println("The problem solving extra para is empty.");
			}
		}*/
		return subSection;
	}

	private SectionText getDerailmentPara(String keyMap, String key) {

		String txt = "";
		if (volatil == 1 && micro == 1 && closed == 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else if (volatil == 1 && micro == 1 && closed == 0) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (volatil == 1 && micro == 0 && closed == 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (volatil == 0 && micro == 1 && closed == 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else if (volatil == 1 && micro == 0 && closed == 0) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "5", lang);
		} else if (volatil == 0 && micro == 1 && closed == 0) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "6", lang);
		} else if (volatil == 0 && micro == 0 && closed == 1) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "7", lang);
		} else if (volatil == 0 && micro == 0 && closed == 0) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "8", lang);
		} else {
			System.out.println("The derailmentP1 is empty.");
		}
		SectionText sectionText = new SectionText();
		sectionText.setText(replaceContentVar(txt));
		return sectionText;
	}

	private SubSection getVolatileText(SubSection subSection, String keyMap, String key) {
		String txt = "";

		if (volatilePct >= 91 && volatilePct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else if (volatilePct >= 85 && volatilePct <= 90) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (volatilePct >= 50 && volatilePct <= 84) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (volatilePct >= 1 && volatilePct <= 49) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else {
			System.out.println("The volatileText is empty.");
		}
		SubSectionText subText = new SubSectionText();
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	private SubSection getMicroText(SubSection subSection, String keyMap, String key) {
		String txt = "";

		if (microPct >= 91 && microPct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else if (microPct >= 85 && microPct <= 90) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (microPct >= 50 && microPct <= 84) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (microPct >= 1 && microPct <= 49) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else {
			System.out.println("The micro text is empty.");
		}

		SubSectionText subText = new SubSectionText();
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	private SubSection getClosedText(SubSection subSection, String keyMap, String key) {
		String txt = "";

		if (closedPct >= 91 && closedPct <= 99) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "1", lang);
		} else if (closedPct >= 85 && closedPct <= 90) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "2", lang);
		} else if (closedPct >= 50 && closedPct <= 84) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "3", lang);
		} else if (closedPct >= 1 && closedPct <= 49) {
			txt = staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, "4", lang);
		} else {
			System.out.println("The closed is empty.");
		}
		SubSectionText subText = new SubSectionText();
		subText.setText(replaceContentVar(txt));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	// starting build list
	private List<ChallengeListItem> getChallengeListItem() {
		List<ChallengeListItem> challengeItemList = new ArrayList<ChallengeListItem>();
		ChallengeListItem challengeItem = null;

		for (int i = 0; i < KEY_CHALLENGE_LIST.size(); i++) {
			challengeItem = new ChallengeListItem();
			challengeItem.setText(KEY_CHALLENGE_LIST.get(i));
			challengeItem.setIsAddressing(false);

			for (int j = 0; j < key_challenge_list.size(); j++) {
				if (j == i && key_challenge_list.get(j) == 1)
					challengeItem.setIsAddressing(true);
			}
			challengeItemList.add(challengeItem);
		}
		return challengeItemList;
	}

	private List<Level> getLevelItem() {

		String keyMap = "level";
		String key = "label";

		List<Level> levelList = new ArrayList<Level>();
		Level level = null;

		// System.out.println(" ROLE_LEVEL_LIST.size()=" +
		// ROLE_LEVEL_LIST.size());

		int x = ROLE_LEVEL_LIST.size() - 1;
		for (int i = x; i > -1; i--) {
			// System.out.println(" current level ="+aCurrRoleLevel+" aspiration level="+aLtAspiration+" target level="+aTgtRole);
			// System.out.println(" New Role Level i ="+i+" level text="+ROLE_LEVEL_LIST.get(i));
			level = new Level();
			// level.setLabel(ROLE_LEVEL_LIST.get(i));
			level.setLabel(staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null, String.valueOf(i + 1), lang));
			if (i == aCurrRoleLevel)
				level.setIsCurrent(true);
			if (i == aLtAspiration)
				level.setIsAspiration(true);
			if (i == aTgtRole)
				level.setIsTarget(true);

			levelList.add(level);
		}
		return levelList;
	}

	// this part does not display in re-design doc and it is un-tested.
	// private List<Recommendation> getRecommendationItem() {
	// List<Recommendation> recommList = new ArrayList<Recommendation>();
	// Recommendation recomm = null;
	// for (int i = 1; i < 5; i++) {
	// recomm = new Recommendation();
	// recomm.setYearsLabel(staticContent.getKeyMapValue4Lang("recommendation",
	// "yearsLabel", lang));
	//
	// if (i == 1) {
	// recomm.setMin(new BigInteger(String.valueOf(2)));
	// recomm.setMax(new BigInteger(String.valueOf(i + 2)));
	// recomm.setIsCurrent(true);
	// } else if (i > 1 && i < 4) {
	// recomm.setMin(new BigInteger(String.valueOf(3)));
	// recomm.setMax(new BigInteger(String.valueOf(5)));
	// recomm.setIsCurrent(false);
	// } else if (i == 4) {
	// recomm.setMin(new BigInteger(String.valueOf(8)));
	// recomm.setMax(new BigInteger(String.valueOf(13)));
	// recomm.setIsCurrent(true);
	// }
	// recomm.setText(staticContent.getKeyMapCaseSentenceLangValue("recommendation",
	// "text", null,
	// String.valueOf(i), lang));
	// recommList.add(recomm);
	// }
	// return recommList;
	// }

	private List<Priority> getPriorityItem() {
		String keyMap = "priority";
		List<Priority> priorityList = new ArrayList<Priority>();

		final int impt = 3;

		int prioCapacityPct;

		// if no Ravens score (capacity) we don't want it to show up as a
		// development priority

		if (NO_RAVENS) {
			prioCapacityPct = 99;
		} else {
			prioCapacityPct = capacityPct;
		}

		// in some case because the factor pct allow to divated by 5%, the text
		// to display may different
		int[][] prioTable = new int[][] {
				// (new int[] { 1, drivePct, calcPriority(drivePct), 2, impt,
				// calcGap(drivePct) }),
				// Don't ever make Drive appear in the Development Priorities
				// per Stacy Davies
				// Setting it to 99 so it will not show up and changed column 4
				// to a 6
				(new int[] { 1, 99, calcPriority(99), 6, impt, calcGap(99) }),
				(new int[] { 2, focusPct, calcPriority(focusPct), 2, impt, calcGap(focusPct) }),
				(new int[] { 3, engagementPct, calcPriority(engagementPct), 2, impt, calcGap(engagementPct) }),
				(new int[] { 4, corePct, calcPriority(corePct), 1, impt, calcGap(corePct) }),
				(new int[] { 5, perspectivePct, calcPriority(perspectivePct), 2, impt, calcGap(perspectivePct) }),
				(new int[] { 6, keyChallengePct, calcPriority(keyChallengePct), 2, impt, calcGap(keyChallengePct) }),
				(new int[] { 7, slfAwarePct, calcPriority(slfAwarePct), 3, impt, calcGap(slfAwarePct) }),
				(new int[] { 8, sitAwarePct, calcPriority(sitAwarePct), 3, impt, calcGap(sitAwarePct) }),
				(new int[] { 9, peoplePct, calcPriority(peoplePct), 3, impt, calcGap(peoplePct) }),
				(new int[] { 10, changePct, calcPriority(changePct), 3, impt, calcGap(changePct) }),
				(new int[] { 11, resultsPct, calcPriority(resultsPct), 3, impt, calcGap(resultsPct) }),
				(new int[] { 12, mentalPct, calcPriority(mentalPct), 3, impt, calcGap(mentalPct) }),
				(new int[] { 13, focusedPct, calcPriority(focusedPct), 3, impt, calcGap(focusedPct) }),
				(new int[] { 14, sociablePct, calcPriority(sociablePct), 3, impt, calcGap(sociablePct) }),
				(new int[] { 15, tolerancePct, calcPriority(tolerancePct), 3, impt, calcGap(tolerancePct) }),
				(new int[] { 16, composedPct, calcPriority(composedPct), 3, impt, calcGap(composedPct) }),
				(new int[] { 17, optimismPct, calcPriority(optimismPct), 3, impt, calcGap(optimismPct) }),
				(new int[] { 18, prioCapacityPct, calcPriority(prioCapacityPct), 5, impt, calcGap(prioCapacityPct) }), };

		// following prioTable hard code data for test case alop hammer down to
		// validate the sorting function is working.
		// the test result is same as For QA and TESTING - KFALP ORIG - Score
		// and Report_08-28-2014.xlsm Case 8.
		// final int[][] prioTable = new int[][] {
		// (new int[] { 1, 60, 3, 2, impt, 20 }),
		// (new int[] { 2, 75, 3, 3, impt, 5 }),
		// (new int[] { 3, 64, 3, 3, impt, 16 }),
		// (new int[] { 4, 99, 3, 3, impt, -19 }),
		// (new int[] { 5, 25, 2, 3, impt, 55 }),
		// (new int[] { 6, 37, 2, 3, impt, 43 }),
		// (new int[] { 7, 96, 3, 2, impt, -16 }),
		// (new int[] { 8, 88, 3, 3, impt, -8 }),
		// (new int[] { 9, 53, 3, 5, impt, 27 }),
		// (new int[] { 10, 52, 3, 5, impt, 28 }),
		// (new int[] { 11, 4, 1, 1, impt, 76 }),
		// (new int[] { 12, 90, 3, 2, impt, -10 }),
		// (new int[] { 13, 96, 3, 2, impt, -16 }),
		// (new int[] { 14, 20, 1, 2, impt, 60 }),
		// (new int[] { 15, 85, 3, 3, impt, -5 }),
		// (new int[] { 16, 66, 3, 3, impt, 14 }),
		// (new int[] { 17, 96, 3, 3, impt, -16 }),
		// (new int[] { 18, 25, 2, 3, impt, 55 }), };

		// use test
		// KfapUtils.print(test);
		// System.out.println();

		/* sort logic
		 * 1. first sort on column 3, index 2 for priority col by ascendng
		 *  	if there is dup on first sort, do second sort
		 * 2. second sort on column 4 index 3 for effort by ascendng
		 * 		if there is dup on second sort, do third and final sort
		 * 3. third sort on column 5 index 4 for gap. by decscenting
		 */

		// first sort priority(3rd) column from low to high-ascending
		// KfapUtils.sortPriorityTable(prioTable);
		Arrays.sort(prioTable, new SortDevPriorities());
		// for (int[] i : prioTable) {
		// System.out.println("SPOrder : " + i[0] + " Percentile : " + i[1] +
		// " Priority : " + i[2] + " Effort : "
		// + i[3] + " Impt: " + i[4] + " Gap : " + i[5]);
		// }
		// System.out.println("first sort: to 3 row for priority column 3 values: value1 = "+prioTable[0][2]
		// +" value2 = "+prioTable[1][2]+" value3 = "+prioTable[2][2]);
		// NEED TIE BRAKER IF more than 3 items for column 3 are the SAME!!!
		// AHHSDAR@*(@!E$!@

		// List<int[]> sortTop3 = new ArrayList<int[]>();
		//
		// int[] fistSortTop1 = { prioTable[0][2], prioTable[0][3],
		// prioTable[0][5] };
		// int[] fistSortTop2 = { prioTable[1][2], prioTable[1][3],
		// prioTable[1][5] };
		// int[] fistSortTop3 = { prioTable[2][2], prioTable[2][3],
		// prioTable[2][5] };
		//
		// sortTop3.add(fistSortTop1);
		// sortTop3.add(fistSortTop2);
		// sortTop3.add(fistSortTop3);
		//
		// // skip impt sort. spec: for impt all the same at the moment but is a
		// // placeholder for future revisions
		// boolean dup = KfapUtils.hasTop3Duplicates(sortTop3);
		//
		// // System.out.println("first sort dup?="+dup);
		// // System.out.println("first sort row1 ="+prioTable[0][2]);
		// // System.out.println("first sort row2 ="+prioTable[1][2]);
		// // System.out.println("first sort row3 ="+prioTable[2][2]);
		//
		// if (dup) {
		// // second sort effort(4rd) column from low to high-ascending
		// KfapUtils.sortPriorityTable(prioTable, 3, true);
		// dup = false;
		// }
		//
		// int[] secondSortTop1 = { prioTable[0][2], prioTable[0][3],
		// prioTable[0][5] };
		// int[] secondSortTop2 = { prioTable[1][2], prioTable[1][3],
		// prioTable[1][5] };
		// int[] secondSortTop3 = { prioTable[2][2], prioTable[2][3],
		// prioTable[2][5] };
		//
		// sortTop3.clear();
		// sortTop3.add(secondSortTop1);
		// sortTop3.add(secondSortTop2);
		// sortTop3.add(secondSortTop3);
		//
		// dup = KfapUtils.hasTop3Duplicates(sortTop3);
		//
		// // System.out.println("after second sort, dup="+dup);
		// //
		// System.out.println("second sort row1 col2="+prioTable[0][2]+" col3="+prioTable[0][3]);
		// //
		// System.out.println("second sort row2 col2="+prioTable[1][2]+" col3="+prioTable[1][3]);
		// //
		// System.out.println("second sort row3 col2="+prioTable[2][2]+" col3="+prioTable[2][3]);
		//
		// if (dup) {
		// // third sort gap(5rd) column from high to low-descending
		// KfapUtils.sortPriorityTable(prioTable, 5, false); // third sort: gap
		// }
		//
		// // finally done all sorts if applicable
		// // pick up top three row to display 3 priorities
		int[] topPriorities = { prioTable[0][0], prioTable[1][0], prioTable[2][0] };

		// POSSIBLY NEED TO SORT TOP THREE BASED ON SPORDER (HARD CODED ORDER)
		// IF THERE ARE TIES THROUGH THE 3RD SORT

		// System.out.println("All sorts are done, the final top priorities: #1="
		// + prioTable[0][0] + " #2="
		// + prioTable[1][0] + " #3=" + prioTable[2][0]);

		Priority priority = null;
		int case2ListLength = 7;
		int case18ListLength = 3;
		for (int i = 0; i < topPriorities.length; i++) {
			String contents = "";
			priority = new Priority();
			priority.setLabel(staticContent.getKeyMapCaseSentenceLangValue(keyMap, "label", null,
					String.valueOf(i + 1), lang));

			contents += "<b>";
			contents += staticContent.getKeyMapCaseSentenceLangValue(keyMap, "text", "1",
					String.valueOf(topPriorities[i]), lang);
			contents += "</b>";

			contents += staticContent.getKeyMapCaseSentenceLangValue(keyMap, "text", "2",
					String.valueOf(topPriorities[i]), lang);

			if (topPriorities[i] == 2) {
				contents += "<p><br>";
				for (int j = 0; j < case2ListLength; j++) {
					contents += staticContent.getKeyMapCaseSentenceLangValue(keyMap, "listForCase2", null,
							String.valueOf(j + 1), lang);
					contents += "<br>";
				}
				contents += "</p>";
			}
			if (topPriorities[i] == 18) {
				contents += "<p><br>";
				for (int j = 0; j < case18ListLength; j++) {
					contents += staticContent.getKeyMapCaseSentenceLangValue(keyMap, "listForCase18", null,
							String.valueOf(j + 1), lang);
					contents += "<br>";
				}
				contents += "</p>";
			}
			priority.setText(contents);
			priorityList.add(priority);
		}
		return priorityList;
	}

	private int calcPriority(int pct) {
		if (pct < 25)
			pct = 1;
		else if (pct >= 25 && pct <= 50)
			pct = 2;
		else if (pct > 50)
			pct = 3;

		return pct;
	}

	private int calcGap(int pct) {
		return 99 - pct;
	}

	private List<BackgroundListItem> getBackgroundListItem() {
		String keyMap = "backgroundListItem";
		String key = "text";

		List<BackgroundListItem> backgroundItemList = new ArrayList<BackgroundListItem>();
		List<Integer> items = Arrays.asList(yrsInWorkforce, yrsInManagement, nOrgs, yrsInCurrOrg, yrsInCurrRole,
				yrsOnBoard, nCountries);
		BackgroundListItem backgroudItem = null;

		for (int i = 0; i < items.size(); i++) {
			backgroudItem = new BackgroundListItem();
			backgroudItem.setNumberLabel(new BigInteger(String.valueOf(items.get(i))));
			backgroudItem.setText(staticContent.getKeyMapCaseSentenceLangValue(keyMap, key, null,
					String.valueOf(i + 1), lang));
			backgroundItemList.add(backgroudItem);
		}
		return backgroundItemList;
	}

	private GoalsObjectivesList getGoalsObjectivesList() {
		// does first bulletPoint dynamic?
		GoalsObjectivesList goalsObjectives = new GoalsObjectivesList();

		goalsObjectives.getBulletPoint().add(
				staticContent.getKeyMapCaseSentenceLangValue("goalsObjectivesList", "bullet1", null,
						String.valueOf(carPlan), lang));

		goalsObjectives.getBulletPoint().add(
				staticContent.getKeyMapCaseSentenceLangValue("goalsObjectivesList", "bullet2", null,
						String.valueOf(threeToFiveYrsGoal), lang));

		// goalsObjectives = new GoalsObjectivesList();
		goalsObjectives.getBulletPoint().add(
				replaceContentVar(staticContent.getKeyMapCaseSentenceLangValue("goalsObjectivesList", "bullet3", null,
						String.valueOf(ltAspiration), lang)));

		return goalsObjectives;
	}

	private IdealRoleList getIdealRoleList() {
		IdealRoleList idealRoleList = new IdealRoleList();
		@SuppressWarnings("unchecked")
		List<String> selTxtList = (List<String>) getListObject(ideal_role_list, IDEAL_ROLE_LIST, STR_ARR);
		// System.out.println(" selected ideal role list size="+selTxtList);
		for (int i = 0; i < selTxtList.size(); i++) {
			idealRoleList.getBulletPoint().add(selTxtList.get(i));
		}
		return idealRoleList;
	}

	private List<CategoryItem> getOrgTypeCategoryItem() {
		List<CategoryItem> categoryItemList = new ArrayList<CategoryItem>();
		String currOrgTypeTxt = ORG_TYPE_LIST_CURRENT.get(aCurrOrgType);

		// List<String> selOrgTypeTxt = (List<String>)
		// getListObject(org_type_list, ORG_TYPE_LIST, STR_ARR);
		// List<String> prefOrgTypeTxt = (List<String>)
		// getListObject(pref_org_type_list, ORG_TYPE_LIST, STR_ARR);
		// for (int i = 0; i < selOrgTypeTxt.size(); i++) {
		// System.out.println("to build exp OrgTypeTxt=" +
		// selOrgTypeTxt.get(i));
		// }
		// for (int i = 0; i < prefOrgTypeTxt.size(); i++) {
		// System.out.println("to build fut OrgTypeTxt=" +
		// prefOrgTypeTxt.get(i));
		// }
		// System.out.println("build curr OrgType="+currOrgTypeTxt);

		buildCategoryMetric(categoryItemList, org_type_list, pref_org_type_list, ORG_TYPE_LIST, currOrgTypeTxt);

		return categoryItemList;
	}

	private List<CategoryItem> getOrgSizeCategoryItem() {
		List<CategoryItem> categoryItemList = new ArrayList<CategoryItem>();
		String currOrgSizeTxt = ORG_SIZE_LIST.get(aCurrOrgSize);

		// List<String> selOrgSizeTxt = (List<String>)
		// getListObject(org_size_list, ORG_SIZE_LIST, STR_ARR);
		// List<String> prefOrgSizeTxt = (List<String>)
		// getListObject(pref_org_size_list, ORG_SIZE_LIST, STR_ARR);
		// for (int i = 0; i < selOrgSizeTxt.size(); i++) {
		// System.out.println("to build exp OrgSizeTxt=" +
		// selOrgSizeTxt.get(i));
		// }
		// for (int i = 0; i < prefOrgSizeTxt.size(); i++) {
		// System.out.println("to build fut prefOrgSizeTxt=" +
		// prefOrgSizeTxt.get(i));
		// }
		//
		// System.out.println("build curr OrgSizeTxt=" + currOrgSizeTxt);

		buildCategoryMetric(categoryItemList, org_size_list, pref_org_size_list, ORG_SIZE_LIST, currOrgSizeTxt);

		return categoryItemList;
	}

	private List<CategoryItem> getIndTypeCategoryItem() {
		List<CategoryItem> categoryItemList = new ArrayList<CategoryItem>();
		String currIndTxt = IND_TYPE_LIST.get(aCurrIndustry);

		// List<String> selIndTxt = (List<String>) getListObject(ind_type_list,
		// IND_TYPE_LIST, STR_ARR);
		// List<String> prefIndTxt = (List<String>)
		// getListObject(pref_ind_type_list, IND_TYPE_LIST, STR_ARR);
		// for (int i = 0; i < selIndTxt.size(); i++) {
		// System.out.println("build exp indTxt=" + selIndTxt.get(i));
		// }
		// for (int i = 0; i < prefIndTxt.size(); i++) {
		// System.out.println("build fut prefIndTxt=" + prefIndTxt.get(i));
		// }
		// System.out.println("build curr IndustryTxt=" + currIndTxt);

		buildCategoryMetric(categoryItemList, ind_type_list, pref_ind_type_list, IND_TYPE_LIST, currIndTxt);

		return categoryItemList;
	}

	private List<CategoryItem> getRoleTypeCategoryItem() {
		List<CategoryItem> categoryItemList = new ArrayList<CategoryItem>();
		String currRoleTypeTxt = ROLE_TYPE_LIST.get(aCurrRoleType);

		// List<String> selRoleTypeTxt = (List<String>)
		// getListObject(role_type_list, ROLE_TYPE_LIST, STR_ARR);
		// List<String> prefRoleTypeTxt = (List<String>)
		// getListObject(pref_role_type_list, ROLE_TYPE_LIST, STR_ARR);
		// for (int i = 0; i < selRoleTypeTxt.size(); i++) {
		// System.out.println("build exp RoleTypeTxt=" + selRoleTypeTxt.get(i));
		// }
		// for (int i = 0; i < prefRoleTypeTxt.size(); i++) {
		// System.out.println("build fut RoleTypeTxt=" +
		// prefRoleTypeTxt.get(i));
		// }

		buildCategoryMetric(categoryItemList, role_type_list, pref_role_type_list, ROLE_TYPE_LIST, currRoleTypeTxt);

		return categoryItemList;
	}

	private List<CategoryItem> getFunctAreaCategoryItem() {
		List<CategoryItem> categoryItemList = new ArrayList<CategoryItem>();
		String currFunctAreaTxt = FUNCT_AREA_LIST_CURRENT.get(aCurrFunctArea);

		// List<String> selFunctAreaTxt = (List<String>)
		// getListObject(funct_area_list, FUNCT_AREA_LIST, STR_ARR);
		// List<String> prefFunctAreaTxt = (List<String>)
		// getListObject(pref_funct_area_list, FUNCT_AREA_LIST,
		// STR_ARR);
		// for (int i = 0; i < selFunctAreaTxt.size(); i++) {
		// System.out.println("build exp FunctAreaTxt=" +
		// selFunctAreaTxt.get(i));
		// }
		// for (int i = 0; i < prefFunctAreaTxt.size(); i++) {
		// System.out.println("build fut FunctAreaTxt=" +
		// prefFunctAreaTxt.get(i));
		// }
		// System.out.println("build curr FunctAreaTxt=" + currFunctAreaTxt);

		buildCategoryMetric(categoryItemList, funct_area_list, pref_funct_area_list, FUNCT_AREA_LIST, currFunctAreaTxt);

		return categoryItemList;
	}

	private List<CategoryItem> buildCategoryMetric(List<CategoryItem> categoryItemList, List<Integer> expList,
			List<Integer> futList, List<String> txtList, String currTxt) {

		CategoryItem categoryItem = null;

		/* lower case List is int list and up case list is string list.
		 * int list has value 0 or 1 and Stirng list has String text 
		 * the pair lists with matched indexs. so, when int list value = 1
		 * we grab that text from string list  
		 */

		Map<String, String> tableMap = new HashMap<String, String>();

		for (int i = 0; i < txtList.size(); i++) {
			String txtElm = txtList.get(i);
			boolean isCurr = txtElm.equals(currTxt) ? true : false;

			String result = KfapUtils.getCategoryDisplayText(isCurr, expList.get(i), futList.get(i));
			if (!result.equals("")) {
				tableMap.put(txtElm, result);
			}
		}

		// start to sort table that sync with ordered array list
		String[][] tempTable = new String[tableMap.size()][3];
		int row = 0;
		for (Map.Entry<String, String> entry : tableMap.entrySet()) {
			// System.out.println("table mapKey : " + entry.getKey() +
			// " table mapValue : " + entry.getValue());

			for (int i = 0; i < txtList.size(); i++) {
				if (txtList.get(i).toString().equals(entry.getKey())) {
					tempTable[row][0] = String.valueOf(i);
					tempTable[row][1] = entry.getKey();
					tempTable[row][2] = entry.getValue();
				}
			}
			row++;
		}

		// System.out.println("before sort");
		// KfapUtils.print(tempTable);

		KfapUtils.sortCategoryTable(tempTable, 0);

		// System.out.println("after sort");
		// KfapUtils.print(tempTable);

		// create a linked hash map to implement FIFO then filling it with
		// correctly ordered elements

		LinkedHashMap<String, String> lHashMap = new LinkedHashMap<String, String>();
		for (int rows = 0; rows < tempTable.length; rows++) {
			String col1 = "";
			String col2 = "";
			for (int cols = 1; cols < tempTable[rows].length; cols++) {
				if (cols == 1)
					col1 = tempTable[rows][cols];
				if (cols == 2)
					col2 = tempTable[rows][cols];
			}
			lHashMap.put(col1, col2);
		}

		/* current list txt is not always same as experience and pref list.
		 * if it not exists in experience and pref list, we add it at end of the linked hashmap 
		 */

		if (!txtList.contains(currTxt)) {
			lHashMap.put(currTxt, ",isCurr");
		}

		for (@SuppressWarnings("rawtypes")
		Map.Entry entry : lHashMap.entrySet()) {
			// System.out.println(" new table key=" + entry.getKey() + " value="
			// + entry.getValue());
			categoryItem = new CategoryItem();
			categoryItem.setLabel((String) entry.getKey());

			String entryValue = (String) entry.getValue();

			if (entryValue.contains("isCurr"))
				categoryItem.setIsCurrent(true);
			if (entryValue.contains("isExp"))
				categoryItem.setHasTwoPlusYears(true);
			if (entryValue.contains("isFut"))
				categoryItem.setWouldConsiderInFuture(true);

			categoryItemList.add(categoryItem);
		}

		return categoryItemList;
	}

	private String replaceContentVar(String content) {
		if (StringUtils.isNotEmpty(content)) {
			Pattern pattern = Pattern.compile("\\[(.*?)\\]");
			Matcher matcher = pattern.matcher(content);
			boolean vAvAnFound = false;
			// for each variable found
			while (matcher.find()) {
				String matchTxt = matcher.group().toString().replace("[", "").replace("]", "");
				Iterator<Map.Entry<String, String>> it = varTextWrapper.entrySet().iterator();
				// for each mapped variable value
				while (it.hasNext()) {
					Map.Entry<String, String> entry = it.next();
					if (entry.getKey().equals(matchTxt)) {
						if (entry.getKey().equals("vDateComplete") || entry.getKey().equals("vClient")) {
							content = content.replace(matcher.group().toString(), entry.getValue().toString());
						} else if (entry.getKey().equals("vAvAn")) {
							// log.debug("FOUND A/AN VAR, SKIP FOR NOW");
							vAvAnFound = true;
						} else {
							// entry.getValue().toString().toLowerCase();
							content = content.replace(matcher.group().toString(), entry.getValue().toString()
									.toLowerCase());
						}
					}
				}
			}
			// Logic to put in 'a' or 'an' based on next character
			if (vAvAnFound == true) {
				// Do find again to find all vAvAn vars
				Matcher matcher2 = pattern.matcher(content);
				while (matcher2.find()) {
					String matchTxt = matcher2.group().toString().replace("[", "").replace("]", "");
					Iterator<Map.Entry<String, String>> it = varTextWrapper.entrySet().iterator();
					while (it.hasNext()) {
						Map.Entry<String, String> entry = it.next();
						if (entry.getKey().equals(matchTxt)) {
							String nextValue = content.substring(matcher2.end(), matcher2.end() + 2);
							nextValue = nextValue.trim();
							String[] VOWELS = new String[] { "a", "e", "i", "o", "u" };

							// log.debug("CHECK VOWEL[] against " + nextValue);
							if (!Arrays.asList(VOWELS).contains(nextValue)) {
								// log.debug("NO VOWEL FOUND, USE 'a'");
								content = content.replace(matcher2.group().toString(), "a");
							} else {
								// log.debug("NO VOWEL FOUND, USE 'an'");
								content = content.replace(matcher2.group().toString(), "an");
							}
						}
					}
				}
			}
		}
		return content;
	}

	private Boolean getIsStrength(int value) {
		return value == 1 ? true : false;
	}

	private int roundPct(int pct) {
		if (pct < 1)
			pct = 1;
		if (pct > 99)
			pct = 99;
		return pct;
	}

	private String marshalOutput(KfapIndividualFeedbackReport kfapIndividualFeedbackReport) {
		JAXBContext jc = null;
		Marshaller m = null;
		StringWriter sw = new StringWriter();
		try {
			jc = JAXBContext.newInstance(KfapIndividualFeedbackReport.class);
			m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(kfapIndividualFeedbackReport, sw);

			return sw.toString();
		} catch (JAXBException jbe) {
			log.error("potential KfpReportRgrToRcm.marshalOutput() - JAXBException: Msg={}", jbe.getMessage());
			jbe.printStackTrace();
			return null;
		}
	}

	// private void reverseListElements(List<String> list) {
	// Collections.reverse(list);
	// }

	public Object getListObject(List<Integer> intList, List<String> strList, int returnType) {

		// if return type 1 or 3 need both intList and strList and the
		// isSelected does not matter
		// if return type 2 or 4, a intList parameter only
		List<String> itemStrList = new ArrayList<String>();
		int itemIntValue = 0;

		if (returnType == 1) {
			for (int i = 0; i < intList.size(); i++) {
				if (intList.get(i) == 1) {
					for (int j = 0; j < strList.size(); j++) {
						if (j == i) {
							itemStrList.add(strList.get(i));
							// System.out.println("The Selected int List i="+i+" selected text List="+strList.get(i));
						}
					}
				}
			}
		} else {
			for (int j = 0; j < intList.size(); j++) {
				itemIntValue += 1;
			}
		}
		// 1= STR_ARRAY
		// 2= INT_SUM
		if (returnType == 1) {
			return itemStrList;
		} else if (returnType == 2) {
			return itemIntValue;
		} else {
			return null;
		}
	}

	private String readFile(String file) throws IOException {
		@SuppressWarnings("resource")
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();
		String ls = System.getProperty("line.separator");

		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line);
			stringBuilder.append(ls);
		}

		return stringBuilder.toString();
	}

	private Document getReportResponseData(String urlStr) {
		Document doc = null;
		try {
			URL url = new URL(urlStr);
			@SuppressWarnings("restriction")
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			String line = "";
			String xml = "";
			@SuppressWarnings("restriction")
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while ((line = in.readLine()) != null) {
				xml += line;
			}
			in.close();
			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				doc = docBuilder.parse(new InputSource(new ByteArrayInputStream(xml.getBytes())));
			} catch (ParserConfigurationException e) {
				log.error("Parser error creating document from ext. server response. Message: {}", e.getMessage());
				// e.printStackTrace();
				return null;
			} catch (IOException e) {
				log.error("IO error creating document from ext. server response. Message: {}", e.getMessage());
				// e.printStackTrace();
				return null;
			} catch (SAXException e) {
				log.error("SAX error creating document from ext. server response. Message: {}", e.getMessage());
				// e.printStackTrace();
				return null;
			}
		} catch (MalformedURLException e) {
			log.error("Malformed url: " + urlStr, e.getMessage());
		} catch (IOException e) {
			log.error("IOException url: " + urlStr, e.getMessage());
		}
		return doc;
	}

	// private Document getCalcScore(String type, String pid, String lan) {
	// String urlBase =
	// this.configProperties.getProperty("calculationRequestDataUrl");
	// // String calcScoreUrl = urlBase
	// // + RESTFUL_STRING_PARAMS.replace("{participantId}", pid +
	// // "").replace("{instrumentId}", KFP_INST + "")
	// // .replace("{manifest}", manifest);
	//
	// String calcScoreUrl = urlBase
	// + RESTFUL_STRING_PARAMS.replace("{participantId}", pid +
	// "").replace("{instrumentId}", KFP_INST + "")
	// .replace("{manifest}", manifest).replace("{tgtRole}",
	// String.valueOf(tgtRole));
	//
	// log.info("scored data fetch URL=" + calcScoreUrl);
	// Document doc = getReportResponseData(calcScoreUrl);
	// return doc;
	// }

	private String checkFile(String path) {
		try {
			return readFile(getReportGenRespDir() + "kfp-content.xml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	// private Document getRawResponse(String type, String pid, String lan) {
	// String urlBase =
	// this.configProperties.getProperty("tinCanAdminResponseDataUrl");
	// String rawRespUrl = urlBase
	// + TINCAN_STRING_PARAMS.replace("{participantId}", pid +
	// "").replace("{instrumentId}", KFP_INST);
	// log.info("Response data fetch URL=" + rawRespUrl);
	// Document doc = getReportResponseData(rawRespUrl);
	// return doc;
	// }

	// leave this method to test local input copy for testing

	// private Document loadInputXml(String path) {
	// DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
	// domFactory.setNamespaceAware(true);
	// DocumentBuilder builder;
	// Document doc = null;
	// try {
	// builder = domFactory.newDocumentBuilder();
	// // setXmlDoc(builder.parse(path));
	// doc = builder.parse(path);
	// } catch (ParserConfigurationException e) {
	// e.printStackTrace();
	// } catch (SAXException e) {
	// e.printStackTrace();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// return doc;
	// }

	@SuppressWarnings("unchecked")
	private void mapInputData(Document scoreDoc, String type, String pid, String lang) throws Exception {

		// assume get report title from para

		setReportGenRespDir(configProperties.getProperty("reportContentRepository")
				+ "/reportGenerationResponse/projects/internal/kfap/");

		// feed static content from raw question feed

		String staticContPath = checkFile(getReportGenRespDir() + "kfp-content.xml");
		ReportGenerationResponse content = new ReportGenerationResponse(staticContPath);
		Document contDoc = content.getXmlDoc();

		// separate lists for values captured as current because these values
		// were
		// being used to pull content from lists that are different questions
		// which
		// don't always have the same number of answers, i.e. kfpQ1 doesn't have
		// the same
		// number of answers as kfpQ15, yet the code is trying to pull content
		// from kfpQ15
		// using the value of kfpQ1 but the # of answer between the two don't
		// match
		ORG_TYPE_LIST_CURRENT = KfapUtils.getContentData(contDoc, KFP_INST, true, "kfpQ1");
		IND_TYPE_LIST_CURRENT = KfapUtils.getContentData(contDoc, KFP_INST, true, "kfpQ7");
		FUNCT_AREA_LIST_CURRENT = KfapUtils.getCurrentListContentData(contDoc, KFP_INST, true, "kfpQ8");

		ROLE_LEVEL_LIST = KfapUtils.getContentData(contDoc, KFP_INST, true, "kfpQ3"); // current
		ROLE_TYPE_LIST = KfapUtils.getContentData(contDoc, KFP_INST, true, "kfpQ4"); // current
		ORG_TYPE_LIST = KfapUtils.getContentData(contDoc, KFP_INST, false, "kfpQ15");
		ORG_SIZE_LIST = KfapUtils.getContentData(contDoc, KFP_INST, true, "kfpQ6"); // current
		IND_TYPE_LIST = KfapUtils.getContentData(contDoc, KFP_INST, false, "kfpQ19");
		FUNCT_AREA_LIST = KfapUtils.getContentData(contDoc, KFP_INST, false, "kfpQ20");
		KEY_CHALLENGE_LIST = KfapUtils.getContentData(contDoc, KFP_INST, false, "kfpQ22");
		KEY_CHALLENGE_LIST.remove(KEY_CHALLENGE_LIST.size() - 1);
		IDEAL_ROLE_LIST = KfapUtils.getContentData(contDoc, KFP_INST, false, "kfpQ27");

		// for test local copy of input data
		// String scorePath = checkFile(getReportGenRespDir() +
		// "calculationPayload.xml");
		// ReportGenerationResponse scoreCont = new
		// ReportGenerationResponse(scorePath);

		// String scoreCont = getReportGenRespDir() + "calculationPayload.xml";
		// Document scoreDoc = loadInputXml(scoreCont);

		// get integrated data from restful

		// 7 scaled scores to display the strength bar 1 for strength and 0 for
		// weakness
		interestScore = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"interest.score")));
		experienceScore = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.score")));
		agileScore = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "agile.score")));
		awarenessScore = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"awareness.score")));
		traitsScore = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "traits.score")));
		derailerScore = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"derailer.score")));

		// percentile scroe for factors under scales. data range from 1-99
		drivePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"interest.drive.norm")));
		focusPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"interest.focus.norm")));
		engagementPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"interest.engagement.norm")));
		perspectivePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.perspective.norm")));
		changePct = (int) Math.round(Double.parseDouble(KfapUtils
				.getScoredData(scoreDoc, KFP_INST, "agile.change.norm")));
		mentalPct = (int) Math.round(Double.parseDouble(KfapUtils
				.getScoredData(scoreDoc, KFP_INST, "agile.mental.norm")));
		peoplePct = (int) Math.round(Double.parseDouble(KfapUtils
				.getScoredData(scoreDoc, KFP_INST, "agile.people.norm")));
		resultsPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"agile.results.norm")));
		slfAwarePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"awareness.self.norm")));
		sitAwarePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"awareness.sit.norm")));
		focusedPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"traits.focus.norm")));
		optimismPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"traits.optimism.norm")));
		composedPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"traits.composure.norm")));
		closedPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"derailer.closed.norm")));
		microPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"derailer.micro.norm")));
		volatilePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"derailer.volatile.norm")));
		corePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.core.norm")));
		tolerancePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"traits.tolerance.norm")));
		sociablePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"traits.social.norm")));

		// factor scores 1 or 0
		drive = (int) Math
				.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "interest.drive.score")));
		focus = (int) Math
				.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "interest.focus.score")));
		engagement = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"interest.engagement.score")));
		perspective = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.perspective.score")));
		cAgile = (int) Math
				.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "agile.change.score")));
		mAgile = (int) Math
				.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "agile.mental.score")));
		pAgile = (int) Math
				.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "agile.people.score")));
		rAgile = (int) Math
				.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "agile.results.score")));
		slfAware = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"awareness.self.score")));
		sitAware = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"awareness.sit.score")));
		focused = (int) Math
				.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "traits.focus.score")));
		composed = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"traits.composure.score")));
		closed = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"derailer.closed.score")));
		micro = (int) Math
				.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "derailer.micro.score")));
		core = (int) Math
				.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "experience.core.score")));
		sociable = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"traits.social.score")));
		optimistic = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"traits.optimism.score")));
		tolerance = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"traits.tolerance.score")));
		volatil = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"derailer.volatile.score")));

		// calculated scores for scaled total count for its factors in some
		// measures.
		interest = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "interest.total")));
		experience = (int) Math.round(Double.parseDouble(KfapUtils
				.getScoredData(scoreDoc, KFP_INST, "experience.total")));
		awareness = (int) Math
				.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "awareness.total")));
		lrnAgile = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "agile.total")));
		ldrTraits = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "traits.total")));

		String capacityNorm = KfapUtils.getScoredData(scoreDoc, KFP_INST, "capacity.norm");
		String capacity = KfapUtils.getScoredData(scoreDoc, KFP_INST, "capacity.score");
		if (NO_RAVENS == false) {
			if (capacityNorm == null || capacityNorm == "" || capacityNorm.equals("NaN") || capacity == null
					|| capacity == "" || capacity.equals("NaN")) {
				NO_RAVENS = true;
			} else {
				capacityPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
						"capacity.norm")));
				capacityScore = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
						"capacity.score")));
				// explicitly set because when a report is regenrated it doesn't
				// seem to pick up the boolean at the top of this file
				NO_RAVENS = false;
			}
		}
		// scores for text logic uses.
		coreActual = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.core.actual")));
		coreExpected = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.core.expected")));
		pace = (int) Math
				.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "experience.core.pace")));
		highestRole = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.core.highest")));
		nKeyChallenges = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.key.nKey")));
		kcExp = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.key.rawKeyChallenge")));
		keyChallengePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.key.norm")));
		keyChallenges = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.key.score")));
		expNCountries = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.perspective.ExpNOrgCountry")));
		expNOrgs = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.perspective.ExpNOrgs")));
		expNIndustries = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.perspective.ExpOrgInd")));
		expNOrgSizes = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.perspective.ExpOrgSizes")));
		expNOrgTypes = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.perspective.ExpOrgTypes")));
		expNFunctAreas = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.perspective.ExpOrgsFunc")));
		expNRoleTypes = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"experience.perspective.ExpOrgsRole")));
		nIdealAttributes = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"interest.engagement.ideal")));

		engageScorePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"interest.engagement.engagementPct")));
		nStrongPrefs = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"interest.focus.strongPrefs")));
		rolePref = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"interest.focus.rolePref")));
		orgPref = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"interest.focus.orgTPref")));
		sizePref = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"interest.focus.orgSPref")));
		indPref = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"interest.focus.indPref")));
		functPref = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
				"interest.focus.funcPref")));

		// test local copy of input data
		// String rawPath = getReportGenRespDir() + "rawResponses.xml";
		// ReportGenerationResponse rawCont = new
		// ReportGenerationResponse(rawPath);
		// Document rawDoc = rawCont.getXmlDoc();

		// String rawCont = getReportGenRespDir() + "rawResponses.xml";
		//
		// Document rawDoc = loadInputXml(rawCont);

		// get integrated data from TinCan. Not used for now.
		//
		// Document rawDoc = getRawResponse(type, pid, lang);

		// get all item data from a loop of <integerDataArray id="resume"
		// calculationPayload.xml>

		@SuppressWarnings("rawtypes")
		Map scoreMap = KfapUtils.getScoredDataMap(scoreDoc, KFP_INST, "resume");
		Iterator<Entry<String, String>> it = scoreMap.entrySet().iterator();

		role_level_list.clear();
		org_type_list.clear();
		org_size_list.clear();
		ind_type_list.clear();
		role_type_list.clear();
		funct_area_list.clear();
		key_challenge_list.clear();
		ideal_role_list.clear();
		pref_org_type_list.clear();
		pref_org_size_list.clear();
		pref_ind_type_list.clear();
		pref_role_type_list.clear();
		pref_funct_area_list.clear();

		while (it.hasNext()) {
			Entry<String, String> pair = it.next();
			if (pair.getKey().equals("ITEM1")) {
				currOrgType = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM2")) {
				yrsInCurrOrg = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM3")) {
				currRoleLevel = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM4")) {
				currRoleType = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM5")) {
				yrsInCurrRole = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM6")) {
				currOrgSize = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM7")) {
				currIndustry = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM8")) {
				// sub industry
			} else if (pair.getKey().equals("ITEM9")) {
				currFunctArea = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM10")) {
				// sub funct area
			} else if (pair.getKey().equals("ITEM11")) {
				// year to born
			} else if (pair.getKey().equals("ITEM12")) {
				yrsInWorkforce = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM13")) {
				yrsInManagement = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM14")) {
				yrsOnBoard = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM15")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM16")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM17")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM18")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM19")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM20")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM21")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM22")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM23")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM24")) {
				nOrgs = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM25")) {
				org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM26")) {
				org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM27")) {
				org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM28")) {
				org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM29")) {
				org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM30")) {
				org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM31")) {
				org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM32")) {
				org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM33")) {
				org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM34")) {
				org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM35")) {
				org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM36")) {
				org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM37")) {
				role_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM38")) {
				role_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM39")) {
				carMix = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM40")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM41")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM42")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM43")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM44")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM45")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM46")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM47")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM48")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM49")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM50")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM51")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM52")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM53")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM54")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM55")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM56")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM57")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM58")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM59")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM60")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM61")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM62")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM63")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM64")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM65")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM66")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM67")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM68")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM69")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM70")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM71")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM72")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM73")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM74")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM75")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM76")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM77")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM78")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM79")) {
				nCountries = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM80")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM85")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM90")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM95")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM100")) {
				// ITEM100 can return a value greater than 1 so need to account
				// for that

				expatCount = Integer.parseInt(pair.getValue());

				if (expatCount > 0) {
					expatValue = 1;
				} else {
					expatValue = 0;
				}

				key_challenge_list.add(expatValue);
			} else if (pair.getKey().equals("ITEM101")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM106")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM111")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM116")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM121")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM127")) {
				carPlan = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM128")) {
				threeToFiveYrsGoal = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM129")) {
				jump = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM130")) {
				ltAspiration = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM131")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM132")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM133")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM134")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM135")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM136")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM137")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM138")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM139")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM140")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM141")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM142")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM143")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM144")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM145")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM146")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM147")) {
				// no strong org_type pref
			} else if (pair.getKey().equals("ITEM148")) {
				pref_org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM149")) {
				pref_org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM150")) {
				pref_org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM151")) {
				pref_org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM152")) {
				pref_org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM153")) {
				pref_org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM154")) {
				pref_org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM155")) {
				// no strong org size pref
			} else if (pair.getKey().equals("ITEM156")) {
				pref_org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM157")) {
				pref_org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM158")) {
				pref_org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM159")) {
				pref_org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM160")) {
				pref_org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM161")) {
				// no strong pref for role type
			} else if (pair.getKey().equals("ITEM162")) {
				pref_role_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM163")) {
				pref_role_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM164")) {
				// no strong pref for industry
			} else if (pair.getKey().equals("ITEM165")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM166")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM167")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM168")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM169")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM170")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM171")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM172")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM173")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM174")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM175")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM176")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM177")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM178")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM179")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM180")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM181")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM182")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM183")) {
				// no strong pref for funct area
			} else if (pair.getKey().equals("ITEM184")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM185")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM186")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM187")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM188")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM189")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM190")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM191")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM192")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM193")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM194")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM195")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM196")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM197")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM198")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM199")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM200")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM201")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM202")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM203")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM204")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM205")) {
				// don't know this
			}
		}

		// for (int i = 0; i < org_size_list.size(); i++) {
		// System.out.println("INT ORG SIZE ARRAY2=" + org_size_list.get(i));
		// }
		// for (int i = 0; i < pref_org_size_list.size(); i++) {
		// System.out.println("INT PREF ORG SIZE ARRAY2=" +
		// pref_org_size_list.get(i));
		// }
		// for (int i = 0; i < role_type_list.size(); i++) {
		// System.out.println("INT ROLE TYPE ARRAY2=" + role_type_list.get(i));
		// }
		// for (int i = 0; i < pref_role_type_list.size(); i++) {
		// System.out.println("INT PREF ROLE TYPE ARRAY2=" +
		// pref_role_type_list.get(i));
		// }

		aTgtRole = tgtRole - 1;
		aCurrRoleLevel = currRoleLevel - 1;
		aLtAspiration = ltAspiration - 1;
		// aNtAspiration = jump - 1;
		aCurrRoleType = currRoleType - 1;
		aCurrIndustry = currIndustry - 1;
		aCurrFunctArea = currFunctArea - 1;
		aCurrOrgType = currOrgType - 1;
		aCurrOrgSize = currOrgSize - 1;

		// vCurrRoleLevel = ROLE_LEVEL_LIST.get(aCurrRoleLevel);
		// vCurrRoleType = ROLE_TYPE_LIST.get(aCurrRoleType);
		// vCurrFunctArea = FUNCT_AREA_LIST.get(aCurrFunctArea);
		// vCurrOrgSize = ORG_SIZE_LIST.get(aCurrOrgSize);
		// vCurrOrgType = ORG_TYPE_LIST.get(aCurrOrgType);
		// vCurrIndustry = IND_TYPE_LIST.get(aCurrIndustry);

		// calculate sum of data
		nFunctAreas = (Integer) getListObject(funct_area_list, null, INT_SUM); // NFunctAreas
		nOrgTypes = (Integer) getListObject(org_type_list, null, INT_SUM); // NOrgTypes
		nIndustries = (Integer) getListObject(ind_type_list, null, INT_SUM); // Nindustries
		nOrgSizes = (Integer) getListObject(org_size_list, null, INT_SUM); // NOrgSizes
		nLevels = (Integer) getListObject(role_level_list, null, INT_SUM);
	}

	private void wrapDisplayVars() {

		// clear this guy before load new
		varTextWrapper.clear();

		varTextWrapper.put("vDateComplete", vDateComplete);
		varTextWrapper.put("vTgtRole", vTgtRole);
		varTextWrapper.put("vLTAspiration", vLTAspiration);
		// varTextWrapper.put("vNTAspiration", vNTAspiration);
		varTextWrapper.put("vAvAn", "");
		varTextWrapper.put("vClient", vClient);
		varTextWrapper.put("vCurrRoleType", ROLE_TYPE_LIST.get(aCurrRoleType));
		varTextWrapper.put("vCurrRoleLevel", ROLE_LEVEL_LIST.get(aCurrRoleLevel));
		varTextWrapper.put("vCurrFunctArea", FUNCT_AREA_LIST_CURRENT.get(aCurrFunctArea));
		varTextWrapper.put("vCurrOrgSize", ORG_SIZE_LIST.get(aCurrOrgSize));
		varTextWrapper.put("vCurrOrgType", ORG_TYPE_LIST_CURRENT.get(aCurrOrgType));
		varTextWrapper.put("vCurrIndustry", IND_TYPE_LIST_CURRENT.get(aCurrIndustry));
		varTextWrapper.put("vPace", KfapUtils.convertNumToWord(String.valueOf(pace)));
		varTextWrapper.put("vRoleTenure", KfapUtils.convertNumToWord(String.valueOf(yrsInCurrRole))); // ???
		varTextWrapper.put("vMgmtTenure", KfapUtils.convertNumToWord(String.valueOf(yrsInManagement)));
		varTextWrapper.put("vCarTenure", KfapUtils.convertNumToWord(String.valueOf(yrsInWorkforce)));
		varTextWrapper.put("vNlevels", KfapUtils.convertNumToWord(String.valueOf(nLevels)));
		varTextWrapper.put("vNFunctAreas", KfapUtils.convertNumToWord(String.valueOf(nFunctAreas)));
		varTextWrapper.put("vNorgs", KfapUtils.convertNumToWord(String.valueOf(nOrgs)));
		varTextWrapper.put("vNindustries", KfapUtils.convertNumToWord(String.valueOf(nIndustries)));
		varTextWrapper.put("vBoardTenure", KfapUtils.convertNumToWord(String.valueOf(yrsOnBoard)));

		// Career to Date do not using. array only used in that part comment out
		// for now.
		// varTextWrapper.put("vOrgTypeList", vOrgTypeList);
		// varTextWrapper.put("vOrgSizeList", vOrgSizeList);
		// varTextWrapper.put("vIndList", vIndList);
		// varTextWrapper.put("vRolePrefList", vRolePrefList);
		// varTextWrapper.put("vFunctPrefList", vFunctPrefList);
		// varTextWrapper.put("vSizePrefList", vSizePrefList);
		// varTextWrapper.put("vOrgPrefList", vOrgPrefList);
		// varTextWrapper.put("vIndPrefList", vIndPrefList);
	}

	public static String getReportManifestDir() {
		return reportManifestDir;
	}

	public static void setReportManifestDir(String reportManifestDir) {
		KfapReportRgrToRcm.reportManifestDir = reportManifestDir;
	}

	public static String getReportGenRespDir() {
		return reportGenRespDir;
	}

	public static void setReportGenRespDir(String reportGenRespDir) {
		KfapReportRgrToRcm.reportGenRespDir = reportGenRespDir;
	}

	public static String getReportContentDir() {
		return reportContentDir;
	}

	public static void setReportContentDir(String reportContentDir) {
		KfapReportRgrToRcm.reportContentDir = reportContentDir;
	}

	public ReportContent getVarContent() {
		return varContent;
	}

	public void setVariableContent(ReportContent varContent) {
		this.varContent = varContent;
	}

	public ReportContent getStaticContent() {
		return staticContent;
	}

	public void setStaticContent(ReportContent staticContent) {
		this.staticContent = staticContent;
	}
}
