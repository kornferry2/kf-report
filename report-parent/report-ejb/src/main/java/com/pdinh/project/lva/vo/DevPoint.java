/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import com.pdinh.core.vo.Hyperlink;

/*
 * DevPoint object used in the development section of LvaDbReport only.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder={"title", "text", "link"})
public class DevPoint
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String title;
	private String text;
	@XmlElementWrapper(name = "iaLinks")
	private List<Hyperlink> link;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public DevPoint()
	{
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "        DevPoint:  ";
		str += "title=" + this.title + ", text=" + this.text + "\n";
		if (link != null)
		{
			int i = 0;
			for (Hyperlink ll : this.link)
			{
				if (i > 0)
					str += "\n";
				str += "                " + ll.toString();
				i++;
			}
		}
				
		return str;
	}

	// Getters & setters

	//----------------------------------
	public String getTitle()
	{
		return this.title;
	}

	public void setTitle(String value)
	{
		this.title = value;
	}

	//----------------------------------
	public String getText()
	{
		return this.text;
	}

	public void setText(String value)
	{
		this.text = value;
	}

	//----------------------------------
	// Note that the list has a "get" only
	public List<Hyperlink> getLink()
	{
		if (this.link == null)
			this.link = new ArrayList<Hyperlink>();
		
		return this.link;
	}

}
