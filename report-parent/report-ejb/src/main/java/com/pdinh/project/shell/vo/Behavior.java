/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.shell.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder={"suggestion", "module"})
public class Behavior
{
	//
	// Static data.
	//
 
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute(name = "id")
	private String id;
	@XmlElementWrapper(name = "suggestions")
	private List<Suggestion> suggestion;
	@XmlElementWrapper(name = "modules")
	private List<Module> module;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Behavior()
	{
		// Default constructor
	}

	// 1-parameter constructor (clone)
	public Behavior(Behavior orig)
	{
		this.id = orig.getId() == null ? null : new String(orig.getId());
		if (orig.getSuggestion() == null)
		{
			this.suggestion = null;
		}
		else
		{
			this.suggestion = new ArrayList<Suggestion>();
			for (Suggestion ss : orig.getSuggestion())
			{
				this.suggestion.add(new Suggestion(ss));
			}
		}
		if (orig.getModule() == null)
		{
			this.module = null;
		}
		else
		{
			this.module = new ArrayList<Module>();
			for (Module mm : orig.getModule())
			{
				this.module.add(new Module(mm));
			}
		}
	}

	// 1-parameter constructor
	public Behavior(String id)
	{
		this.id = id;
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "Behavior:  ";
		str += "id=" + this.id + "\n";
		str += "            Suggestions:\n";
		for (Suggestion ds : this.suggestion)
		{
			str += "              " + ds.toString() + "\n";
		}
		str += "            IA Modules:\n";
		for (Module iam : this.module)
		{
			str += "              " + iam.toString() + "\n";
		}

		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getId()
	{
		return this.id;
	}

	public void setId(String value)
	{
		this.id = value;
	}

	//----------------------------------
	public List<Suggestion> getSuggestion()
	{
		if (this.suggestion == null)
			this.suggestion = new ArrayList<Suggestion>();
		
		return this.suggestion;
	}

	public void setSuggestion(List<Suggestion> value)
	{
		this.suggestion = value;
	}
	
	//----------------------------------
	public List<Module> getModule()
	{
		if (this.module == null)
			this.module = new ArrayList<Module>();
		
		return this.module;
	}
	
	public void setModule(List<Module> value)
	{
		this.module = value;
	}
}
