/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/*
 * Ratings object used for the recommendation section of LvaDbReport only.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder={"title", "text", "scoredSection"})
public class Ratings
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String title;
	private String text;
	@XmlElementWrapper(name = "scoredData")
	private List<ScoredSection> scoredSection;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Ratings()
	{
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "  Ratings:  ";
		str += "title=" + this.title + ", text=" + this.text + "\n";
		for (ScoredSection ss : this.scoredSection)
		{
			str += "\n      " + ss.toString();
		}
		
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getTitle()
	{
		return this.title;
	}

	public void setTitle(String value)
	{
		this.title = value;
	}

	//----------------------------------
	public String getText()
	{
		return this.text;
	}

	public void setText(String value)
	{
		this.text = value;
	}

	//----------------------------------
	// Note that the list has a "get" only
	public List<ScoredSection> getScoredSection()
	{
		if (this.scoredSection == null)
			this.scoredSection = new ArrayList<ScoredSection>();
		
		return this.scoredSection;
	}

}
