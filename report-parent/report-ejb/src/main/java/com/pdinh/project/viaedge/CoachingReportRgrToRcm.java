package com.pdinh.project.viaedge;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.pipeline.generator.vo.Stage;
import com.pdinh.pipeline.generator.vo.Var;
import com.pdinh.project.viaedge.reports.jaxb.AgilityScoresSection;
import com.pdinh.project.viaedge.reports.jaxb.IntroductionSection;
import com.pdinh.project.viaedge.reports.jaxb.OverallConfidenceSection;
import com.pdinh.project.viaedge.reports.jaxb.OverviewSection;
import com.pdinh.project.viaedge.reports.jaxb.Participant;
import com.pdinh.project.viaedge.reports.jaxb.Scale;
import com.pdinh.project.viaedge.reports.jaxb.Scales;
import com.pdinh.project.viaedge.reports.jaxb.VScale;
import com.pdinh.project.viaedge.reports.jaxb.VScales;
import com.pdinh.project.viaedge.reports.jaxb.VerificationScalesSection;
import com.pdinh.project.viaedge.reports.jaxb.ViaEdgeCoachingReport;
import com.pdinh.reportcontent.Manifest;
import com.pdinh.reportcontent.ReportContent;

@Stateless
@LocalBean
/* ############################################################# 

	Handles Individual Coaching and Individual Feedback Reports
	Feedback Report is only a slight variation/subset 
	
	############################################################ */
public class CoachingReportRgrToRcm {
	private static final Logger log = LoggerFactory.getLogger(CoachingReportRgrToRcm.class);
	private ReportContent staticContent = null;
	private ReportContent feedbackContent = null;
	private ReportContent agilityContent = null;
	private ReportContent sharedContent = null;
	// private static String SHORT_DATE_FORMAT = "dd-MMM-yyyy";
	// private final String LONG_DATE_FORMAT = "dd MMM yyyy";
	private String LONG_DATE_FORMAT = "MM/dd/yyyy";
	private ReportGenerationRequest rgr;
	private final OverviewSection overviewSection = new OverviewSection();
	private final OverallConfidenceSection cScaleSection = new OverallConfidenceSection();
	private final VerificationScalesSection vScalesSection = new VerificationScalesSection();
	private ViaEdgeCoachingReport report = new ViaEdgeCoachingReport();
	@EJB
	private UserDao userDao;
	private final String gridMapId = "agility";
	private String lang = "en-US";
	private final Map<String, String> agilities = new HashMap<String, String>();
	private final Map<String, String> agilityScales = new HashMap<String, String>();
	private final Map<String, String> verificationScales = new HashMap<String, String>();
	private final Map<String, String> confidence = new HashMap<String, String>();
	private int overallRanking;

	public ReportGenerationRequest getRgr() {
		return rgr;
	}

	public void setRgr(ReportGenerationRequest rgrp) {
		rgr.equals(rgrp);
	}

	@Resource(name = "application/report/config_properties", type = Properties.class)
	private Properties configProperties;

	public CoachingReportRgrToRcm() {

	}

	private final List<String> langCodes = Arrays.asList("en-US", "ja", "zh", "it", "fr", "de", "pt", "es", "ru", "nl",
			"pl", "tr", "ko");

	public String generateReportRcm(ReportGenerationRequest reportGenReq, String language, String reportType)
			throws PalmsException {
		// log.debug("starting generateReportRcm viaEdge iSummaryReport RGR to RCM");
		lang = langCodes.get(0);
		if (language != null) {
			if (langCodes.contains(language)) {
				lang = language;
			}
			if (language.equals("fr_CA")) {
				lang = langCodes.get(4);
			} else if (language.equals("pt_BR")) {
				lang = langCodes.get(6);
			} else if (language.equals("zh_CN")) {
				lang = langCodes.get(2);
			} else if (language.equals("en")) {
				lang = langCodes.get(0);
			}
		}
		this.rgr = reportGenReq;

		User participant = userDao.findById(Integer.parseInt(rgr.getParticipant().get(0).getExtId()));
		if (participant == null) {
			throw new PalmsException("User not found.", PalmsErrorCode.RCM_GEN_FAILED.getCode());
		}
		Participant part = new Participant();
		part.setFirstName(participant.getFirstname());
		part.setLastName(participant.getLastname());

		if (lang.equals("en-US"))
			LONG_DATE_FORMAT = "MM/dd/yyyy";
		if (lang.equals("ja"))
			LONG_DATE_FORMAT = "yyyy/MM/dd";
		if (lang.equals("zh"))
			LONG_DATE_FORMAT = "yyyy/MM/dd";
		if (lang.equals("it"))
			LONG_DATE_FORMAT = "dd/MM/yyyy";
		if (lang.equals("fr"))
			LONG_DATE_FORMAT = "dd/MM/yyyy";
		if (lang.equals("de"))
			LONG_DATE_FORMAT = "dd.MM.yyyy";
		if (lang.equals("pt"))
			LONG_DATE_FORMAT = "dd/MM/yyyy";
		if (lang.equals("es"))
			LONG_DATE_FORMAT = "dd/MM/yyyy";
		if (lang.equals("ru"))
			LONG_DATE_FORMAT = "dd.MM.yyyy";
		if (lang.equals("ko"))
			LONG_DATE_FORMAT = "yyyy/MM/dd";

		// SimpleDateFormat shortSdf = new SimpleDateFormat(SHORT_DATE_FORMAT);
		SimpleDateFormat longSdf = new SimpleDateFormat(LONG_DATE_FORMAT);

		String reportContentRepoBaseDir = configProperties.getProperty("reportContentRepository");
		// set manifest
		Manifest manifest = new Manifest(reportContentRepoBaseDir
				+ "/manifest/projects/internal/viaEdge/coaching/manifest.xml");
		// set agility model content from manifest (shared content)
		agilityContent = new ReportContent(reportContentRepoBaseDir + "/content/" + manifest.getModelContentRef()
				+ ".xml");
		// set static content from manifest
		staticContent = new ReportContent(reportContentRepoBaseDir + "/content/"
				+ manifest.getReportContents().get("staticContent") + ".xml");
		sharedContent = new ReportContent(reportContentRepoBaseDir + "/content/"
				+ manifest.getReportContents().get("sharedContent") + ".xml");
		feedbackContent = new ReportContent(reportContentRepoBaseDir + "/content/"
				+ manifest.getReportContents().get("feedbackContent") + ".xml");
		// new IndividualSummaryReport
		report = new ViaEdgeCoachingReport();
		report.setLang(language); // use iso code passed in, not 'lang'

		report.setCompanyName(rgr.getParticipant().get(0).getCompany());
		// copyright = copyright + in collaboration with... text
		String copyright = sharedContent.getKeyMapValue4Lang("shared", "copyright", lang);
		copyright += "<br/>";
		copyright += staticContent.getKeyMapValue4Lang("coaching", "collabText", lang);
		report.setCopyright(copyright);
		if (!reportType.equals(ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK)) {
			report.setReportTitle(staticContent.getKeyMapValue4Lang("coaching", "reportName", lang));
		} else {
			report.setReportTitle(feedbackContent.getKeyMapValue4Lang("feedback", "reportName", lang));
		}

		report.setParticipant(part);
		// GET STAGES FROM RGR PARTICIPANT
		com.pdinh.pipeline.generator.vo.Participant extP = rgr.getParticipant().get(0);
		List<Stage> stages = extP.getStage();
		report.setCompanyName(rgr.getParticipant().get(0).getCompany());
		report.setReportDate(longSdf.format(new Date()));
		report.setSerialNumber(rgr.getParticipant().get(0).getExtId());
		report.setProfileHeading(staticContent.getKeyMapValue4Lang("coaching", "profileHeading", lang));
		String gridMapId = "agility";
		IntroductionSection intro = new IntroductionSection();
		intro.setChangeDescription(agilityContent.getGridMapValue4Lang(gridMapId, "change", "description", lang));
		intro.setChangeDescriptionHeading(agilityContent.getGridMapValue4Lang(gridMapId, "change", "headingD", lang));
		intro.setMentalDescription(agilityContent.getGridMapValue4Lang(gridMapId, "mental", "description", lang));
		intro.setMentalDescriptionHeading(agilityContent.getGridMapValue4Lang(gridMapId, "mental", "headingD", lang));
		intro.setPeopleDescription(agilityContent.getGridMapValue4Lang(gridMapId, "people", "description", lang));
		intro.setPeopleDescriptionHeading(agilityContent.getGridMapValue4Lang(gridMapId, "people", "headingD", lang));
		intro.setResultsDescription(agilityContent.getGridMapValue4Lang(gridMapId, "results", "description", lang));
		intro.setResultsDescriptionHeading(agilityContent.getGridMapValue4Lang(gridMapId, "results", "headingD", lang));
		intro.setSelfAwareDescription(agilityContent.getGridMapValue4Lang(gridMapId, "selfAware", "description", lang));
		intro.setSelfAwareDescriptionHeading(agilityContent.getGridMapValue4Lang(gridMapId, "selfAware", "headingD",
				lang));
		intro.setP1(sharedContent.getKeyMapValue4Lang("shared", "p1", lang));
		intro.setP2(sharedContent.getKeyMapValue4Lang("shared", "p2", lang));
		intro.setP3(sharedContent.getKeyMapValue4Lang("shared", "p3", lang));
		intro.setTitle(sharedContent.getKeyMapValue4Lang("shared", "introductionTitle", lang));
		report.setIntroductionSection(intro);
		// OVERVIEW SECTION
		OverviewSection overview = new OverviewSection();
		overview.setTitle(staticContent.getKeyMapValue4Lang("coaching", "overviewTitle", lang));
		if (!reportType.equals(ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK)) {
			overview.setTitle(staticContent.getKeyMapValue4Lang("coaching", "overviewTitle", lang));
			overview.setPart1Heading(staticContent.getKeyMapValue4Lang("coaching", "overviewPart1Heading", lang));
			overview.setPart1B1(staticContent.getKeyMapValue4Lang("coaching", "overviewPart1Bullet1", lang));
			overview.setPart1B2(staticContent.getKeyMapValue4Lang("coaching", "overviewPart1Bullet2", lang));
			overview.setPart1B3(staticContent.getKeyMapValue4Lang("coaching", "overviewPart1Bullet3", lang));
			overview.setPart1B4(staticContent.getKeyMapValue4Lang("coaching", "overviewPart1Bullet4", lang));
			overview.setPart2Heading(staticContent.getKeyMapValue4Lang("coaching", "overviewPart2Heading", lang));
			overview.setPart2B1(staticContent.getKeyMapValue4Lang("coaching", "overviewPart2Bullet1", lang));
			overview.setPart2B2(staticContent.getKeyMapValue4Lang("coaching", "overviewPart2Bullet2", lang));
			overview.setPart3Heading(staticContent.getKeyMapValue4Lang("coaching", "overviewPart3Heading", lang));
			overview.setPart3B1(staticContent.getKeyMapValue4Lang("coaching", "overviewPart3Bullet1", lang));
			overview.setPart3B2(staticContent.getKeyMapValue4Lang("coaching", "overviewPart3Bullet2", lang));
			overview.setPart1B4S1(agilityContent.getGridMapValue4Lang(gridMapId, "mental", "headingB", lang));
			overview.setPart1B4S2(agilityContent.getGridMapValue4Lang(gridMapId, "people", "headingB", lang));
			overview.setPart1B4S3(agilityContent.getGridMapValue4Lang(gridMapId, "change", "headingB", lang));
			overview.setPart1B4S4(agilityContent.getGridMapValue4Lang(gridMapId, "results", "headingB", lang));
			overview.setPart1B4S5(agilityContent.getGridMapValue4Lang(gridMapId, "selfAware", "headingB", lang));
		} else {
			overview.setPart1Heading(feedbackContent.getKeyMapValue4Lang("feedback", "overviewPart1Heading", lang));
			overview.setPart1B1(feedbackContent.getKeyMapValue4Lang("feedback", "overviewPart1Bullet1", lang));
			overview.setPart1B2(feedbackContent.getKeyMapValue4Lang("feedback", "overviewPart1Bullet2", lang));
			overview.setPart1B3(feedbackContent.getKeyMapValue4Lang("feedback", "overviewPart1Bullet3", lang));
			overview.setPart1B4(feedbackContent.getKeyMapValue4Lang("feedback", "overviewPart1Bullet4", lang));
			overview.setPart1B4S1(agilityContent.getGridMapValue4Lang(gridMapId, "mental", "headingB", lang));
			overview.setPart1B4S2(agilityContent.getGridMapValue4Lang(gridMapId, "people", "headingB", lang));
			overview.setPart1B4S3(agilityContent.getGridMapValue4Lang(gridMapId, "change", "headingB", lang));
			overview.setPart1B4S4(agilityContent.getGridMapValue4Lang(gridMapId, "results", "headingB", lang));
			overview.setPart1B4S5(agilityContent.getGridMapValue4Lang(gridMapId, "selfAware", "headingB", lang));
		}

		report.setOverviewSection(overview);
		// AGILITY SCORES SECTION
		AgilityScoresSection agilitySection = new AgilityScoresSection();
		if (!reportType.equals(ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK)) {
			agilitySection.setTitle(staticContent.getKeyMapValue4Lang("coaching", "part1HeaderLabel", lang));
			agilitySection.setHeading(staticContent.getKeyMapValue4Lang("coaching", "part1Heading", lang));
			agilitySection.setP1Heading(staticContent.getKeyMapValue4Lang("coaching", "part1P1Heading", lang));
			agilitySection.setP1(staticContent.getKeyMapValue4Lang("coaching", "part1P1", lang));
			agilitySection.setP2Heading(staticContent.getKeyMapValue4Lang("coaching", "part1P2Heading", lang));
			agilitySection.setP2(staticContent.getKeyMapValue4Lang("coaching", "part1P2", lang));
			agilitySection.setP2B1(staticContent.getKeyMapValue4Lang("coaching", "part1P2TalentReviewScale", lang));
			agilitySection.setP2B2(staticContent.getKeyMapValue4Lang("coaching", "part1P2LearnerFeedbackScale", lang));
			agilitySection.setP3Heading(staticContent.getKeyMapValue4Lang("coaching", "part1P3Heading", lang));
			agilitySection.setP3(staticContent.getKeyMapValue4Lang("coaching", "part1P3", lang));
			agilitySection.setP4Heading(staticContent.getKeyMapValue4Lang("coaching", "part1P4Heading", lang));
			agilitySection.setP4(staticContent.getKeyMapValue4Lang("coaching", "part1P4", lang));
			agilitySection.setP5Heading(staticContent.getKeyMapValue4Lang("coaching", "part1P5Heading", lang));
			agilitySection.setP5(staticContent.getKeyMapValue4Lang("coaching", "part1P5", lang));
			agilitySection.setP6Heading(staticContent.getKeyMapValue4Lang("coaching", "part1P6Heading", lang));
			agilitySection.setP6(staticContent.getKeyMapValue4Lang("coaching", "part1P6", lang));
			agilitySection.setActivitiesListHeading(staticContent.getKeyMapValue4Lang("coaching", "part1P7Heading",
					lang));
			agilitySection.setActivity1(staticContent.getKeyMapValue4Lang("coaching", "part1P7Bullet1", lang));
			agilitySection.setActivity2(staticContent.getKeyMapValue4Lang("coaching", "part1P7Bullet2", lang));
			agilitySection.setActivity3(staticContent.getKeyMapValue4Lang("coaching", "part1P7Bullet3", lang));
			agilitySection.setActivity4(staticContent.getKeyMapValue4Lang("coaching", "part1P7Bullet4", lang));
			agilitySection.setActivity5(staticContent.getKeyMapValue4Lang("coaching", "part1P7Bullet5", lang));
			agilitySection.setActivity6(staticContent.getKeyMapValue4Lang("coaching", "part1P7Bullet6", lang));
		} else {
			agilitySection.setTitle(staticContent.getKeyMapValue4Lang("coaching", "part1HeaderLabel", lang));
			agilitySection.setHeading(feedbackContent.getKeyMapValue4Lang("feedback", "part1Heading", lang));
			agilitySection.setP1(feedbackContent.getKeyMapValue4Lang("feedback", "part1P1", lang));
			agilitySection.setP2(feedbackContent.getKeyMapValue4Lang("feedback", "part1P2", lang));
			agilitySection.setP3(feedbackContent.getKeyMapValue4Lang("feedback", "part1P3", lang));
		}
		// .agilityScaleLabelHigh / Low ?

		// EXTRACT DATA FROM RELEVANT STAGES...

		// REFERENCE VAR DATA FROM HASHMAPS ABOVE USING VAR id AS THE KEY
		confidence.clear();
		agilities.clear();
		agilityScales.clear();
		verificationScales.clear();
		for (Stage st : stages) {
			List<Var> vars = st.getVar();
			if (st.getId().equals("agilities")) {
				for (Var v : vars) {
					agilities.put(v.getId(), v.getValue());
				}
			} else if (st.getId().equals("agility-scale")) {
				for (Var v : vars) {
					agilityScales.put(v.getId(), v.getValue());
				}
			} else if (st.getId().equals("verification-scale")) {
				for (Var v : vars) {
					verificationScales.put(v.getId(), v.getValue());
				}
				break;
			} else if (st.getId().equals("cnfdc")) {
				for (Var v : vars) {
					confidence.put(v.getId(), v.getValue());
				}
			} else if (st.getId().equals("cnfdc-index")) {
				for (Var v : vars) {
					confidence.put(v.getId(), v.getValue());
				}
			}
		}
		// SCALES
		Scales aScales = new Scales();
		// Overall Scale
		Scale oScale = new Scale();
		Scale mScale = new Scale();
		Scale pScale = new Scale();
		Scale cScale = new Scale();
		Scale rScale = new Scale();
		Scale sScale = new Scale();
		// set all scale data
		oScale = setScaleData(oScale, "overall", "OLG");
		mScale = setScaleData(mScale, "mental", "MLG");
		pScale = setScaleData(pScale, "people", "PLG");
		cScale = setScaleData(cScale, "change", "CLG");
		rScale = setScaleData(rScale, "results", "RLG");
		sScale = setScaleData(sScale, "selfAware", "SLG");
		// add scales to scales[]
		aScales.getScale().add(oScale);
		aScales.getScale().add(mScale);
		aScales.getScale().add(pScale);
		aScales.getScale().add(cScale);
		aScales.getScale().add(rScale);
		aScales.getScale().add(sScale);
		// add scales[] to agility section
		agilitySection.setScales(aScales);
		// add agility section to report
		report.setAgilityScoresSection(agilitySection);
		if (!reportType.equals(ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK)) {
			// VERIFICATION SCALES SECTION
			VerificationScalesSection vss = new VerificationScalesSection();
			vss.setP1(staticContent.getKeyMapValue4Lang("coaching", "part2P1", lang));
			vss.setP1Header(staticContent.getKeyMapValue4Lang("coaching", "part2P1Heading", lang));
			vss.setP2(staticContent.getKeyMapValue4Lang("coaching", "part2VScale1Desc", lang));
			vss.setP2Header(staticContent.getKeyMapValue4Lang("coaching", "part2VScale1Head", lang));
			vss.setP3(staticContent.getKeyMapValue4Lang("coaching", "part2VScale2Desc", lang));
			vss.setP3Header(staticContent.getKeyMapValue4Lang("coaching", "part2VScale2Head", lang));
			vss.setP4(staticContent.getKeyMapValue4Lang("coaching", "part2VScale3Desc", lang));
			vss.setP4Header(staticContent.getKeyMapValue4Lang("coaching", "part2VScale3Head", lang));
			vss.setP5(staticContent.getKeyMapValue4Lang("coaching", "part2VScale4Desc", lang));
			vss.setP5Header(staticContent.getKeyMapValue4Lang("coaching", "part2VScale4Head", lang));
			vss.setP6(staticContent.getKeyMapValue4Lang("coaching", "part2VScale5Desc", lang));
			vss.setP6Header(staticContent.getKeyMapValue4Lang("coaching", "part2VScale5Head", lang));

			vss.setScoreDescLabelHigh(staticContent.getKeyMapValue4Lang("coaching", "vScaleLabelHigh", lang));
			vss.setScoreDescLabelLow(staticContent.getKeyMapValue4Lang("coaching", "vScaleLabelLow", lang));
			vss.setTitle(staticContent.getKeyMapValue4Lang("coaching", "part2Heading", lang));
			// To reorder the vScales on the report, change the case #s below.
			VScales vs = new VScales();
			vss.setVScales(vs);
			for (int i = 0; i < 5; i++) {
				VScale vScale = new VScale();
				switch (i) {
				case 0:
					vScale = generateVScale("pSP");// pSelfPresent
					break;
				case 1:
					vScale = generateVScale("pCons");// pConsistent
					break;
				case 2:
					vScale = generateVScale("pAlign");// pProfileAlign
					break;
				case 3:
					vScale = generateVScale("pPCount");// 100 - pPersonCounter
					break;
				case 4:
					vScale = generateVScale("pBioCount");// 100 - pBioCounter
					break;
				}
				vss.getVScales().getVScale().add(vScale);
			}
			vss.setWarningHeader(staticContent.getKeyMapValue4Lang("coaching", "part2RedHeading", lang));
			report.setVerificationScalesSection(vss);
		}
		if (!reportType.equals(ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK)) {
			// confidence SECTION
			OverallConfidenceSection ocs = new OverallConfidenceSection();
			ocs.setP1(staticContent.getKeyMapValue4Lang("coaching", "part3P1", lang));
			ocs.setP1Header(staticContent.getKeyMapValue4Lang("coaching", "part3P1Heading", lang));
			ocs.setP2(staticContent.getKeyMapValue4Lang("coaching", "part3P2", lang));
			ocs.setP3(staticContent.getKeyMapValue4Lang("coaching", "part3P3", lang));
			ocs.setP4(staticContent.getKeyMapValue4Lang("coaching", "part3P4", lang));
			ocs.setP5(staticContent.getKeyMapValue4Lang("coaching", "cScaleP1", lang));
			ocs.setP5Header(staticContent.getKeyMapValue4Lang("coaching", "part3SectionHeading", lang));
			ocs.setScaleLabelHigh(staticContent.getKeyMapValue4Lang("coaching", "cScaleLabelHigh", lang));
			ocs.setScaleLabelLow(staticContent.getKeyMapValue4Lang("coaching", "cScaleLabelLow", lang));
			ocs.setScore(this.confidence.get("Confidence_index"));
			ocs.setTitle(staticContent.getKeyMapValue4Lang("coaching", "part3SectionHeading", lang));
			report.setOverallConfidenceSection(ocs);
		}
		String rcm = "";
		rcm = this.marshalOutput(report);
		log.trace("RCM = {}", rcm);
		if (rcm == null) {
			throw new PalmsException("RCM was null.", PalmsErrorCode.REPORT_GEN_FAILED.getCode());
		}
		return rcm; // RCM xml string

	}

	private VScale generateVScale(String scaleId) {
		VScale vscale = new VScale();
		vscale.setScore(verificationScales.get(scaleId + "_scaleBar"));

		if (scaleId == "pSP") {
			vscale.setHighScoreDesc(staticContent.getKeyMapValue4Lang("coaching", "vScale1High", lang));
			vscale.setLowScoreDesc(staticContent.getKeyMapValue4Lang("coaching", "vScale1Low", lang));
			vscale.setTitle(staticContent.getKeyMapValue4Lang("coaching", "part2VScale1Head", lang));
		}
		if (scaleId == "pCons") {
			vscale.setHighScoreDesc(staticContent.getKeyMapValue4Lang("coaching", "vScale2High", lang));
			vscale.setLowScoreDesc(staticContent.getKeyMapValue4Lang("coaching", "vScale2Low", lang));
			vscale.setTitle(staticContent.getKeyMapValue4Lang("coaching", "part2VScale2Head", lang));
		}
		if (scaleId == "pAlign") {
			vscale.setHighScoreDesc(staticContent.getKeyMapValue4Lang("coaching", "vScale3High", lang));
			vscale.setLowScoreDesc(staticContent.getKeyMapValue4Lang("coaching", "vScale3Low", lang));
			vscale.setTitle(staticContent.getKeyMapValue4Lang("coaching", "part2VScale3Head", lang));
		}
		if (scaleId == "pPCount") {
			vscale.setHighScoreDesc(staticContent.getKeyMapValue4Lang("coaching", "vScale4High", lang));
			vscale.setLowScoreDesc(staticContent.getKeyMapValue4Lang("coaching", "vScale4Low", lang));
			vscale.setTitle(staticContent.getKeyMapValue4Lang("coaching", "part2VScale4Head", lang));
		}
		if (scaleId == "pBioCount") {
			vscale.setHighScoreDesc(staticContent.getKeyMapValue4Lang("coaching", "vScale5High", lang));
			vscale.setLowScoreDesc(staticContent.getKeyMapValue4Lang("coaching", "vScale5Low", lang));
			vscale.setTitle(staticContent.getKeyMapValue4Lang("coaching", "part2VScale5Head", lang));
		}
		return vscale;
	}

	private Scale setScaleData(Scale s, String row, String agilityPrefix) {
		// agility gridMap content (use row)
		s.setTitle(agilityContent.getGridMapValue4Lang(gridMapId, row, "headingD", lang));
		s.setHeading(agilityContent.getGridMapValue4Lang(gridMapId, row, "headingA", lang));
		s.setHeading2(agilityContent.getGridMapValue4Lang(gridMapId, row, "headingC", lang));
		s.setLabelLow(agilityContent.getGridMapValue4Lang(gridMapId, row, "traitLow", lang));
		s.setLabelHigh(agilityContent.getGridMapValue4Lang(gridMapId, row, "traitHigh", lang));
		s.setDescLow(agilityContent.getGridMapValue4Lang(gridMapId, row, "traitLowDesc", lang));
		s.setDescHigh(agilityContent.getGridMapValue4Lang(gridMapId, row, "traitHighDesc", lang));
		if (row == "overall") {
			if (confidence.get("OLGHML") != null) {
				int r = Integer.parseInt(confidence.get("OLGHML"));
				s.setScoreNarrativeHeading(agilityContent.getGridMapValue4Lang(gridMapId, row, "headingB", lang));
				if (r == -1) {
					s.setScoreNarrative(agilityContent.getGridMapValue4Lang(gridMapId, row, "lowNarrative", lang));
				} else if (r == 0) {
					s.setScoreNarrative(agilityContent.getGridMapValue4Lang(gridMapId, row, "middleNarrative", lang));
				} else if (r == 1) {
					s.setScoreNarrative(agilityContent.getGridMapValue4Lang(gridMapId, row, "highNarrative", lang));
				}
			}
		}
		// participant adjusted score

		int pAdjScr = Integer.parseInt(agilities.get(agilityPrefix + "_pAdjBar"));
		// ..ScoreSide is used for graphing results
		if (pAdjScr < 0) {
			// log.debug("LOW SIDE");
			s.setAdjpScoreSide("low");
		} else {
			// log.debug("HIGH SIDE");
			s.setAdjpScoreSide("high");
		}
		// participant score
		int pScr = Integer.parseInt(agilities.get(agilityPrefix + "_pValueBar"));

		if (pScr < 0) {
			// log.debug("LOW SIDE");
			s.setPScoreSide("low");
		} else {
			s.setPScoreSide("high");
			// log.debug("HIGH SIDE");
		}
		// set scores as absolute - ui does not handle negative scores
		s.setAdjpScore(String.valueOf(Math.abs(pAdjScr)));
		s.setPScore(String.valueOf(Math.abs(pScr)));

		s.setPadjScaleLeft(agilityScales.get(agilityPrefix + "pAdjScale_leftBlock"));

		s.setPadjScaleMid(agilityScales.get(agilityPrefix + "pAdjScale_midBlock"));

		s.setPadjScaleRight(agilityScales.get(agilityPrefix + "pAdjScale_rightBlock"));
		// scaleKeys content

		s.setPIOLowList(agilityContent.getGridMapValue4Lang("issuesAndTendenciesGrid", row + "IssuesLow", "*", lang));
		s.setBTLowList(agilityContent.getGridMapValue4Lang("issuesAndTendenciesGrid", row + "TendenciesLow", "*", lang));
		s.setPIOHighList(agilityContent.getGridMapValue4Lang("issuesAndTendenciesGrid", row + "IssuesHigh", "*", lang));
		s.setBTHighList(agilityContent.getGridMapValue4Lang("issuesAndTendenciesGrid", row + "TendenciesHigh", "*",
				lang));
		s.setChartLabel(agilityContent.getKeyMapValue4Lang("scaleKeys", "scale1Heading", lang));
		s.setPIOLabel(agilityContent.getKeyMapValue4Lang("issuesAndTendencies", "issuesHeading", lang));
		s.setBTLabel(agilityContent.getKeyMapValue4Lang("issuesAndTendencies", "tendenciesHeading", lang));
		s.setKeyBalanced(agilityContent.getKeyMapValue4Lang("scaleKeys", "scale1KeyB0", lang));
		s.setKeyPotentialOveruse(agilityContent.getKeyMapValue4Lang("scaleKeys", "scale1KeyB2", lang));
		s.setKeyStrong(agilityContent.getKeyMapValue4Lang("scaleKeys", "scale1KeyB1", lang));
		s.setPadjScaleLabel(agilityContent.getKeyMapValue4Lang("scaleKeys", "scale2Heading", lang));
		return s;
	}

	private String marshalOutput(ViaEdgeCoachingReport cReport) {
		JAXBContext jc = null;
		Marshaller m = null;
		StringWriter sw = new StringWriter();
		try {
			jc = JAXBContext.newInstance(ViaEdgeCoachingReport.class);
			m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(cReport, sw);

			return sw.toString();
		} catch (JAXBException jbe) {
			log.error("VIA EDGE CoachingReportRgrToRcm.marshalOutput() - JAXBException: Msg={}", jbe.getMessage());
			jbe.printStackTrace();
			return null;
		}
	}

	public VerificationScalesSection getvScalesSection() {
		return vScalesSection;
	}

	public OverallConfidenceSection getcScaleSection() {
		return cScaleSection;
	}

	public OverviewSection getOverviewSection() {
		return overviewSection;
	}

	public int getOverallRanking() {
		return overallRanking;
	}

	public void setOverallRanking(int overallRanking) {
		this.overallRanking = overallRanking;
	}

	public ReportContent getSharedContent() {
		return sharedContent;
	}

	public void setSharedContent(ReportContent sharedContent) {
		this.sharedContent = sharedContent;
	}
}
