package com.pdinh.project.kfap.language;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.Stateless;

import org.apache.commons.lang3.StringUtils;

@Stateless
public class ReplaceVarForEnglishGB implements LangReplacement {

	@Override
	public String replaceContentVar(String content, Map<String, String> varTextWrapper) {
		if (StringUtils.isNotEmpty(content)) {
			Pattern pattern = Pattern.compile("\\[(.*?)\\]");
			Matcher matcher = pattern.matcher(content);
			boolean vAvAnFound = false;
			// for each variable found, we replace them to British English
			while (matcher.find()) {
				String matchTxt = matcher.group().toString().replace("[", "").replace("]", "");
				Iterator<Map.Entry<String, String>> it = varTextWrapper.entrySet().iterator();
				// for each mapped variable value
				while (it.hasNext()) {
					Map.Entry<String, String> entry = it.next();
					if (entry.getKey().equals(matchTxt)) {
						if (entry.getKey().equals("vDateComplete") || entry.getKey().equals("vClient")) {
							content = content.replace(matcher.group().toString(), entry.getValue().toString());
						} else if (entry.getKey().equals("vAvAn")) {
							// log.debug("FOUND A/AN VAR, SKIP FOR NOW");
							vAvAnFound = true;
						} else {
							// entry.getValue().toString().toLowerCase();
							content = content.replace(matcher.group().toString(), entry.getValue().toString()
									.toLowerCase());
						}
					}
				}
			}
			// Logic to put in 'a' or 'an' based on next character.
			// We may need to revisit this for "h", but skip it for now.
			if (vAvAnFound == true) {
				// Do find again to find all vAvAn vars
				Matcher matcher2 = pattern.matcher(content);
				while (matcher2.find()) {
					String matchTxt = matcher2.group().toString().replace("[", "").replace("]", "");
					Iterator<Map.Entry<String, String>> it = varTextWrapper.entrySet().iterator();
					while (it.hasNext()) {
						Map.Entry<String, String> entry = it.next();
						if (entry.getKey().equals(matchTxt)) {
							String nextValue = content.substring(matcher2.end(), matcher2.end() + 2);
							nextValue = nextValue.trim();
							String[] VOWELS = new String[] { "a", "e", "i", "o", "u" };

							// log.debug("CHECK VOWEL[] against " + nextValue);
							if (!Arrays.asList(VOWELS).contains(nextValue)) {
								// log.debug("NO VOWEL FOUND, USE 'a'");
								content = content.replace(matcher2.group().toString(), "a");
							} else {
								// log.debug("VOWEL FOUND, USE 'an'");
								content = content.replace(matcher2.group().toString(), "an");
							}
						}
					}
				}
			}
		}
		return content;
	}
}
