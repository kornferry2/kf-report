/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.pdinh.core.vo.Participant;
import com.pdinh.core.vo.Project;

/*
 * This is the Head class for the LvaDBReport JAXB structure.
 * 
 * LvaDbReport models the RCM for the LVA Readiness Dashboard Report, but
 * all of the Dashboard Reports have the same structure.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"reportType", "reportSubtype", "reportTitle", "levelName",
								 "reportDate", "assessmentDate", "project", "participant",
								 "companyName", "summary", "ratings", "development", "appendix"})
@XmlRootElement(name = "lvaDbReport")
public class LvaDbReport
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String reportType;
	private String reportSubtype;
	private String reportTitle;
	private String levelName;
	private String reportDate;
	private String assessmentDate;
	private Project project;
	private Participant participant;
	private String companyName;
	//private String configText;
	private Summary summary;
	private Ratings ratings;
	private Development development;
	private Appendix appendix;
	
	//
	// Constructors.
	//

	// 0-parameter constructor
	public LvaDbReport()
	{
	}

//	// 1-parameter constructor (clone) - Not implemented at this time...
//	//    the code below is template code from another class and should not be used as is, but must be modified
//	public IbReport(IbReport orig)
//	{
//		this.reportType = orig.getReportType() == null ? null : new String(orig.getReportType());
//		this.reportTitle = orig.getReportSubtype() == null ? null : new String(orig.getReportSubtype());
//		this.reportTitle = orig.getReportTitle() == null ? null : new String(orig.getReportTitle());
//		this.level = orig.getLevel() == null ? null : new String(orig.getLevel());
//		this.reportDate = orig.getReportDate() == null ? null : new String(orig.getReportDate());
//		this.assessmentDate = orig.getAssessmentDate() == null ? null : new String(orig.getAssessmentDate());
//		this.scoringDate = orig.getScoringDate() == null ? null : new String(orig.getScoringDate());
//		this.company = orig.getCompany() == null ? null : new String(orig.getCompany());
//		this.participant = orig.getParticipant() == null ? null : new Participant(orig.getParticipant());
//		this.overallComment = orig.getOverallComment() == null ? null : new String(orig.getOverallComment());
//		if (orig.getSection() == null)
//		{
//			this.section = null;
//		}
//		else
//		{
//			this.section = new ArrayList<Section>();
//			for (Section ss : orig.getSection())
//			{
//				this.section.add(new Section(ss));
//			}
//		}
//		this.elUrl = orig.getElUrl() == null ? null : new String(orig.getElUrl());
//	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "LvaDashReport:  ";
		str += "reportType=" + this.reportType;
		str += ", reportSubtype=" + this.reportSubtype;
		str += ", reportTitle=" + this.reportTitle;
//		str += ", levelName=" + this.levelName;
//		str += ", reportDate=" + this.reportDate;
//		str += ", assessmentDate=" + this.assessmentDate;
//		if (this.project != null)
//			str += "\n" + this.project.toString() + "  ";
//		if (this.participant != null)
//			str += "\n" + this.participant.toString() + "  ";
//		str += "\nCompanyName=" + this.companyName;
//		//str += "\n  configText=" + this.configText;
//		str += "\n" + this.summary.toString();
//		str += "\n" + this.ratings.toString();
//		str += "\n" + this.development.toString();
		str += "\n" + this.appendix.toString();
		
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getReportType()
	{
		return this.reportType;
	}

	public void setReportType(String value)
	{
		this.reportType = value;
	}

	//----------------------------------
	public String getReportSubtype()
	{
		return this.reportSubtype;
	}

	public void setReportSubtype(String value)
	{
		this.reportSubtype = value;
	}

	//----------------------------------
	public String getReportTitle()
	{
		return this.reportTitle;
	}

	public void setReportTitle(String value)
	{
		this.reportTitle = value;
	}

	//----------------------------------
	public String getLevelName()
	{
		return this.levelName;
	}

	public void setLevelName(String value)
	{
		this.levelName = value;
	}

	//----------------------------------
	public String getReportDate()
	{
		return this.reportDate;
	}

	public void setReportDate(String value)
	{
		this.reportDate = value;
	}

	//----------------------------------
	public String getAssessmentDate()
	{
		return this.assessmentDate;
	}

	public void setAssessmentDate(String value)
	{
		this.assessmentDate = value;
	}

	//----------------------------------
	public Project getProject()
	{
		return this.project;
	}

	public void setProject(Project value)
	{
		this.project = value;
	}

	//----------------------------------
	public Participant getParticipant()
	{
		return this.participant;
	}

	public void setParticipant(Participant value)
	{
		this.participant = value;
	}

	//----------------------------------
	public String getCompanyName()
	{
		return this.companyName;
	}

	public void setCompanyName(String value)
	{
		this.companyName = value;
	}
//
//	//----------------------------------
//	public String getConfigText()
//	{
//		return this.configText;
//	}
//
//	public void setConfigText(String value)
//	{
//		this.configText = value;
//	}

	//----------------------------------
	public Summary getSummary()
	{
		return this.summary;
	}

	public void setSummary(Summary value)
	{
		this.summary = value;
	}

	//----------------------------------
	public Ratings getRatings()
	{
		return this.ratings;
	}

	public void setRatings(Ratings value)
	{
		this.ratings = value;
	}

	//----------------------------------
	public Development getDevelopment()
	{
		return this.development;
	}

	public void setDevelopment(Development value)
	{
		this.development = value;
	}

	//----------------------------------
	public Appendix getAppendix()
	{
		return this.appendix;
	}

	public void setAppendix(Appendix value)
	{
		this.appendix = value;
	}
}
