/**
* Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
* All rights reserved.
*/
package com.pdinh.project.lva.ut;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.pdinh.project.lva.vo.LvaDbReport;


/*
 * Test the unmarshalling of the LVA RCM
 */

public class TestLvaRcmUnmarshal
{
	/*
	 * Test the JAXB annotations (creating XML from POJOs) for the Shell RCM.
	 * 
	 * Generally used to test that we have the class annotations right.
	 * No validation is performed
	 */
	public static void main(String args[])
	{
		System.out.println("Starting LVA RCM (LvaDbReport) unmarshal test...");

		File ff = null;
		//FileInputStream fis = null;
		FileReader fr = null;
		JAXBContext jc = null;
		Unmarshaller um = null;
		LvaDbReport obj = null;

		// Get the parameter (test input file file path)
		if (args.length < 1)
		{
			System.out.println("No test file path passed.  Terminating...");
			return;
		}
		
		String testFilePath = args[0];
		System.out.println("Unmarshalling from " + testFilePath);

		// Set up for unmarshalling
		ff = new File(testFilePath);
		try
		{
			System.out.println("  file.parent=" + ff.getParent());
			System.out.println("  Abs=" + ff.getAbsolutePath());
			System.out.println("  Canonical=" + ff.getCanonicalPath());
		}
		catch (IOException e)
		{
			System.out.println("Unable to get file name.");
			e.printStackTrace();
			return;
		}
		
		//Unmarshal
		try
		{
			//fis = new FileInputStream(ff);
			fr = new FileReader(ff);

			// Create the context & the unmarshaller
			jc = JAXBContext.newInstance(LvaDbReport.class);
			um = jc.createUnmarshaller(); 
			System.out.println("Unmarshalling...");
			obj = (LvaDbReport)um.unmarshal(fr); 

			System.out.println("Unmarshal complete... output:\n" + obj.toString());
			return;
		}
		catch (FileNotFoundException e)
		{
			System.out.println("Input file not found; terminating...  File name=" + ff);
			return;
		}
		catch (JAXBException e)
		{
			// Setting up context or the unmarshaller
			System.out.println("JAXBException; terminating...  msg=" + e.getMessage());
			e.printStackTrace();
			return;
		}
		finally
		{
			// Clean up
			obj = null;
			um = null;
			jc = null;
			if (fr != null)
			{
				try { fr.close(); }
				catch (Exception e) { /* Swallow this guy */ }
			}
			ff = null;
		}
	}

}
