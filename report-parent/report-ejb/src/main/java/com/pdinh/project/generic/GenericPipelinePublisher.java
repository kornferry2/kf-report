package com.pdinh.project.generic;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.pipeline.generator.helpers.PipelineTransformErrorListener;
import com.pdinh.pipeline.generator.helpers.ReportManifestHelper;
import com.pdinh.pipeline.manifest.vo.ReportManifest;
import com.pdinh.pipeline.manifest.vo.Step;

@Stateless
@LocalBean
public class GenericPipelinePublisher {

	final static Logger logger = LoggerFactory.getLogger(GenericPipelinePublisher.class);

	@Resource(mappedName = "custom/generator_properties")
	private Properties generatorProperties;
	static private boolean debugging = false;

	static private String suffix = "/";
	static private String xmlExt = ".xml";
	static private String xslExt = ".xsl";

	static private String reposiDir;
	static private String engineDir;
	static private String publishDir;

	private String reposiPath = "CalculationRepository";
	private String enginePath = "CalculationEngineResources";
	private String publishPath = "CalculationPublisher";

	public GenericPipelinePublisher() {
	}

	/**
	 * @param String
	 *            pipeline - publish all xsl for a pipeline such as lva or
	 *            viaEdge
	 * @param String
	 *            manifest - publish only one manifest (instrument or consultant
	 *            update within a pipeline)
	 * @param Boolean
	 *            debugMode - debugMode will publish stage and var with
	 *            descriptions or non-debug will publish without
	 * 
	 */

	// 2 paramters
	/* @param pipeline such as lva, vedge or kfpi
	 * @param debug mode will enable description info for Stage and var ids
	 */
	public void publishPipeline(String pipeline, Boolean debug) {
		// always populate working directories first
		setWorkDirectories();
		if (checkManifestFolder(pipeline)) {
			try {
				publish(pipeline, null, debug);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("There is no such pipeline in Calculation Repository." + "Pipeline: " + pipeline);
		}
	}

	// 3 parameters
	/* @param pipeline such as lva, vedge or kfpi
	 * @param debug mode will enable description info for Stage and var ids
	 * @param take a manifest as user selected not default
	 */
	public void publishPipeline(String pipeline, String manifest, Boolean debug) {
		// always populate working directories first
		setWorkDirectories();
		if (checkManifestFolder(pipeline) && checkManifestFile(pipeline, manifest)) {
			try {
				publish(pipeline, manifest, debug);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("There is no such pipeline or manifest file in Calculation Repository.  pipeline:"
					+ pipeline + " manifest: " + manifest);
		}
	}

	private void publish(String pipeline, String manifest, Boolean debug) throws Exception {
		// get path for CalculationRepository and CalculationPublisher
		// publish manifests are located in a different place other than rgr
		// manifest.
		// one in CalculationPublisher and later in the
		// CalculationEngineResources
		// since rgr manifest are called at different time and place. the
		// publish manifest
		// can be call once to generate all files per pipeline.
		ReportManifestHelper rmh = new ReportManifestHelper(debugging, publishDir);
		System.out.println("Debug Mode:" + debug);
		if (manifest == null) {
			manifest = pipeline + "Manifest"; // use default manifest
		}
		ReportManifest rm = rmh.getReportManifest(pipeline, manifest + xmlExt);
		TransformerFactory tFactory = TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl", null);

		for (Step ss : rm.getCalculationSteps().getStep()) {
			String stepName = ss.getStepName();
			String stageLabel = ss.getStageLabel();
			String stageDesc = ss.getStageDesc();
			String genFile = ss.getGenFile();
			String xslFile = ss.getXsl();
			// System.out.println("Stage Name:" + stepName);
			// System.out.println("Stage Label:" + stageLabel);
			// System.out.println("Stage Desc:" + stageDesc);
			// System.out.println("genFile:" + genFile);
			// System.out.println("xsl File:" + xslFile);
			String instDir = getContentFromPath(xslFile, 2);

			String tempFileName = genFile.replaceAll(".xsl", "") + "_tmp" + xslExt;
			File tempFile = new File(publishDir + "xsl" + "/" + tempFileName);
			String gFile = null;
			// determine if the pipeline has sub-directory as instrument or post
			if (!instDir.equals(pipeline)) {
				if (debug && !tempFile.exists()) {
					createTempGenFile(publishDir + "xsl" + "/" + genFile, tempFileName);
				}
				if (debug) {
					gFile = tempFile.getName();
				} else {
					gFile = genFile;
				}
			}
			String xslPath = publishDir + "xsl" + "/" + gFile;
			String xmlPath = reposiDir + stepName + xslFile.replaceAll("xsl", "xml");
			String destinationPath = engineDir + "transforms/" + stepName + xslFile;
			// System.out.println("tempFile:"+tempFilePath);
			// System.out.println("XSL path = " + xslPath);
			System.out.println("XML path = " + xmlPath);
			transformProcess(tFactory, xmlPath, xslPath, stageLabel, stageDesc, destinationPath);
		}
		deleteTempGenFile();
	}

	private void transformProcess(TransformerFactory tFactory, String xmlPath, String xsltPath, String stageLabel,
			String stageDesc, String destinationPath) {
		Transformer tformr = null;
		StreamSource styleSheet = new StreamSource(new File(xsltPath));
		StreamSource xmlSource = new StreamSource(new File(xmlPath));
		try {
			// System.out.println("destination: " + destinationPath);
			File resultFile = new File(destinationPath);
			Result result = new StreamResult(resultFile);

			// get a transformer for this particular style sheet
			tformr = tFactory.newTransformer(styleSheet);
			tformr.setParameter("genElement", stageLabel);
			tformr.setParameter("descElement", stageDesc);
			tformr.setErrorListener(new PipelineTransformErrorListener());
			tformr.transform(xmlSource, result);

		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/* 
	 * @param a temp file will create per batch process and will deleted after all process have done within a manifest   
	 */
	private void createTempGenFile(String genFilePath, String tempFileName) {
		String searchStr1 = "<xsl:param name=\"genElement\"";
		String searchStr2 = "<stage id=\"{$genElement}";
		String searchStr3 = "<var id=\"{@id}";
		String replaceL1 = "<xsl:param name=\"genElement\" />  <xsl:param name=\"descElement\"/> ";
		String replaceL2 = "<stage desc=\"{$descElement}\" id=\"{$genElement}\">";
		String replaceL3 = "<var desc=\"{@desc}\" id=\"{@id}\">";

		BufferedReader reader = null;
		BufferedWriter writer = null;
		ArrayList<String> list = new ArrayList<String>();

		try {
			reader = new BufferedReader(new FileReader(genFilePath));
			String tmp;
			while ((tmp = reader.readLine()) != null)
				list.add(tmp);
			reader.close();

			// create a temp file
			String genFile = new File(genFilePath).getName();
			File tempFile = new File(genFilePath.replaceAll(genFile, tempFileName));
			writer = new BufferedWriter(new FileWriter(tempFile));
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).contains(searchStr1)) {
					list.remove(i);
					list.add(i, replaceL1);
				} else if (list.get(i).contains(searchStr2)) {
					list.remove(i);
					list.add(i, replaceL2);
				} else if (list.get(i).contains(searchStr3)) {
					list.remove(i);
					list.add(i, replaceL3);

				}
				writer.write(list.get(i) + "\r\n");
			}

			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
				if (writer != null) {
					writer.close();
				}
			} catch (IOException e) {
				// Log only... we no longer need the reader or writer
				logger.info("Error closing reader or writer in GenericPipelinePublisher.createTempGenFile().  Msg="
						+ e.getMessage());
			}
		}
	}

	private void deleteTempGenFile() {
		try {
			File dir = new File(publishDir + "xsl" + "/");
			File[] files = dir.listFiles();
			String filterFile = null;
			for (int i = 0; i < files.length; i++) {
				filterFile = files[i].getName();
				if (filterFile.endsWith("_tmp.xsl"))
					files[i].delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setWorkDirectories() {
		engineDir = generatorProperties.getProperty("calculationEngineResources");
		if (engineDir != null) {
			String baseDir = engineDir.replaceAll(enginePath + suffix, "");
			setReposiDir(baseDir + reposiPath + suffix);
			setPublishDir(baseDir + publishPath + suffix);
		} else {
			logger.info("No JNDI property is found for CalculationEngineResources:" + engineDir);
		}
	}

	private boolean checkManifestFolder(String pipeline) {
		String mfDirectory = publishDir + "manifest";
		File workingDirectory = new File(mfDirectory); // current directory
		File[] files = workingDirectory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				if (file.getName().equals(pipeline))
					return true;
			}
		}
		return false;
	}

	private boolean checkManifestFile(String pipeline, String manifestFile) {
		String mfDirectory = publishDir + "manifest" + "/" + pipeline;
		File workingDirectory = new File(mfDirectory); // current directory
		File[] files = workingDirectory.listFiles();
		for (File file : files) {
			if (!file.isDirectory() && file.getName().equals(manifestFile + xmlExt))
				return true;
		}
		return false;
	}

	private String getContentFromPath(String value, int section) {
		String instDir[] = value.split("/");
		return instDir[section];
	}

	public static String getReposiDir() {
		return reposiDir;
	}

	public static void setReposiDir(String reposiDir) {
		GenericPipelinePublisher.reposiDir = reposiDir;
	}

	public static String getEngineDir() {
		return engineDir;
	}

	public static void setEngineDir(String engineDir) {
		GenericPipelinePublisher.engineDir = engineDir;
	}

	public static String getPublishDir() {
		return publishDir;
	}

	public static void setPublishDir(String publishDir) {
		GenericPipelinePublisher.publishDir = publishDir;
	}
}
