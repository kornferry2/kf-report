/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/*
* TextSection object used for the recommendation section of LvaDbReport only.
*/
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder={"title", "point"})
public class TextSection
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String title;
  //	private String text;	// Plain text message
	@XmlElementWrapper(name = "points")
	private List<String> point;	// Bulleted texts


	//
	// Constructors.
	//

	// 0-parameter constructor
	public TextSection()
	{
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "TextSection:  ";
		str += "title=" + this.title +  "\n";
//		str += "              text=" + this.text + "\n";
		String xx = "";
		for (String st : this.getPoint())
		{
			if (xx.length() > 0)
				xx += "\n              ";
			xx += st;
		}
		if (xx.length() < 1)
			xx = "<--No points-->";
		str += "              " + xx + "  ";
		
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getTitle()
	{
		return this.title;
	}

	public void setTitle(String value)
	{
		this.title = value;
	}
//
//	//----------------------------------
//	public String getText()
//	{
//		return this.text;
//	}
//
//	public void setText(String value)
//	{
//		this.text = value;
//	}

	//----------------------------------
	// Note that the list has a "get" only
	public List<String> getPoint()
	{
		if (this.point == null)
			this.point = new ArrayList<String>();
		
		return this.point;
	}

}
