/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva;

import java.io.StringWriter;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.Project;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.project.lva.mll.jaxb.About;
import com.pdinh.project.lva.mll.jaxb.Appendix;
import com.pdinh.project.lva.mll.jaxb.Characteristic;
import com.pdinh.project.lva.mll.jaxb.Characteristics;
import com.pdinh.project.lva.mll.jaxb.Competencies;
import com.pdinh.project.lva.mll.jaxb.Competency;
import com.pdinh.project.lva.mll.jaxb.Derailers;
import com.pdinh.project.lva.mll.jaxb.DevSection;
import com.pdinh.project.lva.mll.jaxb.DevSections;
import com.pdinh.project.lva.mll.jaxb.Development;
import com.pdinh.project.lva.mll.jaxb.Entries;
import com.pdinh.project.lva.mll.jaxb.Entry;
import com.pdinh.project.lva.mll.jaxb.Fit;
import com.pdinh.project.lva.mll.jaxb.HtmlSection;
import com.pdinh.project.lva.mll.jaxb.HtmlSections;
import com.pdinh.project.lva.mll.jaxb.IaLinks;
import com.pdinh.project.lva.mll.jaxb.Item;
import com.pdinh.project.lva.mll.jaxb.Items;
import com.pdinh.project.lva.mll.jaxb.Link;
import com.pdinh.project.lva.mll.jaxb.LvaDbReport;
import com.pdinh.project.lva.mll.jaxb.Needs;
import com.pdinh.project.lva.mll.jaxb.Notes;
import com.pdinh.project.lva.mll.jaxb.ObjectFactory;
import com.pdinh.project.lva.mll.jaxb.Participant;
import com.pdinh.project.lva.mll.jaxb.Potential;
import com.pdinh.project.lva.mll.jaxb.RatingLegend;
import com.pdinh.project.lva.mll.jaxb.Ratings;
import com.pdinh.project.lva.mll.jaxb.Readiness;
import com.pdinh.project.lva.mll.jaxb.ReportSubtype;
import com.pdinh.project.lva.mll.jaxb.ReportType;
import com.pdinh.project.lva.mll.jaxb.Score;
import com.pdinh.project.lva.mll.jaxb.Section;
import com.pdinh.project.lva.mll.jaxb.Strengths;
import com.pdinh.project.lva.mll.jaxb.Suggestions;
import com.pdinh.project.lva.mll.jaxb.Summary;
import com.pdinh.project.lva.mll.jaxb.Superfactor;
import com.pdinh.project.lva.mll.jaxb.Superfactors;
import com.pdinh.project.lva.mll.jaxb.Text;
import com.pdinh.project.lva.mll.jaxb.TextSection;
import com.pdinh.project.lva.mll.jaxb.TextSections;
import com.pdinh.reportcontent.Manifest;
import com.pdinh.reportcontent.ReportContent;
import com.pdinh.reportgenerationresponse.ReportGenerationResponse;

/*
 * LvaMllRgrToRcm - A POJO that contains all of the logic to create an RCM for the LVA reports.
 * 
 * The purpose of this class is to convert a Report Generation Result (RGR) XML to a
 * Report Content Model (RCM) for the LVA (now GPS) report model.
 * 
 * It will use the sourceFile which is the RGR, and based on the business rules, it will select the appropriate
 *  content to include into the RCM.
 * It will use the ReportContent and ReportGenerationResponse classes to pull in content.
 * 
 * The business rules and content all currently live in the latest reporting spreadsheet provided by Stacy Davies.
 * 
 * Future: An XML file to make this declarative
 * 
 * Complete end-to-end framework that is declarative:
 * Calculation Manifest and Pipeline
 * ReportGenerationResponse
 * ReportContent
 * RGR2RCM - doesn't yet exist - currently Java
 * ReportContentModel
 * ReportLayout - doesn't yet exist - currently xhtml for wkhtmltopdf
 * 
 */

// TODO Refactor the code so that it is development/readiness aware.  Currently, both content XMLs have to have the same tags
//		in both dev and redi content because the code is not dev/redi aware (example id Development Focus statements... not
//		used in the Readiness Report).
public class LvaMllRgrToRcm {
	private static final Logger log = LoggerFactory.getLogger(LvaMllRgrToRcm.class);
	//
	// Static data.
	//
	// TODO Refactor the position definitions as needed to expose them for
	// others use
	// Where are general purpose constants supposed to be stored?
	// Position in output array
	private static int DEV_RCM = 0; // Development RCM is first
	private static int REDI_RCM = 1; // Readiness RCM is second
	private static int FIT_RCM = 2; // FIT RCM is third

	// Configuration IDs
	private static String CONFIG_ASYNC = "asyncOnly";
	private static String CONFIG_ASYNC_BOSS = "asyncAndBoss";
	private static String CONFIG_ASYNC_DRM = "asyncAndDrm";
	private static String CONFIG_ASYNC_PEER = "asyncAndPeer";
	private static String CONFIG_ASYNC_BOSS_DRM = "asyncAndBossAndDrm";
	private static String CONFIG_ASYNC_BOSS_PEER = "asyncAndBossAndPeer";
	private static String CONFIG_ASYNC_DRM_PEER = "asyncAndDrmAndPeer";
	private static String CONFIG_ASYNC_BOSS_DRM_PEER = "asyncAndBossAndDrmAndPeer";

	// Fitness labels
	private static String STRONG_REC = "Strongly Recommended";
	private static String REC = "Recommended";
	private static String REC_RES = "Recommended with Reservations";
	private static String NOT_REC = "Not Recommended";

	// Readiness labels
	private static String REDI_NOW = "Ready Now";
	private static String REDI_1_2 = "Ready in 1-2 Years";
	private static String REDI_3_5 = "Ready in 3-5 Years";
	private static String REDI_DEV_IN_PLACE = "Develop in Place";

	// Advancement Potential labels
	private static String VERY_STRONG = "Very Strong";
	private static String STRONG = "Strong";
	private static String MIXED = "Mixed";
	private static String WEAK = "Weak";

	// Derailer labels
	private static String MINIMAL = "Minimal";
	private static String LOW = "Low";
	private static String MODERATE = "Moderate";
	private static String HIGH = "High";

	// Flags for trait
	private static int TOO_LOW = -1;
	private static int TOO_HIGH = 1;
	private static int OPTIMAL = 0;

	// Percentile Label
	private static String PercentileText = "Leadership Competency Index (percentile): ";

	//
	// Static classes.
	//

	//
	// Instance data.
	//
	private Manifest manifestRef;
	// Input and content data
	private ReportGenerationResponse reportGenerationResponse = null;
	private ReportContent reportContent = null;
	private ReportContent behaviorContent = null;
	private ReportContent traitContent = null;
	private ReportContent modelContent = null;
	private ReportContent aboutContent = null;
	private ReportContent appendixContent = null;
	private ReportContent graphTypeLookup = null;

	private LvaDbReport lvaDbReport;

	// *** Map data from Report Generation Response ***
	private float potential;
	private float derailers;
	private float interest;
	private float foundations;
	private float competencies;
	private float experience;
	private float readiness;
	private String percentile;
	private float satisfaction;
	private float performance;
	private float fit;
	private float satisfactionUpdate;
	private float performanceUpdate;
	private float fitUpdate;

	// Competencies
	// Doing the mapping in this file... Ultimately we could get this from
	// ModelDao or could even use mapping done for integration.
	// This was path to least resistance for now
	private String reachStrategicDecisions;
	private float b49;
	private float b29;
	private float b71;
	private float b30;
	private float b31;
	private float b32;

	private String engageDiversePerspectives;
	private float b46;
	private float b76;
	private float b47;
	private float b48;

	private String fosterInnovation;
	private float b33;
	private float b34;
	private float b69;
	private float b35;

	private String driveExecution;
	private float b50;
	private float b70;
	private float b51;
	private float b52;
	private float b53;
	private float b54;

	private String leadCourageously;
	private float b72;
	private float b59;
	private float b75;
	private float b36;

	private String promoteCollaboration;
	private float b37;
	private float b60;
	private float b38;
	private float b61;
	private float b39;
	private float b40;

	private String engageAndDevelopOthers;
	private float b62;
	private float b41;
	private float b63;
	private float b73;
	private float b74;
	private float b64;

	private String buildSupportAndInfluence;
	private float b65;
	private float b66;
	private float b67;
	private float b42;

	private String demonstratesAgility;
	private float b43;
	private float b68;
	private float b44;
	private float b45;

	private String promoteStakeholderFocus;
	private float b55;
	private float b56;
	private float b57;
	private float b58;

	// PETSMART model specific competency
	private String thinksCritically;

	// TLT
	// private float leadershipInterest;
	// private float leadershipExperience;
	// private float leadershipFoundations;
	// private float derailmentRisk;

	public enum TLTCharacteristic {
		// Initially Marked "private" because coder was being conservative;
		// taken
		// public because I needed it (pbutler). If this is a good
		// way to centralize this data, might want to open it up and/or move
		// it to a utilities/constants package.

		EGO_CENTERED("ego_centered", 1), MANIPULATION("manipulation", 2), MICRO_MANAGING("micro_managing", 3), PASSIVE_AGGRESSIVE(
				"passive_agressive", 4), LEADERSHIP_ASPIRATION("leadership_aspiration", 5), CAREER_DRIVERS(
				"career_drivers", 6), LEARNING_ORIENTATION("learning_orientation", 7), EXPERIENCE_ORIENTATION(
				"experience_orientation", 8), PROBLEM_SOLVING("problem_solving", 9), INTELLECTUAL_ENGAGEMENT(
				"intellectual_engagement", 10), ATTENTION_TO_DETAIL("attention_to_detail", 11), IMPACT_INFLUENCE(
				"impact_influence", 12), INTERPERSONAL_ENGAGEMENT("interpersonal_engagement", 13), ACHIEVEMENT_DRIVE(
				"achievement_drive", 14), ADVANCEMENT_DRIVE("advancement_drive", 15), COLLECTIVE_ORIENTATION(
				"collective_orientation", 16), FLEXIBILITY_ADAPTABILITY("flexibility_adaptability", 17), BUSINESS_OPERATIONS(
				"business_operations", 18), HANDLING_TOUGH_CHALLENGES("handling_tough_challenges", 19), HIGH_VISIBILITY(
				"high_visibility", 20), GROWING_THE_BUSINESS("growing_the_business", 21), PERSONAL_DEVELOPMENT(
				"personal_development", 22);

		private final String rgrName;
		private final int rgrId;

		TLTCharacteristic(String rgrName, int rgrId) {
			this.rgrName = rgrName;
			this.rgrId = rgrId;
		}

		public String rgrName() {
			return rgrName;
		}

		public int rgrId() {
			return rgrId;
		}
	}

	private final Map<TLTCharacteristic, Integer> characteristicScores = new HashMap<TLTCharacteristic, Integer>();
	private final Map<TLTCharacteristic, String> characteristicTypes = new HashMap<TLTCharacteristic, String>();

	private final List<Item> arrOverallStrengths = new ArrayList<Item>();
	private final List<Item> arrOverallNeeds = new ArrayList<Item>();
	private final List<Item> arrOverallDerailers = new ArrayList<Item>();

	private final List<Item> arrCompetenciesStrengths = new ArrayList<Item>();
	private final List<Item> arrCompetenciesNeeds = new ArrayList<Item>();

	private final List<Item> arrExperienceStrengths = new ArrayList<Item>();
	private final List<Item> arrExperienceNeeds = new ArrayList<Item>();

	private final List<Item> arrFoundationsStrengths = new ArrayList<Item>();
	private final List<Item> arrFoundationsNeeds = new ArrayList<Item>();

	private final List<Item> arrInterestStrengths = new ArrayList<Item>();
	private final List<Item> arrInterestNeeds = new ArrayList<Item>();

	private final List<Item> arrDerailersStanine9 = new ArrayList<Item>();
	private final List<Item> arrDerailersStanine7Or8 = new ArrayList<Item>();

	// Internally used info
	private String configId = null; // The configuration id. Used to fetch
									// report text and norms

	private final String reportContentRepository;

	// Holders for data brought in
	private final Properties lvaProperties;

	private final Project project;
	private final User user;
	private final List<Position> positions;
	private final String voucherId;

	private final int pptExtId;
	private final int projExtId;
	private String pptIntId;
	private String projIntId;

	// private final Properties configProperties;

	//
	// Constructor
	//
	public LvaMllRgrToRcm(Properties lvaProperties, Properties configProperties, ProjectUser pu,
			List<Position> posList, String vId) {
		// Stow the properties...
		this.lvaProperties = lvaProperties;
		this.reportContentRepository = configProperties.getProperty("reportContentRepository");

		// Now get the rest of the parameter data into local variables
		this.project = pu.getProject();
		this.user = pu.getUser();
		this.positions = posList;
		this.voucherId = vId;

		// And set up some widely used data
		this.pptExtId = this.user.getUsersId();
		this.projExtId = this.project.getProjectId();
	}

	//
	// Instance classes.
	//

	// JJB: When generating an RCM need to know the specific client/project
	// generating for so can get the proper content files for overridden data
	// This information should be available to us in the incoming RGR

	// This assumes a single participant in the Incoming RGR
	public String generateLvaRcmXmlXX(ReportGenerationResponse resp, int type, Manifest manifest) {
		log.debug("IN generateLvaRcmXml()");
		manifestRef = manifest;

		int rptType = type;

		this.reportGenerationResponse = resp;

		// Set up content sources
		this.modelContent = new ReportContent(reportContentRepository + "/content/" + manifest.getModelContentRef()
				+ ".xml");

		String rptReportContentPath = "";
		if (type == DEV_RCM) {
			rptReportContentPath = reportContentRepository + "/content/"
					+ manifest.getReportContents().get("development") + ".xml";
		} else if (type == REDI_RCM) {
			rptReportContentPath = reportContentRepository + "/content/"
					+ manifest.getReportContents().get("readiness") + ".xml";
		} else if (type == FIT_RCM) {
			rptReportContentPath = reportContentRepository + "/content/" + manifest.getReportContents().get("fit")
					+ ".xml";
		}

		this.reportContent = new ReportContent(rptReportContentPath);
		this.behaviorContent = new ReportContent(reportContentRepository + "/content/"
				+ manifest.getBehaviorContentRef() + ".xml");
		this.traitContent = new ReportContent(reportContentRepository + "/content/" + manifest.getTraitContentRef()
				+ ".xml");
		this.aboutContent = new ReportContent(reportContentRepository + "/content/" + manifest.getAboutContentRef()
				+ ".xml");
		this.appendixContent = new ReportContent(reportContentRepository + "/content/"
				+ manifest.getAppendixContentRef() + ".xml");
		this.graphTypeLookup = new ReportContent(reportContentRepository + "/content/"
				+ manifest.getGraphTypeLookupRef() + ".xml");

		log.debug("loaded content");

		// Get all of the required data from the response and pull it into local
		// variables
		initValuesFromRgr();
		log.debug("called initValuesFromRgr()");

		// Get internal ppt and proj IDs for use later External Ids should
		// already be available
		this.pptIntId = this.reportGenerationResponse.getNodeAttribute("participant", "id");
		this.projIntId = this.reportGenerationResponse.getNodeAttribute("project", "id");
		// String projExtId =
		// this.reportGenerationResponse.getNodeAttribute("project", "extId");

		// *** Create Report Content Model ***
		ObjectFactory of = new ObjectFactory();
		lvaDbReport = of.createLvaDbReport();
		log.debug("called createLvaDbReport()");

		// Report Type
		ReportType reportType = new ReportType();
		lvaDbReport.setReportType(reportType);
		reportType.setType("lva");
		reportType.setContent("LVA Dashboard");

		// Level Name
		lvaDbReport.setLevelName("Mid-Level Leader");

		// Report Date
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		lvaDbReport.setReportDate(dateFormat.format(date));

		// Assessment Date
		Date asmtDate;
		// Business defined Assessment Date in the extract enhancements:
		// "date for each person (last possible finalization date)"
		// If no data, default to today
		if (this.positions == null) {
			asmtDate = date;
			log.debug("No valid Position rows found for ppt={}, proj={}, ... defaulting to todays date.", pptExtId,
					projExtId);
		} else {
			// Look for the latest date
			int comp = 0;
			Timestamp ts = new Timestamp(0);
			for (Position pos : positions) {
				if (pos.getCompletionStatus() == Position.COMPLETE) {
					comp++;
					if (pos.getCompletionStatusDate() != null && pos.getCompletionStatusDate().after(ts)) {
						ts = pos.getCompletionStatusDate();
					}
				}
			}
			if (comp < 1) {
				asmtDate = date;
				log.debug("No completed Position rows found for ppt={}, proj={} ... defaulting to todays date.",
						pptExtId, projExtId);
			} else {
				asmtDate = ts;
			}
		}
		lvaDbReport.setAssessmentDate(dateFormat.format(asmtDate));

		addProject();
		addParticipant();

		getConfigId();
		// System.out.println("configId=" + this.configId);

		// Build out the major sections...
		// About
		addAboutSection();

		// Summary
		addSummarySection(rptType);

		// Add Ratings Section
		addRatingsSection();

		// Add Appendix
		addAppendix();

		// We now have an RCM with no type associated.
		// Per the design conversation, add the proper type and marshall each,
		// both with all info
		// First the Development RCM...
		if (type == DEV_RCM) {
			ReportSubtype devSubtype = new ReportSubtype();
			lvaDbReport.setReportSubtype(devSubtype);
			devSubtype.setType("development");
			devSubtype.setContent("Development Action Report");
		} else if (type == REDI_RCM) {
			ReportSubtype rediSubtype = new ReportSubtype();
			lvaDbReport.setReportSubtype(rediSubtype);
			rediSubtype.setType("readiness");
			rediSubtype.setContent("Readiness Action Report");
		} else if (type == FIT_RCM) {
			ReportSubtype fitSubtype = new ReportSubtype();
			lvaDbReport.setReportSubtype(fitSubtype);
			fitSubtype.setType("fit");
			fitSubtype.setContent("Fit Report");
		}

		String rcm = marshalOutput(lvaDbReport);
		log.debug(rcm);
		return rcm;
	}

	/**
	 * initValuesFromRgr - Grab a bunch of input data to local private variables
	 */
	private void initValuesFromRgr() {
		log.debug("IN initValuesFromRgr()");
		// *** Map data from Report Generation Response ***
		this.potential = Float.parseFloat(this.reportGenerationResponse.getVarValue("PTB33"));
		this.derailers = Float.parseFloat(this.reportGenerationResponse.getVarValue("tltc", "DRB38"));
		this.interest = Float.parseFloat(this.reportGenerationResponse.getVarValue("CIB46"));
		this.foundations = Float.parseFloat(this.reportGenerationResponse.getVarValue("FDB54"));
		this.competencies = Float.parseFloat(this.reportGenerationResponse.getVarValue("DLCILB"));
		this.experience = Float.parseFloat(this.reportGenerationResponse.getVarValue("EXB64"));
		this.readiness = Float.parseFloat(this.reportGenerationResponse.getVarValue("RNB75"));
		this.percentile = this.reportGenerationResponse.getVarValue("DLCIPC");

		// UPDATE -- map to values after they become available
		// this.satisfaction =
		// Float.parseFloat(this.reportGenerationResponse.getVarValue("SATISFACTION"));
		// this.performance =
		// Float.parseFloat(this.reportGenerationResponse.getVarValue("PERFORMANCE"));
		// this.fit =
		// Float.parseFloat(this.reportGenerationResponse.getVarValue("OVERALL_FIT"));

		String strsatisfaction = this.reportGenerationResponse.getVarValue("SATISFACTION");
		String strperformance = this.reportGenerationResponse.getVarValue("PERFORMANCE");
		String strfit = this.reportGenerationResponse.getVarValue("OVERALL_FIT");

		if (strsatisfaction.length() > 0) {
			this.satisfaction = Float.parseFloat(strsatisfaction);
		} else
			this.satisfaction = 0;

		if (strperformance.length() > 0) {
			this.performance = Float.parseFloat(strperformance);
		} else
			this.performance = 0;

		if (strfit.length() > 0) {
			this.fit = Float.parseFloat(strfit);
		} else
			this.fit = 0;

		// Values that will only be there if consultant updates them
		String strsatisfactionUpdate = this.reportGenerationResponse.getVarValue("SATISFACTION_UPDATE");
		String strperformanceUpdate = this.reportGenerationResponse.getVarValue("PERFORMANCE_UPDATE");
		String strfitUpdate = this.reportGenerationResponse.getVarValue("OVERALL_FIT_UPDATE");

		if (strsatisfactionUpdate.length() > 0) {
			this.satisfactionUpdate = Float.parseFloat(strsatisfactionUpdate);
		} else
			this.satisfactionUpdate = 0;

		if (strperformanceUpdate.length() > 0) {
			this.performanceUpdate = Float.parseFloat(strperformanceUpdate);
		} else
			this.performanceUpdate = 0;

		if (strfitUpdate.length() > 0) {
			this.fitUpdate = Float.parseFloat(strfitUpdate);
		} else
			this.fitUpdate = 0;

		// Competencies - not yet positive in correct order
		this.reachStrategicDecisions = this.roundToFiveTenths(Float.parseFloat(this.reportGenerationResponse
				.getVarValue("C5")));
		this.b49 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB49"));
		this.b29 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB29"));
		this.b71 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB71"));
		this.b30 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB30"));
		this.b31 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB31"));
		this.b32 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB32"));
		this.engageDiversePerspectives = this.roundToFiveTenths(Float.parseFloat(this.reportGenerationResponse
				.getVarValue("C12")));
		this.b46 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB46"));
		this.b76 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB76"));
		this.b47 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB47"));
		this.b48 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB48"));
		this.fosterInnovation = this
				.roundToFiveTenths(Float.parseFloat(this.reportGenerationResponse.getVarValue("C6")));
		this.b33 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB33"));
		this.b34 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB34"));
		this.b69 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB69"));
		this.b35 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB35"));
		this.driveExecution = this
				.roundToFiveTenths(Float.parseFloat(this.reportGenerationResponse.getVarValue("C13")));
		this.b50 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB50"));
		this.b70 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB70"));
		this.b51 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB51"));
		this.b52 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB52"));
		this.b53 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB53"));
		this.b54 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB54"));
		this.leadCourageously = this
				.roundToFiveTenths(Float.parseFloat(this.reportGenerationResponse.getVarValue("C7")));
		this.b72 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB72"));
		this.b59 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB59"));
		this.b75 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB75"));
		this.b36 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB36"));
		this.promoteCollaboration = this.roundToFiveTenths(Float.parseFloat(this.reportGenerationResponse
				.getVarValue("C8")));
		this.b37 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB37"));
		this.b60 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB60"));
		this.b38 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB38"));
		this.b61 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB61"));
		this.b39 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB39"));
		this.b40 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB40"));
		this.engageAndDevelopOthers = this.roundToFiveTenths(Float.parseFloat(this.reportGenerationResponse
				.getVarValue("C9")));
		this.b62 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB62"));
		this.b41 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB41"));
		this.b63 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB63"));
		this.b73 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB73"));
		this.b74 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB74"));
		this.b64 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB64"));
		this.buildSupportAndInfluence = this.roundToFiveTenths(Float.parseFloat(this.reportGenerationResponse
				.getVarValue("C10")));
		this.b65 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB65"));
		this.b66 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB66"));
		this.b67 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB67"));
		this.b42 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB42"));
		this.demonstratesAgility = this.roundToFiveTenths(Float.parseFloat(this.reportGenerationResponse
				.getVarValue("C11")));
		this.b43 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB43"));
		this.b68 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB68"));
		this.b44 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB44"));
		this.b45 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB45"));
		this.promoteStakeholderFocus = this.roundToFiveTenths(Float.parseFloat(this.reportGenerationResponse
				.getVarValue("C14")));
		this.b55 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB55"));
		this.b56 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB56"));
		this.b57 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB57"));
		this.b58 = Float.parseFloat(this.reportGenerationResponse.getVarValue("EMB58"));
		// PETSMART model specific competency petsmart-precomp stage
		// <var id="PetSmart_C5" value="2.6626984126984126"/>
		// <var id="PetSmart_C13" value="3.0999999999999996"/>
		// <var id="PetSmart_C7" value="2.830357142857143"/>
		// <var id="PetSmart_C8" value="2.318121693121693"/>
		// <var id="PetSmart_C9" value="2.05462962962963"/>
		// <var id="PetSmart_C10" value="2.948164682539683"/>
		// <var id="PetSmart_C11" value="2.7043981481481474"/>
		// <var id="PetSmart_C6C14" value="3.175198412698413"/>
		// <var id="PetSmart_C12" value="3.5"/>
		if (this.manifestRef.getModelContentRef().contains("petsmart")) {
			this.thinksCritically = this.roundToFiveTenths(Float.parseFloat(this.reportGenerationResponse
					.getVarValue("PetSmart_C6C14")));
		}
		// // TLT
		// this.leadershipInterest =
		// Float.parseFloat(this.reportGenerationResponse.getVarValue("leadership_interest"));
		// this.leadershipExperience =
		// Float.parseFloat(this.reportGenerationResponse.getVarValue("leadership_experience"));
		// this.leadershipFoundations =
		// Float.parseFloat(this.reportGenerationResponse.getVarValue("leadership_foundations"));
		// this.derailmentRisk =
		// Float.parseFloat(this.reportGenerationResponse.getVarValue("derailment_risk"));

		initCharacteristicScoresAndTypes();
	}

	private void initCharacteristicScoresAndTypes() {
		for (TLTCharacteristic characteristic : TLTCharacteristic.values()) {
			String scoreValue = this.reportGenerationResponse.getVarValue(characteristic.rgrName());
			Integer score = (scoreValue == null || scoreValue.equals("undefined")) ? -1 : new Integer(
					this.reportGenerationResponse.getVarValue(characteristic.rgrName()));
			String type = this.graphTypeLookup.getKeyMapValue("characteristicLookup", characteristic.rgrName() + "");
			characteristicScores.put(characteristic, score);
			characteristicTypes.put(characteristic, type);
		}
	}

	/**
	 * addProject - Set up the Project object in the RCM object stack. Assumes
	 * that the target stack is an instance variable.
	 */
	private void addProject() {
		// private void addProject(String projId, String projExtId) {
		com.pdinh.project.lva.mll.jaxb.Project jaxbProject = new com.pdinh.project.lva.mll.jaxb.Project();
		this.lvaDbReport.setProject(jaxbProject);
		jaxbProject.setId(new BigInteger(this.projIntId));
		jaxbProject.setExtId(new BigInteger("" + this.projExtId));

		jaxbProject.setContent(this.project.getName());

		return;
	}

	/**
	 * addParticipant - Set up the Participant object in the RCM object stack.
	 * Assumes that the target stack is an instance variable.
	 * 
	 * @param pptId
	 *            - The scorecard (internal) participant ID
	 * @param pptExtId
	 *            - The PALMS (external) participant ID
	 */
	// private void addParticipant(String pptId, String pptExtId) {
	private void addParticipant() {
		Participant jaxbPpt = new Participant();
		this.lvaDbReport.setParticipant(jaxbPpt);
		jaxbPpt.setId(new BigInteger(this.pptIntId));
		jaxbPpt.setExtId(new BigInteger("" + this.pptExtId));

		jaxbPpt.setFirstName(this.user.getFirstname());
		jaxbPpt.setLastName(this.user.getLastname());
		jaxbPpt.setTitle(this.user.getTitle());

		// While we have the User object... add the company name
		this.lvaDbReport.setCompanyName(this.user.getCompany() == null ? "Not Available" : this.user.getCompany()
				.getCoName());

		return;
	}

	/**
	 * getConfigId - Get the key that indicates what instruments are being used
	 * and save it in the object stack
	 */
	private void getConfigId() {
		// Figure out what instruments are in the project. This could be done by
		// getting the Scorecard objects, but we don't want references here to
		// the ScorecardServer project so the design calls for going to PALMS
		// and getting the information.
		boolean gotAsync = false;
		int key = 0;
		for (ProjectCourse pc : this.project.getProjectCourses()) {
			if (pc.getSelected()) {
				// Only include the selected ones
				String iabbr = pc.getCourse().getAbbv();
				if (iabbr.equals("iblva")) {
					gotAsync = true;
				} else if (iabbr.equals("lvabm")) {
					key += 1;
				} else if (iabbr.equals("lvadr")) {
					key += 2;
				} else if (iabbr.equals("lvapm")) {
					key += 4;
				}
			}
		}
		// Got the flags set... do checking
		if (!gotAsync) {
			this.configId = "noAsync"; // bogus key
			log.debug("Project {} has no async instrument", this.projExtId);
			return;
		}
		switch (key) {
		case 0:
			this.configId = CONFIG_ASYNC;
			break;
		case 1:
			this.configId = CONFIG_ASYNC_BOSS;
			break;
		case 2:
			this.configId = CONFIG_ASYNC_DRM;
			break;
		case 3:
			this.configId = CONFIG_ASYNC_BOSS_DRM;
			break;
		case 4:
			this.configId = CONFIG_ASYNC_PEER;
			break;
		case 5:
			this.configId = CONFIG_ASYNC_BOSS_PEER;
			break;
		case 6:
			this.configId = CONFIG_ASYNC_DRM_PEER;
			break;
		case 7:
			this.configId = CONFIG_ASYNC_BOSS_DRM_PEER;
			break;
		default:
			this.configId = "invalidKey"; // bogus key
			log.debug("Project {} has an invalid key value of {}", this.projExtId, key);
			break;
		}
		this.lvaDbReport.setLvaSolution(this.configId);

		return;
	}

	/**
	 * addAboutSection - Adds the "about" tag and its constituents to the RCM
	 */
	private void addAboutSection() {
		About about = new About();
		lvaDbReport.setAbout(about);

		about.setText(createText(aboutContent.getKeyMapValue("aboutContent", "aboutThisReport")));

		TextSections aboutTextSections = new TextSections();
		aboutTextSections.getTextSection().add(
				createTextSection("Leadership Competencies",
						aboutContent.getKeyMapValue("aboutContent", "leadershipCompetencies")));
		aboutTextSections.getTextSection().add(
				createTextSection("Leadership Experience",
						aboutContent.getKeyMapValue("aboutContent", "leadershipExperience")));
		aboutTextSections.getTextSection().add(
				createTextSection("Leadership Foundations",
						aboutContent.getKeyMapValue("aboutContent", "leadershipFoundations")));
		aboutTextSections.getTextSection().add(
				createTextSectionTwoItems("Leadership Interest",
						aboutContent.getKeyMapValue("aboutContent", "leadershipInterest1"),
						aboutContent.getKeyMapValue("aboutContent", "leadershipInterest2")));
		aboutTextSections.getTextSection().add(
				createTextSection("Leadership Derailers",
						aboutContent.getKeyMapValue("aboutContent", "leadershipDerailers")));
		about.setTextSections(aboutTextSections);
		RatingLegend ratingLegend = new RatingLegend();
		ratingLegend.setTitle("Detailed Ratings");
		ratingLegend.getTextOrEntries().add(createText(aboutContent.getKeyMapValue("ratingsLegend", "scales")));
		Entries ratingLegendEntries = new Entries();
		ratingLegendEntries.getEntry().add(
				createEntry("blue", aboutContent.getKeyMapValue("ratingsLegend", "clearStrength")));
		ratingLegendEntries.getEntry().add(
				createEntry("green", aboutContent.getKeyMapValue("ratingsLegend", "strength")));
		ratingLegendEntries.getEntry().add(
				createEntry("yellow", aboutContent.getKeyMapValue("ratingsLegend", "slightGap")));
		ratingLegendEntries.getEntry().add(
				createEntry("orange", aboutContent.getKeyMapValue("ratingsLegend", "substantialGap")));
		ratingLegend.getTextOrEntries().add(ratingLegendEntries);
		ratingLegend.getTextOrEntries().add(createText(aboutContent.getKeyMapValue("ratingsLegend", "explanation")));

		// TODO This is not used in the readiness report - figure out how to
		// redo it (Add <development> tag to the RatingLegend?)
		ratingLegend.getTextOrEntries().add(createText(aboutContent.getKeyMapValue("ratingsLegend", "following")));
		about.setRatingLegend(ratingLegend);
	}

	/**
	 * addSummarySection - - Adds the "summary" tag and its constituents to the
	 * RCM
	 */
	private void addSummarySection(int rptType) {
		Summary summary = new Summary();
		lvaDbReport.setSummary(summary);
		String configuration = reportContent.getKeyMapValue("configuration", configId);
		summary.setConfiguration(configuration);

		int fitScore;

		// use updated fit score if it exists
		if (this.fitUpdate > 0) {
			fitScore = (int) Math.floor(this.fitUpdate + 0.5f);
		} else {
			fitScore = (int) Math.floor(this.fit + 0.5f);
		}

		Score rediScore = new Score();
		summary.setScore(rediScore);

		if (rptType == 1) {
			int rScore = (int) Math.floor(this.readiness + 0.5f);
			rediScore.setValue("" + rScore);
			rediScore.setContent(this.getReadinessLabel(rScore));
		} else {
			int rScore = fitScore;
			rediScore.setValue("" + rScore);
			rediScore.setContent(this.getFitLabel(rScore));
		}

		// Set up readiness recommendation
		Readiness redi = new Readiness();
		summary.setReadiness(redi);
		redi.setTitle("Readiness Recommendation");

		String gridMapId = (potential <= 2) ? "readinessRecommendationPotentialLowOrMixed"
				: "readinessRecommendationPotentialModerateOrHigh";
		String competencyId = "competencies" + getRangeString(Math.round(competencies));
		String experienceId = "experience" + getRangeString(Math.round(experience));
		redi.setText(createEditableText(reportContent.getGridMapValue(gridMapId, experienceId, competencyId)));

		// Set up Fit recommendation
		Fit fitness = new Fit();
		summary.setFit(fitness);
		fitness.setTitle("Fit Recommendation");
		fitness.setText(createEditableText(""));

		// No pre-written Fitness text at this time, but there should be some
		// provided in the future.
		// String fitgridMapId = (fit <= 2) ?
		// "fitRecommendationOverallLowOrMixed"
		// : "fitRecommendationPotentialModerateOrHigh";
		// String performanceId = "performance" +
		// getRangeString(Math.round(performance));
		// String satisfactionId = "satisfaction" +
		// getRangeString(Math.round(satisfaction));
		// fitness.setText(createEditableText(reportContent.getGridMapValue(fitgridMapId,
		// satisfactionId, performanceId)));

		// Set up development focus
		Development summaryDevelopment = new Development();
		summary.setDevelopment(summaryDevelopment);
		summaryDevelopment.setTitle("Development Focus");
		// Selecting appropriate id's based on development focus results
		String summaryDevelopmentGridMapId = (potential <= 2) ? "developmentFocusPotentialLowOrMixed"
				: "developmentFocusPotentialModerateOrHigh";
		String summaryDevelopmentCompetencyId = "competencies" + getRangeString(Math.round(competencies));
		String summaryDevelopmentExperienceId = "experience" + getRangeString(Math.round(experience));
		summaryDevelopment.setText(createEditableText(reportContent.getGridMapValue(summaryDevelopmentGridMapId,
				summaryDevelopmentExperienceId, summaryDevelopmentCompetencyId)));

		// Overall Strengths and Needs
		checkOverallStrengthOrNeed(competencies, "competencies", "Leadership Competencies");
		checkOverallStrengthOrNeed(experience, "experience", "Leadership Experience");
		checkOverallStrengthOrNeed(foundations, "foundations", "Leadership Foundations");
		checkOverallStrengthOrNeed(interest, "interest", "Leadership Interest");

		Strengths strengths = new Strengths();
		summary.setStrengths(strengths);
		strengths.setTitle("Strengths");
		strengths.setNoitems(reportContent.getKeyMapValue("overallClearStrength", "none"));

		// Overall Strengths
		if (arrOverallStrengths.size() > 0) {
			Items strengthItems = new Items();
			strengths.setItems(strengthItems);
			strengths.getItems().getItem().addAll(arrOverallStrengths);
		}

		Needs needs = new Needs();
		summary.setNeeds(needs);
		needs.setTitle("Development Needs");
		needs.setNoitems(reportContent.getKeyMapValue("overallSubstantialGap", "none"));

		// Overall Development Needs
		if (arrOverallNeeds.size() > 0) {
			Items needItems = new Items();
			needs.setItems(needItems);
			needs.getItems().getItem().addAll(arrOverallNeeds);
		}

		// Set up Advancement potential (readiness only)
		Potential advPot = new Potential();
		summary.setPotential(advPot);
		advPot.setTitle("Leadership Potential");
		String apGridMapId = (derailers <= 2) ? "advancementPotentialDerailersModerateOrHigh"
				: "advancementPotentialDerailersLowOrMinimal";
		String apInterest = "interest" + getRangeString(Math.round(interest));
		String apFoundations = "foundations" + getRangeString(Math.round(foundations));
		// advPot.setText(createText(reportContent.getGridMapValue(apGridMapId,
		// apInterest, apFoundations)));
		advPot.setText(createEditableText(reportContent.getGridMapValue(apGridMapId, apInterest, apFoundations)));

		Score apScore = new Score();
		advPot.setScore(apScore);
		int aScore = (int) Math.floor(this.potential + 0.5f);
		apScore.setValue("" + aScore);
		apScore.setContent(this.getApLabel(aScore));

		// Leadership Derailers
		Derailers summaryDerailers = new Derailers();
		summary.setDerailers(summaryDerailers);
		summaryDerailers.setTitle("Leadership Derailers");

		Score drScore = new Score();
		summaryDerailers.setScore(drScore);
		int dScore = (int) Math.floor(this.derailers + 0.5f);
		drScore.setValue("" + dScore);
		drScore.setContent(getDerailerLabel(dScore));
		// Don't put out anything if no items are in need. To avoid changes to
		// the report template this will now be an empty string
		summaryDerailers.setNoitems(""); // Empty string

		checkOverallDerailers(characteristicScores.get(TLTCharacteristic.MICRO_MANAGING), "microManaging");
		checkOverallDerailers(characteristicScores.get(TLTCharacteristic.EGO_CENTERED), "egoCentered");
		checkOverallDerailers(characteristicScores.get(TLTCharacteristic.MANIPULATION), "manipulation");
		checkOverallDerailers(characteristicScores.get(TLTCharacteristic.PASSIVE_AGGRESSIVE), "unwillingnessToConfront");

		Items derailerItems = new Items();
		summaryDerailers.setItems(derailerItems);
		// Make room for the preview
		Item derailerSummary = createItem("Preview", "");
		derailerSummary.getText().setUpdateId("derailersPreview");

		summaryDerailers.getItems().getItem().add(derailerSummary);

		if (arrOverallDerailers.size() > 0) {
			summaryDerailers.getItems().getItem().addAll(arrOverallDerailers);
		}
	}

	/**
	 * addRatingsSection - - Adds the "ratings" tag and its constituents to the
	 * RCM
	 */
	private void addRatingsSection() {
		Ratings ratings = new Ratings();
		lvaDbReport.setRatings(ratings);

		// Competencies section
		Section sectionCompetencies = new Section();
		ratings.getSection().add(sectionCompetencies);
		sectionCompetencies.setId("competencies");
		sectionCompetencies.setTitle("Leadership Competencies");

		// Percentile shown for fit and readiness reports
		sectionCompetencies.setPercentileLabel(PercentileText + this.percentile);

		Score sectionCompetenciesScore = new Score();
		sectionCompetencies.setScore(sectionCompetenciesScore);
		sectionCompetenciesScore.setValue(getRangeString(Math.round(competencies)).toLowerCase());
		sectionCompetenciesScore.setContent(getRangeLabel(Math.round(competencies)));
		// sectionCompetencies.setText(createText(reportContent.getKeyMapValue("overallCompetencies",
		// "competencies" + getRangeString(Math.round(competencies)))));
		sectionCompetencies.setText(createEditableText(reportContent.getKeyMapValue("overallCompetencies",
				"competencies" + getRangeString(Math.round(competencies)))));
		sectionCompetencies.getText().setUpdateId("competenciesPreview");

		// TODO Any of the following now?
		// Support pulling in a client-based report content file
		// Allow the competency labels to be overwritten by client
		// Could build from competency model for project
		// Could potentially use maapping integation grid uses
		Superfactors superFactors = new Superfactors();
		sectionCompetencies.setSuperfactors(superFactors);

		if (this.manifestRef.getModelContentRef().contains("internal/gl-gps")
				|| this.manifestRef.getModelContentRef().contains("winn dixie")) {
			Superfactor superFactorThought = new Superfactor();
			superFactors.getSuperfactor().add(superFactorThought);
			superFactorThought.setType("v423");
			superFactorThought.setName(this.modelContent.getKeyMapValue("superFactors", "thoughtLeadership"));
			Competencies competenciesThought = new Competencies();
			superFactorThought.setCompetencies(competenciesThought);
			competenciesThought.getCompetency().add(
					createCompetency(reachStrategicDecisions, "reachStrategicDecisions",
							this.modelContent.getKeyMapValue("competencies", "reachStrategicDecisions")));
			competenciesThought.getCompetency().add(
					createCompetency(fosterInnovation, "fosterInnovation",
							this.modelContent.getKeyMapValue("competencies", "fosterInnovation")));

			Superfactor superFactorResults = new Superfactor();
			superFactors.getSuperfactor().add(superFactorResults);
			superFactorResults.setType("v423");
			superFactorResults.setName(this.modelContent.getKeyMapValue("superFactors", "resultsLeadership"));
			Competencies competenciesResults = new Competencies();
			superFactorResults.setCompetencies(competenciesResults);
			competenciesResults.getCompetency().add(
					createCompetency(driveExecution, "driveExecution",
							this.modelContent.getKeyMapValue("competencies", "driveExecution")));
			competenciesResults.getCompetency().add(
					createCompetency(promoteStakeholderFocus, "promoteStakeholderFocus",
							this.modelContent.getKeyMapValue("competencies", "promoteStakeholderFocus")));
			competenciesResults.getCompetency().add(
					createCompetency(leadCourageously, "leadCourageously",
							this.modelContent.getKeyMapValue("competencies", "leadCourageously")));

			Superfactor superFactorPeople = new Superfactor();
			superFactors.getSuperfactor().add(superFactorPeople);
			superFactorPeople.setType("v423");
			superFactorPeople.setName(this.modelContent.getKeyMapValue("superFactors", "peopleLeadership"));
			Competencies competenciesPeople = new Competencies();
			superFactorPeople.setCompetencies(competenciesPeople);
			competenciesPeople.getCompetency().add(
					createCompetency(promoteCollaboration, "promoteCollaboration",
							this.modelContent.getKeyMapValue("competencies", "promoteCollaboration")));
			competenciesPeople.getCompetency().add(
					createCompetency(engageAndDevelopOthers, "engageAndDevelopOthers",
							this.modelContent.getKeyMapValue("competencies", "engageAndDevelopOthers")));
			competenciesPeople.getCompetency().add(
					createCompetency(buildSupportAndInfluence, "buildSupportAndInfluence",
							this.modelContent.getKeyMapValue("competencies", "buildSupportAndInfluence")));

			Superfactor superFactorPersonal = new Superfactor();
			superFactors.getSuperfactor().add(superFactorPersonal);
			superFactorPersonal.setType("v423");
			superFactorPersonal.setName(this.modelContent.getKeyMapValue("superFactors", "personalLeadership"));
			Competencies competenciesPersonal = new Competencies();
			superFactorPersonal.setCompetencies(competenciesPersonal);
			competenciesPersonal.getCompetency().add(
					createCompetency(demonstratesAgility, "demonstratesAgility",
							this.modelContent.getKeyMapValue("competencies", "demonstratesAgility")));
			competenciesPersonal.getCompetency().add(
					createCompetency(engageDiversePerspectives, "engageDiversePerspectives",
							this.modelContent.getKeyMapValue("competencies", "engageDiversePerspectives")));
		} else if (this.manifestRef.getModelContentRef().contains("petsmart")) {
			// PETSMART MODEL CONFIGURATION
			// ###################################################################
			Superfactor superFactorResults = new Superfactor();
			superFactors.getSuperfactor().add(superFactorResults);
			superFactorResults.setType("v423");
			superFactorResults.setName(this.modelContent.getKeyMapValue("superFactors", "resultsLeadership"));
			Competencies competenciesResults = new Competencies();
			superFactorResults.setCompetencies(competenciesResults);
			competenciesResults.getCompetency().add(
					createCompetency(reachStrategicDecisions, "reachStrategicDecisions",
							this.modelContent.getKeyMapValue("competencies", "reachStrategicDecisions")));
			competenciesResults.getCompetency().add(
					createCompetency(driveExecution, "driveExecution",
							this.modelContent.getKeyMapValue("competencies", "driveExecution")));
			competenciesResults.getCompetency().add(
					createCompetency(leadCourageously, "leadCourageously",
							this.modelContent.getKeyMapValue("competencies", "leadCourageously")));

			Superfactor superFactorPeople = new Superfactor();
			superFactors.getSuperfactor().add(superFactorPeople);
			superFactorPeople.setType("v423");
			superFactorPeople.setName(this.modelContent.getKeyMapValue("superFactors", "peopleLeadership"));
			Competencies competenciesPeople = new Competencies();
			superFactorPeople.setCompetencies(competenciesPeople);
			competenciesPeople.getCompetency().add(
					createCompetency(promoteCollaboration, "promoteCollaboration",
							this.modelContent.getKeyMapValue("competencies", "promoteCollaboration")));
			competenciesPeople.getCompetency().add(
					createCompetency(engageAndDevelopOthers, "engageAndDevelopOthers",
							this.modelContent.getKeyMapValue("competencies", "engageAndDevelopOthers")));
			competenciesPeople.getCompetency().add(
					createCompetency(buildSupportAndInfluence, "buildSupportAndInfluence",
							this.modelContent.getKeyMapValue("competencies", "buildSupportAndInfluence")));

			Superfactor superFactorPersonal = new Superfactor();
			superFactors.getSuperfactor().add(superFactorPersonal);
			superFactorPersonal.setType("v423");
			superFactorPersonal.setName(this.modelContent.getKeyMapValue("superFactors", "personalLeadership"));
			Competencies competenciesPersonal = new Competencies();
			superFactorPersonal.setCompetencies(competenciesPersonal);
			competenciesPersonal.getCompetency().add(
					createCompetency(demonstratesAgility, "demonstratesAgility",
							this.modelContent.getKeyMapValue("competencies", "demonstratesAgility")));
			competenciesPersonal.getCompetency().add(
					createCompetency(thinksCritically, "thinksCritically",
							this.modelContent.getKeyMapValue("competencies", "thinksCritically")));
			competenciesPersonal.getCompetency().add(
					createCompetency(engageDiversePerspectives, "engageDiversePerspectives",
							this.modelContent.getKeyMapValue("competencies", "engageDiversePerspectives")));
		} // END PETSMART MODEL CONFIGURATION
			// ##################################################################

		// TODO Is this something for now or later?
		// Support pulling in a client-based report content file
		// Allow the competency labels to be overwritten by client
		if (this.manifestRef.getModelContentRef().contains("internal/gl-gps")
				|| this.manifestRef.getModelContentRef().contains("winn dixie")) {
			checkCompetenciesStrengthOrNeed(reachStrategicDecisions,
					this.modelContent.getKeyMapValue("competencies", "reachStrategicDecisions"),
					"competenciesStrengths", "competenciesDevelopmentNeeds", "reachStrategicDecisions");
			checkCompetenciesStrengthOrNeed(fosterInnovation,
					this.modelContent.getKeyMapValue("competencies", "fosterInnovation"), "competenciesStrengths",
					"competenciesDevelopmentNeeds", "fosterInnovation");
			checkCompetenciesStrengthOrNeed(driveExecution,
					this.modelContent.getKeyMapValue("competencies", "driveExecution"), "competenciesStrengths",
					"competenciesDevelopmentNeeds", "driveExecution");
			checkCompetenciesStrengthOrNeed(promoteStakeholderFocus,
					this.modelContent.getKeyMapValue("competencies", "promoteStakeholderFocus"),
					"competenciesStrengths", "competenciesDevelopmentNeeds", "promoteStakeholderFocus");
			checkCompetenciesStrengthOrNeed(leadCourageously,
					this.modelContent.getKeyMapValue("competencies", "leadCourageously"), "competenciesStrengths",
					"competenciesDevelopmentNeeds", "leadCourageously");
			checkCompetenciesStrengthOrNeed(promoteCollaboration,
					this.modelContent.getKeyMapValue("competencies", "promoteCollaboration"), "competenciesStrengths",
					"competenciesDevelopmentNeeds", "promoteCollaboration");
			checkCompetenciesStrengthOrNeed(engageAndDevelopOthers,
					this.modelContent.getKeyMapValue("competencies", "engageAndDevelopOthers"),
					"competenciesStrengths", "competenciesDevelopmentNeeds", "engageAndDevelopOthers");
			checkCompetenciesStrengthOrNeed(buildSupportAndInfluence,
					this.modelContent.getKeyMapValue("competencies", "buildSupportAndInfluence"),
					"competenciesStrengths", "competenciesDevelopmentNeeds", "buildSupportAndInfluence");
			checkCompetenciesStrengthOrNeed(demonstratesAgility,
					this.modelContent.getKeyMapValue("competencies", "demonstratesAgility"), "competenciesStrengths",
					"competenciesDevelopmentNeeds", "demonstratesAgility");
			checkCompetenciesStrengthOrNeed(engageDiversePerspectives,
					this.modelContent.getKeyMapValue("competencies", "engageDiversePerspectives"),
					"competenciesStrengths", "competenciesDevelopmentNeeds", "engageDiversePerspectives");
		} else if (this.manifestRef.getModelContentRef().contains("petsmart")) {
			checkCompetenciesStrengthOrNeed(reachStrategicDecisions,
					this.modelContent.getKeyMapValue("competencies", "reachStrategicDecisions"),
					"competenciesStrengths", "competenciesDevelopmentNeeds", "reachStrategicDecisions");
			checkCompetenciesStrengthOrNeed(driveExecution,
					this.modelContent.getKeyMapValue("competencies", "driveExecution"), "competenciesStrengths",
					"competenciesDevelopmentNeeds", "driveExecution");
			// Minus Foster innovation and Promote stockholder focus, ADD
			// thinksCritically
			checkCompetenciesStrengthOrNeed(thinksCritically,
					this.modelContent.getKeyMapValue("competencies", "thinksCritically"), "competenciesStrengths",
					"competenciesDevelopmentNeeds", "thinksCritically");
			checkCompetenciesStrengthOrNeed(leadCourageously,
					this.modelContent.getKeyMapValue("competencies", "leadCourageously"), "competenciesStrengths",
					"competenciesDevelopmentNeeds", "leadCourageously");
			checkCompetenciesStrengthOrNeed(promoteCollaboration,
					this.modelContent.getKeyMapValue("competencies", "promoteCollaboration"), "competenciesStrengths",
					"competenciesDevelopmentNeeds", "promoteCollaboration");
			checkCompetenciesStrengthOrNeed(engageAndDevelopOthers,
					this.modelContent.getKeyMapValue("competencies", "engageAndDevelopOthers"),
					"competenciesStrengths", "competenciesDevelopmentNeeds", "engageAndDevelopOthers");
			checkCompetenciesStrengthOrNeed(buildSupportAndInfluence,
					this.modelContent.getKeyMapValue("competencies", "buildSupportAndInfluence"),
					"competenciesStrengths", "competenciesDevelopmentNeeds", "buildSupportAndInfluence");
			checkCompetenciesStrengthOrNeed(demonstratesAgility,
					this.modelContent.getKeyMapValue("competencies", "demonstratesAgility"), "competenciesStrengths",
					"competenciesDevelopmentNeeds", "demonstratesAgility");
			checkCompetenciesStrengthOrNeed(engageDiversePerspectives,
					this.modelContent.getKeyMapValue("competencies", "engageDiversePerspectives"),
					"competenciesStrengths", "competenciesDevelopmentNeeds", "engageDiversePerspectives");
		}
		// Strengths
		Strengths competenciesStrengths = new Strengths();
		sectionCompetencies.setStrengths(competenciesStrengths);
		competenciesStrengths.setTitle("Strengths");
		competenciesStrengths.setNoitems(reportContent.getKeyMapValue("competenciesStrengths", "none"));
		if (arrCompetenciesStrengths.size() > 0) {
			Items strengthItems = new Items();
			competenciesStrengths.setItems(strengthItems);
			competenciesStrengths.getItems().getItem().addAll(arrCompetenciesStrengths);
		}

		// Development Needs
		Needs competenciesNeeds = new Needs();
		sectionCompetencies.setNeeds(competenciesNeeds);
		competenciesNeeds.setTitle("Development Needs");
		competenciesNeeds.setNoitems(reportContent.getKeyMapValue("competenciesDevelopmentNeeds", "none"));
		if (arrCompetenciesNeeds.size() > 0) {
			Items needItems = new Items();
			competenciesNeeds.setItems(needItems);
			competenciesNeeds.getItems().getItem().addAll(arrCompetenciesNeeds);

			// TODO REFACTOR Find a a better way to do this. Eliminate the 2
			// level redundant check.
			// Maybe do it earlier when setting up the need item and pass it in
			// the item?
			// See if there are any dev sug/links to put out (at least one
			// behavior with a score <= 2.5
			boolean hasNeedSug = false;
			// Checking "reachStrategicDecisions
			if (b49 <= 2.5 || b29 <= 2.5 || b71 <= 2.5 || b30 <= 2.5 || b31 <= 2.5 || b32 <= 2.5)
				hasNeedSug = true;
			// Checking "engageDiversePerspectives"
			if (b46 <= 2.5 || b76 <= 2.5 || b47 <= 2.5 || b48 <= 2.5)
				hasNeedSug = true;
			// Checking "fosterInnovation"
			if (b33 <= 2.5 || b34 <= 2.5 || b69 <= 2.5 || b35 <= 2.5)
				hasNeedSug = true;
			// Checking "driveExecution"
			if (b50 <= 2.5 || b70 <= 2.5 || b51 <= 2.5 || b52 <= 2.5 || b53 <= 2.5 || b54 <= 2.5)
				hasNeedSug = true;
			// Checking "leadCourageously"
			if (b72 <= 2.5 || b59 <= 2.5 || b75 <= 2.5 || b36 <= 2.5)
				hasNeedSug = true;
			// Checking "promoteCollaboration"
			if (b37 <= 2.5 || b60 <= 2.5 || b38 <= 2.5 || b61 <= 2.5 || b39 <= 2.5 || b40 <= 2.5)
				hasNeedSug = true;
			// Checking "engageAndDevelopOthers"
			if (b62 <= 2.5 || b41 <= 2.5 || b63 <= 2.5 || b73 <= 2.5 || b74 <= 2.5 || b64 <= 2.5)
				hasNeedSug = true;
			// Checking "buildSupportAndInfluence"
			if (b65 <= 2.5 || b66 <= 2.5 || b67 <= 2.5 || b42 <= 2.5)
				hasNeedSug = true;
			// Checking "demonstratesAgility"
			if (b43 <= 2.5 || b68 <= 2.5 || b44 <= 2.5 || b45 <= 2.5)
				hasNeedSug = true;
			// Checking "promoteStakeholderFocus"
			if (b55 <= 2.5 || b56 <= 2.5 || b57 <= 2.5 || b58 <= 2.5)
				hasNeedSug = true;

			// Only do this if there is at least one dev sug candidate somewhere
			// in the competencies
			if (hasNeedSug) {
				// Development Suggestions & IA links
				Suggestions competenciesSuggestions = new Suggestions();
				sectionCompetencies.setSuggestions(competenciesSuggestions);
				competenciesSuggestions.setTitle("Development Suggestions");
				DevSections competenciesDevSections = new DevSections();
				competenciesSuggestions.setDevSections(competenciesDevSections);
				// Loop through the "needs" list
				// System.out.println("Competency needs: cnt=" +
				// this.arrCompetenciesNeeds.size());
				for (Item item : arrCompetenciesNeeds) {
					// So now we need to check if THIS competency has dev sugs
					boolean hasSugs = false;
					if (item.getId().equals("reachStrategicDecisions")) {
						if (b49 <= 2.5 || b29 <= 2.5 || b71 <= 2.5 || b30 <= 2.5 || b31 <= 2.5 || b32 <= 2.5)
							hasSugs = true;
					} else if (item.getId().equals("engageDiversePerspectives")) {
						if (b46 <= 2.5 || b76 <= 2.5 || b47 <= 2.5 || b48 <= 2.5)
							hasSugs = true;
					} else if (item.getId().equals("fosterInnovation")) {
						if (b33 <= 2.5 || b34 <= 2.5 || b69 <= 2.5 || b35 <= 2.5)
							hasSugs = true;
					} else if (item.getId().equals("driveExecution")) {
						if (b50 <= 2.5 || b70 <= 2.5 || b51 <= 2.5 || b52 <= 2.5 || b53 <= 2.5 || b54 <= 2.5)
							hasSugs = true;
					} else if (item.getId().equals("leadCourageously")) {
						if (b72 <= 2.5 || b59 <= 2.5 || b75 <= 2.5 || b36 <= 2.5)
							hasSugs = true;
					} else if (item.getId().equals("promoteCollaboration")) {
						if (b37 <= 2.5 || b60 <= 2.5 || b38 <= 2.5 || b61 <= 2.5 || b39 <= 2.5 || b40 <= 2.5)
							hasSugs = true;
					} else if (item.getId().equals("engageAndDevelopOthers")) {
						if (b62 <= 2.5 || b41 <= 2.5 || b63 <= 2.5 || b73 <= 2.5 || b74 <= 2.5 || b64 <= 2.5)
							hasSugs = true;
					} else if (item.getId().equals("buildSupportAndInfluence")) {
						if (b65 <= 2.5 || b66 <= 2.5 || b67 <= 2.5 || b42 <= 2.5)
							hasSugs = true;
					} else if (item.getId().equals("demonstratesAgility")) {
						if (b43 <= 2.5 || b68 <= 2.5 || b44 <= 2.5 || b45 <= 2.5)
							hasSugs = true;
					} else if (item.getId().equals("promoteStakeholderFocus")) {
						if (b55 <= 2.5 || b56 <= 2.5 || b57 <= 2.5 || b58 <= 2.5)
							hasSugs = true;
					} else if (item.getId().equals("thinksCritically")) {
						// PETSMART SPECIFIC! ( promoteStakeholderFocus +
						// fosterInnovation
						// #################################################
						if (b55 <= 2.5 || b56 <= 2.5 || b57 <= 2.5 || b58 <= 2.5 || b33 <= 2.5 || b34 <= 2.5
								|| b69 <= 2.5 || b35 <= 2.5)
							hasSugs = true;
						// PETSMART SPECIFIC!
						// ##############################################################################################
					}

					if (hasSugs) {
						DevSection devSection = new DevSection();
						competenciesSuggestions.getDevSections().getDevSection().add(devSection);
						devSection.setPage(true);
						devSection.setTitle(item.getTitle());

						Items items = new Items();
						devSection.setItems(items);
						IaLinks iaLinks = new IaLinks();
						devSection.setIaLinks(iaLinks);
						addBehaviorDevSuggsAndIaLinks(items, item, iaLinks);
					} // End of "if(hasSugs)"
				} // end of "for (Item item..."
			} // End of "if(hasNeedSug)"
		} // End of "if (arrCompetenciesNeeds.size() > 0)"

		// Experience section
		Section sectionExperience = new Section();
		ratings.getSection().add(sectionExperience);
		sectionExperience.setId("experience");
		sectionExperience.setTitle("Leadership Experience");
		Score sectionExperienceScore = new Score();
		sectionExperience.setScore(sectionExperienceScore);
		sectionExperienceScore.setValue(getRangeString(Math.round(experience)).toLowerCase());
		sectionExperienceScore.setContent(getRangeLabel(Math.round(experience)));
		// sectionExperience.setText(createText(reportContent.getKeyMapValue("overallExperience",
		// "experience" + getRangeString(Math.round(experience)))));
		sectionExperience.setText(createEditableText(reportContent.getKeyMapValue("overallExperience", "experience"
				+ getRangeString(Math.round(experience)))));
		sectionExperience.getText().setUpdateId("experiencePreview");
		// Experience characteristics
		Characteristics expChar = new Characteristics();
		sectionExperience.setCharacteristics(expChar);
		expChar.getCharacteristic().add(
				createCharacteristic("Business Operations Experience", TLTCharacteristic.BUSINESS_OPERATIONS));
		expChar.getCharacteristic().add(
				createCharacteristic("Handling Tough Leadership Challenges",
						TLTCharacteristic.HANDLING_TOUGH_CHALLENGES));
		expChar.getCharacteristic().add(
				createCharacteristic("High Visibility Leadership", TLTCharacteristic.HIGH_VISIBILITY));
		expChar.getCharacteristic().add(
				createCharacteristic("Growing the Business", TLTCharacteristic.GROWING_THE_BUSINESS));
		expChar.getCharacteristic().add(
				createCharacteristic("Personal/Career Development", TLTCharacteristic.PERSONAL_DEVELOPMENT));

		checkExperienceStrengthOrNeed(characteristicScores.get(TLTCharacteristic.BUSINESS_OPERATIONS),
				"Business Operations Experience", "experienceStrengths", "experienceDevelopmentNeeds",
				"businessOperationExperience");
		checkExperienceStrengthOrNeed(characteristicScores.get(TLTCharacteristic.HANDLING_TOUGH_CHALLENGES),
				"Handling Tough Leadership Challenges", "experienceStrengths", "experienceDevelopmentNeeds",
				"handlingToughLeadershipChallenges");
		checkExperienceStrengthOrNeed(characteristicScores.get(TLTCharacteristic.HIGH_VISIBILITY),
				"High Visibility Leadership", "experienceStrengths", "experienceDevelopmentNeeds",
				"highVisibilityLeadership");
		checkExperienceStrengthOrNeed(characteristicScores.get(TLTCharacteristic.GROWING_THE_BUSINESS),
				"Growing the Business", "experienceStrengths", "experienceDevelopmentNeeds", "growingTheBusiness");
		checkExperienceStrengthOrNeed(characteristicScores.get(TLTCharacteristic.PERSONAL_DEVELOPMENT),
				"Personal/Career Development", "experienceStrengths", "experienceDevelopmentNeeds",
				"personalCareerDevelopment");

		Strengths strengths = new Strengths();
		sectionExperience.setStrengths(strengths);
		strengths.setTitle("Strengths");
		strengths.setNoitems(reportContent.getKeyMapValue("experienceStrengths", "none"));
		if (arrExperienceStrengths.size() > 0) {
			Items strengthItems = new Items();
			strengths.setItems(strengthItems);
			strengths.getItems().getItem().addAll(arrExperienceStrengths);
		}

		Needs needs = new Needs();
		sectionExperience.setNeeds(needs);
		needs.setTitle("Development Needs");
		needs.setNoitems(reportContent.getKeyMapValue("experienceDevelopmentNeeds", "none"));
		if (arrExperienceNeeds.size() > 0) {
			Items needItems = new Items();
			needs.setItems(needItems);
			needs.getItems().getItem().addAll(arrExperienceNeeds);

			// Development Suggestions
			Suggestions experienceSuggestions = new Suggestions();
			sectionExperience.setSuggestions(experienceSuggestions);
			experienceSuggestions.setTitle("Development Suggestions");
			DevSections experienceDevSections = new DevSections();
			experienceSuggestions.setDevSections(experienceDevSections);
			// Loop through the experience items identified as having needs
			// System.out.println("Experience needs: cnt=" +
			// this.arrExperienceNeeds.size());
			for (Item item : arrExperienceNeeds) {
				DevSection devSection = new DevSection();
				experienceSuggestions.getDevSections().getDevSection().add(devSection);
				devSection.setPage(false);
				devSection.setTitle(item.getTitle());

				Items items = new Items();
				devSection.setItems(items);
				IaLinks iaLinks = new IaLinks();
				devSection.setIaLinks(iaLinks);
				addTraitDevSuggsAndIaLinks(items, item, iaLinks);
			}
		}

		// Foundations section
		Section sectionFoundations = new Section();
		ratings.getSection().add(sectionFoundations);
		sectionFoundations.setId("foundations");
		sectionFoundations.setTitle("Leadership Foundations");
		Score sectionFoundationsScore = new Score();
		sectionFoundations.setScore(sectionFoundationsScore);
		sectionFoundationsScore.setValue(getRangeString(Math.round(foundations)).toLowerCase());
		sectionFoundationsScore.setContent(getRangeLabel(Math.round(foundations)));
		// sectionFoundations.setText(createText(reportContent.getKeyMapValue("overallFoundations",
		// "foundations" + getRangeString(Math.round(foundations)))));
		sectionFoundations.setText(createEditableText(reportContent.getKeyMapValue("overallFoundations", "foundations"
				+ getRangeString(Math.round(foundations)))));
		sectionFoundations.getText().setUpdateId("foundationsPreview");
		// Foundations characteristics
		Characteristics foundationsChar = new Characteristics();
		sectionFoundations.setCharacteristics(foundationsChar);
		if (characteristicScores.get(TLTCharacteristic.PROBLEM_SOLVING) >= 0) {
			// Problem solving put out only if it is present
			foundationsChar.getCharacteristic().add(
					createCharacteristic("Problem Solving", TLTCharacteristic.PROBLEM_SOLVING));
		}
		foundationsChar.getCharacteristic().add(
				createCharacteristic("Intellectual Engagement", TLTCharacteristic.INTELLECTUAL_ENGAGEMENT));
		foundationsChar.getCharacteristic().add(
				createCharacteristic("Attention to Detail", TLTCharacteristic.ATTENTION_TO_DETAIL));
		foundationsChar.getCharacteristic().add(
				createCharacteristic("Impact/Influence", TLTCharacteristic.IMPACT_INFLUENCE));
		foundationsChar.getCharacteristic().add(
				createCharacteristic("Interpersonal Engagement", TLTCharacteristic.INTERPERSONAL_ENGAGEMENT));
		foundationsChar.getCharacteristic().add(
				createCharacteristic("Achievement Drive", TLTCharacteristic.ACHIEVEMENT_DRIVE));
		foundationsChar.getCharacteristic().add(
				createCharacteristic("Advancement Drive", TLTCharacteristic.ADVANCEMENT_DRIVE));
		foundationsChar.getCharacteristic().add(
				createCharacteristic("Collective Orientation", TLTCharacteristic.COLLECTIVE_ORIENTATION));
		foundationsChar.getCharacteristic().add(
				createCharacteristic("Flexibility/Adaptability", TLTCharacteristic.FLEXIBILITY_ADAPTABILITY));

		if (characteristicScores.get(TLTCharacteristic.PROBLEM_SOLVING) >= 0) {
			// Only put out problem solving if it exists
			checkFoundationsStrengthOrNeed(characteristicScores.get(TLTCharacteristic.PROBLEM_SOLVING),
					"Problem Solving", "foundationsStrengths", "foundationsDevelopmentNeedsTooLow",
					"foundationsDevelopmentNeedsTooHigh", "problemSolving");
		}
		checkFoundationsStrengthOrNeed(characteristicScores.get(TLTCharacteristic.INTELLECTUAL_ENGAGEMENT),
				"Intellectual Engagement", "foundationsStrengths", "foundationsDevelopmentNeedsTooLow",
				"foundationsDevelopmentNeedsTooHigh", "intellectualEngagement");
		checkFoundationsStrengthOrNeed(characteristicScores.get(TLTCharacteristic.ATTENTION_TO_DETAIL),
				"Attention to Detail", "foundationsStrengths", "foundationsDevelopmentNeedsTooLow",
				"foundationsDevelopmentNeedsTooHigh", "attentionToDetail");
		checkFoundationsStrengthOrNeed(characteristicScores.get(TLTCharacteristic.IMPACT_INFLUENCE),
				"Impact/Influence", "foundationsStrengths", "foundationsDevelopmentNeedsTooLow",
				"foundationsDevelopmentNeedsTooHigh", "impactAndInfluence");
		checkFoundationsStrengthOrNeed(characteristicScores.get(TLTCharacteristic.INTERPERSONAL_ENGAGEMENT),
				"Interpersonal Engagement", "foundationsStrengths", "foundationsDevelopmentNeedsTooLow",
				"foundationsDevelopmentNeedsTooHigh", "interpersonalEngagement");
		checkFoundationsStrengthOrNeed(characteristicScores.get(TLTCharacteristic.ACHIEVEMENT_DRIVE),
				"Achievement Drive", "foundationsStrengths", "foundationsDevelopmentNeedsTooLow",
				"foundationsDevelopmentNeedsTooHigh", "achievementDrive");
		checkFoundationsStrengthOrNeed(characteristicScores.get(TLTCharacteristic.ADVANCEMENT_DRIVE),
				"Advancement Drive", "foundationsStrengths", "foundationsDevelopmentNeedsTooLow",
				"foundationsDevelopmentNeedsTooHigh", "advancementDrive");
		checkFoundationsStrengthOrNeed(characteristicScores.get(TLTCharacteristic.COLLECTIVE_ORIENTATION),
				"Collective Orientation", "foundationsStrengths", "foundationsDevelopmentNeedsTooLow",
				"foundationsDevelopmentNeedsTooHigh", "collectiveOrientation");
		checkFoundationsStrengthOrNeed(characteristicScores.get(TLTCharacteristic.FLEXIBILITY_ADAPTABILITY),
				"Flexibility/Adaptability", "foundationsStrengths", "foundationsDevelopmentNeedsTooLow",
				"foundationsDevelopmentNeedsTooHigh", "flexibilityAdaptability");

		Strengths foundationsStrengths = new Strengths();
		sectionFoundations.setStrengths(foundationsStrengths);
		foundationsStrengths.setTitle("Strengths");
		foundationsStrengths.setNoitems(reportContent.getKeyMapValue("foundationsStrengths", "none"));

		// Strengths
		if (arrFoundationsStrengths.size() > 0) {
			Items strengthItems = new Items();
			foundationsStrengths.setItems(strengthItems);
			foundationsStrengths.getItems().getItem().addAll(arrFoundationsStrengths);
		}

		Needs foundationsNeeds = new Needs();
		sectionFoundations.setNeeds(foundationsNeeds);
		foundationsNeeds.setTitle("Development Needs");
		// Do we only put in here if have none, or always?
		foundationsNeeds.setNoitems(reportContent.getKeyMapValue("foundationsDevelopmentNeedsTooLow", "none"));

		// Development Needs
		if (arrFoundationsNeeds.size() > 0) {
			Items needItems = new Items();
			foundationsNeeds.setItems(needItems);
			foundationsNeeds.getItems().getItem().addAll(arrFoundationsNeeds);

			// Development Suggestions
			Suggestions foundationsSuggestions = new Suggestions();
			sectionFoundations.setSuggestions(foundationsSuggestions);
			foundationsSuggestions.setTitle("Development Suggestions");
			DevSections foundationsDevSections = new DevSections();
			foundationsSuggestions.setDevSections(foundationsDevSections);
			// Loop through the foundations needs
			// System.out.println("Foundation needs: cnt=" +
			// this.arrFoundationsNeeds.size());
			for (Item item : arrFoundationsNeeds) {
				DevSection devSection = new DevSection();
				foundationsSuggestions.getDevSections().getDevSection().add(devSection);
				devSection.setPage(false);
				devSection.setTitle(item.getTitle());

				Items items = new Items();
				devSection.setItems(items);
				IaLinks iaLinks = new IaLinks();
				devSection.setIaLinks(iaLinks);
				addTraitDevSuggsAndIaLinks(items, item, iaLinks);
			}
		}

		// Interest section
		Section sectionInterest = new Section();
		ratings.getSection().add(sectionInterest);
		sectionInterest.setId("interest");
		sectionInterest.setTitle("Leadership Interests");
		Score sectionInterestScore = new Score();
		sectionInterest.setScore(sectionInterestScore);
		sectionInterestScore.setValue(getRangeString(Math.round(interest)).toLowerCase());
		sectionInterestScore.setContent(getRangeLabel(Math.round(interest)));
		// sectionInterest.setText(createText(reportContent.getKeyMapValue("overallInterest",
		// "interest" + getRangeString(Math.round(interest)))));
		sectionInterest.setText(createEditableText(reportContent.getKeyMapValue("overallInterest", "interest"
				+ getRangeString(Math.round(interest)))));
		sectionInterest.getText().setUpdateId("interestPreview");
		// Interest characteristics
		// TODO What do we do when the box is variable? l-a is different form
		// c-d is different from both orientations
		Characteristics interestChar = new Characteristics();
		sectionInterest.setCharacteristics(interestChar);
		// No type set for interestChar; all componets will have a type set
		interestChar.getCharacteristic().add(
				createCharacteristic("Leadership Aspirations", TLTCharacteristic.LEADERSHIP_ASPIRATION));
		interestChar.getCharacteristic().add(createCharacteristic("Career Drivers", TLTCharacteristic.CAREER_DRIVERS));
		interestChar.getCharacteristic().add(
				createCharacteristic("Learning Orientation", TLTCharacteristic.LEARNING_ORIENTATION));
		interestChar.getCharacteristic().add(
				createCharacteristic("Experience Orientation", TLTCharacteristic.EXPERIENCE_ORIENTATION));

		checkInterestStrengthOrNeed(characteristicScores.get(TLTCharacteristic.LEADERSHIP_ASPIRATION),
				"Leadership Aspirations", "interestStrengths", "interestDevelopmentNeedsTooLow",
				"interestDevelopmentNeedsTooHigh", "leadershipAspiration");
		checkInterestStrengthOrNeed(characteristicScores.get(TLTCharacteristic.CAREER_DRIVERS), "Career Drivers",
				"interestStrengths", "interestDevelopmentNeedsTooLow", "interestDevelopmentNeedsTooHigh",
				"careerDrivers");
		checkInterestStrengthOrNeed(characteristicScores.get(TLTCharacteristic.LEARNING_ORIENTATION),
				"Learning Orientation", "interestStrengths", "interestDevelopmentNeedsTooLow",
				"interestDevelopmentNeedsTooHigh", "learningOrientation");
		checkInterestStrengthOrNeed(characteristicScores.get(TLTCharacteristic.EXPERIENCE_ORIENTATION),
				"Experience Orientation", "interestStrengths", "interestDevelopmentNeedsTooLow",
				"interestDevelopmentNeedsTooHigh", "experienceOrientation");

		Strengths interestStrengths = new Strengths();
		sectionInterest.setStrengths(interestStrengths);
		interestStrengths.setTitle("Strengths");
		interestStrengths.setNoitems(reportContent.getKeyMapValue("interestStrengths", "none"));

		// Strengths
		if (arrInterestStrengths.size() > 0) {
			Items strengthItems = new Items();
			interestStrengths.setItems(strengthItems);
			interestStrengths.getItems().getItem().addAll(arrInterestStrengths);
		}

		Needs interestNeeds = new Needs();
		sectionInterest.setNeeds(interestNeeds);
		interestNeeds.setTitle("Development Needs");
		interestNeeds.setNoitems(reportContent.getKeyMapValue("interestDevelopmentNeedsTooLow", "none"));

		// Development Needs
		if (arrInterestNeeds.size() > 0) {
			Items needItems = new Items();
			interestNeeds.setItems(needItems);
			interestNeeds.getItems().getItem().addAll(arrInterestNeeds);

			// Development Suggestions
			Suggestions interestSuggestions = new Suggestions();
			sectionInterest.setSuggestions(interestSuggestions);
			interestSuggestions.setTitle("Development Suggestions");
			DevSections interestDevSections = new DevSections();
			interestSuggestions.setDevSections(interestDevSections);
			// System.out.println("Interest needs: cnt=" +
			// this.arrInterestNeeds.size());
			for (Item item : arrInterestNeeds) {
				DevSection devSection = new DevSection();
				interestSuggestions.getDevSections().getDevSection().add(devSection);
				devSection.setPage(false);
				devSection.setTitle(item.getTitle());

				Items items = new Items();
				devSection.setItems(items);
				IaLinks iaLinks = new IaLinks();
				devSection.setIaLinks(iaLinks);
				addTraitDevSuggsAndIaLinks(items, item, iaLinks);
			}
		}

		// Derailers section
		Section sectionDerailers = new Section();
		ratings.getSection().add(sectionDerailers);
		sectionDerailers.setId("derailers");
		sectionDerailers.setTitle("Leadership Derailers");
		Score sectionDerailersScore = new Score();
		sectionDerailers.setScore(sectionDerailersScore);
		sectionDerailersScore.setValue(getRangeString(Math.round(derailers)).toLowerCase());
		sectionDerailersScore.setContent(getRangeLabel(Math.round(derailers)));
		// sectionDerailers.setText(createText(reportContent.getKeyMapValue("overallDerailers",
		// "derailers" + getRangeString(Math.round(derailers)))));
		sectionDerailers.setText(createEditableText(reportContent.getKeyMapValue("overallDerailers", "derailers"
				+ getRangeString(Math.round(derailers)))));
		sectionDerailers.getText().setUpdateId("derailersPreview");
		// Derailers characteristics
		Characteristics derailersChar = new Characteristics();
		sectionDerailers.setCharacteristics(derailersChar);
		derailersChar.getCharacteristic().add(createCharacteristic("Ego Centered", TLTCharacteristic.EGO_CENTERED));
		derailersChar.getCharacteristic().add(createCharacteristic("Manipulative", TLTCharacteristic.MANIPULATION));
		derailersChar.getCharacteristic().add(createCharacteristic("Micro Managing", TLTCharacteristic.MICRO_MANAGING));
		derailersChar.getCharacteristic().add(
				createCharacteristic("Unwillingness to Confront", TLTCharacteristic.PASSIVE_AGGRESSIVE));

		checkDerailers(characteristicScores.get(TLTCharacteristic.EGO_CENTERED), "Ego Centered",
				"derailerDevelopmentNeedsStanine9", "derailerNotesStanine7Or8", "egoCentered");
		checkDerailers(characteristicScores.get(TLTCharacteristic.MANIPULATION), "Manipulative",
				"derailerDevelopmentNeedsStanine9", "derailerNotesStanine7Or8", "manipulation");
		checkDerailers(characteristicScores.get(TLTCharacteristic.MICRO_MANAGING), "Micro Managing",
				"derailerDevelopmentNeedsStanine9", "derailerNotesStanine7Or8", "microManaging");
		checkDerailers(characteristicScores.get(TLTCharacteristic.PASSIVE_AGGRESSIVE), "Unwillingness to Confront",
				"derailerDevelopmentNeedsStanine9", "derailerNotesStanine7Or8", "unwillingnessToConfront");

		// No Strengths for derailers

		// Development Needs
		// Needs
		Needs derailersStanine9 = new Needs();
		sectionDerailers.setNeeds(derailersStanine9);
		derailersStanine9.setTitle("Development Needs");
		derailersStanine9.setNoitems(reportContent.getKeyMapValue("derailerDevelopmentNeedsStanine9", "none"));
		if (arrDerailersStanine9.size() > 0) {
			Items needItems = new Items();
			derailersStanine9.setItems(needItems);
			derailersStanine9.getItems().getItem().addAll(arrDerailersStanine9);
		}

		// Notes
		if (arrDerailersStanine7Or8.size() > 0) {
			Notes derailersStanine7Or8 = new Notes();
			sectionDerailers.setNotes(derailersStanine7Or8);
			derailersStanine7Or8.setTitle("Note");

			Items items = new Items();
			derailersStanine7Or8.setItems(items);
			derailersStanine7Or8.getItems().getItem().addAll(arrDerailersStanine7Or8);
		}

		if (arrDerailersStanine9.size() > 0) {
			// Development Suggestions
			Suggestions derailerSuggestions = new Suggestions();
			sectionDerailers.setSuggestions(derailerSuggestions);
			derailerSuggestions.setTitle("Development Suggestions");
			DevSections derailerDevSections = new DevSections();
			derailerSuggestions.setDevSections(derailerDevSections);
			// System.out.println("Derailers dev sugs: cnt=" +
			// this.arrDerailersStanine9.size());
			for (Item item : arrDerailersStanine9) {
				DevSection devSection = new DevSection();
				derailerSuggestions.getDevSections().getDevSection().add(devSection);
				devSection.setPage(false);
				devSection.setTitle(item.getTitle());

				Items items = new Items();
				devSection.setItems(items);
				IaLinks iaLinks = new IaLinks();
				devSection.setIaLinks(iaLinks);
				addTraitDevSuggsAndIaLinks(items, item, iaLinks);
			}
		}

	}

	private void addAppendix() {
		// Appendix
		log.debug("IN addAppendix");
		Appendix appendix = new Appendix();
		lvaDbReport.setAppendix(appendix);
		appendix.setTitle("Appendix: Definitions of Categories and Scales");

		HtmlSections htmlSections = new HtmlSections();

		htmlSections.getHtmlSection().add(
				createHtmlSection("leadershipCompetencies", "Leadership Competencies",
						appendixContent.getKeyMapValue("leadershipCompetencies", "html")));
		htmlSections.getHtmlSection().add(
				createHtmlSection("leadershipExperience", "Leadership Experience",
						appendixContent.getKeyMapValue("leadershipExperience", "html")));
		htmlSections.getHtmlSection().add(
				createHtmlSection("leadershipFoundations", "Leadership Foundations",
						appendixContent.getKeyMapValue("leadershipFoundations", "html")));
		htmlSections.getHtmlSection().add(
				createHtmlSection("leadershipInterest", "Leadership Interest",
						appendixContent.getKeyMapValue("leadershipInterest", "html")));
		htmlSections.getHtmlSection().add(
				createHtmlSection("leadershipDerailers", "Leadership Derailers",
						appendixContent.getKeyMapValue("leadershipDerailers", "html")));

		appendix.setHtmlSections(htmlSections);

	}

	/**
	 * addBehaviorDevSuggsAndIaLinks - Add development suggestions and IA links
	 * to the appropriate section Put each dev sugg into the output for the low
	 * ranked behaviors
	 * 
	 * @param items
	 *            - The Items section in the overlying development section
	 * @param item
	 *            - The "need" item currently being processed
	 */
	// TODO Wow...really want to drive this from the model...just not enough
	// time to do that yet...
	private void addBehaviorDevSuggsAndIaLinks(Items items, Item item, IaLinks iaLinks) {

		if (item.getId().equals("reachStrategicDecisions")) {
			if (b49 <= 2.5) {
				doBehDsAndIa("devSuggB49", items, iaLinks);
			}
			if (b29 <= 2.5) {
				doBehDsAndIa("devSuggB29", items, iaLinks);
			}
			if (b71 <= 2.5) {
				doBehDsAndIa("devSuggB71", items, iaLinks);
			}
			if (b30 <= 2.5) {
				doBehDsAndIa("devSuggB30", items, iaLinks);
			}
			if (b31 <= 2.5) {
				doBehDsAndIa("devSuggB31", items, iaLinks);
			}
			if (b32 <= 2.5) {
				doBehDsAndIa("devSuggB32", items, iaLinks);
			}
		} else if (item.getId().equals("engageDiversePerspectives")) {
			if (b46 <= 2.5) {
				doBehDsAndIa("devSuggB46", items, iaLinks);
			}
			if (b76 <= 2.5) {
				doBehDsAndIa("devSuggB76", items, iaLinks);
			}
			if (b47 <= 2.5) {
				doBehDsAndIa("devSuggB47", items, iaLinks);
			}
			if (b48 <= 2.5) {
				doBehDsAndIa("devSuggB48", items, iaLinks);
			}
		} else if (item.getId().equals("fosterInnovation")) {
			if (b33 <= 2.5) {
				doBehDsAndIa("devSuggB33", items, iaLinks);
			}
			if (b34 <= 2.5) {
				doBehDsAndIa("devSuggB34", items, iaLinks);
			}
			if (b69 <= 2.5) {
				doBehDsAndIa("devSuggB69", items, iaLinks);
			}
			if (b35 <= 2.5) {
				doBehDsAndIa("devSuggB35", items, iaLinks);
			}
		} else if (item.getId().equals("driveExecution")) {
			if (b50 <= 2.5) {
				doBehDsAndIa("devSuggB50", items, iaLinks);
			}
			if (b70 <= 2.5) {
				doBehDsAndIa("devSuggB70", items, iaLinks);
			}
			if (b51 <= 2.5) {
				doBehDsAndIa("devSuggB51", items, iaLinks);
			}
			if (b52 <= 2.5) {
				doBehDsAndIa("devSuggB52", items, iaLinks);
			}
			if (b53 <= 2.5) {
				doBehDsAndIa("devSuggB53", items, iaLinks);
			}
			if (b54 <= 2.5) {
				doBehDsAndIa("devSuggB54", items, iaLinks);
			}
		} else if (item.getId().equals("leadCourageously")) {
			if (b72 <= 2.5) {
				doBehDsAndIa("devSuggB72", items, iaLinks);
			}
			if (b59 <= 2.5) {
				doBehDsAndIa("devSuggB59", items, iaLinks);
			}
			if (b75 <= 2.5) {
				doBehDsAndIa("devSuggB75", items, iaLinks);
			}
			if (b36 <= 2.5) {
				doBehDsAndIa("devSuggB36", items, iaLinks);
			}
		} else if (item.getId().equals("promoteCollaboration")) {
			if (b37 <= 2.5) {
				doBehDsAndIa("devSuggB37", items, iaLinks);
			}
			if (b60 <= 2.5) {
				doBehDsAndIa("devSuggB60", items, iaLinks);
			}
			if (b38 <= 2.5) {
				doBehDsAndIa("devSuggB38", items, iaLinks);
			}
			if (b61 <= 2.5) {
				doBehDsAndIa("devSuggB61", items, iaLinks);
			}
			if (b39 <= 2.5) {
				doBehDsAndIa("devSuggB39", items, iaLinks);
			}
			if (b40 <= 2.5) {
				doBehDsAndIa("devSuggB40", items, iaLinks);
			}
		} else if (item.getId().equals("engageAndDevelopOthers")) {
			if (b62 <= 2.5) {
				doBehDsAndIa("devSuggB62", items, iaLinks);
			}
			if (b41 <= 2.5) {
				doBehDsAndIa("devSuggB41", items, iaLinks);
			}
			if (b63 <= 2.5) {
				doBehDsAndIa("devSuggB63", items, iaLinks);
			}
			if (b73 <= 2.5) {
				doBehDsAndIa("devSuggB73", items, iaLinks);
			}
			if (b74 <= 2.5) {
				doBehDsAndIa("devSuggB74", items, iaLinks);
			}
			if (b64 <= 2.5) {
				doBehDsAndIa("devSuggB64", items, iaLinks);
			}
		} else if (item.getId().equals("buildSupportAndInfluence")) {
			if (b65 <= 2.5) {
				doBehDsAndIa("devSuggB65", items, iaLinks);
			}
			if (b66 <= 2.5) {
				doBehDsAndIa("devSuggB66", items, iaLinks);
			}
			if (b67 <= 2.5) {
				doBehDsAndIa("devSuggB67", items, iaLinks);
			}
			if (b42 <= 2.5) {
				doBehDsAndIa("devSuggB42", items, iaLinks);
			}
		} else if (item.getId().equals("demonstratesAgility")) {
			if (b43 <= 2.5) {
				doBehDsAndIa("devSuggB43", items, iaLinks);
			}
			if (b68 <= 2.5) {
				doBehDsAndIa("devSuggB68", items, iaLinks);
			}
			if (b44 <= 2.5) {
				doBehDsAndIa("devSuggB44", items, iaLinks);
			}
			if (b45 <= 2.5) {
				doBehDsAndIa("devSuggB45", items, iaLinks);
			}
		} else if (item.getId().equals("promoteStakeholderFocus")) {
			if (b55 <= 2.5) {
				doBehDsAndIa("devSuggB55", items, iaLinks);
			}
			if (b56 <= 2.5) {
				doBehDsAndIa("devSuggB56", items, iaLinks);
			}
			if (b57 <= 2.5) {
				doBehDsAndIa("devSuggB57", items, iaLinks);
			}
			if (b58 <= 2.5) {
				doBehDsAndIa("devSuggB58", items, iaLinks);
			}
		} else if (item.getId().equals("thinksCritically")) {
			// PETSMART SPECIFIC! #################################
			// promoteStakeholderFocus
			if (b55 <= 2.5) {
				doBehDsAndIa("devSuggB55", items, iaLinks);
			}
			if (b56 <= 2.5) {
				doBehDsAndIa("devSuggB56", items, iaLinks);
			}
			if (b57 <= 2.5) {
				doBehDsAndIa("devSuggB57", items, iaLinks);
			}
			if (b58 <= 2.5) {
				doBehDsAndIa("devSuggB58", items, iaLinks);
			}
			// fosterInnovation
			if (b33 <= 2.5) {
				doBehDsAndIa("devSuggB33", items, iaLinks);
			}
			if (b34 <= 2.5) {
				doBehDsAndIa("devSuggB34", items, iaLinks);
			}
			if (b69 <= 2.5) {
				doBehDsAndIa("devSuggB69", items, iaLinks);
			}
			if (b35 <= 2.5) {
				doBehDsAndIa("devSuggB35", items, iaLinks);
			}
			// PETSMART SPECIFIC! #################################
		}
	}

	/**
	 * doBehDsAndIa - Convenience method that calls the methods that set up the
	 * dev sugs and ia links
	 * 
	 * @param key
	 * @param items
	 * @param iaLinks
	 */
	private void doBehDsAndIa(String key, Items items, IaLinks iaLinks) {
		// Get the dev sug
		items.getItem().add(createItem("", behaviorContent.getKeyMapValue(key, "devSugg")));

		// Get the ia link(s)
		List<Link> linkList = new ArrayList<Link>();

		for (int i = 1; i <= 6; i++) {
			// Get the ialink text from the content
			String txt = behaviorContent.getKeyMapValue(key, "Ia" + i + "DisplayText");
			// if null or "" done
			if (txt == null || txt.length() < 1)
				break;
			// get the key
			String id = behaviorContent.getKeyMapValue(key, "Ia" + i + "Name");
			// create a new ialink
			Link link = new Link();
			// put in the text & URL
			link.setDisplayText(txt);
			link.setUrl(makeIaUrl(id));
			// put it in the linkList
			linkList.add(link);
		}

		iaLinks.getLink().addAll(linkList);
		return;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	private String makeIaUrl(String name) {
		// Build the IA URL... start by getting the template
		String urlStr = lvaProperties.getProperty("lvaIaUrlTemplate");
		urlStr = urlStr.replace("{orionHost}", lvaProperties.getProperty("lvaIaOrionHost"));
		urlStr = urlStr.replace("{iaEmailAddr}", lvaProperties.getProperty("mlllvaIaEmailAddr"));
		urlStr = urlStr.replace("{voucherId}", "" + this.voucherId);
		urlStr = urlStr.replace("{iaFirstName}", lvaProperties.getProperty("mlllvaIaFirstName"));
		urlStr = urlStr.replace("{iaLastName}", lvaProperties.getProperty("mlllvaIaLastName"));
		urlStr = urlStr.replace("{iaIntName}", name);

		return urlStr;
	}

	/**
	 * addTraitDevSuggsAndIaLinks - Add trait related dev sugs and IA links
	 * 
	 * @param items
	 * @param item
	 * @param iaLinks
	 */
	// TODO Wow...really want to drive this from the model...just not enough
	// time to do that yet...
	// Right now content hardwired...

	// Make sure this is coordinated with the stuff in
	// checkInterestStrengthOrNeed()
	private void addTraitDevSuggsAndIaLinks(Items items, Item item, IaLinks iaLinks) {

		// Leadership Experience traits
		// Note that experience has a low dev sug only the score should ALWAYS
		// be < 4
		if (item.getId().equals("businessOperationExperience")) {
			if (characteristicScores.get(TLTCharacteristic.BUSINESS_OPERATIONS) < 4) {
				doTraitDsAndIa("devSuggT18", TOO_LOW, items, iaLinks);
			}
		} else if (item.getId().equals("handlingToughLeadershipChallenges")) {
			if (characteristicScores.get(TLTCharacteristic.HANDLING_TOUGH_CHALLENGES) < 4) {
				doTraitDsAndIa("devSuggT19", TOO_LOW, items, iaLinks);
			}
		} else if (item.getId().equals("highVisibilityLeadership")) {
			if (characteristicScores.get(TLTCharacteristic.HIGH_VISIBILITY) < 4) {
				doTraitDsAndIa("devSuggT20", TOO_LOW, items, iaLinks);
			}
		} else if (item.getId().equals("growingTheBusiness")) {
			if (characteristicScores.get(TLTCharacteristic.GROWING_THE_BUSINESS) < 4) {
				doTraitDsAndIa("devSuggT21", TOO_LOW, items, iaLinks);
			}
		} else if (item.getId().equals("personalCareerDevelopment")) {
			if (characteristicScores.get(TLTCharacteristic.PERSONAL_DEVELOPMENT) < 4) {
				doTraitDsAndIa("devSuggT22", TOO_LOW, items, iaLinks);
			}
		}

		// Leadership Foundations
		else if (item.getId().equals("problemSolving")) {
			if (characteristicScores.get(TLTCharacteristic.PROBLEM_SOLVING) >= 0) {
				if (characteristicScores.get(TLTCharacteristic.PROBLEM_SOLVING) < 3) {
					doTraitDsAndIa("devSuggT9", TOO_LOW, items, iaLinks);
				} else if (characteristicScores.get(TLTCharacteristic.PROBLEM_SOLVING) > 7) {
					doTraitDsAndIa("devSuggT9", TOO_HIGH, items, iaLinks);
				}
			}
		} else if (item.getId().equals("intellectualEngagement")) {
			if (characteristicScores.get(TLTCharacteristic.INTELLECTUAL_ENGAGEMENT) < 3) {
				doTraitDsAndIa("devSuggT10", TOO_LOW, items, iaLinks);
			} else if (characteristicScores.get(TLTCharacteristic.INTELLECTUAL_ENGAGEMENT) > 7) {
				doTraitDsAndIa("devSuggT10", TOO_HIGH, items, iaLinks);
			}
		} else if (item.getId().equals("attentionToDetail")) {
			if (characteristicScores.get(TLTCharacteristic.ATTENTION_TO_DETAIL) < 3) {
				doTraitDsAndIa("devSuggT11", TOO_LOW, items, iaLinks);
			} else if (characteristicScores.get(TLTCharacteristic.ATTENTION_TO_DETAIL) > 7) {
				doTraitDsAndIa("devSuggT11", TOO_HIGH, items, iaLinks);
			}
		} else if (item.getId().equals("impactAndInfluence")) {
			if (characteristicScores.get(TLTCharacteristic.IMPACT_INFLUENCE) < 3) {
				doTraitDsAndIa("devSuggT12", TOO_LOW, items, iaLinks);
			} else if (characteristicScores.get(TLTCharacteristic.IMPACT_INFLUENCE) > 7) {
				doTraitDsAndIa("devSuggT12", TOO_HIGH, items, iaLinks);
			}
		} else if (item.getId().equals("interpersonalEngagement")) {
			if (characteristicScores.get(TLTCharacteristic.INTERPERSONAL_ENGAGEMENT) < 3) {
				doTraitDsAndIa("devSuggT13", TOO_LOW, items, iaLinks);
			} else if (characteristicScores.get(TLTCharacteristic.INTERPERSONAL_ENGAGEMENT) > 7) {
				doTraitDsAndIa("devSuggT13", TOO_HIGH, items, iaLinks);
			}
		} else if (item.getId().equals("achievementDrive")) {
			if (characteristicScores.get(TLTCharacteristic.ACHIEVEMENT_DRIVE) < 3) {
				doTraitDsAndIa("devSuggT14", TOO_LOW, items, iaLinks);
			} else if (characteristicScores.get(TLTCharacteristic.ACHIEVEMENT_DRIVE) > 7) {
				doTraitDsAndIa("devSuggT14", TOO_HIGH, items, iaLinks);
			}
		} else if (item.getId().equals("advancementDrive")) {
			if (characteristicScores.get(TLTCharacteristic.ADVANCEMENT_DRIVE) < 3) {
				doTraitDsAndIa("devSuggT15", TOO_LOW, items, iaLinks);
			} else if (characteristicScores.get(TLTCharacteristic.ADVANCEMENT_DRIVE) > 7) {
				doTraitDsAndIa("devSuggT15", TOO_HIGH, items, iaLinks);
			}
		} else if (item.getId().equals("collectiveOrientation")) {
			if (characteristicScores.get(TLTCharacteristic.COLLECTIVE_ORIENTATION) < 3) {
				doTraitDsAndIa("devSuggT16", TOO_LOW, items, iaLinks);
			} else if (characteristicScores.get(TLTCharacteristic.COLLECTIVE_ORIENTATION) > 7) {
				doTraitDsAndIa("devSuggT16", TOO_HIGH, items, iaLinks);
			}
		} else if (item.getId().equals("flexibilityAdaptability")) {
			if (characteristicScores.get(TLTCharacteristic.FLEXIBILITY_ADAPTABILITY) < 3) {
				doTraitDsAndIa("devSuggT17", TOO_LOW, items, iaLinks);
			} else if (characteristicScores.get(TLTCharacteristic.FLEXIBILITY_ADAPTABILITY) > 7) {
				doTraitDsAndIa("devSuggT17", TOO_HIGH, items, iaLinks);
			}
		}

		// Leadership Interest
		else if (item.getId().equals("leadershipAspiration")) {
			if (characteristicScores.get(TLTCharacteristic.LEADERSHIP_ASPIRATION) < 4) {
				doTraitDsAndIa("devSuggT5", TOO_LOW, items, iaLinks);
			} else if (characteristicScores.get(TLTCharacteristic.LEADERSHIP_ASPIRATION) > 8) {
				doTraitDsAndIa("devSuggT5", TOO_HIGH, items, iaLinks);
			}
		} else if (item.getId().equals("careerDrivers")) {
			if (characteristicScores.get(TLTCharacteristic.CAREER_DRIVERS) < 4) {
				doTraitDsAndIa("devSuggT6", TOO_LOW, items, iaLinks);
			}
			// There is no "too high range for career drivers
		} else if (item.getId().equals("learningOrientation")) {
			if (characteristicScores.get(TLTCharacteristic.LEARNING_ORIENTATION) < 3) {
				doTraitDsAndIa("devSuggT7", TOO_LOW, items, iaLinks);
			} else if (characteristicScores.get(TLTCharacteristic.LEARNING_ORIENTATION) > 7) {
				doTraitDsAndIa("devSuggT7", TOO_HIGH, items, iaLinks);
			}
		} else if (item.getId().equals("experienceOrientation")) {
			if (characteristicScores.get(TLTCharacteristic.EXPERIENCE_ORIENTATION) < 3) {
				doTraitDsAndIa("devSuggT8", TOO_LOW, items, iaLinks);
			} else if (characteristicScores.get(TLTCharacteristic.EXPERIENCE_ORIENTATION) > 7) {
				doTraitDsAndIa("devSuggT8", TOO_HIGH, items, iaLinks);
			}
		}

		// Derailers
		else if (item.getId().equals("egoCentered")) {
			if (characteristicScores.get(TLTCharacteristic.EGO_CENTERED) > 8) {
				doTraitDsAndIa("devSuggT1", TOO_HIGH, items, iaLinks);
			}
		} else if (item.getId().equals("manipulation")) {
			if (characteristicScores.get(TLTCharacteristic.MANIPULATION) > 8) {
				doTraitDsAndIa("devSuggT2", TOO_HIGH, items, iaLinks);
			}
		} else if (item.getId().equals("microManaging")) {
			if (characteristicScores.get(TLTCharacteristic.MICRO_MANAGING) > 8) {
				doTraitDsAndIa("devSuggT3", TOO_HIGH, items, iaLinks);
			}
		} else if (item.getId().equals("unwillingnessToConfront")) {
			if (characteristicScores.get(TLTCharacteristic.PASSIVE_AGGRESSIVE) > 8) {
				doTraitDsAndIa("devSuggT4", TOO_HIGH, items, iaLinks);
			}
		} else {
			log.debug("Trait id {} not found.  No devsug/IA links generated", item.getId());
		}
	}

	/**
	 * doTraitDsAndIa - Add Dev Suggs and IA links for traits
	 * 
	 * @param key
	 * @param items
	 * @param iaLinks
	 */
	private void doTraitDsAndIa(String key, int highLow, Items items, IaLinks iaLinks) {
		// Get the dev sug
		if (highLow == TOO_LOW || highLow == TOO_HIGH) {
			String hilo = (highLow == TOO_LOW) ? "devSuggTooLow" : "devSuggTooHigh";
			items.getItem().add(createItem("", traitContent.getKeyMapValue(key, hilo)));
		} else {
			// Invalid flag... scram
			log.debug("Invalid high/low dev sug marker:  key=[], value={}", key, highLow);
			return;
		}

		// Get the ia link(s)
		List<Link> linkList = new ArrayList<Link>();

		for (int i = 1; i <= 2; i++) {
			// Get the ialink text from the content
			String txt = traitContent.getKeyMapValue(key, "Ia" + i + "DisplayText");
			// if null or "" done
			if (txt == null || txt.length() < 1)
				break;
			// get the key
			String id = traitContent.getKeyMapValue(key, "Ia" + i + "Name");
			// create a new ialink
			Link link = new Link();
			// put in the text & URL
			link.setDisplayText(txt);
			link.setUrl(makeIaUrl(id));
			// put it in the linkList
			linkList.add(link);
		}

		iaLinks.getLink().addAll(linkList);
	}

	/**
	 * checkOverallStrengthOrNeed - Determines if a score for an item denotes a
	 * strength or need
	 * 
	 * Note that this method is for the overall ratings Cut points appear to be
	 * 1 and 2 for needs, 3 and 4 for stengths
	 * 
	 * @param score
	 *            - The score to judge against
	 * @param itemId
	 *            - The internal item id
	 * @param itemTitle
	 *            - The item title to be displayed
	 */
	private void checkOverallStrengthOrNeed(float score, String itemId, String itemTitle) {
		if (score >= 1 && score <= 4) {

			Item summaryItem = createItem(itemId, itemTitle, "");
			summaryItem.getText().setUpdateId(itemId + "Preview");
			if (score == 1 || score == 2) {
				arrOverallNeeds.add(summaryItem);
			} else {
				arrOverallStrengths.add(summaryItem);
			}
		}
	}

	/**
	 * checkOverallDerailers - Creates items for notes and risk factors
	 * 
	 * Note that this method is for the overall derailer ratings Cut points
	 * appear to be 7 and 8 for notes, 8 for risk factors
	 * 
	 * @param score
	 *            - The score to judge against
	 * @param rcKeyId
	 *            - The text key
	 */
	private void checkOverallDerailers(int score, String rcKeyId) {
		if (score >= 7 && score <= 9) {
			Item summaryItem;
			if (score == 7 || score == 8) {
				summaryItem = createItem("Note",
						reportContent.getKeyMapValue("leadershipDerailersStanine7Or8", rcKeyId));
			} else {
				summaryItem = createItem("Risk factor",
						reportContent.getKeyMapValue("leadershipDerailersStanine9", rcKeyId));
			}

			arrOverallDerailers.add(summaryItem);
		}
	}

	/**
	 * checkCompetenciesStrengthOrNeed - Separate Strengths from Needs for
	 * competencies
	 * 
	 * Note: The cutoffs: Strength 3.5 thru 5.0, Need - 1.0 thru 2.5
	 * 
	 * @param score
	 *            - The score to test against
	 * @param itemTitle
	 *            - The displayed name of the item
	 * @param rcStrengthsKeyMapId
	 *            - In the key map, the name of the "group" that strength texts
	 *            are in
	 * @param rcNeedsKeyMapId
	 *            - In the key map, the name of the "group" that need texts are
	 *            in
	 * @param rcKeyId
	 *            - The key of the text in the key map
	 */
	private void checkCompetenciesStrengthOrNeed(String score, String itemTitle, String rcStrengthsKeyMapId,
			String rcNeedsKeyMapId, String rcKeyId) {
		if (Double.parseDouble(score) >= 3.5) {
			arrCompetenciesStrengths.add(createItem(rcKeyId, itemTitle,
					reportContent.getKeyMapValue(rcStrengthsKeyMapId, rcKeyId)));
		} else if (Double.parseDouble(score) <= 2.5) {
			// The competency comes in with a value at or below 2.5... There is
			// a need
			arrCompetenciesNeeds.add(createItem(rcKeyId, itemTitle,
					reportContent.getKeyMapValue(rcNeedsKeyMapId, rcKeyId)));
		}
	}

	/**
	 * checkExperienceStrengthOrNeed - Check if the behavior is a strength or
	 * has a need
	 * 
	 * @param score
	 *            - The score to test
	 * @param itemTitle
	 *            - The title of the item in the report
	 * @param rcStrengthsKeyMapId
	 *            - The map section where the stength text resides
	 * @param rcNeedsKeyMapId
	 *            - The map section where the need text resides
	 * @param rcKeyId
	 *            - The id of the text to use/fetch
	 */
	private void checkExperienceStrengthOrNeed(int score, String itemTitle, String rcStrengthsKeyMapId,
			String rcNeedsKeyMapId, String rcKeyId) {
		if (score < 4)
			arrExperienceNeeds.add(createItem(rcKeyId, itemTitle,
					reportContent.getKeyMapValue(rcNeedsKeyMapId, rcKeyId)));
		else
			arrExperienceStrengths
					.add(createItem(itemTitle, reportContent.getKeyMapValue(rcStrengthsKeyMapId, rcKeyId)));
	}

	/**
	 * checkFoundationsStrengthOrNeed - Check the foundation items for strengths
	 * or needs
	 * 
	 * @param score
	 *            - The score to test
	 * @param itemTitle
	 *            - The display title of the item
	 * @param rcStrengthsKeyMapId
	 *            - The group that has the stregths texts
	 * @param rcNeedsLowKeyMapId
	 *            - The group that has the low needs texts
	 * @param rcNeedsHighKeyMapId
	 *            - The group that has the high needs texts
	 * @param rcKeyId
	 *            - The key of the appropriate text
	 */
	private void checkFoundationsStrengthOrNeed(int score, String itemTitle, String rcStrengthsKeyMapId,
			String rcNeedsLowKeyMapId, String rcNeedsHighKeyMapId, String rcKeyId) {
		if (score < 3)
			arrFoundationsNeeds.add(createItem(rcKeyId, itemTitle,
					reportContent.getKeyMapValue(rcNeedsLowKeyMapId, rcKeyId)));
		else if (score > 7)
			arrFoundationsNeeds.add(createItem(rcKeyId, itemTitle,
					reportContent.getKeyMapValue(rcNeedsHighKeyMapId, rcKeyId)));
		else
			// optimal
			arrFoundationsStrengths.add(createItem(itemTitle,
					reportContent.getKeyMapValue(rcStrengthsKeyMapId, rcKeyId)));
	}

	/**
	 * checkInterestStrengthOrNeed - Check the interest items for strengths or
	 * needs
	 * 
	 * @param score
	 *            - The score to test
	 * @param itemTitle
	 *            - The display title of the item
	 * @param rcStrengthsKeyMapId
	 *            - The group that has the strengths texts
	 * @param rcNeedsLowKeyMapId
	 *            - The group that has the low needs texts
	 * @param rcNeedsHighKeyMapId
	 *            - The group that has the high needs texts
	 * @param rcKeyId
	 *            - The key of the appropriate text
	 */
	// Make sure this is coordinated with the stuff in
	// addTraitDevSuggsAndIaLinks()
	private void checkInterestStrengthOrNeed(int score, String itemTitle, String rcStrengthsKeyMapId,
			String rcNeedsLowKeyMapId, String rcNeedsHighKeyMapId, String rcKeyId) {
		int band = OPTIMAL; // Default is optimal
		if (rcKeyId.equals("learningOrientation") || rcKeyId.equals("experienceOrientation")) {
			if (score < 3)
				band = TOO_LOW;
			else if (score > 7)
				band = TOO_HIGH;
		} else if (rcKeyId.equals("leadershipAspiration")) {
			if (score < 4)
				band = TOO_LOW;
			else if (score > 8)
				band = TOO_HIGH;
		} else if (rcKeyId.equals("careerDrivers")) {
			if (score < 4)
				band = TOO_LOW;
			// there is no "too high" band for career drivers
		}

		if (band == TOO_LOW)
			arrInterestNeeds.add(createItem(rcKeyId, itemTitle,
					reportContent.getKeyMapValue(rcNeedsLowKeyMapId, rcKeyId)));
		else if (band == TOO_HIGH)
			arrInterestNeeds.add(createItem(rcKeyId, itemTitle,
					reportContent.getKeyMapValue(rcNeedsHighKeyMapId, rcKeyId)));
		else
			arrInterestStrengths.add(createItem(rcKeyId, itemTitle,
					reportContent.getKeyMapValue(rcStrengthsKeyMapId, rcKeyId)));
	}

	/**
	 * checkDerailers - Check the interest items for strengths or needs
	 * 
	 * @param score
	 *            - The score to test
	 * @param itemTitle
	 *            - The display title of the item
	 * @param rcNeedsKeyMapId
	 *            - The group that has the needs texts
	 * @param rcNotesKeyMapId
	 *            - The group that has the notes texts
	 * @param rcKeyId
	 *            - The key of the appropriate text
	 */
	private void checkDerailers(int score, String itemTitle, String rcNeedsKeyMapId, String rcNotesKeyMapId,
			String rcKeyId) {
		if (score > 8) {
			// Score = 9
			arrDerailersStanine9.add(createItem(rcKeyId, itemTitle,
					reportContent.getKeyMapValue(rcNeedsKeyMapId, rcKeyId)));
		} else if (score > 6) {
			// score = 7 or 8
			arrDerailersStanine7Or8.add(createItem(rcKeyId, itemTitle,
					reportContent.getKeyMapValue(rcNotesKeyMapId, rcKeyId)));
		}
		// No strengths or optimal items for derailers
	}

	/**
	 * getRangeString - Returns a key for a participant's results
	 * ("ClearStrength" to "SustantialGap")
	 * 
	 * NOTE: If changes are made here, they mus also be reflected in the
	 * rangeToNumber() function in
	 * scorecard-ejb/.../scorecard/extract/GpsExtractBean
	 * 
	 * @param index
	 *            - The value (1 to 4) of the participants standing in the
	 *            results
	 * @return The appropriate key value
	 */
	private String getRangeString(int index) {
		String result = "";
		switch (index) {
		case 1:
			result = "SubstantialGap";
			break;
		case 2:
			result = "SlightGap";
			break;
		case 3:
			result = "Strength";
			break;
		case 4:
			result = "ClearStrength";
			break;
		default:
			log.warn(
					"LvaMllRgrToRcmBean.getRangeString() has an invalid input parameter.  Allowed values= 1-4, input value={}",
					index);
			break;
		}

		return result;
	}

	/**
	 * getRangeLabel - Returns a 4-value label for a participant's results
	 * ("Clear Strength" to "Sustantial Gap")
	 * 
	 * @param index
	 *            - The value (1 to 4) of the participants standing in the
	 *            results
	 * @return The appropriate label
	 */
	private String getRangeLabel(int index) {
		String result = "";
		switch (index) {
		case 1:
			result = "Substantial Gap";
			break;
		case 2:
			result = "Slight Gap";
			break;
		case 3:
			result = "Strength";
			break;
		case 4:
			result = "Clear Strength";
			break;
		}

		return result;
	}

	/**
	 * getReadinessLabel - Get a label for the readiness score
	 * 
	 * @param index
	 *            - The readiness score
	 * @return The label string associated with the label
	 */
	private String getReadinessLabel(int index) {
		String label;
		switch (index) {
		case 4:
			label = REDI_NOW;
			break;
		case 3:
			label = REDI_1_2;
			break;
		case 2:
			label = REDI_3_5;
			break;
		case 1:
			label = REDI_DEV_IN_PLACE;
			break;
		default:
			label = "";
			log.warn("Invalid readiness score; value={}", index);
			break;
		}

		return label;
	}

	private String getFitLabel(int index) {
		String label;
		switch (index) {
		case 4:
			label = STRONG_REC;
			break;
		case 3:
			label = REC;
			break;
		case 2:
			label = REC_RES;
			break;
		case 1:
			label = NOT_REC;
			break;
		default:
			label = "";
			log.warn("Invalid fit score; value={}", index);
			break;
		}

		return label;
	}

	/**
	 * getDerailerLabel - Get a label for the derailer score
	 * 
	 * NOTE that Big is good but sounds low
	 * 
	 * @param index
	 *            - The derailer score
	 * @return The label string associated with the score
	 */
	private String getDerailerLabel(int index) {
		String label;
		switch (index) {
		case 4:
			label = MINIMAL;
			break;
		case 3:
			label = LOW;
			break;
		case 2:
			label = MODERATE;
			break;
		case 1:
			label = HIGH;
			break;
		default:
			label = "";
			log.warn("Invalid derailer score; value={}", index);
			break;
		}

		return label;
	}

	/**
	 * getApLabel - Get a label for the Advancement potential score
	 * 
	 * @param index
	 *            - The advancement potential score
	 * @return The label string associated with the label
	 */
	private String getApLabel(int index) {
		String label;
		switch (index) {
		case 4:
			label = VERY_STRONG;
			break;
		case 3:
			label = STRONG;
			break;
		case 2:
			label = MIXED;
			break;
		case 1:
			label = WEAK;
			break;
		default:
			label = "";
			log.warn("Invalid advancement potential score; value={}", index);
			break;
		}

		return label;
	}

	/**
	 * createText - A convenience method that takes a String as input and
	 * creates a Text object with the "editable" attribute set false and the
	 * "visible" attribute set true.
	 * 
	 * @param content
	 *            - The display string
	 * @return A Text object
	 */
	private Text createText(String content) {
		return createText(content, false, true);
	}

	/**
	 * createText - A convenience method that takes a String as input and
	 * creates a Text object with the "editable" attribute set true and the
	 * "visible" attribute set true.
	 * 
	 * @param content
	 *            - The display string
	 * @return A Text object
	 */
	private Text createEditableText(String content) {
		return createText(content, true, true);
	}

	/**
	 * createText - A method that create a Text object. Requires all attributes
	 * and content be passed.
	 * 
	 * @param content
	 *            - The display string
	 * @param editable
	 *            - A boolean used for the "editable" attribute
	 * @param visible
	 *            - A boolean used for the "visible" attribute
	 * @return A Text object
	 */
	private Text createText(String content, Boolean editable, Boolean visible) {
		Text text = new Text();
		text.setContent(content);
		text.setEditable(editable);
		text.setVisible(visible);

		return text;
	}

	/**
	 * createTextSection - Creates a TextSection object from the input
	 * parameters
	 * 
	 * @param title
	 *            - The section title
	 * @param content
	 *            - The section content (descriptive text)
	 * @returns The create TextSectyion object
	 */
	private TextSection createTextSection(String title, String content) {
		TextSection textSection = new TextSection();
		textSection.setTitle(title);
		Text textSectionText = createText(content);
		textSection.getText().add(textSectionText);

		return textSection;
	}

	/**
	 * createTextSectionTwoItems - Creates a TextSection object with two content
	 * sections from the input parameters
	 * 
	 * @param title
	 *            - The section title
	 * @param content1
	 *            - The first piece of section content
	 * @param content2
	 *            - The second piece of section content
	 * @returns The created TextSection object
	 */
	private TextSection createTextSectionTwoItems(String title, String content1, String content2) {
		TextSection textSection = new TextSection();
		textSection.setTitle(title);
		Text textSectionText1 = createText(content1);
		textSection.getText().add(textSectionText1);
		Text textSectionText2 = createText(content2);
		textSection.getText().add(textSectionText2);

		return textSection;
	}

	/**
	 * createEntry - Creates an Entry object
	 * 
	 * @param color
	 *            - The color to be displayed for the entry
	 * @param content
	 *            - The descriptive content for the entry
	 * @returns The created TextSection object
	 */
	private Entry createEntry(String color, String content) {
		Entry entry = new Entry();
		entry.setColor(color);
		entry.setContent(content);
		return entry;
	}

	/**
	 * createItem - Creates an Item object
	 * 
	 * @param title
	 *            - The title of the item
	 * @param content
	 *            - The descriptive content for the item
	 * @returns The created Item object
	 */
	private Item createItem(String title, String content) {
		Item item = new Item();
		item.setTitle(title);
		item.setText(createText(content));
		return item;
	}

	/**
	 * createItem - Creates an Item object
	 * 
	 * @param id
	 *            - The internal id of the item
	 * @param title
	 *            - The title of the item
	 * @param content
	 *            - The descriptive content for the item
	 * @returns The created Item object
	 */
	private Item createItem(String id, String title, String content) {
		Item item = new Item();
		item.setId(id);
		item.setTitle(title);
		item.setText(createText(content));
		return item;
	}

	/**
	 * createCompetency - Creates an Competency object
	 * 
	 * @param pdiRating
	 *            - The PDINH rating for the competency
	 * @param content
	 *            - The descriptive content for the competency
	 * @returns The created Item object
	 */
	private Competency createCompetency(String pdiRating, String compKey, String content) {
		Competency competency = new Competency();
		// log.debug("PDI RATING for " + content + " is " + pdiRating +
		// " (float)");
		// competency.setPdiRating(formatDecimal(pdiRating)); //function
		// commented out below
		// log.debug("PDI RATING for " + content + " is " +
		// competency.getPdiRating() + " (formatDecimal (old method))");
		competency.setPdiRating(pdiRating);
		competency.setCompKey(compKey);
		competency.setContent(content);
		return competency;
	}

	/**
	 * formatDecimal - Format a floating poing number to a String
	 * 
	 * @param value
	 *            - The floating point number to be formatted
	 * @returns The formatted String
	 */
	private String roundToFiveTenths(float val) {
		float f = val * 2;
		int r = Math.round(f);
		double rounded = ((double) r) / 2;
		// log.debug(f + "rounded = " + r + " divided by 2 = " + rounded);
		// log.debug("PDI RATING for " + content + " is " + rounded +
		// " (ROUNDED  .5 (new method))");
		return String.valueOf(rounded);
	}

	// private String formatDecimal(float value) {
	// DecimalFormat myFormatter = new DecimalFormat("###.0");
	// // Make sure correct rounding mode
	// myFormatter.setRoundingMode(RoundingMode.UP);
	// return myFormatter.format(value);
	// }

	/**
	 * createCharacteristic - method to create a RCM Characteristic object This
	 * was created instead of creating a 2 parameter constructor because the
	 * objects were generated and the generator doesn't do constructors.
	 * 
	 * @param text
	 *            - the name of the characteristic (goes into "content")
	 * @param tltChar
	 *            - the characteristic (goes into "stanine")
	 * @return A Characteristic object
	 */
	private Characteristic createCharacteristic(String text, TLTCharacteristic tltChar) {
		Characteristic ch = new Characteristic();
		ch.setStanine(BigInteger.valueOf(characteristicScores.get(tltChar)));
		ch.setContent(text);
		ch.setType(characteristicTypes.get(tltChar));
		ch.setRgrId(tltChar.rgrId() + "");

		return ch;
	}

	/**
	 * createHtmlSection - Creates an HtmlSection object
	 * 
	 * @param id
	 *            - The internal id of the item
	 * @param title
	 *            - The title of the item
	 * @param content
	 *            - The descriptive content for the item
	 * @returns The created HtmlSection object
	 */
	private HtmlSection createHtmlSection(String id, String title, String content) {
		HtmlSection hs = new HtmlSection();
		hs.setId(id);
		hs.setTitle(title);
		hs.setText(createText(content));
		return hs;
	}

	/**
	 * marshalOutput - convert the RCM object stack to a string
	 * 
	 * @param lvaDbReport
	 *            - The RCM object stack
	 * @return The RCM string
	 */
	private String marshalOutput(LvaDbReport lvaDbReport) {
		JAXBContext jc = null;
		Marshaller m = null;
		StringWriter sw = new StringWriter();
		try {
			jc = JAXBContext.newInstance(LvaDbReport.class);
			m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(lvaDbReport, sw);

			return sw.toString();
		} catch (JAXBException jbe) {
			log.error("LvaMllRgrToRcmBean.marshalOutput() - JAXBException: Msg={}", jbe.getMessage());
			jbe.printStackTrace();
			return null;
		}
	}

	public ReportContent getAboutContent() {
		return aboutContent;
	}

	public void setAboutContent(ReportContent aboutContent) {
		this.aboutContent = aboutContent;
	}

	public ReportContent getAppendixContent() {
		return appendixContent;
	}

	public void setAppendixContent(ReportContent appendixContent) {
		this.appendixContent = appendixContent;
	}

}
