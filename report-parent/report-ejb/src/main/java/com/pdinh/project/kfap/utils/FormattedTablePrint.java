package com.pdinh.project.kfap.utils;

public class FormattedTablePrint {

	public static void printRow(int[] row) {
        for (int i : row) {
            System.out.print(i);
            System.out.print("\t");
        }
        System.out.println();
    }

    public static void print(int[][] table) {
        int i,j,k=1;

        for(i=0;i<table.length;i++) {
            for(j=0;j<7;j++) {
                table[i][j]=k;
                k++;
            }
        }
        for(int[] row : table) {
            printRow(row);
        }
    }

}
