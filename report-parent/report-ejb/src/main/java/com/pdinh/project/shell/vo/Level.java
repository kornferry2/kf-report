/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.shell.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/*
* This is the Level object used for IbReport only.
*/

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "level")
public class Level
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute
	private String name;
	
	private List<String> point;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Level()
	{
		// Default constructor
	}

	// 1-parameter constructor (clone)
	public Level(Level orig)
	{
		this.name = orig.getName() == null ? null : new String(orig.getName());
		if (orig.point == null)
		{
			this.point = null;
		}
		else
		{
			this.point = new ArrayList<String>();
			for(String str : orig.getPoint())
			{
				this.point.add(new String(str));
			}
		}
	}

	// 1-parameter constructor
	public Level(String name)
	{
		this.name = name;
	}
	
	//
	//
	// Instance methods.
	//
	
	public String toString()
	{
		String str = "Level:  name=" + this.name;
		if(point == null)
		{
			str += "  No points  ";
		}
		else
		{
			for (String st : this.point)
			{
				str += "  point=" + st + "  ";
			}
		}
		
		return str;
	}

	// Getters & setters

	//----------------------------------
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String value)
	{
		this.name = value;
	}

	//----------------------------------
	public List<String> getPoint()
	{
		if (this.point == null)
			this.point = new ArrayList<String>();

		return this.point;
	}
}
