/**
* Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
* All rights reserved.
*/
package com.pdinh.project.lva.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/*
 * Competency object used for the scored section of lvaDbReport only.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "competency")
public class Competency
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute
	private float pdiRating;
	@XmlAttribute
	private String type;
	@XmlValue
	private String name;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Competency()
	{
		// Default constructor
	}

	// 1-parameter constructor (clone)
	public Competency(Competency orig)
	{
		this.pdiRating = orig.pdiRating;
		this.name = orig.getName() == null ? null : new String(orig.getName());
	}

	// 2-parameter constructor
	public Competency(float rating, String name)
	{
		this.pdiRating = rating;
		this.name = name;
	}

	// 3-parameter constructor
	public Competency(float rating, String type, String name)
	{
		this.pdiRating = rating;
		this.type = type;
		this.name = name;
	}
	
	//
	// Instance methods.
	//
	
	public String toString()
	{
		String str = "Competency:  PDI rating=" + this.pdiRating + ", type=" + this.type + ", name=" + this.name;
		
		return str;
	}

	// Getters & setters

	//----------------------------------
	public float getPdiRating()
	{
		return this.pdiRating;
	}
	
	public void setPdiRating(float value)
	{
		this.pdiRating = value;
	}

	//----------------------------------
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String value)
	{
		this.name = value;
	}
}
