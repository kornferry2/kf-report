/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.shell.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/*
 *  Used in IbReport
 *  
 *  NOTE:	The "key" attribute is created in scoring and is used to find back the dev sug
 *  		and the module entries.  It is not present and cannot be relied upon if the
 *  		Suggestion object is built in any other fashion or if this is part of an
 *  		IbReport object unmarshalled from XML (it does not exist in the xml).
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Module
{
	//
	// Static data.
	//
 
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute
	private String key;
	private String displayText;
	private String iaUrl;
	
	//
	// Constructors.
	//

	// 0-parameter constructor
	public Module()
	{
		// Default constructor
		//this.selected = true;	// Default creation state is "true"
	}

	// 1-parameter constructor (clone)
	public Module(Module orig)
	{
		this.key = orig.getKey() == null ? null : new String(orig.getKey());
		this.displayText = orig.getDisplayText() == null ? null : new String(orig.getDisplayText());
		this.iaUrl = orig.getIaUrl() == null ? null : new String(orig.getIaUrl());
	}

	// 2-parameter constructor
	public Module(String text, String url)
	{
		this.displayText = text;
		this.iaUrl = url;
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "IA Mod:  ";
		//str += "key=" + this.key + ", mid=" + this.mid + ", title=" + this.title;
		str += "key=" + this.key + ", dispText=" + this.displayText + ", url=" + this.iaUrl;

		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getDisplayText()
	{
		return this.displayText;
	}

	public void setDisplayText(String value)
	{
		this.displayText = value;
	}

	//----------------------------------
	public String getIaUrl()
	{
		return this.iaUrl;
	}

	public void setIaUrl(String value)
	{
		this.iaUrl = value;
	}

	//----------------------------------
	public String getKey()
	{
		return this.key;
	}

	public void setKey(String value)
	{
		this.key = value;
	}
}
