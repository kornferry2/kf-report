/**
* Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
* All rights reserved.
*/
package com.pdinh.project.shell.utils;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.pdinh.pipeline.dto.StatusDTO;

/*
 * Utility class for the Shell project
 * 
 * Once we figure it out, we may want to move some of this stuff out into general purpose utilities.
 */
public class ShellUtils
{
	//
	// Static data.
	//
	
	// Note that validate type strings must match the name of the .xsd files in the schemas folder
	public static String RQR_TYPE = "ReportGenerationRequest";
	public static String SHELL_LAR_RCM_TYPE = "ShellLARReportContentModel";
	
	private static String SCHEMA_LANGUAGE = "http://www.w3.org/2001/XMLSchema";

	//
	// Static methods.
	//
	
	/**
	 * Convenience validator that takes an XML Sring object
	 * 
	 * @param ceResources The path to the CalculationEngineResource folder
	 * @param schemaType - Typename for the schema
	 * @param xml - The xml to validate
	 * @returns A StatusDTO object
	 */
	public static StatusDTO validateXml(String ceResources, String schemaType, String xml)
	{
		if (xml == null)
		{
			String errMsg = "validateXml:  Null XML string";
			System.out.println(errMsg);
			StatusDTO ret = new StatusDTO();
			ret.setFailure();
			ret.setErrorMsg(errMsg);
			return ret;
		}
		
		return doXmlValidation(ceResources, schemaType, new StreamSource(new StringReader(xml)));
	}


	/*
	 * Convenience validator that takes an XML File object
	 * 
	 * @param ceResources The path to the CalculationEngineResource folder
	 * @param schemaType - Typename for the schema
	 * @param xml - The xml to validate
	 * @returns A StatusDTO object
	 */
	public static StatusDTO validateXml(String ceResources, String schemaType, File xml)
	{
		String errMsg;
		StatusDTO ret = new StatusDTO();
		
		if (xml == null)
		{
			errMsg = "validateXml:  No XML File object";
			System.out.println(errMsg);
			ret.setFailure();
			ret.setErrorMsg(errMsg);
			return ret;
		}
		
		if (! xml.exists())
		{
			errMsg = "XML file (" + xml.getAbsolutePath() + ") does not exist";
			System.out.println(errMsg);
			ret.setFailure();
			ret.setErrorMsg(errMsg);
			return ret;
			
		}

		return doXmlValidation(ceResources, schemaType, new StreamSource(xml));
	}


	/*
	 * Generalized XML validation routine
	 * 
	 * From here:  http://www.java-tips.org/java-se-tips/javax.xml.validation/how-to-create-xml-validator-from-xml-s.html
	 * Or here:  http://www.ibm.com/developerworks/xml/library/x-javaxmlvalidapi/index.html  <-- this is good; has more info
	 * 
	 * @param ceResources The path to the CalculationEngineResource folder
	 * @param schemaType - Typename for the schema
	 * @param xmlStream - The input data in a StreamSource object
	 * @returns A StatusDTO object
	 */
	private static StatusDTO doXmlValidation(String ceResources, String schemaType, Source xmlStream)
	{
		StatusDTO ret = new StatusDTO();

		Schema schema = null;
		
		File schemaFile = new File(ceResources + "schemas/" + schemaType + ".xsd");
		//// Debug -------------
		//try
		//{
		//	System.out.println("  Abs   - " + schemaFile.getAbsolutePath());
		//	System.out.println("  Canon - " + schemaFile.getCanonicalPath());
		//}
		//catch (IOException e) { /* Ignore me */ }
		//// -------------------
		
		// ...and see if it is there
		if (! schemaFile.exists())
		{
			ret.setFailure();
			ret.setErrorMsg("Schema file not found at " + ceResources + "schemas/" + schemaType + ".xsd");
			return ret;
		}
		
		// Do the validation
		// 1. Lookup a factory for the W3C XML Schema language
		SchemaFactory factory = SchemaFactory.newInstance(SCHEMA_LANGUAGE);

		// 2. Compile the schema. 
		try
		{
			schema = factory.newSchema(schemaFile);
		}
		catch(SAXException e)
		{
			ret.setFailure();
			ret.setErrorMsg("Unable to create Schema instance...  type=" + schemaType + ", msg=" + e.getMessage());
			return ret;
		}

		// 3. Get a validator from the schema and set up its error handling.
		Validator validator = schema.newValidator();
		validator.setErrorHandler(new ShellUtils().new ValidationErrorHandler()); 

		// 4. Check the document
		try
		{
			validator.validate(xmlStream);
			//System.out.println(xmlDoc + " is valid.");
		}
		catch (IOException ex)
		{
			ret.setFailure();
			ret.setErrorMsg("IO error validating XML.  msg=" + ex.getMessage());
			return ret;
		}
		catch (SAXException ex)
		{
			ret.setFailure();
			ret.setErrorMsg("Validation error.  msg=" + ex.getMessage());
			return ret;
		}  
		
		return ret;
	}
	

	/*
	 * Validation Error handler.  Tell us about Warnings, but kick out on
	 * parse errors and fatal errors
	 */
	private class ValidationErrorHandler implements ErrorHandler
	{
	    public void warning(SAXParseException ex)
	    {
	        System.err.println("Validator Parse Warning:  " + ex.getMessage());
	    }

	    public void error(SAXParseException ex)
	    	throws SAXException
	    {
	    	String err = "Validator Parse error:  " + ex.getMessage();
	        System.err.println(err);
	        throw new SAXException(err, ex);
	    }

	    public void fatalError(SAXParseException ex)
	    	throws SAXException
	    {
	    	String err = "Validator Fatal error:  " + ex.getMessage();
	        System.err.println(err);
	        throw new SAXException(err, ex);
	    }

	} 

	//
	// Instance data.
	//

	//
	// Instance methods.
	//
	//

}
