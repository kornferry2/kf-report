/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/*
* DevSection object used for the development section of LvaDbReport only.
*/

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder={"title", "text", "devElement"})
public class DevSection
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String title;
	private String text;	// Plain text message
	@XmlElementWrapper(name = "devElements")
	private List<DevElement> devElement;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public DevSection()
	{
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "DevSection:  ";
		str += "title=" + this.title +  "\n";
		str += "              text=" + this.text + "\n";
		if (devElement != null)
		{
			for (DevElement de : this.devElement)
			{
				str += "\n      " + de.toString();
			}
		}
		
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getTitle()
	{
		return this.title;
	}

	public void setTitle(String value)
	{
		this.title = value;
	}

	//----------------------------------
	public String getText()
	{
		return this.text;
	}

	public void setText(String value)
	{
		this.text = value;
	}

	//----------------------------------
	// Note that the list has a "get" only
	public List<DevElement> getDevElement()
	{
		if (this.devElement == null)
			this.devElement = new ArrayList<DevElement>();
		
		return this.devElement;
	}

}
