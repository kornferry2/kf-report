package com.pdinh.project.kfap;

import java.io.StringWriter;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.fasterxml.jackson.databind.JsonNode;
import com.kf.project.kfap.reports.jaxb.AssessmentDetailsSectionV2;
import com.kf.project.kfap.reports.jaxb.BackgroundListItem;
import com.kf.project.kfap.reports.jaxb.CapacitySection;
import com.kf.project.kfap.reports.jaxb.CategoryItem;
import com.kf.project.kfap.reports.jaxb.ChallengeListItem;
import com.kf.project.kfap.reports.jaxb.DerailmentSection;
import com.kf.project.kfap.reports.jaxb.DevelopmentPrioritiesSection;
import com.kf.project.kfap.reports.jaxb.DriversSection;
import com.kf.project.kfap.reports.jaxb.ExpTable;
import com.kf.project.kfap.reports.jaxb.ExpTableCategory;
import com.kf.project.kfap.reports.jaxb.ExperienceSection;
import com.kf.project.kfap.reports.jaxb.GoalsObjectivesList;
import com.kf.project.kfap.reports.jaxb.IdealRoleList;
import com.kf.project.kfap.reports.jaxb.KeyLeadershipChallenges;
import com.kf.project.kfap.reports.jaxb.KfapIndividualFeedbackReportV2;
import com.kf.project.kfap.reports.jaxb.LeadershipTraitsSection;
import com.kf.project.kfap.reports.jaxb.LearningAgilitySection;
import com.kf.project.kfap.reports.jaxb.Level;
import com.kf.project.kfap.reports.jaxb.OverviewSection;
import com.kf.project.kfap.reports.jaxb.ParticipantName;
import com.kf.project.kfap.reports.jaxb.Priority;
import com.kf.project.kfap.reports.jaxb.SectionText;
import com.kf.project.kfap.reports.jaxb.SelfAwareSection;
import com.kf.project.kfap.reports.jaxb.SubSection;
import com.kf.project.kfap.reports.jaxb.SubSectionText;
import com.kf.project.kfap.reports.jaxb.SummarySection;
import com.pdinh.data.ms.dao.PositionDao;
import com.pdinh.data.ms.dao.ProjectCourseDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.ProjectCourse;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.pipeline.generator.vo.Participant;
import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.project.kfap.language.LangFactory;
import com.pdinh.project.kfap.language.LangReplacement;
import com.pdinh.project.kfap.utils.ContentRetriever;
import com.pdinh.project.kfap.utils.KfapUtils;
import com.pdinh.project.kfap.utils.RgrRetriever;
import com.pdinh.project.kfap.utils.SortDevPrioritiesV2;
import com.pdinh.report.util.XmlUtil;

@Stateless
@LocalBean
public class KfapReportRgrToRcmV2 {
	private static final Logger log = LoggerFactory.getLogger(KfapReportRgrToRcmV2.class);

	@Resource(name = "application/report/config_properties", type = Properties.class)
	private Properties configProperties;

	private JsonNode jsonRptContent = null;
	private String jsonAsmtContent = "";

	private String longDateFormat;
	private String strDate = "";
	private String strCreateDate = "";
	private String createDate = "";
	// private String strDateCompleted = "";
	private String strDateCompletedForP1 = "";

	private KfapIndividualFeedbackReportV2 rpt;

	// These language code reflect the ISO standard ("cc_vv") rather than the
	// format used in the CMS ("cc-vv")
	private String[] langCodes = { "en", "zh_CN", "ko", "ja", "fr", "es", "de", "ru", "pt_BR", "en_GB", "tr" };
	// private String[] langCodes = { "en", "zh_CN", "ko", "ja", "fr", "es",
	// "de", "ru", "pt_BR", "en_GB" };
	// private String[] langCodes = { "en", "ko", "zh_CN", "en_GB", "fr", "es",
	// "it", "de", "pt_BR", "sv", "ja", "pl",
	// "ru", "nl", "id", "cs", "hu", "no", "ko", "tr" };

	// store rgr2 id and its mapped text
	private Map<String, String> varTextWrapper = new HashMap<String, String>();

	private final String MANIFEST = "kfpManifestV2";
	private final String KFP_INST = "kfp";
	private final String DATA_SET = "resume";
	private final String REQUEST_STRING = "jaxrs/mappedCalculationRequest";
	// private final String CONTENT_VER = "v1";
	// private final String REPORT_VER = "V2";
	// private final String REPORT_TYPE = "individual";
	// private final String ASSESSMENT_CODE = "kfp-grouped";
	// private final String FORMAT = "json";
	private final String DEFAULT_LANG = "en-US";
	private final String CONTENT_FOLDER = "feedback";

	// getListItem return type
	private final int STR_ARR = 1;
	private final int INT_SUM = 2;
	private final int ONE = 1;

	// no ravens taken
	boolean NO_RAVENS = false;

	// store individual item text and item value matched
	private String vTgtRole = "";
	private String vLTAspiration = "";
	private String vDateComplete = "";
	private String vClient = "";
	private String vFirstName = "";
	private String vLastName = "";

	private List<String> ROLE_LEVEL_LIST = null;
	private List<String> ROLE_TYPE_LIST = null;
	private List<String> ORG_TYPE_LIST = null;
	private List<String> ORG_TYPE_LIST_CURRENT = null;
	private List<String> ORG_SIZE_LIST = null;
	private List<String> IND_TYPE_LIST = null;
	private List<String> IND_TYPE_LIST_CURRENT = null;
	private List<String> FUNCT_AREA_LIST = null;
	private List<String> FUNCT_AREA_LIST_CURRENT = null;
	private List<String> KEY_CHALLENGE_LIST = null;
	private List<String> IDEAL_ROLE_LIST = null;

	private int tgtRole; // from project setup
	private int currOrgType; // ITM1
	private int yrsInCurrOrg; // ITM2
	private int currRoleLevel; // ITM3
	private int currRoleType; // ITM4
	private int yrsInCurrRole; // ITM5
	private int currOrgSize; // ITM6
	private int currIndustry; // ITM7
	private int currFunctArea; // ITM9
	private int yrsInWorkforce; // ITM12
	private int yrsInManagement; // ITM13
	private int yrsOnBoard; // ITM14
	private int nLevels;

	// aVariable only use to match an element of an arrayList
	// they always equal to int variable - ONE
	private int aTgtRole;
	private int aCurrRoleLevel;
	private int aLtAspiration;
	private int aCurrRoleType;
	private int aCurrIndustry;
	private int aCurrFunctArea;
	private int aCurrOrgType;
	private int aCurrOrgSize;

	// CAREER GOALS & OBJECTIVES
	private int carPlan; // ITM127

	private int threeToFiveYrsGoal;
	private int threeToFiveYrsRoleLevel;

	private int ltAspiration; // ITM130 aspire_role_level
	private int nOrgs; // ITM24
	private int nCountries; // ITM79

	// scores suppose to get from input
	private int driversScore; // 1 or 0
	private int experienceScore; // 1 or 0
	private int awarenessScore; // 1 or 0
	private int agilityScore; // 1 or 0
	private int traitsScore; // 1 or 0
	private int capacityScore; // 1 or 0
	private int derailerScore; // 1 or 0

	// private int highestRole; // HighestRole
	private int nFunctAreas; // NFunctAreas
	// private int nOrgTypes; // NOrgTypes
	private int nIndustries; // Nindustries
	// private int nOrgSizes; // NOrgSizes
	private int rolePref; // RolePref pref_role_types
	private int functPref; // FunctPref
	private int orgPref; // OrgPref
	private int sizePref; // SizePref
	private int indPref; // IndPref
	private int nStrongPrefs; // NStrongPrefs

	private int pace;
	private int jump;
	private int driversAdvdrive; // interest drive for v1
	private int driversCareerplan; // interest focus for v1
	private int driversRolepref; // interest engagement for v1
	private int expCore;
	private int awareSlfAware;
	private int awareSitAware;
	private int lrnAgile;
	private int expPerspective;
	private int nKeyChallenges;
	private int expKeyChallenges;
	private int ldrTraits; // n_traits
	private int derailVolatile;
	private int derailMicro;
	private int derailClosed;

	private int driversAdvdrivePct; // was interest drive pct
	private int driversCareerplanPct; // was interest focus pct
	private int driversRoleprefPct; // was interest engagement pct
	private int awareSlfAwarePct;
	private int awareSitAwarePct;
	private int agilityChangePct;
	private int agilityResultsPct;
	private int agilityPeoplePct;
	private int agilityMentalPct;
	private int traitsFocusPct; // was focus v1
	private int traitsPersistPct; // ase traits social v1
	private int traitsAssertivePct; // was traits composed v1
	private int traitsOptimismPct;
	private int traitsTolerancePct;
	private int derailVolatilePct;
	private int derailMicroPct;
	private int derailClosedPct;
	private int capacityProSolvingPct; // is it a problem solving percentile
										// score?
	private int expPerspectivePct;
	private int expKeyChallengePct;
	private int expCorePct;

	// list
	private List<Integer> role_level_list = new ArrayList<Integer>();
	private List<Integer> role_type_list = new ArrayList<Integer>();
	private List<Integer> org_type_list = new ArrayList<Integer>();
	private List<Integer> org_size_list = new ArrayList<Integer>();
	private List<Integer> ind_type_list = new ArrayList<Integer>();
	private List<Integer> funct_area_list = new ArrayList<Integer>();
	private List<Integer> key_challenge_list = new ArrayList<Integer>();
	private List<Integer> ideal_role_list = new ArrayList<Integer>();
	private List<Integer> pref_org_type_list = new ArrayList<Integer>();
	private List<Integer> pref_org_size_list = new ArrayList<Integer>();
	private List<Integer> pref_ind_type_list = new ArrayList<Integer>();
	private List<Integer> pref_role_type_list = new ArrayList<Integer>();
	private List<Integer> pref_funct_area_list = new ArrayList<Integer>();

	@EJB
	private ProjectUserDao projectUserDao;

	@EJB
	private ProjectDao projectDao;

	@EJB
	private UserDao userDao;

	@EJB
	private PositionDao positionDao;

	@EJB
	private ProjectCourseDao projectCourseDao;

	@EJB
	private RgrRetriever rgrRetriever;

	@EJB
	private ContentRetriever contentRetriever;

	private LangReplacement langFactory;

	// construct
	public KfapReportRgrToRcmV2() {
	}

	/**************************************************************************************
	 * call to generate RCM
	 * 
	 * @param String
	 *            type - report type
	 * @param String
	 *            projId - project Id
	 * @param String
	 *            pid - participant Id
	 * @param String
	 *            language - language code
	 * @param String
	 *            targ - target role level
	 * 
	 * @return RCM XML String
	 **************************************************************************************/

	public String generateReportRcm(String type, String projId, String pid, String language, String targ)
			throws PalmsException {

		log.debug("RgrToRcmV2: type=" + type + " pid=" + pid + " targ=" + targ + " language=" + language);

		Document document;
		document = this.rgrRetriever.getRgr(type, REQUEST_STRING, KFP_INST, MANIFEST, pid, projId, targ);

		log.debug("RGR=" + XmlUtil.convertDocumentToXmlString(document));

		return this.generateReportRcm(document, type, projId, pid, language, targ);
	}

	public String generateReportRcm(Document scoreDoc, String type, String projId, String pid, String language,
			String targ) throws PalmsException {

		String lang = getLanguage(language);
		langFactory = LangFactory.getLanguage(lang);

		// extract report and assessment content from marklogic
		// jsonRptContent = this.contentRetriever.getContent(CONTENT_VER,
		// REPORT_VER, REPORT_TYPE, lang);
		// jsonAsmtContent =
		// this.contentRetriever.getAssessmentContent(ASSESSMENT_CODE,
		// lang.equals(DEFAULT_LANG) ? langCodes[0] : lang, FORMAT);

		jsonRptContent = this.contentRetriever.getReportContent(CONTENT_FOLDER, lang);
		jsonAsmtContent = this.contentRetriever.getAssessmentContent(lang);

		// populate list text from asmtContent

		mapListContent(jsonAsmtContent, lang);

		// date format
		longDateFormat = getDataFormat(lang);

		SimpleDateFormat longSdf = new SimpleDateFormat(longDateFormat);
		SimpleDateFormat longSdfForP1 = new SimpleDateFormat("MMMMM yyyy");
		strDate = longSdf.format(new Date());

		// Assessment Date
		// Business defined Assessment Date in the extract enhancements:
		// "date for each person (last possible finalization date)"
		// If no data, default to today

		List<ProjectCourse> pcs = projectCourseDao.findProjectCourseByProjectId(Integer.parseInt(projId));
		NO_RAVENS = true;
		for (ProjectCourse pc : pcs) {
			if (pc.getCourse().getAbbv().equals("rv2") && pc.getSelected()) {
				NO_RAVENS = false;
				break;
			}
		}

		List<Position> positions = positionDao.findPositionsByUsersIdAndProjectId(Integer.parseInt(pid),
				Integer.parseInt(projId));
		if (positions == null) {
			// strDateCompleted = strDate;
			log.debug("No valid Position rows found for ppt={}, proj={}, ... defaulting to todays date.", pid, projId);
		} else {
			// Look for the latest date
			int comp = 0;
			Timestamp ts = new Timestamp(0);
			for (Position pos : positions) {
				if (pos.getCompletionStatus() == Position.COMPLETE) {
					comp++;
					if (pos.getCompletionStatusDate() != null && pos.getCompletionStatusDate().after(ts)) {
						ts = pos.getCompletionStatusDate();
					}
				}
			}
			if (comp < 1) {
				// strDateCompleted = strDate;
				log.debug("No completed Position rows found for ppt={}, proj={} ... defaulting to todays date.", pid,
						projId);
			} else {
				long l = ts.getTime();
				// strDateCompleted = longSdf.format(new Date(l));
				strDateCompletedForP1 = longSdfForP1.format(new Date(l));
			}
		}

		ReportGenerationRequest rgr = new ReportGenerationRequest();
		Participant ppt = new Participant();
		rgr.getParticipant().add(ppt);
		ppt.setExtId(pid + "");

		com.pdinh.persistence.ms.entity.Project proj = projectDao.findById(Integer.parseInt(projId));

		tgtRole = Integer.parseInt(targ);
		User user = userDao.findById(Integer.parseInt(pid), true);
		vFirstName = user.getFirstname();
		vLastName = user.getLastname();

		ParticipantName part = new ParticipantName();
		part.setFirstName(vFirstName);
		part.setLastName(vLastName);

		mapScores(scoreDoc);

		try {
			mapRawResponseData(scoreDoc);
		} catch (Exception e) {
			log.info("Problem to fetch raw responses from payload!");
			e.printStackTrace();
			return null;
		}

		vTgtRole = ROLE_LEVEL_LIST.get(aTgtRole);
		vLTAspiration = ROLE_LEVEL_LIST.get(aLtAspiration);
		vDateComplete = xformMonthDate(strDateCompletedForP1, language);
		vClient = proj.getCompany().getCoName();

		// last thing to call is wrap a map to replace [xxx] variables
		varTextWrapper = wrapDisplayVars(lang);

		strCreateDate = this.contentRetriever.getRptContentTxt(jsonRptContent, "shared", "createDate");
		createDate = strCreateDate.replace("{todaysDate}", strDate);

		// get participant version from calculationPayload
		String pVer = new String(KfapUtils.getVersion4ParticipantAssessment(scoreDoc, KFP_INST, "version"));

		// set default participant version 2
		pVer = (pVer.equals("") || pVer == null) ? "2" : pVer.trim();

		return setReportAttributes(pid, part, pVer, lang);
	}

	private String setReportAttributes(String pId, ParticipantName pName, String pVer, String lang)
			throws PalmsException {

		int pVersion = Integer.parseInt(pVer);
		rpt = new KfapIndividualFeedbackReportV2();
		rpt.setLang(lang);
		String rcm = "";

		// set parent report object value
		log.debug("Report title: " + jsonRptContent.get("version").get("versionNumber").textValue());

		rpt.setReportName(this.contentRetriever.getRptContentTxt(jsonRptContent, "shared", "reportTitle"));
		rpt.setAssessmentTitle(this.contentRetriever.getRptContentTxt(jsonRptContent, "shared", "reportName"));
		rpt.setConfidential(this.contentRetriever.getRptContentTxt(jsonRptContent, "shared", "confidential"));
		rpt.setConfidentialWPeriod(this.contentRetriever.getRptContentTxt(jsonRptContent, "shared",
				"confidentialWPeriod"));
		rpt.setNameLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "shared", "nameLabel"));

		rpt.setClientLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "shared", "clientLabel"));
		rpt.setTargetLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "shared", "targetLabel"));
		rpt.setReportDateLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "shared", "reportDateLabel"));

		rpt.setClient(vClient);
		rpt.setTarget(vTgtRole);
		rpt.setReportDate(strDate);
		// rpt.setReportDate(longSdf.format(new Date()));
		// rpt.setReportDate(DateUtils.truncate(new Date(),
		// java.util.Calendar.DAY_OF_MONTH).toString());
		rpt.setSerialNumber(pId);
		rpt.setParticipantName(pName);
		rpt.setCopyright(this.contentRetriever.getRptContentTxt(jsonRptContent, "shared", "copyright"));
		rpt.setProvidedBy(this.contentRetriever.getRptContentTxt(jsonRptContent, "shared", "providedBy"));

		rpt.setCreateDate(createDate);

		if (NO_RAVENS) {
			rpt.setNoRavens(true);
		}

		rpt.setEndpageHeader(this.contentRetriever.getRptContentTxt(jsonRptContent, "shared", "endpageHeader"));
		rpt.setEndpageP1(this.contentRetriever.getRptContentTxt(jsonRptContent, "shared", "endpageP1"));
		rpt.setEndpageP2(this.contentRetriever.getRptContentTxt(jsonRptContent, "shared", "endpageP2"));
		rpt.setEndpageP3(this.contentRetriever.getRptContentTxt(jsonRptContent, "shared", "endpageP3"));

		rpt.setVersionNumber(this.contentRetriever.getRptContentTxt(jsonRptContent, "version", "versionNumber"));

		rpt.setDriversLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "preSection", "driversLabel"));
		rpt.setIsDriversStregth(getIsStrength(driversScore));
		rpt.setExperienceLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "preSection", "experienceLabel"));
		rpt.setIsExperienceStregth(getIsStrength(experienceScore));
		rpt.setSelfAwarenessLabel(this.contentRetriever
				.getRptContentTxt(jsonRptContent, "preSection", "awarenessLabel"));
		rpt.setIsSelfAwarenessStregth(getIsStrength(awarenessScore));
		rpt.setLearningAgilityLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "preSection",
				"learningAgilityLabel"));
		rpt.setIsLearningAgilityStregth(getIsStrength(agilityScore));
		rpt.setLeadershipTraitsLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "preSection",
				"leadershipTraitsLabel"));
		rpt.setIsLeadershipTraitsStregth(getIsStrength(traitsScore));

		if (!NO_RAVENS) {
			rpt.setCapacityLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "preSection", "capacityLabel"));
			rpt.setIsCapacityStregth(getIsStrength(capacityScore));
		}

		rpt.setDerailmentRisksLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "preSection",
				"derailmentRisksLabel"));
		rpt.setIsDerailmentRisksStregth(getIsStrength(derailerScore));

		// set report content for Overview Section
		OverviewSection overview = new OverviewSection();

		overview.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent, "overview", "title"));
		overview.setP1(langFactory.replaceContentVar(
				this.contentRetriever.getRptContentTxt(jsonRptContent, "overview", "p1"), varTextWrapper));
		overview.setP2(langFactory.replaceContentVar(
				this.contentRetriever.getRptContentTxt(jsonRptContent, "overview", "p2"), varTextWrapper));
		overview.setNeedsLabel(langFactory.replaceContentVar(
				this.contentRetriever.getRptContentTxt(jsonRptContent, "overview", "needsLabel"), varTextWrapper));
		overview.setStrengthLabel(langFactory.replaceContentVar(
				this.contentRetriever.getRptContentTxt(jsonRptContent, "overview", "strengthLabel"), varTextWrapper));
		overview.setStrengthIconFootnote(langFactory.replaceContentVar(
				this.contentRetriever.getRptContentTxt(jsonRptContent, "overview", "strengthIconFootnote"),
				varTextWrapper));
		overview.setDevNeedIconFootnote(langFactory.replaceContentVar(
				this.contentRetriever.getRptContentTxt(jsonRptContent, "overview", "devNeedIconFootnot"),
				varTextWrapper));

		rpt.setOverviewSection(overview);

		// set report content for Overview Section
		AssessmentDetailsSectionV2 assessmentDetails = new AssessmentDetailsSectionV2();

		assessmentDetails.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent, "assessmentDetailsSection",
				"title"));

		// previous is Interest section
		DriversSection driversSection = new DriversSection();

		driversSection.setLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "driversSection", "label"));
		driversSection.getSectionText().add(getDriversSectionPara("driversSection"));

		// previous is drive sub section
		// v1 participant show on V2 report logic
		if (pVersion == 1) {
			SubSection drive = new SubSection();
			drive.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent, "driveSubsection", "title"));
			drive.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent, "driveSubsection",
					"developmentText"));
			drive.setScore(new BigInteger(String.valueOf(roundPct(driversAdvdrivePct))));
			drive.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent, "driveSubsection", "strongText"));
			driversSection.getSubSection().add(getV1ParticipantDriveText(drive, "driveSubsection"));
		} else {
			SubSection advancementDriveSubsection = new SubSection();

			advancementDriveSubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent,
					"advancementDriveSubsection", "title"));
			advancementDriveSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent,
					"advancementDriveSubsection", "developmentText"));
			advancementDriveSubsection.setScore(new BigInteger(String.valueOf(roundPct(driversAdvdrivePct))));

			advancementDriveSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent,
					"advancementDriveSubsection", "strongText"));
			driversSection.getSubSection().add(
					getAdvancementDriveSubsectionText(advancementDriveSubsection, "advancementDriveSubsection"));
		}

		// previous is focus sub section
		SubSection careerPlanningSubsection = new SubSection();

		careerPlanningSubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"careerPlanningSubsection", "title"));
		careerPlanningSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"careerPlanningSubsection", "developmentText"));
		careerPlanningSubsection.setScore(new BigInteger(String.valueOf(roundPct(driversCareerplanPct))));
		careerPlanningSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"careerPlanningSubsection", "strongText"));
		driversSection.getSubSection().add(
				getCareerPlanningSubsectionText(careerPlanningSubsection, "careerPlanningSubsection"));

		// previous is engagement sub section
		SubSection rolePreferencesSubsection = new SubSection();

		rolePreferencesSubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"rolePreferencesSubsection", "title"));
		rolePreferencesSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"rolePreferencesSubsection", "developmentText"));
		rolePreferencesSubsection.setScore(new BigInteger(String.valueOf(roundPct(driversRoleprefPct))));
		rolePreferencesSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"rolePreferencesSubsection", "strongText"));
		driversSection.getSubSection().add(
				getRolePreferenceSubsectionText(rolePreferencesSubsection, "rolePreferencesSubsection"));
		assessmentDetails.setDriversSection(driversSection);

		// create experience section
		ExperienceSection experienceSection = new ExperienceSection();

		experienceSection
				.setLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "experienceSection", "label"));
		experienceSection.getSectionText().add(getExperienceSectionPara("experienceSection"));
		SubSection coreExperienceSubsection = new SubSection();
		coreExperienceSubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"coreExperienceSubsection", "title"));
		coreExperienceSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"coreExperienceSubsection", "developmentText"));
		coreExperienceSubsection.setScore(new BigInteger(String.valueOf(roundPct(expCorePct))));
		coreExperienceSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"coreExperienceSubsection", "strongText"));
		experienceSection.getSubSection().add(
				getCoreExperienceSubsectionText(coreExperienceSubsection, "coreExperienceSubsection"));

		SubSection perspectiveSubsection = new SubSection();

		perspectiveSubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent, "perspectiveSubsection",
				"title"));
		perspectiveSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"perspectiveSubsection", "developmentText"));
		perspectiveSubsection.setScore(new BigInteger(String.valueOf(roundPct(expPerspectivePct))));
		perspectiveSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"perspectiveSubsection", "strongText"));
		experienceSection.getSubSection().add(
				getPerspectiveSubsectionText(perspectiveSubsection, "perspectiveSubsection"));

		SubSection keyChallengesSubsection = new SubSection();

		keyChallengesSubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"keyChallengeSubsection", "title"));
		keyChallengesSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"keyChallengeSubsection", "developmentText"));
		keyChallengesSubsection.setScore(new BigInteger(String.valueOf(roundPct(expKeyChallengePct))));
		keyChallengesSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"keyChallengeSubsection", "strongText"));
		experienceSection.getSubSection().add(
				getKeyChallengeSubsectionText(keyChallengesSubsection, "keyChallengeSubsection"));
		assessmentDetails.setExperienceSection(experienceSection);

		KeyLeadershipChallenges keyLeadershipChallenges = new KeyLeadershipChallenges();
		keyLeadershipChallenges.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"keyLeadershipChallenges", "title"));
		keyLeadershipChallenges.getChallengeListItem().addAll(getChallengeListItem());
		assessmentDetails.setKeyLeadershipChallenges(keyLeadershipChallenges);

		// create awareness section
		SelfAwareSection awarenessSection = new SelfAwareSection();
		awarenessSection.setLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "awarenessSection", "label"));
		awarenessSection.getSectionText().add(getAwarenessSectionPara("awarenessSection"));

		SubSection selfAwarenessSubsetion = new SubSection();
		selfAwarenessSubsetion.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"selfAwarenessSubsetion", "title"));
		selfAwarenessSubsetion.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"selfAwarenessSubsetion", "developmentText"));
		selfAwarenessSubsetion.setScore(new BigInteger(String.valueOf(roundPct(awareSlfAwarePct))));
		selfAwarenessSubsetion.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"selfAwarenessSubsetion", "strongText"));
		awarenessSection.getSubSection().add(
				getSelfAwarenessSubsectionText(selfAwarenessSubsetion, "selfAwarenessSubsetion"));

		SubSection situationalSelfAwarenessSubsection = new SubSection();
		situationalSelfAwarenessSubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"situationalSelfAwarenessSubsection", "title"));
		situationalSelfAwarenessSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"situationalSelfAwarenessSubsection", "developmentText"));
		situationalSelfAwarenessSubsection.setScore(new BigInteger(String.valueOf(roundPct(awareSitAwarePct))));
		situationalSelfAwarenessSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"situationalSelfAwarenessSubsection", "strongText"));
		awarenessSection.getSubSection().add(
				getSituationalSelfAwarenessSubsectionText(situationalSelfAwarenessSubsection,
						"situationalSelfAwarenessSubsection"));
		assessmentDetails.setSelfAwareSection(awarenessSection);

		// create Agility section
		LearningAgilitySection agilitySection = new LearningAgilitySection();
		agilitySection.setLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "learningAgilitySection",
				"label"));
		agilitySection.getSectionText().add(getLearningAgilitySectionPara("learningAgilitySection"));

		SubSection mentalSubsection = new SubSection();
		mentalSubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent, "mentalSubsection", "title"));
		mentalSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent, "mentalSubsection",
				"developmentText"));
		mentalSubsection.setScore(new BigInteger(String.valueOf(roundPct(agilityMentalPct))));
		mentalSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent, "mentalSubsection",
				"strongText"));
		agilitySection.getSubSection().add(getMentalSubsectionText(mentalSubsection, "mentalSubsection"));

		SubSection peopleSubsection = new SubSection();
		peopleSubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent, "peopleSubsection", "title"));
		peopleSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent, "peopleSubsection",
				"developmentText"));
		peopleSubsection.setScore(new BigInteger(String.valueOf(roundPct(agilityPeoplePct))));
		peopleSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent, "peopleSubsection",
				"strongText"));
		agilitySection.getSubSection().add(getPeopleSubsectionText(peopleSubsection, "peopleSubsection"));

		SubSection changeSubsection = new SubSection();
		changeSubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent, "changeSubsection", "title"));
		changeSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent, "changeSubsection",
				"developmentText"));
		changeSubsection.setScore(new BigInteger(String.valueOf(roundPct(agilityChangePct))));
		changeSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent, "changeSubsection",
				"strongText"));
		agilitySection.getSubSection().add(getChangeSubsectionText(changeSubsection, "changeSubsection"));

		SubSection resultsSubsection = new SubSection();
		resultsSubsection
				.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent, "resultsSubsection", "title"));
		resultsSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"resultsSubsection", "developmentText"));
		resultsSubsection.setScore(new BigInteger(String.valueOf(roundPct(agilityResultsPct))));
		resultsSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent, "resultsSubsection",
				"strongText"));
		agilitySection.getSubSection().add(getResultsSubsectionText(resultsSubsection, "resultsSubsection"));
		assessmentDetails.setLearningAgilitySection(agilitySection);

		// create Leadership Traits section
		LeadershipTraitsSection leadershipTraitsSection = new LeadershipTraitsSection();
		leadershipTraitsSection.setLabel(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"leadershipTraitsSection", "label"));
		leadershipTraitsSection.getSectionText().add(getLeadershipTraitsSectionPara("leadershipTraitsSection"));

		// previous version is focusedSubsection
		SubSection focusSubsection = new SubSection();
		focusSubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent, "focusSubsection", "title"));
		focusSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent, "focusSubsection",
				"developmentText"));
		focusSubsection.setScore(new BigInteger(String.valueOf(roundPct(traitsFocusPct))));
		focusSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent, "focusSubsection",
				"strongText"));
		leadershipTraitsSection.getSubSection().add(getFocusSubsectionText(focusSubsection, "focusSubsection"));

		// previous version is sociableSubsection
		SubSection persistenceSubsection = new SubSection();
		persistenceSubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent, "persistenceSubsection",
				"title"));
		persistenceSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"persistenceSubsection", "developmentText"));
		persistenceSubsection.setScore(new BigInteger(String.valueOf(roundPct(traitsPersistPct))));
		persistenceSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"persistenceSubsection", "strongText"));
		leadershipTraitsSection.getSubSection().add(
				getPersistenceSubsectionText(persistenceSubsection, "persistenceSubsection"));

		SubSection toleranceOfAmbiguitySubsection = new SubSection();
		toleranceOfAmbiguitySubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"toleranceOfAmbiguitySubsection", "title"));
		toleranceOfAmbiguitySubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"toleranceOfAmbiguitySubsection", "developmentText"));
		toleranceOfAmbiguitySubsection.setScore(new BigInteger(String.valueOf(roundPct(traitsTolerancePct))));
		toleranceOfAmbiguitySubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"toleranceOfAmbiguitySubsection", "strongText"));
		leadershipTraitsSection.getSubSection()
				.add(getToleranceOfAmbiguitySubsectionText(toleranceOfAmbiguitySubsection,
						"toleranceOfAmbiguitySubsection"));

		// composure for v1
		SubSection assertivenessSubsection = new SubSection();
		assertivenessSubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"assertivenessSubsection", "title"));
		assertivenessSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"assertivenessSubsection", "developmentText"));
		assertivenessSubsection.setScore(new BigInteger(String.valueOf(roundPct(traitsAssertivePct))));
		assertivenessSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"assertivenessSubsection", "strongText"));
		leadershipTraitsSection.getSubSection().add(
				getAssertivenessSubsectionText(assertivenessSubsection, "assertivenessSubsection"));

		SubSection optimismSubsection = new SubSection();
		optimismSubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent, "optimismSubsection",
				"title"));
		optimismSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"optimismSubsection", "developmentText"));
		optimismSubsection.setScore(new BigInteger(String.valueOf(roundPct(traitsOptimismPct))));
		optimismSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent, "optimismSubsection",
				"strongText"));

		leadershipTraitsSection.getSubSection()
				.add(getOptimismSubsectionText(optimismSubsection, "optimismSubsection"));
		assessmentDetails.setLeadershipTraitsSection(leadershipTraitsSection);

		// create Capacity section only when Reaven capacity is available
		if (!NO_RAVENS) {
			CapacitySection capacitySection = new CapacitySection();
			capacitySection
					.setLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "capacitySection", "label"));
			capacitySection.getSectionText().add(getCapacitySectionPara("capacitySection"));

			SubSection problemSolvingSubsetion = new SubSection();
			problemSolvingSubsetion.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent,
					"problemSolvingSubsection", "title"));
			problemSolvingSubsetion.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent,
					"problemSolvingSubsection", "developmentText"));
			problemSolvingSubsetion.setScore(new BigInteger(String.valueOf(roundPct(capacityProSolvingPct))));
			problemSolvingSubsetion.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent,
					"problemSolvingSubsection", "strongText"));
			capacitySection.getSubSection().add(
					getProblemSolvingSubsectionText(problemSolvingSubsetion, "problemSolvingSubsection"));
			assessmentDetails.setCapacitySection(capacitySection);
		}

		// create derailment risk section
		DerailmentSection derailmentRisksSection = new DerailmentSection();
		derailmentRisksSection.setLabel(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"derailmentRisksSection", "label"));
		derailmentRisksSection.getSectionText().add(getDerailmentRisksSectionPara("derailmentRisksSection"));

		SubSection volatileSubsection = new SubSection();
		volatileSubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent, "volatileSubsection",
				"title"));
		volatileSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"volatileSubsection", "developmentText"));
		volatileSubsection.setScore(new BigInteger(String.valueOf(roundPct(derailVolatilePct))));
		volatileSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent, "volatileSubsection",
				"strongText"));
		derailmentRisksSection.getSubSection().add(getVolatileSubsectionText(volatileSubsection, "volatileSubsection"));

		SubSection micromanagingSubsection = new SubSection();
		micromanagingSubsection.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"micromanagingSubsection", "title"));
		micromanagingSubsection.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"micromanagingSubsection", "developmentText"));
		micromanagingSubsection.setScore(new BigInteger(String.valueOf(roundPct(derailMicroPct))));
		micromanagingSubsection.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent,
				"micromanagingSubsection", "strongText"));
		derailmentRisksSection.getSubSection().add(
				getMicromanagingSubsectionText(micromanagingSubsection, "micromanagingSubsection"));

		SubSection closedSubsetion = new SubSection();
		closedSubsetion.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent, "closedSubsection", "title"));
		closedSubsetion.setDevelopmentText(this.contentRetriever.getRptContentTxt(jsonRptContent, "closedSubsection",
				"developmentText"));
		closedSubsetion.setScore(new BigInteger(String.valueOf(roundPct(derailClosedPct))));
		closedSubsetion.setStrongText(this.contentRetriever.getRptContentTxt(jsonRptContent, "closedSubsection",
				"strongText"));

		derailmentRisksSection.getSubSection().add(getClosedSubsectionText(closedSubsetion, "closedSubsection"));

		assessmentDetails.setDerailmentSection(derailmentRisksSection);

		rpt.setAssessmentDetailsSectionV2(assessmentDetails);

		// set developmentPriorities section
		DevelopmentPrioritiesSection developmentPriorities = new DevelopmentPrioritiesSection();
		developmentPriorities.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent, "developmentPriorities",
				"title"));
		developmentPriorities.setP1(this.contentRetriever.getRptContentTxt(jsonRptContent, "developmentPriorities",
				"p1"));
		developmentPriorities.getPriority().addAll(getDevelopmentPriority("developmentPriorities"));
		rpt.setDevelopmentPrioritiesSection(developmentPriorities);

		// set Summary section
		SummarySection summary = new SummarySection();

		summary.setTitle(this.contentRetriever.getRptContentTxt(jsonRptContent, "summary", "title"));
		summary.setCurrentLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "summary", "currentLabel"));
		summary.setAspirationLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "summary", "aspirationLabel"));
		summary.setTargetLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "summary", "targetLabel"));
		summary.getLevel().addAll(getLevelItem());

		summary.setBackgroundHeading(this.contentRetriever.getRptContentTxt(jsonRptContent, "summary",
				"backgroundHeading"));
		summary.getBackgroundListItem().addAll(getBackgroundListItem());
		summary.setGoalsObjectivesHeading(this.contentRetriever.getRptContentTxt(jsonRptContent, "summary",
				"goalsObjectivesHeading"));
		summary.setGoalsObjectivesList(getGoalsObjectivesList());
		summary.setIdealRoleHeading(this.contentRetriever.getRptContentTxt(jsonRptContent, "summary",
				"idealRoleHeading"));
		summary.setIdealRoleList(getIdealRoleList());

		ExpTable expTable = new ExpTable();
		expTable.setExpColHeader(this.contentRetriever.getRptContentTxt(jsonRptContent, "expTable", "expColHeader"));
		expTable.setFutureColHeader(this.contentRetriever.getRptContentTxt(jsonRptContent, "expTable",
				"futureColHeader"));
		expTable.setCurrentRoleLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "expTable",
				"currentRoleLabel"));

		ExpTableCategory expTableCategory = null;
		expTableCategory = new ExpTableCategory();

		expTableCategory.setCategoryLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "expTableCategory",
				"categoryLabelCase1"));
		expTableCategory.getCategoryItem().addAll(getOrgTypeCategoryItem());
		expTable.getExpTableCategory().add(expTableCategory);

		expTableCategory = new ExpTableCategory();

		expTableCategory.setCategoryLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "expTableCategory",
				"categoryLabelCase2"));
		expTableCategory.getCategoryItem().addAll(getOrgSizeCategoryItem());
		expTable.getExpTableCategory().add(expTableCategory);

		expTableCategory = new ExpTableCategory();

		expTableCategory.setCategoryLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "expTableCategory",
				"categoryLabelCase3"));
		expTableCategory.getCategoryItem().addAll(getIndTypeCategoryItem());
		expTable.getExpTableCategory().add(expTableCategory);

		expTableCategory = new ExpTableCategory();

		expTableCategory.setCategoryLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "expTableCategory",
				"categoryLabelCase4"));
		expTableCategory.getCategoryItem().addAll(getRoleTypeCategoryItem());
		expTable.getExpTableCategory().add(expTableCategory);

		expTableCategory = new ExpTableCategory();

		expTableCategory.setCategoryLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, "expTableCategory",
				"categoryLabelCase5"));
		expTableCategory.getCategoryItem().addAll(getFunctAreaCategoryItem());
		expTable.getExpTableCategory().add(expTableCategory);

		summary.setExpTable(expTable);
		rpt.setSummarySection(summary);

		rcm = marshalOutput(rpt);
		// log.debug("RCM=" + rcm);

		return rcm;
	}

	private SectionText getDriversSectionPara(String keyMap) throws PalmsException {
		String txt = "";

		log.debug("DriversSection: driversAdvdrive=" + driversAdvdrive + " driversCareerplan=" + driversCareerplan
				+ " driversRolepref=" + driversRolepref);

		if (driversAdvdrive == 1 && driversCareerplan == 1 && driversRolepref == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (driversAdvdrive == 1 && driversCareerplan == 1 && driversRolepref == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (driversAdvdrive == 1 && driversCareerplan == 0 && driversRolepref == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (driversAdvdrive == 0 && driversCareerplan == 1 && driversRolepref == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (driversAdvdrive == 1 && driversCareerplan == 0 && driversRolepref == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (driversAdvdrive == 0 && driversCareerplan == 1 && driversRolepref == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (driversAdvdrive == 0 && driversCareerplan == 0 && driversRolepref == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else if (driversAdvdrive == 0 && driversCareerplan == 0 && driversRolepref == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase8");
		} else {
			log.debug("The drivers main para content is empty.");
			return null;
		}
		SectionText sectionText = new SectionText();
		sectionText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		return sectionText;
	}

	// drive sub section for previous version
	private SubSection getAdvancementDriveSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";
		SubSectionText subText = new SubSectionText();

		int a = roundPct(driversAdvdrivePct); // Advancement Drive Score
												// (percentile)
		int b = threeToFiveYrsGoal; // ITEM128
		int c = currRoleLevel - threeToFiveYrsRoleLevel; // ItemID003 �
															// ItemID129

		if (threeToFiveYrsRoleLevel == 0) { // participant not answered
			c = 0; // Equivalent "na"
		} else {
			if (c >= 2)
				c = 2; // Equivalent "+2"
			if (c < 2)
				c = 1; // Equivalent "<2"
		}

		log.debug("currRoleLevel=" + currRoleLevel + " threeToFiveYrsRoleLevel=" + threeToFiveYrsRoleLevel);
		log.debug("A=" + a + " B=" + b + " C=" + c);

		if (a >= 75 && b == 1 && c == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (a >= 50 && a <= 74 && b == 1 && c == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (a >= 25 && a <= 49 && b == 1 && c == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (a >= 11 && a <= 24 && b == 1 && c == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (a >= 1 && a <= 10 && b == 1 && c == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (a >= 75 && b == 1 && c == 2) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (a >= 50 && a <= 74 && b == 1 && c == 2) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else if (a >= 25 && a <= 49 && b == 1 && c == 2) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase8");
		} else if (a >= 11 && a <= 24 && b == 1 && c == 2) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase9");
		} else if (a >= 1 && a <= 10 && b == 1 && c == 2) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase10");
		} else if (a >= 75 && b == 2 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase11");
		} else if (a >= 50 && a <= 74 && b == 2 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase12");
		} else if (a >= 25 && a <= 49 && b == 2 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase13");
		} else if (a >= 11 && a <= 24 && b == 2 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase14");
		} else if (a >= 1 && a <= 10 && b == 2 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase15");
		} else if (a >= 75 && b == 3 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase16");
		} else if (a >= 50 && a <= 74 && b == 3 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase17");
		} else if (a >= 25 && a <= 49 && b == 3 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase18");
		} else if (a >= 11 && a <= 24 && b == 3 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase19");
		} else if (a >= 1 && a <= 10 && b == 3 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase20");
		} else if (a >= 75 && b == 4 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase21");
		} else if (a >= 50 && a <= 74 && b == 4 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase22");
		} else if (a >= 25 && a <= 49 && b == 4 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase23");
		} else if (a >= 11 && a <= 24 && b == 4 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase24");
		} else if (a >= 1 && a <= 10 && b == 4 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase25");
		} else if (a >= 75 && b == 5 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase26");
		} else if (a >= 50 && a <= 74 && b == 5 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase27");
		} else if (a >= 25 && a <= 49 && b == 5 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase28");
		} else if (a >= 11 && a <= 24 && b == 5 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase29");
		} else if (a >= 1 && a <= 10 && b == 5 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase30");
		} else if (a >= 75 && b >= 6 && b <= 7 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase31");
		} else if (a >= 50 && a <= 74 && b >= 6 && b <= 7 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase32");
		} else if (a >= 25 && a <= 49 && b >= 6 && b <= 7 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase33");
		} else if (a >= 11 && a <= 24 && b >= 6 && b <= 7 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase34");
		} else if (a >= 1 && a <= 10 && b >= 6 && b <= 7 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase35");
		} else if (a >= 75 && b == 8 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase36");
		} else if (a >= 50 && a <= 74 && b == 8 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase37");
		} else if (a >= 25 && a <= 49 && b == 8 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase38");
		} else if (a >= 11 && a <= 24 && b == 8 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase39");
		} else if (a >= 1 && a <= 10 && b == 8 && c == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase40");
		} else {
			log.debug("drives Advancement Drive subsection is empty!");
		}

		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	private SubSection getV1ParticipantDriveText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";
		SubSectionText subText = new SubSectionText();

		/**********************************************************************************
		 * RoleTenure - a = scored input: interest.drive.yeasCurrentRole
		 * threeToFiveYrsGoal - b = scored input:
		 * interest.drive.threeToFiveYearGoal jump - c = scored input:
		 * interest.drive.jump; d = if ltAspiration > b true, Else = false
		 * 
		 * jump is actually a level and not the calculation, hence the new value
		 * for c below
		 * *******************************************************************************/

		int a = yrsInCurrRole;
		int b = threeToFiveYrsGoal;
		int c = 0;
		int theJump = 0;
		theJump = jump; // if this value is 0 then the person didn't answer
						// the question
		int d = 1;

		// lower target level numbers are actually higher levels so the logic is
		// reversed

		// d = 1 = A < T
		// d = 0 = A >= T

		if (ltAspiration > tgtRole) {
			d = 1;
		} else {
			d = 0;
		}

		log.debug("V1ParticipantDriveText: a=" + a + " b=" + b + " c=" + c);

		if (theJump != 0) {
			c = (currRoleLevel - theJump);

			if (a >= 3 && b == 1 && c >= 2 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
			} else if (a >= 3 && b == 1 && c < 2 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
			} else if (a >= 3 && b == 1 && c >= 2 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase10");
			} else if (a >= 3 && b == 1 && c < 2 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase11");
			} else if (a < 3 && b == 1 && c >= 2 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase19");
			} else if (a < 3 && b == 1 && c < 2 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase20");
			} else if (a < 3 && b == 1 && c >= 2 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase28");
			} else if (a < 3 && b == 1 && c < 2 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase29");
			}

		} else {

			if (a >= 3 && b == 2 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
			} else if (a >= 3 && b == 3 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
			} else if (a >= 3 && b == 4 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
			} else if (a >= 3 && b == 5 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
			} else if (a >= 3 && b == 6 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
			} else if (a >= 3 && b == 7 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase8");
			} else if (a >= 3 && b == 8 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase9");
			} else if (a >= 3 && b == 2 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase12");
			} else if (a >= 3 && b == 3 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase13");
			} else if (a >= 3 && b == 4 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase14");
			} else if (a >= 3 && b == 5 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase15");
			} else if (a >= 3 && b == 6 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase16");
			} else if (a >= 3 && b == 7 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase17");
			} else if (a >= 3 && b == 8 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase18");
			} else if (a < 3 && b == 2 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase21");
			} else if (a < 3 && b == 3 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase22");
			} else if (a < 3 && b == 4 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase23");
			} else if (a < 3 && b == 5 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase24");
			} else if (a < 3 && b == 6 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase25");
			} else if (a < 3 && b == 7 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase26");
			} else if (a < 3 && b == 8 && d == 1) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase27");
			} else if (a < 3 && b == 2 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase30");
			} else if (a < 3 && b == 3 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase31");
			} else if (a < 3 && b == 4 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase32");
			} else if (a < 3 && b == 5 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase33");
			} else if (a < 3 && b == 6 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase34");
			} else if (a < 3 && b == 7 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase35");
			} else if (a < 3 && b == 8 && d == 0) {
				txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase36");
			}
		}
		LangReplacement enLangFactory = LangFactory.getLanguage("en-US");
		subText.setText(enLangFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	/* ***************************************
	 * No logic change from V1 focus A = 3 to 5 year career plan 
	 * B = N of strong preferences
	 * ***************************************
	 */
	private SubSection getCareerPlanningSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		SubSectionText subText = new SubSectionText();

		String txt = "";
		int a = carPlan; // ITEM 127
		int b = nStrongPrefs; // drivers.careerplan.strongPrefs derived from sum
								// of ITEM147 through ITEM205

		log.debug("Drivers CareerPlan subsection: carPlan=" + carPlan + " nStrongPrefs=" + nStrongPrefs);

		if (a >= 1 && a <= 2 && b >= 4 && b <= 5) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (a >= 1 && a <= 2 && b >= 2 && b <= 3) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (a >= 3 && a <= 4 && b >= 4 && b <= 5) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (a >= 1 && a <= 2 && b >= 0 && b <= 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (a == 3 && b >= 2 && b <= 3) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (a == 4 && b == 3) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (a >= 5 && a <= 6 && b >= 4 && b <= 5) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else if (a >= 3 && a <= 4 && b >= 0 && b <= 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase8");
		} else if (a == 4 && b == 2) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase9");
		} else if (a >= 5 && a <= 6 && b >= 2 && b <= 3) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase10");
		} else if (a >= 5 && a <= 6 && b >= 0 && b <= 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase11");
		} else {
			log.debug("Drivers section Career Planning subsection is empty!");
		}

		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);

		SubSectionText listText = null;

		if (nStrongPrefs == 5) {
			listText = new SubSectionText();
			listText.setText(this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "preListTextCase1"));
			subSection.getSubSectionText().add(listText);
			addCareerPlanningList(subSection, true, 1);

		} else if (nStrongPrefs >= 1 && nStrongPrefs <= 4) {
			listText = new SubSectionText();
			listText.setText(langFactory.replaceContentVar(
					this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "preListTextCase1"), varTextWrapper));
			subSection.getSubSectionText().add(listText);

			addCareerPlanningList(subSection, false, 1);

			listText = new SubSectionText();
			listText.setText(langFactory.replaceContentVar(
					this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "elseText"), varTextWrapper));
			subSection.getSubSectionText().add(listText);

			addCareerPlanningList(subSection, false, 0);

		} else if (nStrongPrefs == 0) {
			listText = new SubSectionText();
			listText.setText(langFactory.replaceContentVar(
					this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "preListTextCase2"), varTextWrapper));
			subSection.getSubSectionText().add(listText);

			addCareerPlanningList(subSection, true, 1);
		} else {
			log.debug("drivers career planning list content is empty!");
		}
		return subSection;
	}

	private void addCareerPlanningList(SubSection subSection, boolean full, int filterNum) throws PalmsException {
		List<Integer> prefList = Arrays.asList(rolePref, orgPref, indPref, sizePref, functPref);

		String keyMap = "careerPlanningSubsection";
		log.debug("Career Planning List: boolean full=" + full + " filterNum=" + filterNum);
		log.debug("Career Planning List: rolePref=" + rolePref + " orgPref=" + orgPref + " indPref=" + indPref
				+ " sizePref=" + sizePref + " functPref=" + functPref);

		SubSectionText listText = new SubSectionText();
		String listContent = "<ul>";
		for (int i = 0; i < prefList.size(); i++) {

			if (full) {
				listContent += "<li>";
				listContent += this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "listContentCase"
						+ String.valueOf(i + 1));
				listContent += "</li>";
			} else {
				if (prefList.get(i) == filterNum) {
					listContent += "<li>";
					listContent += this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "listContentCase"
							+ String.valueOf(i + 1));
					listContent += "</li>";
				}
			}
		}
		listContent += "</ul>";
		listText.setText(langFactory.replaceContentVar(listContent, varTextWrapper));
		subSection.getSubSectionText().add(listText);
	}

	/*****************************************************************************
	 * Substantial changes in the Scoring and logic to display text from V1
	 * replaces �Engagement� from v1
	 *****************************************************************************/
	private SubSection getRolePreferenceSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";
		SubSectionText subText = new SubSectionText();

		log.debug("RolePreference Subsection: driversRoleprefPct=" + driversRoleprefPct);

		if (driversRoleprefPct >= 75) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (driversRoleprefPct >= 50 && driversRoleprefPct <= 74) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (driversRoleprefPct >= 25 && driversRoleprefPct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (driversRoleprefPct >= 11 && driversRoleprefPct <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (driversRoleprefPct >= 1 && driversRoleprefPct <= 10) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else {
			log.debug("drivers role preference sub section content is empty.");
		}
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	private SectionText getExperienceSectionPara(String keyMap) throws PalmsException {
		log.debug("Experience section: expCore=" + expCore + " expPerspective=" + expPerspective + " expKeyChallenges="
				+ expKeyChallenges);
		String txt = "";
		if (expCore == 1 && expPerspective == 1 && expKeyChallenges == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (expCore == 1 && expPerspective == 1 && expKeyChallenges == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (expCore == 1 && expPerspective == 0 && expKeyChallenges == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (expCore == 0 && expPerspective == 1 && expKeyChallenges == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (expCore == 1 && expPerspective == 0 && expKeyChallenges == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (expCore == 0 && expPerspective == 1 && expKeyChallenges == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (expCore == 0 && expPerspective == 0 && expKeyChallenges == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else if (expCore == 0 && expPerspective == 0 && expKeyChallenges == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase8");
		} else {
			log.debug("The experience Main Para content is empty.");
		}
		SectionText sectionText = new SectionText();
		sectionText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		return sectionText;
	}

	private SubSection getCoreExperienceSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";
		SubSectionText subText = new SubSectionText();

		String relCoreExp = "";

		if (expCorePct >= 50) {
			relCoreExp = "more";
		} else if (expCorePct < 50 && expCorePct >= 25) {
			relCoreExp = "same";
		} else if (expCorePct < 25) {
			relCoreExp = "less";
		}

		log.debug("Experience core section: relCoreExp=" + relCoreExp + " pace=" + pace);

		if (relCoreExp.equals("more") && pace >= 5) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (relCoreExp.equals("more") && pace >= 3 && pace <= 4) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (relCoreExp.equals("more") && pace >= 1 && pace <= 2) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (relCoreExp.equals("same") && pace >= 5) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (relCoreExp.equals("same") && pace >= 3 && pace <= 4) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (relCoreExp.equals("same") && pace >= 1 && pace <= 2) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (relCoreExp.equals("less") && pace >= 5) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else if (relCoreExp.equals("less") && pace >= 3 && pace <= 4) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase8");
		} else if (relCoreExp.equals("less") && pace >= 1 && pace <= 2) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase9");
		} else {
			log.debug("Experience core experience content is empty.");
		}

		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	private SubSection getPerspectiveSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";
		SubSectionText subText = new SubSectionText();

		log.debug("Experience perspective subsection: expPerspectivePct=" + expPerspectivePct);

		if (expPerspectivePct >= 75 && expPerspectivePct <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (expPerspectivePct >= 50 && expPerspectivePct <= 74) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (expPerspectivePct >= 25 && expPerspectivePct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (expPerspectivePct >= 11 && expPerspectivePct <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (expPerspectivePct >= 1 && expPerspectivePct <= 10) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else {
			log.debug("experience perspective sub section content is empty.");
		}

		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	private SubSection getKeyChallengeSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";
		SubSectionText subText = new SubSectionText();

		int a = nKeyChallenges;
		int b = expKeyChallengePct;

		log.debug("Experience Keychallenges subsection: a=" + a + " b=" + b);

		if (a >= 6 && a <= 10 && b >= 75 && b <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (a >= 6 && a <= 10 && b >= 50 && b <= 74) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (a >= 6 && a <= 10 && b >= 25 && b <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (a >= 6 && a <= 10 && b >= 1 && b <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (a >= 3 && a <= 5 && b >= 75 && b <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (a >= 3 && a <= 5 && b >= 50 && b <= 74) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (a >= 3 && a <= 5 && b >= 25 && b <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else if (a >= 3 && a <= 5 && b >= 1 && b <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase8");
		} else if (a >= 0 && a <= 2 && b >= 75 && b <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase9");
		} else if (a >= 0 && a <= 2 && b >= 50 && b <= 74) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase10");
		} else if (a >= 0 && a <= 2 && b >= 25 && b <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase11");
		} else if (a >= 0 && a <= 2 && b >= 1 && b <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase12");
		} else {
			log.debug("The experience KeyChallenge subsection content is empty.");
		}

		String checkmarkTxt = "";
		checkmarkTxt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "checkmarkText");

		txt = txt + " " + checkmarkTxt;
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	private SectionText getAwarenessSectionPara(String keyMap) throws PalmsException {
		String txt = "";

		log.debug("Awareness section: awareSlfAware=" + awareSlfAware + " awareSitAware=" + awareSitAware);

		if (awareSlfAware == 1 && awareSitAware == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (awareSlfAware == 1 && awareSitAware == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (awareSlfAware == 0 && awareSitAware == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (awareSlfAware == 0 && awareSitAware == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else {
			log.debug("The Awareness main section content is empty.");
		}
		SectionText sectionText = new SectionText();
		sectionText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		return sectionText;
	}

	private SubSection getSelfAwarenessSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";
		SubSectionText subText = new SubSectionText();

		log.debug("Awareness Self-Awareness subsection: awareSlfAwarePct=" + awareSlfAwarePct);

		if (awareSlfAwarePct >= 1 && awareSlfAwarePct <= 10) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (awareSlfAwarePct >= 11 && awareSlfAwarePct <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (awareSlfAwarePct >= 25 && awareSlfAwarePct <= 34) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (awareSlfAwarePct >= 35 && awareSlfAwarePct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (awareSlfAwarePct >= 50 && awareSlfAwarePct <= 65) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (awareSlfAwarePct >= 66 && awareSlfAwarePct <= 90) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (awareSlfAwarePct >= 91 && awareSlfAwarePct <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else {
			log.debug("Awareness self awareness content is empty.");
		}
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	private SubSection getSituationalSelfAwarenessSubsectionText(SubSection subSection, String keyMap)
			throws PalmsException {
		String txt = "";
		SubSectionText subText = new SubSectionText();

		log.debug("Awareness SituationalSelfAwareness subsection: awareSitAwarePct=" + awareSitAwarePct);

		if (awareSitAwarePct >= 1 && awareSitAwarePct <= 10) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (awareSitAwarePct >= 11 && awareSitAwarePct <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (awareSitAwarePct >= 25 && awareSitAwarePct <= 34) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (awareSitAwarePct >= 35 && awareSitAwarePct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (awareSitAwarePct >= 50 && awareSitAwarePct <= 65) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (awareSitAwarePct >= 66 && awareSitAwarePct <= 90) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (awareSitAwarePct >= 91 && awareSitAwarePct <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else {
			log.debug("Awareness situational self awareness Subsection content is empty.");
		}
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	private SectionText getLearningAgilitySectionPara(String keyMap) throws PalmsException {
		String txt = "";

		log.debug("LearningAgility Section: lrnAgile=" + lrnAgile);

		if (lrnAgile == 4) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (lrnAgile == 3) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (lrnAgile == 2) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (lrnAgile == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (lrnAgile == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else {
			log.debug("Learning Agility main para content is empty.");
		}
		SectionText sectionText = new SectionText();
		sectionText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		return sectionText;

	}

	private SubSection getMentalSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";

		log.debug("LearningAgility mental subsection: agilityMentalPct=" + agilityMentalPct);

		if (agilityMentalPct >= 1 && agilityMentalPct <= 10) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (agilityMentalPct >= 11 && agilityMentalPct <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (agilityMentalPct >= 25 && agilityMentalPct <= 34) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (agilityMentalPct >= 35 && agilityMentalPct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (agilityMentalPct >= 50 && agilityMentalPct <= 65) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (agilityMentalPct >= 66 && agilityMentalPct <= 90) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (agilityMentalPct >= 91 && agilityMentalPct <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else {
			log.debug("The agility mental content is empty.");
		}

		SubSectionText subText = new SubSectionText();
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);

		return subSection;
	}

	private SubSection getPeopleSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";

		log.debug("LearningAgility people subsection: agilityPeoplePct=" + agilityPeoplePct);

		if (agilityPeoplePct >= 1 && agilityPeoplePct <= 10) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (agilityPeoplePct >= 11 && agilityPeoplePct <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (agilityPeoplePct >= 25 && agilityPeoplePct <= 34) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (agilityPeoplePct >= 35 && agilityPeoplePct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (agilityPeoplePct >= 50 && agilityPeoplePct <= 65) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (agilityPeoplePct >= 66 && agilityPeoplePct <= 90) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (agilityPeoplePct >= 91 && agilityPeoplePct <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else {
			log.debug("The agility people content is empty.");
		}

		SubSectionText subText = new SubSectionText();
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);

		return subSection;
	}

	private SubSection getChangeSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";

		log.debug("LearningAgility change subsection: agilityChangePct=" + agilityChangePct);

		if (agilityChangePct >= 1 && agilityChangePct <= 10) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (agilityChangePct >= 11 && agilityChangePct <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (agilityChangePct >= 25 && agilityChangePct <= 34) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (agilityChangePct >= 35 && agilityChangePct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (agilityChangePct >= 50 && agilityChangePct <= 65) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (agilityChangePct >= 66 && agilityChangePct <= 90) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (agilityChangePct >= 91 && agilityChangePct <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else {
			log.debug("The agility change content is empty.");
		}
		SubSectionText subText = new SubSectionText();
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);

		return subSection;
	}

	private SubSection getResultsSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";

		log.debug("LearningAgility results subsection: agilityResultsPct=" + agilityResultsPct);

		if (agilityResultsPct >= 1 && agilityResultsPct <= 10) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (agilityResultsPct >= 11 && agilityResultsPct <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (agilityResultsPct >= 25 && agilityResultsPct <= 34) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (agilityResultsPct >= 35 && agilityResultsPct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (agilityResultsPct >= 50 && agilityResultsPct <= 65) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (agilityResultsPct >= 66 && agilityResultsPct <= 90) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (agilityResultsPct >= 91 && agilityResultsPct <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else {
			log.debug("The agility results content is empty.");
		}

		SubSectionText subText = new SubSectionText();
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);

		return subSection;
	}

	private SectionText getLeadershipTraitsSectionPara(String keyMap) throws PalmsException {
		String txt = "";

		log.debug("Leadership Traits section: ldrTraits=" + ldrTraits);

		if (ldrTraits == 5) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (ldrTraits == 4) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (ldrTraits == 3) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (ldrTraits == 2) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (ldrTraits == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (ldrTraits == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else {
			log.debug("Leadership Traits main para content is empty.");
		}
		SectionText sectionText = new SectionText();
		sectionText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		return sectionText;
	}

	private SubSection getFocusSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";

		log.debug("Leadership Traits Focus subsection: traitsFocusPct=" + traitsFocusPct);

		if (traitsFocusPct >= 1 && traitsFocusPct <= 10) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (traitsFocusPct >= 11 && traitsFocusPct <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (traitsFocusPct >= 25 && traitsFocusPct <= 34) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (traitsFocusPct >= 35 && traitsFocusPct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (traitsFocusPct >= 50 && traitsFocusPct <= 65) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (traitsFocusPct >= 66 && traitsFocusPct <= 90) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (traitsFocusPct >= 91 && traitsFocusPct <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else {
			log.debug("The leadershipTrait focus sub section content is empty.");
		}

		SubSectionText subText = new SubSectionText();
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);

		return subSection;
	}

	private SubSection getPersistenceSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";

		log.debug("Leadership Traits Persistence subsection: traitsPersistPct=" + traitsPersistPct);

		if (traitsPersistPct >= 1 && traitsPersistPct <= 10) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (traitsPersistPct >= 11 && traitsPersistPct <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (traitsPersistPct >= 25 && traitsPersistPct <= 34) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (traitsPersistPct >= 35 && traitsPersistPct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (traitsPersistPct >= 50 && traitsPersistPct <= 65) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (traitsPersistPct >= 66 && traitsPersistPct <= 90) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (traitsPersistPct >= 91 && traitsPersistPct <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else {
			log.debug("Leadership Traits persistence sub section content is empty.");
		}
		SubSectionText subText = new SubSectionText();
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);

		return subSection;
	}

	private SubSection getToleranceOfAmbiguitySubsectionText(SubSection subSection, String keyMap)
			throws PalmsException {
		String txt = "";

		log.debug("Leadership Traits ToleranceOfAmbiguity subsection: traitsTolerancePct=" + traitsTolerancePct);

		if (traitsTolerancePct >= 1 && traitsTolerancePct <= 10) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (traitsTolerancePct >= 11 && traitsTolerancePct <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (traitsTolerancePct >= 25 && traitsTolerancePct <= 34) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (traitsTolerancePct >= 35 && traitsTolerancePct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (traitsTolerancePct >= 50 && traitsTolerancePct <= 65) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (traitsTolerancePct >= 66 && traitsTolerancePct <= 90) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (traitsTolerancePct >= 91 && traitsTolerancePct <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else {
			log.debug("Leadership traits tolerance of ambiguity subsection content is empty.");
		}
		SubSectionText subText = new SubSectionText();
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);

		return subSection;
	}

	private SubSection getAssertivenessSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";

		log.debug("Leadership Traits Assertiveness subsection: traitsAssertivePct=" + traitsAssertivePct);

		if (traitsAssertivePct >= 1 && traitsAssertivePct <= 10) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (traitsAssertivePct >= 11 && traitsAssertivePct <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (traitsAssertivePct >= 25 && traitsAssertivePct <= 34) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (traitsAssertivePct >= 35 && traitsAssertivePct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (traitsAssertivePct >= 50 && traitsAssertivePct <= 65) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (traitsAssertivePct >= 66 && traitsAssertivePct <= 90) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (traitsAssertivePct >= 91 && traitsAssertivePct <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else {
			log.debug("The leadership traits assertiveness subsection content is empty.");
		}

		SubSectionText subText = new SubSectionText();
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);

		return subSection;
	}

	private SubSection getOptimismSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";

		log.debug("Leadership Traits Optimism subsection: traitsOptimismPct=" + traitsOptimismPct);

		if (traitsOptimismPct >= 1 && traitsOptimismPct <= 10) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (traitsOptimismPct >= 11 && traitsOptimismPct <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (traitsOptimismPct >= 25 && traitsOptimismPct <= 34) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (traitsOptimismPct >= 35 && traitsOptimismPct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (traitsOptimismPct >= 50 && traitsOptimismPct <= 65) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (traitsOptimismPct >= 66 && traitsOptimismPct <= 90) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (traitsOptimismPct >= 91 && traitsOptimismPct <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else {
			log.debug("The traits composed subsection content is empty.");
		}
		SubSectionText subText = new SubSectionText();
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);

		return subSection;
	}

	// Capacity main para content is static
	private SectionText getCapacitySectionPara(String keyMap) throws PalmsException {
		SectionText sectionText = new SectionText();
		sectionText.setText(langFactory.replaceContentVar(
				this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "text"), varTextWrapper));

		return sectionText;
	}

	private SubSection getProblemSolvingSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";

		log.debug("Capacity ProblemSolving subsection: capacityProSolvingPct=" + capacityProSolvingPct);

		if (capacityProSolvingPct >= 1 && capacityProSolvingPct <= 10) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (capacityProSolvingPct >= 11 && capacityProSolvingPct <= 24) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (capacityProSolvingPct >= 25 && capacityProSolvingPct <= 34) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (capacityProSolvingPct >= 35 && capacityProSolvingPct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (capacityProSolvingPct >= 50 && capacityProSolvingPct <= 65) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (capacityProSolvingPct >= 66 && capacityProSolvingPct <= 90) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (capacityProSolvingPct >= 91 && capacityProSolvingPct <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else {
			log.debug("The capacity problem solving sub section is empty.");
		}

		SubSectionText subText = new SubSectionText();
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);

		return subSection;
	}

	private SectionText getDerailmentRisksSectionPara(String keyMap) throws PalmsException {
		String txt = "";

		log.debug("DerailmentRisks section: derailVolatile=" + derailVolatile + " derailMicro=" + derailMicro
				+ " derailClosed=" + derailClosed);

		if (derailVolatile == 1 && derailMicro == 1 && derailClosed == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (derailVolatile == 1 && derailMicro == 1 && derailClosed == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (derailVolatile == 1 && derailMicro == 0 && derailClosed == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (derailVolatile == 0 && derailMicro == 1 && derailClosed == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else if (derailVolatile == 1 && derailMicro == 0 && derailClosed == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase5");
		} else if (derailVolatile == 0 && derailMicro == 1 && derailClosed == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase6");
		} else if (derailVolatile == 0 && derailMicro == 0 && derailClosed == 1) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase7");
		} else if (derailVolatile == 0 && derailMicro == 0 && derailClosed == 0) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase8");
		} else {
			log.debug("Derailment risks main para is empty.");
		}
		SectionText sectionText = new SectionText();
		sectionText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		return sectionText;
	}

	private SubSection getVolatileSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";

		log.debug("DerailmentRisks Volatile Subsection: derailVolatilePct=" + derailVolatilePct);

		if (derailVolatilePct >= 91 && derailVolatilePct <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (derailVolatilePct >= 85 && derailVolatilePct <= 90) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (derailVolatilePct >= 50 && derailVolatilePct <= 84) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (derailVolatilePct >= 1 && derailVolatilePct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else {
			log.debug("Derailment volatile sub section is empty.");
		}
		SubSectionText subText = new SubSectionText();
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	private SubSection getMicromanagingSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";

		log.debug("DerailmentRisks Micromanaging Subsection: derailMicroPct=" + derailMicroPct);

		if (derailMicroPct >= 91 && derailMicroPct <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (derailMicroPct >= 85 && derailMicroPct <= 90) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (derailMicroPct >= 50 && derailMicroPct <= 84) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (derailMicroPct >= 1 && derailMicroPct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else {
			log.debug("Derailment micromanaging sub section is empty.");
		}

		SubSectionText subText = new SubSectionText();
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	private SubSection getClosedSubsectionText(SubSection subSection, String keyMap) throws PalmsException {
		String txt = "";

		log.debug("DerailmentRisks closed Subsection: derailClosedPct=" + derailClosedPct);

		if (derailClosedPct >= 91 && derailClosedPct <= 99) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase1");
		} else if (derailClosedPct >= 85 && derailClosedPct <= 90) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase2");
		} else if (derailClosedPct >= 50 && derailClosedPct <= 84) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase3");
		} else if (derailClosedPct >= 1 && derailClosedPct <= 49) {
			txt = this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "textCase4");
		} else {
			log.debug("The Derailment closed sub section is empty.");
		}
		SubSectionText subText = new SubSectionText();
		subText.setText(langFactory.replaceContentVar(txt, varTextWrapper));
		subSection.getSubSectionText().add(subText);
		return subSection;
	}

	// starting build list
	private List<ChallengeListItem> getChallengeListItem() {
		List<ChallengeListItem> challengeItemList = new ArrayList<ChallengeListItem>();
		ChallengeListItem challengeItem = null;

		for (int i = 0; i < KEY_CHALLENGE_LIST.size(); i++) {
			challengeItem = new ChallengeListItem();
			challengeItem.setText(KEY_CHALLENGE_LIST.get(i));
			challengeItem.setIsAddressing(false);

			for (int j = 0; j < key_challenge_list.size(); j++) {
				if (j == i && key_challenge_list.get(j) == 1)
					challengeItem.setIsAddressing(true);
			}
			challengeItemList.add(challengeItem);
		}
		return challengeItemList;
	}

	private List<Level> getLevelItem() throws PalmsException {

		String keyMap = "individualStaticContentLevel";

		List<Level> levelList = new ArrayList<Level>();
		Level level = null;

		int x = ROLE_LEVEL_LIST.size() - 1;
		for (int i = x; i > -1; i--) {
			// log.debug(" current level ="+aCurrRoleLevel+" aspiration level="+aLtAspiration+" target level="+aTgtRole);
			// log.debug(" New Role Level i ="+i+" level text="+ROLE_LEVEL_LIST.get(i));

			level = new Level();
			// level.setLabel(ROLE_LEVEL_LIST.get(i));
			level.setLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap,
					"labelCase" + String.valueOf(i + 1)));
			if (i == aCurrRoleLevel)
				level.setIsCurrent(true);
			if (i == aLtAspiration)
				level.setIsAspiration(true);
			if (i == aTgtRole)
				level.setIsTarget(true);

			levelList.add(level);
		}
		return levelList;
	}

	private List<Priority> getDevelopmentPriority(String keyMap) throws PalmsException {
		List<Priority> priorityList = new ArrayList<Priority>();

		// if no Ravens score (capacity) we don't want it to show up as a
		// development priority

		int[][] prioTable = new int[][] {

		(new int[] { 1, calcGap(driversAdvdrivePct) }), (new int[] { 2, calcGap(driversCareerplanPct) }),
				(new int[] { 3, calcGap(driversRoleprefPct) }), (new int[] { 4, calcGap(expCorePct) }),
				(new int[] { 5, calcGap(expPerspectivePct) }), (new int[] { 6, calcGap(expKeyChallengePct) }),
				(new int[] { 7, calcGap(awareSlfAwarePct) }), (new int[] { 8, calcGap(awareSitAwarePct) }),
				(new int[] { 9, calcGap(agilityMentalPct) }), (new int[] { 10, calcGap(agilityPeoplePct) }),
				(new int[] { 11, calcGap(agilityChangePct) }), (new int[] { 12, calcGap(agilityResultsPct) }),
				(new int[] { 13, calcGap(traitsFocusPct) }), (new int[] { 14, calcGap(traitsPersistPct) }),
				(new int[] { 15, calcGap(traitsTolerancePct) }), (new int[] { 16, calcGap(traitsAssertivePct) }),
				(new int[] { 17, calcGap(traitsOptimismPct) }),
				(new int[] { 18, NO_RAVENS ? 0 : calcGap(capacityProSolvingPct) }), };

		Arrays.sort(prioTable, new SortDevPrioritiesV2());

		// pick up top three row to display 3 priorities
		int[] topPriorities = { prioTable[0][0], prioTable[1][0], prioTable[2][0] };

		log.debug("#1 = " + prioTable[0][0] + " #2 = " + prioTable[1][0] + " #3 = " + prioTable[2][0]);

		Priority priority = null;
		int case2ListLength = 7;
		int case18ListLength = 3;
		for (int i = 0; i < topPriorities.length; i++) {
			String contents = "";
			String priorityLabel = "labelCase" + String.valueOf(i + 1);

			priority = new Priority();

			priority.setLabel(this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, priorityLabel));

			log.debug("topPriority=" + topPriorities[i]);

			// KFP-713 - Added trim() to drop extraneous characters and &nbsp;
			// to put in the two spaces.
			// The call to trim() may be un-needed because Nelli is fixing the
			// content, but it doesn't hurt
			contents += "<b>";
			contents += this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap,
					"textSentence1Case" + String.valueOf(topPriorities[i])).trim();
			contents += "</b>";

			contents += "&nbsp;&nbsp;";

			contents += this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap,
					"textSentence2Case" + String.valueOf(topPriorities[i]));

			if (topPriorities[i] == 2) {
				contents += "<p><br>";
				for (int j = 0; j < case2ListLength; j++) {
					contents += this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "listForCase2Case"
							+ String.valueOf(j + 1));
					contents += "<br>";
				}
				contents += "</p>";
			}
			if (topPriorities[i] == 18) {
				contents += "<p><br>";
				for (int j = 0; j < case18ListLength; j++) {
					contents += this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap, "listForCase18Case"
							+ String.valueOf(j + 1));
					contents += "<br>";
				}
				contents += "</p>";
			}
			priority.setText(contents);
			priorityList.add(priority);
		}
		return priorityList;
	}

	private int calcGap(int pct) {
		return 99 - pct;
	}

	private List<BackgroundListItem> getBackgroundListItem() throws PalmsException {
		String keyMap = "backgroundListItem";

		List<BackgroundListItem> backgroundItemList = new ArrayList<BackgroundListItem>();
		List<Integer> items = Arrays.asList(yrsInWorkforce, yrsInManagement, nOrgs, yrsInCurrOrg, yrsInCurrRole,
				yrsOnBoard, nCountries);
		BackgroundListItem backgroudItem = null;

		for (int i = 0; i < items.size(); i++) {
			backgroudItem = new BackgroundListItem();
			backgroudItem.setNumberLabel(new BigInteger(String.valueOf(items.get(i))));
			backgroudItem.setText(this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap,
					"textCase" + String.valueOf(i + 1)));
			backgroundItemList.add(backgroudItem);
		}
		return backgroundItemList;
	}

	private GoalsObjectivesList getGoalsObjectivesList() throws PalmsException {

		String keyMap = "goalsObjectivesList";
		GoalsObjectivesList goalsObjectives = new GoalsObjectivesList();

		goalsObjectives.getBulletPoint().add(
				langFactory.replaceContentVar(
						this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap,
								"bullet1Case" + String.valueOf(carPlan)), varTextWrapper));

		goalsObjectives.getBulletPoint().add(
				langFactory.replaceContentVar(
						this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap,
								"bullet2Case" + String.valueOf(threeToFiveYrsGoal)), varTextWrapper));

		goalsObjectives.getBulletPoint().add(
				langFactory.replaceContentVar(
						this.contentRetriever.getRptContentTxt(jsonRptContent, keyMap,
								"bullet3Case" + String.valueOf(ltAspiration)), varTextWrapper));

		return goalsObjectives;
	}

	private IdealRoleList getIdealRoleList() {
		IdealRoleList idealRoleList = new IdealRoleList();
		@SuppressWarnings("unchecked")
		List<String> selTxtList = (List<String>) getListObject(ideal_role_list, IDEAL_ROLE_LIST, STR_ARR);
		for (int i = 0; i < selTxtList.size(); i++) {
			idealRoleList.getBulletPoint().add(selTxtList.get(i));
		}
		return idealRoleList;
	}

	private List<CategoryItem> getOrgTypeCategoryItem() {
		List<CategoryItem> categoryItemList = new ArrayList<CategoryItem>();
		String currOrgTypeTxt = ORG_TYPE_LIST_CURRENT.get(aCurrOrgType);
		buildCategoryMetric(categoryItemList, org_type_list, pref_org_type_list, ORG_TYPE_LIST, currOrgTypeTxt);

		return categoryItemList;
	}

	private List<CategoryItem> getOrgSizeCategoryItem() {
		List<CategoryItem> categoryItemList = new ArrayList<CategoryItem>();
		String currOrgSizeTxt = ORG_SIZE_LIST.get(aCurrOrgSize);

		buildCategoryMetric(categoryItemList, org_size_list, pref_org_size_list, ORG_SIZE_LIST, currOrgSizeTxt);

		return categoryItemList;
	}

	private List<CategoryItem> getIndTypeCategoryItem() {
		List<CategoryItem> categoryItemList = new ArrayList<CategoryItem>();
		String currIndTxt = IND_TYPE_LIST.get(aCurrIndustry);

		buildCategoryMetric(categoryItemList, ind_type_list, pref_ind_type_list, IND_TYPE_LIST, currIndTxt);

		return categoryItemList;
	}

	private List<CategoryItem> getRoleTypeCategoryItem() {
		List<CategoryItem> categoryItemList = new ArrayList<CategoryItem>();
		String currRoleTypeTxt = ROLE_TYPE_LIST.get(aCurrRoleType);

		buildCategoryMetric(categoryItemList, role_type_list, pref_role_type_list, ROLE_TYPE_LIST, currRoleTypeTxt);

		return categoryItemList;
	}

	private List<CategoryItem> getFunctAreaCategoryItem() {
		List<CategoryItem> categoryItemList = new ArrayList<CategoryItem>();
		String currFunctAreaTxt = FUNCT_AREA_LIST_CURRENT.get(aCurrFunctArea);

		buildCategoryMetric(categoryItemList, funct_area_list, pref_funct_area_list, FUNCT_AREA_LIST, currFunctAreaTxt);

		return categoryItemList;
	}

	private List<CategoryItem> buildCategoryMetric(List<CategoryItem> categoryItemList, List<Integer> expList,
			List<Integer> futList, List<String> txtList, String currTxt) {

		CategoryItem categoryItem = null;

		/*
		 * lower case List is int list and up case list is string list. int list
		 * has value 0 or 1 and Stirng list has String text the pair lists with
		 * matched indexs. so, when int list value = 1 we grab that text from
		 * string list
		 */

		Map<String, String> tableMap = new HashMap<String, String>();

		for (int i = 0; i < txtList.size(); i++) {
			String txtElm = txtList.get(i);
			boolean isCurr = txtElm.equals(currTxt) ? true : false;

			String result = KfapUtils.getCategoryDisplayText(isCurr, expList.get(i), futList.get(i));
			if (!result.equals("")) {
				tableMap.put(txtElm, result);
			}
		}

		// start to sort table that sync with ordered array list
		String[][] tempTable = new String[tableMap.size()][3];
		int row = 0;
		for (Map.Entry<String, String> entry : tableMap.entrySet()) {
			// log.info("table mapKey : " + entry.getKey() +
			// " table mapValue : " + entry.getValue());

			for (int i = 0; i < txtList.size(); i++) {
				if (txtList.get(i).toString().equals(entry.getKey())) {
					tempTable[row][0] = String.valueOf(i);
					tempTable[row][1] = entry.getKey();
					tempTable[row][2] = entry.getValue();
				}
			}
			row++;
		}

		KfapUtils.sortCategoryTable(tempTable, 0);

		LinkedHashMap<String, String> lHashMap = new LinkedHashMap<String, String>();
		for (int rows = 0; rows < tempTable.length; rows++) {
			String col1 = "";
			String col2 = "";
			for (int cols = 1; cols < tempTable[rows].length; cols++) {
				if (cols == 1)
					col1 = tempTable[rows][cols];
				if (cols == 2)
					col2 = tempTable[rows][cols];
			}
			lHashMap.put(col1, col2);
		}

		/*
		 * current list txt is not always same as experience and pref list. if
		 * it not exists in experience and pref list, we add it at end of the
		 * linked hashmap
		 */

		if (!txtList.contains(currTxt)) {
			lHashMap.put(currTxt, ",isCurr");
		}

		for (@SuppressWarnings("rawtypes")
		Map.Entry entry : lHashMap.entrySet()) {
			// log.info(" new table key=" + entry.getKey() + " value="
			// + entry.getValue());
			categoryItem = new CategoryItem();
			categoryItem.setLabel((String) entry.getKey());

			String entryValue = (String) entry.getValue();

			if (entryValue.contains("isCurr"))
				categoryItem.setIsCurrent(true);
			if (entryValue.contains("isExp"))
				categoryItem.setHasTwoPlusYears(true);
			if (entryValue.contains("isFut"))
				categoryItem.setWouldConsiderInFuture(true);

			categoryItemList.add(categoryItem);
		}

		return categoryItemList;
	}

	private Boolean getIsStrength(int value) {
		return value == 1 ? true : false;
	}

	private int roundPct(int pct) {
		if (pct < 1)
			pct = 1;
		if (pct > 99)
			pct = 99;
		return pct;
	}

	private int convertToBinary(int value) {
		return value > 0 ? 1 : 0;
	}

	private String marshalOutput(KfapIndividualFeedbackReportV2 kfapIndividualFeedbackReportV2) {
		JAXBContext jc = null;
		Marshaller m = null;
		StringWriter sw = new StringWriter();
		try {
			jc = JAXBContext.newInstance(KfapIndividualFeedbackReportV2.class);
			m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(kfapIndividualFeedbackReportV2, sw);

			return sw.toString();
		} catch (JAXBException jbe) {
			log.error("ALP KfpReportRgrToRcmV2.marshalOutput() - JAXBException: Msg={}", jbe.getMessage());
			jbe.printStackTrace();
			return null;
		}
	}

	public Object getListObject(List<Integer> intList, List<String> strList, int returnType) {

		List<String> itemStrList = new ArrayList<String>();
		int itemIntValue = 0;

		if (returnType == 1) {
			for (int i = 0; i < intList.size(); i++) {
				if (intList.get(i) == 1) {
					for (int j = 0; j < strList.size(); j++) {
						if (j == i) {
							itemStrList.add(strList.get(i));
							// log.info("The Selected int List i="+i+" selected text List="+strList.get(i));
						}
					}
				}
			}
		} else {
			for (int j = 0; j < intList.size(); j++) {
				itemIntValue += 1;
			}
		}
		// 1= STR_ARRAY
		// 2= INT_SUM
		if (returnType == 1) {
			return itemStrList;
		} else if (returnType == 2) {
			return itemIntValue;
		} else {
			return null;
		}
	}

	private String getLanguage(String language) {
		/* 
		 * in a case other languages are not available it use default "en", 
		 * which the langCode = langCodes.get(0) in the language code array.   
		 */
		String lang = "";

		if (language != null) {
			if (Arrays.asList(langCodes).contains(language)) {
				if (language.indexOf("_") != -1) {
					lang = language.replace("_", "-");
				} else if (language.equals("en")) {
					lang = DEFAULT_LANG;
				} else {
					lang = language;
				}
			} else {
				lang = DEFAULT_LANG;
			}
		} else {
			lang = DEFAULT_LANG;
		}
		return lang;
	}

	private String getDataFormat(String lang) {

		// Modified per KFP-708
		if (lang.equals(DEFAULT_LANG) || lang.equals("en")) {
			// longDateFormat = "dd MMMMM yyyy";
			longDateFormat = "MM/dd/yyyy";
		} else if (lang.equals("zh-CN")) {
			longDateFormat = "yyyy-MM-dd";
		} else if (lang.equals("ko")) {
			longDateFormat = "yyyy-MM-dd";
		} else if (lang.equals("ja")) {
			longDateFormat = "yyyy-MM-dd";
		} else if (lang.equals("fr")) {
			longDateFormat = "dd/MM/yyyy";
		} else if (lang.equals("es")) {
			longDateFormat = "dd/MM/yyyy";
		} else if (lang.equals("de")) {
			longDateFormat = "dd.MM.yyyy";
		} else if (lang.equals("tr")) {
			longDateFormat = "dd.MM.yyyy";
		} else if (lang.equals("ru")) {
			longDateFormat = "dd.MM.yyyy";
		} else if (lang.equals("pt-BR")) {
			longDateFormat = "dd/MM/yyyy";
		} else if (lang.equals("en-GB")) {
			longDateFormat = "dd/MM/yyyy";
		} else if (lang.equals("it")) {
			longDateFormat = "dd-MM-yyyy";
		} else {
			// Default... All unspecified languages end up here
			longDateFormat = "dd-MM-yyyy";
		}

		return longDateFormat;
	}

	/*
	 * for test in local CalculationPayload.xml file private Document
	 * loadInputXml(String path) { DocumentBuilderFactory domFactory =
	 * DocumentBuilderFactory.newInstance(); domFactory.setNamespaceAware(true);
	 * DocumentBuilder builder; Document doc = null; try { builder =
	 * domFactory.newDocumentBuilder(); // setXmlDoc(builder.parse(path)); doc =
	 * builder.parse(path); } catch (ParserConfigurationException e) {
	 * e.printStackTrace(); } catch (SAXException e) { e.printStackTrace(); }
	 * catch (IOException e) { e.printStackTrace(); } return doc; }
	 */

	private void mapListContent(String jsonString, String lang) throws PalmsException {

		ORG_TYPE_LIST_CURRENT = this.contentRetriever.getAsmtListTxt(jsonString, 6, "kfpQ1");
		ROLE_LEVEL_LIST = this.contentRetriever.getAsmtListTxt(jsonString, 6, "kfpQ3"); // current
		ROLE_TYPE_LIST = this.contentRetriever.getAsmtListTxt(jsonString, 6, "kfpQ4"); // current
		ORG_SIZE_LIST = this.contentRetriever.getAsmtListTxt(jsonString, 6, "kfpQ6"); // current
		IND_TYPE_LIST_CURRENT = this.contentRetriever.getAsmtListTxt(jsonString, 6, "kfpQ7");
		FUNCT_AREA_LIST_CURRENT = this.contentRetriever.getAsmtListTxt(jsonString, 6, "kfpQ8");
		ORG_TYPE_LIST = this.contentRetriever.getAsmtListTxt(jsonString, 7, "kfpQ15");
		IND_TYPE_LIST = this.contentRetriever.getAsmtListTxt(jsonString, 7, "kfpQ19");
		FUNCT_AREA_LIST = this.contentRetriever.getAsmtListTxt(jsonString, 7, "kfpQ20");
		KEY_CHALLENGE_LIST = this.contentRetriever.getAsmtListTxt(jsonString, 7, "kfpQ22");
		IDEAL_ROLE_LIST = this.contentRetriever.getAsmtListTxt(jsonString, 8, "kfpQ27");

		// remove no used element
		// KEY_CHALLENGE_LIST.remove(KEY_CHALLENGE_LIST.size() - ONE);
		// ORG_TYPE_LIST_CURRENT.remove(ORG_TYPE_LIST_CURRENT.size() - ONE);

	}

	private void mapRawResponseData(Document scoreDoc) throws Exception {

		@SuppressWarnings("rawtypes")
		Map scoreMap = KfapUtils.getScoredDataMap(scoreDoc, KFP_INST, DATA_SET);
		@SuppressWarnings("unchecked")
		Iterator<Entry<String, String>> it = scoreMap.entrySet().iterator();

		role_level_list.clear();
		org_type_list.clear();
		org_size_list.clear();
		ind_type_list.clear();
		role_type_list.clear();
		funct_area_list.clear();
		key_challenge_list.clear();
		ideal_role_list.clear();
		pref_org_type_list.clear();
		pref_org_size_list.clear();
		pref_ind_type_list.clear();
		pref_role_type_list.clear();
		pref_funct_area_list.clear();

		while (it.hasNext()) {
			Entry<String, String> pair = it.next();
			if (pair.getKey().equals("ITEM1")) {
				currOrgType = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM2")) {
				yrsInCurrOrg = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM3")) {
				currRoleLevel = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM4")) {
				currRoleType = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM5")) {
				yrsInCurrRole = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM6")) {
				currOrgSize = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM7")) {
				currIndustry = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM8")) {
				// sub industry
			} else if (pair.getKey().equals("ITEM9")) {
				currFunctArea = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM10")) {
				// sub funct area
			} else if (pair.getKey().equals("ITEM11")) {
				// year to born
			} else if (pair.getKey().equals("ITEM12")) {
				yrsInWorkforce = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM13")) {
				yrsInManagement = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM14")) {
				yrsOnBoard = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM15")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM16")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM17")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM18")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM19")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM20")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM21")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM22")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM23")) {
				role_level_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM24")) {
				nOrgs = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM25")) {
				org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM26")) {
				org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM27")) {
				org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM28")) {
				org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM29")) {
				org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM30")) {
				org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM31")) {
				org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM32")) {
				org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM33")) {
				org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM34")) {
				org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM35")) {
				org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM36")) {
				org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM37")) {
				role_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM38")) {
				role_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM39")) {
				// carMix = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM40")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM41")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM42")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM43")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM44")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM45")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM46")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM47")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM48")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM49")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM50")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM51")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM52")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM53")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM54")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM55")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM56")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM57")) {
				ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM58")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM59")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM60")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM61")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM62")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM63")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM64")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM65")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM66")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM67")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM68")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM69")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM70")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM71")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM72")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM73")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM74")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM75")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM76")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM77")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM78")) {
				funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM79")) {
				nCountries = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM80")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM85")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM90")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM95")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM100")) {
				// ITEM100 can return a value greater than 1 so need to account
				// for that. if it is bigger than 1 equals to 1
				key_challenge_list.add(convertToBinary(Integer.parseInt(pair.getValue())));
			} else if (pair.getKey().equals("ITEM101")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM106")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM111")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM116")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM121")) {
				key_challenge_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM127")) {
				carPlan = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM128")) {
				threeToFiveYrsGoal = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM129")) {
				threeToFiveYrsRoleLevel = Integer.parseInt(pair.getValue());
				jump = Integer.parseInt(pair.getValue());
				log.debug("jump=" + jump);
			} else if (pair.getKey().equals("ITEM130")) {
				ltAspiration = Integer.parseInt(pair.getValue());
			} else if (pair.getKey().equals("ITEM131")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM132")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM133")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM134")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM135")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM136")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM137")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM138")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM139")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM140")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM141")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM142")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM143")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM144")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM145")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM146")) {
				ideal_role_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM147")) {
				// no strong org_type pref
			} else if (pair.getKey().equals("ITEM148")) {
				pref_org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM149")) {
				pref_org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM150")) {
				pref_org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM151")) {
				pref_org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM152")) {
				pref_org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM153")) {
				pref_org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM154")) {
				pref_org_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM155")) {
				// no strong org size pref
			} else if (pair.getKey().equals("ITEM156")) {
				pref_org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM157")) {
				pref_org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM158")) {
				pref_org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM159")) {
				pref_org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM160")) {
				pref_org_size_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM161")) {
				// no strong pref for role type
			} else if (pair.getKey().equals("ITEM162")) {
				pref_role_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM163")) {
				pref_role_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM164")) {
				// no strong pref for industry
			} else if (pair.getKey().equals("ITEM165")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM166")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM167")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM168")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM169")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM170")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM171")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM172")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM173")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM174")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM175")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM176")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM177")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM178")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM179")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM180")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM181")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM182")) {
				pref_ind_type_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM183")) {
				// no strong pref for funct area
			} else if (pair.getKey().equals("ITEM184")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM185")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM186")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM187")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM188")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM189")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM190")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM191")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM192")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM193")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM194")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM195")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM196")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM197")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM198")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM199")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM200")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM201")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM202")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM203")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM204")) {
				pref_funct_area_list.add(Integer.parseInt(pair.getValue()));
			} else if (pair.getKey().equals("ITEM205")) {
				// none of the above for pref function area
			}
		}

		aTgtRole = tgtRole - ONE;
		aCurrRoleLevel = currRoleLevel - ONE;
		aLtAspiration = ltAspiration - ONE;
		aCurrRoleType = currRoleType - ONE;
		aCurrIndustry = currIndustry - ONE;
		aCurrFunctArea = currFunctArea - ONE;
		aCurrOrgType = currOrgType - ONE;
		aCurrOrgSize = currOrgSize - ONE;

		nFunctAreas = (Integer) getListObject(funct_area_list, null, INT_SUM); // NFunctAreas
		nIndustries = (Integer) getListObject(ind_type_list, null, INT_SUM); // Nindustries
		nLevels = (Integer) getListObject(role_level_list, null, INT_SUM);

	}

	private void mapScores(Document scoreDoc) throws PalmsException {
		try {

			/*
			 * There are 7 scales and total 21 factors 1. drivers (Advancement
			 * Drive; Career Planning; Role Preference) 2. experience (Core
			 * Experience; Perspective; Key Challenges) 3. Awareness
			 * (Self-Awareness; Situational Self-Awareness) 4. Learning Agility
			 * (Change; Results; People; Mental) 5. Leadership Traits (Focus;
			 * Persistence; Assertiveness; Tolerance of Ambiguity; Optimism) 6.
			 * Capacity (Problem solving) 7. Derailment Risks (Volatile;
			 * Micromanaging; Closed)
			 * 
			 * Scale score: scale name + Score Factor score: scale name + factor
			 * nameand Factor percentile score: scale name + factor name + Pct
			 */

			// drivers. was interest for v1
			driversScore = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"drivers.score")));
			driversAdvdrivePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"drivers.advdrive.norm")));
			driversCareerplanPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"drivers.careerplan.norm")));
			driversRoleprefPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"drivers.rolepref.norm")));
			driversAdvdrive = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"drivers.advdrive.score")));
			driversCareerplan = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"drivers.careerplan.score")));
			driversAdvdrive = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"drivers.advdrive.score")));
			driversRolepref = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"drivers.rolepref.score")));
			nStrongPrefs = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"drivers.careerplan.strongPrefs")));
			rolePref = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"drivers.careerplan.rolePref")));
			orgPref = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"drivers.careerplan.orgTPref")));
			sizePref = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"drivers.careerplan.orgSPref")));
			indPref = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"drivers.careerplan.indPref")));
			functPref = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"drivers.careerplan.funcPref")));

			// experience
			experienceScore = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"experience.score")));
			expPerspectivePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"experience.perspective.norm")));
			expCorePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"experience.core.norm")));
			expCore = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"experience.core.score")));
			expPerspective = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"experience.perspective.score")));
			expKeyChallengePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"experience.key.norm")));
			expKeyChallenges = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"experience.key.score")));
			nKeyChallenges = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"experience.key.nKey")));
			pace = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"experience.core.pace")));

			// awareness
			awarenessScore = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"awareness.score")));
			awareSlfAwarePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"awareness.self.norm")));
			awareSitAwarePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"awareness.sit.norm")));
			awareSlfAware = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"awareness.self.score")));
			awareSitAware = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"awareness.sit.score")));

			// leadership agility
			agilityScore = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"agile.score")));
			agilityChangePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"agile.change.norm")));
			agilityMentalPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"agile.mental.norm")));
			agilityPeoplePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"agile.people.norm")));
			agilityResultsPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"agile.results.norm")));
			lrnAgile = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "agile.total")));

			// leadership traits
			traitsScore = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"traits.score")));
			traitsFocusPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"traits.focus.norm")));
			traitsOptimismPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"traits.optimism.norm")));
			traitsAssertivePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"traits.assert.norm")));
			traitsTolerancePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"traits.tolerance.norm")));
			traitsPersistPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"traits.persist.norm")));
			ldrTraits = (int) Math
					.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST, "traits.total")));

			// derailment risks
			derailerScore = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"derailer.score")));
			derailClosedPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"derailer.closed.norm")));
			derailMicroPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"derailer.micro.norm")));
			derailVolatilePct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"derailer.volatile.norm")));
			derailClosed = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"derailer.closed.score")));
			derailMicro = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"derailer.micro.score")));
			derailVolatile = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
					"derailer.volatile.score")));

			// capacity
			String capacityNorm = KfapUtils.getScoredData(scoreDoc, KFP_INST, "capacity.norm");
			String capacity = KfapUtils.getScoredData(scoreDoc, KFP_INST, "capacity.score");
			if (NO_RAVENS == false) {
				if (StringUtils.isBlank(capacityNorm) || capacityNorm.equals("NaN") || StringUtils.isBlank(capacity)
						|| capacity.equals("NaN")) {
					NO_RAVENS = true;

				} else {
					capacityProSolvingPct = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc,
							KFP_INST, "capacity.norm")));
					capacityScore = (int) Math.round(Double.parseDouble(KfapUtils.getScoredData(scoreDoc, KFP_INST,
							"capacity.score")));
					// explicitly set because when a report is regenrated it
					// doesn't
					// seem to pick up the boolean at the top of this file
					NO_RAVENS = false;
				}
			}
		} catch (PalmsException pe) {
			throw pe;
		} catch (NumberFormatException nfe) {
			throw new PalmsException("Problem mapping scores: " + nfe.getMessage());
		}
	}

	private Map<String, String> wrapDisplayVars(String lang) {

		// clear this guy before load new
		varTextWrapper.clear();
		varTextWrapper.put("vDateComplete", vDateComplete);
		varTextWrapper.put("vTgtRole", vTgtRole);
		varTextWrapper.put("vLTAspiration", vLTAspiration);
		// varTextWrapper.put("vNTAspiration", vNTAspiration);
		varTextWrapper.put("vAvAn", "");
		varTextWrapper.put("vClient", vClient);
		varTextWrapper.put("vCurrRoleType", ROLE_TYPE_LIST.get(aCurrRoleType));
		varTextWrapper.put("vCurrRoleLevel", ROLE_LEVEL_LIST.get(aCurrRoleLevel));
		varTextWrapper.put("vCurrFunctArea", FUNCT_AREA_LIST_CURRENT.get(aCurrFunctArea));
		varTextWrapper.put("vCurrOrgSize", ORG_SIZE_LIST.get(aCurrOrgSize));
		varTextWrapper.put("vCurrOrgType", ORG_TYPE_LIST_CURRENT.get(aCurrOrgType));
		varTextWrapper.put("vCurrIndustry", IND_TYPE_LIST_CURRENT.get(aCurrIndustry));
		if (lang.equals("en-US")) {
			varTextWrapper.put("vPace", KfapUtils.convertNumToWord(String.valueOf(pace)));
		} else {
			varTextWrapper.put("vPace", String.valueOf(pace));
		}
		varTextWrapper.put("vRoleTenure", KfapUtils.convertNumToWord(String.valueOf(yrsInCurrRole))); // ???
		varTextWrapper.put("vMgmtTenure", KfapUtils.convertNumToWord(String.valueOf(yrsInManagement)));
		varTextWrapper.put("vCarTenure", KfapUtils.convertNumToWord(String.valueOf(yrsInWorkforce)));
		varTextWrapper.put("vNlevels", KfapUtils.convertNumToWord(String.valueOf(nLevels)));
		varTextWrapper.put("vNFunctAreas", KfapUtils.convertNumToWord(String.valueOf(nFunctAreas)));
		varTextWrapper.put("vNorgs", KfapUtils.convertNumToWord(String.valueOf(nOrgs)));
		varTextWrapper.put("vNindustries", KfapUtils.convertNumToWord(String.valueOf(nIndustries)));
		varTextWrapper.put("vBoardTenure", KfapUtils.convertNumToWord(String.valueOf(yrsOnBoard)));

		return varTextWrapper;
	}

	/**
	 * xformMonthDate - Takes a string with format 'MMMMM yyyy' and substitutes
	 * the month name found in the report content. This should replace the
	 * English month name with the month name for the language pulled for report
	 * content. This does mean that the English month name is replaced with a
	 * string that is is same as the original month name.
	 * 
	 * Assumptions: -- Output format is 'MMMMM yyyy'. The reconstructed string
	 * is the new month string + " " + the year from the old string.
	 * 
	 * @param monthYearStr
	 *            A string of 'MMMM yyyy' format (long month name with year).
	 *            Month name is English.
	 * @return A string with the Month name in the language in which the report
	 *         language was fetched.
	 */
	private String xformMonthDate(String monthYearStr, String langCode) throws PalmsException {
		String ret = "";
		String err = "";

		if (jsonRptContent == null) {
			err = "KfapReportRgrToRcmV2.xformMonthDate():  JSON content is not present";
			log.error(err);
			throw new IllegalStateException(err);
		}

		if (monthYearStr == null || monthYearStr.length() < 1) {
			err = "KfapReportRgrToRcmV2.xformMonthDate():  Month language transform requres an input parameter";
			log.error(err);
			throw new IllegalArgumentException(err);
		}
		if (Character.isDigit(monthYearStr.charAt(0))) {
			err = "KfapReportRgrToRcmV2.xformMonthDate():  Month language transform requires an input in 'MMMM yyyy' format.  Current input ("
					+ monthYearStr + ") has a digit as its first character";
			log.error(err);
			throw new IllegalArgumentException(err);
		}
		if (!monthYearStr.contains(" ")) {
			err = "KfapReportRgrToRcmV2.xformMonthDate():  Month language transform requires an input in 'MMMM yyyy' format.  Current input ("
					+ monthYearStr + ") has no space enclosed";
			log.error(err);
			throw new IllegalArgumentException(err);
		}

		// Get the parts... note the assumption of a space delimiter
		int idx = monthYearStr.indexOf(' ');
		String key = monthYearStr.substring(0, 3).toLowerCase();
		String yr = monthYearStr.substring(idx + 1);

		// get the month info
		String mon = this.contentRetriever.getSharedContentTxt(jsonRptContent, "months", key);

		// assemble the parts
		if (langCode.equals("ko")) {
			// Korean is YYYY MMMM (months from the CMS are formatted correctly)
			// "년" = \uB144
			ret = yr + '\uB144' + " " + mon;
		} else if (langCode.equals("ja")) {
			// Japanese is YYYY MMMM
			// "年" = \u5E74
			ret = yr + '\u5E74' + " " + mon;
		} else {
			// Default... English format MMMM YYYY
			ret = mon + " " + yr;
		}

		return ret;
	}

	public Map<String, String> getVarTextWrapper() {
		return varTextWrapper;
	}

	public void setVarTextWrapper(Map<String, String> varTextWrapper) {
		this.varTextWrapper = varTextWrapper;
	}

	public JsonNode getjsonRptContent() {
		return jsonRptContent;
	}

	public String getJsonAsmtContent() {
		return jsonAsmtContent;
	}

	public void setJsonAsmtContent(String jsonAsmtContent) {
		this.jsonAsmtContent = jsonAsmtContent;
	}

	public LangReplacement getLangFactory() {
		return langFactory;
	}

	public void setLangFactory(LangReplacement langFactory) {
		this.langFactory = langFactory;
	}
}
