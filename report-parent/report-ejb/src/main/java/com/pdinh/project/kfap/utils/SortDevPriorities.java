package com.pdinh.project.kfap.utils;

import java.util.Comparator;


public class SortDevPriorities implements Comparator<int[]>
{
       @Override
       public int compare(int[] ary1, int[] ary2)
       {
              int comp;
              // Asc on Priority
              comp = ary1[2] - ary2[2];
              if (comp != 0)  return comp;
              // Asc on Effort
              comp = ary1[3] - ary2[3];
              if (comp != 0)  return comp;
              // Dsc on GAP
              comp = ary2[5] - ary1[5];
              
              if (comp != 0) {
            	 return comp;
              }else{
            	  //Asc on SPORDER
            	 return ary1[0] - ary2[0];
              }
       }
}
