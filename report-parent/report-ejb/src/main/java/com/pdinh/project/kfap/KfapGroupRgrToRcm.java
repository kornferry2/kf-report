package com.pdinh.project.kfap;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Future;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import sun.net.www.protocol.http.HttpURLConnection;

import com.kf.project.kfap.reports.jaxb.AboutSection;
import com.kf.project.kfap.reports.jaxb.DefinitionsSection;
import com.kf.project.kfap.reports.jaxb.DetailedResultsSection;
import com.kf.project.kfap.reports.jaxb.KfapGroupReport;
import com.kf.project.kfap.reports.jaxb.Row;
import com.kf.project.kfap.reports.jaxb.Rows;
import com.kf.project.kfap.reports.jaxb.Table;
import com.kf.project.kfap.reports.jaxb.Tables;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpClientConfig;
import com.ning.http.client.Response;
import com.pdinh.data.ms.dao.PositionDao;
import com.pdinh.data.ms.dao.ProjectCourseDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.project.kfap.utils.KfapUtils;
import com.pdinh.report.util.JaxbUtil;
import com.pdinh.reportcontent.Manifest;
import com.pdinh.reportcontent.ReportContent;
import com.pdinh.reporting.generation.Participants;
import com.pdinh.reporting.generation.ReportParticipant;

@Stateless
@LocalBean
public class KfapGroupRgrToRcm {
	private static final Logger log = LoggerFactory.getLogger(KfapReportRgrToRcm.class);

	@Resource(name = "application/report/config_properties", type = Properties.class)
	private Properties configProperties;

	private static String reportManifestDir = "";
	private static String reportContentDir = "";
	private static String reportGenRespDir = "";

	private final ReportContent varContent = null;
	private ReportContent sharedStaticContent = null;
	private ReportContent staticContent = null;

	private String LONG_DATE_FORMAT = "MM/dd/yyyy";
	private String strDate;
	private String strCreateDate;
	private String createDate;
	private String strDateCompleted;

	// private ReportGenerationRequest rgr;
	private KfapGroupReport rpt;

	private String lang = "en-US";
	private final List<String> langCodes = Arrays.asList("en-US");

	// store rgr2 id and its mapped text
	private final Map<String, String> varTextWrapper = new HashMap<String, String>();

	// getListItem return type
	private static int STR_ARR = 1;
	private static int INT_SUM = 2;

	// no ravens project according to ProjectCourse
	boolean NO_RAVENS = false;
	// no ravens score from R
	boolean NO_RavensScore = false;

	private static String KFP_INST = "kfp";
	private static String manifest = "kfpManifest";

	// private static String RESTFUL_STRING_PARAMS =

	private static String RESTFUL_STRING_PARAMS = "/{instrumentId}/{manifest}/{tgtRole}";
	private static String TINCAN_STRING_PARAMS = "?participantId={participantId}&instrumentId={instrumentId}";

	// store individual item text and item value matched
	private String vTgtRole = "";
	private String vClient = "";

	private int interestScore; // 1 or 0
	private int experienceScore; // 1 or 0
	private int awarenessScore; // 1 or 0
	private int agileScore; // 1 or 0
	private int traitsScore; // 1 or 0
	private int capacityScore; // 1 or 0
	private int derailerScore; // 1 or 0

	private String strCompleted = "";

	private int slfAwarePct;
	private int sitAwarePct;
	private int changePct;
	private int resultsPct;
	private int peoplePct;
	private int mentalPct;
	private int focusedPct;
	private int sociablePct;
	private int composedPct;
	private int optimismPct;
	private int tolerancePct;
	private int volatilePct;
	private int closedPct;
	private int microPct;
	private int capacityPct; // is it a problem solving percentile score?
	private int perspectivePct;
	private int engagementPct;
	private int keyChallengePct;
	private int drivePct;
	private int focusPct;
	private int corePct;
	private int devEffort;

	private int currRoleLevel; // ITEM3
	private int yrsInCurrRole; // ITEM5
	private int threeToFiveYrsGoal; // ITEM128 ntAspiration
	private int jump; // ITEM129
	private int ltAspiration; // ITEM130
	private int tgtRole;

	@EJB
	private ProjectDao projectDao;

	@EJB
	private UserDao userDao;

	@EJB
	private PositionDao positionDao;

	@EJB
	private ProjectCourseDao projectCourseDao;

	public KfapGroupRgrToRcm() {

	}

	public String generateReportRcm(String type, String pptList, String language, String targ, String rptTitle)
			throws PalmsException {

		// log.debug("starting generateReportRcm potential RGR to RCM - language="
		// + language);
		lang = langCodes.get(0);
		if (language != null) {
			if (langCodes.contains(language)) {
				lang = language;
			}
			if (language.equals("fr_CA")) {
				lang = langCodes.get(4);
			} else if (language.equals("pt_BR")) {
				lang = langCodes.get(6);
			} else if (language.equals("zh_CN")) {
				lang = langCodes.get(2);
			} else if (language.equals("en")) {
				lang = langCodes.get(0);
			}
		}
		log.debug("using " + lang);

		// date format
		if (lang.equals("en-US"))
			// LONG_DATE_FORMAT = "MM/dd/yyyy";
			LONG_DATE_FORMAT = "dd MMMMM yyyy";
		if (lang.equals("ja"))
			LONG_DATE_FORMAT = "yyyy-MM-dd";
		if (lang.equals("zh"))
			LONG_DATE_FORMAT = "yyyy-MM-dd";
		if (lang.equals("it"))
			LONG_DATE_FORMAT = "dd-MM-yyyy";
		if (lang.equals("fr"))
			LONG_DATE_FORMAT = "dd.MM.yyyy";
		if (lang.equals("de"))
			LONG_DATE_FORMAT = "dd.MM.yyyy";
		if (lang.equals("pt"))
			LONG_DATE_FORMAT = "dd-MM-yyyy";
		if (lang.equals("es"))
			LONG_DATE_FORMAT = "dd.MM.yyyy";
		if (lang.equals("ru"))
			LONG_DATE_FORMAT = "dd.MM.yyyy";

		// SimpleDateFormat shortSdf = new SimpleDateFormat(SHORT_DATE_FORMAT);
		SimpleDateFormat longSdf = new SimpleDateFormat(LONG_DATE_FORMAT);

		strDate = longSdf.format(new Date());

		tgtRole = Integer.parseInt(targ);

		// Project proj = new Project();
		// com.pdinh.persistence.ms.entity.Project proj =
		// projectDao.findById(Integer.parseInt(projId));

		// Get List of Part IDs that was sent
		Participants participants = (Participants) JaxbUtil.unmarshal(pptList, Participants.class);
		List<Integer> allGroupPIds = new ArrayList<Integer>();
		for (ReportParticipant ppt : participants.getParticipants()) {
			Integer id;
			try {
				id = Integer.parseInt(ppt.getParticipantId());
			} catch (NumberFormatException e) {
				log.error("Invalid pptId " + ppt.getParticipantId() + ".  Skipped.");
				continue;
			}

			// int version =
			// StringNumberUtil.getCourseVersionFromMap(ppt.getCourseVersionMap(),
			// "kfp");

			// log.debug("Course Version is {}", version);
			// TODO: do something based on course version
			allGroupPIds.add(id);
		}

		// List<Position> positions =
		// positionDao.findPositionsByUsersIdsAndCourseAbbvs(allGroupPIds,
		// Arrays.asList(Course.KFP));

		// Make two arraylists, 1 for participants that have complete
		// assessments
		// Another for participants that are incomplete
		List<Integer> groupPIds = new ArrayList<Integer>();
		List<Integer> incompletGroupPIds = new ArrayList<Integer>();

		Integer pid = 0;
		String strpid = "";
		// String courseAbbv = "";
		Integer complStatus;
		Integer h = 0;
		vClient = "";

		for (int j = 0; j < allGroupPIds.size(); j++) {

			pid = allGroupPIds.get(j);
			strpid = allGroupPIds.get(j).toString();

			Position position = positionDao.findByUsersIdAndCourseAbbv(Integer.parseInt(strpid), "kfp");

			strCompleted = "Complete";

			if (position == null) {
				strCompleted = "Incomplete";
			} else {

				// kfp assessment (courseAbbv = kfp) needs to be done in
				// order to send someone for scoring

				complStatus = position.getCompletionStatus();

				if (complStatus != Position.COMPLETE) {
					strCompleted = "Incomplete";
				}

				if (h == 0) {

					vClient = position.getCompany().getCoName();
				}

				h = h + 1;

			}

			if (strCompleted.equals("Incomplete")) {
				incompletGroupPIds.add(pid);
			} else {
				groupPIds.add(pid);
			}

		}

		List<List<String>> returnedData = new ArrayList<List<String>>();

		try {
			// send only those partids that have completed assessment(s)

			returnedData = mapInputData(type, groupPIds, language, targ);

		} catch (Exception e) {
			System.out.println("problem to fetch and map input data!");
			e.printStackTrace();
			throw new PalmsException("Problem generating RCM due to " + e.getMessage(),
					PalmsErrorCode.RCM_GEN_FAILED.getCode());
			// return null;
		}

		// vClient = proj.getCompany().getCoName();

		// set manifest
		setReportManifestDir(configProperties.getProperty("reportContentRepository")
				+ "/manifest/projects/internal/kfap/");
		setReportContentDir(configProperties.getProperty("reportContentRepository") + "/content/");

		Manifest manifest = new Manifest(getReportManifestDir() + "group/" + "manifest.xml");

		sharedStaticContent = new ReportContent(getReportContentDir()
				+ manifest.getReportContents().get("sharedStaticContent") + ".xml");

		staticContent = new ReportContent(getReportContentDir() + manifest.getReportContents().get("staticContent")
				+ ".xml");

		strCreateDate = sharedStaticContent.getKeyMapValue4Lang("shared", "createDate", lang);
		createDate = strCreateDate.replace("{todaysDate}", strDate);

		vTgtRole = sharedStaticContent.getKeyMapCaseSentenceLangValue("level", "label", null, targ, lang);

		rpt = new KfapGroupReport();
		if (rptTitle != null && rptTitle.length() > 0) {
			rpt.setGroup(rptTitle);
			rpt.setGroupLabel(sharedStaticContent.getKeyMapValue4Lang("shared", "groupLabel", lang));
		} else {
			rpt.setGroup("");
			rpt.setGroupLabel("");
		}
		rpt.setLang(lang);
		rpt.setReportName(sharedStaticContent.getKeyMapValue4Lang("shared", "reportTitleGroupSlate", lang));
		rpt.setAssessmentTitle(sharedStaticContent.getKeyMapValue4Lang("shared", "reportName", lang));
		rpt.setConfidential(sharedStaticContent.getKeyMapValue4Lang("shared", "confidential", lang));
		rpt.setConfidentialWPeriod(sharedStaticContent.getKeyMapValue4Lang("shared", "confidentialWPeriod", lang));

		rpt.setClientLabel(sharedStaticContent.getKeyMapValue4Lang("shared", "clientLabel", lang));
		rpt.setTargetLabel(sharedStaticContent.getKeyMapValue4Lang("shared", "targetLabel", lang));
		rpt.setReportDateLabel(sharedStaticContent.getKeyMapValue4Lang("shared", "reportDateLabel", lang));

		rpt.setClient(vClient);
		rpt.setTarget(vTgtRole);
		rpt.setReportDate(strDate);

		rpt.setCopyright(sharedStaticContent.getKeyMapValue4Lang("shared", "copyright", lang));
		rpt.setProvidedBy(sharedStaticContent.getKeyMapValue4Lang("shared", "providedBy", lang));
		rpt.setCreateDate(createDate);

		if (NO_RAVENS) {
			rpt.setNoRavens(true);
		}

		rpt.setEndpageHeader(sharedStaticContent.getKeyMapValue4Lang("shared", "endpageHeader", lang));
		rpt.setEndpageP1(sharedStaticContent.getKeyMapValue4Lang("shared", "endpageP1", lang));
		rpt.setEndpageP2(sharedStaticContent.getKeyMapValue4Lang("shared", "endpageP2", lang));
		rpt.setEndpageP3(sharedStaticContent.getKeyMapValue4Lang("shared", "endpageP3", lang));

		// set report content for Overview Section
		AboutSection about = new AboutSection();
		about.setTitle(staticContent.getKeyMapValue4Lang("aboutSection", "title", lang));
		about.setP1(staticContent.getKeyMapValue4Lang("aboutSection", "p1", lang));
		about.setStrongIndicator(staticContent.getKeyMapValue4Lang("aboutSection", "strongIndicator", lang));
		about.setDevelopmentIndicator(staticContent.getKeyMapValue4Lang("aboutSection", "developmentIndicator", lang));
		about.setOptimalIndicator(staticContent.getKeyMapValue4Lang("aboutSection", "optimalIndicator", lang));
		about.setBelowOptimalIndicator(staticContent.getKeyMapValue4Lang("aboutSection", "belowOptimalIndicator", lang));
		about.setAboveOptimalIndicator(staticContent.getKeyMapValue4Lang("aboutSection", "aboveOptimalIndicator", lang));
		about.setDerailerIndicator(staticContent.getKeyMapValue4Lang("aboutSection", "derailerIndicator", lang));
		rpt.setAboutSection(about);

		// set report content for Definitions Section
		DefinitionsSection definitions = new DefinitionsSection();
		definitions.setTitle(staticContent.getKeyMapValue4Lang("definitionsSection", "title", lang));
		definitions.setInterestLabel(staticContent.getKeyMapValue4Lang("labels", "interestLabel", lang));
		definitions.setExperienceLabel(staticContent.getKeyMapValue4Lang("labels", "experienceLabel", lang));
		definitions.setSelfAwarenessLabel(staticContent.getKeyMapValue4Lang("labels", "selfAwarenessLabel", lang));
		definitions.setLearningAgilityLabel(staticContent.getKeyMapValue4Lang("labels", "learningAgilityLabel", lang));
		definitions
				.setLeadershipTraitsLabel(staticContent.getKeyMapValue4Lang("labels", "leadershipTraitsLabel", lang));
		definitions.setCapacityLabel(staticContent.getKeyMapValue4Lang("labels", "capacityLabel", lang));
		definitions.setDerailmentRisksLabel(staticContent.getKeyMapValue4Lang("labels", "derailmentRisksLabel", lang));

		definitions.setInterestShortText(staticContent.getKeyMapValue4Lang("definitionsSection", "interestShortText",
				lang));
		definitions.setExperienceShortText(staticContent.getKeyMapValue4Lang("definitionsSection",
				"experienceShortText", lang));
		definitions.setSelfAwareShortText(staticContent.getKeyMapValue4Lang("definitionsSection", "selfAwareShortText",
				lang));
		definitions.setLearningAgilityShortText(staticContent.getKeyMapValue4Lang("definitionsSection",
				"learningAgilityShortText", lang));
		definitions.setLeadershipTraitsShortText(staticContent.getKeyMapValue4Lang("definitionsSection",
				"leadershipTraitsShortText", lang));
		definitions.setCapacityShortText(staticContent.getKeyMapValue4Lang("definitionsSection", "capacityShortText",
				lang));
		definitions.setDerailmentShortText(staticContent.getKeyMapValue4Lang("definitionsSection",
				"derailmentShortText", lang));
		rpt.setDefinitionsSection(definitions);

		DetailedResultsSection detailedResults = new DetailedResultsSection();
		detailedResults.setTitle(staticContent.getKeyMapValue4Lang("detailedResultsSection", "title", lang));
		detailedResults.setNeedsLabel(staticContent.getKeyMapValue4Lang("labels", "needsLabel", lang));
		detailedResults.setStrengthLabel(staticContent.getKeyMapValue4Lang("labels", "strengthLabel", lang));
		detailedResults.setDevelopmentEffortLabel(staticContent.getKeyMapValue4Lang("labels", "developmentEffortLabel",
				lang));
		detailedResults.setIncompleteLabel(staticContent.getKeyMapValue4Lang("labels", "incompleteLabel", lang));
		detailedResults.setOptimalLabel(staticContent.getKeyMapValue4Lang("labels", "optimalLabel", lang));
		detailedResults.setBelowOptimalLabel(staticContent.getKeyMapValue4Lang("labels", "belowOptimalLabel", lang));
		detailedResults.setAboveOptimalLabel(staticContent.getKeyMapValue4Lang("labels", "aboveOptimalLabel", lang));
		detailedResults.setDerailerLabel(staticContent.getKeyMapValue4Lang("labels", "derailerLabel", lang));
		detailedResults.setInterestLabel(staticContent.getKeyMapValue4Lang("labels", "interestLabel", lang));
		detailedResults.setDriveLabel(staticContent.getKeyMapValue4Lang("labels", "driveLabel", lang));
		detailedResults.setFocusLabel(staticContent.getKeyMapValue4Lang("labels", "focusLabel", lang));
		detailedResults.setEngagementLabel(staticContent.getKeyMapValue4Lang("labels", "engagementLabel", lang));
		detailedResults.setExperienceLabel(staticContent.getKeyMapValue4Lang("labels", "experienceLabel", lang));
		detailedResults.setCoreLabel(staticContent.getKeyMapValue4Lang("labels", "coreLabel", lang));
		detailedResults.setPerspectiveLabel(staticContent.getKeyMapValue4Lang("labels", "perspectiveLabel", lang));
		detailedResults.setKeyChallengesLabel(staticContent.getKeyMapValue4Lang("labels", "keyChallengesLabel", lang));
		detailedResults.setSelfAwarenessLabel(staticContent.getKeyMapValue4Lang("labels", "selfAwarenessLabel", lang));
		detailedResults.setSelfLabel(staticContent.getKeyMapValue4Lang("labels", "selfLabel", lang));
		detailedResults.setSituationalLabel(staticContent.getKeyMapValue4Lang("labels", "situationalLabel", lang));
		detailedResults.setLearningAgilityLabel(staticContent.getKeyMapValue4Lang("labels", "learningAgilityLabel",
				lang));
		detailedResults.setPeopleLabel(staticContent.getKeyMapValue4Lang("labels", "peopleLabel", lang));
		detailedResults.setResultsLabel(staticContent.getKeyMapValue4Lang("labels", "resultsLabel", lang));
		detailedResults.setMentalLabel(staticContent.getKeyMapValue4Lang("labels", "mentalLabel", lang));
		detailedResults.setChangeLabel(staticContent.getKeyMapValue4Lang("labels", "changeLabel", lang));
		detailedResults.setLeadershipTraitsLabel(staticContent.getKeyMapValue4Lang("labels", "leadershipTraitsLabel",
				lang));
		detailedResults.setFocusedLabel(staticContent.getKeyMapValue4Lang("labels", "focusedLabel", lang));
		detailedResults.setSociableLabel(staticContent.getKeyMapValue4Lang("labels", "sociableLabel", lang));
		detailedResults.setToleranceLabel(staticContent.getKeyMapValue4Lang("labels", "toleranceLabel", lang));
		detailedResults.setComposedLabel(staticContent.getKeyMapValue4Lang("labels", "composedLabel", lang));
		detailedResults.setOptimisticLabel(staticContent.getKeyMapValue4Lang("labels", "optimisticLabel", lang));
		detailedResults.setCapacityLabel(staticContent.getKeyMapValue4Lang("labels", "capacityLabel", lang));
		detailedResults.setProblemSolveLabel(staticContent.getKeyMapValue4Lang("labels", "problemSolveLabel", lang));
		detailedResults.setDerailmentRisksLabel(staticContent.getKeyMapValue4Lang("labels", "derailmentRisksLabel",
				lang));
		detailedResults.setVolatileLabel(staticContent.getKeyMapValue4Lang("labels", "volatileLabel", lang));
		detailedResults.setMicroManageLabel(staticContent.getKeyMapValue4Lang("labels", "microManageLabel", lang));
		detailedResults.setClosedLabel(staticContent.getKeyMapValue4Lang("labels", "closedLabel", lang));

		Tables tables = new Tables();
		detailedResults.setTables(tables);
		Table table = new Table();
		Rows rows = new Rows();
		table.setRows(rows);

		String strNeeds = "";
		String strStrength = "";
		String strDevEffort = "";
		String strInterestScore = "";
		String strDrive = "";
		String strFocus = "";
		String strEngagement = "";
		String strExperienceScore = "";
		String strCore = "";
		String strPerspective = "";
		String strKeyChallenges = "";
		String strAwarenessScore = "";
		String strSelfAware = "";
		String strSituational = "";
		String strLearnAgile = "";
		String strPeople = "";
		String strResults = "";
		String strMental = "";
		String strChange = "";
		String strLeadTraits = "";
		String strFocused = "";
		String strSociable = "";
		String strTolerance = "";
		String strComposed = "";
		String strOptimistic = "";
		String strCapacity = "";
		String strProblemSolve = "";
		String strDerail = "";
		String strVolatile = "";
		String strMicro = "";
		String strClosed = "";
		String strPartId = "";

		for (int i = 0; i < returnedData.size(); i++) {

			Row rw = new Row();
			Object[] theData = returnedData.get(i).toArray();

			// System.out.println("PartIDinforLoop = " + theData[0]);
			strPartId = (String) theData[0];
			strNeeds = (String) theData[1];
			strStrength = (String) theData[2];
			strInterestScore = (String) theData[3];
			strDrive = (String) theData[4];
			strFocus = (String) theData[5];
			strEngagement = (String) theData[6];
			strExperienceScore = (String) theData[7];
			strCore = (String) theData[8];
			strPerspective = (String) theData[9];
			strKeyChallenges = (String) theData[10];
			strAwarenessScore = (String) theData[11];
			strSelfAware = (String) theData[12];
			strSituational = (String) theData[13];
			strLearnAgile = (String) theData[14];
			strPeople = (String) theData[15];
			strResults = (String) theData[16];
			strMental = (String) theData[17];
			strChange = (String) theData[18];
			strLeadTraits = (String) theData[19];
			strFocused = (String) theData[20];
			strSociable = (String) theData[21];
			strTolerance = (String) theData[22];
			strComposed = (String) theData[23];
			strOptimistic = (String) theData[24];
			strCapacity = (String) theData[25];
			strProblemSolve = (String) theData[26];
			strDerail = (String) theData[27];
			strVolatile = (String) theData[28];
			strMicro = (String) theData[29];
			strClosed = (String) theData[30];
			strDevEffort = (String) theData[31];

			User user = userDao.findById(Integer.parseInt(strPartId));

			rw.setNeedsCount(new BigInteger(strNeeds));
			rw.setStrengthCount(new BigInteger(strStrength));
			rw.setDevEffortScore(new BigInteger(strDevEffort));
			rw.setLastNameFirstName(user.getLastname() + ", " + user.getFirstname());

			if (strInterestScore.equals("1")) {
				rw.setIsInterestStregth(true);
			}

			rw.setDriveValue(strDrive);
			rw.setFocusValue(strFocus);
			rw.setEngagementValue(strEngagement);

			if (strExperienceScore.equals("1")) {
				rw.setIsExperienceStregth(true);
			}

			rw.setCoreValue(strCore);
			rw.setPerspectiveValue(strPerspective);
			rw.setKeyChallengesValue(strKeyChallenges);

			if (strAwarenessScore.equals("1")) {
				rw.setIsSelfAwarenessStregth(true);
			}

			rw.setSelfValue(strSelfAware);
			rw.setSituationalValue(strSituational);

			if (strLearnAgile.equals("1")) {
				rw.setIsLearningAgilityStregth(true);
			}

			rw.setPeopleValue(strPeople);
			rw.setResultsValue(strResults);
			rw.setMentalValue(strMental);
			rw.setChangeValue(strChange);

			if (strLeadTraits.equals("1")) {
				rw.setIsLeadershipTraitsStregth(true);
			}

			rw.setFocusedValue(strFocused);
			rw.setSociableValue(strSociable);
			rw.setToleranceValue(strTolerance);
			rw.setComposedValue(strComposed);
			rw.setOptimisticValue(strOptimistic);

			if (strCapacity.equals("1")) {
				rw.setIsCapacityStregth(true);
			}

			rw.setProblemSolveValue(strProblemSolve);

			if (strDerail.equals("1")) {
				rw.setIsDerailmentRisksStregth(true);
			}

			rw.setVolatileValue(strVolatile);
			rw.setMicroManageValue(strMicro);
			rw.setClosedValue(strClosed);

			table.getRows().getRow().add(rw);

		}

		// Now fill info for particpants that have not completed all
		// assessment(s)

		String strIncompletePartID = "";

		for (int k = 0; k < incompletGroupPIds.size(); k++) {

			strIncompletePartID = incompletGroupPIds.get(k).toString();

			User incompleteUser = userDao.findById(Integer.parseInt(strIncompletePartID));

			Row r = new Row();
			r.setNeedsCount(new BigInteger("-1"));
			r.setStrengthCount(new BigInteger("-1"));
			r.setDevEffortScore(new BigInteger("101"));
			r.setLastNameFirstName(incompleteUser.getLastname() + ", " + incompleteUser.getFirstname());
			r.setIsInterestStregth(false);
			r.setDriveValue("N/A");
			r.setFocusValue("N/A");
			r.setEngagementValue("N/A");
			r.setIsExperienceStregth(false);
			r.setCoreValue("N/A");
			r.setPerspectiveValue("N/A");
			r.setKeyChallengesValue("N/A");
			r.setIsSelfAwarenessStregth(false);
			r.setSelfValue("N/A");
			r.setSituationalValue("N/A");
			r.setIsLearningAgilityStregth(false);
			r.setPeopleValue("N/A");
			r.setResultsValue("N/A");
			r.setMentalValue("N/A");
			r.setChangeValue("N/A");
			r.setIsLeadershipTraitsStregth(false);
			r.setFocusedValue("N/A");
			r.setSociableValue("N/A");
			r.setToleranceValue("N/A");
			r.setComposedValue("N/A");
			r.setOptimisticValue("N/A");
			r.setIsCapacityStregth(false);
			r.setProblemSolveValue("N/A");
			r.setIsDerailmentRisksStregth(false);
			r.setVolatileValue("N/A");
			r.setMicroManageValue("N/A");
			r.setClosedValue("N/A");

			table.getRows().getRow().add(r);

		}

		detailedResults.getTables().getTable().add(table);

		rpt.setDetailedResultsSection(detailedResults);

		String rcm = "";
		rcm = marshalOutput(rpt);

		// test print rcm.xml string
		// System.out.println("RCM = " + rcm);

		@SuppressWarnings("unused")
		String rptUrl = configProperties.getProperty("reportserverBaseUrl") + "/GenerateReportFromRcm";

		return rcm; // RCM xml string
	}

	private String marshalOutput(KfapGroupReport KfapGroupReport) {
		JAXBContext jc = null;
		Marshaller m = null;
		StringWriter sw = new StringWriter();
		try {
			jc = JAXBContext.newInstance(KfapGroupReport.class);
			m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(KfapGroupReport, sw);

			return sw.toString();
		} catch (JAXBException jbe) {
			log.error("potential KfapGroupRgrToRcm.marshalOutput() - JAXBException: Msg={}", jbe.getMessage());
			jbe.printStackTrace();
			return null;
		}
	}

	private List<List<String>> mapInputData(String type, List<Integer> groupPIds, String lang, String targ)
			throws Exception {

		String urlBase = this.configProperties.getProperty("calculationRequestDataUrl");
		String calcScoreUrl = urlBase
				+ RESTFUL_STRING_PARAMS.replace("{instrumentId}", KFP_INST + "").replace("{manifest}", manifest)
						.replace("{tgtRole}", targ);

		String strPartId = "0";
		String datasetId = "";
		// Document mainDoc = null;
		Document subDoc = null;
		Document scoreDoc = null;
		Integer g = 0;

		for (int start = 0; start < groupPIds.size(); start += 20) {
			int end = Math.min(start + 20, groupPIds.size());
			List<Integer> sublist = groupPIds.subList(start, end);
			// System.out.println(sublist);

			// Create the CalculationPayload xml to send -- this will get the
			// data
			// needed below

			// for (int j = 0; j < groupPIds.size(); j++) {
			// strPartId = groupPIds.get(j).toString();
			// datasetId = datasetId + "<dataset id=\"" + strPartId + "\"/>";

			// }

			datasetId = "";
			for (int j = 0; j < sublist.size(); j++) {
				strPartId = sublist.get(j).toString();
				datasetId = datasetId + "<dataset id=\"" + strPartId + "\"/>";

			}

			String payload1 = "<CalculationPayload>\n";
			String payload2 = "</CalculationPayload>\n";

			String payload = payload1 + datasetId + payload2;

			System.out.println("payload sent =  " + payload);

			if (g == 0) {
				scoreDoc = getScores(calcScoreUrl, payload);
			} else {
				subDoc = getScores(calcScoreUrl, payload);
				scoreDoc = combineXML(scoreDoc, subDoc);
			}
			g = g + 1;
		}

		// Use code below to get the scoreDoc

		/*Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		// initialize StreamResult with File object to save to file
		StreamResult result = new StreamResult(new StringWriter());
		DOMSource source = new DOMSource(scoreDoc);
		transformer.transform(source, result);
		String xmlString = result.getWriter().toString();
		System.out.println(xmlString);*/

		List<List<String>> dataforRcm = new ArrayList<List<String>>();

		int strengthCount;
		int needsCount;
		String strDrive = "";
		String strFocus = "";
		String strEngagement = "";
		String strCore = "";
		String strPerspective = "";
		String strKeyChallenges = "";
		String strSelfAware = "";
		String strSituational = "";
		String strPeople = "";
		String strResults = "";
		String strMental = "";
		String strChange = "";
		String strFocused = "";
		String strSociable = "";
		String strTolerance = "";
		String strComposed = "";
		String strOptimistic = "";
		String strProblemSolve = "";
		String strVolatile = "";
		String strMicro = "";
		String strClosed = "";
		String strReturnedDrive = "";

		int tempCapScore = 0;

		strPartId = "0";

		for (int i = 0; i < groupPIds.size(); i++) {

			strPartId = groupPIds.get(i).toString();

			interestScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "interest.score")));

			experienceScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "experience.score")));
			agileScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "agile.score")));
			awarenessScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "awareness.score")));
			traitsScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "traits.score")));
			derailerScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "derailer.score")));

			String capacity = KfapUtils.getGroupScoredData(scoreDoc, strPartId, KFP_INST, "capacity.score");

			if (capacity == null || capacity == "" || capacity.equals("NaN")) {
				NO_RavensScore = true;
				capacityScore = 0;

			} else {
				capacityPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
						KFP_INST, "capacity.norm")));
				capacityScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
						KFP_INST, "capacity.score")));
				// explicitly set because when a report is regenerated it
				// doesn't
				// seem to pick up the boolean at the top of this file
				NO_RavensScore = false;

			}

			tempCapScore = capacityScore;

			strengthCount = interestScore + experienceScore + agileScore + awarenessScore + traitsScore + derailerScore
					+ tempCapScore;

			if (NO_RavensScore) {
				needsCount = 6 - strengthCount;

			} else {

				needsCount = 7 - strengthCount;
			}

			// percentile score for factors under scales. data range from 1-99
			drivePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId, KFP_INST,
					"interest.drive.norm")));
			focusPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId, KFP_INST,
					"interest.focus.norm")));
			engagementPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "interest.engagement.norm")));
			keyChallengePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "experience.key.norm")));
			perspectivePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "experience.perspective.norm")));
			changePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId, KFP_INST,
					"agile.change.norm")));
			mentalPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId, KFP_INST,
					"agile.mental.norm")));
			peoplePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId, KFP_INST,
					"agile.people.norm")));
			resultsPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "agile.results.norm")));
			slfAwarePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "awareness.self.norm")));
			sitAwarePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "awareness.sit.norm")));
			focusedPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "traits.focus.norm")));
			optimismPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "traits.optimism.norm")));
			composedPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "traits.composure.norm")));
			closedPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId, KFP_INST,
					"derailer.closed.norm")));
			microPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId, KFP_INST,
					"derailer.micro.norm")));
			volatilePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "derailer.volatile.norm")));
			corePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId, KFP_INST,
					"experience.core.norm")));
			tolerancePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "traits.tolerance.norm")));
			sociablePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId,
					KFP_INST, "traits.social.norm")));
			devEffort = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredData(scoreDoc, strPartId, KFP_INST,
					"dev.effort")));

			/*@SuppressWarnings("rawtypes")
			Map scoreMap = KfapUtils.getGroupScoredDataMap(scoreDoc, strPartId, KFP_INST, "resume");
			@SuppressWarnings("unchecked")
			Iterator<Entry<String, String>> it = scoreMap.entrySet().iterator();

			while (it.hasNext()) {
				Entry<String, String> pair = it.next();
				if (pair.getKey().equals("ITEM3")) {
					currRoleLevel = Integer.parseInt(pair.getValue());
				} else if (pair.getKey().equals("ITEM5")) {
					yrsInCurrRole = Integer.parseInt(pair.getValue());
				} else if (pair.getKey().equals("ITEM128")) {
					threeToFiveYrsGoal = Integer.parseInt(pair.getValue());
				} else if (pair.getKey().equals("ITEM129")) {
					jump = Integer.parseInt(pair.getValue());
				} else if (pair.getKey().equals("ITEM130")) {
					ltAspiration = Integer.parseInt(pair.getValue());
				}
			}*/

			// calculate whether the Drive Percentile is optimal or Below
			// Optimal
			// strReturnedDrive = getDriveString(yrsInCurrRole,
			// threeToFiveYrsGoal, ltAspiration);

			// strDrive = strReturnedDrive;

			if (drivePct < 50) {
				strDrive = "B";
			} else {
				strDrive = "O";
			}

			if (focusPct < 25) {
				strFocus = "B";
			} else {
				strFocus = "O";
			}

			if (engagementPct < 25) {
				strEngagement = "B";
			} else {
				strEngagement = "O";
			}

			if (corePct < 25) {
				strCore = "B";
			} else {
				strCore = "O";
			}

			if (perspectivePct < 25) {
				strPerspective = "B";
			} else {
				strPerspective = "O";
			}

			if (keyChallengePct < 25) {
				strKeyChallenges = "B";
			} else {
				strKeyChallenges = "O";
			}

			if (slfAwarePct < 25) {
				strSelfAware = "B";
			} else {
				strSelfAware = "O";
			}

			if (sitAwarePct < 25) {
				strSituational = "B";
			} else {
				strSituational = "O";
			}

			if (peoplePct < 25) {
				strPeople = "B";
			} else if (peoplePct > 90) {
				strPeople = "A";
			} else {
				strPeople = "O";
			}

			if (resultsPct < 25) {
				strResults = "B";
			} else if (resultsPct > 90) {
				strResults = "A";
			} else {
				strResults = "O";
			}

			if (mentalPct < 25) {
				strMental = "B";
			} else if (mentalPct > 90) {
				strMental = "A";
			} else {
				strMental = "O";
			}

			if (changePct < 25) {
				strChange = "B";
			} else if (changePct > 90) {
				strChange = "A";
			} else {
				strChange = "O";
			}

			if (focusedPct < 25) {
				strFocused = "B";
			} else if (focusedPct > 90) {
				strFocused = "A";
			} else {
				strFocused = "O";
			}

			if (sociablePct < 25) {
				strSociable = "B";
			} else if (sociablePct > 90) {
				strSociable = "A";
			} else {
				strSociable = "O";
			}

			if (tolerancePct < 25) {
				strTolerance = "B";
			} else if (tolerancePct > 90) {
				strTolerance = "A";
			} else {
				strTolerance = "O";
			}

			if (composedPct < 25) {
				strComposed = "B";
			} else if (composedPct > 90) {
				strComposed = "A";
			} else {
				strComposed = "O";
			}

			if (optimismPct < 25) {
				strOptimistic = "B";
			} else if (optimismPct > 90) {
				strOptimistic = "A";
			} else {
				strOptimistic = "O";
			}

			if (capacityPct < 25) {
				strProblemSolve = "B";
			} else if (capacityPct > 90) {
				strProblemSolve = "A";
			} else {
				strProblemSolve = "O";
			}

			if (NO_RavensScore) {
				strProblemSolve = "N/A";
			}

			if (volatilePct < 85) {
				strVolatile = "O";
			} else {
				strVolatile = "D";
			}

			if (microPct < 85) {
				strMicro = "O";
			} else {
				strMicro = "D";
			}

			if (closedPct < 85) {
				strClosed = "O";
			} else {
				strClosed = "D";
			}

			// devEffort shouldn't go over 100 so set back to 100 if over

			if (devEffort > 100) {
				devEffort = 100;
			}

			//
			// DevEffort score calculated below -- now comes from Calculation
			// Payload (scoreDoc)

			// int sum;
			// double tltScore;
			// double devScore;
			// double devScorePct;
			// int devEffortScore;

			// sum = calcGap(drivePct) + calcGap(focusPct) +
			// calcGap(engagementPct) + calcGap(corePct)
			// + calcGap(perspectivePct) + calcGap(keyChallengePct) +
			// calcGap(slfAwarePct) + calcGap(sitAwarePct)
			// + calcGap(peoplePct) + calcGap(changePct) + calcGap(resultsPct) +
			// calcGap(mentalPct)
			// + calcGap(focusedPct) + calcGap(sociablePct) +
			// calcGap(tolerancePct) + calcGap(composedPct)
			// + calcGap(optimismPct) + volatilePct + microPct + closedPct;

			// if (NO_RAVENS) {
			// tltScore = 1660;
			// } else {
			// tltScore = 1740;
			// sum = sum + calcGap(capacityPct);
			// }

			// sum = Math.abs(sum);

			// devScore = (sum / tltScore);

			// devScorePct = devScore * 100;

			// devEffortScore = (int) Math.round(devScorePct);

			// ##############################################################

			// 0 = strPartId, 1 = strNeedsCount, 2 = strStrengthCount 3 =
			// interestScore, 4 = strDrive, 5 = strFocus, 6 = strEngagement 7 =
			// experienceScore, 8 = strCore, 9 = strPerspective, 10 =
			// strKeyChallenges, 11 = awarenessScore, strSelfAware = 12 ,
			// strSituational = 13, agileScore = 14, strPeople = 15 , strResults
			// = 16, strMental = 17, strChange = 18, traitsScore = 19,
			// strFocused = 20, strSociable = 21, strTolerance = 22, strComposed
			// = 23 , strOptimistic = 24, capacityScore = 25, strProblemSolve =
			// 26, derailerScore = 27, strVolatile = 28, strMicro = 29,
			// strClosed = 30, devEffort = 31

			dataforRcm.add(Arrays.asList(strPartId, needsCount + "", strengthCount + "", interestScore + "", strDrive,
					strFocus, strEngagement, experienceScore + "", strCore, strPerspective, strKeyChallenges,
					awarenessScore + "", strSelfAware, strSituational, agileScore + "", strPeople, strResults,
					strMental, strChange, traitsScore + "", strFocused, strSociable, strTolerance, strComposed,
					strOptimistic, capacityScore + "", strProblemSolve, derailerScore + "", strVolatile, strMicro,
					strClosed, devEffort + ""));

			/*log.debug("strPartId = " + strPartId);
			log.debug("interestScore = " + interestScore);
			log.debug("experienceScore = " + experienceScore);
			log.debug("agileScore = " + agileScore);
			log.debug("awarenessScore = " + awarenessScore);
			log.debug("traitsScore = " + traitsScore);
			log.debug("derailerScore = " + derailerScore);

			if (NO_RAVENS) {
				log.debug("capacityScore = N/A");
			} else {
				log.debug("capacityScore = " + capacityScore);
			}

			// log.debug("devEffortScore = " + devEffortScore);
			log.debug("drivePct = " + drivePct);
			log.debug("focusPct = " + focusPct);
			log.debug("engagementPct = " + engagementPct);
			log.debug("perspectivePct = " + perspectivePct);
			log.debug("changePct = " + changePct);
			log.debug("mentalPct = " + mentalPct);
			log.debug("peoplePct = " + peoplePct);
			log.debug("resultsPct = " + resultsPct);
			log.debug("slfAwarePct = " + slfAwarePct);
			log.debug("sitAwarePct = " + sitAwarePct);
			log.debug("focusedPct = " + focusedPct);
			log.debug("optimismPct = " + optimismPct);
			log.debug("composedPct = " + composedPct);
			log.debug("closedPct = " + closedPct);
			log.debug("microPct = " + microPct);
			log.debug("volatilePct = " + volatilePct);
			log.debug("corePct = " + corePct);
			log.debug("tolerancePct = " + tolerancePct);
			log.debug("sociablePct = " + sociablePct);
			log.debug("keyChallengePct = " + keyChallengePct);
			log.debug("capacityPct = " + capacityPct);
			log.debug("strengthCount = " + strengthCount);
			log.debug("devEffort = " + devEffort);*/

		}

		return dataforRcm;

	}

	private Document getRScores(String strUrl, String payload) throws Exception {
		Document doc = null;
		// default value 600000 , 10 minutes
		int connTimeout = Integer.valueOf(configProperties.getProperty("calculationPlatformTimeout", "600000"));
		try {
			AsyncHttpClientConfig config;
			AsyncHttpClientConfig.Builder builder = new AsyncHttpClientConfig.Builder();
			builder.setConnectionTimeoutInMs(connTimeout);// 10 min
			builder.setRequestTimeoutInMs(connTimeout);// 10 min
			builder.setIdleConnectionInPoolTimeoutInMs(connTimeout); // 10 min
			builder.setIdleConnectionTimeoutInMs(connTimeout); // 10 minutes
																// IMPORTANT!
																// fixed
																// KFP-409
			config = builder.build();
			log.debug("Timeout set to {} milliseconds", connTimeout);
			AsyncHttpClient asyncHttpClient = new AsyncHttpClient(config);

			Future<Response> f = asyncHttpClient.preparePost(strUrl).setHeader("Content-Type", "application/xml")
					.setBody(payload).execute();

			Response r = f.get();
			String returnedData = r.getResponseBody();
			asyncHttpClient.close();
			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				doc = docBuilder.parse(new InputSource(new ByteArrayInputStream(returnedData.getBytes())));
			} catch (ParserConfigurationException e) {
				log.error("Parser error creating document from ext. server response. Message: {}", e.getMessage());
				// e.printStackTrace();
				return null;
			} catch (IOException e) {
				log.error("IO error creating document from ext. server response. Message: {}", e.getMessage());
				// e.printStackTrace();
				return null;
			} catch (SAXException e) {
				log.error("SAX error creating document from ext. server response. Message: {}", e.getMessage());
				// e.printStackTrace();
				return null;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return doc;
	}

	@SuppressWarnings({ "restriction" })
	private Document getScores(String strUrl, String payload) throws Exception {
		Document doc = null;
		HttpURLConnection conn = null;
		URL url = null;
		String line = "";
		String xml = "";

		try {
			url = new URL(strUrl);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/xml");
			conn.setDoOutput(true);

			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

			writer.write(payload);
			writer.flush();

			// Get Response
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while ((line = in.readLine()) != null) {
				xml += line;
			}
			in.close();
			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				doc = docBuilder.parse(new InputSource(new ByteArrayInputStream(xml.getBytes())));
			} catch (ParserConfigurationException e) {
				log.error("Parser error creating document from ext. server response. Message: {}", e.getMessage());
				// e.printStackTrace();
				return null;
			} catch (IOException e) {
				log.error("IO error creating document from ext. server response. Message: {}", e.getMessage());
				// e.printStackTrace();
				return null;
			} catch (SAXException e) {
				log.error("SAX error creating document from ext. server response. Message: {}", e.getMessage());
				// e.printStackTrace();
				return null;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return doc;
	}

	private String getDriveString(Integer yrsInCurrRole, Integer threeToFiveYrsGoal, Integer ltAspiration) {

		String driveString = "";
		int a = yrsInCurrRole;
		int b = threeToFiveYrsGoal;
		int d = 1;

		// lower target level numbers are actually higher levels so the logic is
		// reversed

		// d = 1 = A < T
		// d = 0 = A >= T

		if (ltAspiration > tgtRole) {
			d = 1;
		} else {
			d = 0;
		}

		// Below is for 25 Percentile cutoff
		/*if (a >= 3 && b == 4 && d == 1) {
			driveString = "B";
		} else if (a >= 3 && b == 5 && d == 1) {
			driveString = "B";
		} else if (a >= 3 && b == 6 && d == 1) {
			driveString = "B";
		} else if (a >= 3 && b == 7 && d == 1) {
			driveString = "B";
		} else if (a >= 3 && b == 8 && d == 1) {
			driveString = "B";
		} else if (a >= 3 && b == 8 && d == 0) {
			driveString = "B";
		} else if (a < 3 && b == 5 && d == 1) {
			driveString = "B";
		} else if (a < 3 && b == 6 && d == 1) {
			driveString = "B";
		} else if (a < 3 && b == 7 && d == 1) {
			driveString = "B";
		} else if (a < 3 && b == 8 && d == 1) {
			driveString = "B";
		} else if (a < 3 && b == 8 && d == 0) {
			driveString = "B";
		} else {
			driveString = "O";
		}*/

		// Below is for 50 Percentile cutoff
		if (a >= 3 && b == 2 && d == 1) {
			driveString = "B";
		} else if (a >= 3 && b == 3 && d == 1) {
			driveString = "B";
		} else if (a >= 3 && b == 4 && d == 1) {
			driveString = "B";
		} else if (a >= 3 && b == 5 && d == 1) {
			driveString = "B";
		} else if (a >= 3 && b == 6 && d == 1) {
			driveString = "B";
		} else if (a >= 3 && b == 7 && d == 1) {
			driveString = "B";
		} else if (a >= 3 && b == 8 && d == 1) {
			driveString = "B";
		} else if (a >= 3 && b == 4 && d == 0) {
			driveString = "B";
		} else if (a >= 3 && b == 5 && d == 0) {
			driveString = "B";
		} else if (a >= 3 && b == 6 && d == 0) {
			driveString = "B";
		} else if (a >= 3 && b == 7 && d == 0) {
			driveString = "B";
		} else if (a >= 3 && b == 8 && d == 0) {
			driveString = "B";
		} else if (a < 3 && b == 2 && d == 1) {
			driveString = "B";
		} else if (a < 3 && b == 3 && d == 1) {
			driveString = "B";
		} else if (a < 3 && b == 4 && d == 1) {
			driveString = "B";
		} else if (a < 3 && b == 5 && d == 1) {
			driveString = "B";
		} else if (a < 3 && b == 6 && d == 1) {
			driveString = "B";
		} else if (a < 3 && b == 7 && d == 1) {
			driveString = "B";
		} else if (a < 3 && b == 8 && d == 1) {
			driveString = "B";
		} else if (a < 3 && b == 4 && d == 0) {
			driveString = "B";
		} else if (a < 3 && b == 5 && d == 0) {
			driveString = "B";
		} else if (a < 3 && b == 6 && d == 0) {
			driveString = "B";
		} else if (a < 3 && b == 7 && d == 0) {
			driveString = "B";
		} else if (a < 3 && b == 8 && d == 0) {
			driveString = "B";
		} else {
			driveString = "O";
		}

		return driveString;
	}

	private Document combineXML(Document mainDoc, Document subDoc) throws PalmsException {

		try {

			NodeList nodes = mainDoc.getElementsByTagName("calculationPayload");

			NodeList nodes1 = subDoc.getElementsByTagName("calculationPayload");

			for (int i = 0; i < nodes1.getLength(); i = i + 1) {

				Node n = mainDoc.importNode(nodes1.item(i), true);
				nodes.item(i).getParentNode().appendChild(n);
			}

		} catch (Exception e1) {
			throw new PalmsException("Problem combining XML Payloads.", PalmsErrorCode.RCM_GEN_FAILED.getCode(),
					e1.getMessage());
		}

		return mainDoc;
	}

	public static String getReportManifestDir() {
		return reportManifestDir;
	}

	public static void setReportManifestDir(String reportManifestDir) {
		KfapGroupRgrToRcm.reportManifestDir = reportManifestDir;
	}

	public static String getReportContentDir() {
		return reportContentDir;
	}

	public static void setReportContentDir(String reportContentDir) {
		KfapGroupRgrToRcm.reportContentDir = reportContentDir;
	}

	private int calcGap(int pct) {
		return 99 - pct;
	}

}
