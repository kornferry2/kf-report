/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.VoucherIdServiceLocal;
import com.pdinh.data.ms.dao.PositionDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.pipeline.generator.helpers.PipelineTransformHelper;
import com.pdinh.pipeline.generator.helpers.ReportManifestHelper;
import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.pipeline.manifest.vo.ReportManifest;
import com.pdinh.reportcontent.Manifest;
import com.pdinh.reportgenerationresponse.ReportGenerationResponse;

/*
 * Scoring for the LVA/GPS scorecards
 */

@Stateless
@LocalBean
public class LvaMllScoreBean {
	private static final Logger log = LoggerFactory.getLogger(LvaMllScoreBean.class);
	//
	// Static data.
	//

	// public static int ALLSCORE = 0; // Score results are Shell LAR RCM XML
	// // format
	// public static int CONSULTANT = 1; // Score results to IbReport object
	// public static int TLTUPDATE = 2; // Score results are Shell LAR RCM XML
	// // format

	private static int DEV_RCM = 0; // Development RCM is first
	private static int REDI_RCM = 1; // Readiness RCM is second
	private static int FIT_RCM = 2; // FIT RCM is third

	static private boolean debugging = false;
	static private String cerPath;

	static private String lvaManifest = "lvaManifest.xml";
	static private String consManifest = "lvaManifestConsultant.xml";
	static private String tltManifest = "lvaManifestTltUpdate.xml";
	static private String petsmartManifest = "lvaPetsmartManifest.xml";
	static private String consPetsmartManifest = "lvaPetsmartManifestConsultant.xml";

	private static String VOUCHER_NOT_AVAILABLE = "NotAvailable";

	//
	// Beans & Resources
	//
	@Resource(mappedName = "custom/generator_properties")
	private Properties generatorProperties;

	@Resource(mappedName = "custom/lva_properties")
	private Properties lvaProperties;

	@Resource(mappedName = "application/report/config_properties")
	private Properties configProperties;

	@EJB
	private ProjectUserDao projectUserDao;
	@EJB
	private PositionDao positionDao;
	@EJB
	private VoucherIdServiceLocal voucherIdService;

	//
	// Constructor
	//
	public LvaMllScoreBean() {

	}

	//
	// Instance methods.
	//

	/**
	 * scoreLva - Invoke the Calculation pipeline with the LVA scoring manifest
	 * 
	 * @param inputRgr
	 *            A ReportGenerationRequest with responses only (not yet scored
	 *            with the target manifest)
	 * @return A ReportGenerationRequest object with the scored values included
	 */
	public ReportGenerationRequest scoreLva(ReportGenerationRequest inputRgr) {

		ReportGenerationRequest rgr = new ReportGenerationRequest();
		String mf = getClientSpecificCalculationManifest(lvaManifest, inputRgr);
		rgr = score(mf, inputRgr);

		return rgr;
	}

	/**
	 * scoreConsultantComps - Invoke the Calculation pipeline with the
	 * Consultant scoring manifest. Uses competency scores input by the
	 * consultant.
	 * 
	 * @param inputRgr
	 *            A ReportGenerationRequest with responses only (not yet scored
	 *            with the target manifest)
	 * @return A ReportGenerationRequest object with the scored values included
	 */
	public ReportGenerationRequest scoreConsultantComps(ReportGenerationRequest inputRgr) {

		ReportGenerationRequest rgr = new ReportGenerationRequest();
		String mf = getClientSpecificCalculationManifest(consManifest, inputRgr);
		rgr = score(mf, inputRgr);

		return rgr;
	}

	/**
	 * scoreTlt - Invoke the Calculation pipeline with the TLT scoring manifest.
	 * Uses TLT inputs from the consultant.
	 * 
	 * @param inputRgr
	 *            A ReportGenerationRequest with responses only (not yet scored
	 *            with the target manifest)
	 * @return A ReportGenerationRequest object with the scored values included
	 */
	public ReportGenerationRequest scoreTlt(ReportGenerationRequest inputRgr) {
		ReportGenerationRequest rgr = new ReportGenerationRequest();
		rgr = score(tltManifest, inputRgr);

		return rgr;
	}

	/**
	 * score - The private methods that is the core of the scoring process.
	 * Invokes the Calculation pipeline with a passes manifest name and input
	 * RGR.
	 * 
	 * @param mf
	 *            The name of the manifest to use in scoring
	 * @param inputRgr
	 *            The input RGR to be scored
	 * @return A ReportGenerationRequest object containing the input RGR plus
	 *         added scores.
	 */
	private ReportGenerationRequest score(String mf, ReportGenerationRequest inputRgr) {
		cerPath = generatorProperties.getProperty("calculationEngineResources");

		ReportManifestHelper rmh = new ReportManifestHelper(debugging, cerPath);
		ReportManifest rm = rmh.getReportManifest("lva", mf);

		if (rm == null) {
			log.warn("LvaMllScoreBean - score:  Unable to fetch calculation pipeline manifest.");
			return null;
		}

		PipelineTransformHelper pth = new PipelineTransformHelper(debugging, cerPath);
		String respXml = convertRgrToXmlString(inputRgr);

		if (respXml == null) {

			log.warn("LvaMllScoreBean - score:  Calculation pipeline did not return scored data.");
			return null;
		}

		String output = pth.process(rm, respXml);

		System.out.println("=== RGR2 OUTPUT ===");
		System.out.println(output);

		ReportGenerationRequest rgrOutput = null;
		if (output != null) {
			rgrOutput = convertXmlStringToRgr(output);
		}

		return rgrOutput;
	}

	/**
	 * convertXmlStringToRgr - Unmarshal an XML String to the LVA RGR object
	 * stack
	 * 
	 * @param xml
	 *            The input RGR-formatted XML
	 * @return The ReportGenerationRequest object stack embodied in the input
	 *         XML
	 */
	public ReportGenerationRequest convertXmlStringToRgr(String xml) {
		InputStream is = null;
		JAXBContext jc = null;
		Unmarshaller um = null;
		ReportGenerationRequest rgr = null;

		try {
			is = new ByteArrayInputStream(xml.getBytes("UTF-8"));

			// Create the context & the unmarshaller
			jc = JAXBContext.newInstance(ReportGenerationRequest.class);
			um = jc.createUnmarshaller();
			rgr = (ReportGenerationRequest) um.unmarshal(is);

			return rgr;
		} catch (UnsupportedEncodingException e) {
			log.error("Invalid encoding for unmarshalling scored XML. ", e.getMessage());
			return null;
		} catch (JAXBException e) {
			log.error("Error unmarshalling scored XML.  ", e.getMessage());
			return null;
		} finally {
			um = null;
			jc = null;
			try {
				is.close();
			} catch (Exception e) {
				/* Swallow it */
			}
		}
	}

	/**
	 * convertRgrToXmlString - Marshal an LVA/GPS RGR object stack to XML
	 * 
	 * @param rgr
	 *            The RGR object stack to be marshalled
	 * @return The XML string
	 */
	public String convertRgrToXmlString(ReportGenerationRequest rgr) {
		JAXBContext jc = null;
		String xml = null;

		try {
			jc = JAXBContext.newInstance(ReportGenerationRequest.class);
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter sw = new StringWriter();
			m.marshal(rgr, sw);
			xml = sw.toString();

		} catch (JAXBException e) {
			log.error("Failed to marshal RGR into XML.  ", e.getMessage());
			e.printStackTrace();

		}
		return xml;
	}

	// ---------------- RCM stuff ------------------

	/**
	 * createLvaRcmXML - Create an LVA RCM XML string
	 * 
	 * @param xml
	 *            - A ReportGenerationRequest XML string
	 * @return An LVA RCM XML string
	 */
	public List<String> createLvaRcmXML(String rgrXml) {
		// Create the ReportGenerationResponse object from the RGR XML string...
		ReportGenerationResponse resp = new ReportGenerationResponse(rgrXml);

		return createLvaRcmXML(resp);
	}

	/**
	 * createLvaRcmXML - Create an LVA RCM XML string
	 * 
	 * @param xml
	 *            - A ReportGenerationRequest Object stack
	 * @return An LVA RCM XML string
	 */
	public List<String> createLvaRcmXML(ReportGenerationRequest rgrObject) {
		// Create the ReportGenerationResponse Object from an RGR object...
		ReportGenerationResponse resp = new ReportGenerationResponse(rgrObject);

		return createLvaRcmXML(resp);

	}

	/**
	 * createLvaRcmXML - Create the RCM XML for the reports (dev and readiness)
	 * 
	 * @param resp
	 *            The ReportGenerationResponse containing RCM data
	 * @return A list of the RCMs created (XML strings)
	 */
	public List<String> createLvaRcmXML(ReportGenerationResponse resp) {
		// Set up return array. Size of the output is 3; 1 for dev & one for
		// readiness, and now 1 for Fit
		List<String> ret = new ArrayList<String>(3);

		// Get some IDs then get the relevant ProjectUser object.
		String pptExtId = resp.getNodeAttribute("participant", "extId");
		String projExtId = resp.getNodeAttribute("project", "extId");

		ProjectUser pu = projectUserDao.findByProjectIdAndUserId(Integer.parseInt(projExtId),
				Integer.parseInt(pptExtId));

		// Get the position list for this ppt and proj
		List<Position> positions = positionDao.findPositionsByUsersIdAndProjectId(Integer.parseInt(pptExtId),
				Integer.parseInt(projExtId));

		// Get the voucher ID for this ppt
		// String voucherId =
		// voucherIdService.getVoucherIdByUserId(Integer.parseInt(pptExtId));
		// voucherId for lms launches is now the company id
		String voucherId = String.valueOf(pu.getUser().getCompany().getCompanyId());

		if (voucherId == null)
			voucherId = VOUCHER_NOT_AVAILABLE;
		// System.out.println("For ppt=" + pptExtId +
		// ", Voucher ID from service=" + voucherId);

		// Data all gathered... instantiate the needed generation objects...
		// We might be able to do this with one instantiation, but lets do two
		LvaMllRgrToRcm lvaMllRgrToRcmDevelopment = new LvaMllRgrToRcm(lvaProperties, configProperties, pu, positions,
				voucherId);
		LvaMllRgrToRcm lvaMllRgrToRcmReadiness = new LvaMllRgrToRcm(lvaProperties, configProperties, pu, positions,
				voucherId);
		LvaMllRgrToRcm lvaMllRgrToRcmFit = new LvaMllRgrToRcm(lvaProperties, configProperties, pu, positions, voucherId);

		// ...get the instance-specific stuff...
		// hard-code path for now
		String manifestPath = configProperties.getProperty("reportContentRepository") + "/manifest/"
				+ pu.getProject().getReportContentManifestRef() + "/manifest.xml";
		Manifest manifest = new Manifest(manifestPath);

		// ...and call it once for each type
		ret.add(DEV_RCM, lvaMllRgrToRcmDevelopment.generateLvaRcmXmlXX(resp, DEV_RCM, manifest));
		ret.add(REDI_RCM, lvaMllRgrToRcmReadiness.generateLvaRcmXmlXX(resp, REDI_RCM, manifest));
		ret.add(FIT_RCM, lvaMllRgrToRcmFit.generateLvaRcmXmlXX(resp, FIT_RCM, manifest));

		return ret;
	}

	/**
	 * lookup a client specific calculation pipeline manifest
	 * 
	 * I initial thought it could have been no hard coded manifest names by
	 * looking up the project/user setup path. But since there is no control
	 * from pipeline on what path will be added and for what purpose in project
	 * setup process, it will be in a risk to use path as manifest names. we
	 * therefore use a specific name to lookup to make the call robust. For now
	 * there are two sets of calculation manifests - one for lva in general and
	 * the other is the PetSmart client specific set of manifests.
	 * 
	 * @param string
	 *            : manifest name
	 * @param ReportGenerationRequest
	 *            : input RGR1
	 * @return A manifest string either a client specific or standard lva
	 *         manifest
	 */

	private String getClientSpecificCalculationManifest(String manifest, ReportGenerationRequest rgr1) {
		String clientSpecifiedManifest = "";
		com.pdinh.pipeline.generator.vo.Participant pt = rgr1.getParticipant().get(0);
		String extPid = pt.getExtId();
		String extProj = pt.getProject().getExtId();

		String specifiedClient = null;
		ProjectUser pu = projectUserDao.findByProjectIdAndUserId(Integer.parseInt(extProj), Integer.parseInt(extPid));
		if (pu != null) {
			specifiedClient = pu.getProject().getReportContentManifestRef();
		}

		if (specifiedClient != null) {
			if (manifest.toLowerCase().contains("consultant")) {
				if (specifiedClient.contains("petsmart")) {
					clientSpecifiedManifest = consPetsmartManifest;
				} else {
					clientSpecifiedManifest = consManifest;
				}
			} else {
				if (specifiedClient.contains("petsmart")) {
					clientSpecifiedManifest = petsmartManifest;
				} else {
					clientSpecifiedManifest = lvaManifest;
				}
			}
		} else {
			if (manifest.toLowerCase().contains("consultant")) {
				clientSpecifiedManifest = consManifest;
			} else {
				clientSpecifiedManifest = lvaManifest;
			}
		}
		return clientSpecifiedManifest;
	}

	// Getters and setters for properties and daos
	public Properties getLvaProperties() {
		return lvaProperties;
	}

	public void setLvaProperties(Properties lvaProperties) {
		this.lvaProperties = lvaProperties;
	}

	public Properties getConfigProperties() {
		return configProperties;
	}

	public void setConfigProperties(Properties configProperties) {
		this.configProperties = configProperties;
	}

	public ProjectUserDao getProjectUserDao() {
		return projectUserDao;
	}

	public void setProjectUserDao(ProjectUserDao projectUserDao) {
		this.projectUserDao = projectUserDao;
	}

	public PositionDao getPositionDao() {
		return positionDao;
	}

	public void setPositionDao(PositionDao positionDao) {
		this.positionDao = positionDao;
	}

	public VoucherIdServiceLocal getVoucherIdServiceLocal() {
		return voucherIdService;
	}

	public void setVoucherIdServiceLocal(VoucherIdServiceLocal voucherIdService) {
		this.voucherIdService = voucherIdService;
	}
}
