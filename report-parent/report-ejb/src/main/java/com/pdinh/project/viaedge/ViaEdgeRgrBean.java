/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.viaedge;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import sun.net.www.protocol.http.HttpURLConnection;

import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.exception.ServerUnavailableException;
import com.pdinh.pipeline.generator.vo.GroupStage;
import com.pdinh.pipeline.generator.vo.Instrument;
import com.pdinh.pipeline.generator.vo.Participant;
import com.pdinh.pipeline.generator.vo.Project;
import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.pipeline.generator.vo.Stage;
import com.pdinh.pipeline.generator.vo.Var;
import com.pdinh.report.util.XmlUtil;
import com.pdinh.reportserver.data.dao.jpa.ReportRequestResponseDao;
import com.pdinh.reportserver.persistence.jpa.ReportRequestResponse;

/*
 * Creates RGR with response data in it for viaEdge
 */

@Stateless
@LocalBean
public class ViaEdgeRgrBean {

	private static String VIA_EDGE_INSTRUMENT_ID = "vedge";
	private static String LEARNING_AGILITY_INSTRUMENT_ID = "lagil";
	private static String QUERY_STRING_PARAMS = "?participantId={participantId}&instrumentId={instrumentId}";
	private static final Logger log = LoggerFactory.getLogger(ViaEdgeRgrBean.class);

	@Resource(mappedName = "application/report/config_properties")
	private Properties reportConfigProperties;

	@EJB
	private ReportRequestResponseDao reportResponseDao;

	private static String RGR_TYPE = "VIAEDGE_INDIV";

	public ViaEdgeRgrBean() {
	}

	public ReportGenerationRequest createIndividualReportRgr(int extPtId) throws PalmsException{
		String urlBase = this.reportConfigProperties.getProperty("tinCanAdminResponseDataUrl");
		return createIndividualReportRgrInternal(extPtId, urlBase, VIA_EDGE_INSTRUMENT_ID);
	}

	public ReportGenerationRequest createIndividualReportRgrScorm(int extPtId) throws PalmsException{
		String urlBase = this.reportConfigProperties.getProperty("scormResponseDataUrl");
		return createIndividualReportRgrInternal(extPtId, urlBase, LEARNING_AGILITY_INSTRUMENT_ID);
	}

	public ReportGenerationRequest createIndividualReportRgrInternal(int extPtId, String responseDataUrlBase,
			String instrumentId) throws PalmsException{
		ReportGenerationRequest rgr = new ReportGenerationRequest();

		// We don't go through scorecard workflow so all we have is external IDs
		// to set
		Participant ppt = new Participant();
		rgr.getParticipant().add(ppt);
		ppt.setExtId(extPtId + "");

		Project proj = new Project();
		proj.setExtId("");
		ppt.setProject(proj);

		String url = responseDataUrlBase
				+ QUERY_STRING_PARAMS.replace("{participantId}", extPtId + "").replace("{instrumentId}", instrumentId);
		log.debug("Response data fetch URL= {}", url);

		Document doc = getIndividualReportResponseData(url);
		fillIndividualReportResponseData(ppt, doc);
		return rgr;
	}

	public ReportGenerationRequest createGroupReportRgr(List<Integer> participants) throws PalmsException{
		ReportGenerationRequest rgr = new ReportGenerationRequest();
		GroupStage groupStage = new GroupStage();
		rgr.getGroupStage().add(groupStage);

		Document doc;
		// KS- a participant may or may not completed her or his survey. When a
		// participant doesn't finish
		// the survey, in order to display participant info in the report
		// without score, we need to
		// create a rgr2 info for this participant with "undefined" scores
		
		//batch retrieve, much more efficient with many ppts -- nw
		List<ReportRequestResponse> rgr2s = this.reportResponseDao.findBatchByParticipantIdAndRgrType(
				participants, ReportRequestResponse.VIAEDGE_INDIVIDUALS);

		if (!(rgr2s == null)){
			log.debug("{} ppts, {} rgrs", participants.size(), rgr2s.size());
		}else{
			log.debug("No rgrs found");
			rgr2s = new ArrayList<ReportRequestResponse>();
		}
		Iterator<Integer> it = participants.iterator();
		while (it.hasNext()) {
			int value = it.next();
			// ReportRequestResponse rgr2 =
			// this.reportResponseDao.findSingleByParticipantId(value);
			//ReportRequestResponse rgr2 = this.reportResponseDao.findSingleByParticipantIdAndType(value,
				//	ReportRequestResponse.VIAEDGE_INDIVIDUALS);
			// log.info(" ks-rgr2:"+ JaxbUtil.marshalToXmlString(rgr2,
			// ReportRequestResponse.class));
			ReportRequestResponse rgr2 = null;
			for(ReportRequestResponse rrr : rgr2s){
				
				if (value == rrr.getParticipantId()){
					rgr2 = rrr;
					break;
				}
				
			}

			Participant ppt = new Participant();
			rgr.getParticipant().add(ppt);
			ppt.setExtId(value + "");

			if (rgr2 != null) {
				doc = getGroupReportResponseData(rgr2);
				fillGroupReportResponseData(ppt, doc);
			} else {
				doc = null;
				fillGroupReportWithoutResponseData(ppt);
			}
		}
		
		return rgr;
	}

	private Document getIndividualReportResponseData(String urlStr) throws PalmsException{
		Document doc = null;
		try {
			URL url = new URL(urlStr);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			String line = "";
			String xml = "";
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while ((line = in.readLine()) != null) {
				xml += line;
			}
			in.close();
			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				doc = docBuilder.parse(new InputSource(new ByteArrayInputStream(xml.getBytes())));
			} catch (ParserConfigurationException e) {
				log.error("Parser error creating document from ext. server response. Message: {}", e.getMessage());
				throw new PalmsException("Parser error creating document from ext. server response.", PalmsErrorCode.REPORT_GEN_FAILED.getCode(), e.getMessage());
			} catch (IOException e) {
				log.error("IO error creating document from ext. server response. Message: {}", e.getMessage());
				throw new PalmsException("IO error creating document from ext. server response.", PalmsErrorCode.REPORT_GEN_FAILED.getCode(), e.getMessage());
			} catch (SAXException e) {
				log.error("SAX error creating document from ext. server response. Message: {}", e.getMessage());
				throw new PalmsException("SAX error creating document from ext. server response.", PalmsErrorCode.REPORT_GEN_FAILED.getCode(), e.getMessage());
			}
		} catch (MalformedURLException e) {
			log.error("Malformed url: " + urlStr, e.getMessage());
			throw new PalmsException("Malformed URL.", PalmsErrorCode.REPORT_GEN_FAILED.getCode(), e.getMessage());
		} catch (IOException e) {
			log.error("IOException url: " + urlStr, e.getMessage());
			throw new ServerUnavailableException(urlStr, "Server Unavailable: " + urlStr);
		}
		return doc;
	}

	private Document getGroupReportResponseData(ReportRequestResponse r) throws PalmsException{
		//ReportRequestResponse r = reportResponseDao.findSingleByParticipantIdAndType(pid, RGR_TYPE);
		try {
			r.getRgrXml();
		} catch (NullPointerException e) {
			log.error("There is no RGR available for this participant. Either the participant has not completed "
					+ "her or his course nor the data is not reachable. {}", e.getMessage());
			throw new PalmsException("There is no RGR available for this participant. (" + r.getParticipantId() + ")", PalmsErrorCode.REPORT_GEN_FAILED.getCode());
		}
		Document doc = XmlUtil.convertXmlStringToDocument(r.getRgrXml());
		if (doc == null){
			throw new PalmsException("Error transforming xml for participant (" + r.getParticipantId() + ")", PalmsErrorCode.REPORT_GEN_FAILED.getCode());
		}
		return doc;
	}

	private void fillIndividualReportResponseData(Participant ppt, Document doc) {

		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression expr;

		Stage stage = new Stage("responses");

		try {
			expr = xpath.compile("//participant[@id='" + ppt.getExtId() + "']/instruments/instrument[@id='"
					+ VIA_EDGE_INSTRUMENT_ID + "']/responses/response");
			Object result = expr.evaluate(doc, XPathConstants.NODESET);
			NodeList nodes = (NodeList) result;

			Instrument instrument = new Instrument(VIA_EDGE_INSTRUMENT_ID);

			log.debug("reponse node count={}", nodes.getLength());
			for (int i = 0; i < nodes.getLength(); i++) {
				Var var = new Var();
				Element response = (Element) nodes.item(i);
				String id = response.getAttributeNode("id").getValue();
				var.setId(id.toLowerCase());

				for (int j = 0; j < response.getChildNodes().getLength(); j++) {
					Node child = response.getChildNodes().item(j);
					if (child instanceof Element && child.getNodeName().equals("value")) {
						var.setValue(child.getTextContent());
					}
				}
				instrument.getVar().add(var);
			}
			stage.getInstrument().add(instrument);
			ppt.getStage().add(stage);
		} catch (XPathExpressionException e) {
			log.error("XPATH Error {}", e.getMessage());
		}
	}

	private void fillGroupReportResponseData(Participant ppt, Document doc) {
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression expr;

		XPath xpath2 = xPathfactory.newXPath();
		XPathExpression expr2;

		Stage stage = new Stage("responses");

		try {
			expr = xpath.compile("//stage[@id='adj-pvalue']/var");
			Object result = expr.evaluate(doc, XPathConstants.NODESET);
			NodeList nodes = (NodeList) result;

			Instrument instrument = new Instrument(VIA_EDGE_INSTRUMENT_ID);

			for (int i = 0; i < nodes.getLength(); i++) {
				Var var = new Var();
				Element response = (Element) nodes.item(i);
				var.setId(response.getAttributeNode("id").getValue());
				var.setValue(response.getAttributeNode("value").getValue());
				instrument.getVar().add(var);
			}

			expr2 = xpath2.compile("//stage[@id='cnfdc']/var");
			Object result2 = expr2.evaluate(doc, XPathConstants.NODESET);
			NodeList nodes2 = (NodeList) result2;

			Var var = new Var();
			Element response2 = (Element) nodes2.item(0);
			var.setId(response2.getAttributeNode("id").getValue());
			var.setValue(response2.getAttributeNode("value").getValue());

			instrument.getVar().add(var);
			stage.getInstrument().add(instrument);
			ppt.getStage().add(stage);

		} catch (XPathExpressionException e) {
			log.error("XPATH Error", e.getMessage());
		}
	}

	private void fillGroupReportWithoutResponseData(Participant ppt) {
		Stage stage = new Stage("responses");
		Instrument instrument = new Instrument(VIA_EDGE_INSTRUMENT_ID);

		String[] scores = { "pAdjOLG", "pAdjMLG", "pAdjPLG", "pAdjCLG", "pAdjRLG", "pAdjSLG", "Confidence_index" };

		for (int i = 0; i < scores.length; i++) {
			Var var = new Var();
			var.setId(scores[i]);
			var.setValue("undefined");
			instrument.getVar().add(var);
		}
		stage.getInstrument().add(instrument);
		ppt.getStage().add(stage);
	}


	public ReportRequestResponseDao getReportResponseDao() {
		return reportResponseDao;
	}

	public void setReportResponseDao(ReportRequestResponseDao reportResponseDao) {
		this.reportResponseDao = reportResponseDao;
	}

	public Properties getReportConfigProperties() {
		return reportConfigProperties;
	}

	public void setReportConfigProperties(Properties reportConfigProperties) {
		this.reportConfigProperties = reportConfigProperties;
	}
}
