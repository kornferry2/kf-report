package com.pdinh.project.generic;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.pipeline.generator.helpers.PipelineTransformHelper;
import com.pdinh.pipeline.generator.helpers.ReportManifestHelper;
import com.pdinh.pipeline.generator.vo.Participant;
import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.pipeline.generator.vo.Stage;
import com.pdinh.pipeline.manifest.vo.ReportManifest;

@Stateless
@LocalBean
public class GenericRgrBean implements Serializable {

	// Static variables
	private static final Logger log = LoggerFactory.getLogger(GenericRgrBean.class);
	private static final long serialVersionUID = 1L;

	// Instance variables

	static private boolean debugging = false;
	static private String cerPath = null;
	static private String instCodeLocal = null;

	@Resource(mappedName = "custom/generator_properties")
	private Properties generatorProperties;

	// Constructors

	/**
	 * Generic method called by specific instrument score bean to achieve:
	 * 
	 * 1. get response raw score rgr1 for a instrument.
	 * 2. trigger pipeline calculation
	 * 3. return a rgr2  
	*/
	public GenericRgrBean() {

	}

	//
	// Instance methods.
	//

	/**
	 * @param manifest
	 *   - A instrument manifest
	 *   - A raw response RGR
	 * 
	 * @return A ReportGenerationRequest object
	 */
	public ReportGenerationRequest createScoredRgr(String instCode, String manifest, ReportGenerationRequest Rgr1) {
		// Get the raw response RGR
		if (Rgr1 == null) {
			// Message probably came out already
			return null;
		} else {
			return createObjRgr2(instCode, manifest, Rgr1);
		}
	}

	private ReportGenerationRequest createObjRgr2(String instCode, String manifest, ReportGenerationRequest rawRgr) {

		// This is the pipeline process

		cerPath = generatorProperties.getProperty("calculationEngineResources");

		// Get the manifest
		ReportManifestHelper rmh = new ReportManifestHelper(debugging, cerPath);
		
		ReportManifest reportManifest = rmh.getReportManifest(instCode, manifest); 
		
		// Process the XML per the manifest
		PipelineTransformHelper pth = new PipelineTransformHelper(debugging, cerPath);

		String respXml = convertRgrToXmlStr(rawRgr);
		String output = pth.process(reportManifest, respXml);

		//System.out.println("Calc output XML:\n" + output);

		return convertXmlStrToObj(output);
		
	}
	
	/**
	 * @param manifest
	 *   - A instrument manifest
	 *   - A raw response RGR
	 * 
	 * @return A ReportGenerationRequest object
	 */
	public ReportGenerationRequest createScoredRgr(String manifest, ReportGenerationRequest Rgr1) {
		// Get the raw response RGR
		if (Rgr1 == null) {
			// Message probably came out already
			return null;
		} else {
			return createObjRgr2(manifest, Rgr1);
		}
	}

	private ReportGenerationRequest createObjRgr2(String manifest, ReportGenerationRequest rawRgr) {

		// This is the pipeline process

		cerPath = generatorProperties.getProperty("calculationEngineResources");

		// Get the manifest
		ReportManifestHelper rmh = new ReportManifestHelper(debugging, cerPath);
		
		instCodeLocal = getInstCodeLocal(rawRgr);
		ReportManifest reportManifest = rmh.getReportManifest(instCodeLocal, manifest); 
		
		// Process the XML per the manifest
		PipelineTransformHelper pth = new PipelineTransformHelper(debugging, cerPath);

		String respXml = convertRgrToXmlStr(rawRgr);
		String output = pth.process(reportManifest, respXml);

		//System.out.println("Calc output XML:\n" + output);

		return convertXmlStrToObj(output);
		
	}

	private ReportGenerationRequest convertXmlStrToObj(String xml) {
		InputStream is = null;
		JAXBContext jc = null;
		Unmarshaller um = null;
		ReportGenerationRequest rgr = null;

		try {
			is = new ByteArrayInputStream(xml.getBytes("UTF-8"));

			// Create the context & the unmarshaller
			jc = JAXBContext.newInstance(ReportGenerationRequest.class);
			um = jc.createUnmarshaller();
			rgr = (ReportGenerationRequest) um.unmarshal(is);

			return rgr;
		} catch (UnsupportedEncodingException e) {
			System.out.println("Invalid encoding for unmarshalling scored XML. ");
			return null;
		} catch (JAXBException e) {
			System.out.println("Error unmarshalling scored XML.  ");
			return null;
		} finally {
			um = null;
			jc = null;
			try {
				is.close();
			} catch (Exception e) { /* Swallow it */
			}
		}
	}
	
	private String convertRgrToXmlStr(ReportGenerationRequest rgr) {
		JAXBContext jc = null;
		String xml = null;

		try {
			jc = JAXBContext.newInstance(ReportGenerationRequest.class);
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter sw = new StringWriter();
			m.marshal(rgr, sw);
			xml = sw.toString();

		} catch (JAXBException e) {
			log.error("Failed to marshal RGR into XML.  ", e.getMessage());
			e.printStackTrace();

		}
		return xml;
	}
	
	private String getInstCodeLocal(ReportGenerationRequest rawRgr){
		
		Participant ppt = rawRgr.getParticipant().get(0);
		
		if (ppt == null) {
			return null;
		}
		// pptId = ppt.getId();
		List<Stage> stages = ppt.getStage();
		// Look for the "raw" stage
		for (Stage st : stages) {
			if (st.getId().equals("raw") || st.getId().equals("responses")) {
				instCodeLocal = st.getInstrument().get(0).getId();
				break;
			}
		}
		return instCodeLocal.toLowerCase();
	}
}
