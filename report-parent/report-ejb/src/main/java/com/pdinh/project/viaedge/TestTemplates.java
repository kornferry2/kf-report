package com.pdinh.project.viaedge;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class TestTemplates {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Configuration cfg = new Configuration();
		cfg.setObjectWrapper(new DefaultObjectWrapper());

		String reportSerial = "3209487s982342";
		String reportDate = "34-34-3425";
		String reportName = "ReportName";
		Template template = null;
		cfg.setTemplateLoader(com.pdinh.report.util.FreeMarkerUtil.getClassTemplateLoader());
		try {
			// CREATE Freemarker TEMPLATE
			template = cfg.getTemplate("/projects/viaEdge/groupFeedback/cover.ftl");

			Map<String, String> cover_dataModel = new HashMap<String, String>();
			cover_dataModel.put("reportName", reportName);
			cover_dataModel.put("reportSerial", reportSerial);
			cover_dataModel.put("reportDate", reportDate);

			StringWriter out = new StringWriter();
			// PROCESS TEMPLATE + MODEL
			template.process(cover_dataModel, out);
			// CONVERT OUTPUT to StringBuffer
			StringBuffer sb = out.getBuffer();


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
