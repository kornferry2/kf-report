package com.pdinh.project.kfap.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.pdinh.exception.PalmsException;
import com.pdinh.exception.RCMGenerationException;
import com.pdinh.report.util.StringNumberUtil;

public class KfapUtils {

	private static final Logger log = LoggerFactory.getLogger(KfapUtils.class);

	public static String getStringFromDocument(Document rgrDoc) {
		try {
			DOMSource domSource = new DOMSource(rgrDoc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
			return writer.toString();
		} catch (TransformerException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static String getStringFromElement(Element rgrElement) {
		try {
			DOMSource domSource = new DOMSource(rgrElement);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
			return writer.toString();
		} catch (TransformerException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static Document processRgrXml(String rgrXml) throws PalmsException {
		Document doc = null;
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			doc = docBuilder.parse(new InputSource(new ByteArrayInputStream(rgrXml.getBytes())));
		} catch (ParserConfigurationException e) {
			log.error("Parser error creating document from ext. server response. Message: {}", e.getMessage());
			// e.printStackTrace();
			throw new PalmsException("Parser error creating document from ext. server response. Message: "
					+ e.getMessage());
			// return null;
		} catch (IOException e) {
			log.error("IO error creating document from ext. server response. Message: {}", e.getMessage());
			// e.printStackTrace();
			throw new PalmsException("IO error creating document from ext. server response. Message: " + e.getMessage());
			// return null;
		} catch (SAXException e) {
			log.error("SAX error creating document from ext. server response. Message: {}", e.getMessage());
			// e.printStackTrace();
			throw new PalmsException("SAX error creating document from ext. server response. Message: "
					+ e.getMessage());
			// return null;
		}
		return doc;
	}

	public static void printElemnt(Element element) {

		log.debug("List attributes for node: " + element.getNodeName());

		// get a map containing the attributes of this node
		NamedNodeMap attributes = element.getAttributes();

		// get the number of nodes in this map
		int numAttrs = attributes.getLength();

		for (int i = 0; i < numAttrs; i++) {
			Attr attr = (Attr) attributes.item(i);

			String attrName = attr.getNodeName();
			String attrValue = attr.getNodeValue();

			log.debug("Found attribute: " + attrName + " with value: " + attrValue);

		}
	}

	public static String getScoredData(Document inputXmlDoc, String instId, String itemId) throws PalmsException {

		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("//dataset[@id='" + instId + "']/doubleDataArray[@id='" + itemId
					+ "']/value");

			Element elm = (Element) expr.evaluate(inputXmlDoc, XPathConstants.NODE);
			if (elm == null) {
				System.out.println("Value not found for " + instId + "/" + itemId);
				throw new RCMGenerationException("Expected Data not found in calculation payload for instrument: "
						+ instId + ", Item: " + itemId);
				// return "";
			} else {

				// String test = elm.getTextContent();

				return elm.getTextContent();
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			throw new RCMGenerationException("Error compiling xpath expression for instrument: " + instId + ", Item: "
					+ itemId);
			// return "";
		}
	}

	public static String getGroupScoredData(Document inputXmlDoc, String partId, String instId, String itemId)
			throws PalmsException {

		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("//calculationPayload[@particpantId='" + partId + "']/dataset[@id='"
					+ instId + "']/doubleDataArray[@id='" + itemId + "']/value");

			Element elm = (Element) expr.evaluate(inputXmlDoc, XPathConstants.NODE);
			if (elm == null) {
				System.out.println("Value not found for " + instId + "/" + itemId);
				throw new RCMGenerationException("Expected Data not found in calculation payload for participant "
						+ partId + ", Instrument: " + instId + ", Item: " + itemId);
				// return "";
			} else {
				return elm.getTextContent();
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			throw new RCMGenerationException("Error compiling xpath expression for participant " + partId
					+ ", Instrument: " + instId + ", Item: " + itemId);
			// return "";
		}
	}

	public static String getGroupScoredDatabyElement(Element inputXml, String instId, String itemId)
			throws PalmsException {

		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("dataset[@id='" + instId + "']/doubleDataArray[@id='" + itemId
					+ "']/value");

			Element elm = (Element) expr.evaluate(inputXml, XPathConstants.NODE);
			if (elm == null) {
				System.out.println("Value not found for " + instId + "/" + itemId);
				throw new RCMGenerationException("Expected Data not found in scoreMap for Instrument: " + instId
						+ ", Item: " + itemId);
				// return "";
			} else {
				return elm.getTextContent();
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			throw new RCMGenerationException("Error compiling xpath expression for ScoreMap, Instrument: " + instId
					+ ", Item: " + itemId);
			// return "";
		}
	}

	public static Double getTotalCount(Document inputXmlDoc, String instId, String itemId) throws PalmsException {

		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("count(//calculationPayload/dataset[@id='" + instId
					+ "']/doubleDataArray[@id='" + itemId + "'])");
			Object result = expr.evaluate(inputXmlDoc, XPathConstants.NUMBER);
			Double totalCount = (Double) result;

			// And for the other format
			XPathExpression expr2 = xpath.compile("count(//CalculationPayload/dataset[@id='" + instId
					+ "']/doubleDataArray[@id='" + itemId + "'])");
			Object result2 = expr2.evaluate(inputXmlDoc, XPathConstants.NUMBER);
			Double totalCount2 = (Double) result2;

			// ...and verify that SOMETHING is present
			if (totalCount == null && totalCount2 == null) {
				System.out.println("Value not found for totalCount");
				throw new RCMGenerationException(
						"Expected Data not found in calculation payload to calculate getTotalCount. " + "Instrument: "
								+ instId + ", ItemID: " + itemId);
			} else {
				return totalCount + totalCount2;
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			throw new RCMGenerationException("Error compiling xpath expression for getTotalCount. " + "Instrument: "
					+ instId + ", ItemID: " + itemId);
			// return "";
		}
	}

	public static Double getNoRavensCount(Document inputXmlDoc, String instId, String itemId) throws PalmsException {

		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("count(//calculationPayload/dataset[@id='" + instId
					+ "']/doubleDataArray[@id='" + itemId + "'][value='NaN'])");
			Object result = expr.evaluate(inputXmlDoc, XPathConstants.NUMBER);
			Double noRavenCount = (Double) result;

			// Now do the same for the other format
			XPathExpression expr2 = xpath.compile("count(//CalculationPayload/dataset[@id='" + instId
					+ "']/doubleDataArray[@id='" + itemId + "'][value='NaN'])");
			Object result2 = expr2.evaluate(inputXmlDoc, XPathConstants.NUMBER);
			Double noRavenCount2 = (Double) result2;

			// ...and verify that SOMETHING is present
			if (noRavenCount == null && noRavenCount2 == null) {
				System.out.println("Value not found for RavensCount");
				throw new RCMGenerationException(
						"Expected Data not found in calculation payload to calculate getNoRavensCount. "
								+ "Instrument: " + instId + ", Item: " + itemId);
			} else {
				return noRavenCount + noRavenCount2;
			}

		} catch (XPathExpressionException e) {
			e.printStackTrace();
			throw new RCMGenerationException("Error compiling xpath expression for getNoRavensCount. " + "Instrument: "
					+ instId + ", Item: " + itemId);
			// return "";
		}
	}

	public static Double getVersionCount(Document inputXmlDoc, String instId, String versionId) throws PalmsException {

		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("count(//calculationPayload/dataset[@version='" + versionId + "'])");
			Object result = expr.evaluate(inputXmlDoc, XPathConstants.NUMBER);
			Double versionCount = (Double) result;

			XPathExpression expr2 = xpath.compile("count(//CalculationPayload/dataset[@version='" + versionId + "'])");
			Object result2 = expr2.evaluate(inputXmlDoc, XPathConstants.NUMBER);
			Double versionCount2 = (Double) result2;

			if (versionCount == null && versionCount2 == null) {
				System.out.println("Value not found for versionCount");
				throw new RCMGenerationException(
						"Expected Data not found in calculation payload to calculate getVersionCount. "
								+ "Instrument: " + instId + ", VersionID: " + versionId);
			} else {
				return versionCount + versionCount2;
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			throw new RCMGenerationException("Error compiling xpath expression for getVersionCount. " + "Instrument: "
					+ instId + ", VersionID: " + versionId);
			// return "";
		}
	}

	@SuppressWarnings({ "rawtypes" })
	public static Map getScoredDataMap(Document inputXmlDoc, String dataSetId, String mapId) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList nodeList;
		LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();

		try {
			String expr = "//dataset[@id='" + dataSetId + "']/integerDataArray[@id='" + mapId + "']/value";
			nodeList = (NodeList) xpath.evaluate(expr, inputXmlDoc, XPathConstants.NODESET);
			for (int i = 0; i < nodeList.getLength(); i++) {
				// Element e = (Element) node;
				String value = nodeList.item(i).getTextContent();
				// System.out.println("ITEM"+String.valueOf(i+1)+" value="+value
				// .trim());
				dataSet.put("ITEM" + String.valueOf(i + 1), value.trim());
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return null;
		}
		return dataSet;
	}

	@SuppressWarnings({ "rawtypes" })
	public static Map getGroupScoredDataMap(Document inputXmlDoc, String partId, String dataSetId, String mapId) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList nodeList;
		LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();

		try {
			String expr = "//calculationPayload[@particpantId='" + partId + "']/dataset[@id='" + dataSetId
					+ "']/integerDataArray[@id='" + mapId + "']/value";
			nodeList = (NodeList) xpath.evaluate(expr, inputXmlDoc, XPathConstants.NODESET);
			for (int i = 0; i < nodeList.getLength(); i++) {
				// Element e = (Element) node;
				String value = nodeList.item(i).getTextContent();
				// System.out.println("ITEM"+String.valueOf(i+1)+" value="+value
				// .trim());
				dataSet.put("ITEM" + String.valueOf(i + 1), value.trim());
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return null;
		}
		return dataSet;
	}

	public static List<String> getContentData(Document inputXmlDoc, String dataSetId, boolean isSingle,
			String questionId) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList nodeList;
		List<String> inputData = new ArrayList<String>();

		String expr = null;

		if (isSingle) {
			expr = "/assessment[@id='" + dataSetId + "']/activities/activities/activities/singleChoiceQuestion[@id='"
					+ questionId + "']/options/option";
		} else {
			expr = "/assessment[@id='" + dataSetId + "']/activities/activities/activities/manyChoiceQuestion[@id='"
					+ questionId + "']/options/option";
		}
		try {
			nodeList = (NodeList) xpath.evaluate(expr, inputXmlDoc, XPathConstants.NODESET);
			for (int i = 0; i < nodeList.getLength(); i++) {
				String value = nodeList.item(i).getTextContent();
				// Element e = (Element) node;
				// System.out.println("get node value i of "+i+" ="+value.trim());
				inputData.add(value);
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return null;
		}
		return inputData;
	}

	public static List<String> getCurrentListContentData(Document inputXmlDoc, String dataSetId, boolean isSingle,
			String questionId) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList nodeList;
		List<String> inputData = new ArrayList<String>();

		String expr = null;

		if (isSingle) {
			expr = "/assessment[@id='" + dataSetId + "']/activities/activities/activities/singleChoiceQuestion[@id='"
					+ questionId + "']/options/option";
		} else {
			expr = "/assessment[@id='" + dataSetId + "']/activities/activities/activities/manyChoiceQuestion[@id='"
					+ questionId + "']/options/option";
		}
		try {
			nodeList = (NodeList) xpath.evaluate(expr, inputXmlDoc, XPathConstants.NODESET);
			for (int i = 0; i < nodeList.getLength(); i++) {
				String value = nodeList.item(i).getTextContent();
				Scanner scanner = new Scanner(value);
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					// System.out.println("lines="+line);
					inputData.add(line);
					break;
				}
				scanner.close();
				// System.out.println("get node value i of "+i+" ="+value.trim());
				// inputData.add(value);
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return null;
		}
		return inputData;
	}

	public static String getRawResponseData(Document inputXmlDoc, String dataSetId, String responseId) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expr = xpath.compile("//instrument[@id='" + dataSetId + "']/responses/response[@id='"
					+ responseId + "']/value");

			Element elm = (Element) expr.evaluate(inputXmlDoc, XPathConstants.NODE);
			if (elm == null) {
				System.out.println("raw response xml " + dataSetId + "/" + responseId + "/");
				return "";
			} else {
				return elm.getTextContent();
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getVersion4ParticipantAssessment(Document inputXmlDoc, String dataSetId, String item) {
		String version = "";
		NodeList nodes = inputXmlDoc.getElementsByTagName("dataset");

		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			Attr attr1 = (Attr) node.getAttributes().getNamedItem("id");
			if (attr1.getValue().equals(dataSetId)) {
				Attr attr2 = (Attr) node.getAttributes().getNamedItem(item);
				if (attr2 != null) {
					version = attr2.getValue();
				}
			}
		}
		return version;
	}

	public static String getNumOrWord(String value) {
		return Integer.parseInt(value) >= 11 ? String.valueOf(value) : convertNumToWord(value);
	}

	public static String convertNumToWord(String value) {
		Pattern pattern = Pattern.compile("[0-9]+");
		Matcher matcher = pattern.matcher(value);

		// convert number to word in the getValue() String

		while (matcher.find()) {
			// Get the matching string
			String match = matcher.group();
			if (match.length() == value.length()) {
				// System.out.println("number in string=" + match
				// + " match string=" + value);
				return StringNumberUtil.numberToWord(Integer.parseInt(value)).replaceAll(" ", "");
			}
		}
		return null;
	}

	// for testing
	public static void print(Object[][] a) {
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++)
				System.out.print("2D table=" + a[i][j] + " ");
			System.out.println();
		}
	}

	// public static void sortPriorityTable(int [][] table, final int sortCol,
	// boolean isAscending){
	// if(isAscending){
	// Arrays.sort(table, new Comparator<int[]>() {
	// @Override
	// public int compare(int[] o1, int[] o2) {
	// return Double.compare(o1[sortCol], o2[sortCol]); //ascending -low to high
	// }
	// });
	// }else{
	// Arrays.sort(table, new Comparator<int[]>() {
	// @Override
	// public int compare(int[] o1, int[] o2) {
	// return Double.compare(o2[sortCol], o1[sortCol]); //descending -high to
	// low
	// }
	// });
	//
	// }
	// }

	public static void sortCategoryTable(String[][] table, final int sortCol) {
		Arrays.sort(table, new Comparator<String[]>() {
			@Override
			public int compare(String[] o1, String[] o2) {
				return Double.valueOf(o1[sortCol]).compareTo(Double.valueOf(o2[sortCol]));
			}
		});
	}

	public static boolean hasArrayDuplicates(int[] intArr) {
		for (int j = 0; j < intArr.length; j++) {
			for (int k = j + 1; k < intArr.length; k++) {
				if (intArr[k] == intArr[j]) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean hasTop3Duplicates(List<int[]> tops) {
		int[] top1 = tops.get(0);
		int[] top2 = tops.get(1);
		int[] top3 = tops.get(2);

		if (Arrays.equals(top1, top2)) {
			return true;
		} else if (Arrays.equals(top1, top3)) {
			return true;
		} else if (Arrays.equals(top2, top3)) {
			return true;
		}
		return false;
	}

	public static String getCategoryDisplayText(boolean isCurrent, int expInt, int futInt) {
		StringBuilder builder = new StringBuilder("");
		if (expInt == 1) {
			builder.append(",isExp");
		}
		if (futInt == 1) {
			builder.append(",isFut");
		}
		if (isCurrent) {
			builder.append(",isCurr");
		}
		return builder.toString();
	}

}
