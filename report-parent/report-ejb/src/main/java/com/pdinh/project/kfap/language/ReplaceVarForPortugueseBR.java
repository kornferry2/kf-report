package com.pdinh.project.kfap.language;

import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.Stateless;

import org.apache.commons.lang3.StringUtils;

@Stateless
public class ReplaceVarForPortugueseBR implements LangReplacement {

	@Override
	public String replaceContentVar(String content, Map<String, String> varTextWrapper) {
		if (StringUtils.isNotEmpty(content)) {
			Pattern pattern = Pattern.compile("\\[(.*?)\\]");
			Matcher matcher = pattern.matcher(content);
			// for each variable found, we replace them to Japanese
			while (matcher.find()) {
				String matchTxt = matcher.group().toString().replace("[", "").replace("]", "");
				Iterator<Map.Entry<String, String>> it = varTextWrapper.entrySet().iterator();
				// for each mapped variable value
				while (it.hasNext()) {
					Map.Entry<String, String> entry = it.next();
					if (entry.getKey().equals(matchTxt)) {
						if (entry.getKey().equals("vDateComplete") || entry.getKey().equals("vClient")) {
							content = content.replace(matcher.group().toString(), entry.getValue().toString());
						} else {
							// entry.getValue().toString().toLowerCase();
							content = content.replace(matcher.group().toString(), entry.getValue().toString()
									.toLowerCase());
						}
					}
				}
			}
		}
		return content;
	}

}
