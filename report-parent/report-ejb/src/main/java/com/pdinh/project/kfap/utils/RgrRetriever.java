package com.pdinh.project.kfap.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.helper.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import sun.net.www.protocol.http.HttpURLConnection;

import com.pdinh.exception.PalmsException;
import com.pdinh.exception.RCMGenerationException;
import com.pdinh.exception.ServerUnavailableException;
import com.pdinh.reportserver.data.dao.jpa.ReportRequestResponseDao;
import com.pdinh.reportserver.persistence.jpa.ReportRequestResponse;

/**
 * Created by jsaunder on 11/10/2014.
 */
@Stateless
public class RgrRetriever {

    @Resource(name = "application/report/config_properties", type = Properties.class)
    private Properties configProperties;

    private static final Logger log = LoggerFactory.getLogger(RgrRetriever.class);

    private final String RESTFUL_STRING_PARAMS = "/{instrumentId}/{manifest}/{participantId}/{tgtRole}";
    private final String TINCAN_RESTFUL_STRING_PARAMS = "/{calRequest}/instruments/{instrumentId}/manifests/{manifest}/participants/{participantId}/projects/{projectId}?targetLevel={tgtRole}";
    private final String KFP_INST = "kfp";
    private final String manifest = "kfpManifest";

    @EJB
	private ReportRequestResponseDao rgrAccessBean;
    
    public Document getRgr(String type, String pid, String tgtRole) throws PalmsException {
        String urlBase = this.configProperties.getProperty("calculationRequestDataUrl");


        String calcScoreUrl = urlBase
                + RESTFUL_STRING_PARAMS.replace("{participantId}", pid + "").replace("{instrumentId}", KFP_INST + "")
                .replace("{manifest}", manifest).replace("{tgtRole}", tgtRole);

        log.info("scored data fetch URL=" + calcScoreUrl);

        Document doc = this.getReportResponseData(calcScoreUrl);
        return doc;
    }
    
    //a Tincan restful service call.  
    public Document getRgr(String type, String requestStr, String inst, String custManifest, 
    						String pid, String projectId, String tgtRole) throws PalmsException {
    	Document rgrDoc = null;
    	String urlBase = this.configProperties.getProperty("tincanAdminRestUrl");
    	
    	Integer pptId = Integer.parseInt(pid);
    	String rgrXml = retrieveRgRFromDB (pptId, type, tgtRole);
    	Integer targLevelIndex = Integer.valueOf(tgtRole);
    	if (!StringUtils.isBlank(rgrXml)) {
    		rgrDoc = KfapUtils.processRgrXml(rgrXml);
    	} else {
            String calcScoreUrl = urlBase
                    + TINCAN_RESTFUL_STRING_PARAMS.replace("{calRequest}", requestStr + "").replace("{instrumentId}", inst + "")
                    .replace("{manifest}", custManifest + "").replace("{participantId}", pid + "").replace("{projectId}", projectId + "").
                    replace("{tgtRole}", tgtRole);

            log.info("Fatch tincan admin score data URL=" + calcScoreUrl);

            rgrDoc = this.getReportResponseData(calcScoreUrl);
            if (isPersistable(rgrDoc)){
	            String rgrXmlString = KfapUtils.getStringFromDocument(rgrDoc);
	            if (!StringUtil.isBlank(rgrXmlString)){
	            	saveRgRInDB(pptId, type, rgrXmlString, targLevelIndex);	
	            } else {
	            	 throw new PalmsException("RGR persistence failed.  Ppt= " + pptId + ", type= " + type + ", xml= " + rgrXml);
	            }
            }
    	}
        return rgrDoc;
    }
    
    private String retrieveRgRFromDB (Integer pptId, String type, String tgtRole) {
    	
    	String rgrXml = "";
    	ReportRequestResponse rptReqResp = null;
		Integer targInt = Integer.valueOf(tgtRole);
    	rptReqResp = rgrAccessBean.findSingleByParticipantIdAndTypeAndTarget(pptId, type, targInt);
			if (rptReqResp == null) {
				log.info("There is no RGR persisted for Ppt={}, type={}", pptId, type);
				return null;
			}
			rgrXml = rptReqResp.getRgrXml();
		return rgrXml;
    }
    
    private void saveRgRInDB (Integer pptId, String type, String rgrXml, Integer targLevelIndex) {
    	
    	ReportRequestResponse rptReqResp = rgrAccessBean.createOrUpdate(pptId, type, rgrXml, targLevelIndex);
    	if (rptReqResp == null) {
			log.error("RGR persistence failed.  Ppt={}, type={}, xml={}", pptId, type, rgrXml);
		}
    }
    
    private boolean isPersistable(Document rgrDoc) throws PalmsException {
    	Element parent = rgrDoc.getDocumentElement();
    	for(Node child = parent.getFirstChild(); child != null; child = child.getNextSibling())
        {
            if(child instanceof Element && "messages".equals(child.getNodeName())){ 
            	return false;
            }
        }
    	
    	return true;
    }
    
    private Document getReportResponseData(String urlStr) throws PalmsException{
        Document rgrDoc = null;
        try {
            URL url = new URL(urlStr);
            @SuppressWarnings("restriction")
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
           
            String line = "";
            String rgrXml = "";
            @SuppressWarnings("restriction")
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = in.readLine()) != null) {
            	rgrXml += line;
            }
            in.close();
            rgrDoc = KfapUtils.processRgrXml(rgrXml);
        } catch (MalformedURLException e) {
            log.error("Malformed url: " + urlStr, e.getMessage());
            throw new PalmsException(e.getMessage());
        } catch (IOException e) {
            log.error("IOException url: {}, Message: {}", urlStr, e.getMessage());
            throw new ServerUnavailableException(urlStr, "Server Unavailable: " + urlStr);
        }
        return rgrDoc;
    }
}
