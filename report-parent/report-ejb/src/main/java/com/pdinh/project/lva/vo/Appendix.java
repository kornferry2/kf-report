/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/*
 * This is the top level JAXB class for the boilerplate data that is present in the
 * Appendix to the dashboard reports.  It is used in the LvaDBReport JAXB structure.
 */
// TODO Should this be a general class rather than specifically for the LVA reports?

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"title", "appSection"})
public class Appendix
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String title;
	@XmlElementWrapper(name = "appendixSections")
	private List<AppSection> appSection;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Appendix()
	{
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
	
		String str = "  Appendix:  ";
		str += "title=" + this.title + "\n";
		for (AppSection as : this.appSection)
		{
			str += "\n      " + as.toString();
		}
		
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getTitle()
	{
		return this.title;
	}

	public void setTitle(String value)
	{
		this.title = value;
	}

	//----------------------------------
	// Note that the list has a "get" only
	public List<AppSection> getAppSection()
	{
		if (this.appSection == null)
			this.appSection = new ArrayList<AppSection>();
		
		return this.appSection;
	}
}
