/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/*
* Development object used for the ScoredData section of LvaDBReport only.
*/
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder={"title", "devSection"})
public class Development
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String title;
	@XmlElementWrapper(name = "devSections")
	private List<DevSection> devSection;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Development()
	{
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "Development:  ";
		str += "title=" + this.title + "  ";
		str += "\n        devSections:  ";
		if (this.devSection == null)
		{
			str += "<None>";
		}
		else
		{
			str += "\n";
			for (DevSection ds : this.devSection)
			{
				str += "          " + ds.toString() + "\n";
			}
		}
		
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getTitle()
	{
		return this.title;
	}

	public void setTitle(String value)
	{
		this.title = value;
	}

	//----------------------------------
	// Note that the list has a "get" only
	public List<DevSection> getDevSection()
	{
		if (this.devSection == null)
			this.devSection = new ArrayList<DevSection>();
		
		return this.devSection;
	}

}
