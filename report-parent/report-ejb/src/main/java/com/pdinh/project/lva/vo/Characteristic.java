/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/*
* Characteristic object used for the scored section of LvaDbReport only.
*/
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "characteristic")
public class Characteristic
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute
	private int stanine;
	@XmlValue
	private String name;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Characteristic()
	{
		// Default constructor
	}

	// 1-parameter constructor (clone)
	public Characteristic(Characteristic orig)
	{
		this.stanine = orig.stanine;
		this.name = orig.getName() == null ? null : new String(orig.getName());
	}

	// 2-parameter constructor
	public Characteristic(int stanine, String name)
	{
		this.stanine = stanine;
		this.name = name;
	}
	
	//
	// Instance methods.
	//
	
	public String toString()
	{
		String str = "Charicteristic:  stanine=" + this.stanine + ", name=" + this.name;
		
		return str;
	}

	// Getters & setters

	//----------------------------------
	public int getStanine()
	{
		return this.stanine;
	}
	
	public void setStanine(int value)
	{
		this.stanine = value;
	}

	//----------------------------------
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String value)
	{
		this.name = value;
	}
}
