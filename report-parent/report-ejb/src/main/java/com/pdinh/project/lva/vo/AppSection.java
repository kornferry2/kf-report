/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/*
 * This is the second level JAXB class for the boilerplate data that is present in the
 * Appendix to the dashboard reports.  It is used in the Appendix JAXB class.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"title", "text", "appSubSection"})
public class AppSection
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String title;
	private String text;
	@XmlElementWrapper(name = "appendixSubSections")
	private List<AppSubSection> appSubSection;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public AppSection()
	{
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
	
		String str = "  AppSection:  ";
		str += "title=" + this.title + ", text=" + this.text + "\n";
		for (AppSubSection ass : this.getAppSubSection())
		{
			str += "\n      " + ass.toString();
		}
		
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getText()
	{
		return this.text;
	}

	public void setText(String value)
	{
		this.text = value;
	}

	//----------------------------------
	public String getTitle()
	{
		return this.title;
	}

	public void setTitle(String value)
	{
		this.title = value;
	}

	//----------------------------------
	// Note that the list has a "get" only
	public List<AppSubSection> getAppSubSection()
	{
		if (this.appSubSection == null)
			this.appSubSection = new ArrayList<AppSubSection>();
		
		return this.appSubSection;
	}
}
