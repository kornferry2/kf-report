/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import com.pdinh.core.vo.Score;


/*
* ScoredSection object used for the ScoredData section of LvaDBReport only.
*/
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder={"title", "score", "text", "superfactor", "characteristic"})
public class ScoredSection
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute
	private String id;
	@XmlAttribute
	private String type;
	private String title;
	private Score score;
	private String text = null;
	@XmlElementWrapper(name = "superfactors")
	private List<Superfactor> superfactor = null;
	@XmlElementWrapper(name = "characteristics")
	private List<Characteristic> characteristic = null;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public ScoredSection()
	{
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "ScoredSection:  ";
		str += "id=" + this.id + "  ";
		str += "type=" + this.type + "  ";
		str += "title=" + this.title + "  ";
		str += (this.score == null ? "<-- No section score -->" : this.score.toString()) + "\n";
		str += "        text=" + (this.text == null ? "<None>" : this.text) + "\n";
		str += "        characteristics:  ";
		if (this.characteristic == null)
		{
			str += "<None>";
		}
		else
		{
			str += "\n";
			for (Characteristic ch : this.characteristic)
			{
				str += "          " + ch.toString() + "\n";
			}
		}
		
		str += "\n        superfactors:  ";
		if (this.superfactor == null)
		{
			str += "<None>";
		}
		else
		{
			str += "\n";
			for (Superfactor sf : this.superfactor)
			{
				str += "          " + sf.toString() + "\n";
			}
		}
		
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getId()
	{
		return this.id;
	}

	public void setId(String value)
	{
		this.id = value;
	}

	//----------------------------------
	public String getType()
	{
		return this.type;
	}

	public void setType(String value)
	{
		this.type = value;
	}

	//----------------------------------
	public String getTitle()
	{
		return this.title;
	}

	public void setTitle(String value)
	{
		this.title = value;
	}

	//----------------------------------
	public Score getScore()
	{
		return this.score;
	}

	public void setScore(Score value)
	{
		this.score = value;
	}

	//----------------------------------
	public String getText()
	{
		return this.text;
	}

	public void setText(String value)
	{
		this.text = value;
	}

	//----------------------------------
	// Note that the list has a "get" only
	public List<Superfactor> getSuperfactor()
	{
		if (this.superfactor == null)
			this.superfactor = new ArrayList<Superfactor>();
		
		return this.superfactor;
	}

	//----------------------------------
	// Note that the list has a "get" only
	public List<Characteristic> getCharacteristic()
	{
		if (this.characteristic == null)
			this.characteristic = new ArrayList<Characteristic>();
		
		return this.characteristic;
	}
}
