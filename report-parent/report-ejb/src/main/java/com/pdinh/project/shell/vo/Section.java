/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.shell.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import com.pdinh.core.vo.Score;

//import com.pdinh.shell.vo.Suggestions;

/*
* This is the Section object used for IbReport only.
*/

//proporder has no effect on attributes
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder={"label", "score", "level", "comments", "behavior", "positiveFeedback"})
public class Section
{
	//
	// Static data.
	//
 
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute(name = "name")
	private String name;
	
	private String label;
	private Score score;
	@XmlElementWrapper(name = "behaviors")
	private List<Level> level;
	private String comments;
	@XmlElementWrapper(name = "development")
	private List<Behavior> behavior;
	private String positiveFeedback;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Section()
	{
		// Default constructor
	}

	// 1-parameter constructor (clone)
	public Section(Section orig)
	{
		this.name = orig.getName() == null ? null : new String(orig.getName());
		this.label = orig.getLabel() == null ? null : new String(orig.getLabel());
		this.score = orig.getScore() == null ? null : new Score(orig.getScore());
		if (orig.getLevel() == null)
		{
			this.level = null;
		}
		else
		{
			this.level = new ArrayList<Level>();
			for(Level ll : orig.getLevel())
			{
				this.level.add(new Level(ll));
			}
		}
		this.comments = orig.getComments() == null ? null : new String(orig.getComments());
		if (orig.getBehavior() == null)
		{
			this.behavior = null;
		}
		else
		{
			this.behavior = new ArrayList<Behavior>();
			for (Behavior bb : orig.getBehavior())
			{
				this.behavior.add(new Behavior(bb));
			}
		}
		this.positiveFeedback = orig.getPositiveFeedback() == null ? null : new String(orig.getPositiveFeedback());
	}
	
	// 2-parameter constructor
	public Section(String name, String label)
	{
		this.name = name;
		this.label = label;
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "Section:  ";
		str += "name=" + this.name + ",  label=" + this.label + ",  score=" + this.score + "\n";
		str += "        behaviors:\n";
		for (Level ll : this.level)
		{
			str += "          " + ll.toString() + "\n";
		}
		str += "        comments=" + this.comments + "\n";
		str += "        development:\n";
		if (this.behavior == null)
		{
			str += "            No behaviors\n";
		}
		else
		{
			for (Behavior bb : this.behavior)
			{
				str += "          " + bb.toString() + "\n";
			}
		}
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getName()
	{
		return this.name;
	}

	public void setName(String value)
	{
		this.name = value;
	}

	//----------------------------------
	public String getLabel()
	{
		return this.label;
	}

	public void setLabel(String value)
	{
		this.label = value;
	}

	//----------------------------------
	public Score getScore()
	{
		return this.score;
	}

	public void setScore(Score value)
	{
		this.score = value;
	}

	//----------------------------------
	public List<Level> getLevel()
	{
		if (this.level == null)
			this.level = new ArrayList<Level>();
		
		return this.level;
	}
	
	//----------------------------------
	public String getComments()
	{
		return this.comments;
	}

	public void setComments(String value)
	{
		this.comments = value;
	}

	//----------------------------------
	// Note that the Behavior list has a "get" only
	public List<Behavior> getBehavior()
	{
		if (this.behavior == null)
			this.behavior = new ArrayList<Behavior>();
		
		return this.behavior;
	}
	
	//----------------------------------
	public String getPositiveFeedback()
	{
		return this.positiveFeedback;
	}

	public void setPositiveFeedback(String value)
	{
		this.positiveFeedback = value;
	}

}
