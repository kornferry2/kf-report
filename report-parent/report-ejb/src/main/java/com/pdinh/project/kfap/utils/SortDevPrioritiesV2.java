package com.pdinh.project.kfap.utils;

import java.util.Comparator;


public class SortDevPrioritiesV2 implements Comparator<int[]>
{
       @Override
       public int compare(int[] ary1, int[] ary2)
       {
              int comp;
              // Dsc on GAP
              comp = ary2[1] - ary1[1];
              
              if (comp != 0) {
            	 return comp;
              }else{
            	  //Asc on SPORDER
            	 return ary1[0] - ary2[0];
              }
       }
}
