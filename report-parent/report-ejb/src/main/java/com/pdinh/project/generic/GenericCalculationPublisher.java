package com.pdinh.project.generic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.pipeline.generator.helpers.CalculationManifestHelper;
//import com.marklogic.client.DatabaseClient;
//import com.marklogic.client.DatabaseClientFactory;
//import com.marklogic.client.document.XMLDocumentManager;
//import com.marklogic.client.io.JAXBHandle;
//import com.marklogic.xcc.ContentSource;
//import com.marklogic.xcc.ContentSourceFactory;
import com.pdinh.pipeline.generator.helpers.PipelineTransformErrorListener;
import com.pdinh.pipeline.manifest.vo.CalculationManifest;
import com.pdinh.pipeline.manifest.vo.Step;

@Stateless
@LocalBean
public class GenericCalculationPublisher {

	final static Logger logger = LoggerFactory
			.getLogger(GenericCalculationPublisher.class);

	@Resource(mappedName = "custom/generator_properties")
	private Properties generatorProperties;

	static private boolean debugging = false;

	static private String suffix = "/";
	static private String xmlExt = ".xml";
	static private String xslExt = ".xsl";

	static private String reposiDir;
	static private String engineDir;
	static private String publishDir;
	//static private String manifestName;

	private String reposiPath = "CalculationRepository";
	private String enginePath = "CalculationEngineResources";
	private String publishPath = "CalculationPublisher";

	public GenericCalculationPublisher() {
	}

	/**
	 * @param String
	 *            pipeline - publish all xsl for a pipeline such as lva or
	 *            viaEdge
	 * @param String
	 *            manifest - publish only one manifest (instrument or consultant
	 *            update within a pipeline)
	 * @param Boolean
	 *            debugMode - debugMode will publish stage and var with
	 *            descriptions or non-debug will publish without
	 * 
	 */

	// 2 paramters
	/*
	 * @param pipeline such as lva, vedge or kfpi
	 * 
	 * @param debug mode will enable description info for Stage and var ids
	 */
	public void publishPipeline(String pipeline, String type, Boolean debug) {
		// always populate working directories first
		setWorkDirectories();
		if (checkManifestFolder(pipeline)) {
			publish(pipeline, null, type, debug);
		} else {
			System.out
					.println("There is no such pipeline in Calculation Repository."
							+ "Pipeline: " + pipeline);
		}
	}

	// 3 parameters
	/*
	 * @param pipeline such as lva, vedge or kfpi
	 * 
	 * @param debug mode will enable description info for Stage and var ids
	 * 
	 * @param take a manifest as user selected not default
	 */
	public void publishPipeline(String pipeline, String manifest, String type, Boolean debug) {
		// always populate working directories first
		setWorkDirectories();
		if (checkManifestFolder(pipeline)
				&& checkManifestFile(pipeline, manifest)) {
			publish(pipeline, manifest, type, debug);
		} else {
			System.out
					.println("There is no such pipeline or manifest file in Calculation Repository.  pipeline:"
							+ pipeline + " manifest: " + manifest);
		}
	}

	private void publish(String pipeline, String manifest, String type, Boolean debug) {
		// get path for CalculationRepository and CalculationPublisher
		// publish manifests are located in a different place other than rgr
		// manifest.
		// one in CalculationPublisher and later in the
		// CalculationEngineResources
		// since rgr manifest are called at different time and place. the
		// publish manifest
		// can be call once to generate all files per pipeline.
		CalculationManifestHelper cmh = new CalculationManifestHelper(
				debugging, publishDir);

		manifest = manifest == null? pipeline + "Manifest" : manifest;
		// ReportManifest rm = rmh.getReportManifest(pipeline, manifest + xmlExt);
		
		CalculationManifest cm = cmh.getCalculationManifest(pipeline, manifest
				+ xmlExt);

		TransformerFactory tFactory = TransformerFactory.newInstance(
				"net.sf.saxon.TransformerFactoryImpl", null);

		//String engine = cm.getSteps().getEngineType();

		// No longer use raw and stanine gen, All use calculationgen or groupcalculationgen

	    String generator = type==null? "calculationgen"+xslExt : "groupcalculationgen"+xslExt;
		String xslGenerationPath = publishDir + "xsl"+ suffix + generator;
		String destinationPath = null;

System.out.println("generator file="+xslGenerationPath);

		for (Step ss : cm.getSteps().getStep()) {
			String id = ss.getId();
			String calculationRef = ss.getCalculationRef();
			String xmlCalculationPath = null;
 				
			xmlCalculationPath = reposiDir + "calculation"+suffix + calculationRef + xmlExt;

			destinationPath = engineDir + "transforms"+suffix + calculationRef + xslExt;

			try {
				transformProcess(tFactory, xmlCalculationPath, xslGenerationPath,
						id, destinationPath);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				System.out.println("One of file is not found.");
				return;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("IO error.");
				return;
			} catch (TransformerConfigurationException e) {
				// TODO Auto-generated catch block
				System.out
						.println("A transformer Configuration Error occures...");
				return;
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				System.out.println("A transformer error occures...");
				return;
			}
		}
	}

	private void transformProcess(TransformerFactory tFactory, String xmlPath,
			String xsltPath, String stageLabel, String destinationPath)
			throws FileNotFoundException, IOException,
			TransformerConfigurationException, TransformerException {

		Transformer tformr = null;
		StreamSource styleSheet = new StreamSource(new File(xsltPath));
		StreamSource xmlSource = new StreamSource(new File(xmlPath));

		// System.out.println("destination: " + destinationPath);
		File resultFile = new File(destinationPath);
		Result result = new StreamResult(resultFile); 
		// get a transformer for this particular style sheet
		tformr = tFactory.newTransformer(styleSheet);
		tformr.setParameter("genElement", stageLabel);
		tformr.setErrorListener(new PipelineTransformErrorListener());
		tformr.transform(xmlSource, result);

	}

	private void setWorkDirectories() {
		engineDir = generatorProperties
				.getProperty("calculationEngineResources");
		if (engineDir != null) {
			String baseDir = engineDir.replaceAll(enginePath + suffix, "");
			setReposiDir(baseDir + reposiPath + suffix);
			setPublishDir(baseDir + publishPath + suffix);
		} else {
			logger.info("No property resource is found for CalculationEngineResources:"
					+ engineDir);
		}
	}

	private boolean checkManifestFolder(String pipeline) {
		String mfDirectory = publishDir + "manifest";
		File workingDirectory = new File(mfDirectory); // current directory
		File[] files = workingDirectory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				if (file.getName().equals(pipeline))
					return true;
			}
		}
		return false;
	}

	private boolean checkManifestFile(String pipeline, String manifestFile) {
		String mfDirectory = engineDir + "manifest" + "/" + pipeline;
		File workingDirectory = new File(mfDirectory); // current directory
		File[] files = workingDirectory.listFiles();
		for (File file : files) {
			if (!file.isDirectory()
					&& file.getName().equals(manifestFile + xmlExt))
				return true;
		}
		return false;
	}

	public static String getReposiDir() {
		return reposiDir;
	}

	public static void setReposiDir(String reposiDir) {
		GenericCalculationPublisher.reposiDir = reposiDir;
	}

	public static String getEngineDir() {
		return engineDir;
	}

	public static void setEngineDir(String engineDir) {
		GenericCalculationPublisher.engineDir = engineDir;
	}

	public static String getPublishDir() {
		return publishDir;
	}

	public static void setPublishDir(String publishDir) {
		GenericCalculationPublisher.publishDir = publishDir;
	}

}
