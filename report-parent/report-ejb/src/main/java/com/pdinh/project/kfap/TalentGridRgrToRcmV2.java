package com.pdinh.project.kfap;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import sun.net.www.protocol.http.HttpURLConnection;

import com.fasterxml.jackson.databind.JsonNode;
import com.kf.project.kfap.reports.jaxb.AboutSectionV2;
import com.kf.project.kfap.reports.jaxb.DefinitionsSectionV2;
import com.kf.project.kfap.reports.jaxb.DetailedResultsSectionV2;
import com.kf.project.kfap.reports.jaxb.RowV2;
import com.kf.project.kfap.reports.jaxb.RowsV2;
import com.kf.project.kfap.reports.jaxb.TableV2;
import com.kf.project.kfap.reports.jaxb.TablesV2;
import com.kf.project.kfap.reports.jaxb.TalentGridV2;
import com.pdinh.data.ms.dao.PositionDao;
import com.pdinh.data.ms.dao.ProjectCourseDao;
import com.pdinh.data.ms.dao.ProjectDao;
import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.project.kfap.utils.ContentRetriever;
import com.pdinh.project.kfap.utils.KfapUtils;
import com.pdinh.report.util.JaxbUtil;
import com.pdinh.reportcontent.ReportContent;
import com.pdinh.reporting.generation.Participants;
import com.pdinh.reporting.generation.ReportParticipant;
import com.pdinh.reportserver.data.dao.jpa.ReportRequestResponseDao;
import com.pdinh.reportserver.persistence.jpa.ReportRequestResponse;

@Stateless
@LocalBean
public class TalentGridRgrToRcmV2 {
	private static final Logger log = LoggerFactory.getLogger(TalentGridRgrToRcmV2.class);

	@Resource(name = "application/report/config_properties", type = Properties.class)
	private Properties configProperties;

	private static String reportManifestDir = "";
	private static String reportContentDir = "";
	private static String reportGenRespDir = "";

	private final ReportContent varContent = null;
	private ReportContent sharedStaticContent = null;
	private ReportContent staticContent = null;

	private JsonNode jsonNode = null;

	private String longDateFormat;
	private String strDate;
	private String strCreateDate;
	private String createDate;
	private String strDateCompleted;

	// private ReportGenerationRequest rgr;
	private TalentGridV2 rpt;

	private final String DEFAULT_LANG = "en-US";

	private String[] langCodes = { "en", "ko", "zh_CN", "en_GB", "fr", "es", "de", "pt_BR", "ja", "ru", "tr" };
	// ("en", "en_GB",
	// "fr", "es", "it", "de", "pt_BR", "sv", "ja",
	// "zh-CN", "pl", "ru", "nl", "id", "cs", "hu", "no", "ko", "tr");

	// store rgr2 id and its mapped text
	private final Map<String, String> varTextWrapper = new HashMap<String, String>();

	private static String CONTENT_VER = "v1";
	private static String REPORT_VER = "V2";
	private static String REPORT_TYPE = "group";
	private static String RICH_TXT = "true";
	private static String CONTENT_FOLDER = "group_slate";

	// getListItem return type
	private static int STR_ARR = 1;
	private static int INT_SUM = 2;

	// no ravens project according to ProjectCourse
	boolean NO_RAVENS = false;
	boolean mixedV1andV2 = false;
	// no ravens score from R
	boolean NO_RavensScore = false;
	boolean incompletes = false;

	private static String KFP_INST = "kfp";
	private static String manifest = "kfpManifestV2";

	// private static String RESTFUL_STRING_PARAMS =

	private static String RESTFUL_STRING_PARAMS = "/jaxrs/mappedCalculationRequest/instruments/{instrumentId}/manifests/{manifest}?targetLevel={tgtRole}";

	// store individual item text and item value matched
	private String vTgtRole = "";
	private String vClient = "";

	private int ravensValue; // 0, 1, or 2 0 = No Ravens, 1 = Some Ravens, 2, =
								// All Ravens
	private int driversScore; // 1 or 0 // replaces interest
	private int experienceScore; // 1 or 0
	private int awarenessScore; // 1 or 0
	private int agileScore; // 1 or 0
	private int traitsScore; // 1 or 0
	private int capacityScore; // 1 or 0
	private int derailerScore; // 1 or 0

	private String strCompleted = "";

	private int slfAwarePct;
	private int sitAwarePct;
	private int changePct;
	private int resultsPct;
	private int peoplePct;
	private int mentalPct;
	private int focusedPct;
	private int optimismPct;
	private int tolerancePct;
	private int volatilePct;
	private int closedPct;
	private int microPct;
	private int capacityPct; // is it a problem solving percentile score?
	private int perspectivePct;
	private int keyChallengePct;
	private int advDrivePct;
	private int corePct;
	private int careerPlanPct;
	private int rolePrefPct;
	private int persistencePct;
	private int assertivenessPct;
	private int tgtRole;

	@EJB
	private ProjectDao projectDao;

	@EJB
	private UserDao userDao;

	@EJB
	private ProjectCourseDao projectCourseDao;

	@EJB
	private PositionDao positionDao;

	@EJB
	private ReportRequestResponseDao reportResponseDao;

	@EJB
	private ContentRetriever contentRetriever;

	public TalentGridRgrToRcmV2() {

	}

	public String generateReportRcm(String type, String pptList, String language, String targ, String rptTitle)
			throws PalmsException {

		String lang = getLanguage(language);

		/*
				if (language != null)
					if (langCodes.contains(language))
						lang = language;
				if (language.equals("en_GB"))
					lang = langCodes.get(1);
				if (language.equals("fr_CA"))
					lang = langCodes.get(2);
				if (language.equals("pt_BR"))
					lang = langCodes.get(6);
				if (language.equals("zh_CN"))
					lang = langCodes.get(9);
				if (language.equals("en"))
					lang = DEFAULT_LANG;
		*/
		// date format

		// Date formats per KFP-708
		if (lang.equals("en-US")) {
			// longDateFormat = "dd MMMMM yyyy"; // Changed per KFP-708
			longDateFormat = "MM/dd/yyyy";
		} else if (lang.equals("zh-CN")) {
			longDateFormat = "yyyy-MM-dd";
		} else if (lang.equals("ko")) {
			longDateFormat = "yyyy-MM-dd";
		} else if (lang.equals("ja")) {
			longDateFormat = "yyyy-MM-dd";
		} else if (lang.equals("fr")) {
			longDateFormat = "dd/MM/yyyy";
		} else if (lang.equals("es")) {
			longDateFormat = "dd/MM/yyyy";
		} else if (lang.equals("de")) {
			longDateFormat = "dd.MM.yyyy";
		} else if (lang.equals("tr")) {
			longDateFormat = "dd.MM.yyyy";
		} else if (lang.equals("ru")) {
			longDateFormat = "dd.MM.yyyy";
		} else if (lang.equals("pt-BR")) {
			longDateFormat = "dd/MM/yyyy";
		} else if (lang.equals("en-GB")) {
			longDateFormat = "dd/MM/yyyy";
		} else {
			// Default... any country code not in the above list
			longDateFormat = "dd-MM-yyyy";
		}

		SimpleDateFormat longSdf = new SimpleDateFormat(longDateFormat);

		strDate = longSdf.format(new Date());

		tgtRole = Integer.parseInt(targ);

		// Get List of Part IDs that was sent

		Participants participants = (Participants) JaxbUtil.unmarshal(pptList, Participants.class);
		List<Integer> allGroupPIds = new ArrayList<Integer>();
		Map<Integer, String> mapOfUserIdsProjIds = new HashMap<Integer, String>();
		for (ReportParticipant ppt : participants.getParticipants()) {
			Integer id;
			try {
				id = Integer.parseInt(ppt.getParticipantId());
			} catch (NumberFormatException e) {
				log.error("Invalid pptId " + ppt.getParticipantId() + ".  Skipped.");
				continue;
			}

			// int version =
			// StringNumberUtil.getCourseVersionFromMap(ppt.getCourseVersionMap(),
			// "kfp");

			// log.debug("Course Version is {}", version);
			// TODO: do something based on course version
			allGroupPIds.add(id);
			mapOfUserIdsProjIds.put(id, ppt.getProjectId());
		}

		List<Position> positions = positionDao.findPositionsByUsersIdsAndCourseAbbvs(allGroupPIds,
				Arrays.asList(Course.KFP));

		// Make two arraylists, 1 for participants that have complete
		// assessments
		// Another for participants that are incomplete
		List<Integer> groupPIds = new ArrayList<Integer>();
		List<Integer> incompletGroupPIds = new ArrayList<Integer>();

		Integer pid = 0;
		String strpid = "";
		// String courseAbbv = "";
		Integer complStatus;
		Integer h = 0;
		vClient = "";
		incompletes = false;

		Integer userID = 0;
		String usersLastFirst = "";

		Map<String, String> userMap = new HashMap<String, String>();

		log.debug("Starting loop through positions list to get ids, completion status and put data in userMap");

		for (int j = 0; j < positions.size(); j++) {

			Position position = positions.get(j);
			userID = position.getId().getUsersId();
			strpid = userID.toString();
			usersLastFirst = position.getUser().getLastname() + ", " + position.getUser().getFirstname();
			userMap.put(strpid, usersLastFirst);

			strCompleted = "Complete";

			complStatus = position.getCompletionStatus();

			if (complStatus != Position.COMPLETE) {
				strCompleted = "Incomplete";
			}

			if (h == 0) {

				vClient = position.getCompany().getCoName();
			}

			h = h + 1;

			if (strCompleted.equals("Incomplete")) {
				incompletGroupPIds.add(userID);
				incompletes = true;
			} else {
				groupPIds.add(userID);
			}
		}

		log.debug("Finished loop through positions list to get ids, completion status and put data in userMap");

		List<List<String>> returnedData = new ArrayList<List<String>>();

		try {
			// send only those partids that have completed assessment(s)

			returnedData = mapInputData(type, groupPIds, mapOfUserIdsProjIds, language, targ);

		} catch (PalmsException e) {
			log.debug("problem to fetch and map input data!");
			e.printStackTrace();
			throw new PalmsException("Problem Generating RCM due to : " + e.getMessage(),
					PalmsErrorCode.RCM_GEN_FAILED.getCode());
			// return null;
		}

		// set manifest
		// setReportManifestDir(configProperties.getProperty("reportContentRepository")
		// + "/manifest/projects/internal/kfap-v2/");
		// setReportContentDir(configProperties.getProperty("reportContentRepository")
		// + "/content/");

		// Manifest manifest = new Manifest(getReportManifestDir() + "group/" +
		// "manifest.xml");

		// /sharedStaticContent = new ReportContent(getReportContentDir()
		// + manifest.getReportContents().get("sharedStaticContent") + ".xml");

		// staticContent = new ReportContent(getReportContentDir() +
		// manifest.getReportContents().get("staticContent")
		// + ".xml");

		// strCreateDate = sharedStaticContent.getKeyMapValue4Lang("shared",
		// "createDate", lang);

		try {
			// jsonNode = this.contentRetriever.getContent(CONTENT_VER,
			// REPORT_VER, REPORT_TYPE, lang);
			jsonNode = this.contentRetriever.getReportContent(CONTENT_FOLDER, lang);
		} catch (PalmsException e) {
			log.info("Fail to extract Json object from marklogic.");
			e.printStackTrace();
		}

		strCreateDate = jsonNode.get("shared").get("createDate").textValue();
		createDate = strCreateDate.replace("{todaysDate}", strDate);

		String labelCase = "labelCase";

		labelCase = labelCase + targ;

		vTgtRole = jsonNode.get("level").get(labelCase).textValue();

		rpt = new TalentGridV2();
		if (rptTitle != null && rptTitle.length() > 0) {
			rpt.setGroup(rptTitle);
			rpt.setGroupLabel(jsonNode.get("shared").get("groupLabel").textValue());

		} else {
			rpt.setGroup("");
			rpt.setGroupLabel("");
		}
		rpt.setLang(lang);
		// rpt.setLang("ko");

		rpt.setReportName(jsonNode.get("shared").get("reportTitleGroupSlate").textValue());
		rpt.setAssessmentTitle(jsonNode.get("shared").get("reportName").textValue());
		rpt.setConfidential(jsonNode.get("shared").get("confidential").textValue());
		rpt.setConfidentialWPeriod(jsonNode.get("shared").get("confidentialWPeriod").textValue());

		rpt.setClientLabel(jsonNode.get("shared").get("clientLabel").textValue());
		rpt.setTargetLabel(jsonNode.get("shared").get("targetLabel").textValue());
		rpt.setReportDateLabel(jsonNode.get("shared").get("reportDateLabel").textValue());

		rpt.setClient(vClient);
		rpt.setTarget(vTgtRole);
		rpt.setReportDate(strDate);
		rpt.setCopyright(jsonNode.get("shared").get("copyright").textValue());
		rpt.setProvidedBy(jsonNode.get("shared").get("providedBy").textValue());
		rpt.setCreateDate(createDate);

		rpt.setRavensValue(BigInteger.valueOf(ravensValue));

		if (mixedV1andV2) {
			rpt.setMixedV1AndV2(true);
		}

		rpt.setEndpageHeader(jsonNode.get("shared").get("endpageHeader").textValue());
		rpt.setEndpageP1(jsonNode.get("shared").get("endpageP1").textValue());
		rpt.setEndpageP2(jsonNode.get("shared").get("endpageP2").textValue());
		rpt.setEndpageP3(jsonNode.get("shared").get("endpageP3").textValue());
		rpt.setVersionNumber(jsonNode.get("version").get("versionNumber").textValue());

		// set report content for Overview Section
		AboutSectionV2 about = new AboutSectionV2();
		about.setTitle(jsonNode.get("aboutSection").get("title").textValue());
		about.setP1(jsonNode.get("aboutSection").get("p1").textValue());
		about.setStrongIndicator(jsonNode.get("aboutSection").get("strongIndicator").textValue());
		about.setDevelopmentIndicator(jsonNode.get("aboutSection").get("developmentIndicator").textValue());
		about.setWellAboveAverageIndicator(jsonNode.get("aboutSection").get("wellAboveAverageIndicator").textValue());
		about.setAtOrAboveAverageIndicator(jsonNode.get("aboutSection").get("atOrAboveAverageIndicator").textValue());
		about.setBelowAverageIndicator(jsonNode.get("aboutSection").get("belowAverageIndicator").textValue());
		about.setWellbelowAverageIndicator(jsonNode.get("aboutSection").get("wellbelowAverageIndicator").textValue());
		about.setDerailerIndicator(jsonNode.get("aboutSection").get("derailerIndicator").textValue());
		about.setNoProblemSolvingText(jsonNode.get("aboutSection").get("noProblemSolvingText").textValue());
		about.setMixedV1AndV2Text(jsonNode.get("aboutSection").get("mixedV1andV2Text").textValue());
		rpt.setAboutSectionV2(about);

		// set report content for Definitions Section
		DefinitionsSectionV2 definitions = new DefinitionsSectionV2();
		definitions.setTitle(jsonNode.get("definitionsSection").get("title").textValue());
		definitions.setDriversLabel(jsonNode.get("labels").get("driversLabel").textValue());
		definitions.setExperienceLabel(jsonNode.get("labels").get("experienceLabel").textValue());
		definitions.setSelfAwarenessLabel(jsonNode.get("labels").get("selfAwarenessLabel").textValue());
		definitions.setLearningAgilityLabel(jsonNode.get("labels").get("learningAgilityLabel").textValue());
		definitions.setLeadershipTraitsLabel(jsonNode.get("labels").get("leadershipTraitsLabel").textValue());
		definitions.setCapacityLabel(jsonNode.get("labels").get("capacityLabel").textValue());
		definitions.setDerailmentRisksLabel(jsonNode.get("labels").get("derailmentRisksLabel").textValue());
		definitions.setDriversShortText(jsonNode.get("definitionsSection").get("driversShortText").textValue());
		definitions.setExperienceShortText(jsonNode.get("definitionsSection").get("experienceShortText").textValue());
		definitions.setSelfAwareShortText(jsonNode.get("definitionsSection").get("selfAwareShortText").textValue());
		definitions.setLearningAgilityShortText(jsonNode.get("definitionsSection").get("learningAgilityShortText")
				.textValue());
		definitions.setLeadershipTraitsShortText(jsonNode.get("definitionsSection").get("leadershipTraitsShortText")
				.textValue());
		definitions.setCapacityShortText(jsonNode.get("definitionsSection").get("capacityShortText").textValue());
		definitions.setDerailmentShortText(jsonNode.get("definitionsSection").get("derailmentShortText").textValue());
		rpt.setDefinitionsSectionV2(definitions);

		DetailedResultsSectionV2 detailedResults = new DetailedResultsSectionV2();
		detailedResults.setTitle(jsonNode.get("detailedResultsSection").get("title").textValue());
		detailedResults.setNeedsLabel(jsonNode.get("labels").get("needsLabel").textValue());
		detailedResults.setStrengthLabel(jsonNode.get("labels").get("strengthLabel").textValue());
		detailedResults.setIncompleteLabel(jsonNode.get("labels").get("incompleteLabel").textValue());
		detailedResults.setWellAboveAverageLabel(jsonNode.get("labels").get("wellAboveAverageLabel").textValue());
		detailedResults.setAtOrAboveAverageLabel(jsonNode.get("labels").get("atOrAboveAverageLabel").textValue());
		detailedResults.setBelowAverageLabel(jsonNode.get("labels").get("belowAverageLabel").textValue());
		detailedResults.setWellbelowAverageLabel(jsonNode.get("labels").get("wellbelowAverageLabel").textValue());
		detailedResults.setNotAvailableLabel(jsonNode.get("labels").get("notAvailableLabel").textValue());
		detailedResults.setDerailerLabel(jsonNode.get("labels").get("derailerLabel").textValue());
		detailedResults.setDriversLabel(jsonNode.get("labels").get("driversLabel").textValue());
		detailedResults.setAdvancementDriveLabel(jsonNode.get("labels").get("advancementDriveLabel").textValue());
		detailedResults.setCareerPlanningLabel(jsonNode.get("labels").get("careerPlanningLabel").textValue());
		detailedResults.setRolePreferenceLabel(jsonNode.get("labels").get("rolePreferenceLabel").textValue());
		detailedResults.setExperienceLabel(jsonNode.get("labels").get("experienceLabel").textValue());
		detailedResults.setCoreLabel(jsonNode.get("labels").get("coreLabel").textValue());
		detailedResults.setPerspectiveLabel(jsonNode.get("labels").get("perspectiveLabel").textValue());
		detailedResults.setKeyChallengesLabel(jsonNode.get("labels").get("keyChallengesLabel").textValue());
		detailedResults.setSelfAwarenessLabel(jsonNode.get("labels").get("selfAwarenessLabel").textValue());
		detailedResults.setSelfLabel(jsonNode.get("labels").get("selfLabel").textValue());
		detailedResults.setSituationalLabel(jsonNode.get("labels").get("situationalLabel").textValue());
		detailedResults.setLearningAgilityLabel(jsonNode.get("labels").get("learningAgilityLabel").textValue());
		detailedResults.setPeopleLabel(jsonNode.get("labels").get("peopleLabel").textValue());
		detailedResults.setResultsLabel(jsonNode.get("labels").get("resultsLabel").textValue());
		detailedResults.setMentalLabel(jsonNode.get("labels").get("mentalLabel").textValue());
		detailedResults.setChangeLabel(jsonNode.get("labels").get("changeLabel").textValue());
		detailedResults.setLeadershipTraitsLabel(jsonNode.get("labels").get("leadershipTraitsLabel").textValue());
		detailedResults.setFocusedLabel(jsonNode.get("labels").get("focusedLabel").textValue());
		detailedResults.setPersistenceLabel(jsonNode.get("labels").get("persistenceLabel").textValue());
		detailedResults.setToleranceLabel(jsonNode.get("labels").get("toleranceLabel").textValue());
		detailedResults.setAssertivenessLabel(jsonNode.get("labels").get("assertivenessLabel").textValue());
		detailedResults.setOptimisticLabel(jsonNode.get("labels").get("optimisticLabel").textValue());
		detailedResults.setCapacityLabel(jsonNode.get("labels").get("capacityLabel").textValue());
		detailedResults.setProblemSolveLabel(jsonNode.get("labels").get("problemSolveLabel").textValue());
		detailedResults.setDerailmentRisksLabel(jsonNode.get("labels").get("derailmentRisksLabel").textValue());
		detailedResults.setVolatileLabel(jsonNode.get("labels").get("volatileLabel").textValue());
		detailedResults.setMicroManageLabel(jsonNode.get("labels").get("microManageLabel").textValue());
		detailedResults.setClosedLabel(jsonNode.get("labels").get("closedLabel").textValue());

		TablesV2 tables = new TablesV2();
		detailedResults.setTablesV2(tables);
		TableV2 table = new TableV2();
		RowsV2 rows = new RowsV2();
		table.setRowsV2(rows);

		String strNeeds = "";
		String strStrength = "";

		String strDriversScore = "";
		String strAdvDrive = "";
		String strCareerPlan = "";
		String strRolePref = "";
		String strExperienceScore = "";
		String strCore = "";
		String strPerspective = "";
		String strKeyChallenges = "";
		String strAwarenessScore = "";
		String strSelfAware = "";
		String strSituational = "";
		String strLearnAgile = "";
		String strPeople = "";
		String strResults = "";
		String strMental = "";
		String strChange = "";
		String strLeadTraits = "";
		String strFocused = "";
		String strPersistence = "";
		String strTolerance = "";
		String strAssertiveness = "";
		String strOptimistic = "";
		String strCapacity = "";
		String strProblemSolve = "";
		String strDerail = "";
		String strVolatile = "";
		String strMicro = "";
		String strClosed = "";
		String strPartId = "";
		String strWellAboveCount = "";
		String strAboveCount = "";
		String strBelowCount = "";

		log.debug("Starting loop to put data into Jaxb for Completed Users");

		for (int i = 0; i < returnedData.size(); i++) {

			RowV2 rw = new RowV2();
			Object[] theData = returnedData.get(i).toArray();

			// System.out.println("PartIDinforLoop = " + theData[0]);
			strPartId = (String) theData[0];
			strNeeds = (String) theData[1];
			strStrength = (String) theData[2];
			strDriversScore = (String) theData[3];
			strAdvDrive = (String) theData[4];
			strCareerPlan = (String) theData[5];
			strRolePref = (String) theData[6];
			strExperienceScore = (String) theData[7];
			strCore = (String) theData[8];
			strPerspective = (String) theData[9];
			strKeyChallenges = (String) theData[10];
			strAwarenessScore = (String) theData[11];
			strSelfAware = (String) theData[12];
			strSituational = (String) theData[13];
			strLearnAgile = (String) theData[14];
			strPeople = (String) theData[15];
			strResults = (String) theData[16];
			strMental = (String) theData[17];
			strChange = (String) theData[18];
			strLeadTraits = (String) theData[19];
			strFocused = (String) theData[20];
			strPersistence = (String) theData[21];
			strTolerance = (String) theData[22];
			strAssertiveness = (String) theData[23];
			strOptimistic = (String) theData[24];
			strCapacity = (String) theData[25];
			strProblemSolve = (String) theData[26];
			strDerail = (String) theData[27];
			strVolatile = (String) theData[28];
			strMicro = (String) theData[29];
			strClosed = (String) theData[30];
			strWellAboveCount = (String) theData[31];
			strAboveCount = (String) theData[32];
			strBelowCount = (String) theData[33];

			rw.setNeedsCount(new BigInteger(strNeeds));
			rw.setStrengthCount(new BigInteger(strStrength));

			rw.setLastNameFirstName(userMap.get(strPartId));

			if (strDriversScore.equals("1")) {
				rw.setIsDriversStregth(true);
			}

			rw.setAdvancementDriveValue(strAdvDrive);
			rw.setCareerPlanningValue(strCareerPlan);
			rw.setRolePreferenceValue(strRolePref);

			if (strExperienceScore.equals("1")) {
				rw.setIsExperienceStregth(true);
			}

			rw.setCoreValue(strCore);
			rw.setPerspectiveValue(strPerspective);
			rw.setKeyChallengesValue(strKeyChallenges);

			if (strAwarenessScore.equals("1")) {
				rw.setIsSelfAwarenessStregth(true);
			}

			rw.setSelfValue(strSelfAware);
			rw.setSituationalValue(strSituational);

			if (strLearnAgile.equals("1")) {
				rw.setIsLearningAgilityStregth(true);
			}

			rw.setPeopleValue(strPeople);
			rw.setResultsValue(strResults);
			rw.setMentalValue(strMental);
			rw.setChangeValue(strChange);

			if (strLeadTraits.equals("1")) {
				rw.setIsLeadershipTraitsStregth(true);
			}

			rw.setFocusedValue(strFocused);
			rw.setPersistenceValue(strPersistence);
			rw.setToleranceValue(strTolerance);
			rw.setAssertivenessValue(strAssertiveness);
			rw.setOptimisticValue(strOptimistic);

			if (strCapacity.equals("1")) {
				rw.setIsCapacityStregth(true);
			}

			rw.setProblemSolveValue(strProblemSolve);

			if (strDerail.equals("1")) {
				rw.setIsDerailmentRisksStregth(true);
			}

			rw.setVolatileValue(strVolatile);
			rw.setMicroManageValue(strMicro);
			rw.setClosedValue(strClosed);

			rw.setWellAboveCount(new BigInteger(strWellAboveCount));
			rw.setAboveCount(new BigInteger(strAboveCount));
			rw.setBelowCount(new BigInteger(strBelowCount));

			table.getRowsV2().getRowV2().add(rw);

		}

		log.debug("Completed loop to put data into Jaxb for Completed Users");

		// Now fill info for participants that have not completed all
		// assessment(s)

		String strIncompletePartID = "";

		log.debug("Starting loop to put data into Jaxb for Incompleted Users");

		for (int k = 0; k < incompletGroupPIds.size(); k++) {

			strIncompletePartID = incompletGroupPIds.get(k).toString();

			RowV2 r = new RowV2();
			r.setNeedsCount(new BigInteger("-1"));
			r.setStrengthCount(new BigInteger("-1"));

			r.setLastNameFirstName(userMap.get(strIncompletePartID));

			r.setIsDriversStregth(false);
			r.setAdvancementDriveValue("N/A");
			r.setCareerPlanningValue("N/A");
			r.setRolePreferenceValue("N/A");
			r.setIsExperienceStregth(false);
			r.setCoreValue("N/A");
			r.setPerspectiveValue("N/A");
			r.setKeyChallengesValue("N/A");
			r.setIsSelfAwarenessStregth(false);
			r.setSelfValue("N/A");
			r.setSituationalValue("N/A");
			r.setIsLearningAgilityStregth(false);
			r.setPeopleValue("N/A");
			r.setResultsValue("N/A");
			r.setMentalValue("N/A");
			r.setChangeValue("N/A");
			r.setIsLeadershipTraitsStregth(false);
			r.setFocusedValue("N/A");
			r.setPersistenceValue("N/A");
			r.setToleranceValue("N/A");
			r.setAssertivenessValue("N/A");
			r.setOptimisticValue("N/A");
			r.setIsCapacityStregth(false);
			r.setProblemSolveValue("N/A");
			r.setIsDerailmentRisksStregth(false);
			r.setVolatileValue("N/A");
			r.setMicroManageValue("N/A");
			r.setClosedValue("N/A");
			r.setWellAboveCount(new BigInteger("0"));
			r.setAboveCount(new BigInteger("0"));
			r.setBelowCount(new BigInteger("0"));

			table.getRowsV2().getRowV2().add(r);

		}

		log.debug("Completed loop to put data into Jaxb for Incompleted Users");

		detailedResults.getTablesV2().getTableV2().add(table);

		rpt.setDetailedResultsSectionV2(detailedResults);

		String rcm = "";
		rcm = marshalOutput(rpt);

		// test print rcm.xml string
		// System.out.println("RCM = " + rcm);

		@SuppressWarnings("unused")
		String rptUrl = configProperties.getProperty("reportserverBaseUrl") + "/GenerateReportFromRcm";

		log.debug("Right before returning RCM");

		return rcm; // RCM xml string
	}

	private String marshalOutput(TalentGridV2 TalentGridV2) {
		JAXBContext jc = null;
		Marshaller m = null;
		StringWriter sw = new StringWriter();
		try {
			jc = JAXBContext.newInstance(TalentGridV2.class);
			m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(TalentGridV2, sw);

			return sw.toString();
		} catch (JAXBException jbe) {
			log.error("potential TalentGridV2RgrToRcm.marshalOutput() - JAXBException: Msg={}", jbe.getMessage());
			jbe.printStackTrace();
			return null;
		}
	}

	private List<ReportRequestResponse> existingRgrDbLookup(String type, List<Integer> groupPIds, Integer targLevelIndex) {

		List<ReportRequestResponse> existingRgrList = this.reportResponseDao
				.findBatchByParticipantIdAndRgrTypeAndTarget(groupPIds, type, targLevelIndex);

		if (!(existingRgrList == null)) {
			log.debug("{} ppts, {} rgrs", groupPIds.size(), existingRgrList.size());
		} else {
			log.debug("No rgrs found");
			return null;
		}

		return existingRgrList;
	}

	private List<Integer> removeExistingPids(List<Integer> groupPIds, List<ReportRequestResponse> existingRgrList) {

		List<Integer> nonExistingPids = new ArrayList<Integer>();
		nonExistingPids.addAll(groupPIds);
		for (ReportRequestResponse rptReqResp : existingRgrList) {
			Integer persistedPid = rptReqResp.getParticipantId();
			if (nonExistingPids.contains(persistedPid)) {
				nonExistingPids.remove(persistedPid);
			}
		}

		return nonExistingPids;
	}

	private Map<String, Element> prepareExistingScoreMap(List<ReportRequestResponse> existingRgrList)
			throws PalmsException {

		Map<String, Element> persistedScoreMap = new HashMap<String, Element>();

		for (ReportRequestResponse rptReqResp : existingRgrList) {
			String existingRgrStr = rptReqResp.getRgrXml();
			log.debug("existingRgrStr is  = " + existingRgrStr);
			Document rgrDoc = KfapUtils.processRgrXml(existingRgrStr);
			Element calculationPayloadElement = rgrDoc.getDocumentElement();
			String participantId = calculationPayloadElement.getAttribute("particpantId");
			persistedScoreMap.put(participantId, calculationPayloadElement);
		}

		return persistedScoreMap;
	}

	private void saveScoresToDB(Document scoreDoc, String type, Integer targLevelIndex) throws PalmsException {
		log.debug("scoreDoc value is  = " + KfapUtils.getStringFromDocument(scoreDoc));
		NodeList nodes = scoreDoc.getElementsByTagName("calculationPayload");
		for (int i = 0; i < nodes.getLength(); i = i + 1) {
			Element calculationPayload = (Element) nodes.item(i);
			KfapUtils.printElemnt(calculationPayload);
			String participantId = calculationPayload.getAttribute("particpantId");
			String individualScoreElementStr = KfapUtils.getStringFromElement(calculationPayload);
			log.debug("individualScoreElementStr is  = " + individualScoreElementStr);
			this.reportResponseDao.createOrUpdate(Integer.valueOf(participantId), type, individualScoreElementStr,
					targLevelIndex);
		}
	}

	private List<List<String>> mapInputData(String type, List<Integer> groupPIds, Map<Integer, String> mapOfUserIdsProjIds, String lang, String targ)
			throws PalmsException {

		String urlBase = this.configProperties.getProperty("tincanAdminRestUrl");
		String calcScoreUrl = urlBase
				+ RESTFUL_STRING_PARAMS.replace("{instrumentId}", KFP_INST + "").replace("{manifest}", manifest)
						.replace("{tgtRole}", targ);

		log.debug("Calculation URL = " + calcScoreUrl);

		String strPartId = "0";
		String datasetId = "";
		String projId = "";
		Document scoreDoc = null;
		Integer g = 0;
		Integer targLevelIndex = Integer.valueOf(targ);

		Date date = new Date();
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(date);
		Integer Hour = calendar.get(Calendar.HOUR_OF_DAY);
		Integer Minutes = calendar.get(Calendar.MINUTE);
		Integer Seconds = calendar.get(Calendar.SECOND);

		Map<String, Element> scoreMap = new HashMap<String, Element>();
		List<Integer> nonExistingPids = new ArrayList<Integer>();
		String payloadSessionID = Hour + "" + Minutes + "" + Seconds + "";

		List<ReportRequestResponse> existingRgrList = existingRgrDbLookup(type, groupPIds, targLevelIndex);
		if (existingRgrList != null && !existingRgrList.isEmpty()) {
			Map<String, Element> existingScoreMap = prepareExistingScoreMap(existingRgrList);
			scoreMap.putAll(existingScoreMap);
			nonExistingPids = removeExistingPids(groupPIds, existingRgrList);
		} else {
			nonExistingPids.addAll(groupPIds);
		}

		for (int start = 0; start < nonExistingPids.size(); start += 10) {
			int end = Math.min(start + 10, nonExistingPids.size());
			List<Integer> sublist = nonExistingPids.subList(start, end);

			datasetId = "";
			for (int j = 0; j < sublist.size(); j++) {
				strPartId = sublist.get(j).toString();
				projId = mapOfUserIdsProjIds.get(Integer.parseInt(strPartId));
				datasetId = datasetId + "<dataset id=\"" + strPartId + "\" projectId=\"" + projId + "\" />";

			}

			String payload1 = "<CalculationPayload>\n";
			String payload2 = "</CalculationPayload>\n";

			String payload = payload1 + datasetId + payload2;

			log.debug("payload session: " + payloadSessionID + "; Burst Number: " + (g + 1) + "; Payload sent = "
					+ payload);

			scoreDoc = getScores(calcScoreUrl, payload);

			saveScoresToDB(scoreDoc, type, targLevelIndex);

			NodeList nodes = scoreDoc.getElementsByTagName("calculationPayload");

			log.debug("Starting loop to put score data into Map");
			for (int i = 0; i < nodes.getLength(); i = i + 1) {

				Element calculationPayload = (Element) nodes.item(i);
				String Attribute = calculationPayload.getAttribute("particpantId");
				scoreMap.put(Attribute, calculationPayload);
			}
			log.debug("Finished loop to put score data into Map");
			g = g + 1;
		}

		Double noRavensCount = 0.0;
		Double capacityCount = 0.0;
		Double versionCount = 0.0;
		Double totalNoRavensCount = 0.0;
		Double totalCapacityCount = 0.0;
		Double totalVersionCount = 0.0;

		for (Entry<String, Element> scoreMapEntry : scoreMap.entrySet()) {
			Element singlePidResult = scoreMapEntry.getValue();
			KfapUtils.printElemnt(singlePidResult);

			String singlePidResultElementStr = KfapUtils.getStringFromElement(singlePidResult);

			log.debug("singlePidResultElementStr " + singlePidResultElementStr);
			Document singleScoreDoc = KfapUtils.processRgrXml(singlePidResultElementStr);

			noRavensCount = (KfapUtils.getNoRavensCount(singleScoreDoc, KFP_INST, "capacity.score"));
			capacityCount = (KfapUtils.getTotalCount(singleScoreDoc, KFP_INST, "capacity.score"));
			versionCount = (KfapUtils.getVersionCount(singleScoreDoc, KFP_INST, "1"));

			totalNoRavensCount = totalNoRavensCount + noRavensCount;
			totalCapacityCount = totalCapacityCount + capacityCount;
			totalVersionCount = totalVersionCount + versionCount;
		}

		log.debug("All payload data sent, received, and assembled for payload session " + payloadSessionID);

		Integer dCompare = Double.compare(totalCapacityCount, totalNoRavensCount);

		if (dCompare == 0) {
			ravensValue = 0;
		} else {

			if (totalNoRavensCount > 0) {
				ravensValue = 1;
			} else {
				ravensValue = 2;
			}
		}

		// Determine if there are any Version 1 scorings in the Payload

		if (totalVersionCount > 0) {
			mixedV1andV2 = true;
		} else {
			mixedV1andV2 = false;
		}

		List<List<String>> dataforRcm = new ArrayList<List<String>>();

		int strengthCount;
		int needsCount;
		int wellAboveCount;
		int aboveCount;
		int belowCount;

		String strAdvDrive = "";
		String strCareerPlan = "";
		String strRolePref = "";
		String strCore = "";
		String strPerspective = "";
		String strKeyChallenges = "";
		String strSelfAware = "";
		String strSituational = "";
		String strPeople = "";
		String strResults = "";
		String strMental = "";
		String strChange = "";
		String strFocused = "";
		String strPersistence = "";
		String strTolerance = "";
		String strAssertiveness = "";
		String strOptimistic = "";
		String strProblemSolve = "";
		String strVolatile = "";
		String strMicro = "";
		String strClosed = "";

		strPartId = "0";

		log.debug("Starting loop to read data from CalculationPayload XML");

		for (int i = 0; i < groupPIds.size(); i++) {

			strPartId = groupPIds.get(i).toString();

			wellAboveCount = 0;
			aboveCount = 0;
			belowCount = 0;

			driversScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "drivers.score")));
			experienceScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "experience.score")));
			agileScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "agile.score")));
			awarenessScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "awareness.score")));
			traitsScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "traits.score")));
			derailerScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "derailer.score")));

			if (ravensValue == 0) {
				capacityScore = 0;
				capacityPct = 0;

			} else {
				String capacity = KfapUtils.getGroupScoredDatabyElement(scoreMap.get(strPartId), KFP_INST,
						"capacity.score");
				if (capacity == null || capacity == "" || capacity.equals("NaN")) {
					capacityScore = 0;
					capacityPct = 0;

				} else {
					capacityPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
							scoreMap.get(strPartId), KFP_INST, "capacity.norm")));
					capacityScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
							scoreMap.get(strPartId), KFP_INST, "capacity.score")));
				}
			}

			strengthCount = driversScore + experienceScore + agileScore + awarenessScore + traitsScore + derailerScore;

			if (ravensValue == 0 || ravensValue == 1) {
				needsCount = 6 - strengthCount;

			} else {
				strengthCount = strengthCount + capacityScore;
				needsCount = 7 - strengthCount;
			}

			advDrivePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "drivers.advdrive.norm")));

			careerPlanPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "drivers.careerplan.norm")));

			rolePrefPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "drivers.rolepref.norm")));

			keyChallengePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "experience.key.norm")));
			perspectivePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "experience.perspective.norm")));
			changePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "agile.change.norm")));
			mentalPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "agile.mental.norm")));
			peoplePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "agile.people.norm")));
			resultsPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "agile.results.norm")));
			slfAwarePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "awareness.self.norm")));
			sitAwarePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "awareness.sit.norm")));
			focusedPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "traits.focus.norm")));
			optimismPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "traits.optimism.norm")));

			assertivenessPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "traits.assert.norm")));

			closedPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "derailer.closed.norm")));
			microPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "derailer.micro.norm")));
			volatilePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "derailer.volatile.norm")));
			corePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "experience.core.norm")));
			tolerancePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "traits.tolerance.norm")));
			persistencePct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "traits.persist.norm")));

			if (advDrivePct > 49 && advDrivePct < 91) {
				strAdvDrive = "A";
				aboveCount = aboveCount + 1;
			} else if (advDrivePct > 10 && advDrivePct < 50) {
				strAdvDrive = "B";
				belowCount = belowCount + 1;
			} else if (advDrivePct > 90) {
				strAdvDrive = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strAdvDrive = "WB";
			}

			if (careerPlanPct > 49 && careerPlanPct < 91) {
				strCareerPlan = "A";
				aboveCount = aboveCount + 1;
			} else if (careerPlanPct > 10 && careerPlanPct < 50) {
				strCareerPlan = "B";
				belowCount = belowCount + 1;
			} else if (careerPlanPct > 90) {
				strCareerPlan = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strCareerPlan = "WB";
			}

			if (rolePrefPct > 49 && rolePrefPct < 91) {
				strRolePref = "A";
				aboveCount = aboveCount + 1;
			} else if (rolePrefPct > 10 && rolePrefPct < 50) {
				strRolePref = "B";
				belowCount = belowCount + 1;
			} else if (rolePrefPct > 90) {
				strRolePref = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strRolePref = "WB";
			}

			if (corePct > 49 && corePct < 91) {
				strCore = "A";
				aboveCount = aboveCount + 1;
			} else if (corePct > 10 && corePct < 50) {
				strCore = "B";
				belowCount = belowCount + 1;
			} else if (corePct > 90) {
				strCore = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strCore = "WB";
			}

			if (perspectivePct > 49 && perspectivePct < 91) {
				strPerspective = "A";
				aboveCount = aboveCount + 1;
			} else if (perspectivePct > 10 && perspectivePct < 50) {
				strPerspective = "B";
				belowCount = belowCount + 1;
			} else if (perspectivePct > 90) {
				strPerspective = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strPerspective = "WB";
			}

			if (keyChallengePct > 49 && keyChallengePct < 91) {
				strKeyChallenges = "A";
				aboveCount = aboveCount + 1;
			} else if (keyChallengePct > 10 && keyChallengePct < 50) {
				strKeyChallenges = "B";
				belowCount = belowCount + 1;
			} else if (keyChallengePct > 90) {
				strKeyChallenges = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strKeyChallenges = "WB";
			}

			if (slfAwarePct > 49 && slfAwarePct < 91) {
				strSelfAware = "A";
				aboveCount = aboveCount + 1;
			} else if (slfAwarePct > 10 && slfAwarePct < 50) {
				strSelfAware = "B";
				belowCount = belowCount + 1;
			} else if (slfAwarePct > 90) {
				strSelfAware = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strSelfAware = "WB";
			}

			if (sitAwarePct > 49 && sitAwarePct < 91) {
				strSituational = "A";
				aboveCount = aboveCount + 1;
			} else if (sitAwarePct > 10 && sitAwarePct < 50) {
				strSituational = "B";
				belowCount = belowCount + 1;
			} else if (sitAwarePct > 90) {
				strSituational = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strSituational = "WB";
			}

			if (peoplePct > 49 && peoplePct < 91) {
				strPeople = "A";
				aboveCount = aboveCount + 1;
			} else if (peoplePct > 10 && peoplePct < 50) {
				strPeople = "B";
				belowCount = belowCount + 1;
			} else if (peoplePct > 90) {
				strPeople = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strPeople = "WB";
			}

			if (resultsPct > 49 && resultsPct < 91) {
				strResults = "A";
				aboveCount = aboveCount + 1;
			} else if (resultsPct > 10 && resultsPct < 50) {
				strResults = "B";
				belowCount = belowCount + 1;
			} else if (resultsPct > 90) {
				strResults = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strResults = "WB";
			}

			if (mentalPct > 49 && mentalPct < 91) {
				strMental = "A";
				aboveCount = aboveCount + 1;
			} else if (mentalPct > 10 && mentalPct < 50) {
				strMental = "B";
				belowCount = belowCount + 1;
			} else if (mentalPct > 90) {
				strMental = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strMental = "WB";
			}

			if (changePct > 49 && changePct < 91) {
				strChange = "A";
				aboveCount = aboveCount + 1;
			} else if (changePct > 10 && changePct < 50) {
				strChange = "B";
				belowCount = belowCount + 1;
			} else if (changePct > 90) {
				strChange = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strChange = "WB";
			}

			if (focusedPct > 49 && focusedPct < 91) {
				strFocused = "A";
				aboveCount = aboveCount + 1;
			} else if (focusedPct > 10 && focusedPct < 50) {
				strFocused = "B";
				belowCount = belowCount + 1;
			} else if (focusedPct > 90) {
				strFocused = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strFocused = "WB";
			}

			if (persistencePct > 49 && persistencePct < 91) {
				strPersistence = "A";
				aboveCount = aboveCount + 1;
			} else if (persistencePct > 10 && persistencePct < 50) {
				strPersistence = "B";
				belowCount = belowCount + 1;
			} else if (persistencePct > 90) {
				strPersistence = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strPersistence = "WB";
			}

			if (tolerancePct > 49 && tolerancePct < 91) {
				strTolerance = "A";
				aboveCount = aboveCount + 1;
			} else if (tolerancePct > 10 && tolerancePct < 50) {
				strTolerance = "B";
				belowCount = belowCount + 1;
			} else if (tolerancePct > 90) {
				strTolerance = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strTolerance = "WB";
			}

			if (assertivenessPct > 49 && assertivenessPct < 91) {
				strAssertiveness = "A";
				aboveCount = aboveCount + 1;
			} else if (assertivenessPct > 10 && assertivenessPct < 50) {
				strAssertiveness = "B";
				belowCount = belowCount + 1;
			} else if (assertivenessPct > 90) {
				strAssertiveness = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strAssertiveness = "WB";
			}

			if (optimismPct > 49 && optimismPct < 91) {
				strOptimistic = "A";
				aboveCount = aboveCount + 1;
			} else if (optimismPct > 10 && optimismPct < 50) {
				strOptimistic = "B";
				belowCount = belowCount + 1;
			} else if (optimismPct > 90) {
				strOptimistic = "WA";
				wellAboveCount = wellAboveCount + 1;
			} else {
				strOptimistic = "WB";
			}

			if (ravensValue == 0) {
				strProblemSolve = "N/A";
			} else {

				if (capacityPct > 49 && capacityPct < 91) {
					strProblemSolve = "A";
					if (ravensValue == 2) {
						aboveCount = aboveCount + 1;
					}
				} else if (capacityPct == 0) {
					strProblemSolve = "ND";
				} else if (capacityPct > 10 && capacityPct < 50) {
					strProblemSolve = "B";
					if (ravensValue == 2) {
						belowCount = belowCount + 1;
					}
				} else if (capacityPct > 90) {
					strProblemSolve = "WA";
					if (ravensValue == 2) {
						wellAboveCount = wellAboveCount + 1;
					}
				} else {
					strProblemSolve = "WB";
				}
			}

			if (volatilePct < 91) {
				strVolatile = "A";
			} else {
				strVolatile = "D";
			}

			if (microPct < 91) {
				strMicro = "A";
			} else {
				strMicro = "D";
			}

			if (closedPct < 91) {
				strClosed = "A";
			} else {
				strClosed = "D";
			}

			// ##############################################################

			// 0 = strPartId, 1 = strNeedsCount, 2 = strStrengthCount 3 =
			// driversScore, 4 = strAdvDrive, 5 = strCareerPlan, 6 = strRolePref
			// 7 =
			// experienceScore, 8 = strCore, 9 = strPerspective, 10 =
			// strKeyChallenges, 11 = awarenessScore, strSelfAware = 12 ,
			// strSituational = 13, agileScore = 14, strPeople = 15 , strResults
			// = 16, strMental = 17, strChange = 18, traitsScore = 19,
			// strFocused = 20, strPersistence = 21, strTolerance = 22,
			// strAssertiveness
			// = 23 , strOptimistic = 24, capacityScore = 25, strProblemSolve =
			// 26, derailerScore = 27, strVolatile = 28, strMicro = 29,
			// strClosed = 30, strWellAboveCount = 31, strAboveCount = 32,
			// strBelowCount = 33

			dataforRcm.add(Arrays.asList(strPartId, needsCount + "", strengthCount + "", driversScore + "",
					strAdvDrive, strCareerPlan, strRolePref, experienceScore + "", strCore, strPerspective,
					strKeyChallenges, awarenessScore + "", strSelfAware, strSituational, agileScore + "", strPeople,
					strResults, strMental, strChange, traitsScore + "", strFocused, strPersistence, strTolerance,
					strAssertiveness, strOptimistic, capacityScore + "", strProblemSolve, derailerScore + "",
					strVolatile, strMicro, strClosed, wellAboveCount + "", aboveCount + "", belowCount + ""));

			/*log.debug("strPartId = " + strPartId);
			log.debug("interestScore = " + interestScore);
			log.debug("experienceScore = " + experienceScore);
			log.debug("agileScore = " + agileScore);
			log.debug("awarenessScore = " + awarenessScore);
			log.debug("traitsScore = " + traitsScore);
			log.debug("derailerScore = " + derailerScore);

			if (NO_RAVENS) {
				log.debug("capacityScore = N/A");
			} else {
				log.debug("capacityScore = " + capacityScore);
			}

			// log.debug("devEffortScore = " + devEffortScore);
			log.debug("drivePct = " + drivePct);
			log.debug("focusPct = " + focusPct);
			log.debug("engagementPct = " + engagementPct);
			log.debug("perspectivePct = " + perspectivePct);
			log.debug("changePct = " + changePct);
			log.debug("mentalPct = " + mentalPct);
			log.debug("peoplePct = " + peoplePct);
			log.debug("resultsPct = " + resultsPct);
			log.debug("slfAwarePct = " + slfAwarePct);
			log.debug("sitAwarePct = " + sitAwarePct);
			log.debug("focusedPct = " + focusedPct);
			log.debug("optimismPct = " + optimismPct);
			log.debug("composedPct = " + composedPct);
			log.debug("closedPct = " + closedPct);
			log.debug("microPct = " + microPct);
			log.debug("volatilePct = " + volatilePct);
			log.debug("corePct = " + corePct);
			log.debug("tolerancePct = " + tolerancePct);
			log.debug("sociablePct = " + sociablePct);
			log.debug("keyChallengePct = " + keyChallengePct);
			log.debug("capacityPct = " + capacityPct);
			log.debug("strengthCount = " + strengthCount);
			log.debug("devEffort = " + devEffort);

			log.debug("wellAboveCount = " + wellAboveCount);
			log.debug("aboveCount = " + aboveCount);
			log.debug("belowCount = " + belowCount);*/
		}

		log.debug("Finished loop to read data from CalculationPayload XML");

		return dataforRcm;

	}

	@SuppressWarnings({ "restriction" })
	private Document getScores(String strUrl, String payload) throws PalmsException {
		Document doc = null;
		HttpURLConnection conn = null;
		URL url = null;
		String line = "";
		String xml = "";

		try {
			url = new URL(strUrl);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/xml");
			conn.setDoOutput(true);

			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

			writer.write(payload);
			writer.flush();

			// Get Response
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while ((line = in.readLine()) != null) {
				xml += line;
			}
			in.close();
			writer.close();
			conn.disconnect();

			try {
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				doc = docBuilder.parse(new InputSource(new ByteArrayInputStream(xml.getBytes())));
			} catch (ParserConfigurationException e) {
				log.error("Parser error creating document from ext. server response. Message: {}", e.getMessage());
				throw new PalmsException("Parser error creating document from ext. server response.",
						PalmsErrorCode.RCM_GEN_FAILED.getCode(), e.getMessage());

			} catch (IOException e) {
				log.error("IO error creating document from ext. server response. Message: {}", e.getMessage());
				throw new PalmsException("IO error creating document from ext. server response.",
						PalmsErrorCode.RCM_GEN_FAILED.getCode(), e.getMessage());
			} catch (SAXException e) {
				log.error("SAX error creating document from ext. server response. Message: {}", e.getMessage());
				throw new PalmsException("SAX error creating document from ext. server response.",
						PalmsErrorCode.RCM_GEN_FAILED.getCode(), e.getMessage());
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return doc;
	}

	private Document combineXML(Document mainDoc, Document subDoc) throws PalmsException {

		try {

			NodeList nodes = mainDoc.getElementsByTagName("calculationPayload");

			NodeList nodes1 = subDoc.getElementsByTagName("calculationPayload");

			for (int i = 0; i < nodes1.getLength(); i = i + 1) {

				Node n = mainDoc.importNode(nodes1.item(i), true);
				nodes.item(i).getParentNode().appendChild(n);
			}

		} catch (Exception e1) {
			throw new PalmsException("Problem combining XML Payloads.", PalmsErrorCode.RCM_GEN_FAILED.getCode(),
					e1.getMessage());
		}

		return mainDoc;
	}

	private String getLanguage(String language) {
		/* 
		 * in a case other languages are not available it use default "en", 
		 * which the langCode = langCodes.get(0) in the language code array.   
		 */
		String lang = "";

		if (language != null) {
			if (Arrays.asList(langCodes).contains(language)) {
				if (language.indexOf("_") != -1) {
					lang = language.replace("_", "-");
				} else if (language.equals("en")) {
					lang = DEFAULT_LANG;
				} else {
					lang = language;
				}
			} else {
				lang = DEFAULT_LANG;
			}
		} else {
			lang = DEFAULT_LANG;
		}
		return lang;
	}

	public static String getReportManifestDir() {
		return reportManifestDir;
	}

	public static void setReportManifestDir(String reportManifestDir) {
		TalentGridRgrToRcmV2.reportManifestDir = reportManifestDir;
	}

	public static String getReportContentDir() {
		return reportContentDir;
	}

	public static void setReportContentDir(String reportContentDir) {
		TalentGridRgrToRcmV2.reportContentDir = reportContentDir;
	}

	public JsonNode getJsonNode() {
		return jsonNode;
	}

	public void setJsonNode(JsonNode jsonNode) {
		this.jsonNode = jsonNode;
	}

}
