/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/*
 * DevElemant object used in the development section of LvaDbReport only.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder={"title", "text", "devPoint"})
public class DevElement
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String title;
	private String text;
	@XmlElementWrapper(name = "devPoints")
	private List<DevPoint> devPoint;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public DevElement()
	{
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "      DevElement:  ";
		str += "title=" + this.title + ", text=" + this.text + "\n";
		if (devPoint != null)
		{
			for (DevPoint dp : this.devPoint)
			{
				str += "\n      " + dp.toString();
			}
		}
		
		return str;
	}

	// Getters & setters

	//----------------------------------
	public String getTitle()
	{
		return this.title;
	}

	public void setTitle(String value)
	{
		this.title = value;
	}

	//----------------------------------
	public String getText()
	{
		return this.text;
	}

	public void setText(String value)
	{
		this.text = value;
	}

	//----------------------------------
	// Note that the list has a "get" only
	public List<DevPoint> getDevPoint()
	{
		if (this.devPoint == null)
			this.devPoint = new ArrayList<DevPoint>();
		
		return this.devPoint;
	}

}
