/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.shell.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.pdinh.core.vo.Participant;

/*
 * This is the Head class for the IbReport JAXB structure.
 * 
 * IbReport models the RCM for the Shell Leadership Assessment Report.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"reportType", "reportSubtype", "reportTitle", "level",
								 "reportDate", "assessmentDate", "scoringDate",
								 "company", "participant", "overallComment", "section", "elUrl"})
@XmlRootElement(name = "ibReport")
public class IbReport
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String reportType;
	private String reportSubtype;
	private String reportTitle;
	private String level;
	private String reportDate;
	private String assessmentDate;
	private String scoringDate;
	private String company;
	private Participant participant;
	private String overallComment;
	private List<Section> section;
	private String elUrl;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public IbReport()
	{
	}

	// 1-parameter constructor (clone)
	public IbReport(IbReport orig)
	{
		this.reportType = orig.getReportType() == null ? null : new String(orig.getReportType());
		this.reportTitle = orig.getReportSubtype() == null ? null : new String(orig.getReportSubtype());
		this.reportTitle = orig.getReportTitle() == null ? null : new String(orig.getReportTitle());
		this.level = orig.getLevel() == null ? null : new String(orig.getLevel());
		this.reportDate = orig.getReportDate() == null ? null : new String(orig.getReportDate());
		this.assessmentDate = orig.getAssessmentDate() == null ? null : new String(orig.getAssessmentDate());
		this.scoringDate = orig.getScoringDate() == null ? null : new String(orig.getScoringDate());
		this.company = orig.getCompany() == null ? null : new String(orig.getCompany());
		this.participant = orig.getParticipant() == null ? null : new Participant(orig.getParticipant());
		this.overallComment = orig.getOverallComment() == null ? null : new String(orig.getOverallComment());
		if (orig.getSection() == null)
		{
			this.section = null;
		}
		else
		{
			this.section = new ArrayList<Section>();
			for (Section ss : orig.getSection())
			{
				this.section.add(new Section(ss));
			}
		}
		this.elUrl = orig.getElUrl() == null ? null : new String(orig.getElUrl());
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "IbReport:  ";
		str += "reportType=" + this.reportType;
		str += ", reportSubtype=" + this.reportSubtype;
		str += "reportTitle=" + this.reportTitle;
		str += ", level=" + this.level;
		str += ", dates (r/a/s)=" + this.reportDate + "/" + this.assessmentDate + "/" + this.scoringDate;
		str += ", company=" + this.company + "  ";
		if (this.participant != null)
			str += this.participant.toString() + "  ";
		str += "\nOverall-" + this.overallComment + "  ";
		if (this.section != null) {
			for (Section ss : this.section)
			{
				str += "\n      " + ss.toString();
			}
		}
		str += "elUrl=" + this.elUrl;
		
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getReportType()
	{
		return this.reportType;
	}

	public void setReportType(String value)
	{
		this.reportType = value;
	}

	//----------------------------------
	public String getReportSubtype()
	{
		return this.reportSubtype;
	}

	public void setReportSubtype(String value)
	{
		this.reportSubtype = value;
	}

	//----------------------------------
	public String getReportTitle()
	{
		return this.reportTitle;
	}

	public void setReportTitle(String value)
	{
		this.reportTitle = value;
	}

	//----------------------------------
	public String getLevel()
	{
		return this.level;
	}

	public void setLevel(String value)
	{
		this.level = value;
	}

	//----------------------------------
	public String getReportDate()
	{
		return this.reportDate;
	}

	public void setReportDate(String value)
	{
		this.reportDate = value;
	}

	//----------------------------------
	public String getAssessmentDate()
	{
		return this.assessmentDate;
	}

	public void setAssessmentDate(String value)
	{
		this.assessmentDate = value;
	}

	//----------------------------------
	public String getScoringDate()
	{
		return this.scoringDate;
	}

	public void setScoringDate(String value)
	{
		this.scoringDate = value;
	}

	//----------------------------------
	public String getCompany()
	{
		return this.company;
	}

	public void setCompany(String value)
	{
		this.company = value;
	}

	//----------------------------------
	public Participant getParticipant()
	{
		return this.participant;
	}

	public void setParticipant(Participant value)
	{
		this.participant = value;
	}

	//----------------------------------
	public String getOverallComment()
	{
		return this.overallComment;
	}

	public void setOverallComment(String value)
	{
		this.overallComment = value;
	}

	//----------------------------------
	// Lists do lazy initialization and there is no setter
	public List<Section> getSection()
	{
		if(this.section == null)
			section = new ArrayList<Section>();
		
		return this.section;
	}

	//----------------------------------
	public String getElUrl()
	{
		return this.elUrl;
	}

	public void setElUrl(String value)
	{
		this.elUrl = value;
	}
}
