/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva.ut;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.pdinh.core.vo.Hyperlink;
import com.pdinh.core.vo.Participant;
import com.pdinh.core.vo.Project;
import com.pdinh.project.lva.vo.AppSection;
import com.pdinh.project.lva.vo.AppSubSection;
import com.pdinh.project.lva.vo.AppTextGroup;
import com.pdinh.project.lva.vo.Appendix;
import com.pdinh.project.lva.vo.Characteristic;
import com.pdinh.project.lva.vo.Competency;
import com.pdinh.project.lva.vo.DevElement;
import com.pdinh.project.lva.vo.DevPoint;
import com.pdinh.project.lva.vo.DevSection;
import com.pdinh.project.lva.vo.Development;
import com.pdinh.project.lva.vo.LvaDbReport;
import com.pdinh.project.lva.vo.Ratings;
import com.pdinh.project.lva.vo.ScoredSection;
import com.pdinh.project.lva.vo.Summary;
import com.pdinh.project.lva.vo.Superfactor;
import com.pdinh.project.lva.vo.TextSection;
import com.sun.xml.bind.v2.runtime.IllegalAnnotationException;

/**
 * Test the JAXB annotations (creating XML from POJOs) for the LVA Dashboard
 * Report Content Model.
 * 
 * Generally used to test that we have the class annotations right.
 * 
 * NOT a JUnit test
 */
public class TestLvaRcmMarshal {

	/*
	 * Test for Marshalling for LVA dashboard reports.  This example builds a Readiness
	 * report but the format is the same for all (Readiness, Fit, and Development).
	 */
	public static void main(String args[]) {
		LvaDbReport lrdr;

		System.out.println("Start LVA RCM marshalling (POJO to XML) tests...");

		// Do only one of these at a time
		// Development report
		System.out.println("Testing Dev report object...");
		lrdr = getDevRptObject();

		// // Readiness Report
		// System.out.println("Testing Readiness report object...");
		// lrdr = getRediRptObject();

		// not done yet
		// System.out.println("Testing Fit report object...");
		// lrdr = getFitRptObject();

		// Marshall it
		JAXBContext jc = null;
		Marshaller marshaller = null;
		StringWriter sw = null;
		try {
			System.out.println("Set up the JAXBContext...");
			jc = JAXBContext.newInstance(LvaDbReport.class);
			marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); // Make
																			// pretty
																			// output
			sw = new StringWriter();
			marshaller.marshal(lrdr, sw);
			// Dump the output to screen
			System.out.print("Marshalling complete...  ");
			String str = (sw == null) ? "Failed... no output" : "Output XML:\n" + sw.toString();
			System.out.println(str);
		} catch (IllegalAnnotationException e) {
			System.out.println("Illegal annotation:");
			System.out.println(e.toString());
			return;
		} catch (JAXBException e) {
			System.out.println("JAXBException detected... Terminating.  Msg=" + e.getMessage());
			e.printStackTrace();
			return;
		} finally {
			if (sw != null) {
				try {
					sw.close();
				} catch (Exception e) {
					System.out.println("Exception on StringWriter close in TestLvaRcmMarshal.main().  Ignored.  Msg="
							+ e.getMessage());
				}
				sw = null;
			}
			marshaller = null;
			jc = null;
			lrdr = null;
		}
	}

	private static LvaDbReport getDevRptObject() {
		// create the object structure for the Development report
		LvaDbReport ret = new LvaDbReport();
		ret.setReportType("LVA Dashboard");
		ret.setReportSubtype("Development");
		ret.setReportTitle("Development Dashboard Report");
		ret.setLevelName("Mid-Level Leader");
		ret.setReportDate("2012-09-27");
		ret.setAssessmentDate("2012-09-23");
		ret.setProject(new Project(1234, "Mid-level Leader Virtual Assessment"));
		Participant pp = new Participant();
		pp.setId(12);
		pp.setExtId("367990");
		pp.setFirstName("Jonathan");
		pp.setLastName("Douglas");
		pp.setTitle("Current");
		ret.setParticipant(pp);
		ret.setCompanyName("Global Corporation");

		// Summary section
		Summary sum = new Summary();
		ret.setSummary(sum);
		sum.setTitle("Summary");
		sum.setConfigText("Jonathan Douglas� assessment results reflect an integration of his responses to inventories (including personality, cognitive ability, experiences, and interests) as well as his performance in the online business simulator.");
		sum.setRationaleText("Jonathan Douglas displays a strong interest in leadership and has had strong leadership experiences to date. However, his foundational predispositions and leadership skills lack balance in that he is strong in some areas yet weak in others.");
		TextSection ts = new TextSection();
		sum.getTextSection().add(ts);
		ts.setTitle("Strengths");
		ts.getPoint()
				.add("Jonathan Douglas displays strong global perspective by demonstrating sensitivity and a global mindset when making decisions that involve cultural issues or geographic differences.");
		ts.getPoint()
				.add("Jonathan Douglas displays strong execution by holding others accountable for completing work responsibilities, setting standards, and developing realistic plans.");
		ts.getPoint()
				.add("Jonathan Douglas� business operations experience to date compares favorably with that of successful mid-level leaders (place holder for more descriptive text around business operations).");
		ts.getPoint()
				.add("Jonathan Douglas� personal development experience to date compares favorably with that of successful mid-level leaders (place holder for more descriptive text around personal development).");
		ts.getPoint()
				.add("Jonathan Douglas� interest, energy and passion for a more challenging leadership role compare favorably with that of successful mid-level leaders and should contribute to his transition into a mid-level role or position.");
		ts = new TextSection();
		sum.getTextSection().add(ts);
		ts.setTitle("Development Needs");
		ts.getPoint()
				.add("Jonathan Douglas shows the need to develop at fostering innovation. Jonathan displays little curiosity or open-mindedness when presented with problems and/or resists others� efforts to challenge current processes, downplaying the efforts of those who try to view issues in new and different ways.");
		ts.getPoint()
				.add("Jonathan Douglas needs to develop at promoting collaboration. Jonathan does not involve others in plans and actions or make an effort to understand their views; allows conflicting views to limit others� involvement and minimize collaboration.");
		ts.getPoint()
				.add("Jonathan Douglas shows the need to develop at engaging and develop others. Jonathan gives feedback that is inaccurate or insensitive, or shies away from providing feedback. Jonathan demonstrates a negative attitude or lack of commitment; does not attempt to encourage commitment in others, acknowledge their efforts, or convey confidence in their abilities.");
		ts.getPoint()
				.add("Jonathan Douglas shows the need to develop at building support and influencing others. Jonathan does not provide sufficient rationale to support recommendations, and struggles to address others� needs and priorities; is unable to rely on relationships to build support.");
		ts.getPoint()
				.add("Jonathan should pay particular attention to the tendency to let risk tolerance lead to excessively risky decisions.");
		ts.getPoint()
				.add("Jonathan should pay particular attention to the tendency to let independent nature isolate them from the team or peers.");
		ts.getPoint()
				.add("Jonathan should pay particular attention to the tendency to be overly rigid or structured in his approach.");
		ts = new TextSection();
		sum.getTextSection().add(ts);
		ts.setTitle("Leadership Derailers");
		ts.getPoint()
				.add("Jonathan�s results suggest low risk of derailment while transitioning to a mid level leadership role or position.");
		ts = new TextSection();
		sum.getTextSection().add(ts);
		ts.setTitle("Development Focus");
		ts.getPoint()
				.add("Jonathan should focus on balancing his foundations and increasing his leadership competencies. Particularly, he needs to focus on his people leadership skills including collaborating, engaging, and influencing others. Additionally he should work on being more flexible and adaptable.");
		// Here ends the recommendation section

		// Ratings section
		Ratings rat = new Ratings();
		ret.setRatings(rat);
		rat.setTitle("Detailed Ratings");
		rat.setText("The following shows more detailed assessment ratings for this person. In the graphic ratings below, green boxes represent a strength, yellow boxes represent tentative/mixed scores, and orange boxes represent unfavorable scores. In addition, very strong competencies are indicated with blue boxes.The ranges of favorability vary from category to category, and in some cases a person can score too highly on a category. For example, under Leadership Foundations, a person could be be too low on attention to detail (yellow or orange boxes on the left side), but they could also be too highly focused on details which could negatively impact their work (yellow or orange boxes on the right side).");
		// Leadership Competencies
		ScoredSection ss = new ScoredSection();
		rat.getScoredSection().add(ss);
		ss.setId("leadership_competencies");
		ss.setType("v423");
		ss.setTitle("Leadership Competencies");
		ss.setText("Jonathan Douglas demonstrated a mixed level of performance on competencies and skills. While he executes well and shows a global perspective, he has room for development in his people leadership skills and could do more to innovate.");
		Superfactor sf = new Superfactor();
		ss.getSuperfactor().add(sf);
		sf.setType("v423");
		sf.setName("Thought Leadership");
		sf.getCompetency().add(new Competency(3.0f, "v423", "Reach Strategic Decisions"));
		sf.getCompetency().add(new Competency(3.5f, "v423", "Display Global Perspective"));
		sf.getCompetency().add(new Competency(2.5f, "v423", "Foster Innovation"));
		sf = new Superfactor();
		ss.getSuperfactor().add(sf);
		sf.setName("Results Leadership");
		sf.getCompetency().add(new Competency(3.0f, "Manage Execution"));
		sf.getCompetency().add(new Competency(3.0f, "Show Courageous Determination"));
		sf = new Superfactor();
		ss.getSuperfactor().add(sf);
		sf.setName("People Leadership");
		sf.getCompetency().add(new Competency(2.5f, "Promote Collaboration"));
		sf.getCompetency().add(new Competency(2.5f, "Engage and Develop Others"));
		sf.getCompetency().add(new Competency(2.5f, "Build Support and Influence"));
		sf = new Superfactor();
		ss.getSuperfactor().add(sf);
		sf.setName("Personal Leadership");
		sf.getCompetency().add(new Competency(4.0f, "Adapt and Learn"));
		sf.getCompetency().add(new Competency(3.0f, "Establish Credibility"));
		// Leadership Experience
		ss = new ScoredSection();
		rat.getScoredSection().add(ss);
		ss.setId("leadership_experience");
		ss.setType("v36");
		ss.setTitle("Leadership Experience");
		ss.setText("Jonathan Douglas� leadership experience to date compares favorably with that of successful mid-level leaders and positions him well for an uneventful transition into a mid-level leadership role.");
		ss.getCharacteristic().add(new Characteristic(5, "Business Operations"));
		ss.getCharacteristic().add(new Characteristic(4, "Handling Tough Challenges"));
		ss.getCharacteristic().add(new Characteristic(4, "High Visibility"));
		ss.getCharacteristic().add(new Characteristic(4, "Growing the Business"));
		ss.getCharacteristic().add(new Characteristic(5, "Personal Development"));
		// Leadership Foundations
		ss = new ScoredSection();
		rat.getScoredSection().add(ss);
		ss.setId("leadership_foundations");
		ss.setType("v252");
		ss.setTitle("Leadership Foundations");
		ss.setText("Jonathan Douglas� leadership styles and predispositions lack balance, meaning he tends to overuse some styles and/or underuse others. He should pay particular attention to the tendency to letting his risk tolerance lead to excessively risky decisions, his independent nature isolate him from the team, and being overly rigid or structured in his approach.");
		ss.getCharacteristic().add(new Characteristic(4, "Problem Solving"));
		ss.getCharacteristic().add(new Characteristic(6, "Intellectual Engagement"));
		ss.getCharacteristic().add(new Characteristic(4, "Attention to Detail"));
		ss.getCharacteristic().add(new Characteristic(5, "Impact/Influence"));
		ss.getCharacteristic().add(new Characteristic(8, "Interpersonal Engagement"));
		ss.getCharacteristic().add(new Characteristic(7, "Achievement Drive"));
		ss.getCharacteristic().add(new Characteristic(8, "Advancement Drive"));
		ss.getCharacteristic().add(new Characteristic(2, "Collective Orientation"));
		ss.getCharacteristic().add(new Characteristic(3, "Flexibility/Adaptability"));
		// Leadership Interest
		ss = new ScoredSection();
		rat.getScoredSection().add(ss);
		ss.setId("leadership_interest");
		ss.setType("v351");
		ss.setTitle("Leadership Interest");
		ss.setText("Mr. Douglas� interest, energy, and passion for a more challenging leadership role compare favorably with that of successful mid-level leaders and should contribution to his transition into a MLL position.");
		ss.getCharacteristic().add(new Characteristic(7, "Leadership Aspirations"));
		ss.getCharacteristic().add(new Characteristic(8, "Career Drivers"));
		ss.getCharacteristic().add(new Characteristic(3, "Learning Orientation"));
		ss.getCharacteristic().add(new Characteristic(4, "Experience Orientation"));
		// Leadership Derailers
		ss = new ScoredSection();
		rat.getScoredSection().add(ss);
		ss.setId("leadership_derailers");
		ss.setType("v333");
		ss.setTitle("Leadership Derailers");
		ss.setText("Mr. Douglas� results suggest low risk of derailment while transitioning to a mid level leadership role or position.");
		ss.getCharacteristic().add(new Characteristic(6, "Ego Centered"));
		ss.getCharacteristic().add(new Characteristic(6, "Manipulative"));
		ss.getCharacteristic().add(new Characteristic(4, "Micro Managing"));
		ss.getCharacteristic().add(new Characteristic(4, "Unwillingness to Confront"));
		// End of rating section

		// Development section
		Development dev = new Development();
		ret.setDevelopment(dev);
		dev.setTitle("Development Recommendations");
		// Leveraging Strengths
		DevSection ds = new DevSection();
		dev.getDevSection().add(ds);
		ds.setTitle("Leveraging Strengths");
		ds.setText("In addition to focusing on development needs, it is also vitally important to leverage strengths. Jonathan should use his experience and his skills in managing execution. Building on these strengths will enhance his reputation as a leader and provide a solid foundation for continued growth and success.");
		// Addressing Development Needs
		ds = new DevSection();
		dev.getDevSection().add(ds);
		ds.setTitle("Addressing Development Needs");
		DevElement de = new DevElement();
		ds.getDevElement().add(de);
		de.setTitle("Leadership Foundations");
		de.setText("Leadership foundations refer to your leadership styles and predispositions. These are the attributes that make us who we are and influence the ways we behave and our reactions to new situations or unexpected events. When it comes to leadership styles, balance and moderation are key. A repertoire of styles creates versatility by allowing you to draw upon the style or styles that are most effective for a given situation. Your results indicate a lack of balance in your leadership styles meaning that you are likely to overuse some styles and under-use others and suggesting that leadership foundations is a priority development area for you. Listed below are the leadership foundations where additional development will help to balance your leadership styles and make the transition to senior level leader smoother and more effective.");
		DevPoint dp = new DevPoint();
		de.getDevPoint().add(dp);
		dp.setTitle("Advancement Drive");
		dp.setText("Your results suggest you are more comfortable with risk than is typical for leaders at your level. Don�t let your comfort with risk and preference for advancement over security lead you to take unnecessary risks and to make decisions without having fully collected or consider all relevant data and points of view. Here is a link to content you may find helpful for your development in this area:");
		dp.getLink()
				.add(new Hyperlink(
						"Assessing Risk",
						"http://www.bogus.com/login?command=lms&w=1&email=bogus&voucher_id=123abc456xyz&firstname=bogus&lastname=bogus&cdrom=false&name=bogusmod1"));
		dp = new DevPoint();
		de.getDevPoint().add(dp);
		dp.setTitle("Collective Orientation");
		dp.setText("Your results suggest you are more comfortable working on your own than is typical for leaders at your level. This may isolate or even alienate you from your peers and direct reports. Don�t let you independent nature lead you to isolate yourself from your team or reject efforts at consensus out of hand. Here is a link to content you may find helpful for your development in this area:");
		dp.getLink()
				.add(new Hyperlink(
						"Team Work",
						"http://www.bogus.com/login?command=lms&w=1&email=bogus&voucher_id=123abc456xyz&firstname=bogus&lastname=bogus&cdrom=false&name=bogusmod2"));
		dp = new DevPoint();
		de.getDevPoint().add(dp);
		dp.setTitle("Flexibility/Adaptability");
		dp.setText("Your responses indicate that you are more prone to anxiety and worry than others at the target leadership level and may struggle to maintain composure in stressful situations. Don�t let your concerns cause you to resist changes that would improve or resolve the situation or challenge. Be careful not to let your relatively lower tolerance of stressful situations create unnecessary and unhelpful stress and anxiety for those around you. Here is a link to content you may find helpful for your development in this area:");
		dp.getLink()
				.add(new Hyperlink(
						"Handling Difficult Circumstances",
						"http://www.bogus.com/login?command=lms&w=1&email=bogus&voucher_id=123abc456xyz&firstname=bogus&lastname=bogus&cdrom=false&name=bogusmod3"));
		de = new DevElement();
		ds.getDevElement().add(de);
		de.setTitle("Leadership Competencies");
		de.setText("Competencies are the skills a person can demonstrate and utilize to be an effective leader. Listed below are the areas where additional development will help to prepare you for the transition to mid level leader smoother and more effective.");
		dp = new DevPoint();
		de.getDevPoint().add(dp);
		dp.setTitle("Fostering Innovation");
		dp.setText("Creative people engage in undisciplined, open-minded, and uncritical thinking when they face problems. This mind-set allows them to generate more options. To increase your use of creative approaches, try the following suggestions: Realize that how you view a situation often corresponds to how you approach it. Develop a more positive mind-set; instead of thinking of a situation as a problem to solve, view it as a challenge to meet and exceed.  Assess what types of situations intrigue you, and make you curious and enthusiastic. Then identify what makes other situations unappealing. Here is a link to content you may find helpful for your development in this area:");
		dp.getLink()
				.add(new Hyperlink(
						"How Do I Innnovate?",
						"http://www.bogus.com/login?command=lms&w=1&email=bogus&voucher_id=123abc456xyz&firstname=bogus&lastname=bogus&cdrom=false&name=iaprbn02fxx4"));
		dp = new DevPoint();
		de.getDevPoint().add(dp);
		dp.setTitle("Promoting Collaboration");
		dp.setText("Teamwork takes time, effort, and consistency. This can be difficult to achieve across groups, especially if there are personality conflicts, resource constraints, rivalries, or other factors that create competitiveness instead of collaboration. They can also engage teams in a collaborative process. When a cross-functional team comes to a decision point, avoid voting for decisions. Instead, strive for consensus. Voting is not likely to produce a true team decision.  Evaluate people on their willingness and ability to work as part of a team in the organization. Encourage them to develop relationships throughout the organization, not just within their own functional area. Here is a link to content you may find helpful for your development in this area:");
		dp.getLink()
				.add(new Hyperlink(
						"Involve Others",
						"http://www.bogus.com/login?command=lms&w=1&email=bogus&voucher_id=123abc456xyz&firstname=bogus&lastname=bogus&cdrom=false&name=bogusmod5"));
		dp = new DevPoint();
		de.getDevPoint().add(dp);
		dp.setTitle("Engaging and Developing Others");
		dp.setText("When individuals have an opportunity to meet both professional and personal needs within the same environment, they often achieve optimum performance. As a leader, you can help create this environment by offering information and encouragement to each person on your team. Consider the following suggestions: Develop strong, trusting relationships with your direct reports. Learn about each person�s personal and professional goals, why they come to work, why they chose to be a member of this team, and what they hope to personally accomplish if the team achieves its goals and vision. Make explicit connections between the team�s goal and vision, and each person�s needs, motives, and personal and professional goals. Watch for opportunities to point out how the team�s success will help each team member to succeed. Here is a link to content you may find helpful for your development in this area:");
		dp.getLink()
				.add(new Hyperlink(
						"Qualities of a Superior Mentor",
						"http://www.bogus.com/login?command=lms&w=1&email=bogus&voucher_id=123abc456xyz&firstname=bogus&lastname=bogus&cdrom=false&name=bogusmod8"));
		dp = new DevPoint();
		de.getDevPoint().add(dp);
		dp.setTitle("Building Support and Influencing Others");
		dp.setText("If you want to engage, motivate, inspire, captivate, energize, or convince people, you need to present your ideas with confidence and enthusiasm. Your ideas deserve the best delivery you can give them. Consider the following suggestions: Increase your confidence by becoming well versed in your topic. Be ready to talk about it broadly and in detail, and to answer a wide range of questions. Clarify your purpose for sharing your ideas. People need an overall sense of what you�re talking about and why they should listen to you. Show your confidence by talking about how people will benefit from your idea. Talk about how it connects to other initiatives, both present and future. When appropriate, tie your idea or solution to an already acknowledged problem. Here is a link to content you may find helpful for your development in this area:");
		dp.getLink()
				.add(new Hyperlink(
						"Increase My Sphere of Influence",
						"http://www.bogus.com/login?command=lms&w=1&email=bogus&voucher_id=123abc456xyz&firstname=bogus&lastname=bogus&cdrom=false&name=bogusmod7"));

		// // INDIVIDUAL DEVELOPMENT PLAN
		// ds = new DevSection();
		// dev.getDevSection().add(ds);
		// ds.setTitle("Individual Development Plan");
		// ds.setText("Below is a template you can use to list your development priorities and create an action plan.");

		ret.setAppendix(getAppendix());

		return ret;
	}

	private static Appendix getAppendix() {
		Appendix app = new Appendix();
		app.setTitle("Definitions of Categories and Scales");
		// Competencies
		AppSection as = new AppSection();
		app.getAppSection().add(as);
		as.setTitle("Leadership Competencies");
		as.setText("Leadership competencies are the skills a person demonstrates that are causally related to and predictive of successful performance on the job. The definition of each competency is listed below.");
		AppSubSection ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Thought Leadership");
		AppTextGroup atg = new AppTextGroup();
		ass.getAppTextGroup().add(atg);
		atg.setHeading("Reach Strategic Decisions");
		atg.setText("Integrating information, guidelines, and requirements and evaluating alternatives to reach effective decisions that align with the strategies of the organization.");
		atg = new AppTextGroup();
		ass.getAppTextGroup().add(atg);
		atg.setHeading("Display Global Perspective");
		atg.setText("Showing interest in the global scene and taking into account the unique challenges, constraints, and dynamics involved in doing business globally across cultures.");
		atg = new AppTextGroup();
		ass.getAppTextGroup().add(atg);
		atg.setHeading("Foster Innovation");
		atg.setText("Generating and championing new ideas, approaches, and initiatives, and creates an environment that nurtures and supports innovation.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("People Leadership");
		atg = new AppTextGroup();
		ass.getAppTextGroup().add(atg);
		atg.setHeading("Promote Collaboration");
		atg.setText("Fostering a sense of collaboration, and working effectively with others across the organization to achieve goals.");
		atg = new AppTextGroup();
		ass.getAppTextGroup().add(atg);
		atg.setHeading("Engage and Develop Others");
		atg.setText("Conveying and instilling a sense of energy and optimism, helping others envision a greater sense of what is possible, and ensuring that talent receives mentoring, training, feedback, and development opportunities.");
		atg = new AppTextGroup();
		ass.getAppTextGroup().add(atg);
		atg.setHeading("Build Support and Influence");
		atg.setText("Positioning and explaining ideas and proposals in ways that get support from others.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Results Leadership");
		atg = new AppTextGroup();
		ass.getAppTextGroup().add(atg);
		atg.setHeading("Manage Execution");
		atg.setText("Holding others accountable for completing work responsibilities by setting standards, developing realistic plans, delegating assignments, and monitoring progress.");
		atg = new AppTextGroup();
		ass.getAppTextGroup().add(atg);
		atg.setHeading("Show Courageous Determination");
		atg.setText("Demonstrating and fostering a sense of urgency, a \"can-do\" spirit, and commitment to achieving goals and organizational success while address difficult issues and championing others who challenge \"the way it has always been done\".");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Personal Leadership");
		atg = new AppTextGroup();
		ass.getAppTextGroup().add(atg);
		atg.setHeading("Adapt and Learn");
		atg.setText("Responding resourcefully, flexibly, and positively when faced with new challenges and demands and open to constant learning and improvement.");
		atg = new AppTextGroup();
		ass.getAppTextGroup().add(atg);
		atg.setHeading("Establish Credibility");
		atg.setText("Gaining the confidence and trust of others through principled leadership, sound business ethics, authenticity, and consistency between words and actions.");
		// Leadership Experience
		as = new AppSection();
		app.getAppSection().add(as);
		as.setTitle("Leadership Experience");
		as.setText("Feedback about previous leadership experience provides insight into areas where additional experience is likely to accelerate transition to the next level of leadership.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Business Operations");
		ass.setText("Measures your experience with day-to-day business operations such as managing or implementing key projects or managing core operations (e.g., procuring resources or facilities, scheduling production or delivering or servicing products).");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Handling Tough Challenges");
		ass.setText("Measures your experience handling tough business challenges (i.e., those above and beyond the challenges of day-to-day business operations) such as taking over an existing situation with significant problems or particularly tough staffing situations (e.g., instituting across the board salary reductions, shutting down a plant or location or significantly reducing headcount, managing through a downturn, or from adversarial interpersonal conflicts).");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Growing the Business");
		ass.setText("Measures your experience in business development or marketing, managing a start-up or new business, growing a new or existing business, or developing or enhancing products or services.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Personal Development");
		ass.setText("Measures experience gained through learning a new role, enhancing performance in an existing role, dealing with change or experiences that are new and different or even by applying experiences and expertise gained outside of the organization (e.g., community activities).");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("High Visibility");
		ass.setText("Measures your experience performing in the spotlight. Highly visible experiences are those that are critical for business success or have the attention of senior leaders, key customers, or the media. They can include critical or public negotiations or responsibility for situations that promise significant returns, but are very risky in terms of potential failure, cost, or negative impact on the organization.");
		// Leadership Experience
		as = new AppSection();
		app.getAppSection().add(as);
		as.setTitle("Leadership Foundations");
		as.setText("Leadership foundations refer to styles, predispositions, or attributes that make us who we are, and influence the ways we behave and our reactions to new situations or unexpected events.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Problem Solving");
		ass.setText("Measures conceptual reasoning ability � the ability to spot patterns and trends and draw correct conclusions from conflicting, confusing or ambiguous information.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Achievement Drive");
		ass.setText("Measures your energy level and preference for staying busy and active, especially on tasks that are personally challenging or that present demanding or difficult goals.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Intellectual Engagement");
		ass.setText("Measures the breadth and diversity of your problemsolving style, including the extent to which you are likely to be imaginative or expressive, capable of cutting through extraneous or ambiguous information to detect the underlying patterns or themes, to have foresight, and whether you tend take a longer- or shorter-term perspective in your thinking.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Advancement Drive");
		ass.setText("Measures your determination to succeed in your chosen career and your preference for advancement over job security, together with the tendency to take chances based on limited information and to feel comfortable in situations involving uncertainty or risk.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Attention to Detail");
		ass.setText("Measures the extent to which you focus on the details and prefer to be exacting, precise, neat, thorough and complete.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Collective Orientation");
		ass.setText("Measures your preference for working as part of a team rather than autonomously and to feel a strong obligation to adhere to the rules, policies and social mores of the groups to which you belong.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Impact/Influence");
		ass.setText("Measures your preference for being in charge and for attempting to persuade or influence people to your point of view.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Flexibility/Adaptability");
		ass.setText("Measures your willingness to change your approach in order to adjust to constraints, multiple demands, or adversity.  Includes elements of being optimistic about the future and generally satisfied with your position, pay, and opportunities.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Interpersonal Engagement");
		ass.setText("Measures your inclination to be sociable, enjoy the company of others and be attracted to situations providing opportunities for engaging with others.");
		// Leadership Interest
		as = new AppSection();
		app.getAppSection().add(as);
		as.setTitle("Leadership Interest");
		as.setText("Feedback about career goals and interests can help decide if progressing to higher-level leadership roles is the right choice at this point in the person�s career.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Leadership Aspirations");
		ass.setText("The extent to which you aspire to higher-level leadership roles and have a specific plan in mind for reaching those roles.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Learning Orientation");
		ass.setText("The extent to which you are open to and energized by opportunities to learn or acquire new information.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Career Drivers");
		ass.setText("The features or elements of jobs that are most important to you and provide you the greatest work satisfaction.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Experience Orientation");
		ass.setText("The extent to which you are open to and energized by new or different challenges, experiences, or situations (e.g., working in a new functional area or geography).");
		as = new AppSection();
		as.setTitle("Leadership Experience");
		as.setText("Feedback about previous leadership experience provides insight into areas where additional experience is likely to accelerate transition to the next level of leadership.");
		// Leadership Derailers
		as = new AppSection();
		app.getAppSection().add(as);
		as.setTitle("Leadership Derailers");
		as.setText("The Derailment Risk score indicates the chances that specific behaviors will significantly impede effectiveness as a leader.  Derailment include four behavioral patterns known to impact leadership effectiveness. The four are:");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Ego Centered");
		ass.setText("Leaders who are excessively Ego-Centered are overly involved with and concerned about their own well being and importance; tend to have an inflated evaluation of personal skills and abilities; often appear condescending to others; and may convey an attitude of entitlement to position and rewards.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Micro Managing");
		ass.setText("Leaders who excessively Micro-Manage stay involved in too many decisions rather than passing on responsibility; do detailed work rather than delegating it; and stay too involved with direct reports rather than building teamwork among the staff.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Manipulative");
		ass.setText("Leaders who are excessively Manipulative have a tendency to try to cover up mistakes; may act to protect themselves by shifting blame onto others or by carefully sharing information to serve their own purpose to the detriment of others; and may have a willingness to take advantage of others.");
		ass = new AppSubSection();
		as.getAppSubSection().add(ass);
		ass.setTitle("Unwillingness to Confront");
		ass.setText("Leaders who are excessively Unwilling to Confront are prone to communicating or implying cooperation, conveying acceptance by lack of objection, or expressing support for another person�s idea, but behaving in contradictory ways that serve their own self-interest or undermine the efforts of others who are seen as possible threats.");

		return app;
	}

	// Redo for new data layout
	// private static LvaDbReport getRediRptObject()
	// {
	// // create the object structure for the Development report
	// LvaDbReport ret = new LvaDbReport();
	// ret.setReportType("LVA Dashboard");
	// ret.setReportSubtype("Readiness");
	// ret.setReportTitle("Readiness Dashboard Report");
	// ret.setLevelName("Mid-Level Leader");
	// ret.setReportDate("2012-09-27");
	// ret.setAssessmentDate("2012-09-23");
	// ret.setProject(new Project(1234, "Mid-Level Leader VPA"));
	// Participant pp = new Participant();
	// pp.setId(12);
	// pp.setExtId("367990");
	// pp.setFirstName("Pat");
	// pp.setLastName("Sample");
	// pp.setTitle("Current Title");
	// ret.setParticipant(pp);
	// ret.setCompanyName("Global Corporation");
	//
	// // Summary section
	// Summary sum = new Summary();
	// ret.setSummary(sum);
	// sum.setScore(3);
	// sum.getLabel().add("DEVELOP IN PLACE");
	// sum.getLabel().add("READY IN 3-5 YEARS");
	// sum.getLabel().add("READY IN 1-2 YEARS");
	// sum.getLabel().add("READY NOW");
	// sum.setTitle("READINESS RECOMMENDATION");
	// sum.setConfigText("Tentatively, this section will contain configuration text.  This will be updated when the report format is finalized.");
	// sum.setRationaleText("Possible rationale text goes here.  This will be updated when the report format is finalized.");
	// TextSection ts = new TextSection();
	// sum.getTextSection().add(ts);
	// ts.setTitle("STRENGTHS");
	// ts.getPoint().add("Pat Sample displays strong global perspective by demonstrating sensitivity and a global mindset when making decisions that involve cultural issues or geographic differences.");
	// ts.getPoint().add("Pat Sample displays strong adaptability by responding resourcefully to new demands or priorities, seeking opportunities to gather feedback and learn new things, and maintaining composure and patience in stressful situations.");
	// ts.getPoint().add("Pat Sample's business operations experience to date compares favorably with that of successful mid-level leaders (place holder for more descriptive text around business operations)");
	// ts.getPoint().add("Pat Sample's personal development experience to date compares favorably with that of successful mid-level leaders (place holder for more descriptive text around personal development)");
	// ts.getPoint().add("Pat Sample's interest, energy and passion for a more challenging leadership role compare favorably with that of successful mid-level leaders and should contribute to his transition into a mid-level role or position.");
	// ts = new TextSection();
	// sum.getTextSection().add(ts);
	// ts.setTitle("DEVELOPMENT NEEDS");
	// ts.getPoint().add("Pat Sample shows the need to develop at fostering innovation. Pat displays little curiosity or open-mindedness when presented with problems and/or resists others' efforts to challenge current processes, downplaying the efforts of those who try to view issues in new and different ways.");
	// ts.getPoint().add("Pat Sample needs to develop at promoting collaboration. Pat does not involve others in plans and actions or make an effort to understand their views; allows conflicting views to limit others� involvement and minimize collaboration.");
	// ts.getPoint().add("Pat Sample shows the need to develop at engaging and develop others. Pat gives feedback that is inaccurate or insensitive, or shies away from providing feedback. Pat demonstrates a negative attitude or lack of commitment; does not attempt to encourage commitment in others, acknowledge their efforts, or convey confidence in their abilities.");
	// ts.getPoint().add("Pat Sample shows the need to develop at building support and influencing others. Pat does not provide sufficient rationale to support recommendations, and struggles to address others� needs and priorities; is unable to rely on relationships to build support.");
	// ts.getPoint().add("Pat should pay particular attention to the tendency to let personal connections with the team lead to perceptions of favoritism.");
	// ts.getPoint().add("Pat should pay particular attention to the tendency to set unrealistically challenging goals for self or team.");
	// ts.getPoint().add("Pat should pay particular attention to the tendency to let risk tolerance lead to excessively risky decisions.");
	// ts.getPoint().add("Pat should pay particular attention to the tendency to let independent nature isolate them from the team or peers.");
	// ts.getPoint().add("Pat should pay particular attention to the tendency to be overly rigid or structured in their approach.");
	// ts = new TextSection();
	// sum.getTextSection().add(ts);
	// ts.setTitle("LONG-TERM ADVANCEMENT POTENTIAL");
	// ts.getPoint().add("Although Pat may be successful at the next level, he is less likely to make the transition two levels up. Interest is strong but foundations are mixed.");
	// ts = new TextSection();
	// sum.getTextSection().add(ts);
	// ts.setTitle("LEADERSHIP DERAILERS");
	// ts.getPoint().add("Pat's results suggest low risk of derailment while transitioning to a mid level leadership role or position.");
	// ts = new TextSection();
	// sum.getTextSection().add(ts);
	// ts.setTitle("DEVELOPMENT FOCUS");
	// ts.getPoint().add("(Need text here) Development for Pat should focus on balancing foundations and increasing competencies/skills. Text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text.");
	// // Here ends the recommendation section
	//
	// // Ratings section
	// Ratings rat = new Ratings();
	// ret.setRatings(rat);
	// rat.setTitle("DETAILED RATINGS");
	// rat.setText("The following shows more detailed assessment ratings for this person. In the graphic ratings below, blue represents a clear strength, green boxes represent  an emerging strength, yellow boxes represent tentative/mixed scores, and orange boxes represent unfavorable scores. The ranges of favorability vary from category to category, and in some cases a person can score too highly on a category. For example, under Leadership Foundations, a person could be be too low on attention to detail (yellow or orange boxes on the left side), but they could also be too highly focused on details which could negatively impact their work (yellow or orange boxes on the right side).");
	// // Leadership Competencies
	// ScoredSection ss = new ScoredSection();
	// rat.getScoredSection().add(ss);
	// ss.setTitle("Leadership Competencies");
	// ss.setScore(new Score(3, "Mixed (dLCI = 32)"));
	// ss.setText("Mr. Sample demonstrated a mixed level of performance on competencies and skills. Enter additional canned text here� text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text  text text text text text text text text text text text text text text text text text text text text text text text text.");
	// Superfactor sf = new Superfactor();
	// ss.getSuperfactor().add(sf);
	// sf.setName("Thought Leadership");
	// sf.getCompetency().add(new Competency(3.0f,
	// "Reach Strategic Decisions"));
	// sf.getCompetency().add(new Competency(3.5f,
	// "Display Global Perspective"));
	// sf.getCompetency().add(new Competency(2.5f, "Foster Innovation"));
	// sf = new Superfactor();
	// ss.getSuperfactor().add(sf);
	// sf.setName("Results Leadership");
	// sf.getCompetency().add(new Competency(3.0f, "Manage Execution"));
	// sf.getCompetency().add(new Competency(3.0f,
	// "Show Courageous Determination"));
	// sf = new Superfactor();
	// ss.getSuperfactor().add(sf);
	// sf.setName("People Leadership");
	// sf.getCompetency().add(new Competency(2.5f, "Promote Collaboration"));
	// sf.getCompetency().add(new Competency(2.5f,
	// "Engage and Develop Others"));
	// sf.getCompetency().add(new Competency(2.5f,
	// "Build Support and Influence"));
	// sf = new Superfactor();
	// ss.getSuperfactor().add(sf);
	// sf.setName("Personal Leadership");
	// sf.getCompetency().add(new Competency(4.0f, "Adapt and Learn"));
	// sf.getCompetency().add(new Competency(3.0f, "Establish Credibility"));
	// // Leadership Experience
	// ss = new ScoredSection();
	// rat.getScoredSection().add(ss);
	// ss.setTitle("Leadership Experience");
	// ss.setScore(new Score(3, "Strong"));
	// ss.setText("Mr. Sample's leadership experience to date compares favorably with that of successful mid-level leaders and positions him well for an uneventful transition into a mid-level leadership role.");
	// ss.getCharacteristic().add(new Characteristic(5, "Business Operations"));
	// ss.getCharacteristic().add(new Characteristic(4,
	// "Handling Tough Challenges"));
	// ss.getCharacteristic().add(new Characteristic(4, "High Visibility"));
	// ss.getCharacteristic().add(new Characteristic(4,
	// "Growing the Business"));
	// ss.getCharacteristic().add(new Characteristic(5,
	// "Personal Development"));
	// // Leadership Foundations
	// ss = new ScoredSection();
	// rat.getScoredSection().add(ss);
	// ss.setTitle("Leadership Foundations");
	// ss.setScore(new Score(3, "Mixed"));
	// ss.setText("Mr. Sample�s leadership styles and predispositions lack balance, meaning he tends to overuse some styles and/or underuse others.  He should pay particular attention to letting personal connections with his team lead to perceptions of favoritism, setting unrealistically challenging goals, and letting his risk tolerance lead to excessively risky decisions.");
	// ss.getCharacteristic().add(new Characteristic(2, "Problem Solving"));
	// ss.getCharacteristic().add(new Characteristic(5,
	// "Intellectual Engagement"));
	// ss.getCharacteristic().add(new Characteristic(8, "Attention to Detail"));
	// ss.getCharacteristic().add(new Characteristic(5, "Impact/Influence"));
	// ss.getCharacteristic().add(new Characteristic(9,
	// "Interpersonal Engagement"));
	// ss.getCharacteristic().add(new Characteristic(5, "Achievement Drive"));
	// ss.getCharacteristic().add(new Characteristic(5, "Advancement Drive"));
	// ss.getCharacteristic().add(new Characteristic(8,
	// "Collective Orientation"));
	// ss.getCharacteristic().add(new Characteristic(5,
	// "Flexibility/Adaptability"));
	// // Leadership Interest
	// ss = new ScoredSection();
	// rat.getScoredSection().add(ss);
	// ss.setTitle("Leadership Interest");
	// ss.setScore(new Score(2, "Strong"));
	// ss.setText("Mr. Sample�s interest, energy, and passion for a more challenging leadership role compare favorably with that of successful mid-level leaders and should contribution to his transition into a MLL position.");
	// ss.getCharacteristic().add(new Characteristic(7,
	// "Leadership Aspirations"));
	// ss.getCharacteristic().add(new Characteristic(8, "Career Drivers"));
	// ss.getCharacteristic().add(new Characteristic(3,
	// "Learning Orientation"));
	// ss.getCharacteristic().add(new Characteristic(4,
	// "Experience Orientation"));
	// // Leadership Derailers
	// ss = new ScoredSection();
	// rat.getScoredSection().add(ss);
	// ss.setTitle("Leadership Derailers");
	// ss.setScore(new Score(3, "Low"));
	// ss.setText("The Derailment Risk score indicates the chances that specific behaviors will significantly impede effectiveness as a leader. Derailment Risk is determined from a combination of four behavioral patterns known to impact leadership effectiveness including being excessively self-focused, neing manipulative, micro-managing, and being unwilling to confront.");
	// ss.getCharacteristic().add(new Characteristic(6, "Ego Centered"));
	// ss.getCharacteristic().add(new Characteristic(6, "Manipulative"));
	// ss.getCharacteristic().add(new Characteristic(4, "Micro Managing"));
	// ss.getCharacteristic().add(new Characteristic(4,
	// "Unwillingness to Confront"));
	// // End of rating section
	//
	// // Development section
	// Development dev = new Development();
	// ret.setDevelopment(dev);
	// dev.setTitle("DEVELOPMENT RECOMMENDATIONS");
	// // Leveraging Strengths
	// DevSection ds = new DevSection();
	// dev.getDevSection().add(ds);
	// ds.setTitle("LEVERAGING STRENGTHS");
	// ds.setText("Add some text here about leveraging strengths. ..by category maybe.. particularly strong in experience so leverage what you have learned from that experince (holistic catagory comments).");
	// // Addressing Development Needs
	// ds = new DevSection();
	// dev.getDevSection().add(ds);
	// ds.setTitle("ADDRESSING DEVELOPMENT NEEDS");
	// DevElement de = new DevElement();
	// ds.getDevElement().add(de);
	// de.setTitle("LEADERSHIP FOUNDATIONS");
	// de.setText("Leadership foundations refer to your leadership styles and predispositions. These are the attributes that make us who we are and influence the ways we behave and our reactions to new situations or unexpected events. When it comes to leadership styles, balance and moderation are key. A repertoire of styles creates versatility by allowing you to draw upon the style or styles that are most effective for a given situation. Your results indicate a lack of balance in your leadership styles meaning that you are likely to overuse some styles and under-use others and suggesting that leadership foundations is a priority development area for you. Listed below are the leadership foundations where additional development will help to balance your leadership styles and make the transition to senior level leader smoother and more effective.");
	// DevPoint dp = new DevPoint();
	// de.getDevPoint().add(dp);
	// dp.setTitle("INTERPERSONAL ENGAGEMENT");
	// dp.setText("Your interest in the personal well being of those around you may cause you weigh personal situations too heavily when applying policy resulting in the perception that you play favorites.  Don't let your skill and enjoyment of relationships with your team lead to personal policy exception or perceptions of favoritism. Here is a link to content you may find helpful for your development in this area:");
	// dp.getLink().add(new
	// Hyperlink("(insert hyperlink)#1","http://www.bogus.com/login?command=lms&amp;w=1&amp;email=bogus&amp;voucher_id=123abc456xyz&amp;firstname=bogus&amp;lastname=bogus&amp;cdrom=false&amp;name=bogusmod1"));
	// dp = new DevPoint();
	// de.getDevPoint().add(dp);
	// dp.setTitle("ACHIEVEMENT DRIVE");
	// dp.setText("Your higher energy level and preference for very challenging goals may lead you to set overly challenging goals and can be perceived by your peers or direct reports as if you are competing with them. Don't let your enjoyment of personal achievement lead you to compete with your team or peers. Here is a link to content you may find helpful for your development in this area:");
	// dp.getLink().add(new
	// Hyperlink("(insert hyperlink)#2","http://www.bogus.com/login?command=lms&amp;w=1&amp;email=bogus&amp;voucher_id=123abc456xyz&amp;firstname=bogus&amp;lastname=bogus&amp;cdrom=false&amp;name=bogusmod2"));
	// dp = new DevPoint();
	// de.getDevPoint().add(dp);
	// dp.setTitle("ADVANCEMENT DRIVE");
	// dp.setText("Your TalentView results suggest you are more comfortable with risk than is typical for leaders at your level. Don't let your comfort with risk and preference for advancement over security lead you to take unnecessary risks and to make decisions without having fully collected or consider all relevant data and points of view.  Here is a link to content you may find helpful for your development in this area:");
	// dp.getLink().add(new
	// Hyperlink("Assessing Risk","http://www.bogus.com/login?command=lms&amp;w=1&amp;email=bogus&amp;voucher_id=123abc456xyz&amp;firstname=bogus&amp;lastname=bogus&amp;cdrom=false&amp;name=bogusmod3"));
	// dp = new DevPoint();
	// de.getDevPoint().add(dp);
	// dp.setTitle("COLLECTIVE ORIENTATION");
	// dp.setText("Your TalentView results suggest you are more comfortable working on your own than is typical for leaders at your level. This may isolate or even alienate you from your peers and direct reports.  Don't let you independent nature lead you to isolate yourself from your team or reject efforts at consensus out of hand. Here is a link to content you may find helpful for your development in this area:");
	// dp.getLink().add(new
	// Hyperlink("(insert hyperlink)#4","http://www.bogus.com/login?command=lms&amp;w=1&amp;email=bogus&amp;voucher_id=123abc456xyz&amp;firstname=bogus&amp;lastname=bogus&amp;cdrom=false&amp;name=bogusmod4"));
	// dp = new DevPoint();
	// de.getDevPoint().add(dp);
	// dp.setTitle("FLEXIBILITY/ADAPTABILITY");
	// dp.setText("text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text");
	// dp.getLink().add(new
	// Hyperlink("(insert hyperlink)#5","http://www.bogus.com/login?command=lms&amp;w=1&amp;email=bogus&amp;voucher_id=123abc456xyz&amp;firstname=bogus&amp;lastname=bogus&amp;cdrom=false&amp;name=bogusmod5"));
	// de = new DevElement();
	// ds.getDevElement().add(de);
	// de.setTitle("LEADERSHIP COMPETENCIES");
	// de.setText("Competencies text text text text text text  text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text. Listed below are the competencies and skills where additional development will help to prepare you for the transition to mid level leader smoother and more effective.");
	// dp = new DevPoint();
	// de.getDevPoint().add(dp);
	// dp.setTitle("FOSTERING INNOVATION");
	// dp.setText("Creative people engage in undisciplined, open-minded, and uncritical thinking when they face problems. This mind-set allows them to generate more options. To increase your use of creative approaches, try the following suggestions: Realize that how you view a situation often corresponds to how you approach it. Develop a more positive mind-set; instead of thinking of a situation as a problem to solve, view it as a challenge to meet and exceed. Assess what types of situations intrigue you, and make you curious and enthusiastic. Then identify what makes other situations unappealing. What are the differences?  Here is a link to content you may find helpful for your development in this area:");
	// dp.getLink().add(new
	// Hyperlink("How Do I Innnovate?","http://www.bogus.com/login?command=lms&amp;w=1&amp;email=bogus&amp;voucher_id=123abc456xyz&amp;firstname=bogus&amp;lastname=bogus&amp;cdrom=false&amp;name=iaprbn02fxx4"));
	// dp = new DevPoint();
	// de.getDevPoint().add(dp);
	// dp.setTitle("PROMOTING COLLABORATION");
	// dp.setText("Teamwork takes time, effort, and consistency. This can be difficult to achieve across groups, especially if there are personality conflicts, resource constraints, rivalries, or other factors that create competitiveness instead of collaboration. Consider the following suggestions: Build teamwork among different groups through formal committees. For example, management committees (which may include nonmanagerial employees) can be used to build teamwork and a sense of common purpose across all work groups represented. They can also engage teams in a collaborative process. When a cross-functional team comes to a decision point, avoid voting for decisions. Instead, strive for consensus. Voting is not likely to produce a true team decision. Evaluate people on their willingness and ability to work as part of a team in the organization. Encourage them to develop relationships throughout the organization, not just within their own functional area. Here is a link to content you may find helpful for your development in this area:");
	// dp.getLink().add(new
	// Hyperlink("Involve Others","http://www.bogus.com/login?command=lms&amp;w=1&amp;email=bogus&amp;voucher_id=123abc456xyz&amp;firstname=bogus&amp;lastname=bogus&amp;cdrom=false&amp;name=bogusmod7"));
	// dp = new DevPoint();
	// de.getDevPoint().add(dp);
	// dp.setTitle("ENGAGING AND DEVELOPING OTHERS");
	// dp.setText("When individuals have an opportunity to meet both professional and personal needs within the same environment, they often achieve optimum performance. As a leader, you can help create this environment by offering information and encouragement to each person on your team. Consider the following suggestions: Develop strong, trusting relationships with your direct reports. Learn about each person�s personal and professional goals, why they come to work, why they chose to be a member of this team, and what they hope to personally accomplish if the team achieves its goals and vision. Make explicit connections between the team�s goal and vision, and each person�s needs, motives, and personal and professional goals. Watch for opportunities to point out how the team�s success will help each team member to succeed. Here is a link to content you may find helpful for your development in this area:");
	// dp.getLink().add(new
	// Hyperlink("Qualities of a Superior Mentor","http://www.bogus.com/login?command=lms&amp;w=1&amp;email=bogus&amp;voucher_id=123abc456xyz&amp;firstname=bogus&amp;lastname=bogus&amp;cdrom=false&amp;name=bogusmod8"));
	// dp.getLink().add(new Hyperlink("The Culture of Urgency",
	// "http://www.bogus.com/login?command=lms&amp;w=1&amp;email=bogus&amp;voucher_id=123abc456xyz&amp;firstname=bogus&amp;lastname=bogus&amp;cdrom=false&amp;name=bogusmod9"));
	// dp = new DevPoint();
	// de.getDevPoint().add(dp);
	// dp.setTitle("BUILDING SUPPORT AND INFLUENCING OTHERS");
	// dp.setText("If you want to engage, motivate, inspire, captivate, energize, or convince people, you need to present your ideas with confidence and enthusiasm. Your ideas deserve the best delivery you can give them. Consider the following suggestions: Increase your confidence by becoming well versed in your topic. Be ready to talk about it broadly and in detail, and to answer a wide range of questions. Clarify your purpose for sharing your ideas. People need an overall sense of what you're talking about and why they should listen to you. If you don't have a clear purpose, people may understand your points, but they won't understand why you're making those points. Show your confidence by talking about how people will benefit from your idea. Talk about how it connects to other initiatives, both present and future. When appropriate, tie your idea or solution to an already acknowledged problem. Here is a link to content you may find helpful for your development in this area:");
	// dp.getLink().add(new
	// Hyperlink("Increase My Sphere of Influence","http://www.bogus.com/login?command=lms&amp;w=1&amp;email=bogus&amp;voucher_id=123abc456xyz&amp;firstname=bogus&amp;lastname=bogus&amp;cdrom=false&amp;name=bogusmod10"));
	//
	// // INDIVIDUAL DEVELOPMENT PLAN
	// ds = new DevSection();
	// dev.getDevSection().add(ds);
	// ds.setTitle("INDIVIDUAL DEVELOPMENT PLAN");
	// ds.setText("Text holder: either say something to the effect of \"here's the plan that was completed during your coaching session\" or \"here is a template you can use with your manager to plan out and work on your development\" depending on if they had a lead consultant there to start filling it out with them");
	//
	// return ret;
	// }
}
