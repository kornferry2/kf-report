/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.shell;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.pdinh.core.vo.Score;
import com.pdinh.pipeline.dto.ObjectDTO;
import com.pdinh.pipeline.dto.StatusDTO;
import com.pdinh.pipeline.generator.helpers.PipelineTransformHelper;
import com.pdinh.pipeline.generator.helpers.ReportManifestHelper;
import com.pdinh.pipeline.generator.vo.ReportContentModelData;
import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.pipeline.generator.vo.Stage;
import com.pdinh.pipeline.generator.vo.Var;
import com.pdinh.pipeline.manifest.vo.ReportManifest;
import com.pdinh.project.shell.utils.ShellUtils;
import com.pdinh.project.shell.vo.Behavior;
import com.pdinh.project.shell.vo.IbReport;
import com.pdinh.project.shell.vo.Level;
import com.pdinh.project.shell.vo.Module;
import com.pdinh.project.shell.vo.Section;
import com.pdinh.project.shell.vo.Suggestion;
import com.sun.xml.bind.v2.runtime.IllegalAnnotationException;

/*
 * This class contains the scoring method for the Shell scorecard
 */

@Stateless
@LocalBean
public class ShellScoreBean {
	//
	// Beans and resources
	//

	@Resource(mappedName = "custom/generator_properties")
	private Properties generatorProperties;

	@Resource(mappedName = "custom/shell_properties")
	private Properties shellProperties;

	//
	// Static data.
	//
	public static int XML_OUT = 1; // Score results are Shell LAR RCM XML format
	public static int OBJ_OUT = 11; // Score results to IbReport object

	// private statics

	// Would rather not have to do this... perhaps some RCM manifest or
	// something that will give positive input on counts
	private static int MAX_BEH_SEQ = 8;
	private static int MAX_DEV_SUG = 1;
	private static int MAX_IA_MOD = 3;

	private static String NEED;
	private static String COMP;
	private static String HIGH;

	private static String BEH_SCORE_STAGE_ID = "behaviors";
	private static String COMP_SCORE_STAGE_ID = "comps";

	private static String HI_COMP_LEVEL_NAME = "effective";
	private static String MID_COMP_LEVEL_NAME = "competent";
	private static String LO_COMP_LEVEL_NAME = "development";

	// Generic date format - used to set up date in XML; final format generated
	// later in the process
	// Used only for the scoring date
	private static String DATE_FORMAT = "yyyy-MM-dd";

	public ShellScoreBean() {
	}

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	//
	// Instance methods.
	//

	/*
	 * Convenience method - Takes an RGR object and converts it
	 * to XML, then scores the scorecard responses and returns
	 * a Shell LAR RCM object (IbReport)
	 */
	public ObjectDTO score(ReportGenerationRequest rgr) {
		JAXBContext jc = null;
		Marshaller marshaller = null;
		StringWriter sw = null;
		try {
			jc = JAXBContext.newInstance(ReportGenerationRequest.class);
			marshaller = jc.createMarshaller();
			// marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			// // Makes it pretty
			sw = new StringWriter();
			marshaller.marshal(rgr, sw);
			// System.out.println("Marshalling complete... Output XML:\n" +
			// sw.toString());

			// Now go score it
			return score(sw.toString(), OBJ_OUT);
		} catch (JAXBException e) {
			String str = "ShellScoreBean- score(<rgr>):  JAXBException detected marshalling RGR... msg="
					+ e.getMessage();
			System.out.println(str);
			e.printStackTrace();
			return new ObjectDTO().failed(str);
		} finally {
			if (sw != null) {
				try {
					sw.close();
				} catch (Exception e) {
					System.out.println("Exception on StringWriter close in ShellScoreBean.score().  Ignored.  Msg="
							+ e.getMessage());
				}
				sw = null;
			}
			marshaller = null;
			jc = null;
			rgr = null;
		}
	}

	/*
	 * Convenience method - Score the scorecard responses and return
	 * an Shell LAR RCM object (IbReport)
	 */
	public ObjectDTO score(String responseXml) {
		return score(responseXml, OBJ_OUT);
	}

	/*
	 * Score the scorecard responses and build an Shell LAR RCM XML
	 */
	public ObjectDTO score(String responseXml, int outputType) {
		System.out.println("ENTER score:  type=" + outputType);

		if (!(outputType == XML_OUT || outputType == OBJ_OUT)) {
			String str = "ShellScoreBean - score(<xml>,<type>):  Invalid output type (" + outputType + ") selected.";
			System.out.println(str);
			return new ObjectDTO().failed(str);
		}

		// Get the resource bundle
		ResourceBundle rb = ResourceBundle.getBundle("com.pdinh.project.shell.application", Locale.getDefault());
		if (rb == null) {
			String str = "ShellScoreBean - score(<xml>,<type>):  ResourceBundle not available.";
			System.out.println(str);
			return new ObjectDTO().failed(str);
		}

		// Initialize some stuff from the bundle
		NEED = rb.getString("NEED");
		COMP = rb.getString("COMP");
		HIGH = rb.getString("HIGH");

		// NOTE: This validation may not (probably is not) needed as we build
		// the XML from JAXB classes that were
		// used to generate the schema for validation. It can be a real pain in
		// a swiftly changing environment.
		// System.out.println("begin validation");
		// validate the incoming RGR xml
		StatusDTO sd = ShellUtils.validateXml(generatorProperties.getProperty("calculationEngineResources"),
				ShellUtils.RQR_TYPE, responseXml);
		if (!sd.isSuccess()) {
			String str = "ShellScoreBean - score(<xml>,<type>):  Validation failed.  Msg=" + sd.getErrorMsg();
			System.out.println(str);
			return new ObjectDTO().failed(str);
		}

		// get the manifest for Shell LAR first pass scoring
		// System.out.println("Scorecard scoring (for LAR)");
		ReportManifestHelper rmh = new ReportManifestHelper(new Boolean(rb.getString("debugOn")),
				generatorProperties.getProperty("calculationEngineResources"));
		ReportManifest rm = rmh.getReportManifest("ssc");
		if (rm == null) {
			String str = "ShellScoreBean - score(<xml>,<type>):  Unable to fetch calculation pipeline manifest.";
			System.out.println(str);
			return new ObjectDTO().failed(str);
		}

		// run the pipeline - 1 stage (response to scale scores)
		// System.out.println("pipeline - 1 stage");
		PipelineTransformHelper pth = new PipelineTransformHelper(new Boolean(rb.getString("debugOn")),
				generatorProperties.getProperty("calculationEngineResources"));
		String scoredXml = pth.process(rm, responseXml);
		if (scoredXml == null) {
			String str = "ShellScoreBean - score(<xml>,<type>):  Calculation pipeline did not return scored data.";
			System.out.println(str);
			return new ObjectDTO().failed(str);
		}

		// Start to build the preliminary
		// First unmarshal the RCM output (which is an updated RGR XML)
		// System.out.println("unmarshal the RCM output");
		ObjectDTO dto = getScoredObject(scoredXml);
		if (!dto.isSuccess()) {
			String str = "ShellScoreBean - score(<xml>,<type>):  Unable to get the scored RGR.  Msg="
					+ dto.getErrorMsg();
			System.out.println(str);
			return dto;
		}
		// System.out.println("dto.getObject()");
		ReportGenerationRequest rgr = (ReportGenerationRequest) dto.getObject();
		// Scoring is done... RGR is updated with the scored output

		// Build the RCM
		// System.out.println("Build the RCM");
		ObjectDTO rcmDto = buildRcm(rb, rgr);
		if (!rcmDto.isSuccess()) {
			String str = "ShellScoreBean - score(<xml>,<type>):  Unable to build RCM.  Msg=" + rcmDto.getErrorMsg();
			System.out.println(str);
			return rcmDto;
		}

		ObjectDTO ret = null;
		if (outputType == ShellScoreBean.OBJ_OUT) {
			// Put out to RCM (IbReport)
			ret = rcmDto;
		} else if (outputType == ShellScoreBean.XML_OUT) {
			// Put out the XML
			ret = rcm2xml((IbReport) rcmDto.getObject());
			if (!ret.isSuccess()) {
				String str = "ShellScoreBean - score(<xml>,<type>):  Unable to convert RCM to XML string.  Msg="
						+ ret.getErrorMsg();
				System.out.println(str);
			}
		} else {
			// Redundant check; should not be needed
			ret = new ObjectDTO().failed("ShellScoreBean - score:  Invalid output type (" + outputType
					+ "); no data returned.");
		}

		// Return this output no matter what
		return ret;
	}

	/*
	 * getScoredObject - converts a ReportGeneration Request XML to Java objects
	 */
	private ObjectDTO getScoredObject(String xml) {
		InputStream is = null;
		JAXBContext jc = null;
		Unmarshaller um = null;
		ReportGenerationRequest rgr = null;

		try {
			is = new ByteArrayInputStream(xml.getBytes("UTF-8"));

			// Create the context & the unmarshaller
			jc = JAXBContext.newInstance(ReportGenerationRequest.class);
			um = jc.createUnmarshaller();
			rgr = (ReportGenerationRequest) um.unmarshal(is);

			return new ObjectDTO(rgr);
		} catch (UnsupportedEncodingException e) {
			return new ObjectDTO().failed("Invalid encoding for unmarshalling scored XML,  msg=" + e.getMessage());
		} catch (JAXBException e) {
			return new ObjectDTO().failed("Error unmarshalling scored XML.  msg=" + e.getMessage());
		} finally {
			um = null;
			jc = null;
			try {
				is.close();
			} catch (Exception e) { /* Swallow it */
			}
		}
	}

	/*
	 * getScores - get the scores from the RGR object
	 * 
	 * The object returned is a HashMap of scores
	 */
	private ObjectDTO getScoreValues(ReportGenerationRequest rgr) {
		// HashMap<String, Float> scores = new HashMap<String, Float>();
		HashMap<String, Integer> scores = new HashMap<String, Integer>();

		com.pdinh.pipeline.generator.vo.Participant ppt = rgr.getParticipant().get(0); // Assume
																						// a
																						// single
																						// participant
																						// for
																						// now
		if (ppt == null) {
			return new ObjectDTO().failed("No participant found in scored XML.");
		}

		// get the "behavior" stage
		// Stage stg = null;
		// for (Stage st : ppt.getStage())
		// {
		// if(st.getId().equals(BEH_SCORE_STAGE_ID) ||
		// st.getId().equals(COMP_SCORE_STAGE_ID))
		// {
		// stg = st;
		// break;
		// }
		// }
		// if (stg == null)
		// {
		// return new
		// ObjectDTO().failed("Unable to find participant scores (no \"behaviors\" tag).");
		// }
		// for (Var v : stg.getVar())
		// {
		// Integer val = (v.getValue().matches("[-+]?\\d+(\\.\\d+)?")) ?
		// Integer.parseInt(v.getValue()) : new Integer(0);
		// scores.put(v.getId(), val);
		// }

		// Stage stg = null;
		for (Stage st : ppt.getStage()) {
			if (st.getId().equals(BEH_SCORE_STAGE_ID) || st.getId().equals(COMP_SCORE_STAGE_ID)) {
				for (Var v : st.getVar()) {
					Integer val = (v.getValue().matches("[-+]?\\d+(\\.\\d+)?")) ? Integer.parseInt(v.getValue())
							: new Integer(0);
					scores.put(v.getId(), val);
				}
			}
		}
		if (scores.size() < 1) {
			return new ObjectDTO().failed("Unable to find participant scores (no \"behaviors\" or \"comps\" tags).");
		}

		return new ObjectDTO(scores);
	}

	/*
	 * buildRcm - create the output RCM for the Shell LAR
	 * 
	 * NOTE:	The code assumes that all of the relevant scores are present and
	 * 			scores that are not present aren't used for the report.
	 */
	private ObjectDTO buildRcm(ResourceBundle rb, ReportGenerationRequest rgr) {
		// Get the scores. Note that this includes both Section and behavior
		// scores.
		ObjectDTO dto = getScoreValues(rgr);
		if (!dto.isSuccess()) {
			return dto;
		}
		@SuppressWarnings("unchecked")
		HashMap<String, Integer> allScores = (HashMap<String, Integer>) dto.getObject();

		// Get the RGR participant... Assumes a single participant
		com.pdinh.pipeline.generator.vo.Participant rgrPpt = rgr.getParticipant().get(0);

		IbReport rcm = new IbReport();

		// Header stuff
		rcm.setReportType(rb.getString("LAR_RPT_TYPE"));
		rcm.setReportSubtype(rb.getString("LAR_RPT_SUBTYPE"));
		rcm.setReportTitle(rb.getString("LAR_TITLE"));
		if (rgrPpt.getReportContentModel() == null)
			rgrPpt.setReportContentModel(new ReportContentModelData());
		rcm.setLevel(rgrPpt.getReportContentModel().getLevel());
		rcm.setAssessmentDate(rgrPpt.getAssessmentDate());

		// Assume scoring date set here
		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
		Date today = new Date();
		rcm.setScoringDate(format.format(today));

		rcm.setCompany(rgrPpt.getCompany());
		com.pdinh.core.vo.Participant rcmPpt = new com.pdinh.core.vo.Participant();
		rcmPpt.setId(rgrPpt.getId());
		rcmPpt.setExtId(rgrPpt.getExtId());
		rcmPpt.setFirstName(rgrPpt.getFirstName());
		rcmPpt.setLastName(rgrPpt.getLastName());
		rcmPpt.setTitle(rgrPpt.getTitle());
		rcm.setParticipant(rcmPpt);
		rcm.setElUrl(rb.getString("EL_URL"));

		// Do the sections. Order is hard-coded in below
		// Authenticity - Section 0
		String secAbbrev = "AUTH";
		Section sec = new Section();
		rcm.getSection().add(sec);
		sec.setName(rb.getString("LAR_AUTHENTICITY").toLowerCase());
		sec.setLabel(rb.getString("LAR_AUTHENTICITY"));
		sec.setScore(getSectionScore(rb, allScores.get("SSC_AUTH")));
		HashMap<String, List<String>> behLists = getBehLists(rb, allScores, secAbbrev);
		// Set up the behavior list (the <level> tags in the <behavior> tag)
		addBehLevels(rb, behLists, secAbbrev, sec);
		// Development object/tag
		addDev(rb, allScores, secAbbrev, sec);

		// Growth - Section 1
		secAbbrev = "GROW";
		sec = new Section();
		rcm.getSection().add(sec);
		sec.setName(rb.getString("LAR_GROWTH").toLowerCase());
		sec.setLabel(rb.getString("LAR_GROWTH"));
		sec.setScore(getSectionScore(rb, allScores.get("SSC_GROW")));
		behLists = getBehLists(rb, allScores, secAbbrev);
		// Set up the behavior list (the <level> tags in the <behavior> tag)
		addBehLevels(rb, behLists, secAbbrev, sec);
		// Development object/tag
		addDev(rb, allScores, secAbbrev, sec);

		// Collaboration - Section 2
		secAbbrev = "COLL";
		sec = new Section();
		rcm.getSection().add(sec);
		sec.setName(rb.getString("LAR_COLLABORATION").toLowerCase());
		sec.setLabel(rb.getString("LAR_COLLABORATION"));
		sec.setScore(getSectionScore(rb, allScores.get("SSC_COLL")));
		behLists = getBehLists(rb, allScores, secAbbrev);
		// Set up the behavior list (the <level> tags in the <behavior> tag)
		addBehLevels(rb, behLists, secAbbrev, sec);
		// Development object/tag
		addDev(rb, allScores, secAbbrev, sec);

		// Performance - Section 3
		secAbbrev = "PERF";
		sec = new Section();
		rcm.getSection().add(sec);
		sec.setName(rb.getString("LAR_PERFORMANCE").toLowerCase());
		sec.setLabel(rb.getString("LAR_PERFORMANCE"));
		sec.setScore(getSectionScore(rb, allScores.get("SSC_PERF")));
		behLists = getBehLists(rb, allScores, secAbbrev);
		// Set up the behavior list (the <level> tags in the <behavior> tag)
		addBehLevels(rb, behLists, secAbbrev, sec);
		// Development object/tag
		addDev(rb, allScores, secAbbrev, sec);

		// Done... Successful... Return the RCM
		return new ObjectDTO(rcm);
	}

	/*
	 * setupRcm - convert an IbReport RCM (Shell LAR) to XML
	 * 
	 * NOTE:	The code assumes that all of the relevant scores are present and
	 * 			scores that are not present aren't used for the report.
	 */
	private ObjectDTO rcm2xml(IbReport rcm) {
		// marshal the RCM
		JAXBContext jc = null;
		Marshaller marshaller = null;
		StringWriter sw = null;
		try {
			jc = JAXBContext.newInstance(IbReport.class);
			marshaller = jc.createMarshaller();
			// marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			// // Make pretty output
			sw = new StringWriter();
			marshaller.marshal(rcm, sw);

			// Done... successful
			return new ObjectDTO(sw.toString());
		} catch (IllegalAnnotationException e) {
			return new ObjectDTO("ShellScoreBean - rcm2xml:  Illegal annotation:  " + e.toString());
		} catch (JAXBException e) {
			return new ObjectDTO()
					.failed("ShellScoreBean - rcm2xml:  JAXBException detected marshalling the RCM.  Msg="
							+ e.getMessage());
		}
	}

	/*
	 * decodeBehaviorScore - Determine display score string from numeric score
	 * 
	 * The only operative score is NEED (for turning on development content)
	 */
	private String decodeBehaviorScore(Integer score) {
		String scr = null;
		switch (score) {
		case 2:
			scr = HIGH;
			break;
		case 1:
			scr = COMP;
			break;
		case 0:
			scr = NEED;
			break;
		}

		return scr;
	}

	/*
	 * getSectionScore - Determine score value and display string for a section from numeric score
	 * 
	 * Behaviors aren't used much
	 */
	private Score getSectionScore(ResourceBundle rb, Integer score) {
		String str = "";

		switch (score) {
		case 5:
			str = rb.getString("LAR_EXCELLENT");
			break;
		case 4:
			str = rb.getString("LAR_STRONG");
			break;
		case 3:
			str = rb.getString("LAR_SOLID");
			break;
		case 2:
			str = rb.getString("LAR_INTERMEDIATE");
			break;
		case 1:
			str = rb.getString("LAR_NOVICE");
			break;
		}

		return new Score(score, str);
	}

	/*
	 * getBehLists - Get the three-column list of behaviors.
	 * 
	 * Returns three lists of behaviors - one for each column.  Column
	 * placement (where the behavior ends up) depends upon the behavior score.
	 * 
	 * Loops through the max number of behaviors.  Assumes that if no score is returned
	 * from the score loop then the behavior doesn't exist and it is skipped.
	 */
	private HashMap<String, List<String>> getBehLists(ResourceBundle rb, HashMap<String, Integer> scores, String sect) {
		// Create the output map...
		HashMap<String, List<String>> ret = new HashMap<String, List<String>>();

		// ...and initialize it.
		ret.put(HIGH, new ArrayList<String>());
		ret.put(COMP, new ArrayList<String>());
		ret.put(NEED, new ArrayList<String>());

		// Loop through the max number of behaviors
		// Assumes that the behavior scores are all of the form "XXXX_N" where
		// XXXX
		// is the section abbreviation and N is a sequential number (gaps
		// allowed)
		for (int i = 1; i <= MAX_BEH_SEQ; i++) {
			// Get the score
			String key = sect + "_" + i;
			Integer scor = scores.get(key);
			if (scor == null) {
				// If not present, skip it
				continue;
			}

			// Got a score... rate it
			String str = decodeBehaviorScore(scor);
			// Get the related string
			String key2 = key + "_" + str;
			// ...and stuff it into the proper list
			String txt = rb.getString(key2);
			ret.get(str).add(txt);
		}

		// Return
		return ret;
	}

	/*
	 * addBehLevels - Add the three levels of behavior text to the RCM
	 */
	private void addBehLevels(ResourceBundle rb, HashMap<String, List<String>> behLists, String secKey, Section sec) {
		// High level competency level statements
		Level lev = new Level(HI_COMP_LEVEL_NAME);
		sec.getLevel().add(lev);
		for (String s : behLists.get(HIGH)) {
			lev.getPoint().add(s);
		}

		// Mid-level competency level statements
		lev = new Level(MID_COMP_LEVEL_NAME);
		sec.getLevel().add(lev);
		for (String s : behLists.get(COMP)) {
			lev.getPoint().add(s);
		}

		// Low level competency level statements
		lev = new Level(LO_COMP_LEVEL_NAME);
		sec.getLevel().add(lev);
		for (String s : behLists.get(NEED)) {
			lev.getPoint().add(s);
		}

		// See if positive feedback is needed (we are at the lowest comp level)
		if (lev.getPoint().size() < 1) {
			// Add the positive feedback
			String str = rb.getString("GENERIC_POS_FEEDBACK") + "  " + rb.getString(secKey + "_POS_FEEDBACK");
			sec.setPositiveFeedback(str);
		}

		return;
	}

	/*
	 * addDev - Add the development for a section to the RCM object
	 */
	private void addDev(ResourceBundle rb, HashMap<String, Integer> scores, String secKey, Section sec) {
		// Container for unique ids used to check for duplicate IA modules in
		// the current section
		Set<String> iaMods = new HashSet<String>();

		// Loop through all possible behaviors in this section
		// It is the same loop as in getBehLists but it has a different payload
		for (int i = 1; i <= MAX_BEH_SEQ; i++) {
			// Get the score
			String key = secKey + "_" + i;
			Integer is = scores.get(key);
			if (is == null) {
				// If not present, skip it
				continue;
			}

			String prefix = key.substring(0, 1) + i;

			// Decode the behavior score
			String str = decodeBehaviorScore(is);
			if (!str.equals(NEED)) {
				// If the score is not POOR/NEED skip it
				continue;
			}

			// Create the behavior
			Behavior beh = new Behavior(key);
			sec.getBehavior().add(beh);
			// put out dev sugs
			for (int j = 1; j <= MAX_DEV_SUG; j++) {
				// This will work only if there are always exactly MAX_DEV_SUG
				// Dev Sug items
				// If that changes, re-factor as the modules do it.
				String dsKey = key + "_DS" + j;
				String ds = rb.getString(dsKey);
				if (!(ds == null)) {
					String sKey = prefix + "S" + j;
					beh.getSuggestion().add(new Suggestion(sKey, ds));
				}
			}

			// put out modules as needed
			for (int k = 1; k <= MAX_IA_MOD; k++) {
				String title = null;

				try {
					title = rb.getString(key + "_IA_TITLE" + k);
				} catch (MissingResourceException e) {
					// It's done (assumes no holes in the sequence)
					break;
				}

				// Figure that it is legit if it has a title
				String iaidKey = key + "_IA_NAME" + k;

				// Get and check the IA module unique identifier
				String modId = rb.getString(iaidKey);
				if (iaMods.contains(modId)) {
					// IA mod already in this section... skip it
					continue;
				}

				// IA mod not in this section. Save it in the checking Set and
				// go on
				iaMods.add(modId);

				// Build the IA URL... start by getting the template
				String urlStr = shellProperties.getProperty("shellIaUrlTemplate");
				urlStr = urlStr.replace("{orionHost}", shellProperties.getProperty("shellOrionHost"));
				urlStr = urlStr.replace("{iaEmailAddr}", shellProperties.getProperty("shellIaEmailAddr"));
				urlStr = urlStr.replace("{voucherId}", shellProperties.getProperty("shellVoucherId"));
				urlStr = urlStr.replace("{iaFirstName}", shellProperties.getProperty("shellIaFirstName"));
				urlStr = urlStr.replace("{iaLastName}", shellProperties.getProperty("shellIaLastName"));
				urlStr = urlStr.replace("{iaIntName}", modId);

				Module mod = new Module(title, urlStr);

				mod.setKey(prefix + "M" + k);
				beh.getModule().add(mod);
			} // Enf of IA for loop
		} // End of behavior loop (MAX_BEH_SEQ)

		return;
	} // End addDev
}
