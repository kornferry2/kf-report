package com.pdinh.project.kfap.language;

import java.util.Map;
import javax.ejb.Local;

@Local
public interface LangReplacement {
	public String replaceContentVar(String content, Map<String, String> varTextWrapper);
}
