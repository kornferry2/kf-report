package com.pdinh.project.kfap.language;

public class LangFactory {
	public static LangReplacement getLanguage(String lang) {
		// Many of these files have duplicated functionality. We create separate
		// class files for them (per Kehai) so that we may add specific
		// functionality in the future if needed.
		//
		// At this point in time, we COULD get by with two separate files; one
		// for English (both US and GB, handling the 'a/an' issue) and one for
		// all other languages.

		if (lang.equals("en-US")) {
			return new ReplaceVarForEngish();
		} else if (lang.equals("ko")) {
			return new ReplaceVarForKorean();
		} else if (lang.equals("zh-CN")) {
			return new ReplaceVarForChinese();
		} else if (lang.equals("ja")) {
			return new ReplaceVarForJapanese();
		} else if (lang.equals("fr")) {
			return new ReplaceVarForFrench();
		} else if (lang.equals("es")) {
			return new ReplaceVarForSpanish();
		} else if (lang.equals("de")) {
			return new ReplaceVarForGerman();
		} else if (lang.equals("ru")) {
			return new ReplaceVarForRussian();
		} else if (lang.equals("pt-BR")) {
			return new ReplaceVarForPortugueseBR();
		} else if (lang.equals("en-GB")) {
			return new ReplaceVarForEnglishGB();
		} else {
			// default to US English
			return new ReplaceVarForEngish();
		}
	}
}
