package com.pdinh.project.viaedge;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.data.ms.dao.UserDao;
import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.User;
import com.pdinh.pipeline.generator.vo.ReportGenerationRequest;
import com.pdinh.pipeline.generator.vo.Stage;
import com.pdinh.pipeline.generator.vo.Var;
import com.pdinh.project.viaedge.reports.jaxb.IntroductionSection;
import com.pdinh.project.viaedge.reports.jaxb.Participant;
import com.pdinh.project.viaedge.reports.jaxb.ResultsSummarySection;
import com.pdinh.project.viaedge.reports.jaxb.SubSection;
import com.pdinh.project.viaedge.reports.jaxb.ViaEdgeIndividualSummaryReport;
import com.pdinh.reportcontent.Manifest;
import com.pdinh.reportcontent.ReportContent;

@Stateless
@LocalBean
public class IndividualReportRgrToRcm {
	private static final Logger log = LoggerFactory.getLogger(IndividualReportRgrToRcm.class);
	private ReportContent staticContent = null;
	private ReportContent sharedContent = null;
	private ReportContent agilityContent = null;
	// private static String SHORT_DATE_FORMAT = "dd-MMM-yyyy";
	// private final String LONG_DATE_FORMAT = "dd MMM yyyy";
	private String LONG_DATE_FORMAT = "MM/dd/yyyy";
	private ReportGenerationRequest rgr;
	private final ResultsSummarySection rss = new ResultsSummarySection();
	private ViaEdgeIndividualSummaryReport sumre = new ViaEdgeIndividualSummaryReport();
	@EJB
	private UserDao userDao;
	private final String gridMapId = "agility";
	private String lang = "en-US";

	public ReportGenerationRequest getRgr() {
		return rgr;
	}

	public void setRgr(ReportGenerationRequest rgrp) {
		rgr.equals(rgrp);
	}

	@Resource(name = "application/report/config_properties", type = Properties.class)
	private Properties configProperties;

	public IndividualReportRgrToRcm() {

	}

	private final List<String> langCodes = Arrays.asList("en-US", "ja", "zh", "it", "fr", "de", "pt", "es", "ru", "nl",
			"pl", "tr", "ko");

	public String generateReportRcm(ReportGenerationRequest reportGenReq, String language) throws PalmsException {
		log.debug("starting generateReportRcm viaEdge iSummaryReport RGR to RCM - language= {}", language);
		lang = langCodes.get(0);
		if (language != null) {
			if (langCodes.contains(language)) {
				lang = language;
			}
			if (language.equals("fr_CA")) {
				lang = langCodes.get(4);
			} else if (language.equals("pt_BR")) {
				lang = langCodes.get(6);
			} else if (language.equals("zh_CN")) {
				lang = langCodes.get(2);
			} else if (language.equals("en")) {
				lang = langCodes.get(0);
			}
		}
		this.rgr = reportGenReq;
		log.debug("using {}", lang);

		User participant = userDao.findById(Integer.parseInt(rgr.getParticipant().get(0).getExtId()), true);
		if (participant == null) {
			throw new PalmsException("User Not Found.", PalmsErrorCode.RCM_GEN_FAILED.getCode());
		}
		Participant part = new Participant();
		part.setFirstName(participant.getFirstname());
		part.setLastName(participant.getLastname());

		if (lang.equals("en-US"))
			LONG_DATE_FORMAT = "MM/dd/yyyy";
		if (lang.equals("ja"))
			LONG_DATE_FORMAT = "yyyy/MM/dd";
		if (lang.equals("zh"))
			LONG_DATE_FORMAT = "yyyy/MM/dd";
		if (lang.equals("it"))
			LONG_DATE_FORMAT = "dd/MM/yyyy";
		if (lang.equals("fr"))
			LONG_DATE_FORMAT = "dd/MM/yyyy";
		if (lang.equals("de"))
			LONG_DATE_FORMAT = "dd.MM.yyyy";
		if (lang.equals("pt"))
			LONG_DATE_FORMAT = "dd/MM/yyyy";
		if (lang.equals("es"))
			LONG_DATE_FORMAT = "dd/MM/yyyy";
		if (lang.equals("ru"))
			LONG_DATE_FORMAT = "dd.MM.yyyy";
		if (lang.equals("ko"))
			LONG_DATE_FORMAT = "yyyy/MM/dd";

		// SimpleDateFormat shortSdf = new SimpleDateFormat(SHORT_DATE_FORMAT);
		SimpleDateFormat longSdf = new SimpleDateFormat(LONG_DATE_FORMAT);

		String reportContentRepoBaseDir = configProperties.getProperty("reportContentRepository");
		// set manifest
		Manifest manifest = new Manifest(reportContentRepoBaseDir
				+ "/manifest/projects/internal/viaEdge/summary/manifest.xml");
		// set agility model content from manifest (shared content)
		agilityContent = new ReportContent(reportContentRepoBaseDir + "/content/" + manifest.getModelContentRef()
				+ ".xml");
		// set static content from manifest
		staticContent = new ReportContent(reportContentRepoBaseDir + "/content/"
				+ manifest.getReportContents().get("staticContent") + ".xml");
		sharedContent = new ReportContent(reportContentRepoBaseDir + "/content/"
				+ manifest.getReportContents().get("sharedContent") + ".xml");

		// new IndividualSummaryReport
		sumre = new ViaEdgeIndividualSummaryReport();
		sumre.setLang(lang);
		sumre.setCompanyName(rgr.getParticipant().get(0).getCompany());
		sumre.setCopyright(sharedContent.getKeyMapValue4Lang("shared", "copyright", lang));
		sumre.setReportTitle(staticContent.getKeyMapValue4Lang("summary", "reportName", lang));
		sumre.setParticipant(part);

		sumre.setReportDate(longSdf.format(new Date()));
		sumre.setSerialNumber(rgr.getParticipant().get(0).getExtId());
		String gridMapId = "agility";
		IntroductionSection intro = new IntroductionSection();
		intro.setChangeDescription(agilityContent.getGridMapValue4Lang(gridMapId, "change", "description", lang));
		intro.setChangeDescriptionHeading(agilityContent.getGridMapValue4Lang(gridMapId, "change", "headingA", lang));
		intro.setMentalDescription(agilityContent.getGridMapValue4Lang(gridMapId, "mental", "description", lang));
		intro.setMentalDescriptionHeading(agilityContent.getGridMapValue4Lang(gridMapId, "mental", "headingA", lang));
		intro.setPeopleDescription(agilityContent.getGridMapValue4Lang(gridMapId, "people", "description", lang));
		intro.setPeopleDescriptionHeading(agilityContent.getGridMapValue4Lang(gridMapId, "people", "headingA", lang));
		intro.setResultsDescription(agilityContent.getGridMapValue4Lang(gridMapId, "results", "description", lang));
		intro.setResultsDescriptionHeading(agilityContent.getGridMapValue4Lang(gridMapId, "results", "headingA", lang));
		intro.setSelfAwareDescription(agilityContent.getGridMapValue4Lang(gridMapId, "selfAware", "description", lang));
		intro.setSelfAwareDescriptionHeading(agilityContent.getGridMapValue4Lang(gridMapId, "selfAware", "headingA",
				lang));
		intro.setP1(sharedContent.getKeyMapValue4Lang("shared", "p1", lang));
		intro.setP2(sharedContent.getKeyMapValue4Lang("shared", "p2", lang));
		intro.setP3(staticContent.getKeyMapValue4Lang("summary", "p3", lang));
		intro.setTitle(sharedContent.getKeyMapValue4Lang("shared", "introductionTitle", lang));
		sumre.setIntroductionSection(intro);

		// Do loop to setup sub sections based on score
		com.pdinh.pipeline.generator.vo.Participant extP = rgr.getParticipant().get(0);
		List<Stage> stages = extP.getStage();
		rss.getSubSection().clear();
		rss.setTitle(staticContent.getKeyMapValue4Lang("summary", "resultsTitle", lang));
		for (Stage st : stages) {
			// only interested in this one stage. (calculated scores)
			if (st.getId().equals("adj-rank")) {

				List<Var> vars = st.getVar();
				// each VAR contains the score and texts needed
				for (Var v : vars) {
					// x is the score
					double x = Double.parseDouble((v.getValue()));
					log.debug("var : " + v.getId());
					// for each VAR, we call setSectionText() to set RCM values
					if (v.getId().equals("pAdjMLG_rank")) {
						log.debug("call setSection 1");
						setSectionTexts("mental", x);
					} else if (v.getId().equals("pAdjPLG_rank")) {
						log.debug("call setSection 2");
						setSectionTexts("people", x);
					} else if (v.getId().equals("pAdjCLG_rank")) {
						log.debug("call setSection 3");
						setSectionTexts("change", x);
					} else if (v.getId().equals("pAdjRLG_rank")) {
						setSectionTexts("results", x);
						log.debug("call setSection 4");
					} else if (v.getId().equals("pAdjSLG_rank")) {
						setSectionTexts("selfAware", x);
						log.debug("call setSection 5");
					}
					;

					log.debug("rss sections = {}", rss.getSubSection().size());
				}
			}
		}

		sumre.setResultsSummarySection(rss);
		String rcm = "";
		rcm = marshalOutput(sumre);
		log.trace("RCM RCM RCM!!! : {}", rcm);
		if (rcm == null) {
			throw new PalmsException("RCM was null.", PalmsErrorCode.RCM_GEN_FAILED.getCode());
		}
		return rcm; // RCM xml string

	}

	private void setSectionTexts(String row, double score) {
		SubSection s = new SubSection();
		log.debug("NEW SECTION: " + row + " " + score);
		s.setScore(score);
		s.setId(row);
		if (score == -1) {
			s.setScoreNarrative(agilityContent.getGridMapValue4Lang(gridMapId, row, "lowNarrative", lang));
		} else if (score == 0) {
			s.setScoreNarrative(agilityContent.getGridMapValue4Lang(gridMapId, row, "middleNarrative", lang));
		} else if (score == 1) {
			s.setScoreNarrative(agilityContent.getGridMapValue4Lang(gridMapId, row, "highNarrative", lang));
		}
		s.setHeading(agilityContent.getGridMapValue4Lang(gridMapId, row, "headingA", lang));
		s.setTraitLow(agilityContent.getGridMapValue4Lang(gridMapId, row, "traitLow", lang));
		s.setTraitHigh(agilityContent.getGridMapValue4Lang(gridMapId, row, "traitHigh", lang));
		rss.getSubSection().add(s);

	}

	private String marshalOutput(ViaEdgeIndividualSummaryReport iReport) {
		JAXBContext jc = null;
		Marshaller m = null;
		StringWriter sw = new StringWriter();
		try {
			jc = JAXBContext.newInstance(ViaEdgeIndividualSummaryReport.class);
			m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(iReport, sw);

			return sw.toString();
		} catch (JAXBException jbe) {
			log.error("VIA EDGE IndividualReportRgrToRcm.marshalOutput() - JAXBException: Msg={}", jbe.getMessage());
			jbe.printStackTrace();
			return null;
		}
	}

	public ReportContent getSharedContent() {
		return sharedContent;
	}

	public void setSharedContent(ReportContent sharedContent) {
		this.sharedContent = sharedContent;
	}
}
