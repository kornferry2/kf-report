/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;


/*
* Superfactor object used for the scored section of LvaDbReport only.
*/
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder={"name", "competency"})
public class Superfactor
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute
	private String type;
	private String name;
	@XmlElementWrapper(name = "competencies")
	private List<Competency> competency = null;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Superfactor()
	{
		// Default constructor
	}
	
	//
	// Instance methods.
	//
	
	public String toString()
	{
		String str = "Superfactor:  name=" + this.name + ", type=" + this.type;
		
		if (this.competency == null)
		{
			str += "\n<No competencies>";
		}
		else
		{
			str += "\n";
			for (Competency cmp : this.competency)
			{
				str += "            " + cmp.toString() + "\n";
			}
		}
		
		return str;
	}

	// Getters & setters

	//----------------------------------
	public String getType()
	{
		return this.type;
	}
	
	public void setType(String value)
	{
		this.type = value;
	}

	//----------------------------------
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String value)
	{
		this.name = value;
	}

	//----------------------------------
	// Note that the list has a "get" only
	public List<Competency> getCompetency()
	{
		if (this.competency == null)
			this.competency = new ArrayList<Competency>();
		
		return this.competency;
	}
}
