/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.shell.vo;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/*
 *  Used in IbReport
 *  
 *  NOTE:	The "key" attribute is created in scoring and is used to find back the dev sug
 *  		and the module entries.  It is not present and cannot be relied upon if the
 *  		Suggestion object is built in any other fashion or if this is part of an
 *  		IbReport object unmarshalled from XML (it does not exist in the xml).
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Suggestion
{
	//
	// Static data.
	//
 
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlValue
	private String text;
	@XmlAttribute
	private String key;
	
	//
	// Constructors.
	//

	// 0-parameter constructor
	public Suggestion()
	{
		// Default constructor
	}

	// 1-parameter constructor (clone)
	public Suggestion(Suggestion orig)
	{
		this.text = orig.getText() == null ? null : new String(orig.getText());
		this.key = orig.getKey() == null ? null : new String(orig.getKey());
	}

	// 1-parameter constructor
	public Suggestion(String text)
	{
		// Default constructor
		this.text = text;
	}

	// 2-parameter constructor
	public Suggestion(String key, String text)
	{
		// Default constructor
		this.key = key;
		this.text = text;
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		//String str = "Dev Sug:  ";
		//str += "sel=" + this.selected + ",  text=" + this.text;
		String str = "Dev Sug:  key=" + this.key + ", text=" + this.text;
		
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getText()
	{
		return this.text;
	}

	public void setText(String value)
	{
		this.text = value;
	}

	//----------------------------------
	public String getKey()
	{
		return this.key;
	}

	public void setKey(String value)
	{
		this.key = value;
	}
}
