/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/*
 * This is the bottom level JAXB class for the boilerplate data that is present in the
 * Appendix to the dashboard reports.  It is used in the AppSection JAXB class.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"heading", "text"})
public class AppTextGroup
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String heading;
	private String text;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public AppTextGroup()
	{
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "        AppTextGroup:  ";
		str += "heading=" + this.heading + "  text=" + this.text;
		
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getHeading()
	{
		return this.heading;
	}

	public void setHeading(String value)
	{
		this.heading = value;
	}

	//----------------------------------
	public String getText()
	{
		return this.text;
	}

	public void setText(String value)
	{
		this.text = value;
	}
}
