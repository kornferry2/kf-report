/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.project.lva.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/*
 * This is the Summary object used for lvaDbReport only.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"title", "configText", "rationaleText", "label", "textSection"})
public class Summary
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
//	@XmlAttribute
//	private int score;
	private String title;
	private String configText;
	private String rationaleText;
	@XmlElementWrapper(name = "scoreLabels")
	private List<String> label;
	@XmlElementWrapper(name = "textSections")
	private List<TextSection> textSection;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Summary()
	{
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "Summary:  ";
		//str += "score=" + this.score + ", title=" + this.title + "\n";
		str += "title=" + this.title + "\n";
		if (this.label != null && this.label.size() > 0)
		{
			for (String ls : this.label)
			{
				str += "      " + ls + "\n";
			}
		}
		str += "  configText=" + this.configText + "\n";
		str += "  rationaleText=" + this.rationaleText + "\n";
		for (TextSection ts : this.textSection)
		{
			str += "      " + ts.toString() + "\n";
		}

		
		return str;
	}
	
	// Getters & setters
//
//	//----------------------------------
//	public int getScore()
//	{
//		return this.score;
//	}
//
//	public void setScore(int value)
//	{
//		this.score = value;
//	}

	//----------------------------------
	public String getTitle()
	{
		return this.title;
	}

	public void setTitle(String value)
	{
		this.title = value;
	}

	//----------------------------------
	public String getConfigText()
	{
		return this.configText;
	}

	public void setConfigText(String value)
	{
		this.configText = value;
	}

	//----------------------------------
	public String getRationaleText()
	{
		return this.rationaleText;
	}

	public void setRationaleText(String value)
	{
		this.rationaleText = value;
	}

	//----------------------------------
	// Note that the list has a "get" only
	public List<String> getLabel()
	{
		if (this.label == null)
			this.label = new ArrayList<String>();
		
		return this.label;
	}

	//----------------------------------
	// Note that the list has a "get" only
	public List<TextSection> getTextSection()
	{
		if (this.textSection == null)
			this.textSection = new ArrayList<TextSection>();
		
		return this.textSection;
	}
}
