package com.pdinh.report.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class StringNumberUtil {
	
	public static String numberToWord(int number) {
		
		String ones[]={"zero"," one"," two"," three"," four"," five"," six"," seven"," eight"," Nine"," ten"," eleven",
					  " twelve"," thirteen"," fourteen","fifteen"," sixteen"," seventeen"," eighteen"," nineteen"};
			 
		String tens[]={" "," "," twenty"," thirty"," forty"," fifty"," sixty","seventy"," eighty"," ninety"};
			 			 
		if( number < 20)  return ones[number];
		if( number < 100) return tens[number/10] + ((number % 10 > 0)? " " + numberToWord(number % 10):"");
		if( number < 1000) return ones[number/100] + " Hundred" + ((number % 100 > 0)?" and " + numberToWord(number % 100):"");
		if( number < 1000000) return numberToWord(number / 1000) + " Thousand " + ((number % 1000 > 0)? " " + numberToWord(number % 1000):"") ;
		return numberToWord(number / 1000000) + " Million " + ((number % 1000000 > 0)? " " + numberToWord(number % 1000000):"") ;	 	
	}
	
	public static String listToFormattedString(List<String> list, String lastSeparator){
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for(int index=0; index < list.size(); index++) {
        	if (first)
        		first = false;
        	else
        		if(index == list.size()-1){
        			sb.append(lastSeparator);
        		}else{
        			sb.append(", ");
        		}
           	sb.append(list.get(index));
        }
		return sb.toString();
	}
	//course version map is multi-delimited string of course abbreviations and versions
	//ex.  kfp:1|rv:5|gtd:1000 ex 2: kfp:1
	
	public static int getCourseVersionFromMap(String mapString, String courseAbbv){
		List<String> courseVersions = new ArrayList<String>();
		if (mapString.contains("|")){
			courseVersions = Arrays.asList(mapString.split("[|]"));
		}else{
			courseVersions.add(mapString);
		}
		
		int version = 1;
		for (String courseVersion : courseVersions){
			if (courseVersion.split("[:]")[0].equals(courseAbbv)){
				version = Integer.valueOf(courseVersion.split("[:]")[1]);
				break;
			}
					
		}
		return version;
	}
}
