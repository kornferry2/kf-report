package com.pdinh.report.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Random;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;

public class FreeMarkerUtil {

	private static ClassTemplateLoader classTemplateLoader = null;

	public static final String TEMPLATES_ROOT = "/com/pdinh/template/freemarker";

	private static String path = "";

	public static ClassTemplateLoader getClassTemplateLoader() {
		if (classTemplateLoader == null) {
			classTemplateLoader = new ClassTemplateLoader(FreeMarkerUtil.class, TEMPLATES_ROOT);
		}
		return classTemplateLoader;
	}

	public static String processTemplate(String pathToTemplate, Map<String, String> dataModel) {
		Configuration cfg = new Configuration();
		cfg.setObjectWrapper(new DefaultObjectWrapper());
		Template template = null;
		cfg.setTemplateLoader(getClassTemplateLoader());
		String processed = "";
		try {
			template = cfg.getTemplate(pathToTemplate);
			StringWriter out = new StringWriter();

			// PROCESS TEMPLATE + MODEL
			template.process(dataModel, out);
			// CONVERT OUTPUT to StringBuffer
			StringBuffer sb = out.getBuffer();

			processed = sb.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return processed;
	}

	public static String tempHtmlFile(String fileName, String htmlCode) {
		try {
			Random r = new Random();
			File htmlFile = new File("c:/Windows/Temp/" + fileName + 100000 + (int) (r.nextFloat() * 900000) + ".html");
			OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(htmlFile), "UTF-8");
			writer.write(htmlCode);
			// FileWriter fileWriter = new FileWriter(htmlFile);
			// fileWriter.write(htmlCode);
			path = htmlFile.getPath();
			// fileWriter.close();
			writer.close();
			return path;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return path;
		}
	}
}
