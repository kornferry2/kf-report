/**
 * Copyright (c) 2013 Personnel Decisions International, Inc. a Korn/Ferry company
 * All rights reserved.
 */
package com.pdinh.report.helper;

import java.io.PrintWriter;
import java.io.StringReader;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.reports.ReportProcessor;
import com.pdinh.reportserver.data.dao.jpa.ReportContentModelDao;
import com.pdinh.reportserver.persistence.jpa.ReportContentModel;

/**
 * A class to contain common code for the GenerateReport and
 * GenerateReportFromRcm classes
 */
@Stateless
@LocalBean
public class GenerateReportHelperBean {
	final static Logger log = LoggerFactory.getLogger(GenerateReportHelperBean.class);

	public static final int GEN_PDF_OUTPUT = 1;
	public static final int GEN_HTML_OUTPUT = 2;

	// Report transform paths... used locally. The string may be used other
	// projects as well... Maven folder structure
	private static final String COACHING_SUMM_RPT_XFORM = "projects/coaching/summary_report.xsl";
	private static final String COACHING_PLAN_RPT_XFORM = "projects/coaching/plan_report.xsl";
	private static final String VEDGE_IND_SUMM_RPT_XFORM = "projects/viaedge/individual_summary_report.xsl";
	private static final String VEDGE_IND_COACH_RPT_XFORM = "projects/viaedge/individual_coaching_report.xsl";
	private static final String VEDGE_IND_FDBK_RPT_XFORM = "projects/viaedge/individual_feedback_report.xsl";
	private static final String VEDGE_GRP_FDBK_RPT_XFORM = "projects/viaedge/group_feedback_report.xsl";
	private static final String LVA_MLL_RPT_XFORM = "projects/lva/mll/lva_report.xsl";
	private static final String KFP_IND_RPT_XFORM = "projects/kfap/individual_feedback_report.xsl";
	private static final String KFP_IND_NAR_RPT_XFORM = "projects/kfap/individual_feedback_report.xsl";
	private static final String ALP_IND_V2_RPT_XFORM = "projects/kfap/individual_feedback_reportV2.xsl";
	private static final String ALP_TALENT_GRID_V2_RPT_XFORM = "projects/kfap/talent_gridV2.xsl";

	// private static final String KFP_GRP_RPT_XFORM =
	// "projects/kfap/group_report.xsl";
	private static final String KFP_SLATE_RPT_XFORM = "projects/kfap/group_report.xsl";

	// Old school... resources not moved for Shell... (not set up for maven)
	private static final String SHELL_RPT_XFORM = "com/pdinh/resource/projects/shell/ib_report.xsl";

	@EJB
	ReportContentModelDao reportDao;

	@EJB
	private ReportProcessor reportProcessor;

	// /**
	// * Common code for generating PDF Reports
	// *
	// * @param reportType
	// * @param rcmXml
	// * @param pptId
	// * @param hybFlag
	// * @param mockFile
	// * @param customParams
	// * @param response
	// * @throws ServletException
	// */
	// public void genReportPdfxx(String reportType, String rcmXml, String
	// pptId, boolean hybFlag, String mockFile,
	// Map<String, String> customParams, HttpServletResponse response) throws
	// ServletException {
	//
	// // Transform a hybrid report to a feedback report.
	// if (hybFlag) {
	// reportType = ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK;
	// }
	//
	// String reportXslt = this.getXslt(reportType);
	//
	// try {
	// DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	// docFactory.setIgnoringElementContentWhitespace(true);
	// DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	// Document doc = null;
	//
	// String fn = null;
	// if (mockFile != null && !mockFile.isEmpty()) {
	// // MOCK: load RCM from xml file - Chris's test code
	// System.out.println("mockFile = " + mockFile);
	// String mockRcmFolder =
	// "C:/Users/chris/workspace-indigo/ReportServer-web/test/";
	// System.out.println("mockRcmFolder = " + mockRcmFolder);
	// String rcmPath = mockRcmFolder + mockFile;
	//
	// try {
	// doc = docBuilder.parse(rcmPath);
	// } catch (SAXException e) {
	// e.printStackTrace();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// fn = "test-" + mockFile + "-" + reportType;
	// } else {
	// // This in't a test... check to see if the rcm is there
	// if (rcmXml == null) {
	// System.out.println("rcm is null");
	// response.setContentType("text/html");
	// PrintWriter pw = response.getWriter();
	// pw.print("<html><body><h1 style=\"color:red;\">No reports generated.</h1>rcm paramemter is null."
	// + ", report type " + reportType + "</body></html>");
	// return;
	// }
	//
	// // Got one to process
	// System.out.println("rcm found, set content type to pdf");
	// // create a filename from params
	// if (pptId == null || pptId.length() < 1) {
	// fn = "";
	// } else {
	// fn = pptId + "-";
	// }
	// fn += reportType;
	//
	// // convert to xml doc
	// doc = docBuilder.parse(new InputSource(new StringReader(rcmXml)));
	// }
	//
	// // set the output info
	// String disposition = String.format("attachment; filename=report-%s.pdf",
	// fn);
	// response.setContentType("application/pdf");
	// response.addHeader("content-disposition", disposition);
	//
	// // call pdf generation
	// System.out.println("calling ProcessReportToPdf");
	// reportProcessor.ProcessReportToPdf(doc, response.getOutputStream(),
	// reportXslt,
	// ReportType.getSubTypeFromType(reportType, hybFlag), customParams);
	//
	// } catch (Exception excp) {
	// excp.printStackTrace();
	// throw new ServletException(excp);
	// }
	//
	// return;
	// }
	// -------------------------------------------------------------------------
	// public void genReportHtmlxx(String reportType, String rcmXml, String
	// pptId, boolean hybFlag, String mockFile,
	// Map<String, String> customParams, HttpServletResponse response) throws
	// ServletException {
	//
	// // Transform a hybrid report to a feedback report.
	// if (hybFlag) {
	// reportType = ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK;
	// }
	//
	// String reportXslt = this.getXslt(reportType);
	//
	// try {
	// DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	// docFactory.setIgnoringElementContentWhitespace(true);
	// DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	// Document doc = null;
	//
	// if (mockFile != null && !mockFile.isEmpty()) {
	// // MOCK: load RCM from xml file - Chris's test code
	// System.out.println("mockFile = " + mockFile);
	// String mockRcmFolder =
	// "C:/Users/chris/workspace-indigo/ReportServer-web/test/";
	// System.out.println("mockRcmFolder = " + mockRcmFolder);
	// String rcmPath = mockRcmFolder + mockFile;
	//
	// try {
	// doc = docBuilder.parse(rcmPath);
	// } catch (SAXException e) {
	// e.printStackTrace();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// } else {
	// // This in't a test... check to see if the rcm is there
	// if (rcmXml == null) {
	// System.out.println("rcm is null");
	// response.setContentType("text/html");
	// PrintWriter pw = response.getWriter();
	// pw.print("<html><body><h1 style=\"color:red;\">No reports generated.</h1>rcm paramemter is null."
	// + ", report type " + reportType + "</body></html>");
	//
	// return;
	// }
	//
	// // Got one to process
	// // convert to xml doc
	// doc = docBuilder.parse(new InputSource(new StringReader(rcmXml)));
	// }
	//
	// // set the output info
	// System.out.println("rcm found, set content type to html");
	// response.setContentType("text/html");
	// response.setCharacterEncoding("UTF-8");
	//
	// // call html generation
	// System.out.println("calling ProcessReportToHtml");
	// reportProcessor.ProcessReportToHtml(doc, response.getOutputStream(),
	// reportXslt,
	// ReportType.getSubTypeFromType(reportType), "pdf", customParams);
	// } catch (Exception excp) {
	// excp.printStackTrace();
	// throw new ServletException(excp);
	// }
	//
	// return;
	// }

	/**
	 * Common code for generating Reports
	 * 
	 * @param reportType
	 * @param rcmXml
	 * @param pptId
	 * @param hybFlag
	 * @param mockFile
	 * @param customParams
	 * @param response
	 * @throws ServletException
	 */
	public void genReport(int outType, String reportType, String rcmXml, String pptId, boolean hybFlag,
			String mockFile, Map<String, String> customParams, HttpServletResponse response) throws ServletException,
			PalmsException {

		// Transform a hybrid report to a feedback report.
		if (hybFlag) {
			reportType = ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK;
		}

		String reportXslt = this.getXslt(reportType);

		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			docFactory.setIgnoringElementContentWhitespace(true);
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = null;

			String fn = null; // Only used for PDFs
			if (mockFile != null && !mockFile.isEmpty()) {
				// MOCK: load RCM from xml file - Chris's test code
				System.out.println("mockFile = " + mockFile);
				String mockRcmFolder = "C:/workspace-kepler/report-parent/report-web/src/test/";
				System.out.println("mockRcmFolder = " + mockRcmFolder);
				String rcmPath = mockRcmFolder + mockFile;

				try {
					doc = docBuilder.parse(rcmPath);
				} catch (Exception e) {
					throw new PalmsException("Error parsing mock file name in GenerateReportHelperBean.genReport()",
							e.getMessage());
				}
				// fn = "test-" + mockFile + "-" + reportType;
				fn = "reportfromProcessor";

				Element root = doc.getDocumentElement();
				XPath xpath = XPathFactory.newInstance().newXPath();

				String disposition = String.format("attachment; filename=%s.pdf", fn);
				response.setContentType("application/pdf");
				response.addHeader("content-disposition", disposition);

				// call pdf generation
				System.out.println("calling ProcessReportToPdf");
				reportProcessor.ProcessReportToPdf(doc, response.getOutputStream(), reportXslt,
						ReportType.getSubTypeFromType(reportType, hybFlag), customParams);

			} else {
				// This in't a test... check to see if the rcm is there
				if (rcmXml == null) {
					System.out.println("rcm is null");

					response.setContentType("text/html");
					PrintWriter pw = response.getWriter();
					pw.print("<html><body><h1 style=\"color:red;\">No reports generated.</h1>rcm paramemter is null."
							+ "  report type " + reportType + "</body></html>");
					response.setHeader("Error",
							"<html><body><h1 style=\"color:red;\">No reports generated.</h1>rcm paramemter is null."
									+ "  report type " + reportType + "</body></html>");
					return;
				}
				log.debug("rcmXml is: " + rcmXml);
				// Got one to process
				// convert to xml doc
				doc = docBuilder.parse(new InputSource(new StringReader(rcmXml)));
				Element root = doc.getDocumentElement();
				XPath xpath = XPathFactory.newInstance().newXPath();
				String reportDate = "";
				// reportDate = (String) xpath.evaluate("reportDate", root,
				// XPathConstants.STRING);
				SimpleDateFormat localDateFormat = new SimpleDateFormat("yyMMdd_HHmmss");
				reportDate = localDateFormat.format(new Date());
				String pptFName = (String) xpath.evaluate("participant/firstName", root, XPathConstants.STRING);
				String pptLName = (String) xpath.evaluate("participant/lastName", root, XPathConstants.STRING);

				log.debug("pptName before the check is: " + pptFName);
				log.debug("pptLName before the check is: " + pptLName);

				// String utf8fname = new String(pptFName.getBytes("UTF8"),
				// "UTF8");
				// log.debug("Converted first name: {}", utf8fname);

				if (reportType.equals((ReportType.KFP_INDIVIDUAL)) || (reportType.equals(ReportType.ALP_IND_V2))) {
					pptFName = (String) xpath.evaluate("participantName/firstName", root, XPathConstants.STRING);
					pptLName = (String) xpath.evaluate("participantName/lastName", root, XPathConstants.STRING);
				}

				log.debug("pptName after the check is: " + pptFName);
				log.debug("pptLName after the check is: " + pptLName);

				log.debug("Initial fn value is: " + fn);

				if (outType == GEN_PDF_OUTPUT) {
					System.out.println("rcm found, set content type to pdf");
					// create a filename from params
					if ((pptId == null || pptId.length() < 1)
							&& (pptFName == null || pptLName == null || pptFName.length() < 1 || pptLName.length() < 1)) {
						fn = "";
					} else if (pptFName != null && pptLName != null
							&& !(pptFName.length() < 1 || pptLName.length() < 1)) {
						fn = pptLName + "_" + pptFName;

					} else {
						fn = pptId + "_";
					}

					log.debug("fn after modification is: " + fn);
					log.debug("fn after modification is: " + fn);
					// REPORT NAME
					// String reportName = "";
					// reportName = (String) xpath.evaluate("reportTitle", root,
					// XPathConstants.STRING);
					// REPORT NAME
					String groupName = "";
					String langCode = "";
					langCode = (String) xpath.evaluate("./@lang", root, XPathConstants.STRING);

					if (reportType.equals(ReportType.VIAEDGE_GROUP_FEEDBACK)) {
						groupName = (String) xpath.evaluate("participant/firstName", root, XPathConstants.STRING);
					} else if (reportType.equals(ReportType.KFP_SLATE)
							|| (reportType.equals(ReportType.ALP_TALENT_GRID_V2))) {
						groupName = (String) xpath.evaluate("group", root, XPathConstants.STRING);

					}
					log.debug("groupName is: " + groupName);
					if ((reportType.equals(ReportType.VIAEDGE_GROUP_FEEDBACK))
							|| (reportType.equals(ReportType.KFP_SLATE))
							|| (reportType.equals(ReportType.ALP_TALENT_GRID_V2))) {

						// participant/firstName here is actually the report
						// 'Group Name'
						// special naming convention for Group Feedback Report
						// http://segjira:8090/browse/NHN-4186

						groupName = StringEscapeUtils.unescapeHtml4(groupName);
						groupName = groupName.replaceAll("[<>/\\\\&#%{}*?$!:@+|=`\"]", ""); // none
						if (groupName != null && groupName.length() > 0) {
							fn += groupName.replaceAll("\\s+", "_");
						}

					}
					log.debug("groupName is: " + groupName);
					log.debug("fn before report type is: " + fn);
					// KFP-469 and KFP-470 stipulate how they want file names to
					// be for KFP (ALP) reports
					if (reportType.equals(ReportType.ALP_IND_V2)) {
						fn += "_ALP_INDIVIDUAL_V2-0";
					} else if (reportType.equals(ReportType.ALP_TALENT_GRID_V2)) {
						fn += "_ALP_TALENT_GRID_V2-0";
					} else if (reportType.equals(ReportType.KFP_INDIVIDUAL)) {
						fn += "_ALP_INDIVIDUAL_V1-0";
					} else if (reportType.equals(ReportType.KFP_SLATE)) {
						fn += "_ALP_TALENT_GRID_V1-0";
					} else {
						fn += "_" + reportType;
					}

					log.debug("fn after appending report type is: " + fn);

					// set the output info
					if (StringUtils.isBlank(fn))
						fn = "report"; // should never happen
					// REPORT LANG
					if (langCode.length() > 0)
						fn += "_" + langCode;
					// REPORT DATE
					if (reportDate.length() > 0)
						fn += "_" + reportDate;
					fn = fn.replace(" ", "_");

					// response.setCharacterEncoding("UTF-8");
					fn = URLEncoder.encode(fn, "UTF-8");
					log.debug("Final fn value is: " + fn);

					String disposition = String.format("attachment; filename=%s.pdf", fn);
					response.setContentType("application/pdf");
					response.addHeader("content-disposition", disposition);

					log.debug("disposition is: " + disposition);

					// call pdf generation
					System.out.println("calling ProcessReportToPdf");
					reportProcessor.ProcessReportToPdf(doc, response.getOutputStream(), reportXslt,
							ReportType.getSubTypeFromType(reportType, hybFlag), customParams);
				} else if (outType == GEN_HTML_OUTPUT) {
					// File name stuff not needed
					// set the output info
					System.out.println("rcm found, set content type to html");
					response.setContentType("text/html");
					response.setCharacterEncoding("UTF-8");

					// call html generation
					System.out.println("calling ProcessReportToHtml");
					reportProcessor.ProcessReportToHtml(doc, response.getOutputStream(), reportXslt,
							ReportType.getSubTypeFromType(reportType), "pdf", customParams);
				} else {
					System.out.println("Invalid outType-" + outType);
					response.setContentType("text/html");
					PrintWriter pw = response.getWriter();
					pw.print("<html><body><h1 style=\"color:red;\">No reports generated.</h1>Output type is invalid."
							+ "  Output type " + outType + "</body></html>");
					response.addHeader("Error",
							"<html><body><h1 style=\"color:red;\">No reports generated.</h1>Output type is invalid."
									+ "  Output type " + outType + "</body></html>");
					return;
				}
			}
		} catch (Exception excp) {
			excp.printStackTrace();
			throw new PalmsException("Failed to Generate Report due to: " + excp.getMessage(),
					PalmsErrorCode.REPORT_GEN_FAILED.getCode());
		}

		return;
	}

	/**
	 * Process for custom parameters
	 * 
	 * @param reportType
	 * @param request
	 * @return
	 */
	public Map<String, String> getCustomParams(String reportType, HttpServletRequest request) {
		// special processing for coaching plan (custom params)
		Map<String, String> ret = new HashMap<String, String>();
		if (reportType.equals(ReportType.COACHING_PLAN)) {
			if (request.getParameter("includeOverview") != null) {
				ret.put("includeOverview", request.getParameter("includeOverview"));
			}
			if (request.getParameter("includeGoals") != null) {
				ret.put("includeGoals", request.getParameter("includeGoals"));
			}
			if (request.getParameter("includeJournal") != null) {
				ret.put("includeJournal", request.getParameter("includeJournal"));
			}
		}
		if (ret.size() < 1) {
			ret = null;
		}

		return ret;
	}

	/**
	 * Get an XSLT path name based upon the report type
	 * 
	 * @param reportType
	 *            A report type code
	 * @return A valid xslt Path or null if there report type is unknown
	 */
	private String getXslt(String reportType) {
		String ret = null;

		if (reportType.equals(ReportType.COACHING_PLAN_SUMMARY)) {
			ret = COACHING_SUMM_RPT_XFORM;
		} else if (reportType.equals(ReportType.COACHING_PLAN)) {
			ret = COACHING_PLAN_RPT_XFORM;
		} else if (reportType.equals(ReportType.VIAEDGE_INDIVIDUAL_COACHING)) {
			ret = VEDGE_IND_COACH_RPT_XFORM;
		} else if (reportType.equals(ReportType.VIAEDGE_INDIVIDUAL_FEEDBACK)) {
			ret = VEDGE_IND_FDBK_RPT_XFORM;
		} else if (reportType.equals(ReportType.VIAEDGE_INDIVIDUAL_SUMMARY)) {
			ret = VEDGE_IND_SUMM_RPT_XFORM;
		} else if (reportType.equals(ReportType.VIAEDGE_GROUP_FEEDBACK)) {
			ret = VEDGE_GRP_FDBK_RPT_XFORM;
		} else if (reportType.equals(ReportType.KFP_INDIVIDUAL)) {
			ret = KFP_IND_RPT_XFORM;
		} else if (reportType.equals(ReportType.KFP_NARRATIVE)) {
			ret = KFP_IND_NAR_RPT_XFORM;
			// } else if (reportType.equals(ReportType.KFP_GROUP)) {
			// ret = KFP_GRP_RPT_XFORM;
		} else if (reportType.equals(ReportType.ALP_IND_V2)) {
			ret = ALP_IND_V2_RPT_XFORM;
		} else if (reportType.equals(ReportType.ALP_TALENT_GRID_V2)) {
			ret = ALP_TALENT_GRID_V2_RPT_XFORM;
		} else if (reportType.equals(ReportType.KFP_SLATE)) {
			ret = KFP_SLATE_RPT_XFORM;
		} else if (ReportType.LVA_MLL_REPORTS_TYPES.contains(reportType)) {
			ret = LVA_MLL_RPT_XFORM;
		} else if (reportType.equals(ReportType.SHELL_LAR_DEVELOPMENT)
				|| reportType.equals(ReportType.SHELL_LAR_READINESS)) {
			ret = SHELL_RPT_XFORM;

		}
		System.out.println("reportXslt = " + ret);

		return ret;
	}

	/**
	 * Method to find the highest phase type for the given participant and
	 * report
	 * 
	 * @param pptId
	 * @param reportType
	 * @return The highest phase type. If none found, returns 0
	 */
	public int findPhase(int pptId, String reportType) {
		int ret = 0;
		// Find the highest phase type. This means that if we come up with
		// new phase types that are not in ascending order then we will have
		// more work to put out the correct phase type ID here
		List<ReportContentModel> rcmList = reportDao.findByParticipantIdAndReportType(pptId, reportType);
		if (rcmList == null) {
			// Do nothing...
			System.out.println("No RCM files for ppt " + pptId + ", rptType " + reportType);
		} else {
			// find the RCM with the highest phase type
			int ph = 0;
			for (ReportContentModel rcm : rcmList) {
				if (rcm.getPhaseType().getPhaseTypeId() > ph)
					ph = rcm.getPhaseType().getPhaseTypeId();
			}
			ret = ph;
		}

		return ret;
	}
}
