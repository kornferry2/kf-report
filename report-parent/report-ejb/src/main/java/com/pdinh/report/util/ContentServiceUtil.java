package com.pdinh.report.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ContentServiceUtil {
	private static final Logger log = LoggerFactory
			.getLogger(ContentServiceUtil.class);

    private String defaultUserName = "appuser";
    private String defaultPassword = "logicmark";
	private String headerInfo = "";
	private String jSON = "";
	private int httpStatusCode = -1;
	private Map<String, List<String>> responseHeaders;

	public ContentServiceUtil() {
		 
	}

	public void initializeVariables() {
		headerInfo = "";
		jSON = "";
		httpStatusCode = -1;
		responseHeaders = null;
	}
	
	public void authenticate(final String userName, final String password){
		
		if (!StringUtils.isBlank(userName) && !StringUtils.isBlank(password)) {
			Authenticator.setDefault(new Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication(userName, password.toCharArray());
	            }
	        });
		} else {
			Authenticator.setDefault(new Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication(defaultUserName, defaultPassword.toCharArray());
	            }
	        });
		}
	}
	
	   public boolean doJavaHttpPost(String uri, String postInput, Map<String, String> requestHeaders)
			      throws Exception
			   {
			      boolean success = true;
			      HttpURLConnection httpConnection = openConnection(uri, postInput, "POST", requestHeaders);

			      BufferedReader bufferedReader = null;
			      try
			      {
			         bufferedReader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));

			         processResponse(httpConnection, bufferedReader);
			      }
			      catch (Exception e)
			      {
			         success = false;
			         //TODO error logging here - to error DB.
			         log.error("Error during Http call to "  + uri, e);
			      }
			      finally
			      {
			         if (null != bufferedReader)
			         {
			            bufferedReader.close();
			         }
			      }

			      return success;
			   }

	public boolean doHttpGet(String uri) throws Exception {
		boolean success = true;
		Map<String, String> requestHeaders = new HashMap<String, String>();

		requestHeaders.put("Content-Type", "application/json;charset=UTF-8");
		HttpURLConnection httpConnection = openConnection(uri, null, "GET", requestHeaders);

		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new InputStreamReader(
					httpConnection.getInputStream(), "UTF-8"));

			processResponse(httpConnection, bufferedReader);
		} catch (Exception e) {
			success = false;
			// TODO error logging here - to error DB.
			log.error("Error during Http call to " + uri, e);
		} finally {
			if (null != bufferedReader) {
				bufferedReader.close();
			}
		}

		return success;
	}

	protected HttpURLConnection openConnection(String uri, String postData, String connectionType, Map<String, String> requestHeaders)
			throws Exception {
		URL url = new URL(uri);
		HttpURLConnection httpConnection = (HttpURLConnection) url
				.openConnection();
		httpConnection.setRequestMethod(connectionType);
		httpConnection.setConnectTimeout(2000);
		httpConnection.setReadTimeout(60000);
		for (String key : requestHeaders.keySet()) {
			httpConnection.setRequestProperty(key, requestHeaders.get(key));
		}

		return httpConnection;
	}

	protected void processResponse(HttpURLConnection httpConnection,
			BufferedReader bufferedReader) throws Exception {
		StringBuilder stringBuilder = new StringBuilder();
		httpStatusCode = httpConnection.getResponseCode();
		responseHeaders = httpConnection.getHeaderFields();
		for (Entry<String, List<String>> header : httpConnection
				.getHeaderFields().entrySet()) {
			stringBuilder.append(header.getKey() + "=" + header.getValue());
		}
		headerInfo = stringBuilder.toString();
		String line = null;
		stringBuilder = new StringBuilder();
		while ((line = bufferedReader.readLine()) != null) {
			stringBuilder.append(line);
		}
		jSON = stringBuilder.toString();
	}

	public String getHeaderInfo() {
		return headerInfo;
	}

	public String getjSON() {
		return jSON;
	}

	public int getHttpStatusCode() {
		return httpStatusCode;
	}
	
	public Map<String, List<String>> getResponseHeaders() {
		return responseHeaders;
	}


}