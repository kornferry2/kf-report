package com.pdinh.report.helper;

import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.AttributedString;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.POIXMLProperties;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.JsonNode;
import com.pdinh.data.ms.dao.PositionDao;
import com.pdinh.data.ms.dao.ProjectUserDao;
import com.pdinh.exception.PalmsErrorCode;
import com.pdinh.exception.PalmsException;
import com.pdinh.persistence.ms.entity.Course;
import com.pdinh.persistence.ms.entity.Position;
import com.pdinh.persistence.ms.entity.ProjectUser;
import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.project.kfap.utils.ContentRetriever;
import com.pdinh.project.kfap.utils.KfapUtils;
import com.pdinh.reporting.generation.ReportParticipant;
import com.pdinh.reportserver.data.dao.GroupExtractData;
import com.pdinh.reportserver.data.dao.jpa.ReportRequestResponseDao;
import com.pdinh.reportserver.persistence.jpa.ReportRequestResponse;

/**
 * class KfalpGroupExtractHelper This class will provide the requisite
 * functionality data for the KfalpGroupExtract servlet. It will be an
 * instantiated object since its if static functionality and requisite data
 * could get "cross-threaded" if more than one user were to call it at the same
 * time
 */
@Stateless
@LocalBean
public class KfalpGroupExtractHelperBean {

	final static Logger log = LoggerFactory.getLogger(KfalpGroupExtractHelperBean.class);

	// Static private variables
	private static final String KFP_INST = "kfp";
	private static String KFP_MANIFEST = "kfpManifestV2";

	private static final String PAYLOAD_START_TAG = "<CalculationPayload>\n";
	private static final String PAYLOAD_END_TAG = "</CalculationPayload>\n";

	//private static String RESTFUL_STRING_PARAMS = "/jaxrs/mappedCalculationRequest/{instrumentId}/{manifest}/{tgtRole}";
	private static String RESTFUL_STRING_PARAMS = "/jaxrs/mappedCalculationRequest/instruments/{instrumentId}/manifests/{manifest}?targetLevel={tgtRole}";
	private static String CONTENT_FOLDER = "group_extract";

	// Watermark value for putting into documents
	private static final String WATERMARK = "Korn Ferry 15846ER2UL9";

	// Password to prevent changes to workbook sheets
	private static final String SHEET_PW = "alpGroupExtract";

	// Location of the logo image
	private static String LOGO_LOC = "projects/kfap/group_extract/demo_logo.png";

	private static final String BULLET = "" + '\u2022';

	// The number of fields that we save in the array for later output to the
	// Extract page. Doesn't include sortty stuff
	private static final int SCORE_FIELD_COUNT = 58;
	// The total number of fields in the object array (includes the sorty stuff)
	private static final int TOTAL_FIELD_COUNT = 61;
	// Define a set of constants that will allow us to populate an array of
	// values and pull them back out again. The number of items defined here
	// should equal the TOTAL_FIELD_COUNT and the highest value assigned should
	// be 1 less than TOTAL_FIELD_COUNT (indices start at 0). Putting out the
	// scores, we go through SCORE_FIELD_COUNT because that is the total number
	// of DISPLAYABLE score fields; TOTAL_... includes "hidden" fields used for
	// sorting.

	private static final int PPT_FIRSTNAME_IDX = 0;
	private static final int PPT_LASTNAME_IDX = 1;
	private static final int TARGET_LEVEL_IDX = 2;
	private static final int SETUP_DATE_IDX = 3;
	private static final int PROJECT_NAME_IDX = 4;
	private static final int OPTIONAL_1_IDX = 5;
	private static final int OPTIONAL_2_IDX = 6;
	private static final int OPTIONAL_3_IDX = 7;
	private static final int SORT_ORDER_IDX = 8;
	private static final int DRVER_SIGNPOST_IDX = 9;
	private static final int AD_RANGE_IDX = 10;
	private static final int AD_SCORE_IDX = 11;
	private static final int CP_RANGE_IDX = 12;
	private static final int CP_SCORE_IDX = 13;
	private static final int RP_RANGE_IDX = 14;
	private static final int RP_SCORE_IDX = 15;
	private static final int EXPER_SIGNPOST_IDX = 16;
	private static final int CE_RANGE_IDX = 17;
	private static final int CE_SCORE_IDX = 18;
	private static final int PE_RANGE_IDX = 19;
	private static final int PE_SCORE_IDX = 20;
	private static final int KC_RANGE_IDX = 21;
	private static final int KC_SCORE_IDX = 22;
	private static final int AWARE_SIGNPOST_IDX = 23;
	private static final int SA_RANGE_IDX = 24;
	private static final int SA_SCORE_IDX = 25;
	private static final int SSA_RANGE_IDX = 26;
	private static final int SSA_SCORE_IDX = 27;
	private static final int AGILITY_SIGNPOST_IDX = 28;
	private static final int MA_RANGE_IDX = 29;
	private static final int MA_SCORE_IDX = 30;
	private static final int PA_RANGE_IDX = 31;
	private static final int PA_SCORE_IDX = 32;
	private static final int CA_RANGE_IDX = 33;
	private static final int CA_SCORE_IDX = 34;
	private static final int RA_RANGE_IDX = 35;
	private static final int RA_SCORE_IDX = 36;
	private static final int TRAITS_SIGNPOST_IDX = 37;
	private static final int FO_RANGE_IDX = 38;
	private static final int FO_SCORE_IDX = 39;
	private static final int PER_RANGE_IDX = 40;
	private static final int PER_SCORE_IDX = 41;
	private static final int TA_RANGE_IDX = 42;
	private static final int TA_SCORE_IDX = 43;
	private static final int AS_RANGE_IDX = 44;
	private static final int AS_SCORE_IDX = 45;
	private static final int OP_RANGE_IDX = 46;
	private static final int OP_SCORE_IDX = 47;
	private static final int CAP_SIGNPOST_IDX = 48;
	private static final int PS_RANGE_IDX = 49;
	private static final int PS_SCORE_IDX = 50;
	private static final int DERAIL_SIGNPOST_IDX = 51;
	private static final int VO_RANGE_IDX = 52;
	private static final int VO_SCORE_IDX = 53;
	private static final int MM_RANGE_IDX = 54;
	private static final int MM_SCORE_IDX = 55;
	private static final int CL_RANGE_IDX = 56;
	private static final int CL_SCORE_IDX = 57;
	// Sorty stuff. Always make these the last ones
	private static final int SORT_MAGIC_IDX = 58;
	private static final int SORT_LNAME_IDX = 59;
	private static final int SORT_FNAME_IDX = 60;

	private static List<Integer> SIGNPOST_LIST = new ArrayList<Integer>(Arrays.asList(DRVER_SIGNPOST_IDX,
			EXPER_SIGNPOST_IDX, AWARE_SIGNPOST_IDX, AGILITY_SIGNPOST_IDX, TRAITS_SIGNPOST_IDX, CAP_SIGNPOST_IDX,
			DERAIL_SIGNPOST_IDX));

	private static List<Integer> SCALE_LIST = new ArrayList<Integer>(Arrays.asList(AD_SCORE_IDX, CP_SCORE_IDX,
			RP_SCORE_IDX, CE_SCORE_IDX, PE_SCORE_IDX, KC_SCORE_IDX, SA_SCORE_IDX, SSA_SCORE_IDX, MA_SCORE_IDX,
			PA_SCORE_IDX, CA_SCORE_IDX, RA_SCORE_IDX, FO_SCORE_IDX, PER_SCORE_IDX, TA_SCORE_IDX, AS_SCORE_IDX,
			OP_SCORE_IDX, PS_SCORE_IDX, VO_SCORE_IDX, MM_SCORE_IDX, CL_SCORE_IDX));

	// Instance variables
	private GroupExtractData inParms = null; // Validated input parameters
	private String coverDate = "";

	// XSSFCellStyle dcs = null;

	// 0, 1, or 2 0 = No Ravens, 1 = Some Ravens, 2, = All Ravens
	private int ravensValue;

	// no ravens project according to ProjectCourse
	boolean mixedV1andV2 = false;
	boolean incompletes = false;

	// Buckets for ppt ID info
	List<Integer> allGroupPIds = new ArrayList<Integer>();

	// Bucket for text data
	private JsonNode jsonNode = null;

	// Bucket for the score data
	private Object scoreData[][];

	// Client name
	private String clientName = null;

	// Target level label
	private String targLabel = null;

	// Default assumes there is a group name.
	private boolean hasGroup = true;

	@Resource(name = "application/report/config_properties", type = Properties.class)
	private Properties configProperties;

	@EJB
	private PositionDao positionDao;

	@EJB
	private ProjectUserDao projectUserDao;

	@EJB
	ReportRequestResponseDao reportResponseDao;

	@EJB
	private ContentRetriever contentRetriever;

	// Constructors
	public KfalpGroupExtractHelperBean() {

	}

	// Construct the spreadsheet
	/**
	 * constructExtractWorkbook - The sole public method in this class, it is
	 * the controller method for fetching text and data and using it to create
	 * the extract workbook.
	 * 
	 * @param parms
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalStateException
	 * @throws PalmsException
	 */
	public Workbook constructExtractWorkbook(GroupExtractData parms) throws IllegalArgumentException,
			IllegalStateException, PalmsException {
		if (parms == null) {
			throw new IllegalArgumentException("No parameter object passed to Extract helper.");
		}
		this.inParms = parms;

		// Set up the date format and the date
		SimpleDateFormat sdf = setUpDateFormat(inParms.getLanguageCode());
		this.coverDate = sdf.format(new Date());

		if (StringUtils.isEmpty(this.inParms.getGroupName())) {
			this.hasGroup = false;
		}

		// Fetch the text
		fetchExtractText();

		// Fetch the score data
		fetchAlpScoreData();

		// Construct the spreadsheet
		Workbook wb = createExtractWorkbook();

		return wb;
	}

	// --------------------------------------------------------------
	//
	// Scorey stuff

	/**
	 * fetchAlpScoreData - Fetches score data for all participants in the
	 * extract. If the ppt is incomplete it sets up dummy rows with "na" for all
	 * score values. The data is stored in the instance so that it can be used
	 * later by the spreadsheet generation. Much of the logic is similar to that
	 * found in the TalentGridRgrToRcmV2 class
	 */
	private void fetchAlpScoreData() throws IllegalStateException, PalmsException {
		if (this.inParms == null) {
			throw new IllegalStateException("Unable to fetch score data; input parameters not initialized.");
		}

		// Create map to hold project name
		Map<Integer, String> userProjectMap = new HashMap<Integer, String>();
		Map<Integer, String> mapOfUserIdsProjIds = new HashMap<Integer, String>();
		// Create HashSet of unique ProjectIds
		Set<Integer> setProjectIDs = new HashSet<Integer>();

		Integer id;
		String projectID;
		Integer intProjectID;
		String projectName;
		// Loop through the participant list, extracting IDs and project info --
		// ID and name
		allGroupPIds.clear();
		for (ReportParticipant ppt : this.inParms.getParticipantsObject().getParticipants()) {

			// String projectInfo;
			try {
				id = Integer.parseInt(ppt.getParticipantId());
				projectID = ppt.getProjectId();
				intProjectID = Integer.parseInt(projectID);
				projectName = ppt.getProjectName();
			} catch (NumberFormatException e) {
				log.error("Invalid pptId " + ppt.getParticipantId() + ".  Skipped.");
				continue;
			}
			allGroupPIds.add(id);

			setProjectIDs.add(intProjectID);

			// add Project Name per user
			userProjectMap.put(id, projectName);
			mapOfUserIdsProjIds.put(id, ppt.getProjectId());

		}
		// Create map to hold DateAdded to project per user
		Map<Integer, String> userProjDateAdded = new HashMap<Integer, String>();

		Integer intProjID = 0;
		@SuppressWarnings("rawtypes")
		Iterator iter = setProjectIDs.iterator();
		while (iter.hasNext()) {
			intProjID = (Integer) iter.next();

			// use projectID to get SetupDate (date_added)
			// can use all the Pids multiple times because it's an IN clause
			List<ProjectUser> projusers = projectUserDao.findProjectUsersFiltered(intProjID, allGroupPIds);

			Integer uID = 0;
			String longDateFormat = "dd-MMMM-yyyy";
			SimpleDateFormat longSdf = new SimpleDateFormat(longDateFormat);
			String dateAdded = "";

			for (int h = 0; h < projusers.size(); h++) {
				ProjectUser projuser = projusers.get(h);
				uID = projuser.getUser().getUsersId();
				dateAdded = longSdf.format(projuser.getDateAdded());
				userProjDateAdded.put(uID, dateAdded);
			}
		}

		// Go get the position rows for these participants
		List<Position> positions = positionDao.findPositionsByUsersIdsAndCourseAbbvs(allGroupPIds,
				Arrays.asList(Course.KFP));

		// Make two arraylists, 1 for participants that have complete
		// assessments and another for participants that are incomplete
		List<Integer> groupPIds = new ArrayList<Integer>();
		List<Integer> incompleteGroupPIds = new ArrayList<Integer>();

		String strpid = "";
		Integer complStatus;

		Integer userID = 0;

		String userData = "";
		String userFirstName = "";
		String userLastName = "";
		String optional1 = "";
		String optional2 = "";
		String optional3 = "";
		String userProjectName = "";
		String userSetupDate = "";
		String opt1 = "";
		String opt2 = "";
		String opt3 = "";

		Map<String, String> userMap = new HashMap<String, String>();

		log.debug("Starting loop through positions list");
		boolean gotClient = false;
		this.incompletes = false;
		this.clientName = "Not Available"; // Just in case...
		for (int j = 0; j < positions.size(); j++) {
			Position position = positions.get(j);
			userID = position.getId().getUsersId();
			strpid = userID.toString();

			userFirstName = position.getUser().getFirstname();
			userLastName = position.getUser().getLastname();
			optional1 = position.getUser().getOptional1();
			optional2 = position.getUser().getOptional2();
			optional3 = position.getUser().getOptional3();

			opt1 = (optional1 == null ? "" : optional1);
			opt2 = (optional2 == null ? "" : optional2);
			opt3 = (optional3 == null ? "" : optional3);

			// Extract Project Name for this person
			userProjectName = userProjectMap.get(userID);

			// Extract Users setup date (Date Added) in the Project
			userSetupDate = userProjDateAdded.get(userID);

			// create string to pass along
			userData = userFirstName + "|" + userLastName + "|" + userSetupDate + "|" + userProjectName + "|" + opt1
					+ "|" + opt2 + "|" + opt3;

			userMap.put(strpid, userData);

			// Get the company name from the first valid candidate.
			// ASSUMPTION: all of the participants belong to the same client...
			// the first name fetched will determine the client name for the
			// report
			if (!gotClient) {
				String cand = position.getUser().getCompany().getCoName();
				if (!StringUtils.isEmpty(cand)) {
					this.clientName = cand;
					gotClient = true;
				}
			}

			complStatus = position.getCompletionStatus();

			if (complStatus == Position.COMPLETE) {
				groupPIds.add(userID);
			} else {
				incompleteGroupPIds.add(userID);
				incompletes = true;
			}
		} // End position for loop
		log.debug("Finished loop through positions list to get ids, completion status and put data in userMap");

		int numScored = groupPIds.size();
		int numUnscored = incompleteGroupPIds.size();
		int len = numScored + numUnscored;
		this.scoreData = new Object[len][TOTAL_FIELD_COUNT];

		// Get the target level label for use in setting up the ppt score arrays
		this.targLabel = this.jsonNode.get("generic").get("labelCase" + this.inParms.getTargetLevel()).textValue();

		// Fetch score data only for those partids that have completed
		// assessment(s)
		try {
			// Use the Talent Grid document type here
			fetchScoredData(ReportType.ALP_IND_V2, groupPIds, mapOfUserIdsProjIds, this.inParms.getTargetLevel(), userMap);
		} catch (PalmsException e) {
			log.debug("problem to fetch and map input data!");
			e.printStackTrace();
			throw new PalmsException("Problem fetching score data due to : " + e.getMessage(),
					PalmsErrorCode.RCM_GEN_FAILED.getCode());
		}

		// And add rows for the unscored participants
		addUnscoredData(incompleteGroupPIds, userMap, numScored);

		// Loop through the completed ppt score list and create artifacts for
		// sorting
		for (Object[] pptEnt : this.scoreData) {
			// Sort 1: Add up the number of signposts that are 1. This works
			// because signposts are always 1 or 0
			int spCnt = 0;
			for (int idx : SIGNPOST_LIST) {
				if (idx == CAP_SIGNPOST_IDX && this.ravensValue != 2) {
					// Skip capacity signpost if not everyone has Ravens
					continue;
				}
				if (pptEnt[idx] == null) {
					// skip unscored items (they effectively score as 0)
					continue;
				}
				int score = (Integer) pptEnt[idx];
				if (score >= 0) {
					spCnt += score;
				}
			}

			// int cnt50 = 0;
			int cnt91 = 0;
			int cnt5090 = 0;
			int cnt1149 = 0;
			// int cnt10u = 0;
			for (int idx : SCALE_LIST) {
				// That is a list of all scales. Exclude any derailer scale
				// scores from the rest of the "magic number" calculations. This
				// works because derailers are the last scores in the list
				if (idx >= DERAIL_SIGNPOST_IDX) {
					continue;
				}
				// skip the problem solving scale if there is at least one ppt
				// with no Ravens
				if (idx == PS_SCORE_IDX && this.ravensValue != 2) {
					// Skip me
					continue;
				}
				// skip any with no scores
				if (pptEnt[idx] == null) {
					continue;
				}
				int score = (Integer) pptEnt[idx];
				// skip na scores (set to -1 earlier)
				if (score < 0) {
					continue;
				}
				//
				// // This is the old sort
				// // Sort 2: Number of scales 50% and above
				// if (score > 49) {
				// cnt50++;
				// }
				// // Sort 3: Number of scales 91% and above
				// if (score > 90) {
				// cnt91++;
				// }
				// // Sort 4: Number of scales 50% - 90% (inclusive)
				// if (score > 49 && score < 91) {
				// cnt5090++;
				// }
				// // Sort 5: Number of scales 11% - 49% (inclusive)
				// if (score > 10 && score < 50) {
				// cnt1149++;
				// }
				// // Sort 6: Number of scales 10% and below
				// if (score < 11) {
				// cnt10u++;
				// }

				// This is the new sort
				// Sort 2: Number of scales 91% and above
				if (score > 90) {
					cnt91++;
				}
				// Sort 3: Number of scales 50% - 90% (inclusive)
				if (score > 49 && score < 91) {
					cnt5090++;
				}
				// Sort 4: Number of scales 11% - 49% (inclusive)
				if (score > 10 && score < 50) {
					cnt1149++;
				}

			}
			// Make the "magic number"
			long magic = spCnt;
			// magic = (magic * 100) + cnt50;
			magic = (magic * 100) + cnt91;
			magic = (magic * 100) + cnt5090;
			magic = (magic * 100) + cnt1149;
			// magic = (magic * 100) + cnt10u;
			pptEnt[SORT_MAGIC_IDX] = magic;
		}

		// Sort
		Arrays.sort(this.scoreData, new ExtractItemsComparator());

		// TOTO Replace this with a loop to put in the sort order
		for (int so = 0; so < this.scoreData.length; so++) {
			// sort order is the index + 1
			this.scoreData[so][SORT_ORDER_IDX] = so + 1;
		}

		return;
	}

	/**
	 * ExtractItemsComparator - Comparator to do the sort on the score data
	 */
	private static class ExtractItemsComparator implements Comparator<Object[]> {
		@Override
		public int compare(Object[] ed1, Object[] ed2) {
			int comp;
			// Sort descending on magic number
			long dif = (Long) ed2[SORT_MAGIC_IDX] - (Long) ed1[SORT_MAGIC_IDX];
			if (dif != 0) {
				// We want to return an int. If we simply cast to int we may
				// lose significant digits and the value may not be correct.
				// Return an integer with the same sign as the result; that's
				// all we need
				return (dif > 0 ? 1 : -1);
			}

			// tie breaker - Sort on name
			// comp = ((String)
			// ed1[SORT_LNAME_IDX]).compareToIgnoreCase((String)
			// ed2[SORT_LNAME_IDX]);
			comp = ((String) ed1[SORT_LNAME_IDX]).compareTo((String) ed2[SORT_LNAME_IDX]);
			if (comp != 0)
				return comp;

			// comp = ((String)
			// ed1[SORT_FNAME_IDX]).compareToIgnoreCase((String)
			// ed2[SORT_FNAME_IDX]);
			comp = ((String) ed1[SORT_FNAME_IDX]).compareTo((String) ed2[SORT_FNAME_IDX]);
			if (comp != 0)
				return comp;

			// If no tie breakers yet, return arbitrary value BUT NOT 0!!!
			return 1;
		}
	}

	/**
	 * setUpDateFormat
	 * 
	 * @param languageCode
	 * @return
	 */
	private SimpleDateFormat setUpDateFormat(String lang) {

		String dateFormat;
		// Date formats per KFP-708
		if (lang.equals("en-US")) {
			// dateFormat = "MM/dd/yyyy";
			// Per KFP-753 date format FOR THE EXTRACT for en-US is:
			dateFormat = "dd MMMM yyyy";
		} else if (lang.equals("zh-CN")) {
			dateFormat = "yyyy-MM-dd";
		} else if (lang.equals("ko")) {
			dateFormat = "yyyy-MM-dd";
		} else if (lang.equals("ja")) {
			dateFormat = "yyyy-MM-dd";
		} else if (lang.equals("fr")) {
			dateFormat = "dd/MM/yyyy";
		} else if (lang.equals("es")) {
			dateFormat = "dd/MM/yyyy";
		} else if (lang.equals("de")) {
			dateFormat = "dd.MM.yyyy";
		} else if (lang.equals("tr")) {
			dateFormat = "dd.MM.yyyy";
		} else if (lang.equals("ru")) {
			dateFormat = "dd.MM.yyyy";
		} else if (lang.equals("pt-BR")) {
			dateFormat = "dd/MM/yyyy";
		} else if (lang.equals("en-GB")) {
			dateFormat = "dd/MM/yyyy";
		} else {
			// Default... any country code not in the above list
			dateFormat = "dd-MM-yyyy";
		}

		return new SimpleDateFormat(dateFormat);
	}

	/**
	 * fetchScoredData - Fetch the score data from tincan and build the ppt
	 * score arrays
	 * 
	 * @param rType
	 * @param groupPIds
	 * @param targLevel
	 * @param userMap
	 * @throws PalmsException
	 */
	private void fetchScoredData(String rType, List<Integer> groupPIds, Map<Integer, String> mapOfUserIdsProjIds, int targLevel, Map<String, String> userMap)
			throws PalmsException {
		String urlBase = this.configProperties.getProperty("tincanAdminRestUrl");

		// // Value for url when going against QA
		// // TODO Comment out this when checking stuff in
		// urlBase = "http://sanfranqagf3:8080/tincanadminrest";

		String calcScoreUrl = urlBase
				+ RESTFUL_STRING_PARAMS.replace("{instrumentId}", KFP_INST + "").replace("{manifest}", KFP_MANIFEST)
						.replace("{tgtRole}", "" + targLevel);
		log.debug("Calculation URL = " + calcScoreUrl);

		Map<String, Element> scoreMap = new HashMap<String, Element>();
		List<Integer> nonExistingPids = new ArrayList<Integer>();
		String payloadSessionID;
		int g = 0;

		// Get the data for the ppts we can
		List<ReportRequestResponse> existingRgrList = existingRgrDbLookup(rType, groupPIds, targLevel);
		if (existingRgrList != null && !existingRgrList.isEmpty()) {
			Map<String, Element> existingScoreMap = prepareExistingScoreMap(existingRgrList);
			scoreMap.putAll(existingScoreMap);
			nonExistingPids = removeExistingPids(groupPIds, existingRgrList);
		} else {
			nonExistingPids.addAll(groupPIds);
		}

		// Create a payload session ID for the current time
		Date date = new Date();
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(date);
		Integer Hour = calendar.get(Calendar.HOUR_OF_DAY);
		Integer Minutes = calendar.get(Calendar.MINUTE);
		Integer Seconds = calendar.get(Calendar.SECOND);
		payloadSessionID = Hour + "" + Minutes + "" + Seconds + "";

		// Go do scoring on those where we don't have the data
		for (int start = 0; start < nonExistingPids.size(); start += 10) {
			int end = Math.min(start + 10, nonExistingPids.size());
			List<Integer> sublist = nonExistingPids.subList(start, end);
			String datasetId = "";
			String strPartId = "0";
			String projId = "";
			
			for (int j = 0; j < sublist.size(); j++) {
				strPartId = sublist.get(j).toString();
				projId = mapOfUserIdsProjIds.get(Integer.parseInt(strPartId));
				datasetId = datasetId + "<dataset id=\"" + strPartId + "\" projectId=\"" + projId + "\" />";
				//datasetId = datasetId + "<dataset id=\"" + strPartId + "\"/>";
			}

			String payload = PAYLOAD_START_TAG + datasetId + PAYLOAD_END_TAG;
			log.debug("payload session: " + payloadSessionID + "; Burst Number: " + (g + 1) + "; Payload sent = "
					+ payload);

			Document scoreDoc = getScores(calcScoreUrl, payload);
			saveScoresToDB(scoreDoc, rType, targLevel);
			NodeList nodes = scoreDoc.getElementsByTagName("calculationPayload");
			log.debug("Starting loop to put score data into Map");
			for (int i = 0; i < nodes.getLength(); i = i + 1) {
				Element calculationPayload = (Element) nodes.item(i);
				String Attribute = calculationPayload.getAttribute("particpantId");
				scoreMap.put(Attribute, calculationPayload);
			}
			log.debug("Finished loop to put score data into Map");
			g = g + 1;
		} // End of scoring (10 at a time) loop

		Double noRavensCount = 0.0;
		Double capacityCount = 0.0;
		Double versionCount = 0.0;
		Double totalNoRavensCount = 0.0;
		Double totalCapacityCount = 0.0;
		Double totalVersionCount = 0.0;
		// Get some summary data
		for (Entry<String, Element> scoreMapEntry : scoreMap.entrySet()) {
			Element singlePidResult = scoreMapEntry.getValue();
			KfapUtils.printElemnt(singlePidResult); // Debug logging

			String singlePidResultElementStr = KfapUtils.getStringFromElement(singlePidResult);
			log.debug("singlePidResultElementStr " + singlePidResultElementStr);
			Document singleScoreDoc = KfapUtils.processRgrXml(singlePidResultElementStr);

			noRavensCount = (KfapUtils.getNoRavensCount(singleScoreDoc, KFP_INST, "capacity.score"));
			capacityCount = (KfapUtils.getTotalCount(singleScoreDoc, KFP_INST, "capacity.score"));
			versionCount = (KfapUtils.getVersionCount(singleScoreDoc, KFP_INST, "1"));

			totalNoRavensCount = totalNoRavensCount + noRavensCount;
			totalCapacityCount = totalCapacityCount + capacityCount;
			totalVersionCount = totalVersionCount + versionCount;
		}
		log.debug("All payload data sent, received, and assembled for payload session " + payloadSessionID);

		Integer dCompare = Double.compare(totalCapacityCount, totalNoRavensCount);

		if (dCompare == 0) {
			this.ravensValue = 0;
		} else {
			if (totalNoRavensCount > 0) {
				this.ravensValue = 1;
			} else {
				this.ravensValue = 2;
			}
		}

		// Determine if there are any Version 1 scorings in the Payload
		if (totalVersionCount > 0) {
			mixedV1andV2 = true;
		} else {
			mixedV1andV2 = false;
		}

		// Assemble the data into object arrays
		for (int i = 0; i < groupPIds.size(); i++) {
			Object pptScores[];
			pptScores = new Object[TOTAL_FIELD_COUNT];

			String strPartId = groupPIds.get(i).toString();

			String usr = userMap.get(strPartId);

			if (usr == null) {
				pptScores[PPT_FIRSTNAME_IDX] = "Not Available";
				pptScores[PPT_LASTNAME_IDX] = "Not Available";
				pptScores[SORT_FNAME_IDX] = "";
				pptScores[SORT_LNAME_IDX] = "";
				pptScores[SETUP_DATE_IDX] = "";
				pptScores[PROJECT_NAME_IDX] = "";
				pptScores[OPTIONAL_1_IDX] = "";
				pptScores[OPTIONAL_2_IDX] = "";
				pptScores[OPTIONAL_3_IDX] = "";
			} else {

				String[] usrData = usr.split("\\|", -1);
				pptScores[PPT_FIRSTNAME_IDX] = usrData[0];
				pptScores[PPT_LASTNAME_IDX] = usrData[1];
				pptScores[SORT_FNAME_IDX] = usrData[0];
				pptScores[SORT_LNAME_IDX] = usrData[1];
				pptScores[SETUP_DATE_IDX] = usrData[2];
				pptScores[PROJECT_NAME_IDX] = usrData[3];
				pptScores[OPTIONAL_1_IDX] = usrData[4];
				pptScores[OPTIONAL_2_IDX] = usrData[5];
				pptScores[OPTIONAL_3_IDX] = usrData[6];
			}

			pptScores[TARGET_LEVEL_IDX] = this.targLabel;

			// For Drivers
			int spScore;
			// Signpost score
			spScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "drivers.score")));
			pptScores[DRVER_SIGNPOST_IDX] = spScore;
			// Constituent Scores
			pptScores[AD_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "drivers.advdrive.norm")));
			pptScores[AD_RANGE_IDX] = calcDeaRange((Integer) pptScores[AD_SCORE_IDX]);
			pptScores[CP_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "drivers.careerplan.norm")));
			pptScores[CP_RANGE_IDX] = calcDeaRange((Integer) pptScores[CP_SCORE_IDX]);
			pptScores[RP_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "drivers.rolepref.norm")));
			pptScores[RP_RANGE_IDX] = calcDeaRange((Integer) pptScores[RP_SCORE_IDX]);

			// For Experience
			// Signpost score
			spScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "experience.score")));
			pptScores[EXPER_SIGNPOST_IDX] = spScore;
			// Constituent Scores
			pptScores[CE_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "experience.core.norm")));
			pptScores[CE_RANGE_IDX] = calcDeaRange((Integer) pptScores[CE_SCORE_IDX]);
			pptScores[PE_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "experience.perspective.norm")));
			pptScores[PE_RANGE_IDX] = calcDeaRange((Integer) pptScores[PE_SCORE_IDX]);
			pptScores[KC_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "experience.key.norm")));
			pptScores[KC_RANGE_IDX] = calcDeaRange((Integer) pptScores[KC_SCORE_IDX]);

			// For Awareness
			// Signpost score
			spScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "awareness.score")));
			pptScores[AWARE_SIGNPOST_IDX] = spScore;
			// Constituent Scores
			pptScores[SA_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "awareness.self.norm")));
			pptScores[SA_RANGE_IDX] = calcDeaRange((Integer) pptScores[SA_SCORE_IDX]);
			pptScores[SSA_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "awareness.sit.norm")));
			pptScores[SSA_RANGE_IDX] = calcDeaRange((Integer) pptScores[SSA_SCORE_IDX]);

			// For Learning agility
			// Signpost score
			spScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "agile.score")));
			pptScores[AGILITY_SIGNPOST_IDX] = spScore;
			// Constituent Scores
			pptScores[MA_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "agile.mental.norm")));
			pptScores[MA_RANGE_IDX] = calcAtcRange((Integer) pptScores[MA_SCORE_IDX]);
			pptScores[PA_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "agile.people.norm")));
			pptScores[PA_RANGE_IDX] = calcAtcRange((Integer) pptScores[PA_SCORE_IDX]);
			pptScores[CA_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "agile.change.norm")));
			pptScores[CA_RANGE_IDX] = calcAtcRange((Integer) pptScores[CA_SCORE_IDX]);
			pptScores[RA_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "agile.results.norm")));
			pptScores[RA_RANGE_IDX] = calcAtcRange((Integer) pptScores[RA_SCORE_IDX]);

			// For Leadership traits
			// Signpost score
			spScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "traits.score")));
			pptScores[TRAITS_SIGNPOST_IDX] = spScore;
			// Constituent Scores
			pptScores[FO_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "traits.focus.norm")));
			pptScores[FO_RANGE_IDX] = calcAtcRange((Integer) pptScores[FO_SCORE_IDX]);
			pptScores[PER_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "traits.persist.norm")));
			pptScores[PER_RANGE_IDX] = calcAtcRange((Integer) pptScores[PER_SCORE_IDX]);
			pptScores[TA_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "traits.tolerance.norm")));
			pptScores[TA_RANGE_IDX] = calcAtcRange((Integer) pptScores[TA_SCORE_IDX]);
			pptScores[AS_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "traits.assert.norm")));
			pptScores[AS_RANGE_IDX] = calcAtcRange((Integer) pptScores[AS_SCORE_IDX]);
			pptScores[OP_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "traits.optimism.norm")));
			pptScores[OP_RANGE_IDX] = calcAtcRange((Integer) pptScores[OP_SCORE_IDX]);

			// For Capacity
			// Signpost score
			int capacityScore = -1;
			int capacityPct = -1;
			if (this.ravensValue == 0) {
				capacityScore = -1;
				capacityPct = -1;
			} else {
				String capacity = KfapUtils.getGroupScoredDatabyElement(scoreMap.get(strPartId), KFP_INST,
						"capacity.score");
				if (capacity == null || capacity == "" || capacity.equals("NaN")) {
					capacityScore = -1;
					capacityPct = -1;
				} else {
					capacityPct = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
							scoreMap.get(strPartId), KFP_INST, "capacity.norm")));
					capacityScore = (int) Math.round(Double.parseDouble(capacity));
				}
			}
			pptScores[CAP_SIGNPOST_IDX] = capacityScore;
			// Constituent Score
			pptScores[PS_SCORE_IDX] = capacityPct;
			pptScores[PS_RANGE_IDX] = calcAtcRange((Integer) pptScores[PS_SCORE_IDX]);

			// Signpost score
			spScore = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "derailer.score")));
			pptScores[DERAIL_SIGNPOST_IDX] = spScore;
			// Constituent Scores
			pptScores[VO_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "derailer.volatile.norm")));
			pptScores[VO_RANGE_IDX] = calcDerailRange((Integer) pptScores[VO_SCORE_IDX]);
			pptScores[MM_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "derailer.micro.norm")));
			pptScores[MM_RANGE_IDX] = calcDerailRange((Integer) pptScores[MM_SCORE_IDX]);
			pptScores[CL_SCORE_IDX] = (int) Math.round(Double.parseDouble(KfapUtils.getGroupScoredDatabyElement(
					scoreMap.get(strPartId), KFP_INST, "derailer.closed.norm")));
			pptScores[CL_RANGE_IDX] = calcDerailRange((Integer) pptScores[CL_SCORE_IDX]);

			this.scoreData[i] = pptScores;
		}

		return;
	}

	/**
	 * addUnscoredData - Put the unscored stuff into the holder by making dummy
	 * rows with scores set to null (default value when you construct an array)
	 * 
	 * @param incompleteGroupPIds
	 * @param userMap
	 * @param numScored
	 */
	private void addUnscoredData(List<Integer> incompleteGroupPIds, Map<String, String> userMap, int numScored) {
		log.debug("Starting loop to put data into Jaxb for Incompleted Users");
		Object[] scorz;
		// This should be the index of the first "open" row in the array of ppt
		// scores arrays
		int l = numScored;
		for (int k = 0; k < incompleteGroupPIds.size(); k++) {
			scorz = new Object[TOTAL_FIELD_COUNT];
			String partId = incompleteGroupPIds.get(k).toString();
			String strPartId = partId.toString();
			String usr = userMap.get(strPartId);
			if (usr == null) {
				scorz[PPT_FIRSTNAME_IDX] = "Not Available";
				scorz[PPT_LASTNAME_IDX] = "Not Available";
				scorz[SORT_FNAME_IDX] = "";
				scorz[SORT_LNAME_IDX] = "";
				scorz[SETUP_DATE_IDX] = "";
				scorz[PROJECT_NAME_IDX] = "";
				scorz[OPTIONAL_1_IDX] = "";
				scorz[OPTIONAL_2_IDX] = "";
				scorz[OPTIONAL_3_IDX] = "";
			} else {
				String[] usrIncData = usr.split("\\|", -1);
				scorz[PPT_FIRSTNAME_IDX] = usrIncData[0];
				scorz[PPT_LASTNAME_IDX] = usrIncData[1];
				scorz[SORT_FNAME_IDX] = usrIncData[0];
				scorz[SORT_LNAME_IDX] = usrIncData[1];
				scorz[SETUP_DATE_IDX] = usrIncData[2];
				scorz[PROJECT_NAME_IDX] = usrIncData[3];
				scorz[OPTIONAL_1_IDX] = usrIncData[4];
				scorz[OPTIONAL_2_IDX] = usrIncData[5];
				scorz[OPTIONAL_3_IDX] = usrIncData[6];
				scorz[TARGET_LEVEL_IDX] = this.targLabel;
			}

			this.scoreData[l] = scorz;
			l++;
		}

		return;
	}

	/**
	 * existingRgrDbLookup - Checks for existing score data for the participants
	 * Lifted directly from TalentGridRgrToRcmV2.
	 * 
	 * @param type
	 * @param groupPIds
	 * @param targLevelIndex
	 * @return
	 */
	private List<ReportRequestResponse> existingRgrDbLookup(String type, List<Integer> groupPIds, Integer targLevelIndex) {

		List<ReportRequestResponse> existingRgrList = this.reportResponseDao
				.findBatchByParticipantIdAndRgrTypeAndTarget(groupPIds, type, targLevelIndex);

		if (!(existingRgrList == null)) {
			log.debug("{} ppts, {} rgrs", groupPIds.size(), existingRgrList.size());
		} else {
			log.debug("No rgrs found");
			return null;
		}

		return existingRgrList;
	}

	/**
	 * prepareExistingScoreMap - Returns a map; key is a participant ID and
	 * value is a a calculationPayloadElement. Lifted directly from
	 * TalentGridRgrToRcmV2.
	 * 
	 * @param existingRgrList
	 * @return
	 * @throws PalmsException
	 */
	private Map<String, Element> prepareExistingScoreMap(List<ReportRequestResponse> existingRgrList)
			throws PalmsException {

		Map<String, Element> persistedScoreMap = new HashMap<String, Element>();

		for (ReportRequestResponse rptReqResp : existingRgrList) {
			String existingRgrStr = rptReqResp.getRgrXml();
			log.debug("existingRgrStr is  = " + existingRgrStr);
			Document rgrDoc = KfapUtils.processRgrXml(existingRgrStr);
			Element calculationPayloadElement = rgrDoc.getDocumentElement();
			String participantId = calculationPayloadElement.getAttribute("particpantId");
			persistedScoreMap.put(participantId, calculationPayloadElement);
		}

		return persistedScoreMap;
	}

	/**
	 * removeExistingPids - Removes the PIDs that have score data from the list
	 * of pids that is complete. Lifted directly from TalentGridRgrToRcmV2.
	 * 
	 * @param groupPIds
	 * @param existingRgrList
	 * @return
	 */
	private List<Integer> removeExistingPids(List<Integer> groupPIds, List<ReportRequestResponse> existingRgrList) {

		List<Integer> nonExistingPids = new ArrayList<Integer>();
		nonExistingPids.addAll(groupPIds);
		for (ReportRequestResponse rptReqResp : existingRgrList) {
			Integer persistedPid = rptReqResp.getParticipantId();
			if (nonExistingPids.contains(persistedPid)) {
				nonExistingPids.remove(persistedPid);
			}
		}

		return nonExistingPids;
	}

	/**
	 * getScores - Fetch scores from tincan. Lifted directly from
	 * TalentGridRgrToRcmV2.
	 * 
	 * @param strUrl
	 * @param payload
	 * @return
	 * @throws PalmsException
	 */
	private Document getScores(String strUrl, String payload) throws PalmsException {
		Document doc = null;
		HttpURLConnection conn = null;
		URL url = null;
		String line = "";
		String xml = "";
		String errStr = null;

		try {
			url = new URL(strUrl);
		} catch (MalformedURLException e1) {
			errStr = "Bad URL (" + strUrl + ") was passed to getScores()";
			log.error(errStr + ".  msg={}", e1.getMessage());
			throw new PalmsException(errStr, PalmsErrorCode.RCM_GEN_FAILED.getCode(), e1.getMessage());
		}

		try {
			conn = (HttpURLConnection) url.openConnection();
		} catch (IOException e2) {
			String str = "Unable to open connection on URL - " + strUrl;
			log.error(str + ".  msg= {}", e2.getMessage());
			throw new PalmsException(str, PalmsErrorCode.RCM_GEN_FAILED.getCode(), e2.getMessage());
		}

		try {
			conn.setRequestMethod("POST");
		} catch (ProtocolException e3) {
			errStr = "Invalid Protocol (POST) set for Request Method.  URL=" + strUrl;
			log.error(errStr + "  Message: {}", e3.getMessage());
			throw new PalmsException("Invalid Protocol (POST) set for Request Method.",
					PalmsErrorCode.RCM_GEN_FAILED.getCode(), e3.getMessage());
		}
		conn.setRequestProperty("Content-Type", "application/xml");
		conn.setDoOutput(true);

		OutputStreamWriter writer = null;
		try {
			writer = new OutputStreamWriter(conn.getOutputStream());

			writer.write(payload);
			writer.flush();
		} catch (IOException e4) {
			errStr = "Error writing to URL " + strUrl + ".  Payload=" + payload;
			log.error(errStr + ".  Message: {}", e4.getMessage());
			throw new PalmsException(errStr, PalmsErrorCode.RCM_GEN_FAILED.getCode(), e4.getMessage());
		}

		// Get Response
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			while ((line = in.readLine()) != null) {
				xml += line;
			}
			in.close();
		} catch (IOException e5) {
			errStr = "Error reading from URL " + strUrl + ".  Payload=" + payload;
			log.error(errStr + ".  Message: {}", e5.getMessage());
			throw new PalmsException(errStr, PalmsErrorCode.RCM_GEN_FAILED.getCode(), e5.getMessage());
		}
		try {
			writer.close();
		} catch (IOException e6) {
			errStr = "Error closing writer to URL " + strUrl;
			log.error(errStr + ".  Message: {}", e6.getMessage());
			throw new PalmsException(errStr, PalmsErrorCode.RCM_GEN_FAILED.getCode(), e6.getMessage());
		}
		conn.disconnect();

		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			doc = docBuilder.parse(new InputSource(new ByteArrayInputStream(xml.getBytes())));
		} catch (ParserConfigurationException e) {
			log.error("Parser error creating document from ext. server response. Message: {}", e.getMessage());
			throw new PalmsException("Parser error creating document from ext. server response.",
					PalmsErrorCode.RCM_GEN_FAILED.getCode(), e.getMessage());

		} catch (IOException e) {
			log.error("IO error creating document from ext. server response. Message: {}", e.getMessage());
			throw new PalmsException("IO error creating document from ext. server response.",
					PalmsErrorCode.RCM_GEN_FAILED.getCode(), e.getMessage());
		} catch (SAXException e) {
			log.error("SAX error creating document from ext. server response. Message: {}", e.getMessage());
			throw new PalmsException("SAX error creating document from ext. server response.",
					PalmsErrorCode.RCM_GEN_FAILED.getCode(), e.getMessage());
		}

		return doc;
	}

	/**
	 * saveScoresToDB - Saves calculation payload to the Report Request Response
	 * table. Lifted directly from TalentGridRgrToRcmV2.
	 * 
	 * @param scoreDoc
	 * @param type
	 * @param targLevelIndex
	 * @throws PalmsException
	 */
	private void saveScoresToDB(Document scoreDoc, String type, Integer targLevelIndex) throws PalmsException {
		log.debug("scoreDoc value is  = " + KfapUtils.getStringFromDocument(scoreDoc));
		NodeList nodes = scoreDoc.getElementsByTagName("calculationPayload");
		for (int i = 0; i < nodes.getLength(); i = i + 1) {
			Element calculationPayload = (Element) nodes.item(i);
			KfapUtils.printElemnt(calculationPayload);
			String participantId = calculationPayload.getAttribute("particpantId");
			String individualScoreElementStr = KfapUtils.getStringFromElement(calculationPayload);
			log.debug("individualScoreElementStr is  = " + individualScoreElementStr);
			this.reportResponseDao.createOrUpdate(Integer.valueOf(participantId), type, individualScoreElementStr,
					targLevelIndex);
		}
	}

	// --------------------------------------------------------------
	//
	// Texty stuff

	/**
	 * fetchExtractText - Get content data published data from the CMS. This may
	 * eventually be a private method called by the constructor. Saves the text
	 * information internally so that it can be used later.
	 * 
	 * @throws PalmsException
	 *             - Passed through from called methods
	 */
	private void fetchExtractText() throws PalmsException {

		try {
			this.jsonNode = this.contentRetriever.getGrpExtractContent(CONTENT_FOLDER, this.inParms.getLanguageCode());
		} catch (PalmsException e) {
			log.info("Fail marklogic.");
			throw new PalmsException("Failed to extract Json object from published CMS content.  msg=" + e.getMessage());
		}

		return;
	}

	// --------------------------------------------------------------
	//
	// Workbooky stuff stuff

	/**
	 * createExtractWorkbook - Controller method for the production of the
	 * extract output workbook.
	 * 
	 * @return
	 */
	private XSSFWorkbook createExtractWorkbook() {
		XSSFWorkbook wb = new XSSFWorkbook();

		// Put in the watermark
		POIXMLProperties xmlProps = ((POIXMLDocument) wb).getProperties();
		POIXMLProperties.CoreProperties coreProps = xmlProps.getCoreProperties();
		coreProps.setTitle(WATERMARK);

		popInfoSheet(wb);

		popBpSheet(wb);

		popDefSheet(wb);

		popInstSheet(wb);

		popExtSheet(wb);

		// wb.lockRevision();
		// wb.lockStructure();

		return wb;
	}

	/**
	 * popInfoSheet - Populate the "Information" sheet
	 * 
	 * @param wb
	 */
	private void popInfoSheet(XSSFWorkbook wb) {
		String txt = null;
		int wrapCnt = 0;
		int rowIdx = 0;
		int colCnt = 5;
		int fontPts;

		// Magic numbers, calculated by taking the point width of the column and
		// dividing by 0.955625
		int c0Width = 12;
		int c0aWidth = 5;
		int c1Width = 26;
		int c2Width = 49;
		int c3Width = 16; // Fudged downward a touch

		Font alpLblFont = wb.createFont();
		alpLblFont.setFontName("Arial");
		alpLblFont.setFontHeightInPoints((short) 12);
		Font titleFont = wb.createFont();
		titleFont.setFontHeightInPoints((short) 36);
		titleFont.setFontName("Arial");
		titleFont.setColor(HSSFColor.PLUM.index);
		Font persFont = wb.createFont();
		persFont.setFontHeightInPoints((short) 14);
		persFont.setFontName("Arial");
		persFont.setItalic(true);
		XSSFFont dataHdrFont = wb.createFont();
		dataHdrFont.setFontHeightInPoints((short) 10);
		dataHdrFont.setFontName("Arial");
		dataHdrFont.setBold(true);
		Font dataFont = wb.createFont();
		dataFont.setFontHeightInPoints((short) 12);
		dataFont.setFontName("Arial");
		XSSFFont aboutFont = wb.createFont();
		aboutFont.setFontHeightInPoints((short) 14);
		aboutFont.setFontName("Arial");
		aboutFont.setBold(true);
		Font dfltFont = wb.createFont();
		dfltFont.setFontHeightInPoints((short) 10);
		dfltFont.setFontName("Arial");
		Font versionFont = wb.createFont();
		versionFont.setFontHeightInPoints((short) 8);
		versionFont.setFontName("Arial");
		versionFont.setColor(HSSFColor.GREY_50_PERCENT.index);

		CellStyle logoStyle = wb.createCellStyle();
		logoStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		CellStyle alpLabelStyle = wb.createCellStyle();
		alpLabelStyle.setFont(alpLblFont);
		CellStyle titleStyle = wb.createCellStyle();
		titleStyle.setFont(titleFont);
		CellStyle persStyle = wb.createCellStyle();
		persStyle.setFont(persFont);
		CellStyle dataHdrStyle = wb.createCellStyle();
		dataHdrStyle.setFont(dataHdrFont);
		CellStyle dataStyle = wb.createCellStyle();
		dataStyle.setFont(dataFont);
		dataStyle.setWrapText(true);
		dataStyle.setVerticalAlignment(CellStyle.VERTICAL_TOP);
		CellStyle aboutStyle = wb.createCellStyle();
		aboutStyle.setFont(aboutFont);
		CellStyle dfltStyle = wb.createCellStyle();
		dfltStyle.setFont(dfltFont);
		CellStyle dfltWrapStyle = wb.createCellStyle();
		dfltWrapStyle.setFont(dfltFont);
		dfltWrapStyle.setWrapText(true);
		dfltWrapStyle.setVerticalAlignment(CellStyle.VERTICAL_TOP);
		CellStyle versionStyle = wb.createCellStyle();
		versionStyle.setFont(versionFont);

		Sheet s = wb.createSheet(this.jsonNode.get("generic").get("tabs").get("info").textValue());
		s.setDisplayGridlines(false);
		s.setColumnWidth(0, c0Width * 256); // Approximation
		s.setColumnWidth(1, c0aWidth * 256); // Approximation
		s.setColumnWidth(2, c1Width * 256); // Approximation
		s.setColumnWidth(3, c2Width * 256); // Approximation
		s.setColumnWidth(4, c3Width * 256); // Approximation
		s.setDefaultRowHeightInPoints(12.75f);

		// Play with print settings
		PrintSetup ps = s.getPrintSetup();
		s.setAutobreaks(true);
		s.setFitToPage(true);
		ps.setFitWidth((short) 1);
		ps.setFitHeight((short) 0);

		Row r0a = createRowWithDefault(s, rowIdx++, colCnt);
		r0a.setHeightInPoints(70f);

		Row r0b = createRowWithDefault(s, rowIdx++, colCnt);
		r0b.setHeightInPoints(45.75f);

		// Add picture (logo) data to this workbook.
		// it is now in header row "r0b"
		boolean hasLogo = true;
		InputStream is = null;
		byte[] bytes = null;

		// Get a line on the logo data

		is = KfalpGroupExtractHelperBean.class.getClassLoader().getResourceAsStream(LOGO_LOC);
		if (is == null) {
			log.error("Unable to access extract logo image.  None will be displayed.");
			hasLogo = false;
		}

		if (hasLogo) {
			try {
				bytes = IOUtils.toByteArray(is);
			} catch (IOException e) {
				log.error("Unable to read extract logo image.  None will be displayed.");
				hasLogo = false;
			}
		}

		if (hasLogo) {
			if (bytes == null || bytes.length == 0) {
				log.error("Logo image not fetched.  None will be displayed.");
				hasLogo = false;
			}
		}

		if (hasLogo) {
			int pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
			try {
				is.close();
			} catch (IOException e) {
				log.error("Close() failed on logo stream.  Processing continues.");
				// No need to fail this close()
			}

			CreationHelper helper = wb.getCreationHelper();

			// Create a drawing patriarch. Top level container for all shapes.
			Drawing drawing = s.createDrawingPatriarch();

			// add a picture shape
			ClientAnchor anchor = helper.createClientAnchor();
			// set top-left corner of the picture,
			// subsequent call of Picture#resize() will operate relative to it
			anchor.setCol1(1);
			anchor.setRow1(r0b.getRowNum());
			Picture pict = drawing.createPicture(anchor, pictureIdx);
			// //auto-size picture relative to its top-left corner... Seems to
			// need this to display
			pict.resize();
		} else {
			r0b.getCell(1).setCellStyle(logoStyle);
			r0b.getCell(1).setCellValue("Korn Ferry");
		}

		Row r0c = createRowWithDefault(s, rowIdx++, colCnt);
		r0c.setHeightInPoints(59f);

		Row r1 = createRowWithDefault(s, rowIdx++, colCnt);
		r1.setHeightInPoints(15.0f);
		r1.getCell(2).setCellStyle(alpLabelStyle);
		r1.getCell(2).setCellValue(this.jsonNode.get("generic").get("alpLabelUpper").textValue());

		Row r2 = createRowWithDefault(s, rowIdx++, colCnt);
		r2.setHeightInPoints(45.75f);
		r2.getCell(2).setCellStyle(titleStyle);
		r2.getCell(2).setCellValue(this.jsonNode.get("generic").get("title").textValue());

		Row r3 = createRowWithDefault(s, rowIdx++, colCnt);
		r3.setHeightInPoints(21.75f);
		r3.getCell(2).setCellStyle(persStyle);
		r3.getCell(2).setCellValue(this.jsonNode.get("generic").get("personalLabel").textValue());

		Row r4 = createRowWithDefault(s, rowIdx++, colCnt);
		r4.setHeightInPoints(41.25f);

		Row r5 = createRowWithDefault(s, rowIdx++, colCnt);
		r5.setHeightInPoints(12.75f);
		r5.getCell(2).setCellStyle(dataHdrStyle);
		r5.getCell(2).setCellValue(this.jsonNode.get("generic").get("clientLabel").textValue());
		if (hasGroup) {
			r5.getCell(3).setCellStyle(dataHdrStyle);
			r5.getCell(3).setCellValue(this.jsonNode.get("generic").get("groupLabel").textValue());
		}

		Row r6 = createRowWithDefault(s, rowIdx++, colCnt);
		r6.setHeightInPoints(15.0f);
		r6.getCell(2).setCellStyle(dataStyle);
		r6.getCell(2).setCellValue(this.clientName);
		fontPts = 12;
		wrapCnt = getWrappedLineCount(r6, this.clientName, c1Width, fontPts);
		r6.setHeightInPoints(wrapCnt * (fontPts * 1.25f));
		if (hasGroup) {
			r6.getCell(3).setCellStyle(dataStyle);
			r6.getCell(3).setCellValue(this.inParms.getGroupName());
		}

		Row r7 = createRowWithDefault(s, rowIdx++, colCnt);
		r7.setHeightInPoints(12.75f);

		Row r8 = createRowWithDefault(s, rowIdx++, colCnt);
		r8.setHeightInPoints(12.75f);
		r8.getCell(2).setCellStyle(dataHdrStyle);
		r8.getCell(2).setCellValue(this.jsonNode.get("generic").get("targetLabel").textValue());
		r8.getCell(3).setCellStyle(dataHdrStyle);
		r8.getCell(3).setCellValue(this.jsonNode.get("generic").get("dateLabel").textValue());

		Row r9 = createRowWithDefault(s, rowIdx++, colCnt);
		r9.getCell(2).setCellStyle(dataStyle);
		r9.getCell(2).setCellValue(this.targLabel);
		fontPts = 12;
		wrapCnt = getWrappedLineCount(r9, this.targLabel, c1Width, fontPts);
		r9.setHeightInPoints(wrapCnt * (fontPts * 1.25f));
		r9.getCell(3).setCellStyle(dataStyle);
		r9.getCell(3).setCellValue(this.coverDate);

		Row r10 = createRowWithDefault(s, rowIdx++, colCnt);
		r10.setHeightInPoints(159.75f);

		Row r11 = createRowWithDefault(s, rowIdx++, colCnt);
		r11.setHeightInPoints(18.0f);
		r11.getCell(2).setCellStyle(aboutStyle);
		r11.getCell(2).setCellValue(this.jsonNode.get("generic").get("aboutKornFerry").get("title").textValue());

		Row r12 = createRowWithDefault(s, rowIdx++, colCnt);
		r12.getCell(2).setCellStyle(dfltWrapStyle);
		r12.getCell(3).setCellStyle(dfltWrapStyle);
		s.addMergedRegion(new CellRangeAddress(r12.getRowNum(), r12.getRowNum(), 2, 3));
		txt = this.jsonNode.get("generic").get("aboutKornFerry").get("endpageP1").textValue();
		r12.getCell(2).setCellValue(txt);
		wrapCnt = getWrappedLineCount(r12, txt, c1Width + c2Width);
		r12.setHeightInPoints((wrapCnt * s.getDefaultRowHeightInPoints()) + 4);

		Row r13 = createRowWithDefault(s, rowIdx++, colCnt);
		r13.getCell(2).setCellStyle(dfltWrapStyle);
		r13.getCell(3).setCellStyle(dfltWrapStyle);
		s.addMergedRegion(new CellRangeAddress(r13.getRowNum(), r13.getRowNum(), 2, 3));
		txt = this.jsonNode.get("generic").get("aboutKornFerry").get("endpageP2").textValue();
		r13.getCell(2).setCellValue(txt);
		wrapCnt = getWrappedLineCount(r13, txt, c1Width + c2Width);
		r13.setHeightInPoints((wrapCnt * s.getDefaultRowHeightInPoints()) + 4);

		Row r14 = createRowWithDefault(s, rowIdx++, colCnt);
		r14.getCell(2).setCellStyle(dfltWrapStyle);
		r14.getCell(3).setCellStyle(dfltWrapStyle);
		s.addMergedRegion(new CellRangeAddress(r14.getRowNum(), r14.getRowNum(), 2, 3));
		txt = this.jsonNode.get("generic").get("aboutKornFerry").get("endpageP3").textValue();
		r14.getCell(2).setCellValue(txt);
		wrapCnt = getWrappedLineCount(r14, txt, c1Width + c2Width);
		r14.setHeightInPoints((wrapCnt * s.getDefaultRowHeightInPoints()) + 4);

		Row r15 = createRowWithDefault(s, rowIdx++, colCnt);
		r15.getCell(2).setCellStyle(dfltWrapStyle);
		r15.getCell(3).setCellStyle(dfltWrapStyle);
		s.addMergedRegion(new CellRangeAddress(r15.getRowNum(), r15.getRowNum(), 2, 3));
		r15.getCell(2).setCellValue(this.jsonNode.get("generic").get("aboutKornFerry").get("copyright").textValue());

		Row r16 = createRowWithDefault(s, rowIdx++, colCnt);
		r16.setHeightInPoints(12.75f);

		Row r17 = createRowWithDefault(s, rowIdx++, colCnt);
		r17.getCell(2).setCellStyle(versionStyle);
		r17.getCell(3).setCellStyle(versionStyle);
		s.addMergedRegion(new CellRangeAddress(r17.getRowNum(), r17.getRowNum(), 2, 3));
		r17.getCell(2).setCellValue(this.jsonNode.get("generic").get("version").textValue());
		// s.protectSheet(SHEET_PW);
	}

	/**
	 * createRowWithDefault - creates a row with a default style of grey cells,
	 * then create teh requisite number of sells in white in the row.
	 * 
	 * @param s
	 * @param idx
	 * @param noCells
	 * @return
	 */
	private Row createRowWithDefault(Sheet s, int idx, int noCells) {
		Row r = s.createRow(idx);
		// r.setRowStyle(this.dcs);
		for (int i = 0; i < noCells; i++) {
			r.createCell(i);
		}
		return r;
	}

	/**
	 * popBpSheet - Populate the "Best Practices" sheet
	 * 
	 * @param wb
	 */
	private void popBpSheet(XSSFWorkbook wb) {
		// Magic numbers, calculated by taking the point width of the column and
		// dividing by 0.955625
		int c0Width = 5;
		int c1Width = 3;
		int c2Width = 96;
		int c3Width = 4; // Fudged downward a touch
		String txt = null;
		int wrapCnt = 0;
		int rowIdx = 0;
		int colCnt = 4;

		Sheet s = wb.createSheet(this.jsonNode.get("generic").get("tabs").get("bestPractices").textValue());
		s.setDisplayGridlines(false);
		s.setColumnWidth(0, c0Width * 256); // Approximation
		s.setColumnWidth(1, c1Width * 256); // Approximation
		s.setColumnWidth(2, c2Width * 256); // Approximation
		s.setColumnWidth(3, c3Width * 256); // Approximation
		s.setDefaultRowHeightInPoints(12.75f);

		// Play with print settings
		PrintSetup ps = s.getPrintSetup();
		s.setAutobreaks(true);
		s.setFitToPage(true);
		ps.setFitWidth((short) 1);
		ps.setFitHeight((short) 0);

		// Set up the needed fonts and styles
		Font titleFont = wb.createFont();
		titleFont.setFontName("Arial");
		titleFont.setFontHeightInPoints((short) 24);
		titleFont.setColor(HSSFColor.PLUM.index);
		Font hdrFont = wb.createFont();
		hdrFont.setFontName("Arial");
		hdrFont.setFontHeightInPoints((short) 14);
		hdrFont.setColor(HSSFColor.PLUM.index);
		Font bodyFont = wb.createFont();
		bodyFont.setFontName("Arial");
		bodyFont.setFontHeightInPoints((short) 10);

		Font bodyItalicFont = wb.createFont();
		bodyItalicFont.setFontName("Arial");
		bodyItalicFont.setFontHeightInPoints((short) 10);
		bodyItalicFont.setItalic(true);
		XSSFFont bulletFont = wb.createFont();
		bulletFont.setFontName("Arial");
		bulletFont.setFontHeightInPoints((short) 10);
		bulletFont.setBold(true);
		XSSFFont sugTitleFont = wb.createFont();
		sugTitleFont.setFontName("Arial");
		sugTitleFont.setFontHeightInPoints((short) 10);
		sugTitleFont.setBold(true);

		CellStyle titleStyle = wb.createCellStyle();
		titleStyle.setFont(titleFont);
		CellStyle hdrStyle = wb.createCellStyle();
		hdrStyle.setFont(hdrFont);
		hdrStyle.setWrapText(true);
		CellStyle bodyStyle = wb.createCellStyle();
		bodyStyle.setVerticalAlignment(CellStyle.VERTICAL_TOP);
		bodyStyle.setFont(bodyFont);
		bodyStyle.setWrapText(true);
		CellStyle bulletStyle = wb.createCellStyle();
		bulletStyle.setVerticalAlignment(CellStyle.VERTICAL_TOP);
		bulletStyle.setAlignment(CellStyle.ALIGN_CENTER);
		bulletStyle.setFont(bulletFont);
		CellStyle sugTitleStyle = wb.createCellStyle();
		sugTitleStyle.setFont(sugTitleFont);
		sugTitleStyle.setWrapText(true);

		// Populate

		Row r0 = createRowWithDefault(s, rowIdx++, colCnt);
		r0.setHeightInPoints(27.0f);

		Row r1 = createRowWithDefault(s, rowIdx++, colCnt);
		r1.setHeightInPoints(30.0f);
		r1.getCell(1).setCellStyle(titleStyle);
		r1.getCell(1).setCellValue(this.jsonNode.get("bestPractices").get("title").textValue());

		Row r2 = createRowWithDefault(s, rowIdx++, colCnt);
		r2.setHeightInPoints(18.0f);
		r2.getCell(1).setCellStyle(hdrStyle);
		r2.getCell(2).setCellStyle(hdrStyle);
		s.addMergedRegion(new CellRangeAddress(2, 2, 1, 2));
		r2.getCell(1).setCellValue(this.jsonNode.get("bestPractices").get("usageSection").get("title").textValue());

		Row r3 = createRowWithDefault(s, rowIdx++, colCnt);
		r3.getCell(1).setCellStyle(bodyStyle);
		r3.getCell(2).setCellStyle(bodyStyle);
		s.addMergedRegion(new CellRangeAddress(3, 3, 1, 2));
		txt = this.jsonNode.get("bestPractices").get("usageSection").get("usageContentP1").textValue();
		RichTextString rts = formatEmbeddedItalics(txt, bodyFont, bodyItalicFont);
		r3.getCell(1).setCellValue(rts);
		wrapCnt = getWrappedLineCount(r3, txt, c1Width + c2Width);
		r3.setHeightInPoints(wrapCnt * s.getDefaultRowHeightInPoints());

		Row r4 = createRowWithDefault(s, rowIdx++, colCnt);
		r4.getCell(1).setCellStyle(bodyStyle);
		r4.getCell(2).setCellStyle(bodyStyle);
		s.addMergedRegion(new CellRangeAddress(4, 4, 1, 2));
		txt = this.jsonNode.get("bestPractices").get("usageSection").get("usageContentP2").textValue();
		r4.getCell(1).setCellValue(txt);
		wrapCnt = getWrappedLineCount(r4, txt, c1Width + c2Width);
		r4.setHeightInPoints(wrapCnt * s.getDefaultRowHeightInPoints());

		Row r5 = createRowWithDefault(s, rowIdx++, colCnt);
		r5.getCell(1).setCellStyle(sugTitleStyle);
		r5.getCell(2).setCellStyle(sugTitleStyle);
		s.addMergedRegion(new CellRangeAddress(5, 5, 1, 2));
		txt = this.jsonNode.get("bestPractices").get("usageSection").get("suggestionsTitle").textValue();
		r5.getCell(1).setCellValue(txt);
		wrapCnt = getWrappedLineCount(r5, txt, c1Width + c2Width);
		r5.setHeightInPoints(wrapCnt * s.getDefaultRowHeightInPoints());

		boolean done = false;
		int i = 1; // Start at 1 since bullet1 is the first
		while (!done) {
			// Creates rows for all of the bullets. NOTE: This logic assumes
			// contiguous ascending numbers on the bullets. If there is a gap,
			// this routine will exit early and subsequent bullets will not be
			// displayed.
			String key = "bullet" + i++;
			JsonNode jn = this.jsonNode.get("bestPractices").get("suggestions").get(key);
			if (jn == null) {
				done = true;
				continue;
			}
			txt = jn.textValue();
			Row bulletRow = createRowWithDefault(s, rowIdx++, colCnt);
			bulletRow.getCell(1).setCellStyle(bulletStyle);
			bulletRow.getCell(1).setCellValue(BULLET);
			bulletRow.getCell(2).setCellStyle(bodyStyle);
			bulletRow.getCell(2).setCellValue(txt);
			wrapCnt = getWrappedLineCount(bulletRow, txt, c2Width);
			bulletRow.setHeightInPoints((wrapCnt * s.getDefaultRowHeightInPoints()) + 3);
		}

		// s.protectSheet(SHEET_PW);
	}

	/**
	 * formatEmbeddedItalics - The Best Practices page has embedded Italicized
	 * text in with the regular text. This method finds the text (delimited by a
	 * single asterisk - "*") and formats a rich text string with the italicized
	 * font.
	 * 
	 * NOTE: This works only if there are no other instances of an asterisk in
	 * the text, and has not been tried with others format enhancing methods. If
	 * it is ever run in conjunction with the formatEmbeddedBold() method found
	 * elsewhere in this code, it should be run last as the bold formating is a
	 * series of 2 asterisks ("**") which would confuse this routine.
	 * 
	 * @param txt
	 * @param reg
	 * @param bold
	 * @return
	 */
	private RichTextString formatEmbeddedItalics(String txt, Font reg, Font bold) {
		XSSFRichTextString ret = new XSSFRichTextString();
		List<Integer> hits = new ArrayList<Integer>();
		StringBuilder nuTxt = new StringBuilder();

		// scan the text for formatting. "*" is an ITALIC delimiter
		boolean done = false;
		int prv = 0;
		while (!done) {
			int idx = txt.indexOf("*", prv);
			if (idx == -1) {
				nuTxt.append(txt.substring(prv));
				done = true;
				continue;
			}
			// Got one...
			nuTxt.append(txt.substring(prv, idx));
			hits.add(nuTxt.length());
			prv = idx + 1;
		}

		// create the string
		ret = new XSSFRichTextString(nuTxt.toString());
		ret.applyFont(reg);

		// format it, but only if we need to
		if (hits.size() > 0) {
			if (hits.size() % 2 != 0) {
				hits.add(nuTxt.length());
			}
			for (int i = 0; i < hits.size(); i += 2) {
				ret.applyFont(hits.get(i), hits.get(i + 1), bold);
			}
		}

		return ret;
	}

	/**
	 * popDefSheet - Populate the "Definitions" sheet
	 * 
	 * @param wb
	 */
	private void popDefSheet(XSSFWorkbook wb) {
		List<String> keys = new ArrayList<String>(7);
		keys.add("drivers");
		keys.add("experience");
		keys.add("awareness");
		keys.add("learningAgility");
		keys.add("leadershipTraits");
		keys.add("capacity");
		keys.add("derailmentRisks");

		// Magic numbers, calculated by taking the point width of the column and
		// dividing by 0.955625
		int c0Width = 5;
		int c1Width = 99;
		int c2Width = 4; // Fudged downward a touch
		int wrapCnt;
		String txt;
		int rowIdx = 0;
		int colCnt = 3;

		Sheet s = wb.createSheet(this.jsonNode.get("generic").get("tabs").get("definitions").textValue());
		s.setDisplayGridlines(false);
		s.setColumnWidth(0, c0Width * 256); // Approximation
		s.setColumnWidth(1, c1Width * 256); // Approximation
		s.setColumnWidth(2, c2Width * 256); // Approximation
		s.setDefaultRowHeightInPoints(12.75f);

		// Play with print settings
		PrintSetup ps = s.getPrintSetup();
		s.setAutobreaks(true);
		s.setFitToPage(true);
		ps.setFitWidth((short) 1);
		ps.setFitHeight((short) 0);

		// Set up the needed fonts and styles
		Font titleFont = wb.createFont();
		titleFont.setFontName("Arial");
		titleFont.setFontHeightInPoints((short) 24);
		titleFont.setColor(HSSFColor.PLUM.index);
		Font hdrFont = wb.createFont();
		hdrFont.setFontName("Arial");
		hdrFont.setFontHeightInPoints((short) 14);
		hdrFont.setColor(HSSFColor.PLUM.index);
		Font bodyFont = wb.createFont();
		bodyFont.setFontName("Arial");
		bodyFont.setFontHeightInPoints((short) 10);
		XSSFFont bodyBoldFont = wb.createFont();
		bodyBoldFont.setFontName("Arial");
		bodyBoldFont.setFontHeightInPoints((short) 10);
		bodyBoldFont.setBold(true);

		CellStyle titleStyle = wb.createCellStyle();
		titleStyle.setFont(titleFont);
		CellStyle hdrStyle = wb.createCellStyle();
		hdrStyle.setFont(hdrFont);
		CellStyle bodyStyle = wb.createCellStyle();
		bodyStyle.setVerticalAlignment(CellStyle.VERTICAL_TOP);
		bodyStyle.setFont(bodyFont);
		bodyStyle.setWrapText(true);

		// Populate

		Row r0 = createRowWithDefault(s, rowIdx++, colCnt);
		r0.setHeightInPoints(27.0f);

		Row r1 = createRowWithDefault(s, rowIdx++, colCnt);
		r1.setHeightInPoints(30.0f);
		r1.getCell(1).setCellStyle(titleStyle);
		r1.getCell(1).setCellValue(this.jsonNode.get("definitions").get("title").textValue());

		// populate the signposts (sub-headers and the text that goes with it)
		for (int i = 0; i < keys.size(); i++) {
			String key = keys.get(i);
			Row rhdr = createRowWithDefault(s, rowIdx++, colCnt);
			rhdr.setHeightInPoints(18.0f);
			rhdr.getCell(1).setCellStyle(hdrStyle);
			rhdr.getCell(1).setCellValue(
					this.jsonNode.get("definitions").get("sevenSignposts").get(key).get("title").textValue());

			// Call the Rich Text formatter because we know there is rich text
			// to format
			Row rdat = createRowWithDefault(s, rowIdx++, colCnt);
			rdat.setHeightInPoints(12.75f);
			rdat.getCell(1).setCellStyle(bodyStyle);
			txt = this.jsonNode.get("definitions").get("sevenSignposts").get(key).get("longDesc").textValue();
			RichTextString rts = formatEmbeddedBold(txt, bodyFont, bodyBoldFont);
			rdat.getCell(1).setCellValue(rts);
			wrapCnt = getWrappedLineCount(rdat, txt, c1Width);
			rdat.setHeightInPoints(wrapCnt * s.getDefaultRowHeightInPoints());

			Row rftr = createRowWithDefault(s, rowIdx++, colCnt);
			rftr.setHeightInPoints(13.5f);
		} // End loop through keys

		// s.protectSheet(SHEET_PW);
	}

	/**
	 * formatEmbeddedBold - The Definitions page has embedded Bold text amongst
	 * the regular text. This method finds the text (delimited by a pir of
	 * asterisks - "**") and formats a rich text string with the bold font type
	 * for that span of characters.
	 * 
	 * NOTE: This works only if there are no other instances of an asterisk in
	 * the text, and has not been tried with others format enhancing methods. If
	 * it is ever run in conjunction with the formatEmbeddedItalics() method
	 * found elsewhere in this code, it should be run first as the bold
	 * formating is a series of 2 asterisks ("**") while the italics is a single
	 * asterisk ("*"); the double asterisk sequence would cause the italics
	 * method to format incorrectly if not processed out first.
	 * 
	 * @param txt
	 * @param reg
	 * @param bold
	 * @return
	 */
	private RichTextString formatEmbeddedBold(String txt, Font reg, Font bold) {
		XSSFRichTextString ret = new XSSFRichTextString();
		List<Integer> bolds = new ArrayList<Integer>();
		StringBuilder nuTxt = new StringBuilder();

		// scan the text for formatting. "**" ia a BOLD delimiter
		boolean done = false;
		int prv = 0;
		while (!done) {
			int idx = txt.indexOf("**", prv);
			if (idx == -1) {
				nuTxt.append(txt.substring(prv));
				done = true;
				continue;
			}
			// Got one...
			nuTxt.append(txt.substring(prv, idx));
			bolds.add(nuTxt.length());
			prv = idx + 2;
		}

		// create the string
		ret = new XSSFRichTextString(nuTxt.toString());
		ret.applyFont(reg);

		// format it, but only if we need to
		if (bolds.size() > 0) {
			if (bolds.size() % 2 != 0) {
				bolds.add(nuTxt.length());
			}
			for (int i = 0; i < bolds.size(); i += 2) {
				ret.applyFont(bolds.get(i), bolds.get(i + 1), bold);
			}
		}

		return ret;
	}

	/**
	 * popInstSheet - Populate the "Instructions" sheet
	 * 
	 * @param wb
	 */
	private void popInstSheet(XSSFWorkbook wb) {
		String txt = null;
		int wrapCnt;
		int rowIdx = 0;
		int colCnt = 4;

		// Magic numbers, calculated by taking the point width of the column and
		// dividing by 0.955625
		int c0Width = 5;
		int c1Width = 3;
		int c2Width = 96;
		int c3Width = 4; // Fudged downward a touch

		Sheet s = wb.createSheet(this.jsonNode.get("generic").get("tabs").get("instructions").textValue());
		s.setDisplayGridlines(false);
		s.setColumnWidth(0, c0Width * 256); // Approximation
		s.setColumnWidth(1, c1Width * 256); // Approximation
		s.setColumnWidth(2, c2Width * 256); // Approximation
		s.setColumnWidth(3, c3Width * 256); // Approximation

		// Play with print settings
		PrintSetup ps = s.getPrintSetup();
		s.setAutobreaks(true);
		s.setFitToPage(true);
		ps.setFitWidth((short) 1);
		ps.setFitHeight((short) 0);

		// Set up the needed fonts and styles
		Font titleFont = wb.createFont();
		titleFont.setFontName("Arial");
		titleFont.setFontHeightInPoints((short) 24);
		titleFont.setColor(HSSFColor.PLUM.index);
		Font hdrFont = wb.createFont();
		hdrFont.setFontName("Arial");
		hdrFont.setFontHeightInPoints((short) 14);
		hdrFont.setColor(HSSFColor.PLUM.index);
		Font bodyFont = wb.createFont();
		bodyFont.setFontName("Arial");
		bodyFont.setFontHeightInPoints((short) 10);
		XSSFFont bulletFont = wb.createFont();
		bulletFont.setFontName("Arial");
		bulletFont.setFontHeightInPoints((short) 10);
		bulletFont.setBold(true);

		CellStyle titleStyle = wb.createCellStyle();
		titleStyle.setFont(titleFont);
		CellStyle hdrStyle = wb.createCellStyle();
		hdrStyle.setFont(hdrFont);
		CellStyle bodyStyle = wb.createCellStyle();
		bodyStyle.setVerticalAlignment(CellStyle.VERTICAL_TOP);
		bodyStyle.setFont(bodyFont);
		bodyStyle.setWrapText(true);
		CellStyle bulletStyle = wb.createCellStyle();
		bulletStyle.setVerticalAlignment(CellStyle.VERTICAL_TOP);
		bulletStyle.setAlignment(CellStyle.ALIGN_CENTER);
		bulletStyle.setFont(bulletFont);

		// Populate

		Row r0 = createRowWithDefault(s, rowIdx++, colCnt);
		r0.setHeightInPoints(27.0f);

		Row r1 = createRowWithDefault(s, rowIdx++, colCnt);
		r1.setHeightInPoints(30.0f);
		r1.getCell(1).setCellStyle(titleStyle);
		r1.getCell(1).setCellValue(this.jsonNode.get("instructions").get("title").textValue());

		Row r2 = createRowWithDefault(s, rowIdx++, colCnt);
		r2.setHeightInPoints(18.0f);
		r2.getCell(1).setCellStyle(hdrStyle);
		r2.getCell(1)
				.setCellValue(this.jsonNode.get("instructions").get("signposts").get("signpostsTitle").textValue());

		Row r3 = createRowWithDefault(s, rowIdx++, colCnt);
		r3.setHeightInPoints(12.75f);
		r3.getCell(1).setCellStyle(bodyStyle);
		r3.getCell(2).setCellStyle(bodyStyle);
		s.addMergedRegion(new CellRangeAddress(3, 3, 1, 2));
		txt = this.jsonNode.get("instructions").get("signposts").get("signpostsContent").textValue();
		r3.getCell(1).setCellValue(txt);
		wrapCnt = getWrappedLineCount(r3, txt, c1Width + c2Width);
		r3.setHeightInPoints(wrapCnt * s.getDefaultRowHeightInPoints());

		// Signpost content bullets
		boolean done = false;
		int i = 1; // Start at 1 since bullet1 is the first
		while (!done) {
			// Creates rows for all of the bullets. NOTE: This logic assumes
			// contiguous ascending numbers on the bullets. If there is a gap,
			// this routine will exit early and subsequent bullets will not be
			// displayed.
			String key = "bullet" + i++;
			JsonNode jn = this.jsonNode.get("instructions").get("signposts").get(key);
			if (jn == null) {
				done = true;
				continue;
			}
			txt = jn.textValue();
			Row bulletRow = createRowWithDefault(s, rowIdx++, colCnt);
			bulletRow.getCell(1).setCellStyle(bulletStyle);
			bulletRow.getCell(1).setCellValue(BULLET);
			bulletRow.getCell(2).setCellStyle(bodyStyle);
			bulletRow.getCell(2).setCellValue(txt);
			wrapCnt = getWrappedLineCount(bulletRow, txt, c2Width);
			bulletRow.setHeightInPoints(wrapCnt * s.getDefaultRowHeightInPoints());
		}

		if (this.ravensValue == 1) {
			Row rav = createRowWithDefault(s, rowIdx++, colCnt);
			rav.setHeightInPoints(12.75f);
			rav.getCell(1).setCellStyle(bodyStyle);
			rav.getCell(2).setCellStyle(bodyStyle);
			s.addMergedRegion(new CellRangeAddress(rav.getRowNum(), rav.getRowNum(), 1, 2));
			txt = this.jsonNode.get("instructions").get("noRavens").textValue();
			rav.getCell(1).setCellValue(txt);
			wrapCnt = getWrappedLineCount(rav, txt, c1Width + c2Width);
			rav.setHeightInPoints(wrapCnt * s.getDefaultRowHeightInPoints());
		}

		Row rSpc1 = createRowWithDefault(s, rowIdx++, colCnt);
		rSpc1.setHeightInPoints(13.5f);

		Row rRngT = createRowWithDefault(s, rowIdx++, colCnt);
		rRngT.setHeightInPoints(18.0f);
		rRngT.getCell(1).setCellStyle(hdrStyle);
		rRngT.getCell(1).setCellValue(this.jsonNode.get("instructions").get("ranges").get("rangesTitle").textValue());

		// Do ranges data... first the Drivers, Experience, Awareness ranges
		rowIdx = doRangeSection(s, rowIdx, colCnt, bodyStyle, bulletStyle, c1Width, "driversExpAwarTitle", c2Width,
				"driversExpAwarBullet");
		rowIdx = doRangeSection(s, rowIdx, colCnt, bodyStyle, bulletStyle, c1Width, "learnAgilityTraitsCapTitle",
				c2Width, "learnAgilityTraitsCapBullet");
		rowIdx = doRangeSection(s, rowIdx, colCnt, bodyStyle, bulletStyle, c1Width, "derailRisksTitle", c2Width,
				"derailRisksBullet");

		Row rSpc2 = createRowWithDefault(s, rowIdx++, colCnt);
		rSpc2.setHeightInPoints(13.5f);

		// Subdimensions
		Row rSubdt = createRowWithDefault(s, rowIdx++, colCnt);
		rSubdt.setHeightInPoints(18.0f);
		rSubdt.getCell(1).setCellStyle(hdrStyle);
		rSubdt.getCell(1).setCellValue(
				this.jsonNode.get("instructions").get("subDimensions").get("subDimTitle").textValue());

		Row rSdtxt = createRowWithDefault(s, rowIdx++, colCnt);
		rSdtxt.setHeightInPoints(12.75f);
		rSdtxt.getCell(1).setCellStyle(bodyStyle);
		rSdtxt.getCell(2).setCellStyle(bodyStyle);
		s.addMergedRegion(new CellRangeAddress(rSdtxt.getRowNum(), rSdtxt.getRowNum(), 1, 2));
		txt = this.jsonNode.get("instructions").get("subDimensions").get("subDimContent").textValue();
		rSdtxt.getCell(1).setCellValue(txt);
		wrapCnt = getWrappedLineCount(rSdtxt, txt, c1Width + c2Width);
		rSdtxt.setHeightInPoints(wrapCnt * s.getDefaultRowHeightInPoints());

		if (mixedV1andV2) {
			Row mixed = createRowWithDefault(s, rowIdx++, colCnt);
			mixed.setHeightInPoints(12.75f);
			mixed.getCell(1).setCellStyle(bodyStyle);
			mixed.getCell(2).setCellStyle(bodyStyle);
			s.addMergedRegion(new CellRangeAddress(mixed.getRowNum(), mixed.getRowNum(), 1, 2));
			txt = this.jsonNode.get("instructions").get("someVer1").textValue();
			mixed.getCell(1).setCellValue(txt);
			wrapCnt = getWrappedLineCount(mixed, txt, c1Width + c2Width);
			mixed.setHeightInPoints(wrapCnt * s.getDefaultRowHeightInPoints());
		}

		Row Spc3 = createRowWithDefault(s, rowIdx++, colCnt);
		Spc3.setHeightInPoints(13.5f);

		Row soTtl = createRowWithDefault(s, rowIdx++, colCnt);
		soTtl.setHeightInPoints(18.0f);
		soTtl.getCell(1).setCellStyle(hdrStyle);
		soTtl.getCell(1).setCellValue(
				this.jsonNode.get("instructions").get("sortOrder").get("sortOrderTitle").textValue());

		Row soCnt = createRowWithDefault(s, rowIdx++, colCnt);
		soCnt.setHeightInPoints(12.75f);
		soCnt.getCell(1).setCellStyle(bodyStyle);
		soCnt.getCell(2).setCellStyle(bodyStyle);
		s.addMergedRegion(new CellRangeAddress(soCnt.getRowNum(), soCnt.getRowNum(), 1, 2));
		txt = this.jsonNode.get("instructions").get("sortOrder").get("sortOrderContent").textValue();
		soCnt.getCell(1).setCellValue(txt);
		wrapCnt = getWrappedLineCount(soCnt, txt, c1Width + c2Width);
		soCnt.setHeightInPoints(wrapCnt * s.getDefaultRowHeightInPoints());

		// Sort order bullets
		done = false;
		i = 1; // Start at 1 since bullet1 is the first
		while (!done) {
			// Creates rows for all of the bullets. NOTE: This logic assumes
			// contiguous ascending numbers on the bullets. If there is a gap,
			// this routine will exit early and subsequent bullets will not be
			// displayed.
			String key = "order" + i++;
			JsonNode jn = this.jsonNode.get("instructions").get("sortOrder").get(key);
			if (jn == null) {
				done = true;
				continue;
			}
			txt = jn.textValue();
			Row bulletRow = createRowWithDefault(s, rowIdx++, colCnt);
			bulletRow.getCell(1).setCellStyle(bulletStyle);
			bulletRow.getCell(1).setCellValue(BULLET);
			bulletRow.getCell(2).setCellStyle(bodyStyle);
			bulletRow.getCell(2).setCellValue(txt);
			wrapCnt = getWrappedLineCount(bulletRow, txt, c2Width);
			bulletRow.setHeightInPoints(wrapCnt * s.getDefaultRowHeightInPoints());
		}

		// s.protectSheet(SHEET_PW);
	}

	/**
	 * doRangeSection - Put out one of the "Ranges" sections on the Instructions
	 * page
	 * 
	 * @param s
	 * @param rowIdx
	 * @param colCnt
	 * @param txtStyle
	 * @param bulletStyle
	 * @param c1Width
	 * @param secTitleKey
	 * @param c2Width
	 * @param bulletKey
	 * @return
	 */
	private int doRangeSection(Sheet s, int rowIdx, int colCnt, CellStyle txtStyle, CellStyle bulletStyle, int c1Width,
			String secTitleKey, int c2Width, String bulletKey) {

		Row rTtl = createRowWithDefault(s, rowIdx++, colCnt);
		rTtl.setHeightInPoints(12.75f);
		rTtl.getCell(1).setCellStyle(txtStyle);
		rTtl.getCell(2).setCellStyle(txtStyle);
		s.addMergedRegion(new CellRangeAddress(rTtl.getRowNum(), rTtl.getRowNum(), 1, 2));
		String txt = this.jsonNode.get("instructions").get("ranges").get(secTitleKey).textValue();
		rTtl.getCell(1).setCellValue(txt);
		int wrapCnt = getWrappedLineCount(rTtl, txt, c1Width + c2Width);
		rTtl.setHeightInPoints(wrapCnt * s.getDefaultRowHeightInPoints());

		// Ranges content bullets
		boolean done = false;
		int i = 1; // Start at 1 since bullet1 is the first
		while (!done) {
			// Creates rows for all of the bullets.
			// NOTE: This logic assumes contiguous ascending numbers on the
			// bullets. If there is a gap, this routine will exit early and
			// subsequent bullets will not be displayed.
			String key = bulletKey + i++;
			JsonNode jn = this.jsonNode.get("instructions").get("ranges").get(key);
			if (jn == null) {
				done = true;
				continue;
			}
			txt = jn.textValue();
			Row bulletRow = createRowWithDefault(s, rowIdx++, colCnt);
			bulletRow.getCell(1).setCellStyle(bulletStyle);
			bulletRow.getCell(1).setCellValue(BULLET);
			bulletRow.getCell(2).setCellStyle(txtStyle);
			bulletRow.getCell(2).setCellValue(txt);
			wrapCnt = getWrappedLineCount(bulletRow, txt, c2Width);
			bulletRow.setHeightInPoints(wrapCnt * s.getDefaultRowHeightInPoints());
		}

		return rowIdx;
	}

	/**
	 * popExtSheet - Populate the "Extract" sheet. This sheet doesn't set up the
	 * background info
	 * 
	 * @param wb
	 */
	private void popExtSheet(XSSFWorkbook wb) {
		// Set up some BG colors
		XSSFColor palePlum = new XSSFColor(new java.awt.Color(244, 230, 242));
		XSSFColor paleGrey = new XSSFColor(new java.awt.Color(231, 233, 232));

		// Set up the needed fonts and styles
		XSSFFont plumBoldFont = wb.createFont();
		plumBoldFont.setFontName("Arial");
		plumBoldFont.setFontHeightInPoints((short) 10);
		plumBoldFont.setColor(HSSFColor.PLUM.index);
		plumBoldFont.setBold(true);
		XSSFFont blackBoldFont = wb.createFont();
		blackBoldFont.setFontName("Arial");
		blackBoldFont.setFontHeightInPoints((short) 10);
		blackBoldFont.setColor(HSSFColor.BLACK.index);
		blackBoldFont.setBold(true);
		Font regFont = wb.createFont();
		regFont.setFontName("Arial");
		regFont.setFontHeightInPoints((short) 10);
		regFont.setColor(HSSFColor.BLACK.index);
		// Header cell styles
		CellStyle plumBoldStyle = wb.createCellStyle();
		plumBoldStyle.setFont(plumBoldFont);
		CellStyle plumBoldRotStyle = wb.createCellStyle();
		plumBoldRotStyle.setFont(plumBoldFont);
		plumBoldRotStyle.setRotation((short) 45);
		CellStyle blackBoldRotStyle = wb.createCellStyle();
		blackBoldRotStyle.setFont(blackBoldFont);
		blackBoldRotStyle.setRotation((short) 45);
		CellStyle blackRegRotStyle = wb.createCellStyle();
		blackRegRotStyle.setFont(regFont);
		blackRegRotStyle.setRotation((short) 45);
		// Data cell styles
		CellStyle blackRegStyle = wb.createCellStyle();
		blackRegStyle.setFont(regFont);
		CellStyle blackBoldCenterStyle = wb.createCellStyle();
		blackBoldCenterStyle.setFont(blackBoldFont);
		blackBoldCenterStyle.setAlignment(CellStyle.ALIGN_CENTER);
		XSSFCellStyle blackBoldPlumStyle = wb.createCellStyle();
		blackBoldPlumStyle.setFont(blackBoldFont);
		blackBoldPlumStyle.setFillForegroundColor(palePlum);
		blackBoldPlumStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		XSSFCellStyle blackRegGreyStyle = wb.createCellStyle();
		blackRegGreyStyle.setFont(regFont);
		blackRegGreyStyle.setFillForegroundColor(paleGrey);
		blackRegGreyStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

		/*
		 *	Map associating related header information
		 *	1. key 1
		 *	2. key 2
		 *	3. cell style 
		 */
		Object[][] hdrData = new Object[][] { (new Object[] { "participantFirstName", null, plumBoldStyle }),
				(new Object[] { "participantLastName", null, plumBoldStyle }),
				(new Object[] { "targetLevelLabel", null, plumBoldStyle }),
				(new Object[] { "setupDate", null, plumBoldStyle }),
				(new Object[] { "projectName", null, plumBoldStyle }),
				(new Object[] { "optional1", null, plumBoldStyle }),
				(new Object[] { "optional2", null, plumBoldStyle }),
				(new Object[] { "optional3", null, plumBoldStyle }),
				(new Object[] { "originalSort", null, blackBoldRotStyle }),
				(new Object[] { "driversLabels", "driversSignpost", plumBoldRotStyle }),
				(new Object[] { "driversLabels", "adRange", blackRegRotStyle }),
				(new Object[] { "driversLabels", "adScore", blackRegRotStyle }),
				(new Object[] { "driversLabels", "cpRange", blackRegRotStyle }),
				(new Object[] { "driversLabels", "cpScore", blackRegRotStyle }),
				(new Object[] { "driversLabels", "rpRange", blackRegRotStyle }),
				(new Object[] { "driversLabels", "rpScore", blackRegRotStyle }),
				(new Object[] { "experienceLabels", "expSignpost", plumBoldRotStyle }),
				(new Object[] { "experienceLabels", "ceRange", blackRegRotStyle }),
				(new Object[] { "experienceLabels", "ceScore", blackRegRotStyle }),
				(new Object[] { "experienceLabels", "peRange", blackRegRotStyle }),
				(new Object[] { "experienceLabels", "peScore", blackRegRotStyle }),
				(new Object[] { "experienceLabels", "kcRange", blackRegRotStyle }),
				(new Object[] { "experienceLabels", "kcScore", blackRegRotStyle }),
				(new Object[] { "awarenessLabels", "awarenessSignpost", plumBoldRotStyle }),
				(new Object[] { "awarenessLabels", "saRange", blackRegRotStyle }),
				(new Object[] { "awarenessLabels", "saScore", blackRegRotStyle }),
				(new Object[] { "awarenessLabels", "ssaRange", blackRegRotStyle }),
				(new Object[] { "awarenessLabels", "ssaScore", blackRegRotStyle }),
				(new Object[] { "learningAgilityLabels", "learningAgilitySignpost", plumBoldRotStyle }),
				(new Object[] { "learningAgilityLabels", "maRange", blackRegRotStyle }),
				(new Object[] { "learningAgilityLabels", "maScore", blackRegRotStyle }),
				(new Object[] { "learningAgilityLabels", "paRange", blackRegRotStyle }),
				(new Object[] { "learningAgilityLabels", "paScore", blackRegRotStyle }),
				(new Object[] { "learningAgilityLabels", "caRange", blackRegRotStyle }),
				(new Object[] { "learningAgilityLabels", "caScore", blackRegRotStyle }),
				(new Object[] { "learningAgilityLabels", "raRange", blackRegRotStyle }),
				(new Object[] { "learningAgilityLabels", "raScore", blackRegRotStyle }),
				(new Object[] { "leadershipTraitsLabels", "leadershipTraitsSignpost", plumBoldRotStyle }),
				(new Object[] { "leadershipTraitsLabels", "foRange", blackRegRotStyle }),
				(new Object[] { "leadershipTraitsLabels", "foScore", blackRegRotStyle }),
				(new Object[] { "leadershipTraitsLabels", "peRange", blackRegRotStyle }),
				(new Object[] { "leadershipTraitsLabels", "peScore", blackRegRotStyle }),
				(new Object[] { "leadershipTraitsLabels", "taRange", blackRegRotStyle }),
				(new Object[] { "leadershipTraitsLabels", "taScore", blackRegRotStyle }),
				(new Object[] { "leadershipTraitsLabels", "asRange", blackRegRotStyle }),
				(new Object[] { "leadershipTraitsLabels", "asScore", blackRegRotStyle }),
				(new Object[] { "leadershipTraitsLabels", "opRange", blackRegRotStyle }),
				(new Object[] { "leadershipTraitsLabels", "opScore", blackRegRotStyle }),
				(new Object[] { "capacityLabels", "capacitySignpost", plumBoldRotStyle }),
				(new Object[] { "capacityLabels", "psRange", blackRegRotStyle }),
				(new Object[] { "capacityLabels", "psScore", blackRegRotStyle }),
				(new Object[] { "derailmentRisksLabels", "derailmentRisksSignpost", plumBoldRotStyle }),
				(new Object[] { "derailmentRisksLabels", "voRange", blackRegRotStyle }),
				(new Object[] { "derailmentRisksLabels", "voScore", blackRegRotStyle }),
				(new Object[] { "derailmentRisksLabels", "mmRange", blackRegRotStyle }),
				(new Object[] { "derailmentRisksLabels", "mmScore", blackRegRotStyle }),
				(new Object[] { "derailmentRisksLabels", "clRange", blackRegRotStyle }),
				(new Object[] { "derailmentRisksLabels", "clScore", blackRegRotStyle }) };

		/*
		 *	Map associating related data information in order of output
		 *	1. Index
		 *	2. default data type
		 *	3. CellStyle
		 */
		Object[][] scoreOrder = new Object[][] {
				(new Object[] { PPT_FIRSTNAME_IDX, Cell.CELL_TYPE_STRING, blackRegStyle }),
				(new Object[] { PPT_LASTNAME_IDX, Cell.CELL_TYPE_STRING, blackRegStyle }),
				(new Object[] { TARGET_LEVEL_IDX, Cell.CELL_TYPE_STRING, blackRegStyle }),
				(new Object[] { SETUP_DATE_IDX, Cell.CELL_TYPE_STRING, blackRegStyle }),
				(new Object[] { PROJECT_NAME_IDX, Cell.CELL_TYPE_STRING, blackRegStyle }),
				(new Object[] { OPTIONAL_1_IDX, Cell.CELL_TYPE_STRING, blackRegStyle }),
				(new Object[] { OPTIONAL_2_IDX, Cell.CELL_TYPE_STRING, blackRegStyle }),
				(new Object[] { OPTIONAL_3_IDX, Cell.CELL_TYPE_STRING, blackRegStyle }),
				(new Object[] { SORT_ORDER_IDX, Cell.CELL_TYPE_NUMERIC, blackBoldCenterStyle }),
				(new Object[] { DRVER_SIGNPOST_IDX, Cell.CELL_TYPE_NUMERIC, blackBoldPlumStyle }),
				(new Object[] { AD_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { AD_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { CP_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { CP_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { RP_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { RP_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { EXPER_SIGNPOST_IDX, Cell.CELL_TYPE_NUMERIC, blackBoldPlumStyle }),
				(new Object[] { CE_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { CE_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { PE_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { PE_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { KC_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { KC_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { AWARE_SIGNPOST_IDX, Cell.CELL_TYPE_NUMERIC, blackBoldPlumStyle }),
				(new Object[] { SA_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { SA_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { SSA_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { SSA_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { AGILITY_SIGNPOST_IDX, Cell.CELL_TYPE_NUMERIC, blackBoldPlumStyle }),
				(new Object[] { MA_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { MA_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { PA_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { PA_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { CA_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { CA_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { RA_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { RA_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { TRAITS_SIGNPOST_IDX, Cell.CELL_TYPE_NUMERIC, blackBoldPlumStyle }),
				(new Object[] { FO_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { FO_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { PER_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { PER_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { TA_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { TA_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { AS_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { AS_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { OP_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { OP_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { CAP_SIGNPOST_IDX, Cell.CELL_TYPE_NUMERIC, blackBoldPlumStyle }),
				(new Object[] { PS_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { PS_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { DERAIL_SIGNPOST_IDX, Cell.CELL_TYPE_NUMERIC, blackBoldPlumStyle }),
				(new Object[] { VO_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { VO_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { MM_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { MM_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }),
				(new Object[] { CL_RANGE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegGreyStyle }),
				(new Object[] { CL_SCORE_IDX, Cell.CELL_TYPE_NUMERIC, blackRegStyle }) };

		// Magic numbers, calculated by taking the point width of the column and
		// dividing by 0.955625
		int c0Width = 17;
		int c1Width = 17;
		int c2Width = 5;
		int cxWidth = 3;

		Sheet s = wb.createSheet(this.jsonNode.get("generic").get("tabs").get("extract").textValue());
		s.setDisplayGridlines(true);

		for (int h = 0; h < 8; h++) {
			s.setColumnWidth(h, c0Width * 256); // Approximation
		}
		// s.setColumnWidth(0, c0Width * 256); // Approximation
		// s.setColumnWidth(1, c1Width * 256); // Approximation

		s.setColumnWidth(8, c2Width * 256); // Approximation

		for (int i = 9; i < SCORE_FIELD_COUNT; i++) {
			s.setColumnWidth(i, cxWidth * 256); // Approximation
		}

		// Set up the header data.

		int rowNo = 0;

		// Put out the header
		Row r0 = s.createRow(rowNo);
		r0.setHeightInPoints(168.75f);
		// CellStyle cs;
		String hdrTxt;
		for (int i = 0; i < SCORE_FIELD_COUNT; i++) {
			Cell cc = r0.createCell(i);
			String key1 = (String) hdrData[i][0];
			String key2 = (String) hdrData[i][1];
			cc.setCellStyle((CellStyle) hdrData[i][2]);
			if (key2 == null) {
				hdrTxt = this.jsonNode.get("extract").get(key1).textValue();
			} else {
				hdrTxt = this.jsonNode.get("extract").get(key1).get(key2).textValue();
			}
			cc.setCellValue(hdrTxt);
		}

		// Put out the data rows
		for (Object[] ppt : this.scoreData) {
			rowNo++;
			Row r = s.createRow(rowNo);
			int cellNo = 0;
			for (Object[] ord : scoreOrder) {
				// create the cell
				Cell c = r.createCell(cellNo);
				// style it
				c.setCellStyle((CellStyle) ord[2]);
				// put in the data
				Object data = ppt[(Integer) ord[0]];
				if (data == null) {
					c.setCellType(Cell.CELL_TYPE_BLANK);
				} else {
					int cellType = (Integer) ord[1];
					// look for 'na' data
					if (cellType == Cell.CELL_TYPE_NUMERIC) {
						int d = (Integer) data;
						if (d < 0) {
							cellType = Cell.CELL_TYPE_STRING;
							data = "na";
						}
					}
					c.setCellType(cellType);
					if (cellType == Cell.CELL_TYPE_STRING) {
						c.setCellValue((String) data);
					} else if (cellType == Cell.CELL_TYPE_NUMERIC) {
						c.setCellValue((Integer) data);
					} else {
						c.setCellType(Cell.CELL_TYPE_BLANK);
						log.error("Unprocessed Cell type (" + cellType + ") detected - set to blank.  ord: 0=" + ord[0]
								+ ", 1=" + ord[1] + ", 2=" + ord[2].toString());
					}
				}
				cellNo++;
			}
		}

		for (int j = 0; j < 8; j++) {
			s.autoSizeColumn(j);
		}
		// s.autoSizeColumn(0);

		// s.protectSheet(SHEET_PW);
	}

	/**
	 * getWrappedLineCount - Convenience method that calculates the wrap count
	 * using a default text point size of 10
	 * 
	 * @param r
	 * @param txt
	 * @param mergedCellWidth
	 * @return
	 */
	private int getWrappedLineCount(Row r, String txt, int mergedCellWidth) {

		// Use a default font size of 10 pt.
		return getWrappedLineCount(r, txt, mergedCellWidth, 10);
	}

	/**
	 * getWrappedLineCount - Calculate the number of rows high that a wrapped
	 * area should be. The code was copied and modified off the internet from
	 * this URL: http://stackoverflow.com/questions/22259731/apache-poi-width-
	 * calculations
	 * 
	 * @param r
	 * @param txt
	 * @param mergedCellWidth
	 * @param fontInPts
	 * @return
	 */
	private int getWrappedLineCount(Row r, String txt, int mergedCellWidth, int fontInPts) {
		int nextPos = 0;
		int lineCnt = 0;

		// Create Font object with Font attribute (e.g. Font family, Font size,
		// etc) for calculation
		java.awt.Font theFont = new java.awt.Font("Arial", 0, fontInPts);
		AttributedString attrStr = new AttributedString(txt);
		attrStr.addAttribute(TextAttribute.FONT, theFont);
		// Use LineBreakMeasurer to count number of lines needed for the text
		FontRenderContext frc = new FontRenderContext(null, true, true);
		LineBreakMeasurer measurer = new LineBreakMeasurer(attrStr.getIterator(), frc);
		while (measurer.getPosition() < txt.length()) {
			// mergedCellWidth is the max width of each line... The constant is
			// another magic number
			// nextPos = measurer.nextOffset(mergedCellWidth * 4.0f);
			nextPos = measurer.nextOffset(mergedCellWidth * 5.25f);
			lineCnt++;
			measurer.setPosition(nextPos);
		}

		return lineCnt;
	}

	/**
	 * calcDeaRange - Calculate the range value from the percentile. Used for
	 * Drivers, Experience, and Awareness components only.
	 * 
	 * @param pctl
	 * @return
	 */
	private int calcDeaRange(int pctl) {
		int ret = 0;
		if (pctl < 1 || pctl > 99) {
			ret = -1;
		} else {
			if (pctl <= 10) {
				// 1st to 10th
				ret = 0;
			} else if (pctl <= 49) {
				// 11th to 49th
				ret = 1;
			} else {
				// 50th to 99th
				ret = 2;
			}
		}

		return ret;
	}

	/**
	 * calcAtcRange - Calculate the range value from the percentile. Used for
	 * Learning agility, Leadership traits, and Capacity components only.
	 * 
	 * @param pctl
	 * @return
	 */
	private int calcAtcRange(int pctl) {
		int ret = 0;
		if (pctl < 1 || pctl > 99) {
			ret = -1;
		} else {
			if (pctl <= 10) {
				// 1st to 10th
				ret = 0;
			} else if (pctl <= 49) {
				// 11th to 49th
				ret = 1;
			} else if (pctl <= 90) {
				// 50th to 90th
				ret = 2;
			} else {
				// 91st to 99th
				ret = 3;
			}
		}

		return ret;
	}

	/**
	 * calcDerailRange - Calculate the range value from the percentile. Used for
	 * Derailler components only.
	 * 
	 * @param pctl
	 * @return
	 */
	private int calcDerailRange(int pctl) {
		int ret = 0;
		if (pctl < 1 || pctl > 99) {
			ret = -1;
		} else {
			if (pctl <= 84) {
				// 1st to 84th
				ret = 2;
			} else if (pctl <= 90) {
				// 85th to 90th
				ret = 1;
			} else {
				// 91st to 99th
				ret = 0;
			}
		}

		return ret;
	}
}
