<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns="http://www.w3.org/1999/xhtml" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xalan="http://xml.apache.org/xslt"
  xmlns:svg="http://www.w3.org/2000/svg"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="xsl xalan xs"
>

  <!--
      http://stackoverflow.com/questions/9538619/xslt-refuses-to-write-doctype-declaration
  -->
  <xsl:output method="xml" indent="yes" encoding="UTF-8"
              doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />
  
  <xsl:param name="baseurl" select="'http://localhost'" />

  <xsl:template match="/ibReport">

    <xsl:variable name="participantFullName" select="concat(participant/firstName,' ',participant/lastName)" />
    <xsl:variable name="displayDate"  select="'Dummy Date'" />

    <html>
      <head>
        <title><xsl:value-of select="reportType" /> - <xsl:value-of select="company" /> - <xsl:value-of select="$participantFullName" /></title>
        <link href="{$baseurl}projects/shell/ib_report.css" rel="stylesheet" type="text/css" />
      </head>
      <body>

        <div class="explanation">
          <h2>About this Report</h2>
          
          <h3>Performance Standards</h3>
          <p>This report provides ratings of the behaviours you exhibited during the Electronic Inbox. Your ratings were determined by comparing your responses during the inbox to the performance standards established for the Leader of Teams role.</p>

          <h3>Contents of the Report</h3>
          <p>The first section of the report provides an overview of your inbox results. Each of the four remaining sections focuses on a specific attribute and offers you more detailed feedback including:</p>
          <ul>
            <li><em>Your level of performance for that attribute</em></li>
            <li><em>Why It’s Important:</em> A description of the attribute and why it is important for leadership roles.</li>
            <li><em>Summary:</em> Specific comments about how you performed on this attribute.</li>
            <li><em>Behaviours Observed During Assessment:</em> Your level of performance on each of the behaviours.</li>
            <li><em>Development Suggestions:</em> For any behaviour needing development, suggestions are provided along with Instant Advice modules you may find helpful.</li>
          </ul>

          <h3>Leveraging Your Strengths</h3>
          <p>In addition to focusing on your development areas, it is also vitally important you leverage your strengths. For each attribute, you can find these strengths in the <em>Behaviours Observed During Assessment</em> section. Building on your strengths will enhance your reputation as a leader and provide a solid foundation for continued growth and success. </p>

          <h3>Comparing Inbox and 360 Results</h3>
          <p>This report is based on behaviours you exhibited during the Electronic Inbox. If you see differences between the results of this report and the results of your 360 report, consider whether you behaved differently during the Inbox than you do on a daily basis. For example, if the Inbox reports Collaboration as a strength, but the 360 reports it as a development need, it is possible that whilst you have the necessary skills and can display them, you need to work on using these skills on a day-to-day basis on the job. Conversely, if the 360 reports Collaboration as a strength, but the Inbox shows it as a development area, this may indicate a need to work on applying your day-to-day skills to new situations. For example, you may collaborate well with others within the familiar daily context of your job, but you may need to work on applying your skills in new, less familiar situations.</p>

          <p class="beAware">Be aware of the date of this report.  People change with time and experience.  If more than two years have passed from when the assessment was conducted, or if significant changes in the individual or the circumstances have occurred, these results may no longer be accurate.</p>
        </div>

        <div class="overall_comments">
          <h2>Overall Comments</h2>
          <p><span><xsl:apply-templates select="overallComment" mode="replaceNL"/></span></p>
          <p>Additional Shell resources on development can be located <a href="{elUrl}">here</a> on the Shell Wiki.</p>
        </div>

        <div class="container">
          <xsl:apply-templates select="section" />
        </div>

      </body>

    </html>

  </xsl:template>

  <xsl:template match="section">

    <div class="competency" id="{@name}">

      <div class="header">
        <div class="label">
          <h2><xsl:value-of select="label" /></h2>
        </div>
        <div class="scoreBar {score}">
          <h3>
            <xsl:choose>
              <xsl:when test="score = 'excellent'">Exceeds Expectations</xsl:when>
              <xsl:when test="score = 'strong'">Exceeds Expectations</xsl:when>
              <xsl:when test="score = 'solid'">Meets Expectations</xsl:when>
              <xsl:when test="score = 'intermediate'">Does Not Meet Expectations</xsl:when>
              <xsl:when test="score = 'novice'">Does Not Meet Expectations</xsl:when>
            </xsl:choose>
          </h3>
          <p class="s1">1</p><p class="s2">2</p><p class="s3">3</p><p class="s4">4</p><p class="s5">5</p>
        </div>
      </div>

      <xsl:choose>
        <xsl:when test="@name='authenticity'">
          <h3>Why It’s Important</h3>
          <p>Being an authentic leader means embodying integrity, transparency and humility. Authentic leaders bring personal meaning to work and lead from an overarching sense of purpose. They are curious, resilient under pressure and strengthened by adversity. Authenticity is the foundation of all effective leaders. Long term leadership success is impossible without it. People naturally look to their leaders to set the tone of the organisation and to model its values and principles. Authentic leaders earn the trust and respect of the people they lead. It is this trust and respect that allows authentic leaders to surmount issues and challenges.</p>
        </xsl:when>
        <xsl:when test="@name='growth'">
          <h3>Why It’s Important</h3>
          <p>Leaders who drive Growth have the ability to innovate in a way that captures opportunities and creates sustainable value. But even though they are willing to take calculated risks in the market, they are uncompromising on matters of safety, ethics and compliance. By balancing the need to innovate with an uncompromising approach to safety, they help to differentiate Shell’s reputation in the market and in society.</p>
        </xsl:when>
        <xsl:when test="@name='collaboration'">
          <h3>Why It’s Important</h3>
          <p>Collaborative leaders work across boundaries at pace and create competitive advantage through strong partnerships and by creating win-win solutions for internal and external customers. 
Because they are effective at influencing even when they don’t have formal authority, collaborative leaders have impact well beyond the formal chain of command. Because they embrace diversity in people and cultures they draw people and opportunities to them in ways that less collaborative leaders cannot.
</p>

        </xsl:when>
        <xsl:when test="@name='performance'">
          <h3>Why It’s Important</h3>
          <p>Leaders who excel at Performance set a high standard of excellence, empower teams and hold them accountable for outcomes. In short they create the conditions that bring out the best in the people around them and in so doing, deliver exceptional results to the market while simultaneously developing the Shell leaders of the future.</p>
        </xsl:when>
      </xsl:choose>

      <div class="comments">
        <h3>Summary</h3>
        <p><span><xsl:apply-templates select="comments" mode="replaceNL"/></span></p>
      </div>

      <h3 class="sectionhead">Behaviours Observed During Assessment</h3>

      <table cellpadding="0" cellspacing="0">
        <thead>
          <tr>
            <td width="33%"><p>Behaviours Needing Development</p></td>
            <td width="33%"><p>Competent Behaviours</p></td>
            <td width="33%"><p>Strengths to Leverage</p></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <ul>
                <xsl:choose>
                  <xsl:when test="count(behaviors/level[@name='development']/point) &gt; 0">
                    <xsl:for-each select="behaviors/level[@name='development']/point">
                      <li><xsl:value-of select="." /></li>
                    </xsl:for-each>
                  </xsl:when>
                  <xsl:otherwise>
                    <li>Your assessment results did not lead to any behaviours in this category</li>
                  </xsl:otherwise>
                </xsl:choose>
              </ul>
            </td>
            <td>
              <ul>
                <xsl:choose>
                  <xsl:when test="count(behaviors/level[@name='competent']/point) &gt; 0">
                    <xsl:for-each select="behaviors/level[@name='competent']/point">
                      <li><xsl:value-of select="." /></li>
                    </xsl:for-each>
                  </xsl:when>
                  <xsl:otherwise>
                    <li>Your assessment results did not lead to any behaviours in this category</li>
                  </xsl:otherwise>
                </xsl:choose>
              </ul>
            </td>
            <td>
              <ul>
                <xsl:choose>
                  <xsl:when test="count(behaviors/level[@name='effective']/point) &gt; 0">
                    <xsl:for-each select="behaviors/level[@name='effective']/point">
                      <li><xsl:value-of select="." /></li>
                    </xsl:for-each>
                  </xsl:when>
                  <xsl:otherwise>
                    <li>Your assessment results did not lead to any behaviours in this category</li>
                  </xsl:otherwise>
                </xsl:choose>
              </ul>
            </td>
          </tr>
        </tbody>
      </table>

      <div class="suggestions">
        <div class="header">
          <div class="label">
            <h2><xsl:value-of select="label" /></h2>
          </div>
        </div>

        <h3>Development Suggestions</h3>
        <ul>
          <xsl:choose>
            <xsl:when test="count(development/behavior/suggestions/suggestion | development/behavior/modules/module) &gt; 0">
              <xsl:for-each select="development/behavior">
                <xsl:for-each select="suggestions/suggestion">
                  <li><xsl:value-of select="." disable-output-escaping="yes" /></li>
                </xsl:for-each>
                <xsl:if test="count(modules/module) &gt; 0">
                  <li>
                    <xsl:text>This Instant Advice may be helpful to you: </xsl:text>
                    <xsl:for-each select="modules/module">
                      <a href="{iaUrl}"><xsl:value-of select="displayText" /></a>
                      <xsl:if test="position() != last()">, </xsl:if>
                    </xsl:for-each>
                  </li>
                </xsl:if>
              </xsl:for-each>
            </xsl:when>
            <xsl:when test="normalize-space(positiveFeedback) != ''">
                <li><xsl:value-of select="positiveFeedback" disable-output-escaping="yes" /></li>
            </xsl:when>
            <xsl:otherwise>
              <li>There are no development suggestions.</li>
            </xsl:otherwise>
          </xsl:choose>

        </ul>

      </div>
    </div>

  </xsl:template>

  <!-- These next two would be used if html in point wasn't escaped -->
  <xsl:template match="point">
    <xsl:apply-templates select="@*|node()" mode="point" />
  </xsl:template>
  <xsl:template match="@*|node()" mode="point">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" mode="point" />
    </xsl:copy>
  </xsl:template>

  <!--  replace newlines with html breaks -->
  <xsl:template match="text()" name="replaceNL" mode="replaceNL">
  <!--
   Not sure why this was done, but it screws up the HTML by forcing everything onto one line.
   <xsl:param name="pText" select="translate(., ' ', '&#160;')"/>
  -->
   <xsl:param name="pText" select="."/>
   <xsl:choose>
     <xsl:when test="not(contains($pText, '&#10;'))">
      <xsl:copy-of select="$pText"/>
     </xsl:when>
     <xsl:otherwise>
       <xsl:copy-of select="substring-before($pText, '&#10;')"/>
       <br />
       <xsl:call-template name="replaceNL">
         <xsl:with-param name="pText" select="substring-after($pText, '&#10;')"/>
       </xsl:call-template>
     </xsl:otherwise>
   </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
