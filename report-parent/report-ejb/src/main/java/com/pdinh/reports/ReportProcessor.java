package com.pdinh.reports;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.pdfbox.util.PDFMergerUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.pdinh.persistence.ms.entity.ReportType;
import com.pdinh.process.ProcessErrorStreamHandler;
import com.pdinh.report.util.FreeMarkerUtil;
import com.pdinh.reporting.PortalServletServiceLocal;

/**
 * NOTE: This is an attempt to make ReportProcessor more generic and to address
 * issues put in comments by Ken an Jeff.
 */
@Stateless
@LocalBean
public class ReportProcessor {

	@Resource(mappedName = "application/report/config_properties")
	private Properties configProperties;

	@Resource(mappedName = "custom/generator_properties")
	private Properties generatorProperties;

	private static final Logger log = LoggerFactory.getLogger(ReportProcessor.class);

	private static final String TOKEN_REPL_XFORM = "projects/lva/mll/tokenreplace.xsl";
	@EJB
	PortalServletServiceLocal portalServletServiceLocal;

	// private List<String> tempFilesRef = new ArrayList<String>();

	public PortalServletServiceLocal getPortalServletServiceLocal() {
		return portalServletServiceLocal;
	}

	public void setPortalServletServiceLocal(PortalServletServiceLocal portalServletServiceLocal) {
		this.portalServletServiceLocal = portalServletServiceLocal;
	}

	/**
	 * Default constructor.
	 */
	public ReportProcessor() {
		;
	}

	/**
	 * Process the report request to intermediate HTML format without custom
	 * params
	 * 
	 * @param reportXml
	 *            XML report message to be processed
	 * @param outputStream
	 *            stream to which generated PDF is written
	 * @param xsltPath
	 *            path (relative to "com/pdinh/resource") of xslt
	 */

	public Document ProcessReportToHtml(Document reportXml, OutputStream outputStream, String xsltPath,
			String reportSubtype, String genmode) {
		return ProcessReportToHtml(reportXml, outputStream, xsltPath, reportSubtype, genmode, null);
	}

	/**
	 * Process the report request to intermediate HTML format with custom params
	 * 
	 * @param reportXml
	 *            XML report message to be processed
	 * @param outputStream
	 *            stream to which generated PDF is written
	 * @param xsltPath
	 *            path (relative to "com/pdinh/resource") of xslt
	 * @param customParams
	 *            map of custom params sent to xslt transform
	 */
	public Document ProcessReportToHtml(Document reportXml, OutputStream outputStream, String xsltPath,
			String reportSubtype, String genmode, Map<String, String> customParams) {
		log.debug("in GenericReportProcessor.processeReportToHtml");
		log.debug("subtype = " + reportSubtype);

		try {

			// reportserver-web base url
			String baseUrl = configProperties.getProperty("reportserverBaseUrl");

			log.debug("baseUrl = " + baseUrl);
			log.debug("xsltPath = " + xsltPath);

			// Get a transform
			Transformer transformer = getTransformerFromXslt(xsltPath, baseUrl);
			// pass subtype param to transform
			transformer.setParameter("subtype", reportSubtype);
			transformer.setParameter("genmode", genmode);

			// Set custom params
			if (customParams != null) {
				setTransformerCustomParams(transformer, customParams);
			}

			// Gender tags may still be present. Apply a transform with unknown
			// gender
			// to replace with "He/she"
			reportXml = replaceGenderTokens(reportXml);
			log.debug("replaced tokens");

			// create source for transform from the xml dom
			DOMSource xmlSource = new DOMSource(reportXml);

			// Do the transform
			log.debug("performing transform");
			try {
				transformer.transform(xmlSource, new StreamResult(outputStream));
			} catch (TransformerException e) {
				log.debug("transform failed...");
				e.printStackTrace();
			}

			outputStream.flush();

		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (Exception excp) {
			excp.printStackTrace();
		} finally {
			// TODO: hmm, can this be cleaned up?
			try {
				outputStream.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

		}
		return reportXml;
	}

	/**
	 * Process the report request to a PDF output stream
	 * 
	 * @param reportXml
	 *            XML report message to be processed
	 * @param outputStream
	 *            stream to which generated PDF is written
	 * @param xsltPath
	 *            path (relative to "com/pdinh/resource") of xslt
	 */
	public void ProcessReportToPdf(Document reportXml, OutputStream outputStream, String xsltPath, String reportSubtype) {
		ProcessReportToPdf(reportXml, outputStream, xsltPath, reportSubtype, null);
	}

	/**
	 * Process the report request to a PDF output stream with custom params
	 * 
	 * @param reportXml
	 *            XML report message to be processed
	 * @param outputStream
	 *            stream to which generated PDF is written
	 * @param xsltPath
	 *            path (relative to "com/pdinh/resource") of xslt
	 * @param customParams
	 *            map of custom params sent to xslt transform
	 */
	public void ProcessReportToPdf(Document reportXml, OutputStream outputStream, String xsltPath,
			String reportSubtype, Map<String, String> customParams) {

		InputStream is = null;
		try {

			// print RCM to log
			// try {
			// StringWriter sw = new StringWriter();
			// TransformerFactory tf = TransformerFactory.newInstance();
			// Transformer transformer = tf.newTransformer();
			// transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
			// "no");
			// transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			// transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			// transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			// transformer.transform(new DOMSource(reportXml), new
			// StreamResult(sw));
			// log.debug(sw.toString());
			// } catch (Exception ex) {
			// throw new RuntimeException("Error converting to String", ex);
			// }

			log.debug("in processReportToPdf");

			String baseUrl = configProperties.getProperty("reportserverBaseUrl");
			log.debug("baseUrl = " + baseUrl);

			String wkhtmltopdfPath = generatorProperties.getProperty("wkhtmltopdfPath");

			log.debug("baseurl = " + baseUrl);
			log.debug("reportSubtype = " + reportSubtype);

			log.debug("xsltPath = " + xsltPath);

			// Get a transform
			Transformer transformer = getTransformerFromXslt(xsltPath, baseUrl);

			// Pass subtype param to transform
			transformer.setParameter("subtype", reportSubtype);

			// Set custom params
			if (customParams != null) {
				setTransformerCustomParams(transformer, customParams);
			}

			// Gender tags may still be present. Apply a transform with unknown
			// gender
			// to replace with "He/she"
			log.debug("reportXml before is = " + reportXml);
			reportXml = replaceGenderTokens(reportXml);
			log.debug("reportXml after replaceGenderTokens is = " + reportXml);

			log.debug("replaced tokens");

			// Create source for transform from the xml dom
			DOMSource xmlSource = new DOMSource(reportXml);

			// Get participant name and assessment date from XML
			XPath xpath = XPathFactory.newInstance().newXPath();

			// Element elm;
			// String firstName=null, lastName=null, assessmentDate=null, xps,
			// fullName;
			String client = null, participantId = null, firstName = null, lastName = null, assessmentDate = null, fullName = null, level = null, lang = null;
			String projName = null;
			Element dateElm = null;
			Element root = reportXml.getDocumentElement();
			String rootTag = root.getTagName();

			try {
				client = (String) xpath.evaluate("companyName", root, XPathConstants.STRING);
				participantId = (String) xpath.evaluate("participant/@id", root, XPathConstants.STRING);
				firstName = (String) xpath.evaluate("participant/firstName", root, XPathConstants.STRING);
				lastName = (String) xpath.evaluate("participant/lastName", root, XPathConstants.STRING);
				level = (String) xpath.evaluate("level", root, XPathConstants.STRING);
				projName = (String) xpath.evaluate("project", root, XPathConstants.STRING);
				// assessmentDate = (String) xpath.evaluate(
				// "/ibReport/assessmentDate", reportXml,
				// XPathConstants.STRING);
				dateElm = (Element) xpath.evaluate("assessmentDate", root, XPathConstants.NODE);
				if (dateElm != null)
					assessmentDate = dateElm.getTextContent();

			} catch (XPathExpressionException e) {
				e.printStackTrace();
			}
			// Now html escape the client and the project
			client = replaceXmlSensitiveChar(client);
			projName = replaceXmlSensitiveChar(projName);

			log.debug("firstName = " + firstName);
			log.debug("lastName = " + lastName);

			log.debug("participant id = " + participantId);
			log.debug("date = " + assessmentDate);

			// TODO We need to figure out a way to specify externally what date
			// template to use for output...
			// This is fine for Shell but needs to be generalized to allow for
			// any desired/specified format
			// Either that or we need to format it appropriately in the
			// precursor code and get rid of this here
			// Standard input format is yyyy-MM-dd (ex., 2012-04-28)
			SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
			// SimpleDateFormat outputFormat = new
			// SimpleDateFormat("MMMMM d, yyyy");
			SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
			if (assessmentDate != null)
				assessmentDate = outputFormat.format(inputFormat.parse(assessmentDate));

			// put formatted date back in RCM
			if (dateElm != null)
				dateElm.setTextContent(assessmentDate);

			fullName = String.format("%s %s", firstName, lastName);
			log.debug("fullName = " + fullName);
			log.debug("date = " + assessmentDate);

			// stream to get result of transform to HTML
			ByteArrayOutputStream resultBuf = new ByteArrayOutputStream();

			// xslt result
			Result output = new StreamResult(resultBuf);

			// Do the transform
			log.debug("performing transform");

			try {
				transformer.transform(xmlSource, output);
			} catch (TransformerException e) {
				log.debug("ReportProcessor.ProcessSingleReportToPdf() transform failed...");
				e.printStackTrace();
			}

			List<String> tempFilesRef = new ArrayList<String>();

			// Build URL for the document header and footer and cover page
			String webPath = null;
			String headerHtml = null, footerHtml = null, coverHtml = null, endHtml = null;
			String marginTop = null, marginSide = null, marginBottom = null;
			String headerSpacing = "0";
			String coverMarginTop = "0", coverMarginSide = "0", coverMarginBottom = "0";
			String pageSize = "Letter"; // default
			String orientation = "Portrait"; // default
			String reportName = null;
			boolean hasCoverPage = true;
			boolean hasEndPage = false;
			log.debug("rootTag = " + rootTag);
			boolean tempFiles = false;
			if (rootTag.equals("lvaDbReport")) {
				webPath = "/core/xhtml";
				marginTop = "37";
				marginBottom = "22";
				marginSide = "10";
				// coverMarginTop = "10";
				// coverMarginSide = "10";
				// coverMarginBottom = "10";
				coverMarginTop = "0";
				coverMarginSide = "0";
				coverMarginBottom = "0";
				headerSpacing = "25";

				// switching over to new wkhtmltopdf
				wkhtmltopdfPath = generatorProperties.getProperty("wkhtmltopdfPathNew");

				// TODO: report processor shouldn't have to deal with these
				// cases
				if (reportSubtype.equals("development"))
					reportName = "Development Action Report";
				else if (reportSubtype.equals("readiness"))
					reportName = "Readiness Action Report";
				else if (reportSubtype.equals("fit"))
					reportName = "Fit Report";

				// headerHtml = formatEncodedUrl(baseUrl + webPath
				// + "/header.xhtml?reportname=%s&participant=%s&headerdate=%s",
				// reportName, fullName,
				// assessmentDate);

				headerHtml = formatEncodedUrl(baseUrl + webPath + "/kfHeader.xhtml?participant=%s", fullName);

				String DateFormat = "dd MMMMM yyyy";
				SimpleDateFormat sdf = new SimpleDateFormat(DateFormat);
				String reportDate = sdf.format(new Date());

				// footerHtml = String.format("%s%s/footer.xhtml", baseUrl,
				// webPath);
				footerHtml = formatEncodedUrl(baseUrl + webPath + "/kfFooter.xhtml?footerdate=%s", "Created "
						+ reportDate + ".");

				// coverHtml = formatEncodedUrl(
				// baseUrl
				// + webPath
				// +
				// "/cover.xhtml?productname=GreatLeaderGPS&reportname=%s&reportsubhead=Mid-level Leader&participant=%s&client=%s&project=%s&assdate=%s",
				// reportName, fullName, client, projName, assessmentDate);

				coverHtml = formatEncodedUrl(baseUrl + webPath
						+ "/kfCover.xhtml?reportname=%s&fullname=%s&clientname=%s&reportdate=%s", reportName, fullName,
						client, assessmentDate);

				endHtml = new String();
				endHtml = baseUrl + webPath + "/endPage.xhtml";
				hasEndPage = true;

			} else if (rootTag.equals("ibReport")) {
				webPath = "/projects/shell";
				marginTop = "46";
				marginBottom = "30";
				marginSide = "12";
				pageSize = "A4";
				coverMarginTop = "12";
				coverMarginSide = "12";
				coverMarginBottom = "12";

				headerHtml = String.format("%s%s/header.xhtml?Participant=%s&Gendate=%s", baseUrl, webPath,
						URLEncoder.encode(fullName, "UTF-8"), assessmentDate);

				footerHtml = String.format("%s%s/footer.xhtml?Participant=%s&Gendate=%s", baseUrl, webPath,
						URLEncoder.encode(fullName, "UTF-8"), assessmentDate);

				coverHtml = String.format("%s%s/cover.xhtml?label=%s&participant=%s&date=%s", baseUrl, webPath, level,
						URLEncoder.encode(fullName, "UTF-8"), assessmentDate);
			} else if (rootTag.equals("coachingPlanReport")) {
				webPath = "/projects/coaching/plan";
				marginTop = "37";
				marginBottom = "22";
				marginSide = "10";
				coverMarginTop = "10";
				coverMarginSide = "10";
				coverMarginBottom = "10";

				String coachFirstName = (String) xpath.evaluate("coach/firstName", root, XPathConstants.STRING);
				String coachLastName = (String) xpath.evaluate("coach/lastName", root, XPathConstants.STRING);

				String coachFullName = coachFirstName + " " + coachLastName;

				String reportDate = (String) xpath.evaluate("reportDate", root, XPathConstants.STRING);

				headerHtml = String.format(
						"%s%s/header.xhtml?participant=%s&coach=%s&headerdate=%s&reportname=Coaching Development Plan",
						baseUrl, webPath, URLEncoder.encode(fullName, "UTF-8"),
						URLEncoder.encode(coachFullName, "UTF-8"), reportDate);

				footerHtml = String.format("%s%s/footer.xhtml", baseUrl, webPath);

				coverHtml = String.format(
						"%s%s/cover.xhtml?reportname=Coaching Development Plan&client=%s&reportdate=%s", baseUrl,
						webPath, client, reportDate);
			} else if (rootTag.equals("coachingSummaryReport")) {
				webPath = "/projects/coaching/summary";
				marginTop = "37";
				marginBottom = "22";
				marginSide = "10";
				coverMarginTop = "10";
				coverMarginSide = "10";
				coverMarginBottom = "10";

				headerHtml = String.format("%s%s/header.xhtml?reportname=Coaching Summary Report", baseUrl, webPath);

				footerHtml = String.format("%s%s/footer.xhtml", baseUrl, webPath);

				hasCoverPage = false;
			} else if (rootTag.equals("viaEdgeIndividualSummaryReport")) {
				webPath = "/projects/viaEdge/individualSummary";
				// marginTop = "31";
				// marginBottom = "22";
				marginTop = "35";
				marginBottom = "20";
				marginSide = "10";
				coverMarginTop = "10";
				coverMarginSide = "10";
				coverMarginBottom = "10";
				String reportDate = (String) xpath.evaluate("reportDate", root, XPathConstants.STRING);
				// String sectionName1 = (String)
				// xpath.evaluate("introductionSection/title", root,
				// XPathConstants.STRING);
				// String sectionName2 = (String)
				// xpath.evaluate("resultsSummarySection/title", root,
				// XPathConstants.STRING);
				String reportSerial = (String) xpath.evaluate("serialNumber", root, XPathConstants.STRING);
				reportName = (String) xpath.evaluate("reportTitle", root, XPathConstants.STRING);
				lang = (String) xpath.evaluate("@lang", root, XPathConstants.STRING);
				fullName = firstName + " " + lastName;
				// headerHtml = String.format("%s%s/header.xhtml", baseUrl,
				// webPath);
				tempFiles = true; // flag to delete temp file when finished
				// HEADER USING FreeMarker
				Map<String, String> header_dataModel = new HashMap<String, String>();
				header_dataModel.put("baseURL", baseUrl);
				header_dataModel.put("webPath", webPath);
				header_dataModel.put("lang", lang);
				String headerHtmlCode = FreeMarkerUtil.processTemplate(
						"/projects/viaEdge/individualSummary/header.ftl", header_dataModel);
				headerHtml = FreeMarkerUtil.tempHtmlFile("VE_i_header", headerHtmlCode);
				tempFilesRef.add(headerHtml);
				String copyright = (String) xpath.evaluate("copyright", root, XPathConstants.STRING);

				footerHtml = String.format("%s%s/footer.xhtml?fullName=%s&copyright=%s&lang=%s", baseUrl, webPath,
						URLEncoder.encode(fullName, "UTF-8"), URLEncoder.encode(copyright, "UTF-8"), lang);

				// FOOTER USING FreeMarker
				// Map<String, String> footer_dataModel = new HashMap<String,
				// String>();
				// footer_dataModel.put("fullName", fullName);
				// footer_dataModel.put("copyright", copyright);
				// footer_dataModel.put("baseURL", baseUrl);
				// footer_dataModel.put("webPath", webPath);
				// String footerHtmlCode = FreeMarkerUtil.processTemplate(
				// "/projects/viaEdge/individualSummary/footer.ftl",
				// footer_dataModel);
				// footerHtml = FreeMarkerUtil.tempHtmlFile("VE_i_footer",
				// footerHtmlCode);
				// tempFilesRef.add(footerHtml);
				// coverHtml =
				// String.format("%s%s/cover.xhtml?fullname=%s&reportdate=%s&reportserial=%s&reportname=%s",
				// baseUrl, webPath, fullName, reportDate, reportserial,
				// reportName);

				// hard wire the report title for ja...
				// lang = (String) xpath.evaluate("@lang", root,
				// XPathConstants.STRING);

				coverHtml = new String();

				// COVERPAGE USING FreeMarker
				Map<String, String> cover_dataModel = new HashMap<String, String>();
				cover_dataModel.put("lang", lang);
				cover_dataModel.put("fullName", fullName);
				cover_dataModel.put("reportName", reportName);
				cover_dataModel.put("reportSerial", reportSerial);
				cover_dataModel.put("reportDate", reportDate);
				cover_dataModel.put("baseURL", baseUrl);
				cover_dataModel.put("webPath", webPath);
				String coverHtmlCode = FreeMarkerUtil.processTemplate("/projects/viaEdge/individualSummary/cover.ftl",
						cover_dataModel);

				coverHtml = FreeMarkerUtil.tempHtmlFile("VE_i_cover", coverHtmlCode);
				tempFilesRef.add(coverHtml);
				// coverHtml = baseUrl + webPath + "/cover.xhtml?fullname=" +
				// URLEncoder.encode(fullName, "UTF-8")
				// + "&reportdate=" + reportDate + "&reportserial=" +
				// reportSerial + "&reportname="
				// + URLEncoder.encode(reportName, "UTF-8") + "&lang=" + lang;
				// System.out.println("coverHtml = " + coverHtml);
				hasCoverPage = true;
			} else if (rootTag.equals("viaEdgeCoachingReport")) {
				if (reportSubtype.equals(ReportType.VIAEDGE_INDIVIDUAL_COACHING)) {
					webPath = "/projects/viaEdge/coaching";
				} else {
					// FEEDBACK REPORT
					if (!reportSubtype.equals("hybridReport")) {
						webPath = "/projects/viaEdge/feedback";
					} else {
						webPath = "/projects/TLTwLA/xhtml";
					}
				}
				// log.debug("webPath = /projects/viaEdge/coaching     " +
				// rootTag);

				marginTop = "37";
				marginBottom = "20";
				marginSide = "10";
				coverMarginTop = "10";
				coverMarginSide = "10";
				coverMarginBottom = "10";
				if (!reportSubtype.equals("hybridReport")) {
					orientation = "Landscape";
					marginTop = "32";
				}
				String reportDate = (String) xpath.evaluate("reportDate", root, XPathConstants.STRING);

				String reportserial = (String) xpath.evaluate("serialNumber", root, XPathConstants.STRING);
				if (!reportSubtype.equals("hybridReport")) {
					reportName = (String) xpath.evaluate("reportTitle", root, XPathConstants.STRING);
				} else {
					reportName = portalServletServiceLocal
							.portalServletRequest("<ReportLabelLookup labelId=\"INDIV_SUMMARY_RPT\" lang=\""
									+ (String) xpath.evaluate("@lang", root, XPathConstants.STRING)
									+ "\"></ReportLabelLookup>");
				}

				fullName = firstName + " " + lastName;

				// hard wire the report title for ja...
				lang = (String) xpath.evaluate("@lang", root, XPathConstants.STRING);

				// headerHtml = baseUrl + webPath + "/header.xhtml?reportname="
				// + URLEncoder.encode(reportName, "UTF-8")
				// + "&participant=" + URLEncoder.encode(fullName, "UTF-8") +
				// "&headerdate="
				// + URLEncoder.encode(reportDate, "UTF-8") + "&lang=" + lang;

				headerHtml = String.format("%s%s/header.xhtml?reportname=%s&participant=%s&headerdate=%s&lang=%s",
						baseUrl, webPath, URLEncoder.encode(reportName, "UTF-8"), URLEncoder.encode(fullName, "UTF-8"),
						reportDate, lang);

				String copyright = (String) xpath.evaluate("copyright", root, XPathConstants.STRING);
				footerHtml = baseUrl + webPath + "/footer.xhtml?participant=" + URLEncoder.encode(fullName, "UTF-8")
						+ "&copyright=" + URLEncoder.encode(copyright, "UTF-8") + "&lang=" + lang;

				coverHtml = new String();

				coverHtml = baseUrl + webPath + "/cover.xhtml?fullname=" + URLEncoder.encode(fullName, "UTF-8")
						+ "&reportdate=" + reportDate + "&reportserial=" + reportserial + "&reportname=";
				coverHtml += URLEncoder.encode(reportName, "UTF-8");
				coverHtml += "&lang=" + lang;

				if (reportSubtype.equals("hybridReport"))
					hasCoverPage = false;
			} else if (rootTag.equals("viaEdgeGroupReport")) {
				webPath = "/projects/viaEdge/groupFeedback";
				marginTop = "31";
				marginBottom = "15";
				marginSide = "10";
				coverMarginTop = "10";
				coverMarginSide = "10";
				coverMarginBottom = "10";
				// String reportDate = (String) xpath.evaluate("reportDate",
				// root, XPathConstants.STRING);
				// String sectionName1 = (String)
				// xpath.evaluate("introductionSection/title", root,
				// XPathConstants.STRING);
				// String sectionName2 = (String)
				// xpath.evaluate("resultsSummarySection/title", root,
				// XPathConstants.STRING);
				String reportserial = (String) xpath.evaluate("serialNumber", root, XPathConstants.STRING);
				reportName = (String) xpath.evaluate("reportTitle", root, XPathConstants.STRING);
				lang = (String) xpath.evaluate("@lang", root, XPathConstants.STRING);

				headerHtml = String.format("%s%s/header.xhtml?lang=%s", baseUrl, webPath, lang);
				fullName = firstName + " " + lastName;
				String copyright = (String) xpath.evaluate("copyright", root, XPathConstants.STRING);
				footerHtml = baseUrl + webPath + "/footer.xhtml?copyright=" + URLEncoder.encode(copyright, "UTF-8")
						+ "&groupname=" + URLEncoder.encode(fullName, "UTF-8") + "&lang=" + lang;

				// coverHtml =
				// String.format("%s%s/cover.xhtml?fullname=%s&reportdate=%s&reportserial=%s&reportname=%s",
				// baseUrl, webPath, fullName, reportDate, reportserial,
				// reportName);
				coverHtml = new String();

				// hard wire the report title for ja...
				lang = (String) xpath.evaluate("@lang", root, XPathConstants.STRING);

				String DateFormat = "MM/dd/yyyy";

				if (lang.equals("en-US"))
					DateFormat = "MM/dd/yyyy";
				if (lang.equals("ja"))
					DateFormat = "yyyy/MM/dd";
				if (lang.equals("zh"))
					DateFormat = "yyyy/MM/dd";
				if (lang.equals("it"))
					DateFormat = "dd/MM/yyyy";
				if (lang.equals("fr"))
					DateFormat = "dd/MM/yyyy";
				if (lang.equals("de"))
					DateFormat = "dd.MM.yyyy";
				if (lang.equals("pt"))
					DateFormat = "dd/MM/yyyy";
				if (lang.equals("es"))
					DateFormat = "dd/MM/yyyy";
				if (lang.equals("ru"))
					DateFormat = "dd.MM.yyyy";
				if (lang.equals("ko"))
					DateFormat = "yyyy/MM/dd";

				SimpleDateFormat sdf = new SimpleDateFormat(DateFormat);
				String reportDate = sdf.format(new Date());

				coverHtml = baseUrl + webPath + "/cover.xhtml?fullname=" + URLEncoder.encode(fullName, "UTF-8")
						+ "&reportdate=" + reportDate + "&reportserial=" + reportserial + "&reportname="
						+ URLEncoder.encode(reportName, "UTF-8") + "&lang=" + lang;
				// System.out.println("coverHtml = " + coverHtml);
				hasCoverPage = true;
			} else if (rootTag.equals("kfapIndividualFeedbackReport") || rootTag.equals("kfapGroupReport")
					|| rootTag.equals("kfapIndividualFeedbackReportV2") || rootTag.equals("talentGridV2")) {
				webPath = "/projects/kfap/shared";
				marginTop = "29";
				marginBottom = "15";
				marginSide = "19";
				headerSpacing = "15";
				coverMarginTop = "0";
				coverMarginSide = "0";
				coverMarginBottom = "0";

				tempFiles = true; // flag to delete temp file when finished

				String reportDate = (String) xpath.evaluate("reportDate", root, XPathConstants.STRING);
				String createDate = (String) xpath.evaluate("createDate", root, XPathConstants.STRING);

				firstName = (String) xpath.evaluate("participantName/firstName", root, XPathConstants.STRING);
				lastName = (String) xpath.evaluate("participantName/lastName", root, XPathConstants.STRING);
				fullName = firstName + " " + lastName;
				String assessmentName = (String) xpath.evaluate("assessmentTitle", root, XPathConstants.STRING);
				String targetLevel = (String) xpath.evaluate("target", root, XPathConstants.STRING);
				String confidential = (String) xpath.evaluate("confidential", root, XPathConstants.STRING);
				String confidentialPeriod = (String) xpath.evaluate("confidentialWPeriod", root, XPathConstants.STRING);
				String nameLabel = (String) xpath.evaluate("nameLabel", root, XPathConstants.STRING);
				String clientLabel = (String) xpath.evaluate("clientLabel", root, XPathConstants.STRING);
				String clientName = (String) xpath.evaluate("client", root, XPathConstants.STRING);
				String targetLabel = (String) xpath.evaluate("targetLabel", root, XPathConstants.STRING);
				String dateLabel = (String) xpath.evaluate("reportDateLabel", root, XPathConstants.STRING);
				String kf = (String) xpath.evaluate("providedBy", root, XPathConstants.STRING);
				String sharedWebPath = "/projects/kfap/shared";
				String copyright = (String) xpath.evaluate("copyright", root, XPathConstants.STRING);
				reportName = (String) xpath.evaluate("reportName", root, XPathConstants.STRING);

				String endpageHeader = (String) xpath.evaluate("endpageHeader", root, XPathConstants.STRING);
				String endpageP1 = (String) xpath.evaluate("endpageP1", root, XPathConstants.STRING);
				String endpageP2 = (String) xpath.evaluate("endpageP2", root, XPathConstants.STRING);
				String endpageP3 = (String) xpath.evaluate("endpageP3", root, XPathConstants.STRING);
				lang = (String) xpath.evaluate("@lang", root, XPathConstants.STRING);

				String headerTarget = targetLabel + ": " + targetLevel;

				log.debug("firstName = " + firstName);
				log.debug("lastName = " + lastName);
				log.debug("fullName = " + fullName);

				wkhtmltopdfPath = generatorProperties.getProperty("wkhtmltopdfPathNew");

				clientName = replaceXmlSensitiveChar(clientName);

				endHtml = new String();

				Map<String, String> endpage_dataModel = new HashMap<String, String>();
				endpage_dataModel.put("baseURL", baseUrl);
				endpage_dataModel.put("webPath", webPath);
				endpage_dataModel.put("endpageHeader", endpageHeader);
				endpage_dataModel.put("endpageP1", endpageP1);
				endpage_dataModel.put("endpageP2", endpageP2);
				endpage_dataModel.put("endpageP3", endpageP3);
				endpage_dataModel.put("copyright", copyright);

				String endpageHtmlCode = FreeMarkerUtil.processTemplate("/projects/kfap/individual/endpage.ftl",
						endpage_dataModel);

				if (rootTag.equals("kfapIndividualFeedbackReportV2") || rootTag.equals("talentGridV2")) {

					String version = (String) xpath.evaluate("versionNumber", root, XPathConstants.STRING);
					endpage_dataModel.put("version", version);
					endpage_dataModel.put("lang", lang);
					endpageHtmlCode = FreeMarkerUtil.processTemplate("/projects/kfap/individual/endpageV2.ftl",
							endpage_dataModel);
				}

				endHtml = FreeMarkerUtil.tempHtmlFile("KFAP_i_endpage", endpageHtmlCode);
				tempFilesRef.add(endHtml);
				hasEndPage = true;

				footerHtml = baseUrl + webPath + "/footer.xhtml?pc=" + URLEncoder.encode(confidentialPeriod, "UTF-8")
						+ "&kf=" + URLEncoder.encode(kf, "UTF-8") + "&footerdate="
						+ URLEncoder.encode(createDate, "UTF-8") + "&lang=" + lang;

				if (rootTag.equals("kfapIndividualFeedbackReport") || rootTag.equals("kfapIndividualFeedbackReportV2")) {

					String assessmentNameValue = URLEncoder.encode(assessmentName, "UTF-8");
					String currentValue = URLEncoder.encode(targetLevel, "UTF-8");
					String participantValue = URLEncoder.encode(fullName, "UTF-8");
					String reportNameValue = URLEncoder.encode(reportName, "UTF-8");

					String perosnalValue = URLEncoder.encode(confidential, "UTF-8");
					String nameValue = URLEncoder.encode(nameLabel, "UTF-8");
					String clientValue = URLEncoder.encode(clientLabel, "UTF-8");
					String fullNameValue = URLEncoder.encode(fullName, "UTF-8");
					String clientNameValue = URLEncoder.encode(clientName, "UTF-8");
					String targetValue = URLEncoder.encode(targetLabel, "UTF-8");
					String dateValue = URLEncoder.encode(dateLabel, "UTF-8");
					String targetNameValue = URLEncoder.encode(targetLevel, "UTF-8");
					String reportDateValue = URLEncoder.encode(reportDate, "UTF-8");

					log.debug("alp=" + assessmentNameValue);
					log.debug("current=" + currentValue);
					log.debug("participant=" + participantValue);
					log.debug("lang=" + lang);
					log.debug("perosnalValue=" + perosnalValue);
					log.debug("nameValue=" + nameValue);
					log.debug("clientValue=" + clientValue);
					log.debug("fullNameValue=" + fullNameValue);
					log.debug("clientNameValue=" + clientNameValue);
					log.debug("targetValue=" + targetValue);
					log.debug("dateValue=" + dateValue);
					log.debug("targetNameValue=" + targetNameValue);
					log.debug("reportDateValue=" + reportDateValue);

					log.debug("reportname=" + reportNameValue);

					log.debug("rootTag=" + rootTag);

					log.debug("Before setting headerHtml=" + headerHtml);
					headerHtml = baseUrl + webPath + "/header3line.xhtml?alp=" + assessmentNameValue + "&current="
							+ currentValue + "&participant=" + participantValue + "&lang=" + lang;
					log.debug("Before setting coverHtml=" + coverHtml);

					coverHtml = new String();
					coverHtml = baseUrl + webPath + "/cover.xhtml?reportname=" + reportNameValue + "&assessmentname="
							+ assessmentNameValue + "&personal=" + perosnalValue + "&name=" + nameValue + "&client="
							+ clientValue + "&fullname=" + fullNameValue + "&clientname=" + clientNameValue
							+ "&target=" + targetValue + "&date=" + dateValue + "&targetname=" + targetNameValue
							+ "&reportdate=" + reportDateValue + "&lang=" + lang;

					hasCoverPage = true;
					log.debug("After setting headerHtml=" + headerHtml);
					log.debug("After setting coverHtml=" + coverHtml);
				} else if (rootTag.equals("kfapGroupReport") || rootTag.equals("talentGridV2")) {

					webPath = "/projects/kfap/group_slate";
					String groupLabel = (String) xpath.evaluate("groupLabel", root, XPathConstants.STRING);
					String group = (String) xpath.evaluate("group", root, XPathConstants.STRING);
					String headerGroup = "";
					if (group.length() > 0)
						headerGroup = groupLabel + ": " + group;

					group = StringEscapeUtils.unescapeHtml4(group);
					group = replaceXmlSensitiveChar(group);

					String assessmentNameValue = URLEncoder.encode(assessmentName, "UTF-8");
					String currentValue = URLEncoder.encode(targetLevel, "UTF-8");
					String participantValue = URLEncoder.encode(headerGroup, "UTF-8");
					String reportNameValue = URLEncoder.encode(reportName, "UTF-8");

					String perosnalValue = URLEncoder.encode(confidential, "UTF-8");
					String nameValue = URLEncoder.encode(nameLabel, "UTF-8");
					String clientValue = URLEncoder.encode(clientLabel, "UTF-8");
					String groupLabelValue = URLEncoder.encode(groupLabel, "UTF-8");
					String groupNameValue = URLEncoder.encode(group, "UTF-8");
					String clientNameValue = URLEncoder.encode(clientName, "UTF-8");
					String targetValue = URLEncoder.encode(targetLabel, "UTF-8");
					String dateValue = URLEncoder.encode(dateLabel, "UTF-8");
					String targetLevelValue = URLEncoder.encode(targetLevel, "UTF-8");
					String reportDateValue = URLEncoder.encode(reportDate, "UTF-8");

					log.debug("alp=" + assessmentNameValue);
					log.debug("current=" + currentValue);
					log.debug("participant=" + participantValue);
					log.debug("lang=" + lang);
					log.debug("reportname=" + reportNameValue);
					log.debug("perosnalValue=" + perosnalValue);
					log.debug("nameValue=" + nameValue);
					log.debug("clientValue=" + clientValue);
					log.debug("groupLabelValue=" + groupLabelValue);
					log.debug("groupNameValue=" + groupNameValue);
					log.debug("clientNameValue=" + clientNameValue);
					log.debug("targetValue=" + targetValue);
					log.debug("dateValue=" + dateValue);
					log.debug("targetLevelValue=" + targetLevelValue);
					log.debug("reportDateValue=" + reportDateValue);

					headerHtml = baseUrl + sharedWebPath + "/header3line.xhtml?alp=" + assessmentNameValue
							+ "&current=" + currentValue + "&participant=" + participantValue + "&lang=" + lang;

					hasCoverPage = true;
					coverHtml = new String();
					coverHtml = baseUrl + webPath + "/cover.xhtml?reportname=" + reportNameValue + "&assessmentname="
							+ assessmentNameValue + "&personal=" + perosnalValue + "&groupLabel=" + groupLabelValue
							+ "&client=" + clientValue + "&groupname=" + groupNameValue + "&clientname="
							+ clientNameValue + "&target=" + targetValue + "&date=" + dateValue + "&targetname="
							+ targetLevelValue + "&reportdate=" + reportDateValue + "&lang=" + lang;
				}
			}
			log.debug("wkhtmltopdfPath = " + wkhtmltopdfPath);
			log.debug("running exec 1");
			// http://code.google.com/p/wkhtmltopdf/

			// Mark Arnold May 12, 2015
			// get the watermark value which is then put into the PDF meta field
			// title. This is then used as a means of blocking/alerting KF
			// about a report being sent outside of KF

			String watermark = "";
			watermark = configProperties.getProperty("watermark");

			Process coverPageProcess = null;

			if (hasCoverPage) {
				log.debug("Performing cover page process");
				// "--no-pdf-compression", (removed)
				String coverPageWkhtmlToPdfCmd[] = new String[] { wkhtmltopdfPath, "--load-error-handling", "ignore",
						"--no-stop-slow-scripts", "--encoding", "UTF-8", "--quiet", "--dpi", "600", "--title",
						watermark, "--orientation", orientation, "--print-media-type", "--disable-smart-shrinking",
						"--page-size", pageSize, "--margin-top", coverMarginTop, "--margin-bottom", coverMarginBottom,
						"--margin-left", coverMarginSide, "--margin-right", coverMarginSide, "--header-spacing", "0",
						"--footer-spacing", "0", coverHtml, "-" };

				log.debug("coverPageCmdParams are: ");

				for (String coverPageCmdParam : coverPageWkhtmlToPdfCmd) {
					log.debug(coverPageCmdParam);
				}

				coverPageProcess = Runtime.getRuntime().exec(coverPageWkhtmlToPdfCmd);
				// log.debug("Calling coverHtml: " + coverHtml);
				// printXhtmlResponse(coverHtml);

				log.debug("fork stderr thread");

				ProcessErrorStreamHandler coverPageErrorStreamHandler = new ProcessErrorStreamHandler(coverPageProcess);
				Thread coverPageErrorThread = new Thread(coverPageErrorStreamHandler);
				coverPageErrorThread.start();
			}

			log.debug("running exec 2");

			// http://code.google.com/p/wkhtmltopdf/
			log.debug("Performing content pages process");
			// "--no-pdf-compression", (removed)
			String contentPageWkhtmlToPdfCmd[] = new String[] { wkhtmltopdfPath, "--load-error-handling", "ignore",
					"--no-stop-slow-scripts", "--encoding", "UTF-8", "--quiet", "--dpi", "600", "--title", watermark,
					"--orientation", orientation, "--print-media-type", "--disable-smart-shrinking", "--page-size",
					pageSize, "--header-html", headerHtml, "--footer-html", footerHtml, "--margin-top", marginTop,
					"--margin-left", marginSide, "--margin-right", marginSide, "--header-spacing", headerSpacing,
					"--margin-bottom", marginBottom, "--footer-spacing", "0", "-", "-" };

			log.debug("contentPageCmdParams are: ");
			for (String contentPageCmdParam : contentPageWkhtmlToPdfCmd) {
				log.debug(contentPageCmdParam);
			}

			Process contentPagesProcess = Runtime.getRuntime().exec(contentPageWkhtmlToPdfCmd);
			// log.debug("Calling headerHtml: " + headerHtml);
			// printXhtmlResponse(headerHtml);

			log.debug("fork stderr thread");

			ProcessErrorStreamHandler contentPagesErrorStreamHandler = new ProcessErrorStreamHandler(
					contentPagesProcess);
			Thread contentPagesErrorThread = new Thread(contentPagesErrorStreamHandler);
			contentPagesErrorThread.start();

			log.debug("write to process");
			// get byte array - this holds the HTML generated by transform
			byte[] content = resultBuf.toByteArray();

			/*----------*/
			/*
			System.out.println("Writing transform output to C:/Windows/Temp/test.html");
			String str = new String(content, "UTF-8");
			FileWriter fileWriter = null;
			try {
				File newTextFile = new File("c:/Windows/Temp/test.html");
				fileWriter = new FileWriter(newTextFile);
				fileWriter.write(str);
				fileWriter.close();
			} catch (IOException ex) {
				//
			} finally {
				try {
					fileWriter.close();
				} catch (IOException ex) {
					//
				}
			}
			*/
			/*----------*/

			// write the html to wkhtmltopdf process
			// Note that names seem backwards - we write to the output stream
			// This is because naming is from perspective of the process owner
			OutputStream out = contentPagesProcess.getOutputStream();

			out.write(content);

			out.flush();
			out.close();

			log.debug("done writing content");
			log.debug("After passing to contentPagesProcess headerHtml=" + headerHtml);
			Process endPageProcess = null;

			if (hasEndPage) {
				log.debug("Performing end page process");
				// "--no-pdf-compression", (removed)
				String endPageWkhtmlToPdfCmd[] = new String[] { wkhtmltopdfPath, "--load-error-handling", "ignore",
						"--no-stop-slow-scripts", "--encoding", "UTF-8", "--quiet", "--dpi", "600", "--orientation",
						orientation, "--print-media-type", "--disable-smart-shrinking", "--page-size", pageSize,
						"--margin-top", "0", "--margin-bottom", "0", "--margin-left", "0", "--margin-right", "0",
						"--header-spacing", "0", "--footer-spacing", "0", endHtml, "-" };

				log.debug("endPageCmdParams are: ");
				for (String endPageCmdParam : endPageWkhtmlToPdfCmd) {
					log.debug(endPageCmdParam);
				}

				endPageProcess = Runtime.getRuntime().exec(endPageWkhtmlToPdfCmd);

				// log.debug("Calling endHtml: " + endHtml);
				// printXhtmlResponse(endHtml);

				log.debug("fork stderr thread");

				ProcessErrorStreamHandler coverPageErrorStreamHandler = new ProcessErrorStreamHandler(coverPageProcess);
				Thread coverPageErrorThread = new Thread(coverPageErrorStreamHandler);
				coverPageErrorThread.start();

			}

			log.debug("finished endPageProcess");

			try {
				log.debug("Starting merge...");
				PDFMergerUtility ut = new PDFMergerUtility();

				// add cover page
				if (hasCoverPage) {
					log.debug("before ut.addSource cover");
					ut.addSource(coverPageProcess.getInputStream());
					ut.getDestinationFileName();
				}
				// add document body

				ut.addSource(contentPagesProcess.getInputStream());
				//
				if (hasEndPage) {
					log.debug("before ut.addSource end page");
					ut.addSource(endPageProcess.getInputStream());
				}
				log.debug("before ut.setDestinationStream");
				ut.setDestinationStream(outputStream);
				log.debug("before ut.mergeDocuments");
				ut.mergeDocuments();

			} catch (Exception e) {
				log.debug("Error merging PDFs" + e);
			}

			log.debug("done merging");

			outputStream.flush();
			if (tempFiles) {
				for (String p : tempFilesRef) {
					String path = p;
					try {
						File f = new File(path);
						boolean deleted = f.delete();
						log.debug((deleted) ? "File Successfully Deleted - " + path : "Could NOT Delete File! - "
								+ path);
					} catch (SecurityException se) {
						log.debug(("File Deletion Failed : SecurityException : - " + se.getMessage()));
					}
				}
			}

		} catch (IOException ioe) {
			ioe.printStackTrace();
			log.error("IO Exception" + ioe.toString());
		} catch (Exception excp) {
			excp.printStackTrace();
			log.error("general EXCEPTION" + excp.toString());
		} finally {
			// TODO: hmm, can this be cleaned up?
			try {
				outputStream.close();
			} catch (IOException e1) {
				e1.printStackTrace();
				log.error("IO EXCEPTION" + e1.toString());
			}
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					log.error("IO EXCEPTION" + e.toString());
				}
			}

		}

	}

	private void printXhtmlResponse(String url) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(url);

		// add request header
		// request.addHeader("User-Agent", USER_AGENT);
		HttpResponse response;
		StringBuffer result = new StringBuffer();
		try {
			response = client.execute(request);

			log.debug("Response Code : " + response.getStatusLine().getStatusCode());

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		log.debug("http response result is: " + result.toString());
	}

	/**
	 * local helper to do XML escapes
	 * 
	 * @param input
	 *            The string to be escaped
	 * @return the escaped string
	 */
	private String replaceXmlSensitiveChar(String input) {
		String ret = input;
		ret = ret.replaceAll("&", "&amp;");
		ret = ret.replaceAll("<", "&lt;");
		ret = ret.replaceAll(">", "&gt;");

		return ret;
	}

	/**
	 * local helper to get a Transformer
	 * 
	 * @param xsltPath
	 *            path (relative to "com/pdinh/resource") of xslt
	 * @return Transformer built from specified xslt path
	 */
	private Transformer getTransformerFromXslt(String xsltPath, String baseUrl) {

		// get stream for xslt file
		// StringBuilder xslt = new StringBuilder("com/pdinh/resource/");
		// xslt.append(xsltPath);
		// log.debug("in getTransformerFromXslt, path=" + xslt.toString());
		// InputStream xslStream =
		// ReportProcessor.class.getClassLoader().getResourceAsStream(xslt.toString());
		InputStream xslStream = ReportProcessor.class.getClassLoader().getResourceAsStream(xsltPath);

		TransformerFactory factory = TransformerFactory.newInstance();

		Source xslSource = new StreamSource(xslStream);

		Transformer transformer = null;

		try {
			transformer = factory.newTransformer(xslSource);
			transformer.setParameter("baseurl", baseUrl);
		} catch (TransformerConfigurationException e) {
			log.debug("create transformer failed...");
			e.printStackTrace();
		} finally {
			try {
				xslStream.close();
			} catch (IOException e) {
				;
			}
		}
		log.debug("return transformer : success");
		return transformer;

	}

	private Document replaceGenderTokens(Document doc) {
		log.debug("IN replace");
		Transformer tformr = getTransformerFromXslt(TOKEN_REPL_XFORM, "");

		DocumentBuilder builder;
		Document doc2 = null;
		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			doc2 = builder.newDocument();
			Result result = new DOMResult(doc2);
			DOMSource source = new DOMSource(doc);
			tformr.transform(source, result);
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		}
		// writeXmlToConsole(doc2);
		return doc2;
	}

	// This is used only in replaceGenderTokens() and has been commented out...
	// commenting out here
	/*
		// for debugging - write xml dom to a file
		private void writeXmlToConsole(Document doc) {
			log.debug("IN writeXmlToConsole");
			Transformer transformer = null;
			try {
				transformer = TransformerFactory.newInstance().newTransformer();
			} catch (TransformerConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TransformerFactoryConfigurationError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			// to string
			// StreamResult result = new StreamResult(new StringWriter());
			// String xmlString = result.getWriter().toString();
			// log.debug(xmlString);

			// to file
			StreamResult result = new StreamResult(new File("C:/output.xml"));
			DOMSource source = new DOMSource(doc);
			try {
				transformer.transform(source, result);
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	*/

	private void setTransformerCustomParams(Transformer transformer, Map<String, String> customParams) {
		Iterator<Entry<String, String>> iterator = customParams.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, String> entry = iterator.next();
			log.debug("Setting transformer custom param key: " + entry.getKey() + " value: " + entry.getValue());
			transformer.setParameter(entry.getKey(), entry.getValue());
		}
	}

	private String formatEncodedUrl(String baseFstring, String... paramValues) {
		if (paramValues == null || baseFstring == null) {
			return baseFstring;
		}

		List<String> values = new ArrayList<String>();
		for (String value : paramValues) {
			try {
				values.add(URLEncoder.encode(value, "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				values.add(value);
			}
		}

		return String.format(baseFstring, values.toArray());
	}
}
