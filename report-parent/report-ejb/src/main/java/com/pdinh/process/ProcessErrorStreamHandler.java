package com.pdinh.process;

import java.io.IOException;
import java.io.InputStream;

public class ProcessErrorStreamHandler implements Runnable {
	private final Process process;

	public ProcessErrorStreamHandler(Process process) {
		this.process = process;
	}

	@Override
	public void run() {
		InputStream stderr = process.getErrorStream();
		try {
			StringBuffer errBuffer = new StringBuffer();
			int nextChar;
			while ((nextChar = stderr.read()) != -1) {
				errBuffer.append((char) nextChar);
			}
			System.out.println("stderr: " + errBuffer.toString());
		} catch (IOException e) {
			;
		}
	}
}
