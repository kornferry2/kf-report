/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.core.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/*
* This is the common Score object used for reporting.
* 
* This object may have to be enhanced as more data is required by additional client reports.
*/

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "score")
public class Score
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute
	private int value;
	@XmlValue
	private String scoreName;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Score()
	{
		// Default constructor
	}

	// 1-parameter constructor
	public Score(Score orig)
	{
		this.value = orig.value;
		this.scoreName = orig.getScoreName() == null ? null : new String(orig.getScoreName());
	}

	// 2-parameter constructor
	public Score(int score, String name)
	{
		this.value = score;
		this.scoreName = name;
	}
	
	//
	// Instance methods.
	//
	
	public String toString()
	{
		String str = "Score:  value=" + this.value + ", name=" + this.scoreName;
		
		return str;
	}

	// Getters & setters

	//----------------------------------
	public int getValue()
	{
		return this.value;
	}
	
	public void setValue(int value)
	{
		this.value = value;
	}

	//----------------------------------
	public String getScoreName()
	{
		return this.scoreName;
	}
	
	public void setScoreName(String value)
	{
		this.scoreName = value;
	}
}
