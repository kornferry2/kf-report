/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.core.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/*
 * This is the common Hyperlink object used for reporting.
 * 
 * This object may be enhanced if more data is required but for
 * now it contains just display text an a URL
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"displayText", "url"})
public class Hyperlink
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String displayText;
	private String url;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Hyperlink()
	{
	}

	// 2-parameter constructor
	public Hyperlink(String txt, String url)
	{
		this.displayText = txt;
		this.url = url;
	}

	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "Hyperlink:  ";
		str += "displayText=" + this.displayText + ", url=" + this.url + "  ";
		
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public String getDisplayText()
	{
		return this.displayText;
	}

	public void setDisplayText(String value)
	{
		this.displayText = value;
	}

	//----------------------------------
	public String getUrl()
	{
		return this.url;
	}

	public void setUrl(String value)
	{
		this.url = value;
	}

}
