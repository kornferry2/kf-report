/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.core.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/*
* This is the common Project object used for reporting.
* 
* This object may have to be enhanced as more data is required by additional client reports.
*/

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "project")
public class Project
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute
	private int id;
	@XmlValue
	private String name;
	
	//
	// Constructors.
	//

	// 0-parameter constructor
	public Project()
	{
		// Default constructor
	}

	// 1-parameter constructor
	public Project(Project orig)
	{
		this.id = orig.id;
		this.name = orig.getName() == null ? null : new String(orig.getName());
	}

	// 2-parameter constructor
	public Project(int id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	//
	// Instance methods.
	//
	
	public String toString()
	{
		String str = "Project:  id=" + this.id + ", name=" + this.name;
		
		return str;
	}

	// Getters & setters

	//----------------------------------
	public int getId()
	{
		return this.id;
	}
	
	public void setId(int value)
	{
		this.id = value;
	}

	//----------------------------------
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String value)
	{
		this.name = value;
	}

}
