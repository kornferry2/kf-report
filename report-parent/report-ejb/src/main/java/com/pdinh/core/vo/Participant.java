/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdinh.core.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/*
 * This is the common Participant object used for reporting.
 * 
 * This object may have to be enhanced as more data is required by additional client reports.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"firstName", "lastName", "title"})
public class Participant
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	@XmlAttribute
	private int id;
	@XmlAttribute
	private String extId;
	private String firstName;
	private String lastName;
	private String title;

	//
	// Constructors.
	//

	// 0-parameter constructor
	public Participant()
	{
	}

	// 1-parameter constructor (clone)
	public Participant(Participant orig)
	{
		this.id = orig.getId();
		this.extId = new String(orig.getExtId());
		this.firstName = new String(orig.getFirstName());
		this.lastName = new String(orig.getLastName());
		this.title = new String(orig.getTitle());
	}
	
	//
	//
	// Instance methods.
	//

	public String toString()
	{
		String str = "Participant:  ";
		str += "id=" + this.id + ", extIid=" + this.extId + ", name=" + this.firstName + " " + this.lastName + ", title=" + this.title + "  ";
		
		return str;
	}
	
	// Getters & setters

	//----------------------------------
	public int getId()
	{
		return this.id;
	}

	public void setId(int value)
	{
		this.id = value;
	}

	//----------------------------------
	public String getExtId()
	{
		return this.extId;
	}

	public void setExtId(String value)
	{
		this.extId = value;
	}

	//----------------------------------
	public String getFirstName()
	{
		return this.firstName;
	}

	public void setFirstName(String value)
	{
		this.firstName = value;
	}

	//----------------------------------
	public String getLastName()
	{
		return this.lastName;
	}

	public void setLastName(String value)
	{
		this.lastName = value;
	}

	//----------------------------------
	public String getTitle()
	{
		return this.title;
	}

	public void setTitle(String value)
	{
		this.title = value;
	}
}
