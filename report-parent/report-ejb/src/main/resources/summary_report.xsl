<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xhtml [ 
<!ENTITY nbsp "&#160;"> 
]>
<xsl:stylesheet version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xslt"
	xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xsl xalan xs">

	<!-- http://stackoverflow.com/questions/9538619/xslt-refuses-to-write-doctype-declaration -->
	<xsl:output method="xml" indent="yes" encoding="UTF-8"
		doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />

	<xsl:param name="baseurl" select="'http://localhost'" />

	<!-- these two not used? -->
	<xsl:param name="genmode" select="'pdf'" />

	<!-- no default match -->
	<xsl:template match="*" />

	<xsl:template match="/coachingSummaryReport">

		<html>
			<head>
				<title>
					<xsl:value-of select="reportTitle"></xsl:value-of>
				</title>
				<!-- <link href="{$baseurl}/core/css/font_{$reportLang}.css" rel="stylesheet" 
					type="text/css" /> -->
				<link href="{$baseurl}/core/css/report.css" rel="stylesheet"
					type="text/css" />
				<link href="{$baseurl}/projects/coaching/summary/report.css"
					rel="stylesheet" type="text/css" />
				<meta name="baseurl" content="{$baseurl}" />
				<meta name="genmode" content="{$genmode}" />
			</head>
			<body>
				<div class="section">
					<p class="s14">
						<xsl:value-of select="companyName"></xsl:value-of>
					</p>
					<p class="s14">
						<xsl:value-of select="reportDate"></xsl:value-of>
					</p>
					<br />
					<br />
					<table class="info-table align-bottom-td">
						<tr>
							<td class="s11 all-uppercase">
								<xsl:value-of select="totalParticipantsLabel"></xsl:value-of>
							</td>
							<td class="s3">
								<xsl:value-of select="totalParticipants"></xsl:value-of>
							</td>
						</tr>
						<tr>
							<td width="38%" class="s11 all-uppercase">
								<xsl:value-of select="averageEngagementLabel"></xsl:value-of>
							</td>
							<td width="62%" class="s3">
								<xsl:value-of select="averageEngagement"></xsl:value-of>
							</td>
						</tr>
					</table>
					<br />
					<br />
				</div>
				<xsl:apply-templates select="statusSection" />
				<xsl:apply-templates select="averageProgressSection" />
				<xsl:apply-templates select="competencyProgressSection" />
			</body>
		</html>
	</xsl:template>


	<xsl:template match="statusSection">

		<!-- statusSection -->


		<div class="section">
			<h1>
				<xsl:value-of select="title"></xsl:value-of>
			</h1>
			<hr />
			<table class="coach-plan-status-table">
				<tr class="item-top-border">
					<td class="border-right" width="30%">
						<table>
							<tr>
								<td class="s11 s15 column-1-label">
									<xsl:value-of select="notStartedLabel"></xsl:value-of>
								</td>
								<td class="column-2-value">
									<xsl:value-of select="notStarted"></xsl:value-of>
								</td>
							</tr>
							<tr>
								<td class="s11 s15">
									<xsl:value-of select="inProgressLabel"></xsl:value-of>
								</td>
								<td class="column-2-value">
									<xsl:value-of select="inProgress"></xsl:value-of>
								</td>
							</tr>
							<tr>
								<td class="s11 s15">
									<xsl:value-of select="completeLabel"></xsl:value-of>
								</td>
								<td class="column-2-value">
									<xsl:value-of select="complete"></xsl:value-of>
								</td>
							</tr>
						</table>
					</td>
					<td class="status-sum-column-2">&nbsp;
					</td>
					<td width="67%" align="right" class="item-column-3">
						<table>
							<tr>
								<td width="321" class="s11 s15 column-1-label">
									<xsl:value-of select="totalCompletedMeetingsLabel"></xsl:value-of>
								</td>
								<td class="column-2-value">
									<xsl:value-of select="totalCompletedMeetings"></xsl:value-of>
								</td>
							</tr>
							<tr>
								<td class="s11 s15 column-1-label td-indent">
									<xsl:value-of select="inPersonMeetingPercentageLabel"></xsl:value-of>
								</td>
								<td class="column-2-value">
									<xsl:value-of select="inPersonMeetingPercentage"></xsl:value-of>%
								</td>
							</tr>
							<tr>
								<td class="s11 s15 column-1-label td-indent">
									<xsl:value-of select="phoneMeetingPercentageLabel"></xsl:value-of>
								</td>
								<td class="column-2-value">
									<xsl:value-of select="phoneMeetingPercentage"></xsl:value-of>%
								</td>
							</tr>
							<tr>
								<td class="s11 s15 column-1-label">
									<xsl:value-of select="averageCompletedMeetingsPerParticipantLabel"></xsl:value-of>
								</td>
								<td class="column-2-value">
									<xsl:value-of select="averageCompletedMeetingsPerParticipant"></xsl:value-of>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br />
			<br />
		</div>
	</xsl:template>
	<xsl:template match="averageProgressSection">
		<!-- averageProgressSection -->

		<div class="section status">
			<h1>
				<xsl:value-of select="title"></xsl:value-of>
				<br />
			</h1>
			<hr />

			<table class="status-table">
				<tr class="noheight">
					<td class="status-column-1 noborders">&nbsp;
					</td>
					<td class="status-column-2 noborders">&nbsp;
					</td>
					<td class="status-spacer-column noborders" align="right">&nbsp;
					</td>
					<td class="status-column-3 noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-spacer-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
				</tr>
				<tr>
					<th class="status-column-1b noborders">&nbsp;
					</th>
					<th class="status-column-2 noborders">&nbsp;
					</th>
					<th class="status-column-3 noborders">&nbsp;
					</th>
					<th class="s11 noborders" colspan="13"></th>
				</tr>
				<tr>
					<th class="status-column-1 s11">
						<span class="special-multicolumn-header-single"><xsl:value-of select="timeElapsedHeader"></xsl:value-of></span>
					</th>
					<th class="status-column-2 s11">
						<div class="special-multicolumn-header-border-block">&nbsp;</div>
						<span class="special-multicolumn-header"><xsl:value-of select="selfReportedHeader"></xsl:value-of></span>
						<span class="special-multicolumn-header-single"><xsl:value-of select="inProgressCompleteHeader"></xsl:value-of></span>
					</th>
					<th class="status-spacer-column" align="right">&nbsp;
					</th>
					<th class="status-column-3"></th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">10</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">20</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">30</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">40</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">50</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">60</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">70</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">80</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">90</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">100</span></p>
					</th>
					<th class="status-spacer-column" align="right">&nbsp;
					</th>
					<th class="status-percentage-column" align="left">
						<p class="s11">
							<span class="special-multicolumn-header-single"><xsl:value-of select="notStartedHeader"></xsl:value-of></span>
						</p>
					</th>
				</tr>

				<xsl:for-each select="quartiles">
					<xsl:apply-templates select="quartile" />
				</xsl:for-each>

			</table>
			<br />
			<br />
			<p class="footnote">
				<xsl:value-of select="inProgressCompleteFootnote"></xsl:value-of>
			</p>
		</div>
		<!-- END OVERALL PROGRESS TABLE -->
	</xsl:template>

	<xsl:template match="quartile">
		<tr>
			<td class="status-column-1">
				<p class="s8">
					<xsl:value-of select="timeElapsedLabel"></xsl:value-of>
				</p>
			</td>
			<td class="status-column-2">
				<p class="s9">
					<xsl:value-of select="inProgressComplete"></xsl:value-of>
				</p>
			</td>
			<td class="status-spacer-column" align="right">&nbsp;
			</td>
			<td class="status-column-3 center">
				<p class="s9">
					<xsl:value-of select="averageProgress"></xsl:value-of>%
				</p>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 10">
						<div class="status-green"><!-- value == 10 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 20">
						<div class="status-green"><!-- value == 20 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 30">
						<div class="status-green"><!-- value == 30 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 40">
						<div class="status-green"><!-- value == 40 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 50">
						<div class="status-green"><!-- value == 50 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 60">
						<div class="status-green"><!-- value == 60 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 70">
						<div class="status-green"><!-- value == 70 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 80">
						<div class="status-green"><!-- value == 80 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 90">
						<div class="status-green"><!-- value == 90 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 100">
						<div class="status-green"><!-- value == 100 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-spacer-column">&nbsp;
			</td>
			<td class="status-percentage-column">
				<xsl:value-of select="notStarted"></xsl:value-of>
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="competencyProgressSection">
		<div class="section statusb">
			<h1>
				<xsl:value-of select="title"></xsl:value-of>
				<br />
			</h1>
			<hr />
			<table class="status-table">
				<tr class="noheight">
					<td class="status-column-1b noborders">&nbsp;
					</td>
					<td class="status-column-2 noborders">&nbsp;
					</td>
					<td class="status-spacer-column noborders" align="right">&nbsp;
					</td>
					<td class="status-column-3 noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
					<td class="status-percentage-column noborders">&nbsp;
					</td>
				</tr>
				<tr>
					<td class="status-column-1b noborders">&nbsp;
					</td>
					<td class="status-column-2 noborders">&nbsp;
					</td>
					<td class="status-column-3 noborders">&nbsp;
					</td>
					<td class="s11" colspan="11 noborders"></td>
				</tr>
				<tr>
					<th class="status-column-1b s11">
						<span class="special-multicolumn-header-single"><xsl:value-of select="competencyHeader"></xsl:value-of></span>
					</th>
					<th class="status-column-2">
						<span class="s11">
							<div class="special-multicolumn-header-border-block header-border-block-b">&nbsp;</div>
							<span class="special-multicolumn-header special-multicolumn-header-b"><xsl:value-of select="selfReportedProgressHeader"></xsl:value-of></span>
							<span class="special-multicolumn-header-single"><xsl:value-of select="inProgressCompleteHeader"></xsl:value-of></span>
						</span>
					</th>
					<th class="status-spacer-column" align="right">&nbsp;
					</th>
					<th class="status-column-3">&nbsp;
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">10</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">20</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">30</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">40</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">50</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">60</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">70</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">80</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">90</span></p>
					</th>
					<th class="status-percentage-column" align="right">
						<p class="s11 status-column-header percent-label"><span class="special-multicolumn-header-single">100</span></p>
					</th>
				</tr>
				<xsl:for-each select="competencies">
					<xsl:apply-templates select="competency" />
				</xsl:for-each>
			</table>
			<br />
			<p class="footnote">
				<xsl:value-of select="inProgressCompleteFootnote"></xsl:value-of>
			</p>
			<br />
		</div>

	</xsl:template>
	<xsl:template match="competency">
		<tr>
			<td class=" status-column-1b">
				<p class="s8">
					<xsl:value-of select="name"></xsl:value-of>
				</p>
			</td>
			<td class=" status-column-2" align="left">
				<p class="s9">
					<xsl:value-of select="inProgressComplete"></xsl:value-of>
				</p>
			</td>
			<td class="status-spacer-column" align="right">&nbsp;
			</td>
			<td class=" status-column-3 center" align="left">
				<span class="s9">
					<xsl:value-of select="averageProgress"></xsl:value-of>%
				</span>
			</td>
			<!-- ******************************************************************************************* 
				in td's below: don't insert div or hide WHEN % is less than the value ********************************************************************************************* -->
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 10">
						<div class="status-green"><!-- value == 10 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 20">
						<div class="status-green"><!-- value == 20 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 30">
						<div class="status-green"><!-- value == 30 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 40">
						<div class="status-green"><!-- value == 40 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 50">
						<div class="status-green"><!-- value == 50 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 60">
						<div class="status-green"><!-- value == 60 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 70">
						<div class="status-green"><!-- value == 70 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 80">
						<div class="status-green"><!-- value == 80 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 90">
						<div class="status-green"><!-- value == 90 -->
						</div>
					</xsl:if>
				</div>
			</td>
			<td class="status-percentage-column">
				<div style="position:relative;">
					<xsl:if test="averageProgress >= 100">
						<div class="status-green"><!-- value == 100 -->
						</div>
					</xsl:if>
				</div>
			</td>
		</tr>
	</xsl:template>
</xsl:stylesheet>
