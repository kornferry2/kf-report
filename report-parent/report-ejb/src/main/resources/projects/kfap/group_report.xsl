<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xhtml [ 
<!ENTITY nbsp "&#160;"> 
]>
<xsl:stylesheet version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xslt"
	xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xsl xalan xs">
	<!-- http://stackoverflow.com/questions/9538619/xslt-refuses-to-write-doctype-declaration -->
	<xsl:output method="html" indent="yes" encoding="UTF-8"
		doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />
	<xsl:param name="baseurl" select="'http://localhost'" />
	<!-- these two not used? -->
	<xsl:param name="genmode" select="'pdf'" />
	<!-- no default match -->
	<xsl:template match="*" />
	<!-- ############################ ALP/KFP GROUP SLATE REPORT ############################ -->
	<xsl:template match="/kfapGroupReport">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>Group Report</title>
				<link href="{$baseurl}/core/css/report.css" rel="stylesheet"
					type="text/css" />

				<link href="{$baseurl}/core/css/kfIcon.css" rel="stylesheet"
					type="text/css" />

				<link href="{$baseurl}/projects/kfap/shared/css/report.css"
					rel="stylesheet" type="text/css" />
				<link href="{$baseurl}/projects/kfap/group_slate/css/report.css"
					rel="stylesheet" type="text/css" />


				<meta name="baseurl" content="{$baseurl}" />
				<meta name="genmode" content="{$genmode}" />
			</head>
			<body>
				<xsl:apply-templates select="aboutSection"></xsl:apply-templates>
				<xsl:apply-templates select="detailedResultsSection"></xsl:apply-templates>
				<xsl:apply-templates select="definitionsSection"></xsl:apply-templates>

			</body>
		</html>
	</xsl:template>
	<xsl:template match="aboutSection">
		<h1>
			<xsl:value-of select="title" disable-output-escaping="yes" />
		</h1>
		<p>
			<xsl:value-of select="p1" disable-output-escaping="yes" />
		</p>
		<table class="about-table">
			<tr>
				<td class="kf-icon about-icon icon-green">&#61473;</td>
				<td>
					<xsl:value-of select="strongIndicator"
						disable-output-escaping="yes" />
				</td>
			</tr>
			<tr>
				<td colspan="2" class="about-line"></td>
			</tr>
			<tr>
				<td class="kf-icon about-icon icon-yellow">&#61474;</td>
				<td>
					<xsl:value-of select="developmentIndicator"
						disable-output-escaping="yes" />
				</td>
			</tr>
			<tr>
				<td colspan="2" class="about-line"></td>
			</tr>
			<tr>
				<td class="kf-icon about-icon small-icon">&#61487;</td>
				<td>
					<xsl:value-of select="optimalIndicator"
						disable-output-escaping="yes" />
				</td>
			</tr>
			<tr>
				<td colspan="2" class="about-line"></td>
			</tr>
			<tr>
				<td class="kf-icon about-icon small-icon">
					<span style="position:relative; bottom:7px;">&#61489;</span>
				</td>
				<td>
					<xsl:value-of select="belowOptimalIndicator"
						disable-output-escaping="yes" />
				</td>
			</tr>
			<tr>
				<td colspan="2" class="about-line"></td>
			</tr>
			<tr>
				<td class="kf-icon about-icon small-icon">
					<span style="position:relative; top:2px;">&#61488;</span>
				</td>
				<td>
					<xsl:value-of select="aboveOptimalIndicator"
						disable-output-escaping="yes" />
				</td>
			</tr>
			<tr>
				<td colspan="2" class="about-line"></td>
			</tr>
			<tr>
				<td class="kf-icon about-icon icon-red">&#61486;</td>
				<td>
					<xsl:value-of select="derailerIndicator"
						disable-output-escaping="yes" />
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template match="detailedResultsSection">
		<xsl:variable name="incompleteLabel" select="incompleteLabel"></xsl:variable>
		<div class="detailed">
			<h1>
				<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
			</h1>
			<!-- I choose to have two different tables (Raven / No Ravens) as opposed to 
			     lots of conditional formating.  This may mean having to update in two places.
			     Hopefully there won't be lots of changes to common elements.  
			     The name column uses a different css attribute depending on the table, and the SVG 
			     locations of the header and footer are mostly different between the two tables -->
			<xsl:choose>
			<xsl:when test="../noRavens = 'true'">
			<!-- Table displayed when there is no ravens for the project -->
			<table class="results-table" cellpadding="0" cellspacing="0">
				
				<thead>
					<tr>
						<th height="151" colspan="36">
							<!-- Create header in SVG so it will repeat in wkhtmltopdf -->
							<svg height="151" width="700">

								<!-- Needs and Strong Icons -->
								<text x="6" y="149" font-family="Korn Ferry Report Icons"
									font-size="26" fill="#f8d436">
									&#61474; </text>
								<text x="38" y="149" font-family="Korn Ferry Report Icons"
									font-size="26" fill="#00ab87">
									&#61473; </text>

								<!-- Needs, Strong, Development Effort Text -->
								<text transform="rotate(-90, 40, 40)" x="-43" y="20"
									font-family="Arial Unicode MS" font-size="10" fill="#555d5c">
									<xsl:value-of select="needsLabel"
										disable-output-escaping="yes"></xsl:value-of>
									<tspan x="-43" y="51">
										<xsl:value-of select="strengthLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="82">
										<xsl:value-of select="developmentEffortLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
								</text>

								<!-- Interest blue backgrounds -->
								<rect x="380" y="15" width="33" height="133" fill="#D6E9F8"></rect>
								<rect x="366" y="0" width="14" height="148" fill="#13ACE3"></rect>

								<!-- Experience blue backgrounds -->
								<rect x="433" y="15" width="33" height="133" fill="#D6E9F8"></rect>
								<rect x="419" y="0" width="14" height="148" fill="#13ACE3"></rect>

								<!-- Awareness blue backgrounds -->
								<rect x="486" y="15" width="22" height="133" fill="#D6E9F8"></rect>
								<rect x="472" y="0" width="14" height="148" fill="#13ACE3"></rect>

								<!-- Learning Agility blue backgrounds -->
								<rect x="528" y="15" width="44" height="133" fill="#D6E9F8"></rect>
								<rect x="514" y="0" width="14" height="148" fill="#13ACE3"></rect>

								<!-- LeadershipTraits blue backgrounds -->
								<rect x="592" y="15" width="55" height="133" fill="#D6E9F8"></rect>
								<rect x="578" y="0" width="14" height="148" fill="#13ACE3"></rect>

								<!-- Derailment blue backgrounds -->
								<rect x="667" y="15" width="33" height="133" fill="#D6E9F8"></rect>
								<rect x="653" y="0" width="14" height="148" fill="#13ACE3"></rect>

								<!-- Icons for Signposts -->
								<text x="368" y="14" font-family="Korn Ferry Report Icons"
									font-size="15" fill="white">
									&#61480;
									<tspan x="421" y="14">&#61481;</tspan>
									<tspan x="474" y="14">&#61482;</tspan>
									<tspan x="516" y="14">&#61483;</tspan>
									<tspan x="579" y="14">&#61484;</tspan>
									<tspan x="656" y="14">&#61486;</tspan>
								</text>

								<!-- Signpost Labels -->
								<text transform="rotate(-90, 40, 40)" x="-66" y="377"
									font-family="Arial Unicode MS" font-size="12" fill="white">
									<xsl:value-of select="interestLabel"
										disable-output-escaping="yes"></xsl:value-of>
									<tspan x="-66" y="429">
										<xsl:value-of select="experienceLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="483">
										<xsl:value-of select="selfAwarenessLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="524">
										<xsl:value-of select="learningAgilityLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="588">
										<xsl:value-of select="leadershipTraitsLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="664">
										<xsl:value-of select="derailmentRisksLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
								</text>

								<!-- Sub Category Labels -->
								<text transform="rotate(-90, 40, 40)" x="-66" y="388"
									font-family="Arial Unicode MS" font-size="8" fill="black">
									<xsl:value-of select="driveLabel"
										disable-output-escaping="yes"></xsl:value-of>
									<tspan x="-66" y="399">
										<xsl:value-of select="focusLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="409">
										<xsl:value-of select="engagementLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="441">
										<xsl:value-of select="coreLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="451">
										<xsl:value-of select="perspectiveLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="462">
										<xsl:value-of select="keyChallengesLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="494">
										<xsl:value-of select="selfLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="505">
										<xsl:value-of select="situationalLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="536">
										<xsl:value-of select="peopleLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="547">
										<xsl:value-of select="resultsLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="558">
										<xsl:value-of select="mentalLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="568">
										<xsl:value-of select="changeLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="601">
										<xsl:value-of select="focusedLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="612">
										<xsl:value-of select="sociableLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="622">
										<xsl:value-of select="toleranceLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="633">
										<xsl:value-of select="composedLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="644">
										<xsl:value-of select="optimisticLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="675">
										<xsl:value-of select="volatileLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="686">
										<xsl:value-of select="microManageLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="697">
										<xsl:value-of select="closedLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
								</text>
							</svg>
						</th>
					</tr>
					<tr class="separated-hz-line">
						<th colspan="4" class="hz-line"></th>
						<th class="blank"></th>
						<th colspan="4" class="hz-line"></th>
						<th class="blank"></th>
						<th colspan="4" class="hz-line"></th>
						<th class="blank"></th>
						<th colspan="3" class="hz-line"></th>
						<th class="blank"></th>
						<th colspan="5" class="hz-line"></th>
						<th class="blank"></th>
						<th colspan="6" class="hz-line"></th>
						<th class="blank"></th>
						<th colspan="4" class="hz-line"></th>
					</tr>
				</thead>
				 
				<tbody>
					<xsl:for-each-group select="tables/table/rows/row"
						group-by="strengthCount">
						<xsl:sort select="current-grouping-key()" order="descending"
							data-type="number" />
						<xsl:for-each-group select="current-group()"
							group-by="devEffortScore">
							<xsl:sort select="devEffortScore" data-type="number"></xsl:sort>
							<xsl:for-each select="current-group()">
								<xsl:sort select="lastNameFirstName" />
								<xsl:if test="position() != 1">
									<!-- the following goes before each row, execpt the one after a 
										full-horizontal line row -->
									<tr class="separated-hz-line">
										<td colspan="4" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="4" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="4" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="3" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="5" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="6" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="4" class="hz-line"></td>
									</tr>
								</xsl:if>

								<tr>
									<td colspan="2" class="bar-col">
										<xsl:for-each select="1 to needsCount">
											<span class="bar need-bar"></span>
										</xsl:for-each>
										<xsl:for-each select="1 to strengthCount">
											<span class="bar strong-bar"></span>
										</xsl:for-each>
										<xsl:if test="devEffortScore = '101'">
											<span class="incomplete">
												<xsl:value-of select="$incompleteLabel"></xsl:value-of>
											</span>
										</xsl:if>
									</td>
									<td class="grey-col score-col">
										<xsl:if test="devEffortScore != '101'">
											<xsl:value-of select="devEffortScore"></xsl:value-of>
										</xsl:if>
									</td>
									<td class="grey-col name-col-ravens">
										<xsl:value-of select="lastNameFirstName" />
									</td>
									<td class="blank"></td>
									<td class="cat-1-1 grey-col">
										<xsl:call-template name="isStrength">
											<xsl:with-param name="value" select="isInterestStregth"></xsl:with-param>
											<xsl:with-param name="des" select="devEffortScore"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-1-2">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="driveValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-1-3">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="focusValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-1-4">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="engagementValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="blank"></td>
									<td class="cat-2-1 grey-col">
										<xsl:call-template name="isStrength">
											<xsl:with-param name="value" select="isExperienceStregth"></xsl:with-param>
											<xsl:with-param name="des" select="devEffortScore"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-2-2">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="coreValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-2-3">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="perspectiveValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-2-4">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="keyChallengesValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="blank"></td>
									<td class="cat-3-1 grey-col">
										<xsl:call-template name="isStrength">
											<xsl:with-param name="value" select="isSelfAwarenessStregth"></xsl:with-param>
											<xsl:with-param name="des" select="devEffortScore"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-3-2">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="selfValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-3-3">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="situationalValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="blank"></td>
									<td class="cat-4-1 grey-col">
										<xsl:call-template name="isStrength">
											<xsl:with-param name="value"
												select="isLearningAgilityStregth"></xsl:with-param>
											<xsl:with-param name="des" select="devEffortScore"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-4-2">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="peopleValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-4-3">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="resultsValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-4-4">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="mentalValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-4-5">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="changeValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="blank"></td>
									<td class="cat-5-1 grey-col">
										<xsl:call-template name="isStrength">
											<xsl:with-param name="value"
												select="isLeadershipTraitsStregth"></xsl:with-param>
											<xsl:with-param name="des" select="devEffortScore"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-5-2">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="focusedValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-5-3">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="sociableValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-5-4">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="toleranceValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-5-5">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="composedValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-5-6">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="optimisticValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="blank"></td>
									<td class="cat-7-1 grey-col">
										<xsl:call-template name="isStrength">
											<xsl:with-param name="value"
												select="isDerailmentRisksStregth"></xsl:with-param>
											<xsl:with-param name="des" select="devEffortScore"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-7-2">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="volatileValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-7-3">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="microManageValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-7-4">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="closedValue"></xsl:with-param>
										</xsl:call-template>
									</td>
								</tr>
								<xsl:if test="devEffortScore != '101'">
								<tr class="separated-hz-line">
										<td colspan="4" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="4" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="4" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="3" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="5" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="6" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="4" class="hz-line"></td>
									</tr>
								</xsl:if>
							</xsl:for-each>
						</xsl:for-each-group>
						<xsl:if test="position() != last()">
							<tr>
								<td colspan="36" class="full-hz-line"></td>
							</tr>
						</xsl:if>
					</xsl:for-each-group>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="36" class="full-hz-line-2"></th>
					</tr>
					<tr>
						<th height="20" colspan="36">
							<!-- Create footer in SVG so you can line up text with icons so it 
								will repeat in wkhtmltopdf -->

							<svg height="20" width="700">

								<text x="370" y="12" font-family="Arial Unicode MS"
									font-size="9" fill="black">
									<xsl:value-of select="optimalLabel"></xsl:value-of>
									<tspan x="436" y="12">
										<xsl:value-of select="belowOptimalLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="530" y="12">
										<xsl:value-of select="aboveOptimalLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="622" y="12">
										<xsl:value-of select="derailerLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
								</text>

								<text x="406" y="15" font-family="Korn Ferry Report Icons"
									font-size="18" fill="black">
									&#61487; 
									<tspan x="501" y="15">&#61489;</tspan>
									<tspan x="597" y="15">&#61488;</tspan>
									</text>
									
									<text x="694" y="12" font-family="Korn Ferry Report Icons"
									font-size="12" fill="#da002e">
									&#61486; 
								</text>
							</svg>
						</th>
					</tr>
				</tfoot>
			</table>
			</xsl:when>
			<xsl:otherwise>
			<!-- Table displayed when Ravens is part of project -->
			
					<table class="results-table" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th height="151" colspan="39">
							<!-- Create header in SVG so it will repeat in wkhtmltopdf -->
							<svg height="151" width="700">

								<!-- Needs and Strong Icons -->
								<text x="6" y="149" font-family="Korn Ferry Report Icons"
									font-size="26" fill="#f8d436">
									&#61474; </text>
								<text x="38" y="149" font-family="Korn Ferry Report Icons"
									font-size="26" fill="#00ab87">
									&#61473; </text>

								<!-- Needs, Strong, Development Effort Text -->
								<text transform="rotate(-90, 40, 40)" x="-43" y="20"
									font-family="Arial Unicode MS" font-size="10" fill="#555d5c">
									<xsl:value-of select="needsLabel"
										disable-output-escaping="yes"></xsl:value-of>
									<tspan x="-43" y="51">
										<xsl:value-of select="strengthLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="82">
										<xsl:value-of select="developmentEffortLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
								</text>

								<!-- Interest blue backgrounds -->
								<rect x="349" y="15" width="33" height="133" fill="#D6E9F8"></rect>
								<rect x="335" y="0" width="14" height="148" fill="#13ACE3"></rect>

								<!-- Experience blue backgrounds -->
								<rect x="402" y="15" width="33" height="133" fill="#D6E9F8"></rect>
								<rect x="388" y="0" width="14" height="148" fill="#13ACE3"></rect>

								<!-- Awareness blue backgrounds -->
								<rect x="455" y="15" width="22" height="133" fill="#D6E9F8"></rect>
								<rect x="441" y="0" width="14" height="148" fill="#13ACE3"></rect>

								<!-- Learning Agility blue backgrounds -->
								<rect x="497" y="15" width="44" height="133" fill="#D6E9F8"></rect>
								<rect x="483" y="0" width="14" height="148" fill="#13ACE3"></rect>

								<!-- LeadershipTraits blue backgrounds -->
								<rect x="561" y="15" width="55" height="133" fill="#D6E9F8"></rect>
								<rect x="547" y="0" width="14" height="148" fill="#13ACE3"></rect>

								<!-- Capacity blue backgrounds -->
								<rect x="636" y="15" width="11" height="133" fill="#D6E9F8"></rect>
								<rect x="622" y="0" width="14" height="148" fill="#13ACE3"></rect>

								<!-- Derailment blue backgrounds -->
								<rect x="667" y="15" width="33" height="133" fill="#D6E9F8"></rect>
								<rect x="653" y="0" width="14" height="148" fill="#13ACE3"></rect>

								<!-- Icons for Signposts -->
								<text x="337" y="14" font-family="Korn Ferry Report Icons"
									font-size="15" fill="white">
									&#61480;
									<tspan x="390" y="14">&#61481;</tspan>
									<tspan x="443" y="14">&#61482;</tspan>
									<tspan x="485" y="14">&#61483;</tspan>
									<tspan x="548" y="14">&#61484;</tspan>
									<tspan x="623" y="14">&#61485;</tspan>
									<tspan x="656" y="14">&#61486;</tspan>
								</text>

								<!-- Signpost Labels -->
								<text transform="rotate(-90, 40, 40)" x="-66" y="346"
									font-family="Arial Unicode MS" font-size="12" fill="white">
									<xsl:value-of select="interestLabel"
										disable-output-escaping="yes"></xsl:value-of>
									<tspan x="-66" y="398">
										<xsl:value-of select="experienceLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="452">
										<xsl:value-of select="selfAwarenessLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="493">
										<xsl:value-of select="learningAgilityLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="557">
										<xsl:value-of select="leadershipTraitsLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="632">
										<xsl:value-of select="capacityLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="664">
										<xsl:value-of select="derailmentRisksLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
								</text>

								<!-- Sub Category Labels -->
								<text transform="rotate(-90, 40, 40)" x="-66" y="357"
									font-family="Arial Unicode MS" font-size="8" fill="black">
									<xsl:value-of select="driveLabel"
										disable-output-escaping="yes"></xsl:value-of>
									<tspan x="-66" y="368">
										<xsl:value-of select="focusLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="378">
										<xsl:value-of select="engagementLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="410">
										<xsl:value-of select="coreLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="420">
										<xsl:value-of select="perspectiveLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="431">
										<xsl:value-of select="keyChallengesLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="463">
										<xsl:value-of select="selfLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="474">
										<xsl:value-of select="situationalLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="505">
										<xsl:value-of select="peopleLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="516">
										<xsl:value-of select="resultsLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="527">
										<xsl:value-of select="mentalLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="537">
										<xsl:value-of select="changeLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="569">
										<xsl:value-of select="focusedLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="580">
										<xsl:value-of select="sociableLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="590">
										<xsl:value-of select="toleranceLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="601">
										<xsl:value-of select="composedLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="612">
										<xsl:value-of select="optimisticLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="644">
										<xsl:value-of select="problemSolveLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="675">
										<xsl:value-of select="volatileLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="686">
										<xsl:value-of select="microManageLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="-66" y="697">
										<xsl:value-of select="closedLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
								</text>
							</svg>
						</th>
					</tr>
					<tr class="separated-hz-line">
						<th colspan="4" class="hz-line"></th>
						<th class="blank"></th>
						<th colspan="4" class="hz-line"></th>
						<th class="blank"></th>
						<th colspan="4" class="hz-line"></th>
						<th class="blank"></th>
						<th colspan="3" class="hz-line"></th>
						<th class="blank"></th>
						<th colspan="5" class="hz-line"></th>
						<th class="blank"></th>
						<th colspan="6" class="hz-line"></th>
						<th class="blank"></th>
						<th colspan="2" class="hz-line"></th>
						<th class="blank"></th>
						<th colspan="4" class="hz-line"></th>
					</tr>
				</thead>
				<tbody>
					<xsl:for-each-group select="tables/table/rows/row"
						group-by="strengthCount">
						<xsl:sort select="current-grouping-key()" order="descending"
							data-type="number" />
						<xsl:for-each-group select="current-group()"
							group-by="devEffortScore">
							<xsl:sort select="devEffortScore" data-type="number"></xsl:sort>
							<xsl:for-each select="current-group()">
								<xsl:sort select="lastNameFirstName" />
								<xsl:if test="position() != 1">
									<!-- the following goes before each row, execpt the one after a 
										full-horizontal line row -->
									<tr class="separated-hz-line">
										<td colspan="4" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="4" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="4" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="3" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="5" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="6" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="2" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="4" class="hz-line"></td>
									</tr>
								</xsl:if>

								<tr>
									<td colspan="2" class="bar-col">
										<xsl:for-each select="1 to needsCount">
											<span class="bar need-bar"></span>
										</xsl:for-each>
										<xsl:for-each select="1 to strengthCount">
											<span class="bar strong-bar"></span>
										</xsl:for-each>
										<xsl:if test="devEffortScore = '101'">
											<span class="incomplete">
												<xsl:value-of select="$incompleteLabel"></xsl:value-of>
											</span>
										</xsl:if>
									</td>
									<td class="grey-col score-col">
										<xsl:if test="devEffortScore != '101'">
											<xsl:value-of select="devEffortScore"></xsl:value-of>
										</xsl:if>
									</td>
									<td class="grey-col name-col">
										<xsl:value-of select="lastNameFirstName" />
									</td>
									<td class="blank"></td>
									<td class="cat-1-1 grey-col">
										<xsl:call-template name="isStrength">
											<xsl:with-param name="value" select="isInterestStregth"></xsl:with-param>
											<xsl:with-param name="des" select="devEffortScore"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-1-2">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="driveValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-1-3">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="focusValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-1-4">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="engagementValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="blank"></td>
									<td class="cat-2-1 grey-col">
										<xsl:call-template name="isStrength">
											<xsl:with-param name="value" select="isExperienceStregth"></xsl:with-param>
											<xsl:with-param name="des" select="devEffortScore"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-2-2">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="coreValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-2-3">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="perspectiveValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-2-4">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="keyChallengesValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="blank"></td>
									<td class="cat-3-1 grey-col">
										<xsl:call-template name="isStrength">
											<xsl:with-param name="value" select="isSelfAwarenessStregth"></xsl:with-param>
											<xsl:with-param name="des" select="devEffortScore"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-3-2">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="selfValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-3-3">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="situationalValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="blank"></td>
									<td class="cat-4-1 grey-col">
										<xsl:call-template name="isStrength">
											<xsl:with-param name="value"
												select="isLearningAgilityStregth"></xsl:with-param>
											<xsl:with-param name="des" select="devEffortScore"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-4-2">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="peopleValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-4-3">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="resultsValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-4-4">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="mentalValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-4-5">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="changeValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="blank"></td>
									<td class="cat-5-1 grey-col">
										<xsl:call-template name="isStrength">
											<xsl:with-param name="value"
												select="isLeadershipTraitsStregth"></xsl:with-param>
											<xsl:with-param name="des" select="devEffortScore"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-5-2">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="focusedValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-5-3">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="sociableValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-5-4">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="toleranceValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-5-5">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="composedValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-5-6">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="optimisticValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="blank"></td>
									<td class="cat-6-1 grey-col">
										<xsl:call-template name="isCapacityStrength">
											<xsl:with-param name="value" select="isCapacityStregth"></xsl:with-param>
											<xsl:with-param name="des" select="devEffortScore"></xsl:with-param>
											<xsl:with-param name="psv" select="problemSolveValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-6-2">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="problemSolveValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="blank"></td>
									<td class="cat-7-1 grey-col">
										<xsl:call-template name="isStrength">
											<xsl:with-param name="value"
												select="isDerailmentRisksStregth"></xsl:with-param>
											<xsl:with-param name="des" select="devEffortScore"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-7-2">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="volatileValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-7-3">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="microManageValue"></xsl:with-param>
										</xsl:call-template>
									</td>
									<td class="cat-7-4">
										<xsl:call-template name="optimalIcon">
											<xsl:with-param name="value" select="closedValue"></xsl:with-param>
										</xsl:call-template>
									</td>
								</tr>
								<xsl:if test="devEffortScore != '101'">
								<tr class="separated-hz-line">
										<td colspan="4" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="4" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="4" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="3" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="5" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="6" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="2" class="hz-line"></td>
										<td class="blank"></td>
										<td colspan="4" class="hz-line"></td>
									</tr>
								</xsl:if>
							</xsl:for-each>
						</xsl:for-each-group>
						<xsl:if test="position() != last()">
							<tr>
								<td colspan="39" class="full-hz-line"></td>
							</tr>
						</xsl:if>
					</xsl:for-each-group>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="39" class="full-hz-line-2"></th>
					</tr>
					<tr>
						<th height="20" colspan="39">
							<!-- Create footer in SVG so you can line up text with icons so it 
								will repeat in wkhtmltopdf -->

							<svg height="20" width="700">

								<text x="345" y="12" font-family="Arial Unicode MS"
									font-size="9" fill="black">
									<xsl:value-of select="optimalLabel"></xsl:value-of>
									<tspan x="424" y="12">
										<xsl:value-of select="belowOptimalLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="520" y="12">
										<xsl:value-of select="aboveOptimalLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
									<tspan x="622" y="12">
										<xsl:value-of select="derailerLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</tspan>
								</text>

								<text x="381" y="15" font-family="Korn Ferry Report Icons"
									font-size="18" fill="black">
									&#61487; 
									<tspan x="489" y="15">&#61489;</tspan>
									<tspan x="587" y="15">&#61488;</tspan>
									</text>
									
									<text x="694" y="12" font-family="Korn Ferry Report Icons"
									font-size="12" fill="#da002e">
									&#61486; 
								</text>
							</svg>
						</th>
					</tr>
				</tfoot>
			</table>
			</xsl:otherwise>
		</xsl:choose>

		</div>
	</xsl:template>
	<xsl:template name="optimalIcon">
		<xsl:param name="value"></xsl:param>
		<xsl:choose>
			<xsl:when test="$value = 'O'">
				<span class="kf-icon">&#61487; </span>
			</xsl:when>
			<xsl:when test="$value = 'B'">
				<span class="kf-icon">&#61489;</span>
			</xsl:when>
			<xsl:when test="$value = 'A'">
				<span class="kf-icon">&#61488;</span>
			</xsl:when>
			<xsl:when test="$value = 'D'">
				<span class="kf-icon icon-red">&#61486;</span>
			</xsl:when>
			<xsl:otherwise>&nbsp;
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="isStrength">
		<xsl:param name="value"></xsl:param>
		<xsl:param name="des"></xsl:param>
		<xsl:choose>
			<xsl:when test="$value = 'true' and $des != '101'">
				<span class="kf-icon icon-green">&#61473;</span>
			</xsl:when>
			<xsl:when test="$value = 'false' and $des != '101'">
				<span class="kf-icon icon-yellow">&#61474;</span>
			</xsl:when>
			<xsl:otherwise>&nbsp;
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="isCapacityStrength">
		<xsl:param name="value"></xsl:param>
		<xsl:param name="des"></xsl:param>
		<xsl:param name="psv"></xsl:param>
		<xsl:choose>
			<xsl:when test="$value = 'true' and $des != '101' and $psv != 'N/A'">
				<span class="kf-icon icon-green">&#61473;</span>
			</xsl:when>
			<xsl:when test="$value = 'false' and $des != '101' and $psv != 'N/A'">
				<span class="kf-icon icon-yellow">&#61474;</span>
			</xsl:when>
			<xsl:otherwise>&nbsp;
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="definitionsSection">
		<div class="definitions">
			<div class="group">
				<h1>
					<xsl:value-of select="title" />
				</h1>
				<h2 class="icon-h2">
					<span class="kf-icon">&#61480;</span>
					<xsl:value-of select="interestLabel"
						disable-output-escaping="yes"></xsl:value-of>
				</h2>
				<p>
					<xsl:value-of select="interestShortText"
						disable-output-escaping="yes"></xsl:value-of>
				</p>
			</div>
			<div class="group">
				<h2 class="icon-h2">
					<span class="kf-icon">&#61481;</span>
					<xsl:value-of select="experienceLabel"
						disable-output-escaping="yes"></xsl:value-of>
				</h2>
				<p>
					<xsl:value-of select="experienceShortText"
						disable-output-escaping="yes"></xsl:value-of>
				</p>
			</div>
			<div class="group">
				<h2 class="icon-h2">
					<span class="kf-icon">&#61482;</span>
					<xsl:value-of select="selfAwarenessLabel"
						disable-output-escaping="yes"></xsl:value-of>
				</h2>
				<p>
					<xsl:value-of select="selfAwareShortText"
						disable-output-escaping="yes"></xsl:value-of>
				</p>
			</div>
			<div class="group">
				<h2 class="icon-h2">
					<span class="kf-icon">&#61483;</span>
					<xsl:value-of select="learningAgilityLabel"
						disable-output-escaping="yes"></xsl:value-of>
				</h2>
				<p>
					<xsl:value-of select="learningAgilityShortText"
						disable-output-escaping="yes"></xsl:value-of>
				</p>
			</div>
			<div class="group">
				<h2 class="icon-h2">
					<span class="kf-icon">&#61484;</span>
					<xsl:value-of select="leadershipTraitsLabel"
						disable-output-escaping="yes"></xsl:value-of>
				</h2>
				<p>
					<xsl:value-of select="leadershipTraitsShortText"
						disable-output-escaping="yes"></xsl:value-of>
				</p>
			</div>
			<div class="group">
			
			<xsl:if test="../noRavens = 'false'">
				<h2 class="icon-h2">
					<span class="kf-icon">&#61485;</span>
					<xsl:value-of select="capacityLabel"
						disable-output-escaping="yes"></xsl:value-of>
				</h2>
				<p>
					<xsl:value-of select="capacityShortText"
						disable-output-escaping="yes"></xsl:value-of>
				</p>
				</xsl:if>
				<h2 class="icon-h2">
					<span class="kf-icon">&#61486;</span>
					<xsl:value-of select="derailmentRisksLabel"
						disable-output-escaping="yes"></xsl:value-of>
				</h2>
				<p>
					<xsl:value-of select="derailmentShortText"
						disable-output-escaping="yes"></xsl:value-of>
				</p>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>