<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xhtml [ 
<!ENTITY nbsp "&#160;"> 
]>
<xsl:stylesheet version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xslt"
	xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xsl xalan xs">
	<!-- http://stackoverflow.com/questions/9538619/xslt-refuses-to-write-doctype-declaration -->
	<xsl:output method="html" indent="yes" encoding="UTF-8"
		doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />
	<xsl:param name="baseurl" select="'http://localhost'" />
	<!-- these two not used? -->
	<xsl:param name="genmode" select="'pdf'" />
	<!-- no default match -->
	<xsl:template match="*" />
	<xsl:template match="/kfapIndividualFeedbackReport">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>Individual Report</title>
				<link href="{$baseurl}/core/css/report.css" rel="stylesheet"
					type="text/css" />

				<link href="{$baseurl}/core/css/kfIcon.css" rel="stylesheet"
					type="text/css" />

				<link href="{$baseurl}/projects/kfap/shared/css/report.css"
					rel="stylesheet" type="text/css" />
				<link href="{$baseurl}/projects/kfap/feedback/css/report.css"
					rel="stylesheet" type="text/css" />
				<script src="{$baseurl}/projects/kfap/shared/js/summaryChart.js"
					type="text/javascript"></script>

				<meta name="baseurl" content="{$baseurl}" />
				<meta name="genmode" content="{$genmode}" />
			</head>
			<body>
				<script type="text/javascript">
					<xsl:text disable-output-escaping="yes">
                <![CDATA[
                    window.onload = function ()
	                {
	                	setTimeout(function(){initChart();}, 500);
	                }
                ]]>
            </xsl:text>
				</script>
				<div class="container">
					<xsl:apply-templates select="overviewSection" />
					<xsl:apply-templates select="assessmentDetailsSection" />
					<xsl:apply-templates select="developmentPrioritiesSection" />
					<xsl:apply-templates select="summarySection" />
				</div>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="overviewSection">
		<div id="test902" class="group">
			<h1>
				<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
			</h1>
			<p>
				<xsl:value-of select="p1" disable-output-escaping="yes"></xsl:value-of>
			</p>
			<p>
				<xsl:value-of select="p2" disable-output-escaping="yes"></xsl:value-of>
			</p>
		</div>
		<div class="group">
			<table class="snapshot-table" border="0" cellspacing="0"
				cellpadding="0">
				<tr>
					<th class="icon-need">
						<span>
							<xsl:value-of select="needsLabel"
								disable-output-escaping="yes"></xsl:value-of>
						</span>
					</th>
					<th class="mid-th-cell">&#160;</th>
					<th class="icon-strength">
						<span>
							<xsl:value-of select="strengthLabel"
								disable-output-escaping="yes"></xsl:value-of>
						</span>
					</th>
				</tr>
				<tr>
					<td class="needs-bar">
						<xsl:choose>
							<xsl:when test="../isInterestStregth = 'false'">
								<div class="snap-cat-title">
									<span class="kf-icon">&#61480;</span>
									<xsl:value-of select="../interestLabel"
										disable-output-escaping="yes"></xsl:value-of>
								</div>
							</xsl:when>
							<xsl:otherwise>
								&#160;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>&#160;</td>
					<td class="strength-bar">
						<xsl:choose>
							<xsl:when test="../isInterestStregth = 'true'">
								<div class="snap-cat-title">
									<span class="kf-icon">&#61480;</span>
									<xsl:value-of select="../interestLabel"
										disable-output-escaping="yes"></xsl:value-of>
								</div>
							</xsl:when>
							<xsl:otherwise>
								&#160;
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				<tr>
					<td class="needs-bar">
						<xsl:choose>
							<xsl:when test="../isExperienceStregth = 'false'">
								<div class="snap-cat-title">
									<span class="kf-icon">&#61481;</span>
									<xsl:value-of select="../experienceLabel"
										disable-output-escaping="yes"></xsl:value-of>
								</div>
							</xsl:when>
							<xsl:otherwise>
								&#160;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>&#160;</td>
					<td class="strength-bar">
						<xsl:choose>
							<xsl:when test="../isExperienceStregth = 'true'">
								<div class="snap-cat-title">
									<span class="kf-icon">&#61481;</span>
									<xsl:value-of select="../experienceLabel"
										disable-output-escaping="yes"></xsl:value-of>
								</div>
							</xsl:when>
							<xsl:otherwise>
								&#160;
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				<tr>
					<td class="needs-bar">
						<xsl:choose>
							<xsl:when test="../isSelfAwarenessStregth = 'false'">
								<div class="snap-cat-title">
									<span class="kf-icon">&#61482;</span>
									<xsl:value-of select="../selfAwarenessLabel"
										disable-output-escaping="yes"></xsl:value-of>
								</div>
							</xsl:when>
							<xsl:otherwise>
								&#160;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>&#160;</td>
					<td class="strength-bar">
						<xsl:choose>
							<xsl:when test="../isSelfAwarenessStregth = 'true'">
								<div class="snap-cat-title">
									<span class="kf-icon">&#61482;</span>
									<xsl:value-of select="../selfAwarenessLabel"
										disable-output-escaping="yes"></xsl:value-of>
								</div>
							</xsl:when>
							<xsl:otherwise>
								&#160;
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				<tr>
					<td class="needs-bar">
						<xsl:choose>
							<xsl:when test="../isLearningAgilityStregth = 'false'">
								<div class="snap-cat-title">
									<span class="kf-icon">&#61483;</span>
									<xsl:value-of select="../learningAgilityLabel"
										disable-output-escaping="yes"></xsl:value-of>
								</div>
							</xsl:when>
							<xsl:otherwise>
								&#160;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>&#160;</td>
					<td class="strength-bar">
						<xsl:choose>
							<xsl:when test="../isLearningAgilityStregth = 'true'">
								<div class="snap-cat-title">
									<span class="kf-icon">&#61483;</span>
									<xsl:value-of select="../learningAgilityLabel"
										disable-output-escaping="yes"></xsl:value-of>
								</div>
							</xsl:when>
							<xsl:otherwise>
								&#160;
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				<tr>
					<td class="needs-bar">
						<xsl:choose>
							<xsl:when test="../isLeadershipTraitsStregth = 'false'">
								<div class="snap-cat-title">
									<span class="kf-icon">&#61484;</span>
									<xsl:value-of select="../leadershipTraitsLabel"
										disable-output-escaping="yes"></xsl:value-of>
								</div>
							</xsl:when>
							<xsl:otherwise>
								&#160;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>&#160;</td>
					<td class="strength-bar">
						<xsl:choose>
							<xsl:when test="../isLeadershipTraitsStregth = 'true'">
								<div class="snap-cat-title">
									<span class="kf-icon">&#61484;</span>
									<xsl:value-of select="../leadershipTraitsLabel"
										disable-output-escaping="yes"></xsl:value-of>
								</div>
							</xsl:when>
							<xsl:otherwise>
								&#160;
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				<xsl:if test="../noRavens = 'false'">
					<tr>
						<td class="needs-bar">
							<xsl:choose>
								<xsl:when test="../isCapacityStregth = 'false'">
									<div class="snap-cat-title">
										<span class="kf-icon">&#61485;</span>
										<xsl:value-of select="../capacityLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</div>
								</xsl:when>
								<xsl:otherwise>
									&#160;
								</xsl:otherwise>
							</xsl:choose>
						</td>
						<td>&#160;</td>
						<td class="strength-bar">
							<xsl:choose>
								<xsl:when test="../isCapacityStregth = 'true'">
									<div class="snap-cat-title">
										<span class="kf-icon">&#61485;</span>
										<xsl:value-of select="../capacityLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</div>
								</xsl:when>
								<xsl:otherwise>
									&#160;
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
				</xsl:if>
				<tr>
					<td class="needs-bar">
						<xsl:choose>
							<xsl:when test="../isDerailmentRisksStregth = 'false'">
								<div class="snap-cat-title">
									<span class="kf-icon">&#61486;</span>
									<xsl:value-of select="../derailmentRisksLabel"
										disable-output-escaping="yes"></xsl:value-of>
								</div>
							</xsl:when>
							<xsl:otherwise>
								&#160;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>&#160;</td>
					<td class="strength-bar">
						<xsl:choose>
							<xsl:when test="../isDerailmentRisksStregth = 'true'">
								<div class="snap-cat-title">
									<span class="kf-icon">&#61486;</span>
									<xsl:value-of select="../derailmentRisksLabel"
										disable-output-escaping="yes"></xsl:value-of>
								</div>
							</xsl:when>
							<xsl:otherwise>
								&#160;
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
			</table>
			<table class="footnotes" border="0" cellspacing="0"
				cellpadding="0">
				<tr>
					<td>
						<span class="icon-strong-grey">&nbsp;
						</span>
					</td>
					<td>
						<xsl:value-of select="strengthIconFootnote"
							disable-output-escaping="yes"></xsl:value-of>
					</td>
				</tr>
				<tr>
					<td>
						<span class="icon-needs-development-grey">&nbsp;
						</span>
					</td>
					<td>
						<xsl:value-of select="devNeedIconFootnote"
							disable-output-escaping="yes"></xsl:value-of>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
	<xsl:template match="assessmentDetailsSection">
		<div class="break group">
			<h1>
				<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
			</h1>
			<xsl:text disable-output-escaping="yes">&lt;div class=&quot;group&quot;&gt;</xsl:text>
			<xsl:choose>
				<xsl:when test="../isInterestStregth = 'true'">
					<span class="icon-strong kf-icon icon-green icon-container-a">&#61473;</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="icon-needs-development kf-icon icon-yellow icon-container-a">&#61474;</span>
				</xsl:otherwise>
			</xsl:choose>
			<h2 class="icon-h2">
				<span class="kf-icon">&#61480;</span>
				<xsl:value-of select="interestSection/label"
					disable-output-escaping="yes"></xsl:value-of>
			</h2>
			<p>
				<xsl:for-each select="interestSection/sectionText">
					<p>
						<xsl:value-of select="text" disable-output-escaping="yes"></xsl:value-of>
					</p>
				</xsl:for-each>
			</p>
			<xsl:for-each select="interestSection/subSection">

				<div class="details-chart group">
					<h4>
						<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
					</h4>
					<hr />
					<table class="subsection-table">
						<tr>
							<td class="dev-strong-cell">
								<span>
									<xsl:value-of select="DevelopmentText"
										disable-output-escaping="yes"></xsl:value-of>
								</span>
							</td>
							<td class="chart-cell">
								<span>
									<span class="scale-pin">
										<xsl:attribute name="style">left:<xsl:value-of
											select="score * 1.01" disable-output-escaping="yes"></xsl:value-of>%</xsl:attribute>

										<xsl:choose>
											<xsl:when test="position() = 1">
												<xsl:choose>
													<xsl:when test="score &lt; 11">
														<span class="kf-icon icon-orange">&#61475;</span>
													</xsl:when>
													<xsl:when test="score &gt; 49">
														<span class="kf-icon icon-green">&#61475;</span>
													</xsl:when>
													<xsl:otherwise>
														<span class="kf-icon icon-yellow">&#61475;</span>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:when>
											<xsl:otherwise>
												<xsl:choose>
													<xsl:when test="score &lt; 11">
														<span class="kf-icon icon-orange">&#61475;</span>
													</xsl:when>
													<xsl:when test="score &gt; 24">
														<span class="kf-icon icon-green">&#61475;</span>
													</xsl:when>
													<xsl:otherwise>
														<span class="kf-icon icon-yellow">&#61475;</span>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:otherwise>
										</xsl:choose>

										<span class="subsection-score">
											<xsl:value-of select="score"
												disable-output-escaping="yes"></xsl:value-of>
										</span>
									</span>
									<div>
										<xsl:choose>
											<xsl:when test="position() = 1">
												<img
													src="{$baseurl}/projects/kfap/shared/css/images/scale-bg-4.png"
													alt="percentile scale" />
											</xsl:when>
											<xsl:otherwise>
												<img
													src="{$baseurl}/projects/kfap/shared/css/images/scale-bg-1.png"
													alt="percentile scale" />
											</xsl:otherwise>
										</xsl:choose>
									</div>
									<span class="scale-label label-1">1</span>
									<span class="scale-label label-11">11</span>

									<xsl:choose>
										<xsl:when test="position() = 1">
											<span class="scale-label label-50">50</span>
										</xsl:when>
										<xsl:otherwise>
											<span class="scale-label label-25">25</span>
										</xsl:otherwise>
									</xsl:choose>

									<span class="scale-label label-99">99</span>
								</span>
							</td>
							<td class="dev-strong-cell">
								<span>
									<xsl:value-of select="StrongText"
										disable-output-escaping="yes"></xsl:value-of>
								</span>
							</td>
						</tr>
					</table>
					<xsl:for-each select="subSectionText">
						<p>
							<xsl:value-of select="text" disable-output-escaping="yes"></xsl:value-of>
						</p>
					</xsl:for-each>
				</div>
				<xsl:if test="position() = 1">
					<xsl:text disable-output-escaping="yes">&lt;/div&gt;</xsl:text>
				</xsl:if>
			</xsl:for-each>
			<p>&nbsp;
			</p>
		</div>
		
			<xsl:text disable-output-escaping="yes">&lt;div class=&quot;group&quot;&gt;</xsl:text>
			<xsl:choose>
				<xsl:when test="../isExperienceStregth = 'true'">
					<span class="icon-strong kf-icon icon-green icon-container-a">&#61473;</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="icon-needs-development kf-icon icon-yellow icon-container-a">&#61474;</span>
				</xsl:otherwise>
			</xsl:choose>
			<h2 class="icon-h2">
				<span class="kf-icon">&#61481;</span>
				<xsl:value-of select="experienceSection/label"
					disable-output-escaping="yes"></xsl:value-of>
			</h2>
			<p>
				<xsl:value-of select="experienceSection/sectionText"
					disable-output-escaping="yes"></xsl:value-of>
			</p>
			<xsl:for-each select="experienceSection/subSection">
				<div class="details-chart group">
					<h4>
						<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
					</h4>
					<hr />
					<table class="subsection-table">
						<tr>
							<td class="dev-strong-cell">
								<span>
									<xsl:value-of select="DevelopmentText"
										disable-output-escaping="yes"></xsl:value-of>
								</span>
							</td>
							<td class="chart-cell">
								<span>
									<span class="scale-pin">
										<xsl:attribute name="style">left:<xsl:value-of
											select="score * 1.01" disable-output-escaping="yes"></xsl:value-of>%</xsl:attribute>
										<xsl:choose>
											<xsl:when test="score &lt; 11">
												<span class="kf-icon icon-orange">&#61475;</span>
											</xsl:when>
											<xsl:when test="score &gt; 24">
												<span class="kf-icon icon-green">&#61475;</span>
											</xsl:when>
											<xsl:otherwise>
												<span class="kf-icon icon-yellow">&#61475;</span>
											</xsl:otherwise>
										</xsl:choose>
										<span class="subsection-score">
											<xsl:value-of select="score"
												disable-output-escaping="yes"></xsl:value-of>
										</span>
									</span>
									<div>
										<img
											src="{$baseurl}/projects/kfap/shared/css/images/scale-bg-1.png"
											alt="percentile scale" />
									</div>
									<span class="scale-label label-1">1</span>
									<span class="scale-label label-11">11</span>
									<span class="scale-label label-25">25</span>
									<span class="scale-label label-99">99</span>
								</span>
							</td>
							<td class="dev-strong-cell">
								<span>
									<xsl:value-of select="StrongText"
										disable-output-escaping="yes"></xsl:value-of>
								</span>
							</td>
						</tr>
					</table>
					<xsl:for-each select="subSectionText">
						<p>
							<xsl:value-of select="text" disable-output-escaping="yes"></xsl:value-of>
						</p>

					</xsl:for-each>
				</div>
				<xsl:if test="position() = 1">
					<xsl:text disable-output-escaping="yes">&lt;/div&gt;</xsl:text>
				</xsl:if>
			</xsl:for-each>
		
		<div class="indent-challenges-list">
			<h4>
				<xsl:value-of select="keyLeadershipChallenges/title"
					disable-output-escaping="yes"></xsl:value-of>
			</h4>
			<table>
				<xsl:for-each select="keyLeadershipChallenges/challengeListItem">
					<tr>
						<xsl:choose>
							<xsl:when test="isAddressing = 'true'">
								<td class="kc-col1 bullet-box-check"></td>
							</xsl:when>
							<xsl:otherwise>
								<td class="kc-col1 bullet-box"></td>
							</xsl:otherwise>
						</xsl:choose>
						<td>
							<xsl:value-of select="text" disable-output-escaping="yes"></xsl:value-of>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</div>
		<div class="group">
			<xsl:text disable-output-escaping="yes">&lt;div class=&quot;group&quot;&gt;</xsl:text>
			<xsl:choose>
				<xsl:when test="../isSelfAwarenessStregth = 'true'">
					<span class="icon-strong kf-icon icon-green icon-container-a">&#61473;</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="icon-needs-development kf-icon icon-yellow icon-container-a">&#61474;</span>
				</xsl:otherwise>
			</xsl:choose>
			<h2 class="icon-h2">
				<span class="kf-icon">&#61482;</span>
				<xsl:value-of select="selfAwareSection/label"
					disable-output-escaping="yes"></xsl:value-of>
			</h2>
			<p>
				<xsl:for-each select="selfAwareSection/sectionText">
					<p>
						<xsl:value-of select="text" disable-output-escaping="yes"></xsl:value-of>
					</p>
				</xsl:for-each>
			</p>
			<xsl:for-each select="selfAwareSection/subSection">
				<div class="details-chart group">
					<h4>
						<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
					</h4>
					<hr />
					<table class="subsection-table">
						<tr>
							<td class="dev-strong-cell">
								<span>
									<xsl:value-of select="DevelopmentText"
										disable-output-escaping="yes"></xsl:value-of>
								</span>
							</td>
							<td class="chart-cell">
								<span>
									<span class="scale-pin">
										<xsl:attribute name="style">left:<xsl:value-of
											select="score * 1.01" disable-output-escaping="yes"></xsl:value-of>%</xsl:attribute>
										<xsl:choose>
											<xsl:when test="score &lt; 11">
												<span class="kf-icon icon-orange">&#61475;</span>
											</xsl:when>
											<xsl:when test="score &gt; 24">
												<span class="kf-icon icon-green">&#61475;</span>
											</xsl:when>
											<xsl:otherwise>
												<span class="kf-icon icon-yellow">&#61475;</span>
											</xsl:otherwise>
										</xsl:choose>
										<span class="subsection-score">
											<xsl:value-of select="score"
												disable-output-escaping="yes"></xsl:value-of>
										</span>
									</span>
									<div>
										<img
											src="{$baseurl}/projects/kfap/shared/css/images/scale-bg-1.png"
											alt="percentile scale" />
									</div>
									<span class="scale-label label-1">1</span>
									<span class="scale-label label-11">11</span>
									<span class="scale-label label-25">25</span>
									<span class="scale-label label-99">99</span>
								</span>
							</td>
							<td class="dev-strong-cell">
								<span>
									<xsl:value-of select="StrongText"
										disable-output-escaping="yes"></xsl:value-of>
								</span>
							</td>
						</tr>
					</table>
					<xsl:for-each select="subSectionText">
						<p>
							<xsl:value-of select="text" disable-output-escaping="yes"></xsl:value-of>
						</p>
					</xsl:for-each>
				</div>
				<xsl:if test="position() = 1">
					<xsl:text disable-output-escaping="yes">&lt;/div&gt;</xsl:text>
				</xsl:if>
			</xsl:for-each>
			<p>&nbsp;
			</p>
		</div>
		<div class="group">
			<xsl:text disable-output-escaping="yes">&lt;div class=&quot;group&quot;&gt;</xsl:text>
			<xsl:choose>
				<xsl:when test="../isLearningAgilityStregth = 'true'">
					<span class="icon-strong kf-icon icon-green icon-container-a">&#61473;</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="icon-needs-development kf-icon icon-yellow icon-container-a">&#61474;</span>
				</xsl:otherwise>
			</xsl:choose>
			<h2 class="icon-h2">
				<span class="kf-icon">&#61483;</span>
				<xsl:value-of select="learningAgilitySection/label"
					disable-output-escaping="yes"></xsl:value-of>
			</h2>
			<p>
				<xsl:value-of select="learningAgilitySection/sectionText"
					disable-output-escaping="yes"></xsl:value-of>
			</p>
			<xsl:for-each select="learningAgilitySection/subSection">
				<div class="details-chart group">
					<h4>
						<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
					</h4>
					<hr />
					<table class="subsection-table">
						<tr>
							<td class="dev-strong-cell">
								<span>
									<xsl:value-of select="DevelopmentText"
										disable-output-escaping="yes"></xsl:value-of>
								</span>
							</td>
							<td class="chart-cell">
								<span>
									<span class="scale-pin">
										<xsl:attribute name="style">left:<xsl:value-of
											select="score * 1.01" disable-output-escaping="yes"></xsl:value-of>%</xsl:attribute>
										<xsl:choose>
											<xsl:when test="score &lt; 11">
												<span class="kf-icon icon-orange">&#61475;</span>
											</xsl:when>
											<xsl:when test="score &gt; 24 and score &lt; 91">
												<span class="kf-icon icon-green">&#61475;</span>
											</xsl:when>
											<xsl:otherwise>
												<span class="kf-icon icon-yellow">&#61475;</span>
											</xsl:otherwise>
										</xsl:choose>
										<span class="subsection-score">
											<xsl:value-of select="score"
												disable-output-escaping="yes"></xsl:value-of>
										</span>
									</span>
									<div>
										<img
											src="{$baseurl}/projects/kfap/shared/css/images/scale-bg-2.png"
											alt="percentile scale" />
									</div>
									<span class="scale-label label-1">1</span>
									<span class="scale-label label-11">11</span>
									<span class="scale-label label-25">25</span>
									<span class="scale-label label-91">91</span>
									<span class="scale-label label-99">99</span>
								</span>
							</td>
							<td class="dev-strong-cell">
								<span>
									<xsl:value-of select="StrongText"
										disable-output-escaping="yes"></xsl:value-of>
								</span>
							</td>
						</tr>
					</table>
					<xsl:for-each select="subSectionText">
						<p>
							<xsl:value-of select="text" disable-output-escaping="yes"></xsl:value-of>
						</p>
					</xsl:for-each>
				</div>
				<xsl:if test="position() = 1">
					<xsl:text disable-output-escaping="yes">&lt;/div&gt;</xsl:text>
				</xsl:if>
			</xsl:for-each>
			<p>&nbsp;
			</p>
		</div>
		<div class="group">
			<xsl:text disable-output-escaping="yes">&lt;div class=&quot;group&quot;&gt;</xsl:text>
			<xsl:choose>
				<xsl:when test="../isLeadershipTraitsStregth = 'true'">
					<span class="icon-strong kf-icon icon-green icon-container-a">&#61473;</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="icon-needs-development kf-icon icon-yellow icon-container-a">&#61474;</span>
				</xsl:otherwise>
			</xsl:choose>
			<h2 class="icon-h2">
				<span class="kf-icon">&#61484;</span>
				<xsl:value-of select="leadershipTraitsSection/label"
					disable-output-escaping="yes"></xsl:value-of>
			</h2>
			<p>
				<xsl:value-of select="leadershipTraitsSection/sectionText"
					disable-output-escaping="yes"></xsl:value-of>
			</p>
			<xsl:for-each select="leadershipTraitsSection/subSection">
				<div class="details-chart group">
					<h4>
						<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
					</h4>
					<hr />
					<table class="subsection-table">
						<tr>
							<td class="dev-strong-cell">
								<span>
									<xsl:value-of select="DevelopmentText"
										disable-output-escaping="yes"></xsl:value-of>
								</span>
							</td>
							<td class="chart-cell">
								<span>
									<span class="scale-pin">
										<xsl:attribute name="style">left:<xsl:value-of
											select="score * 1.01" disable-output-escaping="yes"></xsl:value-of>%</xsl:attribute>
										<xsl:choose>
											<xsl:when test="score &lt; 11">
												<span class="kf-icon icon-orange">&#61475;</span>
											</xsl:when>
											<xsl:when test="score &gt; 24 and score &lt; 91">
												<span class="kf-icon icon-green">&#61475;</span>
											</xsl:when>
											<xsl:otherwise>
												<span class="kf-icon icon-yellow">&#61475;</span>
											</xsl:otherwise>
										</xsl:choose>
										<span class="subsection-score">
											<xsl:value-of select="score"
												disable-output-escaping="yes"></xsl:value-of>
										</span>
									</span>
									<div>
										<img
											src="{$baseurl}/projects/kfap/shared/css/images/scale-bg-2.png"
											alt="percentile scale" />
									</div>
									<span class="scale-label label-1">1</span>
									<span class="scale-label label-11">11</span>
									<span class="scale-label label-25">25</span>
									<span class="scale-label label-91">91</span>
									<span class="scale-label label-99">99</span>
								</span>
							</td>
							<td class="dev-strong-cell">
								<span>
									<xsl:value-of select="StrongText"
										disable-output-escaping="yes"></xsl:value-of>
								</span>
							</td>
						</tr>
					</table>
					<xsl:for-each select="subSectionText">
						<p>
							<xsl:value-of select="text" disable-output-escaping="yes"></xsl:value-of>
						</p>
					</xsl:for-each>
				</div>
				<xsl:if test="position() = 1">
					<xsl:text disable-output-escaping="yes">&lt;/div&gt;</xsl:text>
				</xsl:if>
			</xsl:for-each>
			<p>&nbsp;
			</p>
		</div>
		<xsl:if test="../noRavens = 'false'">
			<div class="group">
				<xsl:text disable-output-escaping="yes">&lt;div class=&quot;group&quot;&gt;</xsl:text>
				<xsl:choose>
					<xsl:when test="../isCapacityStregth = 'true'">
						<span class="icon-strong kf-icon icon-green icon-container-a">&#61473;</span>
					</xsl:when>
					<xsl:otherwise>
						<span
							class="icon-needs-development kf-icon icon-yellow icon-container-a">&#61474;</span>
					</xsl:otherwise>
				</xsl:choose>
				<h2 class="icon-h2">
					<span class="kf-icon">&#61485;</span>
					<xsl:value-of select="capacitySection/label"
						disable-output-escaping="yes"></xsl:value-of>
				</h2>
				<p>
					<xsl:value-of select="capacitySection/sectionText"
						disable-output-escaping="yes"></xsl:value-of>
				</p>
				<xsl:for-each select="capacitySection/subSection">
					<div class="details-chart group">
						<h4>
							<xsl:value-of select="title"
								disable-output-escaping="yes"></xsl:value-of>
						</h4>
						<hr />
						<table class="subsection-table">
							<tr>
								<td class="dev-strong-cell">
									<span>
										<xsl:value-of select="DevelopmentText"
											disable-output-escaping="yes"></xsl:value-of>
									</span>
								</td>
								<td class="chart-cell">
									<span>
										<span class="scale-pin">
											<xsl:attribute name="style">left:<xsl:value-of
												select="score * 1.01" disable-output-escaping="yes"></xsl:value-of>%</xsl:attribute>
											<xsl:choose>
												<xsl:when test="score &lt; 11">
													<span class="kf-icon icon-orange">&#61475;</span>
												</xsl:when>
												<xsl:when test="score &gt; 24 and score &lt; 91">
													<span class="kf-icon icon-green">&#61475;</span>
												</xsl:when>
												<xsl:otherwise>
													<span class="kf-icon icon-yellow">&#61475;</span>
												</xsl:otherwise>
											</xsl:choose>
											<span class="subsection-score">
												<xsl:value-of select="score"
													disable-output-escaping="yes"></xsl:value-of>
											</span>
										</span>
										<div>
											<img
												src="{$baseurl}/projects/kfap/shared/css/images/scale-bg-2.png"
												alt="percentile scale" />
										</div>
										<span class="scale-label label-1">1</span>
										<span class="scale-label label-11">11</span>
										<span class="scale-label label-25">25</span>
										<span class="scale-label label-91">91</span>
										<span class="scale-label label-99">99</span>
									</span>
								</td>
								<td class="dev-strong-cell">
									<span>
										<xsl:value-of select="StrongText"
											disable-output-escaping="yes"></xsl:value-of>
									</span>
								</td>
							</tr>
						</table>
						<xsl:for-each select="subSectionText">
							<p>
								<xsl:value-of select="text"
									disable-output-escaping="yes"></xsl:value-of>
							</p>
						</xsl:for-each>
					</div>
					<xsl:if test="position() = 1">
						<xsl:text disable-output-escaping="yes">&lt;/div&gt;</xsl:text>
					</xsl:if>
				</xsl:for-each>
				<p>&nbsp;
				</p>
			</div>
		</xsl:if>
		
			<xsl:text disable-output-escaping="yes">&lt;div class=&quot;group&quot;&gt;</xsl:text>
			<xsl:choose>
				<xsl:when test="../isDerailmentRisksStregth = 'true'">
					<span class="icon-strong kf-icon icon-green icon-container-a">&#61473;</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="icon-needs-development kf-icon icon-yellow icon-container-a">&#61474;</span>
				</xsl:otherwise>
			</xsl:choose>
			<h2 class="icon-h2">
				<span class="kf-icon">&#61486;</span>
				<xsl:value-of select="derailmentSection/label"
					disable-output-escaping="yes"></xsl:value-of>
			</h2>
			<p>
				<xsl:value-of select="derailmentSection/sectionText"
					disable-output-escaping="yes"></xsl:value-of>
			</p>
			<xsl:for-each select="derailmentSection/subSection">
				<div class="details-chart group">
					<h4>
						<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
					</h4>
					<hr />
					<table class="subsection-table">
						<tr>
							<td class="dev-strong-cell">
								<span>
									<xsl:value-of select="DevelopmentText"
										disable-output-escaping="yes"></xsl:value-of>
								</span>
							</td>
							<td class="chart-cell">
								<span>
									<span class="scale-pin">
										<xsl:attribute name="style">left:<xsl:value-of
											select="score * 1.01" disable-output-escaping="yes"></xsl:value-of>%</xsl:attribute>
										<xsl:choose>
											<xsl:when test="score &lt; 85">
												<span class="kf-icon icon-green">&#61475;</span>
											</xsl:when>
											<xsl:when test="score &gt; 90">
												<span class="kf-icon icon-orange">&#61475;</span>
											</xsl:when>
											<xsl:otherwise>
												<span class="kf-icon icon-yellow">&#61475;</span>
											</xsl:otherwise>
										</xsl:choose>
										<span class="subsection-score">
											<xsl:value-of select="score"
												disable-output-escaping="yes"></xsl:value-of>
										</span>
									</span>
									<div>
										<img
											src="{$baseurl}/projects/kfap/shared/css/images/scale-bg-3.png"
											alt="percentile scale" />
									</div>
									<span class="scale-label label-1">1</span>
									<span class="scale-label label-85">85</span>
									<span class="scale-label label-91">91</span>
									<span class="scale-label label-99">99</span>
								</span>
							</td>
							<td class="dev-strong-cell">
								<span>
									<xsl:value-of select="StrongText"
										disable-output-escaping="yes"></xsl:value-of>
								</span>
							</td>
						</tr>
					</table>
					<xsl:for-each select="subSectionText">
						<p>
							<xsl:value-of select="text" disable-output-escaping="yes"></xsl:value-of>
						</p>
					</xsl:for-each>
				</div>
				<xsl:if test="position() = 1">
					<xsl:text disable-output-escaping="yes">&lt;/div&gt;</xsl:text>
				</xsl:if>
			</xsl:for-each>
		
	</xsl:template>
	<xsl:template match="developmentPrioritiesSection">
		<div class="break group">
			<h1>
				<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
			</h1>
			<p>
				<xsl:value-of select="p1" disable-output-escaping="yes"></xsl:value-of>
			</p>
			<table class="item-table" border="0" cellspacing="0"
				cellpadding="0">
				<xsl:for-each select="priority">
					<tr>
						<td>
							<xsl:choose>
								<xsl:when test="position() != last()">
									<xsl:attribute name="class">rec-col-1-item rec-cell priorities-color</xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="class">rec-col-1-item priorities-color</xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:value-of select="label"
								disable-output-escaping="yes"></xsl:value-of>
						</td>
						<td>
							<xsl:choose>
								<xsl:when test="position() != last()">
									<xsl:attribute name="class">group rec-cell priorities-color priorities-text</xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="class">group priorities-text</xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:value-of select="text" disable-output-escaping="yes"></xsl:value-of>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</div>
	</xsl:template>
	<xsl:template match="summarySection">
		<div class="break group">
			<h1>
				<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
			</h1>
			<div id="step-chart">
				<xsl:for-each select="level">
					<div>
						<xsl:choose>
							<xsl:when
								test="isCurrent = 'true' or isAspiration = 'true' or isTarget = 'true'">
								<xsl:attribute name="class">step step-<xsl:value-of
									select="position()" disable-output-escaping="yes"></xsl:value-of>-active</xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="class">step step-<xsl:value-of
									select="position()" disable-output-escaping="yes"></xsl:value-of></xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:if test="isCurrent = 'true'">
							<div id="pin1" class="pin">
								<div class="step-icon">
									<span class="kf-icon">&#61476;</span>
								</div>
								<div class="label">
									<xsl:value-of select="../currentLabel"
										disable-output-escaping="yes"></xsl:value-of>
								</div>
								<div class="guide"></div>
							</div>
						</xsl:if>
						<xsl:if test="isAspiration = 'true'">
							<div id="pin2" class="pin">
								<div class="step-icon">
									<span class="kf-icon">&#61477;</span>
								</div>
								<div class="label">
									<xsl:value-of select="../aspirationLabel"
										disable-output-escaping="yes"></xsl:value-of>
								</div>
								<div class="guide"></div>
							</div>
						</xsl:if>
						<xsl:if test="isTarget = 'true'">
							<div id="pin3" class="pin">
								<div class="step-icon">
									<span class="kf-icon">&#61478;</span>
								</div>
								<div class="label">
									<xsl:value-of select="../targetLabel"
										disable-output-escaping="yes"></xsl:value-of>
								</div>
								<div class="guide"></div>
							</div>
						</xsl:if>
					</div>
				</xsl:for-each>
				<div class="clearfix" style="float: left; display: block; 
				width: 100%;"></div>
				<xsl:for-each select="level">
					<div>
						<xsl:choose>
							<xsl:when
								test="isCurrent = 'true' or isAspiration = 'true' or isTarget = 'true'">
								<xsl:attribute name="class">step-label</xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="class">step-label disabled</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:value-of select="label" disable-output-escaping="yes"></xsl:value-of>
					</div>
				</xsl:for-each>
			</div>
		</div>
		<table class="group background-container" border="0"
			cellpadding="0" cellspacing="0">
			<tr>
				<td class="overview-col-1">
					<span class="kf-icon nudge-left-3">&#61479;</span>
					<br />
					<span class="background-label">
						<xsl:value-of select="backgroundHeading"
							disable-output-escaping="yes"></xsl:value-of>
					</span>
				</td>
				<td>
					<ul>
						<xsl:for-each select="backgroundListItem">
							<li>
								<span class="background-label">
									<xsl:value-of select="numberLabel"
										disable-output-escaping="yes"></xsl:value-of>
								</span>
								<span>
									<xsl:value-of select="text"
										disable-output-escaping="yes"></xsl:value-of>
								</span>
							</li>
						</xsl:for-each>
					</ul>
				</td>
			</tr>
		</table>
		<div class="group">
			<table class="group goal-obj-role-container" border="0"
				cellpadding="0" cellspacing="0">
				<tr>
					<td class="goal-obj-container">
						<span class="kf-icon nudge-left-6">&#61479;</span>
						<span class="background-label goals-objectives-label">
							<xsl:value-of select="goalsObjectivesHeading"
								disable-output-escaping="yes"></xsl:value-of>
						</span>
						<ul>
							<xsl:for-each select="goalsObjectivesList/bulletPoint">
								<li>
									<xsl:value-of select="text()"
										disable-output-escaping="yes"></xsl:value-of>
								</li>
							</xsl:for-each>
						</ul>
					</td>
					<td class="role-container">
						<span class="kf-icon">&#61477;</span>
						<span class="background-label role-label">
							<xsl:value-of select="idealRoleHeading"
								disable-output-escaping="yes"></xsl:value-of>
						</span>
						<ul>
							<xsl:for-each select="idealRoleList/bulletPoint">
								<li>
									<xsl:value-of select="text()"
										disable-output-escaping="yes"></xsl:value-of>
								</li>
							</xsl:for-each>
						</ul>
					</td>
				</tr>
			</table>
		</div>
		<div class="break group">
			<table class="experience-table">
				<tr class="group">
					<th class="exp-col-1"></th>
					<th class="experience-th-past">
						<span>
							<xsl:value-of select="expTable/expColHeader"
								disable-output-escaping="yes"></xsl:value-of>
							<br />
							<span class="kf-icon nudge-left-3">&#61479;</span>
						</span>
					</th>
					<th class="experience-th-future">
						<span>
							<xsl:value-of select="expTable/futureColHeader"
								disable-output-escaping="yes"></xsl:value-of>
							<br />
							<span class="kf-icon">&#61477;</span>
						</span>
					</th>
					<th></th>
				</tr>
				<xsl:for-each select="expTable/expTableCategory">
					<xsl:variable name="trPostion" select="position()" />
					<tr>
						<xsl:if test="position() = 1 or position() = 3">
							<xsl:attribute name="class">group</xsl:attribute>
						</xsl:if>
						<td>
							<h3>
								<xsl:value-of select="categoryLabel"
									disable-output-escaping="yes"></xsl:value-of>
							</h3>
						</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<xsl:for-each select="categoryItem">
						<tr>
							<td>
								<xsl:choose>
									<xsl:when test="isCurrent = 'true'">
										<xsl:attribute name="class">cat-item-title experience-cat-<xsl:value-of
											select="$trPostion" disable-output-escaping="yes"></xsl:value-of> current</xsl:attribute>
										<span class="kf-icon">&#61476;</span>
									</xsl:when>
									<xsl:otherwise>
										<xsl:attribute name="class">cat-item-title experience-cat-<xsl:value-of
											select="$trPostion" disable-output-escaping="yes"></xsl:value-of></xsl:attribute>
									</xsl:otherwise>
								</xsl:choose>

								<xsl:value-of select="label"
									disable-output-escaping="yes"></xsl:value-of>
							</td>
							<td class="check-box">
								<span>
									<xsl:if test="hasTwoPlusYears = 'true'">
										<img
											src="{$baseurl}/projects/kfap/shared/css/images/icon-checkmark.png"
											alt="checked" />
									</xsl:if>
								</span>
							</td>
							<td class="check-box">
								<span>
									<xsl:if test="wouldConsiderInFuture = 'true'">
										<img
											src="{$baseurl}/projects/kfap/shared/css/images/icon-checkmark.png"
											alt="checked" />
									</xsl:if>
								</span>
							</td>
							<td></td>
						</tr>
					</xsl:for-each>

				</xsl:for-each>
			</table>
			<span class="current-role-key-icon kf-icon">&#61476;</span>
			<span class="current-role-key">
				(
				<xsl:value-of select="expTable/currentRoleLabel"
					disable-output-escaping="yes"></xsl:value-of>
				)
			</span>
		</div>
	</xsl:template>
</xsl:stylesheet>