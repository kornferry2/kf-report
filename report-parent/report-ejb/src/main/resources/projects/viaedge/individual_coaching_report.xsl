<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xhtml [ 
<!ENTITY nbsp "&#160;"> 
]>
<xsl:stylesheet version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xslt"
	xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xsl xalan xs">
	<!-- http://stackoverflow.com/questions/9538619/xslt-refuses-to-write-doctype-declaration -->
	<xsl:output method="xml" indent="yes" encoding="UTF-8"
		doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />
	<xsl:param name="baseurl" select="'http://localhost'" />
	<!-- these two not used? -->
	<xsl:param name="genmode" select="'pdf'" />
	<!-- no default match -->
	<xsl:template match="*" />
	<xsl:template match="/viaEdgeCoachingReport">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>
					<xsl:value-of select="reportTitle"></xsl:value-of>
				</title>
				<!-- <link href="{$baseurl}/core/css/font_{$reportLang}.css" rel="stylesheet" 
					type="text/css" /> -->
				<link href="{$baseurl}/core/css/reportML.css" rel="stylesheet"
					type="text/css" />
				<link href="{$baseurl}/projects/viaEdge/coaching/css/report.css"
					rel="stylesheet" type="text/css" />
				<meta name="baseurl" content="{$baseurl}" />
				<meta name="genmode" content="{$genmode}" />
			</head>
			<body>
			<xsl:choose>
					<xsl:when test="@lang = 'ko'">
						<xsl:attribute name="class">ko</xsl:attribute>
					</xsl:when>
					<xsl:when test="@lang = 'zh_CN'">
						<xsl:attribute name="class">zh</xsl:attribute>
					</xsl:when>
					<xsl:when test="@lang = 'ja'">
						<xsl:attribute name="class">ja</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="class"></xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:variable name="language" select="@lang"/>
				<div class="container">
					<xsl:apply-templates select="introductionSection" />
					<xsl:apply-templates select="overviewSection" />
					<xsl:apply-templates select="agilityScoresSection">
						<xsl:with-param name="language" select="$language"/>
					</xsl:apply-templates>
					<xsl:apply-templates select="verificationScalesSection" />
					<xsl:apply-templates select="overallConfidenceSection" />
				</div>
				<!-- REPORT OVERVIEW -->
				<div class="section group">
				</div>
				<!-- Scales -->
				<div class="section group">
				</div>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="introductionSection">
		<div class="section intro">
			<h1>
				<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
			</h1>
			<br />
			<p>
				<span>
					<xsl:value-of select="p1" disable-output-escaping="yes"></xsl:value-of>
				</span>
			</p>
			<br />
			<p>
				<span>
					<xsl:value-of select="p2" disable-output-escaping="yes"></xsl:value-of>
				</span>
			</p>
			<br />
			<p>
				<span>
					<xsl:value-of select="p3" disable-output-escaping="yes"></xsl:value-of>
				</span>
			</p>
			<br />
			<div>
				<div style="clear:both; padding:5px; "></div>
				<div style="float:left; clear:left; width:20%; padding-left:30px;">
					<span style="font-weight:700;">
						<xsl:value-of select="mentalDescriptionHeading"></xsl:value-of>
					</span>
				</div>
				<div style="float:left; clear:right; width:75%; padding-left:15px;">
					<span>
						<xsl:value-of select="mentalDescription"></xsl:value-of>
					</span>
				</div>
				<div style="clear:both; padding:5px; "></div>
			</div>
			<div>
				<div
					style="float:left; clear:left; width:20%; padding-left:30px; font-weight:700;">
					<span style="font-weight:700;">
						<xsl:value-of select="peopleDescriptionHeading"></xsl:value-of>
					</span>
				</div>
				<div style="float:left; clear:right; width:75%; padding-left:15px;">
					<span>
						<xsl:value-of select="peopleDescription"></xsl:value-of>
					</span>
				</div>
				<div style="clear:both; padding:5px; "></div>
			</div>
			<div>
				<div
					style="float:left; clear:left; width:20%; padding-left:30px; font-weight:700;">
					<span style="font-weight:700;">
						<xsl:value-of select="changeDescriptionHeading"></xsl:value-of>
					</span>
				</div>
				<div style="float:left; clear:right; width:75%; padding-left:15px;">
					<span>
						<xsl:value-of select="changeDescription"></xsl:value-of>
					</span>
				</div>
				<div style="clear:both; padding:5px; "></div>
			</div>
			<div>
				<div
					style="float:left; clear:left; width:20%; padding-left:30px; font-weight:700;">
					<span style="font-weight:700;">
						<xsl:value-of select="resultsDescriptionHeading"></xsl:value-of>
					</span>
				</div>
				<div style="float:left; clear:right; width:75%; padding-left:15px;">
					<span>
						<xsl:value-of select="resultsDescription"></xsl:value-of>
					</span>
				</div>
				<div style="clear:both; padding:5px; "></div>
			</div>
			<div>
				<div
					style="float:left; clear:left; width:20%; padding-left:30px; font-weight:700;">
					<span style="font-weight:700;">
						<xsl:value-of select="selfAwareDescriptionHeading"></xsl:value-of>
					</span>
				</div>
				<div style="float:left; clear:right; width:75%; padding-left:15px;">
					<span>
						<xsl:value-of select="selfAwareDescription"></xsl:value-of>
					</span>
				</div>
				<div style="clear:both;"></div>
			</div>
		</div>
		<br />
	</xsl:template>
	<xsl:template match="overviewSection">
		<div class="group">
			<h1>
				<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
			</h1>
			<br />
			<h3>
				<xsl:value-of select="part1Heading"
					disable-output-escaping="yes"></xsl:value-of>
			</h3>
			<ul>
				<li>
					<xsl:value-of select="part1B1"
						disable-output-escaping="yes"></xsl:value-of>
				</li>
				<li>
					<xsl:value-of select="part1B2"
						disable-output-escaping="yes"></xsl:value-of>
				</li>
				<li>
					<xsl:value-of select="part1B3"
						disable-output-escaping="yes"></xsl:value-of>
				</li>
				<li>
					<xsl:value-of select="part1B4"
						disable-output-escaping="yes"></xsl:value-of>
					<ul>
						<li>
							<xsl:value-of select="part1B4S1"
								disable-output-escaping="yes"></xsl:value-of>
						</li>
						<li>
							<xsl:value-of select="part1B4S2"
								disable-output-escaping="yes"></xsl:value-of>
						</li>
						<li>
							<xsl:value-of select="part1B4S3"
								disable-output-escaping="yes"></xsl:value-of>
						</li>
						<li>
							<xsl:value-of select="part1B4S4"
								disable-output-escaping="yes"></xsl:value-of>
						</li>
						<li>
							<xsl:value-of select="part1B4S5"
								disable-output-escaping="yes"></xsl:value-of>
						</li>
					</ul>
				</li>
			</ul>
			<br />
			<h3>
				<xsl:value-of select="part2Heading"
					disable-output-escaping="yes"></xsl:value-of>
			</h3>
			<ul>
				<li>
					<xsl:value-of select="part2B1"
						disable-output-escaping="yes"></xsl:value-of>
				</li>
				<li>
					<xsl:value-of select="part2B2"
						disable-output-escaping="yes"></xsl:value-of>
				</li>
			</ul>
			<br />
			<h3>
				<xsl:value-of select="part3Heading"
					disable-output-escaping="yes"></xsl:value-of>
			</h3>
			<ul>
				<li>
					<xsl:value-of select="part3B1"
						disable-output-escaping="yes"></xsl:value-of>
				</li>
				<li>
					<xsl:value-of select="part3B2"
						disable-output-escaping="yes"></xsl:value-of>
				</li>
			</ul>
		</div>
		<br />
	</xsl:template>
	<xsl:template match="agilityScoresSection">
		<xsl:param name="language" />
		<div class="group">
			<h1>
				<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
			</h1>
			<div class="group">
				<h2>
					<xsl:value-of select="heading"
						disable-output-escaping="yes"></xsl:value-of>
				</h2>
				<!-- <br /> -->
				<h3>
					<xsl:value-of select="p1Heading"
						disable-output-escaping="yes"></xsl:value-of>
				</h3>
				<p>
					<xsl:value-of select="p1" disable-output-escaping="yes"></xsl:value-of>
				</p>
				<br />
				<h3>
					<xsl:value-of select="p2Heading"
						disable-output-escaping="yes"></xsl:value-of>
				</h3>
				<p>
					<xsl:value-of select="p2" disable-output-escaping="yes"></xsl:value-of>
				</p>
				<ul>
					<li>
						<xsl:value-of select="p2B1" disable-output-escaping="yes"></xsl:value-of>
					</li>
					<li>
						<xsl:value-of select="p2B2" disable-output-escaping="yes"></xsl:value-of>
					</li>
				</ul>
				<br />
				<h3>
					<xsl:value-of select="p3Heading"
						disable-output-escaping="yes"></xsl:value-of>
				</h3>
				<p>
					<xsl:value-of select="p3" disable-output-escaping="yes"></xsl:value-of>
				</p>
			</div>
			<br />

			<div class="group">
				<h3>
					<xsl:value-of select="p4Heading"
						disable-output-escaping="yes"></xsl:value-of>
				</h3>
				<p>
					<xsl:value-of select="p4" disable-output-escaping="yes"></xsl:value-of>
				</p>
				<xsl:choose>
					<xsl:when test="$language != 'ru'">
						<br />
					</xsl:when>
					<xsl:otherwise>
						<!-- nothing! -->
					</xsl:otherwise>
				</xsl:choose>
				
				<h3>
					<xsl:value-of select="p5Heading"
						disable-output-escaping="yes"></xsl:value-of>
				</h3>
				<p>
					<xsl:value-of select="p5" disable-output-escaping="yes"></xsl:value-of>
				</p>
				<xsl:choose>
					<xsl:when test="$language != 'ru'">
						<br />
					</xsl:when>
					<xsl:otherwise>
						<!-- nothing! -->
					</xsl:otherwise>
				</xsl:choose>
				<h3>
					<xsl:value-of select="p6Heading"
						disable-output-escaping="yes"></xsl:value-of>
				</h3>
				<p>
					<xsl:value-of select="p6" disable-output-escaping="yes"></xsl:value-of>
				</p>
				<xsl:choose>
					<xsl:when test="$language != 'ru'">
						<br />
					</xsl:when>
					<xsl:otherwise>
						<!-- nothing! -->
					</xsl:otherwise>
				</xsl:choose>
			</div>
			<div class="group">
				<h3>
					<xsl:value-of select="activitiesListHeading"
						disable-output-escaping="yes"></xsl:value-of>
				</h3>
				<ul>
					<li>
						<xsl:value-of select="activity1"
							disable-output-escaping="yes"></xsl:value-of>
					</li>
					<li>
						<xsl:value-of select="activity2"
							disable-output-escaping="yes"></xsl:value-of>
					</li>
					<li>
						<xsl:value-of select="activity3"
							disable-output-escaping="yes"></xsl:value-of>
					</li>
					<li>
						<xsl:value-of select="activity4"
							disable-output-escaping="yes"></xsl:value-of>
					</li>
					<li>
						<xsl:value-of select="activity5"
							disable-output-escaping="yes"></xsl:value-of>
					</li>
					<li>
						<xsl:value-of select="activity6"
							disable-output-escaping="yes"></xsl:value-of>
					</li>
				</ul>
			</div>
			<br />
		</div>
		<xsl:apply-templates select="scales" mode="summary"></xsl:apply-templates>
		<xsl:apply-templates select="scales" mode="detailed"></xsl:apply-templates>
	</xsl:template>
	<xsl:template match="scales" mode="detailed">
		<xsl:for-each select="scale">
			<!-- ALL DETAILED SCALES -->
			<xsl:if test="position() != 1">
				<div class="group detailed-scale-container">
					<h1><xsl:value-of select="title" disable-output-escaping="yes"/></h1>
					<h2 class="center-header">
						<xsl:value-of select="heading"
							disable-output-escaping="yes"></xsl:value-of>
					</h2>
					<div class="key-container">
						<table>
							<tr>
								<td class="scale-two-label-low">
									<span style="float:right;">
										<div class="summary-side-label">
											<xsl:value-of select="labelLow" disable-output-escaping="yes"></xsl:value-of>
										</div>
									</span>
								</td>
								<td class="scale-two-keys scale-two-keys-top">
									<div class="relative-div">
										<span class="key key-a-50-left">50</span>
										<span class="key key-a-40-left">40</span>
										<span class="key key-a-17-left">17</span>
										<span class="key key-a-0">0</span>
										<span class="key key-a-17-right">17</span>
										<span class="key key-a-40-right">40</span>
										<span class="key key-a-50-right">50</span>
									</div>
								</td>
								<td class="scale-two-label-high">
									<span class="summary-side-label">
										<xsl:value-of select="labelHigh"
											disable-output-escaping="yes"></xsl:value-of>
									</span>
								</td>
							</tr>
						</table>
					</div>
					<div>
						<div class="scale-one-container">
							<div class="scale-left-side">
								&#160;
								<xsl:if test="pScoreSide = 'low'">
									<div>
										<xsl:attribute name="style">width:<xsl:value-of
											select="pScore * 100 div 50"></xsl:value-of>%</xsl:attribute>
										<xsl:attribute name="class">score-bar score-bar-left p-score-bar<xsl:if
											test="pScore &lt; 5"> score-offset-low</xsl:if></xsl:attribute>
										<span>
											<xsl:value-of select="pScore"></xsl:value-of>
										</span>
									</div>
								</xsl:if>
								<xsl:if test="adjpScoreSide = 'low'">
									<div>
										<xsl:attribute name="style">width:<xsl:value-of
											select="adjpScore * 100 div 50"></xsl:value-of>%</xsl:attribute>
										<xsl:attribute name="class">score-bar score-bar-left p-adj-score-bar p-adj-summary-bar-special p-adj-summary-bar-2-<xsl:value-of
											select="position()"></xsl:value-of><xsl:if
											test="adjpScore &lt; 5"> score-offset-low</xsl:if></xsl:attribute>
										<span>
											<xsl:value-of select="adjpScore"></xsl:value-of>
										</span>
									</div>
								</xsl:if>
							</div>
							<div class="scale-right-side">
								&#160;
								<xsl:if test="pScoreSide = 'high'">
									<div>
										<xsl:attribute name="style">width:<xsl:value-of
											select="pScore * 100 div 50"></xsl:value-of>%</xsl:attribute>
										<xsl:attribute name="class">score-bar score-bar-right p-score-bar<xsl:if
											test="pScore &lt; 5"> score-offset</xsl:if></xsl:attribute>
										<span>
											<xsl:value-of select="pScore"></xsl:value-of>
										</span>
									</div>
								</xsl:if>
								<xsl:if test="adjpScoreSide = 'high'">
									<div>
										<xsl:attribute name="style">width:<xsl:value-of
											select="adjpScore * 100 div 50"></xsl:value-of>%</xsl:attribute>
										<xsl:attribute name="class">score-bar score-bar-right p-adj-score-bar p-adj-summary-bar-special p-adj-summary-bar-2-<xsl:value-of
											select="position()"></xsl:value-of><xsl:if
											test="adjpScore &lt; 5"> score-offset</xsl:if></xsl:attribute>
										<span>
											<xsl:value-of select="adjpScore"></xsl:value-of>
										</span>
									</div>
								</xsl:if>
							</div>
						</div>
					</div>
					<div class="key-container">
						<table>
							<tr>
								<td class="po-left-td">
									<xsl:value-of select="keyPotentialOveruse"></xsl:value-of>
								</td>
								<td class="s-left-td">
									<xsl:value-of select="keyStrong"></xsl:value-of>
								</td>
								<td class="b-td">
									<xsl:value-of select="keyBalanced"></xsl:value-of>
								</td>
								<td class="s-right-td">
									<xsl:value-of select="keyStrong"></xsl:value-of>
								</td>
								<td class="po-right-td">
									<xsl:value-of select="keyPotentialOveruse"></xsl:value-of>
								</td>
							</tr>
						</table>
					</div>
					<div class="scale-two-container">
						<!-- 5% minimum , 88.75% maximum inline WIDTH for the first/left score 
							bar below! -->
						<span class="score-bar score-bar-right p-score-bar scale-two-bar">
							<xsl:variable name="widthAdjScaleLeft" select="padjScaleLeft"></xsl:variable>
							<xsl:if
								test="$widthAdjScaleLeft &gt;= 3 and $widthAdjScaleLeft &lt;= 88.75">
								<xsl:attribute name="style">width:<xsl:copy-of
									select="$widthAdjScaleLeft" />%</xsl:attribute>
							</xsl:if>
							<xsl:if test="$widthAdjScaleLeft &lt; 3">
								<xsl:attribute name="style">width:3%</xsl:attribute>
							</xsl:if>
							<xsl:if test="$widthAdjScaleLeft &gt; 88.75">
								<xsl:attribute name="style">width:88.75%</xsl:attribute>
							</xsl:if>
							<span>
								<xsl:value-of select="padjScaleLeft"></xsl:value-of>
							</span>
						</span>
						<span
							class="score-bar score-bar-right p-adj-score-bar scale-two-bar scale-two-bar-cell">
							<xsl:attribute name="class">score-bar score-bar-right p-adj-score-bar scale-two-bar scale-two-bar-cell p-adj-summary-bar-special p-adj-summary-bar-2-<xsl:value-of
								select="position()"></xsl:value-of></xsl:attribute>
							<span>
								<xsl:value-of select="padjScaleMid"></xsl:value-of>
							</span>
						</span>
						<span
							class="score-bar score-bar-right p-score-bar scale-two-bar scale-two-bar-cell">
							<span>
								<xsl:value-of select="padjScaleRight"></xsl:value-of>
							</span>
						</span>
					</div>
					<div class="key-container">
						<table>
							<tr>
								<td class="scale-two-label">
									&#160; </td>
								<td class="scale-two-keys">
									<div class="relative-div">
										<span class="key key-b-0">0</span>
										<span class="key key-b-10">10</span>
										<span class="key key-b-33">33</span>
										<span class="key key-b-50">50</span>
										<span class="key key-b-67">67</span>
										<span class="key key-b-90">90</span>
										<span class="key key-b-100">100</span>
									</div>
								</td>
								<td>&#160;</td>
							</tr>
						</table>
					</div>
					<!-- <table class="group" style="width:95%;"> -->
					<!-- <tr> -->
					<!-- <td class="scale-list-quad"> -->
					<!-- <p> -->
					<!-- <xsl:value-of select="PIOLabel" ></xsl:value-of> -->
					<!-- </p> -->
					<!-- <span> -->
					<!-- <xsl:value-of select="PIOLowList" disable-output-escaping="yes"></xsl:value-of> -->
					<!-- </span> -->
					<!-- </td> -->
					<!-- <td class="scale-list-quad"> -->
					<!-- <p> -->
					<!-- <xsl:value-of select="BTLabel" ></xsl:value-of> -->
					<!-- </p> -->
					<!-- <span> -->
					<!-- <xsl:value-of select="BTLowList" disable-output-escaping="yes"></xsl:value-of> -->
					<!-- </span> -->
					<!-- </td> -->
					<!-- <td class="scale-list-quad"> -->
					<!-- <p> -->
					<!-- <xsl:value-of select="PIOLabel" ></xsl:value-of> -->
					<!-- </p> -->
					<!-- <span> -->
					<!-- <xsl:value-of select="PIOLowList" disable-output-escaping="yes"></xsl:value-of> -->
					<!-- </span> -->
					<!-- </td> -->
					<!-- <td class="scale-list-quad"> -->
					<!-- <p> -->
					<!-- <xsl:value-of select="BTLabel" ></xsl:value-of> -->
					<!-- </p> -->
					<!-- <span> -->
					<!-- <xsl:value-of select="BTLowList" disable-output-escaping="yes"></xsl:value-of> -->
					<!-- </span> -->
					<!-- </td> -->
					<!-- </tr> -->
					<!-- </table> -->
					<!-- <div class="group" style="display:block"> -->
					<div class="scale-list-left">
						<div class="scale-list-quad">
							<h3>
								<xsl:value-of select="PIOLabel"></xsl:value-of>
							</h3>
							<xsl:value-of select="PIOLowList"
								disable-output-escaping="yes"></xsl:value-of>
						</div>
						<div class="scale-list-quad">
							<h3>
								<xsl:value-of select="BTLabel"></xsl:value-of>
							</h3>
							<xsl:value-of select="BTLowList"
								disable-output-escaping="yes"></xsl:value-of>
						</div>
					</div>
					<div class="scale-list-right">
						<div class="scale-list-quad">
							<h3>
								<xsl:value-of select="BTLabel"></xsl:value-of>
							</h3>
							<xsl:value-of select="BTHighList"
								disable-output-escaping="yes"></xsl:value-of>
						</div>
						<div class="scale-list-quad">
							<h3>
								<xsl:value-of select="PIOLabel"></xsl:value-of>
							</h3>
							<xsl:value-of select="PIOHighList"
								disable-output-escaping="yes"></xsl:value-of>
						</div>
					</div>
					<div style="display:block; clear:both;">&#160;</div>
				</div>
				<!-- </div> -->
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="scales" mode="summary">
<!-- 		<xsl:value-of select="scalesHeading"></xsl:value-of> -->
		<xsl:for-each select="scale">
			<xsl:if test="position() = 1">
				<!-- OVERALL SCALE (ONLY FIRST IN LIST) -->

				<div class="group detailed-scale-container">
					<h1>
						<xsl:value-of select="title" disable-output-escaping="yes" />
					</h1>
					<h2 class="center-header">
						<xsl:value-of select="heading"
							disable-output-escaping="yes"></xsl:value-of>
					</h2>
					<div class="key-container">
						<table>
							<tr>
								<td class="scale-two-label">
									<span>
										<xsl:value-of select="chartLabel"
											disable-output-escaping="yes"></xsl:value-of>
									</span>
								</td>
								<td class="scale-two-keys scale-two-keys-top">
									<div class="relative-div">
										<span class="key key-a-50-left">50</span>
										<span class="key key-a-40-left">40</span>
										<span class="key key-a-17-left">17</span>
										<span class="key key-a-0">0</span>
										<span class="key key-a-17-right">17</span>
										<span class="key key-a-40-right">40</span>
										<span class="key key-a-50-right">50</span>
									</div>
								</td>
								<td>&#160;</td>
							</tr>
						</table>
					</div>
					<div>
						<div class="scale-one-container">
							<div class="scale-left-side">
							&#160;
								<xsl:if test="pScoreSide = 'low'">
									<div>
										<xsl:attribute name="style">width:<xsl:value-of
											select="pScore * 100 div 50"></xsl:value-of>%</xsl:attribute>
										<xsl:attribute name="class">score-bar score-bar-left p-score-bar<xsl:if
											test="pScore &lt; 5"> score-offset-low</xsl:if></xsl:attribute>
										<span>
											<xsl:value-of select="pScore"></xsl:value-of>
										</span>
									</div>
								</xsl:if>
								<xsl:if test="adjpScoreSide = 'low'">
									<div>
										<xsl:attribute name="style">width:<xsl:value-of
											select="adjpScore * 100 div 50"></xsl:value-of>%</xsl:attribute>
										<xsl:attribute name="class">score-bar score-bar-left p-adj-score-bar<xsl:if
											test="adjpScore &lt; 5"> score-offset-low</xsl:if></xsl:attribute>
										<span>
											<xsl:value-of select="adjpScore"></xsl:value-of>
										</span>
									</div>
								</xsl:if>
							</div>
							<div class="scale-right-side">
							&#160;
								<xsl:if test="pScoreSide = 'high'">
									<div>
										<xsl:attribute name="style">width:<xsl:value-of
											select="pScore * 100 div 50"></xsl:value-of>%</xsl:attribute>
										<xsl:attribute name="class">score-bar score-bar-right p-score-bar<xsl:if
											test="pScore &lt; 5"> score-offset</xsl:if></xsl:attribute>
										<span>
											<xsl:value-of select="pScore"></xsl:value-of>
										</span>
									</div>
								</xsl:if>
								<xsl:if test="adjpScoreSide = 'high'">
									<div>
										<xsl:attribute name="style">width:<xsl:value-of
											select="adjpScore * 100 div 50"></xsl:value-of>%</xsl:attribute>
										<xsl:attribute name="class">score-bar score-bar-right p-adj-score-bar<xsl:if
											test="adjpScore &lt; 5"> score-offset</xsl:if></xsl:attribute>
										<span>
											<xsl:value-of select="adjpScore"></xsl:value-of>
										</span>
									</div>
								</xsl:if>
							</div>
						</div>
					</div>
					<div class="key-container">
						<table>
							<tr>
								<td class="po-left-td">
									<xsl:value-of select="keyPotentialOveruse"></xsl:value-of>
								</td>
								<td class="s-left-td">
									<xsl:value-of select="keyStrong"></xsl:value-of>
								</td>
								<td class="b-td">
									<xsl:value-of select="keyBalanced"></xsl:value-of>
								</td>
								<td class="s-right-td">
									<xsl:value-of select="keyStrong"></xsl:value-of>
								</td>
								<td class="po-right-td">
									<xsl:value-of select="keyPotentialOveruse"></xsl:value-of>
								</td>
							</tr>
						</table>
					</div>
					<div class="scale-two-container">
						<!-- 5% minimum , 88.75% maximum inline WIDTH for the first/left score 
							bar below! -->
						<span class="score-bar score-bar-right p-score-bar scale-two-bar">
							<xsl:variable name="widthAdjScaleLeft" select="padjScaleLeft"></xsl:variable>
							<xsl:if
								test="$widthAdjScaleLeft &gt;= 3 and $widthAdjScaleLeft &lt;= 88.75">
								<xsl:attribute name="style">width:<xsl:copy-of
									select="$widthAdjScaleLeft" />%</xsl:attribute>
							</xsl:if>
							<xsl:if test="$widthAdjScaleLeft &lt; 3">
								<xsl:attribute name="style">width:3%</xsl:attribute>
							</xsl:if>
							<xsl:if test="$widthAdjScaleLeft &gt; 88.75">
								<xsl:attribute name="style">width:88.75%</xsl:attribute>
							</xsl:if>
							<span>
								<xsl:value-of select="padjScaleLeft"></xsl:value-of>
							</span>
						</span>
						<span
							class="score-bar score-bar-right p-adj-score-bar scale-two-bar scale-two-bar-cell">
							<span>
								<xsl:value-of select="padjScaleMid"></xsl:value-of>
							</span>
						</span>
						<span
							class="score-bar score-bar-right p-score-bar scale-two-bar scale-two-bar-cell">
							<span>
								<xsl:value-of select="padjScaleRight"></xsl:value-of>
							</span>
						</span>
					</div>
					<div class="key-container">
						<table>
							<tr>
								<td class="scale-two-label">
									<span>
										<xsl:value-of select="padjScaleLabel"></xsl:value-of>
									</span>
								</td>
								<td class="scale-two-keys ">
									<div class="relative-div">
										<span class="key key-b-0">0</span>
										<span class="key key-b-10">10</span>
										<span class="key key-b-33">33</span>
										<span class="key key-b-50">50</span>
										<span class="key key-b-67">67</span>
										<span class="key key-b-90">90</span>
										<span class="key key-b-100">100</span>
									</div>
								</td>
								<td>&nbsp;
								</td>
							</tr>
						</table>
					</div>
					<div class="score-desc score-desc-low">
						<h2>
							<xsl:value-of select="labelLow"></xsl:value-of>
						</h2>
						<p>
							<xsl:value-of select="descLow"></xsl:value-of>
						</p>
					</div>
					<div class="score-desc score-desc-high">
						<h2>
							<xsl:value-of select="labelHigh"></xsl:value-of>
						</h2>
						<p>
							<xsl:value-of select="descHigh"></xsl:value-of>
						</p>
					</div>
					<div style="display:block; clear:both;">&#160;</div>
					<h2 class="center-header">
						<xsl:value-of select="scoreNarrativeHeading"></xsl:value-of>
					</h2>
					<p>
						<xsl:value-of select="scoreNarrative"></xsl:value-of>
					</p>
				</div>
				<br />
			</xsl:if>
		</xsl:for-each>
		<div class="group">
			<h1><xsl:value-of select="/viaEdgeCoachingReport/profileHeading" disable-output-escaping="yes"/></h1>
			<xsl:for-each select="scale">
				<xsl:if test="position() != 1">
					<!-- ALL SUMMARY SCALES -->
					<div class="group">
<!-- 						<xsl:if test="position() = 2"> -->
<!-- 							<h1> -->
<!-- 								<xsl:value-of select="title" -->
<!-- 									disable-output-escaping="yes" /> -->
<!-- 							</h1> -->
<!-- 						</xsl:if> -->
						<h2>
							<xsl:attribute name="class">center-header <xsl:if
								test="position() = 3 or position() = 4 or position() = 6">
							ptop15</xsl:if>
						</xsl:attribute>
							<xsl:value-of select="heading2"
								disable-output-escaping="yes"></xsl:value-of>
						</h2>
						<div class="key-container">
							<table>
								<tr>
									<td class="scale-two-label-low test-label">
										<span style="float:right;">
											<div class="summary-side-label">
												<xsl:value-of select="labelLow" disable-output-escaping="yes"></xsl:value-of>
											</div>
										</span>
									</td>
									<td class="scale-two-keys scale-two-keys-top">
										<div class="relative-div">
											<span class="key key-a-50-left">50</span>
											<span class="key key-a-40-left">40</span>
											<span class="key key-a-17-left">17</span>
											<span class="key key-a-0">0</span>
											<span class="key key-a-17-right">17</span>
											<span class="key key-a-40-right">40</span>
											<span class="key key-a-50-right">50</span>
										</div>
									</td>
									<td class="scale-two-label-high test-label">
										<div class="summary-side-label">
											<xsl:value-of select="labelHigh" disable-output-escaping="yes"></xsl:value-of>
										</div>
									</td>
								</tr>
							</table>
						</div>
						<div>
							<div class="scale-one-container scale-one-summary">
								<div class="scale-left-side scale-side-summary">
									<xsl:if test="adjpScoreSide = 'low'">
										<div>
											<xsl:attribute name="style">width:<xsl:value-of
												select="adjpScore * 100 div 50"></xsl:value-of>%</xsl:attribute>
											<xsl:attribute name="class">score-bar score-bar-left summary-bar p-adj-summary-bar-<xsl:value-of
												select="position()"></xsl:value-of><xsl:if
												test="adjpScore &lt; 5"> score-offset-low</xsl:if></xsl:attribute>
											<span>
												<xsl:value-of select="adjpScore"></xsl:value-of>
											</span>
										</div>
									</xsl:if>
									<xsl:if test="adjpScoreSide = 'high'">
										&#160;
									</xsl:if>
								</div>
								<div class="scale-right-side scale-side-summary">
									<xsl:if test="adjpScoreSide = 'high'">
										<div>
											<xsl:attribute name="style">width:<xsl:value-of
												select="adjpScore * 100 div 50"></xsl:value-of>%</xsl:attribute>
											<xsl:attribute name="class">score-bar score-bar-right summary-bar p-adj-summary-bar-<xsl:value-of
												select="position()"></xsl:value-of><xsl:if
												test="adjpScore &lt; 5"> score-offset</xsl:if></xsl:attribute>
											<span>
												<xsl:value-of select="adjpScore"></xsl:value-of>
											</span>
										</div>
									</xsl:if>
									<xsl:if test="adjpScoreSide = 'low'">
										&#160;
									</xsl:if>
								</div>
							</div>
						</div>
						<div class="scale-two-container">
							<!-- 5% minimum , 88.75% maximum inline WIDTH for the first/left score 
								bar below! -->
							<xsl:variable name="widthAdjScaleLeft" select="padjScaleLeft"></xsl:variable>
							<span class="score-bar score-bar-right p-score-bar scale-two-bar">
								<xsl:if
									test="$widthAdjScaleLeft &gt;= 3 and $widthAdjScaleLeft &lt;= 88.75">
									<xsl:attribute name="style">width:<xsl:copy-of
										select="$widthAdjScaleLeft" />%</xsl:attribute>
								</xsl:if>
								<xsl:if test="$widthAdjScaleLeft &lt; 3">
									<xsl:attribute name="style">width:3%</xsl:attribute>
								</xsl:if>
								<xsl:if test="$widthAdjScaleLeft &gt; 88.75">
									<xsl:attribute name="style">width:88.75%</xsl:attribute>
								</xsl:if>
								<span>
									<xsl:value-of select="padjScaleLeft"></xsl:value-of>
								</span>
							</span>
							<span>
								<xsl:attribute name="class">score-bar score-bar-right scale-two-bar scale-two-bar-cell p-adj-summary-bar-special p-adj-summary-bar-2-<xsl:value-of
									select="position()"></xsl:value-of></xsl:attribute>
								<span>
									<xsl:value-of select="padjScaleMid"></xsl:value-of>
								</span>
							</span>
							<span
								class="score-bar score-bar-right p-score-bar scale-two-bar scale-two-bar-cell">
								<span>
									<xsl:value-of select="padjScaleRight"></xsl:value-of>
								</span>
							</span>
						</div>
						<div class="key-container">
							<table>
								<tr>
									<td class="scale-two-label">
										&#160;<!-- <span>Scale Two Label</span> -->
									</td>
									<td class="scale-two-keys">
										<div class="relative-div">
											<span class="key key-b-0">0</span>
											<span class="key key-b-10">10</span>
											<span class="key key-b-33">33</span>
											<span class="key key-b-50">50</span>
											<span class="key key-b-67">67</span>
											<span class="key key-b-90">90</span>
											<span class="key key-b-100">100</span>
										</div>
									</td>
									<td>&#160;</td>
								</tr>
							</table>
						</div>
						<xsl:if test="position() = 4 or position() = 6">
							<div class="key-container group">
								<table>
									<tr>
										<td class="po-left-td">
											<xsl:value-of select="keyPotentialOveruse"></xsl:value-of>
										</td>
										<td class="s-left-td">
											<xsl:value-of select="keyStrong"></xsl:value-of>
										</td>
										<td class="b-td">
											<xsl:value-of select="keyBalanced"></xsl:value-of>
										</td>
										<td class="s-right-td">
											<xsl:value-of select="keyStrong"></xsl:value-of>
										</td>
										<td class="po-right-td">
											<xsl:value-of select="keyPotentialOveruse"></xsl:value-of>
										</td>
									</tr>
								</table>
							</div>
						</xsl:if>
					</div>
				</xsl:if>
			</xsl:for-each>
		</div>
		<br/>
	</xsl:template>
	<xsl:template match="overallConfidenceSection">
		<!-- <div class="blank-div-page-break">&#160;</div> <div> -->
		<div class="group">
			<h1>
				<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
			</h1>
			<div class="group">
				<h2>
					<xsl:value-of select="p1Header"
						disable-output-escaping="yes"></xsl:value-of>
				</h2>
				<p>
					<xsl:value-of select="p1" disable-output-escaping="yes"></xsl:value-of>
				</p>
				<br />
			</div>
			<p>
				<xsl:value-of select="p2" disable-output-escaping="yes"></xsl:value-of>
			</p>
			<br />
			<p>
				<xsl:value-of select="p3" disable-output-escaping="yes"></xsl:value-of>
			</p>
			<br />
			<p>
				<xsl:value-of select="p4" disable-output-escaping="yes"></xsl:value-of>
			</p>
			<div class="group">
				<h2>
					<xsl:value-of select="p5Header"
						disable-output-escaping="yes"></xsl:value-of>
				</h2>
				<p>
					<xsl:value-of select="p5" disable-output-escaping="yes"></xsl:value-of>
				</p>
			</div>
			<div class="c-scale-container">
				<h3 style="float:left">
					<xsl:value-of select="scaleLabelLow"
						disable-output-escaping="yes"></xsl:value-of>
				</h3>
				<h3 style="float:right">
					<xsl:value-of select="scaleLabelHigh"
						disable-output-escaping="yes"></xsl:value-of>
				</h3>
				<table class="cScale">
					<tr>
						<td class="cScaleLowCell">
							<xsl:if test="score &lt; -5">
								<xsl:attribute name="class">cScaleLowCell cScaleMark</xsl:attribute>
							</xsl:if>
							&#160;
						</td>
						<td class="cScaleMidCell">
							<xsl:if test="score &lt; -3 and score &gt; -6">
								<xsl:attribute name="class">cScaleMidCell cScaleMark</xsl:attribute>
							</xsl:if>
							&#160;
						</td>
						<td class="cScaleHighCell">
							<xsl:if test="score &lt; 0 and score &gt; -4">
								<xsl:attribute name="class">cScaleHighCell cScaleMark</xsl:attribute>
							</xsl:if>
							&#160;
						</td>
						<td class="cScaleHighCell cHighBorders">
							<xsl:if test="score &lt; 3 and score &gt; -1">
								<xsl:attribute name="class">cScaleHighCell cHighBorders cScaleMark</xsl:attribute>
							</xsl:if>
							&#160;
						</td>
						<td class="cScaleHighCell">
							<xsl:if test="score &gt; 2">
								<xsl:attribute name="class">cScaleHighCell cScaleMark</xsl:attribute>
							</xsl:if>
							&#160;
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="verificationScalesSection">

		<div class="group">
			<!-- VerificationScalesSection -->
			<h1>
				<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
			</h1>
			<h2 class="red_h2">
				<xsl:value-of select="warningHeader"
					disable-output-escaping="yes"></xsl:value-of>
			</h2>
			<div class="group">
				<h2>
					<xsl:value-of select="p1Header"
						disable-output-escaping="yes"></xsl:value-of>
				</h2>
				<p>
					<xsl:value-of select="p1" disable-output-escaping="yes"></xsl:value-of>
				</p>
				<br />
			</div>
			<div class="group">
				<h3>
					<xsl:value-of select="p2Header"
						disable-output-escaping="yes"></xsl:value-of>
				</h3>
				<p>
					<xsl:value-of select="p2" disable-output-escaping="yes"></xsl:value-of>
				</p>
				<br />
			</div>
			<div class="group">
				<h3>
					<xsl:value-of select="p3Header"
						disable-output-escaping="yes"></xsl:value-of>
				</h3>
				<p>
					<xsl:value-of select="p3" disable-output-escaping="yes"></xsl:value-of>
				</p>
				<br />
			</div>
			<div class="group">
				<h3>
					<xsl:value-of select="p4Header"
						disable-output-escaping="yes"></xsl:value-of>
				</h3>
				<p>
					<xsl:value-of select="p4" disable-output-escaping="yes"></xsl:value-of>
				</p>
				<br />
			</div>
			<div class="group">
				<h3>
					<xsl:value-of select="p5Header"
						disable-output-escaping="yes"></xsl:value-of>
				</h3>
				<p>
					<xsl:value-of select="p5" disable-output-escaping="yes"></xsl:value-of>
				</p>
				<br />
			</div>
			<div class="group">
				<h3>
					<xsl:value-of select="p6Header"
						disable-output-escaping="yes"></xsl:value-of>
				</h3>
				<p>
					<xsl:value-of select="p6" disable-output-escaping="yes"></xsl:value-of>
				</p>
			</div>
			<!-- <div class="group"> <xsl:apply-templates select="vScales"></xsl:apply-templates> 
				</div> -->
		</div>
		<xsl:apply-templates select="vScales"></xsl:apply-templates>
	</xsl:template>
	<xsl:template match="vScales">
		<div class="blank-div-page-break">&#160;</div>
		<div>
			<div class="vScaleThird vScaleThirdLeft">
				<h3 class="vScaleLabel">
					<xsl:value-of select="../scoreDescLabelLow"></xsl:value-of>
				</h3>
			</div>
			<div class="vScaleThird vScaleThirdMid">
				<div class="relative-div">
					&#160;
					<span class="key key-v-0">0</span>
					<span class="key key-v-20">20</span>
					<span class="key key-v-40">40</span>
					<span class="key key-v-60">60</span>
					<span class="key key-v-80">80</span>
					<span class="key key-v-100">100</span>
				</div>
			</div>
			<div class="vScaleThird vScaleThirdRight">
				<h3 class="vScaleLabel">
					<xsl:value-of select="../scoreDescLabelHigh"
						disable-output-escaping="yes"></xsl:value-of>
				</h3>
			</div>
			<xsl:for-each select="vScale">

				<xsl:choose>
					<xsl:when test="position() = 1">
						<div class="group float-container">
							<h3 class="vScaleH3">
								<xsl:value-of select="title"
									disable-output-escaping="yes"></xsl:value-of>
							</h3>
							<div class="vScaleThird vScaleThirdLeft">
								<xsl:value-of select="lowScoreDesc"
									disable-output-escaping="yes"></xsl:value-of>
							</div>
							<div class="vScaleThird vScaleThirdMid">
								<span class="vScaleTick">&#160;</span>
								<span class="vScaleTick">&#160;</span>
								<span class="vScaleTick">&#160;</span>
								<span class="vScaleTick">&#160;</span>
								<span class="vScaleTick vScaleTickLast">&#160;</span>
								<span>
									<xsl:attribute name="class">score-bar score-bar-right p-score-bar vScaleScore <xsl:if
										test="score &lt; 5">score-offset</xsl:if>
						</xsl:attribute>
									<xsl:attribute name="style">width:<xsl:value-of
										select="score" disable-output-escaping="yes"></xsl:value-of>%;</xsl:attribute>
									<span>
										<xsl:value-of select="score"
											disable-output-escaping="yes"></xsl:value-of>
									</span>
								</span>
							</div>
							<div class="vScaleThird vScaleThirdRight">
								<xsl:value-of select="highScoreDesc"
									disable-output-escaping="yes"></xsl:value-of>
							</div>
						</div>
					</xsl:when>
					<xsl:otherwise>
						<div class="group float-container ptop12">
							<h3 class="vScaleH3">
								<xsl:value-of select="title"
									disable-output-escaping="yes"></xsl:value-of>
							</h3>
							<div class="vScaleThird vScaleThirdLeft">
								<xsl:value-of select="lowScoreDesc"
									disable-output-escaping="yes"></xsl:value-of>
							</div>
							<div class="vScaleThird vScaleThirdMid">
								<span class="vScaleTick">&#160;</span>
								<span class="vScaleTick">&#160;</span>
								<span class="vScaleTick">&#160;</span>
								<span class="vScaleTick">&#160;</span>
								<span class="vScaleTick vScaleTickLast">&#160;</span>
								<span>
									<xsl:attribute name="class">score-bar score-bar-right p-score-bar vScaleScore <xsl:if
										test="score &lt; 5">score-offset</xsl:if>
						</xsl:attribute>
									<xsl:attribute name="style">width:<xsl:value-of
										select="score" disable-output-escaping="yes"></xsl:value-of>%;</xsl:attribute>
									<span>
										<xsl:value-of select="score"
											disable-output-escaping="yes"></xsl:value-of>
									</span>
								</span>
							</div>
							<div class="vScaleThird vScaleThirdRight">
								<xsl:value-of select="highScoreDesc"
									disable-output-escaping="yes"></xsl:value-of>
							</div>
						</div>
					</xsl:otherwise>
				</xsl:choose>

			</xsl:for-each>
		</div>
	</xsl:template>
</xsl:stylesheet>