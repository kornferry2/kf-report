<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xslt"
	xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xsl xalan xs">
	<!-- http://stackoverflow.com/questions/9538619/xslt-refuses-to-write-doctype-declaration -->
	<xsl:output method="xml" indent="yes" encoding="UTF-8"
		doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />
	<xsl:param name="baseurl" select="'http://localhost'" />
	<!-- these two not used? -->
	<xsl:param name="genmode" select="'pdf'" />
	<!-- no default match -->
	<xsl:template match="*" />
	<xsl:template match="/viaEdgeGroupReport">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>
					<span>
						<xsl:value-of select="reportTitle" />
					</span>
				</title>
				<!-- <link href="{$baseurl}/core/css/font_{$reportLang}.css" rel="stylesheet" 
					type="text/css" /> -->
				<link href="{$baseurl}/core/css/reportML.css" rel="stylesheet"
					type="text/css" />
				<!-- include coaching report.css for scales styles -->
				<link href="{$baseurl}/projects/viaEdge/coaching/css/report.css"
					rel="stylesheet" type="text/css" />
				<link href="{$baseurl}/projects/viaEdge/groupFeedback/css/report.css"
					rel="stylesheet" type="text/css" />
				<meta name="baseurl" content="{$baseurl}" />
				<meta name="genmode" content="{$genmode}" />
			</head>
			<body>
			<xsl:choose>
					<xsl:when test="@lang = 'ko'">
						<xsl:attribute name="class">ko</xsl:attribute>
					</xsl:when>
					<xsl:when test="@lang = 'zh'">
						<xsl:attribute name="class">zh</xsl:attribute>
					</xsl:when>
					<xsl:when test="@lang = 'ja'">
						<xsl:attribute name="class">ja</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="class"></xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<div class="container">
					<h1>
						<xsl:value-of disable-output-escaping="yes" select="introHeading" />
					</h1>
					<p>
						<xsl:value-of disable-output-escaping="yes" select="reportIntro" />
					</p>
					<br />
					<div class="group">
						<h2>
							<xsl:value-of select="p1Heading" />
						</h2>
						<p>
							<xsl:value-of select="p1" disable-output-escaping="yes" />
						</p>
						<br />
						<h3>
							<xsl:value-of select="p2Heading" />
						</h3>
						<p>
							<xsl:value-of select="p2" disable-output-escaping="yes" />
						</p>
						<br />
						<h3>
							<xsl:value-of select="p3Heading" />
						</h3>
						<p>
							<xsl:value-of select="p3" disable-output-escaping="yes" />
						</p>
						<br />
						<h3>
							<xsl:value-of select="p4Heading" />
						</h3>
						<p>
							<xsl:value-of select="p4" disable-output-escaping="yes" />
						</p>
						<br />
						<ul class="intro-ul">
							<li>
								<xsl:value-of select="p4B1"
									disable-output-escaping="yes" />
							</li>
							<br />
							<li>
								<xsl:value-of select="p4B2"
									disable-output-escaping="yes" />
							</li>
							<br />
							<li>
								<xsl:value-of select="p4B3"
									disable-output-escaping="yes" />
							</li>
							<br />
						</ul>
						<p>
							<xsl:value-of disable-output-escaping="yes" select="p5" />
						</p>
					</div>
					<br />
					<div class="group">
						<h2>
							<xsl:value-of select="p6Heading" />
						</h2>
						<p>
							<xsl:value-of select="p6" disable-output-escaping="yes" />
						</p>
						<br />
					</div>
					<div class="page">
						<xsl:apply-templates select="individualScoresSection">
							<xsl:with-param name="langCode" select="@lang"></xsl:with-param>
						</xsl:apply-templates>
					</div>
					<xsl:apply-templates select="groupSummarySection"></xsl:apply-templates>
					<xsl:apply-templates select="agilityCharts"></xsl:apply-templates>
				</div>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="individualScoresSection">
		<xsl:param name="langCode"></xsl:param>
		<div class="group">
			<h1 class="section">
				<xsl:value-of select="../p1Heading" />
			</h1>
			<!-- <hr /> -->
			<!-- how many rows are there? -->
			<xsl:variable name="tables" select="count(tables/table)" />

			<xsl:variable name="length" select="count(rows/row)" />
			<xsl:for-each select="tables/table">
				<!-- START TABLE -->
				<!-- <xsl:text disable-output-escaping="yes">&lt;table class=&quot;iScores&quot;&gt;</xsl:text> -->
				<table class="iScores group">
					<xsl:variable name="isLastTable" select="position() = $tables"></xsl:variable>
					<xsl:for-each select="rows/row">

						<xsl:if test="position() = 1">
							<tr class="even">
								<th>
									<xsl:value-of disable-output-escaping="yes"
										select="../../../../column1Heading" />
								</th>
								<th class="data-column-wide">
									<xsl:value-of disable-output-escaping="yes"
										select="../../../../column2Heading" />
								</th>
								<th class="data-column-narrow">
									<xsl:value-of disable-output-escaping="yes"
										select="../../../../column3Heading" />
								</th>
								<th class="data-column-narrow">
									<xsl:value-of disable-output-escaping="yes"
										select="../../../../column4Heading" />
								</th>
								<th class="data-column-narrow">
									<xsl:value-of disable-output-escaping="yes"
										select="../../../../column5Heading" />
								</th>
								<th class="data-column-narrow">
									<xsl:value-of disable-output-escaping="yes"
										select="../../../../column6Heading" />
								</th>
								<th class="data-column-narrow">
									<!-- seems we cannot handle XSL 2.0 functions?! on QA/PROD? -->
									<!-- <xsl:value-of disable-output-escaping="yes" select="replace(../../../../column7Heading, 
										'-', '- ')" /> -->
									<xsl:value-of disable-output-escaping="yes"
										select="../../../../column7Heading" />
								</th>
								<th class="data-column-wide">
									<xsl:value-of disable-output-escaping="yes"
										select="../../../../column8Heading" />
								</th>
							</tr>
						</xsl:if>
						<!-- skip this if this is the last ROW of the last TABLE (../) -->
						<xsl:choose>
							<xsl:when test="position()=last() and $isLastTable = true()">
								<!-- handle last row on last table later (table footer) -->
							</xsl:when>
							<xsl:otherwise>
								<tr>
									<xsl:attribute name="class">
								<xsl:choose>
							      <xsl:when test="position() mod 2 = 0">even</xsl:when>
							      <xsl:otherwise>odd</xsl:otherwise>
							    </xsl:choose>
								</xsl:attribute>
									<td class="column-1">
										<span>
											<xsl:value-of select="column1Value"
												disable-output-escaping="yes" />
										</span>
									</td>
									<td class="data-column-wide">
										<span class="col-2-low">
											<xsl:value-of select="column2LowValue"
												disable-output-escaping="yes" />
										</span>
										<span>
											<xsl:value-of select="column2Value"
												disable-output-escaping="yes" />
										</span>
										<span class="col-2-high">
											<xsl:value-of select="column2HighValue"
												disable-output-escaping="yes" />
										</span>
									</td>
									<td class="data-column-narrow">
										<xsl:value-of select="column3Value"
											disable-output-escaping="yes" />
									</td>
									<td class="data-column-narrow">
										<xsl:value-of select="column4Value"
											disable-output-escaping="yes" />
									</td>
									<td class="data-column-narrow">
										<xsl:value-of select="column5Value"
											disable-output-escaping="yes" />
									</td>
									<td class="data-column-narrow">
										<xsl:value-of select="column6Value"
											disable-output-escaping="yes" />
									</td>
									<td class="data-column-narrow">
										<xsl:value-of select="column7Value"
											disable-output-escaping="yes" />
									</td>
									<td>
										<xsl:attribute name="class">data-column-wide conf-color-<xsl:value-of
											select="column8ScoreValue" /></xsl:attribute>
										<xsl:if test="column8isRetake = 'true'">
											**
										</xsl:if>
										<xsl:value-of select="column8ColorValue"
											disable-output-escaping="yes" />
										<xsl:if test="column8isRetake = 'true'">
											**
										</xsl:if>
									</td>
								</tr>
							</xsl:otherwise>
						</xsl:choose>



					</xsl:for-each>
					<!-- LAST TABLE -->
					<xsl:if test="position()=last()">
						<xsl:for-each select="rows/row">
							<!-- LAST ROW (Participants' AVERAGES) -->
							<xsl:if test="position()=last()">
								<tr class="tfooter">
									<td class="column-1 average-label">
										<xsl:value-of select="column1Value"
											disable-output-escaping="yes" />
									</td>
									<td class="data-column-wide">
										<xsl:choose>
											<xsl:when test="not($langCode = 'pl')">
												<xsl:value-of select="column2Value"
													disable-output-escaping="yes" />
											</xsl:when>
											<xsl:otherwise>
											<!-- European (PL) formatting ',' instead of '.'   there is a more robust method provided at the url below-->
											<!-- http://stackoverflow.com/questions/11716358/replacing-characters-in-xslt-1-0 -->
												<xsl:variable name="orgvalue" select="column2Value" />
												<xsl:value-of select="translate($orgvalue,'.',',')"
													disable-output-escaping="yes" />
											</xsl:otherwise>
										</xsl:choose>
									</td>
									<td class="data-column-narrow">
										<xsl:choose>
											<xsl:when test="not($langCode = 'pl')">
												<xsl:value-of select="column3Value"
													disable-output-escaping="yes" />
											</xsl:when>
											<xsl:otherwise>
												<xsl:variable name="orgvalue" select="column3Value" />
												<xsl:value-of select="translate($orgvalue,'.',',')"
													disable-output-escaping="yes" />
											</xsl:otherwise>
										</xsl:choose>
									</td>
									<td class="data-column-narrow">
										<xsl:choose>
											<xsl:when test="not($langCode = 'pl')">
												<xsl:value-of select="column4Value"
													disable-output-escaping="yes" />
											</xsl:when>
											<xsl:otherwise>
												<xsl:variable name="orgvalue" select="column4Value" />
												<xsl:value-of select="translate($orgvalue,'.',',')"
													disable-output-escaping="yes" />
											</xsl:otherwise>
										</xsl:choose>
									</td>
									<td class="data-column-narrow">
										<xsl:choose>
											<xsl:when test="not($langCode = 'pl')">
												<xsl:value-of select="column5Value"
													disable-output-escaping="yes" />
											</xsl:when>
											<xsl:otherwise>
												<xsl:variable name="orgvalue" select="column5Value" />
												<xsl:value-of select="translate($orgvalue,'.',',')"
													disable-output-escaping="yes" />
											</xsl:otherwise>
										</xsl:choose>
									</td>
									<td class="data-column-narrow">
										<xsl:choose>
											<xsl:when test="not($langCode = 'pl')">
												<xsl:value-of select="column6Value"
													disable-output-escaping="yes" />
											</xsl:when>
											<xsl:otherwise>
												<xsl:variable name="orgvalue" select="column6Value" />
												<xsl:value-of select="translate($orgvalue,'.',',')"
													disable-output-escaping="yes" />
											</xsl:otherwise>
										</xsl:choose>
									</td>
									<td class="data-column-narrow">
										<xsl:choose>
											<xsl:when test="not($langCode = 'pl')">
												<xsl:value-of select="column7Value"
													disable-output-escaping="yes" />
											</xsl:when>
											<xsl:otherwise>
												<xsl:variable name="orgvalue" select="column7Value" />
												<xsl:value-of select="translate($orgvalue,'.',',')"
													disable-output-escaping="yes" />
											</xsl:otherwise>
										</xsl:choose>
									</td>
									<td class="data-column-wide">
										&#160; </td>
								</tr>
							</xsl:if>
						</xsl:for-each>
					</xsl:if>
					<!-- END TABLE -->
					<!-- <xsl:text disable-output-escaping="yes">&lt;/table&gt;</xsl:text> -->
				</table>
				<xsl:if test="contains(retakeFootnote, '**')">
					<div class="table-footnote">
						<xsl:value-of select="retakeFootnote"></xsl:value-of>
					</div>
				</xsl:if>
			</xsl:for-each>


		</div>
	</xsl:template>
	<xsl:template match="groupSummarySection">
		<!-- <div class="group"> -->
		<div class="section">
			<h1>
				<xsl:value-of select="../p6Heading" />
			</h1>
			<!-- <hr /> -->
			<h3 class="keysLabel">
				<xsl:value-of select="keysLabel" />
			</h3>
			<div class="vScaleThird vScaleThirdLeft">
				<h3 class="vScaleLabel">
					<xsl:value-of select="scoreDescLabelLow" />
				</h3>
			</div>
			<div class="vScaleThird vScaleThirdMid">
				<div class="relative-div">
					&#160;
					<span class="key key-v-0">0</span>
					<span class="key key-v-20">20</span>
					<span class="key key-v-40">40</span>
					<span class="key key-v-60">60</span>
					<span class="key key-v-80">80</span>
					<span class="key key-v-100">100</span>
				</div>
			</div>
			<div class="vScaleThird vScaleThirdRight">
				<h3 class="vScaleLabel">
					<xsl:value-of select="scoreDescLabelHigh" />
				</h3>
			</div>
			<div class="group">
				<xsl:for-each select="pScales/pScale">

					<h2 class="chart-title">
						<xsl:value-of select="title" disable-output-escaping="yes" />
					</h2>
					<div class="vScaleThird vScaleThirdLeft">
						<xsl:attribute name="style"><xsl:if test="position() != 1">text-align:right; margin-top:22px;</xsl:if></xsl:attribute>
						<xsl:value-of select="lowScoreDesc"
							disable-output-escaping="yes" />
					</div>
					<div class="vScaleThird vScaleThirdMid">
						<span class="vScaleTick">&#160;</span>
						<span class="vScaleTick">&#160;</span>
						<span class="vScaleTick">&#160;</span>
						<span class="vScaleTick">&#160;</span>
						<span class="vScaleTick vScaleTickLast">&#160;</span>
						<span>
							<xsl:attribute name="class">score-bar score-bar-right p-adj-score-bar pScaleBar p-adj-summary-bar-special p-adj-summary-bar-2-<xsl:value-of
								select="position()" /> <xsl:if test="score &lt; 14"> score-offset-overview</xsl:if>
						</xsl:attribute>
							<xsl:attribute name="style">width:<xsl:value-of
								select="score" disable-output-escaping="yes" />%;</xsl:attribute>
							<span>
								<xsl:value-of select="score"
									disable-output-escaping="yes" />
							</span>
						</span>
					</div>
					<div class="vScaleThird vScaleThirdRight">
						<xsl:attribute name="style"><xsl:if test="position() != 1">margin-top:22px;</xsl:if></xsl:attribute>
						<xsl:value-of select="highScoreDesc"
							disable-output-escaping="yes" />
					</div>
					<xsl:if test="position() = 1">
						<div class="summary-footnote">
							<xsl:value-of select="../../summaryFootnote"
								disable-output-escaping="yes" />
						</div>
					</xsl:if>
					<xsl:if test="position() != 1">
						<div style="display:block; clear:both;">&#160;</div>
					</xsl:if>
				</xsl:for-each>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="agilityCharts">
		<xsl:for-each select="agilityChart">
			<div id="wrapper">
				<h1 class="chart-title">
					<xsl:value-of select="title" disable-output-escaping="yes" />
				</h1>
				<div class="chart">
					<h2 class="chart-title">
						<span class="no-wrap">
							<xsl:value-of select="grpMean"
								disable-output-escaping="yes" />
						</span>
						&#160; &#160; &#160; &#160; &#160;
						<span class="no-wrap">
							<xsl:value-of select="stdDev"
								disable-output-escaping="yes" />
						</span>
					</h2>
					<br />
					<h4 class="y-axis-title">
						<xsl:value-of select="yLabel" />
					</h4>
					<div id="figure">
						<div class="graph">
							<ul class="x-axis">
								<li class="key-offset">
									<span>0-9</span>
								</li>
								<li>
									<span>10-19</span>
								</li>
								<li class="key-offset">
									<span>20-29</span>
								</li>
								<li>
									<span>30-39</span>
								</li>
								<li class="key-offset">
									<span>40-49</span>
								</li>
								<li>
									<span>50-59</span>
								</li>
								<li class="key-offset">
									<span>60-69</span>
								</li>
								<li>
									<span>70-79</span>
								</li>
								<li class="key-offset">
									<span>80-89</span>
								</li>
								<li>
									<span>90-100</span>
								</li>
							</ul>
							<h4 class="x-axis-title">
								<xsl:value-of select="xLabel" />
							</h4>
							<ul class="y-axis">
								<li>
									<span>100</span>
								</li>
								<li>
									<span>80</span>
								</li>
								<li>
									<span>60</span>
								</li>
								<li>
									<span>40</span>
								</li>
								<li>
									<span>20</span>
								</li>
								<li>
									<span>0</span>
								</li>
							</ul>
							<div class="bars">
								<div class="bar-group">
									<div>
										<xsl:attribute name="style">height:<xsl:value-of
											select="percentile10" />%</xsl:attribute>
										<xsl:attribute name="class">bar fig0 p-adj-summary-bar-3-<xsl:value-of
											select="position()" /> <xsl:if test="score &gt; 95">score-offset</xsl:if>
										</xsl:attribute>
										<span>
											<xsl:if test="percentile10 != 0">
												<xsl:value-of select="percentile10" />
												%
											</xsl:if>
											<xsl:if test="percentile10 = 0">
												&#160;
											</xsl:if>
										</span>
									</div>
								</div>
								<div class="bar-group">
									<div>
										<xsl:attribute name="style">height:<xsl:value-of
											select="percentile20" />%</xsl:attribute>
										<xsl:attribute name="class">bar fig0 p-adj-summary-bar-3-<xsl:value-of
											select="position()" /> <xsl:if test="score &gt; 95">score-offset</xsl:if>
										</xsl:attribute>
										<span>
											<xsl:if test="percentile20 != 0">
												<xsl:value-of select="percentile20" />
												%
											</xsl:if>
											<xsl:if test="percentile20 = 0">
												&#160;
											</xsl:if>
										</span>
									</div>
								</div>
								<div class="bar-group">
									<div>
										<xsl:attribute name="style">height:
										<xsl:value-of select="percentile30" />%
											</xsl:attribute>
										<xsl:attribute name="class">bar fig0 p-adj-summary-bar-3-<xsl:value-of
											select="position()" /> 
											<xsl:if test="score &gt; 95">
											  score-offset
											</xsl:if>
										</xsl:attribute>
										<span>
											<xsl:if test="percentile30 != 0">
												<xsl:value-of select="percentile30" />
												%
											</xsl:if>
											<xsl:if test="percentile30 = 0">
												&#160;
											</xsl:if>
										</span>
									</div>
								</div>
								<div class="bar-group">
									<div>
										<xsl:attribute name="style">height:<xsl:value-of
											select="percentile40" />%</xsl:attribute>
										<xsl:attribute name="class">bar fig0 p-adj-summary-bar-3-<xsl:value-of
											select="position()" /> <xsl:if test="score &gt; 95">score-offset</xsl:if>
										</xsl:attribute>
										<span>
											<xsl:if test="percentile40 != 0">
												<xsl:value-of select="percentile40" />
												%
											</xsl:if>
											<xsl:if test="percentile40 = 0">
												&#160;
											</xsl:if>
										</span>
									</div>
								</div>
								<div class="bar-group">
									<div>
										<xsl:attribute name="style">height:<xsl:value-of
											select="percentile50" />%</xsl:attribute>
										<xsl:attribute name="class">bar fig0 p-adj-summary-bar-3-<xsl:value-of
											select="position()" /> <xsl:if test="score &gt; 95">score-offset</xsl:if>
										</xsl:attribute>
										<span>
											<xsl:if test="percentile50 != 0">
												<xsl:value-of select="percentile50" />
												%
											</xsl:if>
											<xsl:if test="percentile50 = 0">
												&#160;
											</xsl:if>
										</span>
									</div>
								</div>
								<div class="bar-group">
									<div>
										<xsl:attribute name="style">height:<xsl:value-of
											select="percentile60" />%</xsl:attribute>
										<xsl:attribute name="class">bar fig0 p-adj-summary-bar-3-<xsl:value-of
											select="position()" /> <xsl:if test="score &gt; 95">score-offset</xsl:if>
										</xsl:attribute>
										<span>
											<xsl:if test="percentile60 != 0">
												<xsl:value-of select="percentile60" />
												%
											</xsl:if>
											<xsl:if test="percentile60 = 0">
												&#160;
											</xsl:if>
										</span>
									</div>
								</div>
								<div class="bar-group">
									<div>
										<xsl:attribute name="style">height:<xsl:value-of
											select="percentile70" />%</xsl:attribute>
										<xsl:attribute name="class">bar fig0 p-adj-summary-bar-3-<xsl:value-of
											select="position()" /> <xsl:if test="score &gt; 95">score-offset</xsl:if>
										</xsl:attribute>
										<span>
											<xsl:if test="percentile70 != 0">
												<xsl:value-of select="percentile70" />
												%
											</xsl:if>
											<xsl:if test="percentile70 = 0">
												&#160;
											</xsl:if>
										</span>
									</div>
								</div>
								<div class="bar-group">
									<div>
										<xsl:attribute name="style">height:<xsl:value-of
											select="percentile80" />%</xsl:attribute>
										<xsl:attribute name="class">bar fig0 p-adj-summary-bar-3-<xsl:value-of
											select="position()" /> <xsl:if test="score &gt; 95">score-offset</xsl:if>
										</xsl:attribute>
										<span>
											<xsl:if test="percentile80 != 0">
												<xsl:value-of select="percentile80" />
												%
											</xsl:if>
											<xsl:if test="percentile80 = 0">
												&#160;
											</xsl:if>
										</span>
									</div>
								</div>
								<div class="bar-group">
									<div>
										<xsl:attribute name="style">height:<xsl:value-of
											select="percentile90" />%</xsl:attribute>
										<xsl:attribute name="class">bar fig0 p-adj-summary-bar-3-<xsl:value-of
											select="position()" /> <xsl:if test="score &gt; 95">score-offset</xsl:if>
										</xsl:attribute>
										<span>
											<xsl:if test="percentile90 != 0">
												<xsl:value-of select="percentile90" />
												%
											</xsl:if>
											<xsl:if test="percentile90 = 0">
												&#160;
											</xsl:if>
										</span>
									</div>
								</div>
								<div class="bar-group">
									<div>
										<xsl:attribute name="style">height:<xsl:value-of
											select="percentile100" />%</xsl:attribute>
										<xsl:attribute name="class">bar fig0 p-adj-summary-bar-3-<xsl:value-of
											select="position()" /> <xsl:if test="score &gt; 95">score-offset</xsl:if>
										</xsl:attribute>
										<span>
											<xsl:if test="percentile100 != 0">
												<xsl:value-of select="percentile100" />
												%
											</xsl:if>
											<xsl:if test="percentile100 = 0">
												&#160;
											</xsl:if>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div style="display:block; clear:both; position:relative;">
				<div class="scale-list-quad">
					<h3>
						<xsl:value-of select="BTLowLabel" />
					</h3>
					<xsl:value-of select="BTLowList"
						disable-output-escaping="yes" />
				</div>

				<div class="scale-list-quad">
					<h3>
						<xsl:value-of select="BTHighLabel" />
					</h3>
					<xsl:value-of select="BTHighList"
						disable-output-escaping="yes" />
				</div>
				<div style="display:block; clear:both;">&#160;</div>
				<div class="scale-list-quad">
					<h3>
						<xsl:value-of select="PIOLowLabel" />
					</h3>
					<xsl:value-of select="PIOLowList"
						disable-output-escaping="yes" />
				</div>
				<div class="scale-list-quad">
					<h3>
						<xsl:value-of select="PIOHighLabel" />
					</h3>
					<xsl:value-of select="PIOHighList"
						disable-output-escaping="yes" />
				</div>
				<div style="display:block; clear:both;">&#160;</div>
			</div>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>