<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xhtml [ 
<!ENTITY nbsp "&#160;"> 
]>
<xsl:stylesheet version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xslt"
	xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xsl xalan xs">
	<!-- http://stackoverflow.com/questions/9538619/xslt-refuses-to-write-doctype-declaration -->
	<xsl:output method="xml" indent="yes" encoding="UTF-8"
		doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />
	<xsl:param name="baseurl" select="'http://localhost'" />
	<!-- these two not used? -->
	<xsl:param name="genmode" select="'pdf'" />
	<!-- no default match -->
	<xsl:param name="subtype" select="'subtype'" />
	<!-- no default match -->
	<xsl:template match="*" />
	<xsl:template match="/viaEdgeCoachingReport">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>
					<xsl:value-of select="reportTitle"></xsl:value-of>
				</title>
				<!-- <link href="{$baseurl}/core/css/font_{$reportLang}.css" rel="stylesheet" 
					type="text/css" /> -->
				<link href="{$baseurl}/core/css/reportML.css" rel="stylesheet"
					type="text/css" />
				<link href="{$baseurl}/projects/viaEdge/coaching/css/report.css"
					rel="stylesheet" type="text/css" />
				<meta name="baseurl" content="{$baseurl}" />
				<meta name="genmode" content="{$genmode}" />
				<meta name="subtype" content="{$subtype}" />
			</head>
			<body>
			<xsl:choose>
					<xsl:when test="@lang = 'ko'">
						<xsl:attribute name="class">ko</xsl:attribute>
					</xsl:when>
					<xsl:when test="@lang = 'zh_CN'">
						<xsl:attribute name="class">zh</xsl:attribute>
					</xsl:when>
					<xsl:when test="@lang = 'ja'">
						<xsl:attribute name="class">ja</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="class"></xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<div class="container">
					<xsl:apply-templates select="introductionSection" />
					<xsl:if test="$subtype != 'hybridReport'">
						<xsl:apply-templates select="overviewSection" />
					</xsl:if>
					<xsl:apply-templates select="agilityScoresSection" />

				</div>
				<!-- REPORT OVERVIEW -->
				<div class="section group">
				</div>
				<!-- Scales -->
				<div class="section group">
				</div>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="introductionSection">
		<div class="section intro">
			<xsl:if test="$subtype != 'hybridReport'">
			<h1>
				<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
			</h1>
			<br />
			<p>
				<span>
					<xsl:value-of select="p1" disable-output-escaping="yes"></xsl:value-of>
				</span>
			</p>
			<br />
			<p>
				<span>
					<xsl:value-of select="p2" disable-output-escaping="yes"></xsl:value-of>
				</span>
			</p>
			<br />
			<p>
				<span>
					<xsl:value-of select="p3" disable-output-escaping="yes"></xsl:value-of>
				</span>
			</p>
			<br />
			<div>
				<div style="clear:both; padding:10px; "></div>
				<div style="float:left; clear:left; width:20%; padding-left:30px;">
					<span style="font-weight:700;">
						<xsl:value-of select="mentalDescriptionHeading"></xsl:value-of>
					</span>
				</div>
				<div style="float:left; clear:right; width:75%; padding-left:15px;">
					<span>
						<xsl:value-of select="mentalDescription"></xsl:value-of>
					</span>
				</div>
				<div style="clear:both; padding:5px; "></div>
			</div>
			<div>
				<div
					style="float:left; clear:left; width:20%; padding-left:30px; font-weight:700;">
					<span style="font-weight:700;">
						<xsl:value-of select="peopleDescriptionHeading"></xsl:value-of>
					</span>
				</div>
				<div style="float:left; clear:right; width:75%; padding-left:15px;">
					<span>
						<xsl:value-of select="peopleDescription"></xsl:value-of>
					</span>
				</div>
				<div style="clear:both; padding:5px; "></div>
			</div>
			<div>
				<div
					style="float:left; clear:left; width:20%; padding-left:30px; font-weight:700;">
					<span style="font-weight:700;">
						<xsl:value-of select="changeDescriptionHeading"></xsl:value-of>
					</span>
				</div>
				<div style="float:left; clear:right; width:75%; padding-left:15px;">
					<span>
						<xsl:value-of select="changeDescription"></xsl:value-of>
					</span>
				</div>
				<div style="clear:both; padding:5px; "></div>
			</div>
			<div>
				<div
					style="float:left; clear:left; width:20%; padding-left:30px; font-weight:700;">
					<span style="font-weight:700;">
						<xsl:value-of select="resultsDescriptionHeading"></xsl:value-of>
					</span>
				</div>
				<div style="float:left; clear:right; width:75%; padding-left:15px;">
					<span>
						<xsl:value-of select="resultsDescription"></xsl:value-of>
					</span>
				</div>
				<div style="clear:both; padding:5px; "></div>
			</div>
			<div>
				<div
					style="float:left; clear:left; width:20%; padding-left:30px; font-weight:700;">
					<span style="font-weight:700;">
						<xsl:value-of select="selfAwareDescriptionHeading"></xsl:value-of>
					</span>
				</div>
				<div style="float:left; clear:right; width:75%; padding-left:15px;">
					<span>
						<xsl:value-of select="selfAwareDescription"></xsl:value-of>
					</span>
				</div>
				<div style="clear:both;"></div>
			</div>
			</xsl:if>
		</div>
		<br />
	</xsl:template>
	<xsl:template match="overviewSection">
		<div class="group">
			<h1>
				<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
			</h1>
			<br/>
			<h3>
				<xsl:value-of select="part1Heading"
					disable-output-escaping="yes"></xsl:value-of>
			</h3>
			<ul>
				<li>
					<xsl:value-of select="part1B1"
						disable-output-escaping="yes"></xsl:value-of>
				</li>
				<li>
					<xsl:value-of select="part1B2"
						disable-output-escaping="yes"></xsl:value-of>
				</li>
				<li>
					<xsl:value-of select="part1B3"
						disable-output-escaping="yes"></xsl:value-of>
				</li>
				<li>
					<xsl:value-of select="part1B4"
						disable-output-escaping="yes"></xsl:value-of>
					<ul>
						<li>
							<xsl:value-of select="part1B4S1"
								disable-output-escaping="yes"></xsl:value-of>
						</li>
						<li>
							<xsl:value-of select="part1B4S2"
								disable-output-escaping="yes"></xsl:value-of>
						</li>
						<li>
							<xsl:value-of select="part1B4S3"
								disable-output-escaping="yes"></xsl:value-of>
						</li>
						<li>
							<xsl:value-of select="part1B4S4"
								disable-output-escaping="yes"></xsl:value-of>
						</li>
						<li>
							<xsl:value-of select="part1B4S5"
								disable-output-escaping="yes"></xsl:value-of>
						</li>
					</ul>
				</li>
			</ul>
			<br /><br /><br /><br /><br /><br /><br /><br />		
		</div>
		<br/>
	</xsl:template>
	<xsl:template match="agilityScoresSection">
		<xsl:if test="$subtype != 'hybridReport'">
		<div class="group">
			<h1>
				<xsl:value-of select="title" disable-output-escaping="yes"></xsl:value-of>
			</h1>
			<div class="group">
				<h2>
					<xsl:value-of select="heading"
						disable-output-escaping="yes"></xsl:value-of>
				</h2>
				<br />

				<p>
					<xsl:value-of select="p1" disable-output-escaping="yes"></xsl:value-of>
				</p>
				<br />

				<p>
					<xsl:value-of select="p2" disable-output-escaping="yes"></xsl:value-of>
				</p>
				<br />

				<p>
					<xsl:value-of select="p3" disable-output-escaping="yes"></xsl:value-of>
				</p>
			</div>
			<br /><br /><br /><br />
		</div>
		<br />
		</xsl:if>
		<xsl:apply-templates select="scales" mode="summary"></xsl:apply-templates>
		<xsl:apply-templates select="scales" mode="detailed"></xsl:apply-templates>
	</xsl:template>
	<xsl:template match="scales" mode="detailed">
		<xsl:for-each select="scale">
			<!-- ALL DETAILED SCALES -->
			<xsl:if test="position() != 1 and $subtype != 'hybridReport'">
				<div class="group detailed-scale-container">
					<h1><xsl:value-of select="title" disable-output-escaping="yes"/></h1>
					<h2 class="center-header">
						<xsl:value-of select="heading"
							disable-output-escaping="yes"></xsl:value-of>
					</h2>
					<div class="key-container">
						<table>
							<tr>
								<td class="scale-two-label-low">
									<span style="float:right;">
										<div class="summary-side-label">
											<xsl:value-of select="labelLow" disable-output-escaping="yes"></xsl:value-of>
										</div>
									</span>
								</td>
								<td class="scale-two-keys">
									<div class="relative-div">
										<span class="key key-a-50-left">50</span>
										<span class="key key-a-40-left">40</span>
										<span class="key key-a-17-left">17</span>
										<span class="key key-a-0">0</span>
										<span class="key key-a-17-right">17</span>
										<span class="key key-a-40-right">40</span>
										<span class="key key-a-50-right">50</span>
									</div>
								</td>
								<td class="scale-two-label-high">
									<span class="summary-side-label">
										<xsl:value-of select="labelHigh" disable-output-escaping="yes"></xsl:value-of>
									</span>
								</td>
							</tr>
						</table>
					</div>
					<div>
						<div class="scale-one-container">
							<div class="scale-left-side">
								&#160;
								<!-- <xsl:if test="pScoreSide = 'low'"> -->
								<!-- <div class="score-bar score-bar-left p-score-bar"> -->
								<!-- <xsl:attribute name="style">width:<xsl:value-of select="pScore 
									* 100 div 50"></xsl:value-of>%</xsl:attribute> -->
								<!-- <span> -->
								<!-- <xsl:value-of select="pScore"></xsl:value-of> -->
								<!-- </span> -->
								<!-- </div> -->
								<!-- </xsl:if> -->
								<xsl:if test="adjpScoreSide = 'low'">
									<div>
										<xsl:attribute name="style">width:<xsl:value-of
											select="adjpScore * 100 div 50"></xsl:value-of>%</xsl:attribute>
										<xsl:attribute name="class">score-bar score-bar-left p-adj-score-bar p-adj-score-bar-feedback p-adj-summary-bar-special p-adj-summary-bar-2-<xsl:value-of
											select="position()"></xsl:value-of><xsl:if test="adjpScore &lt; 5"> score-offset-low</xsl:if></xsl:attribute>
										<span>
											<xsl:value-of select="adjpScore"></xsl:value-of>
										</span>
									</div>
								</xsl:if>
							</div>
							<div class="scale-right-side">
								&#160;
								<!-- <xsl:if test="pScoreSide = 'high'"> -->
								<!-- <div class="score-bar score-bar-right p-score-bar"> -->
								<!-- <xsl:attribute name="style">width:<xsl:value-of select="pScore 
									* 100 div 50"></xsl:value-of>%</xsl:attribute> -->
								<!-- <span> -->
								<!-- <xsl:value-of select="pScore"></xsl:value-of> -->
								<!-- </span> -->
								<!-- </div> -->
								<!-- </xsl:if> -->
								<xsl:if test="adjpScoreSide = 'high'">
									<div>
										<xsl:attribute name="style">width:<xsl:value-of
											select="adjpScore * 100 div 50"></xsl:value-of>%</xsl:attribute>
										<xsl:attribute name="class">score-bar score-bar-right p-adj-score-bar p-adj-score-bar-feedback p-adj-summary-bar-special p-adj-summary-bar-2-<xsl:value-of
											select="position()"></xsl:value-of><xsl:if test="adjpScore &lt; 5"> score-offset</xsl:if></xsl:attribute>
										<span>
											<xsl:value-of select="adjpScore"></xsl:value-of>
										</span>
									</div>
								</xsl:if>
							</div>
						</div>
					</div>
					<div class="key-container">
						<table>
							<tr>
								<td class="po-left-td">
									<xsl:value-of select="keyPotentialOveruse"></xsl:value-of>
								</td>
								<td class="s-left-td">
									<xsl:value-of select="keyStrong"></xsl:value-of>
								</td>
								<td class="b-td">
									<xsl:value-of select="keyBalanced"></xsl:value-of>
								</td>
								<td class="s-right-td">
									<xsl:value-of select="keyStrong"></xsl:value-of>
								</td>
								<td class="po-right-td">
									<xsl:value-of select="keyPotentialOveruse"></xsl:value-of>
								</td>
							</tr>
						</table>
					</div>

					<div class="scale-list-left">
						<div class="scale-list-quad">
							<h3>
								<xsl:value-of select="PIOLabel"></xsl:value-of>
							</h3>
							<xsl:value-of select="PIOLowList"
								disable-output-escaping="yes"></xsl:value-of>
						</div>
						<div class="scale-list-quad">
							<h3>
								<xsl:value-of select="BTLabel"></xsl:value-of>
							</h3>
							<xsl:value-of select="BTLowList"
								disable-output-escaping="yes"></xsl:value-of>
						</div>
					</div>
					<div class="scale-list-right">
						<div class="scale-list-quad">
							<h3>
								<xsl:value-of select="BTLabel"></xsl:value-of>
							</h3>
							<xsl:value-of select="BTHighList"
								disable-output-escaping="yes"></xsl:value-of>
						</div>
						<div class="scale-list-quad">
							<h3>
								<xsl:value-of select="PIOLabel"></xsl:value-of>
							</h3>
							<xsl:value-of select="PIOHighList"
								disable-output-escaping="yes"></xsl:value-of>
						</div>
					</div>
					<div style="display:block; clear:both;">&#160;</div>
				</div>
				<!-- </div> -->
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="scales" mode="summary">
		<xsl:for-each select="scale">
			<xsl:if test="position() = 1">
				<!-- OVERALL SCALE (ONLY FIRST IN LIST) -->
				<div class="group landscape-page-height">
				<h1><xsl:value-of select="title"
							disable-output-escaping="yes"/></h1>
					<h2 class="center-header">
						<xsl:value-of select="heading"
							disable-output-escaping="yes"></xsl:value-of>
					</h2>
					<div class="key-container">
						<table>
							<tr>
								<td class="scale-two-label">
									&#160; </td>
								<td class="scale-two-keys">
									<div class="relative-div">
										<span class="key key-a-50-left">50</span>
										<span class="key key-a-40-left">40</span>
										<span class="key key-a-17-left">17</span>
										<span class="key key-a-0">0</span>
										<span class="key key-a-17-right">17</span>
										<span class="key key-a-40-right">40</span>
										<span class="key key-a-50-right">50</span>
									</div>
								</td>
								<td>&#160;</td>
							</tr>
						</table>
					</div>
					<div>
						<div class="scale-one-container">
							<div class="scale-left-side">
								&#160;
								<xsl:if test="adjpScoreSide = 'low'">
									<div>
										<xsl:attribute name="style">width:<xsl:value-of
											select="adjpScore * 100 div 50"></xsl:value-of>%</xsl:attribute>
										<xsl:attribute name="class">score-bar score-bar-left p-adj-score-bar p-adj-score-bar-feedback<xsl:if test="adjpScore &lt; 5"> score-offset-low</xsl:if></xsl:attribute>	
										<span>
											<xsl:value-of select="adjpScore"></xsl:value-of>
										</span> 
									</div>
								</xsl:if>
							</div>
							<div class="scale-right-side">
								&#160;
								<xsl:if test="adjpScoreSide = 'high'">
									<div>
										<xsl:attribute name="style">width:<xsl:value-of
											select="adjpScore * 100 div 50"></xsl:value-of>%</xsl:attribute>
										<xsl:attribute name="class">score-bar score-bar-right p-adj-score-bar p-adj-score-bar-feedback<xsl:if test="adjpScore &lt; 5"> score-offset</xsl:if></xsl:attribute>
										<span>
											<xsl:value-of select="adjpScore"></xsl:value-of>
										</span>
									</div>
								</xsl:if>
							</div>
						</div>
					</div>
					<div class="key-container">
						<table>
							<tr>
								<td class="po-left-td">
									<xsl:value-of select="keyPotentialOveruse"></xsl:value-of>
								</td>
								<td class="s-left-td">
									<xsl:value-of select="keyStrong"></xsl:value-of>
								</td>
								<td class="b-td">
									<xsl:value-of select="keyBalanced"></xsl:value-of>
								</td>
								<td class="s-right-td">
									<xsl:value-of select="keyStrong"></xsl:value-of>
								</td>
								<td class="po-right-td">
									<xsl:value-of select="keyPotentialOveruse"></xsl:value-of>
								</td>
							</tr>
						</table>
					</div>

					<div class="score-desc score-desc-low">
						<h2>
							<xsl:value-of select="labelLow"></xsl:value-of>
						</h2>
						<p>
							<xsl:value-of select="descLow"></xsl:value-of>
						</p>
					</div>
					<div class="score-desc score-desc-high">
						<h2>
							<xsl:value-of select="labelHigh" disable-output-escaping="yes"></xsl:value-of>
						</h2>
						<p>
							<xsl:value-of select="descHigh"></xsl:value-of>
						</p>
					</div>
					<div style="display:block; clear:both;">&#160;</div>
					<h2 class="center-header">
						<xsl:value-of select="scoreNarrativeHeading"></xsl:value-of>
					</h2>
					<p>
						<xsl:value-of select="scoreNarrative"></xsl:value-of>
					</p>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
				</div>
			</xsl:if>
			</xsl:for-each>
			<div class="group">
			<h1><xsl:value-of select="/viaEdgeCoachingReport/profileHeading" disable-output-escaping="yes"/></h1>
			<xsl:for-each select="scale">
			<xsl:if test="position() != 1">
				<!-- ALL SUMMARY SCALES -->
				<div class="group">
<!-- 					<xsl:if test="position() = 2"> -->
<!-- 						<h1><xsl:value-of select="title" -->
<!-- 							disable-output-escaping="yes"/></h1> -->
<!-- 					</xsl:if> -->
					<h2 class="center-header">
						<xsl:value-of select="heading2"
							disable-output-escaping="yes"></xsl:value-of>
					</h2>
					<div class="key-container">
						<table>
							<tr>
								<td class="scale-two-label-low">
									<span style="float:right;">
										<div class="summary-side-label">
											<xsl:value-of select="labelLow" disable-output-escaping="yes"></xsl:value-of>
										</div>
									</span>
								</td>
								<td class="scale-two-keys">
									<div class="relative-div">
										<span class="key key-a-50-left">50</span>
										<span class="key key-a-40-left">40</span>
										<span class="key key-a-17-left">17</span>
										<span class="key key-a-0">0</span>
										<span class="key key-a-17-right">17</span>
										<span class="key key-a-40-right">40</span>
										<span class="key key-a-50-right">50</span>
									</div>
								</td>
								<td class="scale-two-label-high">
									<span class="summary-side-label">
										<xsl:value-of select="labelHigh" disable-output-escaping="yes"></xsl:value-of>
									</span>
								</td>
							</tr>
						</table>
					</div>
					<div>
						<div class="scale-one-container scale-one-summary">
							<div class="scale-left-side scale-side-summary">
								<xsl:if test="adjpScoreSide = 'low'">
										<div>
											<xsl:attribute name="style">width:<xsl:value-of
												select="adjpScore * 100 div 50"></xsl:value-of>%</xsl:attribute>
											<xsl:attribute name="class">score-bar score-bar-left summary-bar p-adj-summary-bar-<xsl:value-of
												select="position()"></xsl:value-of><xsl:if test="adjpScore &lt; 5"> score-offset-low</xsl:if></xsl:attribute>
											<span>
												<xsl:value-of select="adjpScore"></xsl:value-of>
											</span>
										</div>
									</xsl:if>
								<xsl:if test="adjpScoreSide = 'high'">
									&#160;
								</xsl:if>
							</div>
							<div class="scale-right-side scale-side-summary">
								<xsl:if test="adjpScoreSide = 'high'">
										<div>
											<xsl:attribute name="style">width:<xsl:value-of
												select="adjpScore * 100 div 50"></xsl:value-of>%</xsl:attribute>
											<xsl:attribute name="class">score-bar score-bar-right summary-bar p-adj-summary-bar-<xsl:value-of
												select="position()"></xsl:value-of><xsl:if test="adjpScore &lt; 5"> score-offset</xsl:if></xsl:attribute>
											<span>
												<xsl:value-of select="adjpScore"></xsl:value-of>
											</span>
										</div>
									</xsl:if>
								<xsl:if test="adjpScoreSide = 'low'">
									&#160;
								</xsl:if>
							</div>
						</div>
					</div>
					<xsl:if test="position() = 6">
						<div class="key-container group">
							<table>
								<tr>
									<td class="po-left-td">
										<xsl:value-of select="keyPotentialOveruse"></xsl:value-of>
									</td>
									<td class="s-left-td">
										<xsl:value-of select="keyStrong"></xsl:value-of>
									</td>
									<td class="b-td">
										<xsl:value-of select="keyBalanced"></xsl:value-of>
									</td>
									<td class="s-right-td">
										<xsl:value-of select="keyStrong"></xsl:value-of>
									</td>
									<td class="po-right-td">
										<xsl:value-of select="keyPotentialOveruse"></xsl:value-of>
									</td>
								</tr>
							</table>
						</div>
					</xsl:if>
				</div>
			</xsl:if>
		</xsl:for-each>
		</div>
		<br/>
	</xsl:template>
</xsl:stylesheet>