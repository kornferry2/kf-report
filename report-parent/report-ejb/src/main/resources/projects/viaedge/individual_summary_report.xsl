<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xhtml [ 
<!ENTITY nbsp "&#160;"> 
<!ENTITY oplus "&#8853;">
]>
<xsl:stylesheet version="2.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xalan="http://xml.apache.org/xslt" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xsl xalan xs">
	<!-- http://stackoverflow.com/questions/9538619/xslt-refuses-to-write-doctype-declaration -->
	<xsl:output method="xml" indent="yes" encoding="UTF-8" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />
	<xsl:param name="baseurl" select="'http://localhost'" />
	<!-- these two not used? -->
	<xsl:param name="genmode" select="'pdf'" />
	<!-- no default match -->
	<xsl:template match="*" />
	<xsl:template match="/viaEdgeIndividualSummaryReport">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>
					<span>
						<xsl:value-of select="reportTitle"></xsl:value-of>
					</span>
				</title>
				<!-- <link href="{$baseurl}/core/css/font_{$reportLang}.css" rel="stylesheet" type="text/css" /> -->
				<link href="{$baseurl}/core/css/reportML.css" rel="stylesheet" type="text/css" />
				<link href="{$baseurl}/projects/viaEdge/individualSummary/css/report.css" rel="stylesheet"
					type="text/css" />
					<meta name="baseurl" content="{$baseurl}" />
					<meta name="genmode" content="{$genmode}" />
			</head>
			<body>
			<xsl:choose>
					<xsl:when test="@lang = 'ko'">
						<xsl:attribute name="class">ko</xsl:attribute>
					</xsl:when>
					<xsl:when test="@lang = 'zh'">
						<xsl:attribute name="class">zh</xsl:attribute>
					</xsl:when>
					<xsl:when test="@lang = 'ja'">
						<xsl:attribute name="class">ja</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="class"></xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<div class="container">
					<div class="section intro">
						<h1>
							<xsl:value-of select="introductionSection/title" disable-output-escaping="yes"></xsl:value-of>
						</h1>
						<br />
						<p>
							<span>
								<xsl:value-of select="introductionSection/p1" disable-output-escaping="yes"></xsl:value-of>
							</span>
						</p>
						<br />
						<p>
							<span>
								<xsl:value-of select="introductionSection/p2" disable-output-escaping="yes"></xsl:value-of>
							</span>
						</p>
						<br />
						<p>
							<span>
								<xsl:value-of select="introductionSection/p3" disable-output-escaping="yes"></xsl:value-of>
							</span>
						</p>
						<br />
						<div>
							<div style="clear:both; padding:10px; "></div>
							<div style="float:left; clear:left; width:27%;">
								<span style="font-weight:700;">
									<xsl:value-of select="introductionSection/mentalDescriptionHeading"></xsl:value-of>
								</span>
							</div>
							<div style="float:left; clear:right; width:70%; padding-left:15px;">
								<span>
									<xsl:value-of select="introductionSection/mentalDescription"></xsl:value-of>
								</span>
							</div>
							<div style="clear:both; padding:10px; "></div>
						</div>
						<div>
							<div style="float:left; clear:left; width:27%; font-weight:700;">
								<span style="font-weight:700;">
									<xsl:value-of select="introductionSection/peopleDescriptionHeading"></xsl:value-of>
								</span>
							</div>
							<div style="float:left; clear:right; width:70%; padding-left:15px;">
								<span>
									<xsl:value-of select="introductionSection/peopleDescription"></xsl:value-of>
								</span>
							</div>
							<div style="clear:both; padding:10px; "></div>
						</div>
						<div>
							<div style="float:left; clear:left; width:27%; font-weight:700;">
								<span style="font-weight:700;">
									<xsl:value-of select="introductionSection/changeDescriptionHeading"></xsl:value-of>
								</span>
							</div>
							<div style="float:left; clear:right; width:70%; padding-left:15px;">
								<span>
									<xsl:value-of select="introductionSection/changeDescription"></xsl:value-of>
								</span>
							</div>
							<div style="clear:both; padding:10px; "></div>
						</div>
						<div>
							<div style="float:left; clear:left; width:27%; font-weight:700;">
								<span style="font-weight:700;">
									<xsl:value-of select="introductionSection/resultsDescriptionHeading"></xsl:value-of>
								</span>
							</div>
							<div style="float:left; clear:right; width:70%; padding-left:15px;">
								<span>
									<xsl:value-of select="introductionSection/resultsDescription"></xsl:value-of>
								</span>
							</div>
							<div style="clear:both; padding:10px; "></div>
						</div>
						<div>
							<div style="float:left; clear:left; width:27%; font-weight:700;">
								<span style="font-weight:700;">
									<xsl:value-of select="introductionSection/selfAwareDescriptionHeading"></xsl:value-of>
								</span>
							</div>
							<div style="float:left; clear:right; width:70%; padding-left:15px;">
								<span>
									<xsl:value-of select="introductionSection/selfAwareDescription"></xsl:value-of>
								</span>
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
					<br/>
				</div>
				<div class="section" style="display:block; height:1px;">&#160;</div>
				<div>
					<h1>
						<xsl:value-of select="resultsSummarySection/title" disable-output-escaping="yes"></xsl:value-of>
					</h1>
					<br />
					<xsl:for-each select="resultsSummarySection/subSection">
						<div class="container">
							<div
								style="text-align:center; float:left; clear:both; width:95%;">
								<span class="trait-heading">
									<xsl:value-of select="heading"></xsl:value-of>
								</span>
							</div>
							<div class="trait-low-div">
								<p class="trait-span">
									<xsl:value-of select="traitLow" disable-output-escaping="yes"></xsl:value-of>
								</p>
							</div>
							<div class="trait-chart-div">
								<table>
									<tr>
										<td>
											<xsl:attribute name="class">scale-td trait-grid-cell trait-grid-<xsl:value-of
												select="@id" /> 
												
												</xsl:attribute>
											<xsl:if test="score = -1.0"> 
													<div>
														<xsl:attribute name="class">crosshairs-<xsl:value-of select="@id" /></xsl:attribute>
													</div>
												</xsl:if>											
										</td>
										<td>
											<xsl:attribute name="class">scale-td trait-grid-cell trait-grid-<xsl:value-of
												select="@id" /> 
											</xsl:attribute>
											<xsl:if test="score = 0.0">
											<div>
												<xsl:attribute name="class">crosshairs-<xsl:value-of select="@id" /></xsl:attribute>
											</div>
											</xsl:if>
										</td>
										<td>
											<xsl:attribute name="class">scale-td trait-grid-cell-last trait-grid-cell trait-grid-<xsl:value-of
												select="@id" />
												</xsl:attribute>
											<xsl:if test="score = 1.0">
											<div>
												<xsl:attribute name="class">crosshairs-<xsl:value-of select="@id" /></xsl:attribute>
											</div>
											</xsl:if>
										</td>
									</tr>
								</table>
							</div>
							<div class="trait-high-div">
								<p class="trait-span-right">
										<xsl:value-of select="traitHigh" disable-output-escaping="yes"></xsl:value-of>
								</p>
							</div>
							<div class="trait-nar-div">
								<span>
									<xsl:value-of select="scoreNarrative"></xsl:value-of>
								</span>
							</div>
							<hr class="sub-section-hr" />
						</div>
					</xsl:for-each>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>