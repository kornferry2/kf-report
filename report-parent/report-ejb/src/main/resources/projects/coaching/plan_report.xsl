<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xslt"
	xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xsl xalan xs">

	<!-- http://stackoverflow.com/questions/9538619/xslt-refuses-to-write-doctype-declaration -->
	<xsl:output method="xml" indent="yes" encoding="UTF-8"
		doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />

	<xsl:param name="baseurl" select="'http://localhost'" />

	<!-- these two not used? -->
	<xsl:param name="genmode" select="'pdf'" />
	<xsl:param name="subtype" select="'readiness'" />

	<xsl:param name="includeOverview" select="'true'" />
	<xsl:param name="includeGoals" select="'true'" />
	<xsl:param name="includeJournal" select="'true'" />

	<!-- no default match -->
	<xsl:template match="*" />

	<xsl:template match="/coachingPlanReport">

		<xsl:variable name="participantFullName"
			select="concat(participant/firstName,' ',participant/lastName)" />
		<xsl:variable name="displayDate" select="'Dummy Date'" />
		<!-- <xsl:variable name="reportLang" select="'report/language'" /> -->

		<html>
			<head>
				<title>
					<xsl:value-of select="reportType" />
					-
					<xsl:value-of select="company" />
					-
					<xsl:value-of select="$participantFullName" />
				</title>
				<!-- <link href="{$baseurl}/core/css/font_{$reportLang}.css" rel="stylesheet" 
					type="text/css" /> -->
				<link href="{$baseurl}/core/css/report.css" rel="stylesheet"
					type="text/css" />
				<link href="{$baseurl}/projects/coaching/plan/report.css" rel="stylesheet"
					type="text/css" />
				<meta name="baseurl" content="{$baseurl}" />
				<meta name="genmode" content="{$genmode}" />
				<meta name="subtype" content="{$subtype}" />
			</head>
			<body>
				<xsl:if test="$includeOverview = 'true'">
					<xsl:apply-templates select="overviewSection" />
				</xsl:if>
				<xsl:if test="$includeGoals = 'true'">
					<xsl:apply-templates select="goalsSection" />
				</xsl:if>
				<xsl:if test="$includeJournal = 'true'">
					<xsl:apply-templates select="journalSection" />
				</xsl:if>
			</body>
		</html>

	</xsl:template>

	<xsl:template match="overviewSection">
		<div class="section about">
			<p class="s13">
				<xsl:value-of select="title" />
			</p>
			<hr />
			<p class="s6">
				<xsl:value-of select="text[1]" />
			</p>
		</div>

		<br />

		<div class="section goals group">
			<h1>
				<xsl:value-of select="overviewGoalsSection/title[1]" />
				<span class="s7 rightmost-column-header">
					<xsl:value-of select="overviewGoalsSection/progressHeader[1]" />
				</span>
			</h1>

			<xsl:choose>
				<xsl:when
					test="count(overviewGoalsSection/overviewGoals/overviewGoal) > 0">
					<table class="coach-plan-table">
						<xsl:apply-templates
							select="overviewGoalsSection/overviewGoals/overviewGoal" />
					</table>
				</xsl:when>
				<xsl:otherwise>
					<hr />
					<p class="s6">
						<xsl:value-of select="overviewGoalsSection/noOverviewGoalsLabel" />
					</p>
				</xsl:otherwise>
			</xsl:choose>

			<br />
			<br />
		</div>

		<div class="section timeframe group">

			<h1>
				<xsl:value-of select="timeframeSection/title" />
			</h1>
			<hr />

			<table class="coach-plan-table">

				<tr>
					<td class="timeframe-column-1">
						<p class="s8">
							<xsl:value-of select="timeframeSection/timeElapsedLabel" />
						</p>
					</td>
					<td>
						<p class="s9">
							<xsl:value-of select="timeframeSection/timeElapsed" />
							<span> %</span>
						</p>
					</td>
				</tr>
				<tr>
					<td class="timeframe-column-1">
						<p class="s8">
							<xsl:value-of select="timeframeSection/startDateLabel" />
						</p>
					</td>
					<td>
						<p class="s9">
							<xsl:value-of select="timeframeSection/startDate" />
						</p>
					</td>
				</tr>
				<tr>
					<td class="timeframe-column-1">
						<p class="s8">
							<xsl:value-of select="timeframeSection/endDateLabel" />
						</p>
					</td>
					<td>
						<p class="s9">
							<xsl:value-of select="timeframeSection/endDate" />
						</p>
					</td>
				</tr>

			</table>

			<br />
			<br />
		</div>
		<div class="section meetings item-bottom-top group">
			<h1 style="page-break-after:avoid">
				<xsl:value-of select="meetingsSection/title" />
				<span class="s7 rightmost-column-header">
					<xsl:value-of select="meetingsSection/completeHeader" />
				</span>
			</h1>

			<xsl:choose>
				<xsl:when test="count(meetingsSection/meetings/meeting) > 0">
					<table class="coach-plan-table">
						<xsl:apply-templates select="meetingsSection/meetings/meeting" />
					</table>
				</xsl:when>
				<xsl:otherwise>
					<hr />
					<p class="s6">
						<xsl:value-of select="meetingsSection/noMeetingsLabel" />
					</p>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>

	<xsl:template match="overviewGoal">

		<tr class="item-top-border group">
			<td class="item-column-1">
				<p class="s8">
					<xsl:value-of select="genericName" />
				</p>
			</td>
			<td>
				<p class="s9 ">
					<xsl:value-of select="name" />
				</p>
			</td>
			<td align="right" class="item-column-3">
				<p class="s9">
					<xsl:value-of select="progress" />
					<span> %</span>
				</p>
			</td>
		</tr>
		<tr>
			<td></td>
			<td class="item-bottom-padding">
				<p class="s9">
					<xsl:value-of select="competency" />
				</p>
			</td>
			<td></td>
		</tr>
	</xsl:template>

	<xsl:template match="meeting">
		<tr class="item-top-border group">
			<td class="item-bottom-padding item-column-1">
				<p class="s8  avoid-break">
					<xsl:value-of select="date" />
				</p>
			</td>
			<td class="item-bottom-padding item-column-2">
				<p class="s9  avoid-break">
					<xsl:value-of select="type" />
				</p>
			</td>
			<td class="item-bottom-padding">
				<p class="s9 avoid-break">
					<xsl:value-of select="topic" />
				</p>
			</td>
			<td class="item-bottom-padding item-column-3" align="right">
				<xsl:if test="complete = 'true'">
					<img class="img-check-mark" alt="completed" src="{$baseurl}/projects/coaching/plan/css/images/check-mark.png"/>
				</xsl:if>
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="goalsSection">
		<xsl:apply-templates select="goals/goal">
			<xsl:with-param name="goalsSection" select="." />
		</xsl:apply-templates>

	</xsl:template>

	<xsl:template match="goal">
		<xsl:param name="goalsSection" />
		<div class="section section-padding-top group">

			<table class="coach-plan-table group">
				<tr>
					<td colspan="2">
						<p class="s13">
							<xsl:value-of select="title" />
						</p>
						<hr />
						<p class="s6">
							<xsl:value-of select="$goalsSection/goalText" />
						</p>
					</td>
				</tr>
			</table>
			<table class="coach-plan-table group">
				<tr>
					<td class="dev-action-column-1">
						<p class="s8">
							<xsl:value-of select="$goalsSection/competencyLabel" />
						</p>
					</td>
					<td>
						<p class="s9 ">
							<xsl:value-of select="competency" />
						</p>
					</td>
				</tr>
				<tr>
					<td class="dev-action-column-1">
						<p class="s8">
							<xsl:value-of select="$goalsSection/behaviorsLabel" />
						</p>
					</td>
					<td>
						<p class="s9 ">
							<xsl:value-of select="behaviors" />
						</p>
					</td>
				</tr>
			</table>
			<p>
				<br />
			</p>
			<div class="group">
			<h1>
				<xsl:value-of select="$goalsSection/developmentActionsTitle" />
				<span class="s7 rightmost-column-header">
					<xsl:value-of select="$goalsSection/developmentActionsTargetDateHeader" />
				</span>
			</h1>

			<xsl:choose>
				<xsl:when test="count(developmentActions/developmentAction) > 0">
					<table class="coach-plan-table">
						<xsl:apply-templates select="developmentActions/developmentAction">
							<xsl:with-param name="goalsSection" select="$goalsSection" />
						</xsl:apply-templates>
					</table>
				</xsl:when>
				<xsl:otherwise>
					<hr />
					<p class="s6">
						<xsl:value-of select="$goalsSection/noDevelopmentActionsLabel" />
					</p>
				</xsl:otherwise>
			</xsl:choose>
			</div>

			<br />

			<!-- STATUS TABLE -->
			<div class="status avoid-break group">
				<h1>
					<xsl:value-of select="$goalsSection/statusTitle" />
				</h1>
				<hr />
				<table class="status-table">
					<tr>
						<td class="status-column-1"></td>
						<td class="status-column-2"></td>
						<td class="status-percentage-column" align="right">
							<div class="relative-div">
								<span class="status-label-zero s11 status-column-header">0</span>
								<p class="s11 status-column-header">10</p>
							</div>
						</td>
						<td class="status-percentage-column" align="right">
							<p class="s11 status-column-header">20</p>
						</td>
						<td class="status-percentage-column" align="right">
							<p class="s11 status-column-header">30</p>
						</td>
						<td class="status-percentage-column" align="right">
							<p class="s11 status-column-header">40</p>
						</td>
						<td class="status-percentage-column" align="right">
							<p class="s11 status-column-header">50</p>
						</td>
						<td class="status-percentage-column" align="right">
							<p class="s11 status-column-header">60</p>
						</td>
						<td class="status-percentage-column" align="right">
							<p class="s11 status-column-header">70</p>
						</td>
						<td class="status-percentage-column" align="right">
							<p class="s11 status-column-header">80</p>
						</td>
						<td class="status-percentage-column" align="right">
							<p class="s11 status-column-header">90</p>
						</td>
						<td class="status-percentage-column" align="right">
							<p class="s11 status-column-header">100</p>
						</td>
					</tr>
					<tr>
						<td class=" status-column-1">
							<p class="s8">
								<xsl:value-of select="$goalsSection/progressLabel" />
							</p>
						</td>
						<td class=" status-column-2" align="right">
							<p class="s9">
								<xsl:value-of select="progress" />
								<span> %</span>
							</p>
						</td>

						<!-- ******************************************************************************************* 
							in td's below: don't insert div or hide WHEN % is less than ********************************************************************************************* -->
						<td class="status-percentage-column">
							<xsl:if test="progress >= 10">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="progress >= 20">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="progress >= 30">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="progress >= 40">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="progress >= 50">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="progress >= 60">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="progress >= 70">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="progress >= 80">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="progress >= 90">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="progress = 100">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
					</tr>
					<tr>
						<td class=" status-column-1">
							<p class="s8">
								<xsl:value-of select="$goalsSection/commitmentLabel" />
							</p>
						</td>
						<td class=" status-column-2" align="right">
							<p class="s9">
								<xsl:value-of select="commitment" />
								<span> %</span>
							</p>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="commitment >= 10">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="commitment >= 20">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="commitment >= 30">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="commitment >= 40">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="commitment >= 50">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="commitment >= 60">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="commitment >= 70">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="commitment >= 80">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="commitment >= 90">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="commitment = 100">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
					</tr>
					<tr>
						<td class=" status-column-1">
							<p class="s8">
								<xsl:value-of select="$goalsSection/confidenceLabel" />
							</p>
						</td>
						<td class=" status-column-2" align="right">
							<p class="s9">
								<xsl:value-of select="confidence" />
								<span> %</span>
							</p>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="confidence >= 10">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="confidence >= 20">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="confidence >= 30">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="confidence >= 40">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="confidence >= 50">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="confidence >= 60">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="confidence >= 70">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="confidence >= 80">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="confidence >= 90">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
						<td class="status-percentage-column">
							<xsl:if test="confidence = 100">
								<div class="relative-div">
									<span class="status-green"></span>
								</div>
							</xsl:if>
						</td>
					</tr>
				</table>
				<br />
			</div>
			<!-- END STATUS TABLE -->
		</div>
	</xsl:template>

	<xsl:template match="developmentAction">
		<xsl:param name="goalsSection" />

		<tr class="item-top-border group">
			<td class="dev-action-column-1 ">
				<p class="s8">
					<xsl:value-of select="$goalsSection/developmentActionActionLabel" />
				</p>
			</td>
			<td>
				<p class="s9">
					<xsl:value-of select="action" />
				</p>
			</td>
			<td class="dev-action-column-3 " align="right">
				<p class="s9">
					<xsl:value-of select="date" />
				</p>
			</td>
		</tr>
		<tr class="group">
			<td class="dev-action-column-1 ">
				<p class="s8">
					<xsl:value-of select="$goalsSection/developmentActionResourcesLabel" />
				</p>
			</td>
			<td>
				<p class="s9">
					<xsl:value-of select="resources" />
				</p>
			</td>
			<td class="dev-action-column-3" />
		</tr>
	</xsl:template>

	<xsl:template match="journalSection">
		<div class="section journal group">
			<p class="s13 group">
				<xsl:value-of select="title" />
			</p>
			<hr />
			<p class="s6 group">
				<xsl:value-of select="text" />
			</p>
			<br />
			<h1 class="group">
				<xsl:value-of select="notesSection/title" />
			</h1>

			<xsl:choose>
				<xsl:when test="count(notesSection/notes/note) > 0">
					<table class="coach-plan-table">
						<xsl:apply-templates select="notesSection/notes/note" />
					</table>
				</xsl:when>
				<xsl:otherwise>
					<hr />
					<p class="s6 group">
						<xsl:value-of select="notesSection/noNotesLabel" />
					</p>
				</xsl:otherwise>
			</xsl:choose>

		</div>
	</xsl:template>

	<xsl:template match="note">
		<tr class="item-top-border group">
			<td class="item-bottom-padding item-column-1 ">
				<p class="s8 note avoid-break">
					<xsl:value-of select="date" />
				</p>
			</td>
			<td class="item-bottom-padding">
				<p class="s6 note avoid-break">
					<xsl:value-of select="text" />
				</p>
			</td>
		</tr>
	</xsl:template>

</xsl:stylesheet>
