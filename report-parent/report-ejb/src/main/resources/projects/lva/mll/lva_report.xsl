<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html"
	xmlns:ui="http://java.sun.com/jsf/facelets" xmlns:p="http://primefaces.org/ui"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xslt"
	xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xsl xalan xs">

	<!-- http://stackoverflow.com/questions/9538619/xslt-refuses-to-write-doctype-declaration -->
	<xsl:output method="xml" indent="yes" encoding="UTF-8"
		doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />

	<xsl:param name="baseurl" select="'http://localhost'" />

	<!-- these two not used? -->
	<xsl:param name="genmode" select="'pdf'" />
	<xsl:param name="subtype" select="'readiness'" />

	<!-- no default match -->
	<xsl:template match="*" />

    <xsl:template name="javascript">
        <script type="text/javascript">
        <!-- The parser responsible for generating the PDF report is incredibly crabby about
             escape characters; if you screw with this code, make sure you test PDF creation!
             Also, that nested "CDATA" structure is needed to make sure both the transform
             parsers for the HTML and PDF versions handle the Javascript correctly.  Without the
             second one, the HTML parser gags; and without both, the PDF parser escapes the
             less-than, greater-than, and ampersand symbols, breaking the Javascript. -->
        <xsl:text disable-output-escaping="yes">
            <![CDATA[
                //<![CDATA[
                // Fun fact:  wkhtmltopdf, our PDF creator, can't handle jQuery.  Don't use jQuery.
                function populatePreviews() {
                    var idExtractor = 'tied-to-id-(.+)';
                    var elems = document.getElementsByTagName('*'), i;
                    for (i in elems) {
                        if((' ' + elems[i].className + ' ').indexOf(' tied-to-paragraph ') > -1) {
                            var classes = elems[i].className.split(" ");
                            for (var j = 0; j < classes.length; j++) {
                                var idMatch = classes[j].match(idExtractor);
                                if (idMatch != null && idMatch.length > 0) {
                                    var content;
                                    
                                    // If we're not in PDF mode, the text we want will be in a 
                                    // textarea element in the paragraph.  Otherwise, it will simply
                                    // be the paragraph text.
                                    var textareas = elems[i].getElementsByTagName("textarea");
                                    if (textareas.length > 0) {
                                        content = textareas[0].value;
                                    }
                                    else {
                                        content = elems[i].innerHTML;
                                    }
                                
                                    document.getElementById(idMatch[1]).innerHTML = content;
	                                break;
                                }
                            }
                        }
                    }
                }

                function replaceContentInContainer(matchClass,content) {
            ]]>
        </xsl:text>
        <xsl:if test="not(/lvaDbReport//characteristics[@type='v252']/characteristic[.='Problem Solving'])">
            <xsl:text disable-output-escaping="yes">
                <![CDATA[
                    var elems = document.getElementsByTagName('*'), i;
                    for (i in elems) {
                        if((' ' + elems[i].className + ' ').indexOf(' ' + matchClass + ' ') > -1) {
                            elems[i].innerHTML = content;
                        }
                    }
                ]]>
            </xsl:text>
        </xsl:if>
        <xsl:text disable-output-escaping="yes">
            <![CDATA[       
                }
            ]]>
        </xsl:text>
        
        <xsl:if test="$genmode = 'pdf'">
            <xsl:text disable-output-escaping="yes">
                <![CDATA[
                    window.onload = function ()
	                {
	                    populatePreviews();
	                    replaceContentInContainer("cognitive-content", "");
	                }
                ]]>
            </xsl:text>
        </xsl:if>
        <xsl:if test="$genmode = 'jsf'">
            <xsl:text disable-output-escaping="yes">
                <![CDATA[
                    $(document).ready(function() {
                    	populatePreviews();
	                    replaceContentInContainer("cognitive-content", "");
                    });
                ]]>
            </xsl:text>
        </xsl:if>
        
        <xsl:text disable-output-escaping="yes">
            // ]]&gt;
        </xsl:text>
        
        </script>
    </xsl:template>

	<xsl:template match="/lvaDbReport">

		<xsl:variable name="participantFullName"
			select="concat(participant/firstName,' ',participant/lastName)" />
		<xsl:variable name="displayDate" select="'Dummy Date'" />
		<xsl:variable name="reportName" select="'report/language'" />
		
		<xsl:choose>
			<xsl:when test="$genmode='pdf'">
				<html>
					<head>
						<title>
							<xsl:value-of select="reportType" />
							<xsl:value-of select="company" />
							<xsl:value-of select="$participantFullName" />
						</title>
						<!-- <link href="{$baseurl}/core/css/font_{$reportLang}.css" rel="stylesheet" 
							type="text/css" /> -->
						<link href="{$baseurl}/core/css/report.css" rel="stylesheet"
							type="text/css" />
						<link href="{$baseurl}/projects/lva/mll/report.css" rel="stylesheet"
							type="text/css" />

						<meta name="baseurl" content="{$baseurl}" />
						<meta name="genmode" content="{$genmode}" />
						<meta name="subtype" content="{$subtype}" />

					</head>
					<body>
                        <xsl:call-template name="javascript" />                       
						<xsl:apply-templates select="about" />
						<xsl:apply-templates select="summary" />
						<xsl:apply-templates select="ratings" />
						<xsl:call-template name="devplan" />
						<xsl:apply-templates select="appendix" />
					</body>
				</html>
			</xsl:when>
			<xsl:when test="$genmode='jsf'">
				<ui:composition>
                    <xsl:call-template name="javascript" />                       
					<xsl:apply-templates select="about" />
					<xsl:apply-templates select="summary" />
					<xsl:apply-templates select="ratings" />
					<xsl:call-template name="devplan" />
					<xsl:apply-templates select="appendix" />
				</ui:composition>
			</xsl:when>
		</xsl:choose>

	</xsl:template>

	<xsl:template match="about">

		<div class="section about">
			<h1>
				<xsl:value-of select="title" />
			</h1>
			<p class="configText">
				<xsl:apply-templates select="text[1]" mode="textMagic" />
			</p>

			<div class="textSections">
				<img id="assessment_model" src="{$baseurl}/core/png/assessment_model_main.png"
					width="280px" height="280px" />
				<xsl:for-each select="textSections/textSection">
					<h3>
						<xsl:value-of select="title" />
					</h3>
					<p>
						<xsl:apply-templates select="text[1]" mode="textMagic" />
					</p>
				</xsl:for-each>
			</div>
			<xsl:apply-templates select="ratingLegend" />
		</div>
	</xsl:template>

	<xsl:template match="ratingLegend">
		<div id="ratingLegend">
			<h2>
				<xsl:value-of select="title" />
			</h2>
			<p>
				<xsl:value-of select="text[1]" />
			</p>
			<div id="legend">
				<xsl:for-each select="entries/entry">
					<div class="{@color}">
						<p class="box">box</p>
						<p class="label">
							<xsl:value-of select="." />
						</p>
					</div>
				</xsl:for-each>
			</div>
			<xsl:for-each select="text[position() > 1]">
				<p>
					<xsl:value-of select="." />
				</p>
			</xsl:for-each>
		</div>
	</xsl:template>

	<xsl:template match="summary">
		<div class="section summary">
			<div class="group">
				<p class="prefix">
					<xsl:value-of disable-output-escaping="yes" select="configuration" />
				</p>
				<xsl:if test="$subtype='readiness'">
					<p class="readinessIndicators">
						<!-- JJB: All this needs refactored for internationalization. English 
							text hardwired in images right now. -->
						<xsl:choose>
							<xsl:when test="$genmode='pdf'">
								<img id="develop_in_place" width="168px" height="46">
									<xsl:attribute name="src"><xsl:value-of
										select="$baseurl" />/core/svg/develop_in_place<xsl:if
										test="score/@value = '1'">_on</xsl:if>.svg</xsl:attribute>
								</img>
								<img id="ready_in_3-5_years" width="168px" height="46">
									<xsl:attribute name="src"><xsl:value-of
										select="$baseurl" />/core/svg/ready_in_3-5_years<xsl:if
										test="score/@value = '2'">_on</xsl:if>.svg</xsl:attribute>
								</img>
								<img id="ready_in_1-2-years" width="168px" height="46">
									<xsl:attribute name="src"><xsl:value-of
										select="$baseurl" />/core/svg/ready_in_1-2-years<xsl:if
										test="score/@value = '3'">_on</xsl:if>.svg</xsl:attribute>
								</img>
								<img id="ready_now" width="168px" height="46">
									<xsl:attribute name="src"><xsl:value-of
										select="$baseurl" />/core/svg/ready_now<xsl:if
										test="score/@value = '4'">_on</xsl:if>.svg</xsl:attribute>
								</img>
							</xsl:when>
							<!-- JJB Handle when not PDF because old browsers suck. Take easy way 
								out because all needs refactored anyway. -->
							<xsl:otherwise>
								<img id="develop_in_place" width="168px" height="46">
									<xsl:attribute name="src"><xsl:value-of
										select="$baseurl" />/core/png/develop_in_place<xsl:if
										test="score/@value = '1'">_on</xsl:if>.png</xsl:attribute>
								</img>
								<img id="ready_in_3-5_years" width="168px" height="46">
									<xsl:attribute name="src"><xsl:value-of
										select="$baseurl" />/core/png/ready_in_3-5_years<xsl:if
										test="score/@value = '2'">_on</xsl:if>.png</xsl:attribute>
								</img>
								<img id="ready_in_1-2-years" width="168px" height="46">
									<xsl:attribute name="src"><xsl:value-of
										select="$baseurl" />/core/png/ready_in_1-2-years<xsl:if
										test="score/@value = '3'">_on</xsl:if>.png</xsl:attribute>
								</img>
								<img id="ready_now" width="168px" height="46">
									<xsl:attribute name="src"><xsl:value-of
										select="$baseurl" />/core/png/ready_now<xsl:if
										test="score/@value = '4'">_on</xsl:if>.png</xsl:attribute>
								</img>
							</xsl:otherwise>
						</xsl:choose>
					</p>
				</xsl:if>
				
				<xsl:if test="$subtype='fit'">
					<p class="readinessIndicators">
					<xsl:choose>
							<xsl:when test="$genmode='pdf'">
						<!-- JJB: All this needs refactored for internationalization. English 
							text hardwired in images right now. -->
							<img id="develop_in_place" width="168px" height="46">
									<xsl:attribute name="src"><xsl:value-of
										select="$baseurl" />/core/svg/not_recommend<xsl:if
										test="score/@value = '1'">_on</xsl:if>.svg</xsl:attribute>
								</img>
								<img id="ready_in_3-5_years" width="168px" height="46">
									<xsl:attribute name="src"><xsl:value-of
										select="$baseurl" />/core/svg/recommend_res<xsl:if
										test="score/@value = '2'">_on</xsl:if>.svg</xsl:attribute>
								</img>
								<img id="ready_in_1-2-years" width="168px" height="46">
									<xsl:attribute name="src"><xsl:value-of
										select="$baseurl" />/core/svg/recommend<xsl:if
										test="score/@value = '3'">_on</xsl:if>.svg</xsl:attribute>
								</img>
								<img id="ready_now" width="168px" height="46">
									<xsl:attribute name="src"><xsl:value-of
										select="$baseurl" />/core/svg/strongly_recommend<xsl:if
										test="score/@value = '4'">_on</xsl:if>.svg</xsl:attribute>
								</img>
								</xsl:when>
								<xsl:otherwise>
								<img id="develop_in_place" width="168px" height="46">
									<xsl:attribute name="src"><xsl:value-of
										select="$baseurl" />/core/png/not_recommend<xsl:if
										test="score/@value = '1'">_on</xsl:if>.png</xsl:attribute>
								</img>
								<img id="ready_in_3-5_years" width="168px" height="46">
									<xsl:attribute name="src"><xsl:value-of
										select="$baseurl" />/core/png/recommend_res<xsl:if
										test="score/@value = '2'">_on</xsl:if>.png</xsl:attribute>
								</img>
								<img id="ready_in_1-2-years" width="168px" height="46">
									<xsl:attribute name="src"><xsl:value-of
										select="$baseurl" />/core/png/recommend<xsl:if
										test="score/@value = '3'">_on</xsl:if>.png</xsl:attribute>
								</img>
								<img id="ready_now" width="168px" height="46">
									<xsl:attribute name="src"><xsl:value-of
										select="$baseurl" />/core/png/strongly_recommend<xsl:if
										test="score/@value = '4'">_on</xsl:if>.png</xsl:attribute>
								</img>
								</xsl:otherwise>
						</xsl:choose>
							</p>
				</xsl:if>
	
				
				  <xsl:if test="normalize-space(title) != ''">  
					<h1>
						<xsl:value-of select="title" />
					</h1>
				  </xsl:if> 
				  
					  				
				<xsl:choose>
					<xsl:when test="$subtype='readiness'">
						<xsl:apply-templates select="readiness" mode="summary" />
					</xsl:when>
					<xsl:when test="$subtype='development'">
						<xsl:apply-templates select="development" mode="summary" />
					</xsl:when>
					<xsl:when test="$subtype='fit'">
						<xsl:apply-templates select="fit" mode="summary" />
					</xsl:when>
				</xsl:choose>
			</div>
			<xsl:apply-templates select="strengths" mode="summary" />

			<xsl:apply-templates select="needs" mode="summary" />
			<xsl:if test="$subtype='readiness' or $subtype='fit'">
				<xsl:apply-templates select="potential" mode="summary" />
			</xsl:if>

			<xsl:apply-templates select="derailers" mode="summary" />

		</div>
	</xsl:template>

	<xsl:template match="readiness | development | potential | fit"
		mode="summary">
		<div class="{name()} summary">
			<xsl:if test="title">
				<h1>
					<xsl:value-of select="title" />
				</h1>
			</xsl:if>
			<xsl:apply-templates select="text[1]" mode="textMagic" />
		</div>
	</xsl:template>

	<xsl:template match="strengths | needs " mode="summary">
		<xsl:variable name="sum" select="."/>
		<div class="{name()} summary group">
			<xsl:if test="count(items/item) = 0">
				<div class="group">
					<h2>
						<xsl:value-of select="title" />
					</h2>
					<p class="noitems">
						<xsl:apply-templates select="noitems[1]" mode="textMagic" />
					</p>
				</div>
			</xsl:if>
			<xsl:for-each select="items/item">
				<div class="group">
					<xsl:if test="position() = 1">
						<h2>
							<xsl:value-of select="$sum/title" />
						</h2>
					</xsl:if>
					<h3>
						<xsl:value-of select="title" />
					</h3>
					<div class="model">
						<img id="assessment_model" src="{$baseurl}/core/png/assessment_model_{@id}.png"
							class="modelIcon" />
						<p class="modelText" id="{text/@updateId}">
							<xsl:apply-templates select="text[1]" mode="textMagic" />
						</p>
					</div>
				</div>
			</xsl:for-each>
		</div>
	</xsl:template>

	<xsl:template match="derailers" mode="summary">
		<div class="{name()} summary group">
			<h2>
				<xsl:value-of select="title" />
			</h2>
			<xsl:apply-templates select="text[1]" mode="textMagic" />
			<xsl:if test="count(items/item) = 0">
				<p class="noitems">
					<xsl:apply-templates select="noitems[1]" mode="textMagic" />
				</p>
			</xsl:if>
			<xsl:for-each select="items/item">
                <xsl:if test="title='Preview'">
                    <p class="modelText" id="{text/@updateId}">
                        <xsl:apply-templates select="text[1]" mode="textMagic" />
                    </p>
                </xsl:if>
                <xsl:if test="title!='Preview'">
					<p style="clear: left;">
						<xsl:if test="title='Note'">
							<xsl:attribute name="class">note</xsl:attribute>
						</xsl:if>
						<em>
							<xsl:value-of select="title" />
							:
						</em>
						<xsl:apply-templates select="text[1]" mode="textMagic" />
					</p>
				</xsl:if>
			</xsl:for-each>
		</div>
	</xsl:template>


	<xsl:template match="ratings">
		<div class="ratings">
			<xsl:apply-templates select="section" mode="ratings" />
		</div>
	</xsl:template>

	<xsl:template match="section" mode="ratings">
		<div class="section group break">
			<div class="group">
				<h1>
					<xsl:value-of select="title" />
				</h1>
				<xsl:if test="$subtype='readiness' or $subtype='fit'">
					<p class="sectionScore">
						<span class="{score/@value}">
							<xsl:value-of select="score" />
						</span>
					</p>
					<p class="percentile">
					<xsl:value-of select="percentileLabel" /> 
					</p>
				</xsl:if>
				<div class="model">
					<img id="assessment_model" src="{$baseurl}/core/png/assessment_model_{@id}.png"
						class="modelIcon" />
					<p class="modelText tied-to-paragraph tied-to-id-{text/@updateId}">
						<xsl:apply-templates select="text[1]" mode="textMagic" />
					</p>
				</div>
			
			<xsl:apply-templates select="superfactors" mode="ratings" />
			
			<xsl:apply-templates select="characteristics"
				mode="ratings" />
			</div>
			<xsl:apply-templates select="strengths" mode="ratings" />

			<xsl:apply-templates select="needs" mode="ratings" />

			<xsl:apply-templates select="notes" mode="ratings" />

			<xsl:if test="$subtype='development'">
				<xsl:apply-templates select="suggestions" mode="ratings" />
			</xsl:if>

		</div>
	</xsl:template>  <!-- end section -->

	<xsl:template match="superfactors" mode="ratings">
		<xsl:variable name="superfactorcount" select="count(superfactor)" />
		<xsl:variable name="leftcount" select="ceiling($superfactorcount div 2)" />
		<div class="superfactors group">
			<div class="leftcol">
				<xsl:for-each select="superfactor[position() &lt;= $leftcount]">
					<h3>
						<xsl:value-of select="name" />
					</h3>
					<xsl:for-each select="competencies/competency">
						<xsl:apply-templates select="." />
					</xsl:for-each>
				</xsl:for-each>
			</div>
			<div class="rightcol">
				<xsl:for-each select="superfactor[position() &gt; $leftcount]">
					<h3>
						<xsl:value-of select="name" />
					</h3>
					<xsl:for-each select="competencies/competency">
						<xsl:apply-templates select="." />
					</xsl:for-each>
				</xsl:for-each>
			</div>

		</div>
	</xsl:template>

	<xsl:template match="characteristics" mode="ratings">
		<xsl:variable name="charcount" select="count(characteristic)" />
		<xsl:variable name="leftcount" select="ceiling($charcount div 2)" />
		<div class="characteristics group">
			<div class="leftcol">
				<xsl:for-each select="characteristic[position() &lt;= $leftcount]">
					<xsl:apply-templates select="." />
				</xsl:for-each>
			</div>
			<div class="rightcol">
				<xsl:for-each select="characteristic[position() &gt; $leftcount]">
					<xsl:apply-templates select="." />
				</xsl:for-each>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="strengths|needs" mode="ratings">
		<div class="strengths group">
			<h2>
				<xsl:value-of select="title" />
			</h2>
			<xsl:if test="count(items/item) = 0">
				<p class="noitems">
					<xsl:apply-templates select="noitems[1]" mode="textMagic" />
				</p>
			</xsl:if>
			<xsl:apply-templates select="items/item" mode="items" />
		</div>
	</xsl:template>

	<xsl:template match="notes" mode="ratings">
		<xsl:if test="count(items/item) > 0">
			<div class="notes group">
				<h2>
					<xsl:value-of select="title" />
				</h2>
				<xsl:apply-templates select="items/item" mode="items" />
			</div>
		</xsl:if>
	</xsl:template>

	<xsl:template match="item" mode="items">
		<p class="group">
			<em>
				<xsl:value-of select="title" />
				:
			</em>
			<xsl:apply-templates select="text[1]" mode="textMagic" />
		</p>
	</xsl:template>

	<xsl:template match="item" mode="items-unescape">
		<p class="group">
			<em>
				<xsl:value-of select="title" />
				:
			</em>
			<xsl:apply-templates select="text[1]" mode="textMagic" />
		</p>
	</xsl:template>

	<xsl:template match="suggestions" mode="ratings">

		<xsl:variable name="sectionTitle" select="../title" />
		<xsl:variable name="suggs" select="." />
		
		<xsl:for-each select="devSections/devSection">
			<div>
				<xsl:attribute name="class">
          		<xsl:text>devSug group</xsl:text>
          		<xsl:if test="@page='true'"> page</xsl:if>
        </xsl:attribute>
        	
				
				<xsl:if test="count(items/item) = 0">
					<div class="group">
						<h1>
							<xsl:value-of select="$sectionTitle" />
						</h1>
		
						<xsl:if test="@page='true'">
							<h2>Development Suggestions</h2>
						</xsl:if>
		
						<h3>
							<xsl:value-of select="title" />
						</h3>
						<p class="noitems">
							<xsl:apply-templates select="noitems[1]" mode="textMagic" />
						</p>
					</div>
				</xsl:if>
				<xsl:for-each select="items/item">
					<div class="group">
					<xsl:if test="position() = 1">
						<h1>
							<xsl:value-of select="$sectionTitle" />
						</h1>
		
						<!-- 
						<xsl:if test="@page='true'">
							<h2>Development Suggestions</h2>
						</xsl:if>
						 -->
						 <h2><xsl:value-of select="$suggs/title" /></h2>
		
						<h3>
							<!--  xsl:value-of select="$suggs/title" / -->
							<xsl:value-of select="../../title" />
						</h3>
					</xsl:if>
					<xsl:if test="title != ''">
						<h4>
							<xsl:value-of select="title" />
						</h4>
					</xsl:if>
					<xsl:value-of disable-output-escaping="yes" select="text" />
					</div>
				</xsl:for-each>

				<xsl:if test="iaLinks/link">
					<div class="group">
						<xsl:if test="count(devPoints/point) > 0">
							<xsl:attribute name="class">iampad</xsl:attribute>
						</xsl:if>
						<p class="iaTitle">
							<xsl:text>Instant Advice Module(s):</xsl:text>
						</p>
						<ul class="ialist">
							<xsl:for-each select="iaLinks/link">
								<li>
									<a target="_blank" href="{url}">
										<xsl:value-of select="displayText" />
									</a>
									<xsl:if test="position() != last()">
									</xsl:if>
								</li>
							</xsl:for-each>
						</ul>
					</div>
				</xsl:if>
			</div>
		</xsl:for-each>

		<!-- <div class="suggestions"> <h2><xsl:value-of select="title" /></h2> 
			<xsl:apply-templates select="item" mode="items"/> </div> -->

	</xsl:template>


	<xsl:template match="competency|characteristic">
		<div>
			<xsl:attribute name="class">
        <xsl:text>competency </xsl:text>
        <xsl:value-of select="translate(.,' ','_')" />
        <xsl:choose>

          <!-- stop-gap measures until RCM is fixed -->
          <xsl:when test=". = 'Leadership Aspirations'">
            <xsl:text> v351</xsl:text>
          </xsl:when>
          <xsl:when
				test=". = 'Learning Orientation' or . = 'Experience Orientation'">
            <xsl:text> v252</xsl:text>
          </xsl:when>
          <xsl:when test=". = 'Career Drivers'">
            <xsl:text> v36</xsl:text>
          </xsl:when>

          <xsl:when
				test="@type='v423' or ../@type='v423' or ../../@type='v423'">
            <xsl:text> v423</xsl:text>
          </xsl:when>
          <xsl:when
				test="@type='v36' or ../@type='v36' or ../../@type='v36'">
            <xsl:text> v36</xsl:text>
          </xsl:when>
          <xsl:when
				test="@type='v81' or ../@type='v81' or ../../@type='v81'">
            <xsl:text> v81</xsl:text>
          </xsl:when>
          <xsl:when
				test="@type='v252' or ../@type='v252' or ../../@type='v252'">
            <xsl:text> v252</xsl:text>
          </xsl:when>
          <xsl:when
				test="@type='v351' or ../@type='v351' or ../../@type='v351'">
            <xsl:text> v351</xsl:text>
          </xsl:when>
          <xsl:when
				test="@type='v333' or ../@type='v333' or ../../@type='v333'">
            <xsl:text> v333</xsl:text>
          </xsl:when>
        </xsl:choose>
      </xsl:attribute>
			<p class="label">
				<xsl:value-of select="." />
			</p>
			<div>
				<xsl:attribute name="class">
      	<xsl:choose>
      		<xsl:when test="name() = 'competency'">
              <xsl:text>rating bar v</xsl:text>
              <xsl:value-of select="translate(@pdiRating,'.','')" />
      		</xsl:when>
      		<xsl:otherwise>
	      		<xsl:text>stanine bar v</xsl:text>
	      		<xsl:value-of select="@stanine" />
      		</xsl:otherwise>
      	</xsl:choose>
      	</xsl:attribute>
				<p class="square p1"></p>
				<p class="square p2"></p>
				<p class="square p3"></p>
				<p class="square p4"></p>
				<p class="square p5"></p>
				<p class="square p6"></p>
				<p class="square p7"></p>
				<p class="square p8"></p>
				<p class="square p9"></p>
				<div class="spacers">
					<p class="spacer s1"></p>
					<p class="spacer s2"></p>
					<p class="spacer s3"></p>
					<p class="spacer s4"></p>
					<p class="spacer s5"></p>
					<p class="spacer s6"></p>
					<p class="spacer s7"></p>
					<p class="spacer s8"></p>
				</div>
			</div>
		</div>
	</xsl:template>

	<!-- deprecated -->
	<xsl:template match="development" mode="deprecated">
		<div class="section development">
			<h1>
				<xsl:value-of select="title" />
			</h1>

			<xsl:for-each select="devSections/devSection">
				<h2>
					<xsl:value-of select="title" />
				</h2>
				<xsl:if test="text">
					<p>
						<xsl:apply-templates select="text[1]" mode="textMagic" />
					</p>
				</xsl:if>
				<xsl:for-each select="devElements/devElement">
					<h3>
						<xsl:value-of select="title" />
					</h3>
					<p>
						<xsl:value-of select="text" />
					</p>
					<ul>
						<xsl:for-each select="devPoints/devPoint">
							<li class="point">
								<span class="title">
									<xsl:value-of select="title" />
									:
								</span>
								<span class="text">
									<xsl:value-of select="text" />
								</span>
								<xsl:for-each select="iaLinks/link">
									<p class="link">
										<a target="_blank">
											<xsl:attribute name="href">
                      <xsl:value-of
												disable-output-escaping="yes" select="url" />
                    </xsl:attribute>
											<xsl:value-of select="displayText" />
										</a>
									</p>
								</xsl:for-each>
							</li>
						</xsl:for-each>
					</ul>
				</xsl:for-each>
			</xsl:for-each>
		</div>
	</xsl:template>

	<xsl:template name="devplan">
		<!-- <div class="section deveplan"> <h1>Individual Development Plan</h1> 
			<p>Below is a template you can use to list your development priorities and 
			create an action plan.</p> <xsl:call-template name="devplanitem"> <xsl:with-param 
			name="num" select="'1'" /> </xsl:call-template> <xsl:call-template name="devplanitem"> 
			<xsl:with-param name="num" select="'2'" /> </xsl:call-template> <xsl:call-template 
			name="devplanitem"> <xsl:with-param name="num" select="'3'" /> </xsl:call-template> 
			</div> -->
	</xsl:template>

	<xsl:template name="devplanitem">
		<xsl:param name="num" />
		<div class="planitem">
			<!-- <h2>Plan <xsl:value-of select="$num" /></h2> -->
			<div class="row row1 group">
				<div class="pivdev">
					<h3>Pivotal Development Area</h3>
					<div class="inputbox">
						<xsl:comment></xsl:comment>
					</div>
				</div>
				<div class="descr">
					<h3>Description of Issue</h3>
					<div class="inputbox">
						<xsl:comment></xsl:comment>
					</div>
				</div>
			</div>
			<div class="row row2 group">
				<div class="actions">
					<h3>Actions</h3>
					<div class="inputbox">
						<xsl:comment></xsl:comment>
					</div>
				</div>
				<div class="obstacles">
					<h3>Potential Obstacles</h3>
					<div class="inputbox">
						<xsl:comment></xsl:comment>
					</div>
				</div>
				<div class="dates">
					<h3>Target Dates</h3>
					<div class="inputbox">
						<xsl:comment></xsl:comment>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="appendix">
		<div class="section appendix group break">
			<h1>
				<xsl:value-of select="title" />
			</h1>
			<div class="subsection group">
				<h2>
					<xsl:value-of
						select="htmlSections/htmlSection[@id='leadershipCompetencies']/title" />
				</h2>
				<xsl:value-of disable-output-escaping="yes"
					select="htmlSections/htmlSection[@id='leadershipCompetencies']/text" />
			</div>
			<div class="subsection group">
				<h2>
					<xsl:value-of
						select="htmlSections/htmlSection[@id='leadershipExperience']/title" />
				</h2>
				<xsl:value-of disable-output-escaping="yes"
					select="htmlSections/htmlSection[@id='leadershipExperience']/text" />
			</div>
			<div class="subsection pagebreak group">
				<h2>
					<xsl:value-of
						select="htmlSections/htmlSection[@id='leadershipFoundations']/title" />
				</h2>
				<xsl:value-of disable-output-escaping="yes"
					select="htmlSections/htmlSection[@id='leadershipFoundations']/text" />
			</div>
			<div class="subsection group">
				<h2>
					<xsl:value-of
						select="htmlSections/htmlSection[@id='leadershipInterest']/title" />
				</h2>
				<xsl:value-of disable-output-escaping="yes"
					select="htmlSections/htmlSection[@id='leadershipInterest']/text" />
			</div>
			<div class="subsection group">
				<h2>
					<xsl:value-of
						select="htmlSections/htmlSection[@id='leadershipDerailers']/title" />
				</h2>
				<xsl:value-of disable-output-escaping="yes"
					select="htmlSections/htmlSection[@id='leadershipDerailers']/text" />
			</div>
		</div>
	</xsl:template>

	<xsl:template match="appSection">
		<div class="subsection group">
			<h2>
				<xsl:value-of select="title" />
			</h2>
			<p>
				<xsl:apply-templates select="text[1]" mode="textMagic" />
			</p>

			<div class="twocol">
				<xsl:for-each
					select="appendixSubSections/appSubSection[position() mod 2 = 1]">
					<div class="leftcol">
						<xsl:apply-templates select="." />
					</div>
					<div class="rightcol">
						<xsl:apply-templates select="following-sibling::appSubSection[1]" />
					</div>
				</xsl:for-each>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="appSubSection">
		<h3>
			<xsl:value-of select="title" />
		</h3>
		<xsl:apply-templates select="appendixTextGroups" />
	</xsl:template>

	<xsl:template match="appendixTextGroups">
		<xsl:apply-templates select="appTextGroup" />
	</xsl:template>

	<xsl:template match="appTextGroup">
		<h4>
			<xsl:value-of select="heading" />
		</h4>
		<p>
			<xsl:apply-templates select="text[1]" mode="textMagic" />
		</p>
	</xsl:template>

	<!-- XPATH string build logic -->
	<!-- http://www.stylusstudio.com/xsllist/200207/post41700.html -->

	<xsl:template match="noitems | text" mode="textMagic">
		<xsl:choose>
			<xsl:when test="@editable='true' and $genmode='jsf'">
				<xsl:variable name="xpath">
					<xsl:call-template name="generateXPath" />
				</xsl:variable>
				<p:inputTextarea rows="10" style="width:95%" onchange="$('#{@updateId}').text($(this).val());" onkeyup="$('#{@updateId}').text($(this).val());">
					<xsl:attribute name="value">
                        <xsl:text>#{xmlDocumentBean.findNodeByXPath('</xsl:text>
                        <xsl:value-of select="$xpath" />
                        <xsl:text>').textContent}</xsl:text>
                    </xsl:attribute>
				</p:inputTextarea>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="add-br">
					<xsl:with-param name="text" select="." />
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="add-br">
		<xsl:param name="text" select="." />
		<xsl:choose>
			<xsl:when test="contains($text, '&#xD;')">
				<xsl:value-of select="substring-before($text, '&#xD;')" />
				<br />
				<xsl:call-template name="add-br">
					<xsl:with-param name="text"
						select="substring-after($text,'&#xD;')" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="generateXPath">
		<xsl:for-each select="ancestor::*">
			/
			<xsl:value-of select="name()" />
			[
			<xsl:number />
			]
		</xsl:for-each>
		/
		<xsl:value-of select="name()" />
		[
		<xsl:number />
		]
	</xsl:template>


	<!-- http://stackoverflow.com/questions/1496434/getting-xslt-current-node-formatted-as-xpath-query 
		http://stackoverflow.com/questions/10636805/get-current-nodes-xpath -->
	<xsl:template match="*|@*" mode="make-path">
		<xsl:apply-templates select="parent::*" mode="make-path" />
		<xsl:text>/</xsl:text>
		<xsl:apply-templates select="." mode="make-name" />
		<xsl:choose>
			<xsl:when test="self::section">
				<xsl:apply-templates select="@id" mode="make-predicate" />
			</xsl:when>
			<xsl:when test="self::subPage">
				<xsl:apply-templates select="@user" mode="make-predicate" />
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="*|@*" mode="make-predicate">
		<xsl:text>[</xsl:text>
		<xsl:apply-templates select="." mode="make-name" />
		<xsl:text> = '</xsl:text>
		<xsl:value-of select="." />
		<xsl:text>']</xsl:text>
	</xsl:template>

	<xsl:template match="*" mode="make-name">
		<xsl:value-of select="name()" />
	</xsl:template>

	<xsl:template match="@*" mode="make-name">
		<xsl:text>@</xsl:text>
		<xsl:value-of select="name()" />
	</xsl:template>

</xsl:stylesheet>
