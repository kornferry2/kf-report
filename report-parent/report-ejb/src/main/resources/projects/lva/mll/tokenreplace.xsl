<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xalan="http://xml.apache.org/xslt"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="xsl xalan xs"
>

  <xsl:output method="xml" indent="yes" encoding="UTF-8" />

  <xsl:variable name="gender" select="/lvaDbReport/participant/gender" />
  
    <xsl:variable name="heshe">
    <xsl:choose>
      <xsl:when test="$gender = 'MALE'">he</xsl:when>
      <xsl:when test="$gender = 'FEMALE'">she</xsl:when>
      <xsl:when test="$gender = 'YOU'">you</xsl:when>
      <xsl:otherwise>he/she</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="Heshe">
    <xsl:choose>
      <xsl:when test="$gender = 'MALE'">He</xsl:when>
      <xsl:when test="$gender = 'FEMALE'">She</xsl:when>
      <xsl:when test="$gender = 'YOU'">You</xsl:when>
      <xsl:otherwise>He/she</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="hisher">
    <xsl:choose>
      <xsl:when test="$gender = 'MALE'">his</xsl:when>
      <xsl:when test="$gender = 'FEMALE'">her</xsl:when>
      <xsl:when test="$gender = 'YOU'">your</xsl:when>
      <xsl:otherwise>his/her</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="Hisher">
    <xsl:choose>
      <xsl:when test="$gender = 'MALE'">His</xsl:when>
      <xsl:when test="$gender = 'FEMALE'">Her</xsl:when>
      <xsl:when test="$gender = 'YOU'">Your</xsl:when>
      <xsl:otherwise>His/her</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="himher">
    <xsl:choose>
      <xsl:when test="$gender = 'MALE'">him</xsl:when>
      <xsl:when test="$gender = 'FEMALE'">her</xsl:when>
      <xsl:when test="$gender = 'YOU'">your</xsl:when>
      <xsl:otherwise>him/her</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>


  <xsl:variable name="isare">
    <xsl:choose>
      <xsl:when test="$gender = 'YOU'">are</xsl:when>
      <xsl:otherwise>is</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="hashave">
    <xsl:choose>
      <xsl:when test="$gender = 'YOU'">have</xsl:when>
      <xsl:otherwise>has</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="verbend">
    <xsl:choose>
      <xsl:when test="$gender = 'YOU'"></xsl:when>
      <xsl:otherwise>s</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="does">
    <xsl:choose>
      <xsl:when test="$gender = 'YOU'">do</xsl:when>
      <xsl:otherwise>does</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="youparticipants">
    <xsl:choose>
      <xsl:when test="$gender = 'YOU'">your</xsl:when>
      <xsl:otherwise>the participant's</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="youperson">
    <xsl:choose>
      <xsl:when test="$gender = 'YOU'">you</xsl:when>
      <xsl:otherwise>that person</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>


  <xsl:template match="@*|*|processing-instruction()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="text | noitems">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:call-template name="apply-gender">
        <xsl:with-param name="text" select="."/>
      </xsl:call-template>
    </xsl:copy>
  </xsl:template>

  <!--
      Big nested string replace to turn placeholder variables 
      like {himher} into "him" or "her" or "you"
  -->

  <xsl:template name="apply-gender">
    <xsl:param name="text"/>
    <xsl:call-template name="replace-substring">
      <xsl:with-param name="original">
        <xsl:call-template name="replace-substring">
          <xsl:with-param name="original">
            <xsl:call-template name="replace-substring">
              <xsl:with-param name="original">
                <xsl:call-template name="replace-substring">
                  <xsl:with-param name="original">
                    <xsl:call-template name="replace-substring">
                      <xsl:with-param name="original">
                        <xsl:call-template name="replace-substring">
                          <xsl:with-param name="original">
                            <xsl:call-template name="replace-substring">
                              <xsl:with-param name="original">
                                <xsl:call-template name="replace-substring">
                                  <xsl:with-param name="original">
                                    <xsl:call-template name="replace-substring">
                                      <xsl:with-param name="original">
                                        <xsl:call-template name="replace-substring">
                                          <xsl:with-param name="original">
                                            <xsl:call-template name="replace-substring">
                                              <xsl:with-param name="original" select="$text" />
                                              <xsl:with-param name="substring" select="'{youperson}'"/>
                                              <xsl:with-param name="replacement" select="$youperson"/>
                                            </xsl:call-template>
                                          </xsl:with-param>
                                          <xsl:with-param name="substring" select="'{youparticipants}'"/>
                                          <xsl:with-param name="replacement" select="$youparticipants"/>
                                        </xsl:call-template>
                                      </xsl:with-param>
                                      <xsl:with-param name="substring" select="'{does}'"/>
                                      <xsl:with-param name="replacement" select="$does"/>
                                    </xsl:call-template>
                                  </xsl:with-param>
                                  <xsl:with-param name="substring" select="'{s}'"/>
                                  <xsl:with-param name="replacement" select="$verbend"/>
                                </xsl:call-template>
                              </xsl:with-param>
                              <xsl:with-param name="substring" select="'{hashave}'"/>
                              <xsl:with-param name="replacement" select="$hashave"/>
                            </xsl:call-template>
                          </xsl:with-param>
                          <xsl:with-param name="substring" select="'{isare}'"/>
                          <xsl:with-param name="replacement" select="$isare"/>
                        </xsl:call-template>
                      </xsl:with-param>
                      <xsl:with-param name="substring" select="'{himher}'"/>
                      <xsl:with-param name="replacement" select="$himher"/>
                    </xsl:call-template>
                  </xsl:with-param>
                  <xsl:with-param name="substring" select="'{Hisher}'"/>
                  <xsl:with-param name="replacement" select="$Hisher"/>
                </xsl:call-template>
              </xsl:with-param>
              <xsl:with-param name="substring" select="'{hisher}'"/>
              <xsl:with-param name="replacement" select="$hisher"/>
            </xsl:call-template>
          </xsl:with-param>
          <xsl:with-param name="substring" select="'{heshe}'"/>
          <xsl:with-param name="replacement" select="$heshe"/>
        </xsl:call-template>
      </xsl:with-param>
      <xsl:with-param name="substring" select="'{Heshe}'"/>
      <xsl:with-param name="replacement" select="$Heshe"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="replace-substring">
    <xsl:param name="original"/>
    <xsl:param name="substring"/>
    <xsl:param name="replacement" select="''"/>
    <xsl:choose>
      <xsl:when test="contains($original, $substring)">
        <xsl:value-of select="substring-before($original, $substring)"/>
        <xsl:copy-of select="$replacement"/>
        <xsl:call-template name="replace-substring">
          <xsl:with-param name="original" select="substring-after($original, $substring)"/>
          <xsl:with-param name="substring" select="$substring"/>
          <xsl:with-param name="replacement" select="$replacement"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$original"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


</xsl:stylesheet>
