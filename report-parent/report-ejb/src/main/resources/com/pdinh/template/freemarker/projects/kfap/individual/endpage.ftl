<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" 
      xmlns:svg="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink">
<head>
<link href="${baseURL}/core/css/report.css" rel="stylesheet" type="text/css" />
<link href="${baseURL}${webPath}/css/report.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>End Page</title>
</head>

<body style="background-color:#757D7C;">
<img style="position:absolute; top:92px; left:69px;" src="${baseURL}/core/png/kf_logo_white.png" alt="Korn Ferry" />
<img style="position:absolute; top:95px; left:122px; width:3px; height:903px;" src="${baseURL}${webPath}/css/images/WhiteLineonGray.png" alt="Korn Ferry" />
<table  cellspacing="0" cellpadding="0" width="590px">
<tr>
<td height="1000px" width="140px"></td>
<td valign="bottom">
<div class="headertext">${endpageHeader}</div>
<p class="bodytext pad">
${endpageP1}
</p>
<p class="bodytext pad">
${endpageP2}
</p>
<p class="bodytext pad">
${endpageP3}
</p>
<p class="bodytext">
${copyright}
</p>
</td>
</tr>
<tr>
<td height="99px"></td>
<td></td>
<td></td>
<td></td>
</tr>
</table>

</body>
</html>
