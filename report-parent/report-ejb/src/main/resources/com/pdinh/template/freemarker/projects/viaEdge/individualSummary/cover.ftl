<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="www.w3.org/1999/xhtml"
	xmlns:svg="www.w3.org/2000/svg"
	xmlns:xlink="www.w3.org/1999/xlink">
<head>
<link href="${baseURL}/core/css/reportML.css" rel="stylesheet"
	type="text/css" />
<link href="${baseURL}${webPath}/css/report.css" rel="stylesheet" type="text/css" />
<title>cover</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>
<body class="${lang}" >
<div class="container" style="height:980px;">
  <img style="width:1.75in; margin-top:1.13in; margin-left:0.36in; margin-bottom:0.63in;" src="${baseURL}${webPath}/css/images/cover-logo.png" alt="viaEdge" /> 
  <img style="width:826px;" src="${baseURL}${webPath}/css/images/cover-image.jpg" alt="viaEdge Art" />
  
  <div id="upperLeft"> </div>
  <div style="margin-left:.5in;">
    <p class="reportname lang">${reportName}</p>
    <p class="fullname">${fullName}</p>
    <p class="reportdate">${reportDate}</p>
    <p class="reportserial">${reportSerial}</p>
    <!-- <p class="test0">test1</p>
    <p class="test1">test2</p>
    <p class="test2">test3</p>
    <p class="test3">test4</p>
    <p class="test4">test5</p>
   --></div>
   <div style="position:absolute; bottom:0px; left:48px;">
    <img style="heigth:100px;" src="${baseURL}/core/svg/edgeVersion.svg" alt="version"/>
   </div> 
  <div style="position:absolute; bottom:0px; right:-50px;"> <!--<img src="${baseURL}${webPath}/css/images/KF-logo.png" style="height:33px; position:absolute;"/> <span style="float:right; clear:right; font-size:18.5px; position:relative; top:-6px;">KORN/FERRY INTERNATIONAL</span>
    <hr style="height: 1px; position:relative; top:-6px; left:2px; clear:right;"/>
    <span style="float:right; clear:right; position:relative; top:-15px; font-size:1.17em;"> <i style="font-family:'Times, serif;">powered by</i><span style="font-family:'Times, serif;">LOMINGER</span></span>-->
    <img style="heigth:100px;" src="${baseURL}/core/svg/korn-ferry-logo.svg" alt="Korn Ferry"/>
    </div>
</div>
</body>
</html>
