<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:svg="http://www.w3.org/2000/svg"
	xmlns:xlink="http://www.w3.org/1999/xlink">
<head>
<link href="${baseURL}/core/css/report.css" rel="stylesheet"
	type="text/css" />
<link href="${baseURL}/projects/kfap/shared/css/report.css" rel="stylesheet" type="text/css" />
<title>footer</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="${baseURL}/core/js/common.js"></script>
</head>

<body onload="fillIn(['page'])">
	<div class="container">
		<div style="float: left; clear: left; width: 94%;"><span class="pc">${pc}</span> 
		<span class="kf">${kf}</span> <span class="footerdate">${footerdate}</span>
		</div>
		<div style="float: right; clear: right; width: 6%; text-align: right">
			<span class="page">x</span>
		</div>
	</div>
</body>
</html>