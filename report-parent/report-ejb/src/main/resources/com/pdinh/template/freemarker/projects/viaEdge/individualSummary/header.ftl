<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" 
      xmlns:svg="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink">
<head>
	<link href="${baseURL}/core/css/reportML.css" rel="stylesheet" type="text/css" />
	<link href="${baseURL}${webPath}/css/report.css" rel="stylesheet" type="text/css" />
	<title>header</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="text/javascript" src="${baseURL}/core/js/common.js"></script>
</head>

<body class="${lang}" onload="fillIn(['section'])">
	<div id="header">
		<img src="${baseURL}${webPath}/css/images/header-img.png" width="100%" alt="viaEdge" />
		<span class="section header-h1-right"></span>
	</div>
</body>
</html>
