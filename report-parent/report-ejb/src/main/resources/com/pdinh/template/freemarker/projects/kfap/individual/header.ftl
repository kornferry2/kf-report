<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" 
      xmlns:svg="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink">
<head>
<link href="${baseURL}/core/css/report.css" rel="stylesheet" type="text/css" />
<link href="${baseURL}${webPath}/css/report.css" rel="stylesheet" type="text/css" />
<title>KFAP</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
 
<body>

	<div class="container">
	
	<img style="position:absolute; top:40px; left:1px; width:800px; height:3px;" src="${baseURL}${webPath}/css/images/BlackLine.png" />
	
		<div style="float: left; clear: left; width: 94%;"><span class="alp">${alp}</span> | <span class="current">${current}</span>
		<br/><span class="participant">${participant}</span>
		</div>
	</div>
	
</body>
</html>
