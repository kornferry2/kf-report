<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0"
  xmlns="http://www.w3.org/1999/xhtml" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xalan="http://xml.apache.org/xslt"
  xmlns:svg="http://www.w3.org/2000/svg"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  exclude-result-prefixes="xsl xalan"
>

  <xsl:output method="xml" indent="yes"/>
  
  <xsl:param name="baseurl" select="'http://localhost'" />

  <xsl:template match="/ibReport">

    <xsl:variable name="participantFullName" select="concat(participant/firstName,' ',participant/lastName)" />
    <xsl:variable name="displayDate"
                  select="'Dummy Date'" />

    <html>
      <head>
        <title><xsl:value-of select="reportTitle" /> - <xsl:value-of select="company" /> - <xsl:value-of select="$participantFullName" /></title>
        <link href="{$baseurl}/ib_report.css" rel="stylesheet" type="text/css" />
      </head>
      <body>

        <div id="reportName">
          <p id="type"><xsl:value-of select="company" />&#160; <xsl:value-of select="reportTitle" /></p>
          <p id="subtype">Assessment for <xsl:value-of select="level" /></p>
        </div>

        <div id="headerFlow">
          <div class="fieldItem">
            <p class="fieldValue"><span><xsl:value-of select="$participantFullName" /></span></p>
            <p class="fieldSpace"><xsl:value-of select="$participantFullName" /></p>
            <p class="fieldName">Name</p>
          </div>
          <div class="fieldItem">
            <p class="fieldValue"><span><xsl:value-of select="participant/title" /></span></p>
            <p class="fieldSpace"><xsl:value-of select="participant/title" /></p>
            <p class="fieldName">Position</p>
          </div>
          <div class="fieldItem">
            <p class="fieldValue"><span><xsl:value-of select="$displayDate" /></span></p>
            <p class="fieldSpace"><xsl:value-of select="$displayDate" /></p>
            <p class="fieldName">Date</p>
          </div>
        </div>

        <div id="overall">
          <p class="heading">Overall Level of Performance</p>

          <div class="performanceBar">
            <p>
              <xsl:if test="overview/currentAttainment = 'novice'">
                <xsl:attribute name="class">selected</xsl:attribute>
              </xsl:if>
              <xsl:text>Novice</xsl:text>
            </p>
            <p>
              <xsl:if test="overview/currentAttainment = 'intermediate'">
                <xsl:attribute name="class">selected</xsl:attribute>
              </xsl:if>
              <xsl:text>Intermediate</xsl:text>
            </p>
            <p>
              <xsl:if test="overview/currentAttainment = 'solid'">
                <xsl:attribute name="class">selected</xsl:attribute>
              </xsl:if>
              <xsl:text>Solid</xsl:text>
            </p>
            <p>
              <xsl:if test="overview/currentAttainment = 'mastery'">
                <xsl:attribute name="class">selected</xsl:attribute>
              </xsl:if>
              <xsl:text>Mastery</xsl:text>
            </p>
          </div>

          <p class="narrative"><xsl:value-of select="normalize-space(overview/narrative)" /></p>

        </div>

        <div class="container">
          <xsl:apply-templates select="section" />
        </div>

      </body>

    </html>

  </xsl:template>

  <xsl:template match="section">

    <div class="competency" id="{@name}">
      <div class="header">
        <h2><xsl:value-of select="label" /></h2>
        <div class="scoreBar {score}">
          <h3><xsl:value-of select="score" /></h3>
          <p class="s1">1</p><p class="s2">3</p><p class="s3">3</p><p class="s4">4</p>
        </div>
      </div>
      <table cellpadding="0" cellspacing="0">
        <thead>
          <tr>
            <td width="33%">Highly effective behaviours</td>
            <td width="33%">competent behaviours</td>
            <td width="33%">behaviours needing development</td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <xsl:if test="count(behaviors/level[@name='effective']/point) &gt; 0">
                <ul>
                  <xsl:for-each select="behaviors/level[@name='effective']/point">
                    <li><xsl:value-of select="." /></li>
                  </xsl:for-each>
                </ul>
              </xsl:if>
            </td>
            <td>
              <xsl:if test="count(behaviors/level[@name='competent']/point) &gt; 0">
                <ul>
                  <xsl:for-each select="behaviors/level[@name='competent']/point">
                    <li><xsl:value-of select="." /></li>
                  </xsl:for-each>
                </ul>
              </xsl:if>
            </td>
            <td>
              <xsl:if test="count(behaviors/level[@name='development']/point) &gt; 0">
                <ul>
                  <xsl:for-each select="behaviors/level[@name='development']/point">
                    <li><xsl:value-of select="." /></li>
                  </xsl:for-each>
                </ul>
              </xsl:if>
            </td>
          </tr>
        </tbody>
      </table>

      <div class="comments">
        <h3>Comments</h3>
        <p><span><xsl:value-of select="normalize-space(comments)" /></span></p>
      </div>
      <div class="suggestions">
<!--
        <h3>Development Suggestions</h3>
        <xsl:if test="count(suggestions/point) &gt; 0">
          <ul>
            <xsl:for-each select="suggestions/point">
              <li><xsl:value-of select="." disable-output-escaping="yes" /></li>
            </xsl:for-each>
          </ul>
        </xsl:if>
-->
        <h3>Development Suggestions</h3>
        <xsl:if test="count(development/behavior/suggestions/suggestion) &gt; 0">
          <ul>
            <xsl:for-each select="development/behavior">
              <xsl:for-each select="suggestions/suggestion">
                <li><xsl:value-of select="." disable-output-escaping="yes" /></li>
              </xsl:for-each>
              <li>
                <xsl:text>Links to Instant Advice: </xsl:text>
                <xsl:for-each select="modules/module">
                  <a href="http://www.instantadvice.net/web/default.jsp?rn={@rn}&amp;module_id={@mid}"><xsl:value-of select="." /></a>
                  <xsl:if test="position() != last()">, </xsl:if>
                </xsl:for-each>
              </li>
            </xsl:for-each>
          </ul>
        </xsl:if>

      </div>
    </div>

  </xsl:template>

  <!-- These next two would be used if html in point wasn't escaped -->
  <xsl:template match="point">
    <xsl:apply-templates select="@*|node()" mode="point" />
  </xsl:template>
  <xsl:template match="@*|node()" mode="point">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" mode="point" />
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
