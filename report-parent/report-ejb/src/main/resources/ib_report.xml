<?xml version="1.0" encoding="utf-8"?>
<ibReport>
  <reportTitle>Leadership Assessment Report</reportTitle>
  <level>Leader of Teams</level>

  <reportDate>2011-01-11</reportDate>
  <accessmentDate>2011-01-11</accessmentDate>
  <scoringDate>2011-01-11</scoringDate>

  <company>Shell</company>

  <participant>
    <firstName>Samantha</firstName>
    <lastName>Sample</lastName>
    <title>Assistant Plant Manager</title>
  </participant>

  <overview>
    <currentAttainment>intermediate</currentAttainment>
    <narrative>Ms Sample performed at the Intermediate Level on the Leader of Teams Assessment.  She collaborates effectively with internal and external stakeholders, and leads with a strong set of personal values.  She would benefit from a greater focus on driving innovation and getting results through others.</narrative>
  </overview>


  <section name="authenticity">

    <label>Authenticity</label>
    <score>solid</score>

    <behaviors>
      <level name="effective">
        <point>Consistently demonstrates values and beliefs through action</point>
        <point>Has a high level of clarity on the source of personal motivation</point>
        <point>Adroitly balances honest self-expression with being open to other perspectives</point>
      </level>
      <level name="competent">
        <point>Models personal well-being</point>
      </level>
      <level name="development">
        <point>Displays hesitancy to speak difficult truths</point>
      </level>
    </behaviors>

    <comments>
        Ms Sample took many opportunities to convey her values as a
        leader to her team.  She leads with integrity.  She is not
        someone who needs to have the right answers herself, but
        rather is very open to other points of view and
        perspectives.  At the same time, she will readily express her
        opinion.  She comes across as a leader who is comfortable in
        her own skin.
    </comments>

    <suggestions>
      <point>Hold firm to be influential in your organization, you do
      not want to be seen as pushover or as stubborn and
      inflexible. It will always be important to pick your battles
      carefully. In some situations, you will need to maintain
      your position against the attempts to sway you.</point>
      <point>Find out about the other person’s concerns. To discover
      any potential misunderstandings you may want to ask the
      other person to explain your viewpoint, while you offer to
      explain his or hers. Restate your main points to check you
      are being understood.</point>
      <point>Challenge yourself to be outspoken and clear about
      things that are hard for you to say.  Make sure you first
      practice this in a safe environment.</point>
    </suggestions>

  </section>

  <section name="growth">
    <label>Growth</label>
    <score>novice</score>

    <behaviors>
      <level name="effective">
      </level>
      <level name="competent">
        <point>Aligns team targets to business goals.</point>
        <point>Visibly intervenes to manage safety and compliance risks.</point>
        <point>Seeks opportunities to enhance the reputation of the business.</point>
      </level>
      <level name="development">
        <point>Identifies conventional, status quo approaches</point>
        <point>Misses opportunities to encourage the team to apply ideas from outside the business</point>
        <point>Misses opportunities to harness creative ideas and approaches from the team</point>
        <point>Allows resistance to slow down or stall change efforts</point>
      </level>
    </behaviors>

    <comments>
        Ms Sample took a cautious and conventional approach in
        handling the issues in the simulation.  She consistently
        avoided opportunities to apply and leverage innovative
        thinking, even when suggested by others.  In the one situation
        where she seemed inclined to take a bolder approach, she
        backed down when faced with resistance.  She is likely quite
        risk-averse.
    </comments>

    <suggestions>
      <point>To increase your creative thinking, you need to
      challenge your thoughts and assumptions. This may mean being
      more open to ideas and feedback from others, or it may
      involve more sophisticated skills in challenging your
      underlying beliefs.</point>
      <point>Get into the habit of identifying and challenging
      underlying assumptions so you can spot possible errors and
      misjudgements.</point>
      <point>Listen to new employees. They often challenge the
      assumptions and work process. Use their observations to
      revitalize and improve business processes.</point>
      <point>Suggested Instant Advice module &lt;a href="http://www.pdinh.com/module?id=1"&gt;Building Self Confidence&lt;/a&gt; </point>
    </suggestions>

  </section>

  <section name="collaboration">
    <label>Collaboration</label>
    <score>intermediate</score>

    <behaviors>
      <level name="effective">
        <point>Fully and openly collaborates with other parts of the organisation</point>
        <point>Has a keen understanding of and embraces the advantages of diversity</point>
      </level>
      <level name="competent">
        <point>Coaches others to hold a customer and stakeholder mindset.</point>
      </level>
      <level name="development">
        <point>Delivers difficult messages upward.</point>
      </level>
    </behaviors>

    <comments>
      Ms Sample displayed a sincere interest in people and their
      needs, consistently thanked them for their input, and
      appropriately asked for additional information.  She showed a
      strong desire to build a team and to bring diverse people
      together on issues.
    </comments>

    <suggestions>
      <point>Work with your team to come up with different options for meeting key customer and stakeholder needs.</point>
      <point>You might be interested in this Instant Advice module <a href="http://www.pdinh.com/module?id=1">Emotional Intelligence</a> </point>
    </suggestions>

  </section>

  <section name="performance">
    <label>Performance</label>
    <score>intermediate</score>

    <behaviors>
      <level name="effective">
        <point>Consistently empowers the team to act and speak their mind</point>
      </level>
      <level name="competent">
        <point>Focuses team effort on activities that are likely to deliver commercial results</point>
        <point>Structures work allocation to provide some development opportunities</point>
        <point>Finds some opportunities to reduce complexity and waste</point>

      </level>
      <level name="development">
        <point>Sets goals that provide minimal stretch; does not gain commitment to deliver outcomes with urgency.</point>
        <point>Misses opportunities to reward success and delays intervening to apply consequences for non-delivery</point>
        <point>Fails to take advantage of coaching opportunities to enhance others’ performance and capabilities for future roles.</point>
        <point>Misses opportunities to hold honest performance conversations and delays intervening to address patterns of under- performance.</point>
      </level>
    </behaviors>

    <comments>
        Ms Sample did not always convey sufficient urgency to her
        team, and at times seemed to avoid the tougher interpersonal
        issues.  While empowering of her team, she missed many
        opportunities to provide guidance and direction and establish
        accountabilities.  She also did not capitalise on several
        coachable moments.
    </comments>

    <suggestions>
      <point>People learn more quickly when they have the right
      environment for learning. Create a safe atmosphere that is
      conducive to personal development.</point>
      <point>Be genuine. Let your own personality, insights,
      observations, and self-disclosure add depth and richness to
      you coaching efforts.</point>
      <point>Promote active experimentation.  Give employees
      permission to make mistakes as long as they learn from
      them.</point>
      <point>Emphasize small, reasonable steps. Because people learn
      in small steps, expecting too much too soon can discourage
      progress.</point>
    </suggestions>

  </section>

</ibReport>

